package com.dtc.commerce.api;

import java.util.List;

import com.adobe.cq.commerce.api.CommerceFacet;
import com.adobe.cq.commerce.api.CommerceQuery;
import com.adobe.cq.commerce.api.CommerceSort;
import com.adobe.cq.commerce.api.PaginationInfo;

/**
 * Command result.
 */
public abstract interface CommerceResult
{
    /**
     * Gets the pagination info.
     * 
     * @return The pagination info.
     */
    PaginationInfo getPaginationInfo();
    
    /**
     * Gets the sorts.
     * 
     * @return The sorts.
     */
    List<CommerceSort> getSorts();
    
    /**
     * Gets the products.
     * 
     * @return The products.
     */
    List<DtcItem> getProducts();
    
    /**
     * Gets the original query.
     * 
     * @return The original query.
     */
    CommerceQuery getOriginalQuery();
    
    /**
     * Gets the actual query.
     * 
     * @return The actual query.
     */
    CommerceQuery getActualQuery();
    
    /**
     * Gets the facets.
     * 
     * @return The facets.
     */
    List<CommerceFacet> getFacets();
}

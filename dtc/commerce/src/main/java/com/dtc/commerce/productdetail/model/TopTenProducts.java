/**
 * Change History:
 * Revision     Date         Dev         Comments
 * ------------------------------------------------------------------------------------------------
 * 1.0          09.29.2016   Deloitte Digital   MEC-6590 - Shop Landing Page - Top Ten Sellers (based by market) 
 */
package com.dtc.commerce.productdetail.model;

/**
 * The Class TopTenProducts.
 */
public class TopTenProducts {
	
	/**
	 * Instantiates TopTenProducts object.
	 */
	public TopTenProducts() {
	}
	
	/** The product name. */
	private String productName;
	
	/** The product sku. */
	private String productSku;
	
	/** The product image. */
	private String productImage;
	
	/** The product link. */
	private String productLink;
	
	/** The commerce type. */
	private String commerceType;
	
	/** The parent product sku. */
	private String parentProductSku;
	
	/**
	 * Gets the product name.
	 *
	 * @return the product name
	 */
	public String getProductName() {
		return productName;
	}

	/**
	 * Sets the product name.
	 *
	 * @param productName the new product name
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * Gets the product sku.
	 *
	 * @return the product sku
	 */
	public String getProductSku() {
		return productSku;
	}

	/**
	 * Sets the product sku.
	 *
	 * @param productSku the new product sku
	 */
	public void setProductSku(String productSku) {
		this.productSku = productSku;
	}

	/**
	 * Gets the product image.
	 *
	 * @return the product image
	 */
	public String getProductImage() {
		return productImage;
	}

	/**
	 * Sets the product image.
	 *
	 * @param productImage the new product image
	 */
	public void setProductImage(String productImage) {
		this.productImage = productImage;
	}

	/**
	 * Gets the product link.
	 *
	 * @return the product link
	 */
	public String getProductLink() {
		return productLink;
	}

	/**
	 * Sets the product link.
	 *
	 * @param productLink the new product link
	 */
	public void setProductLink(String productLink) {
		this.productLink = productLink;
	}

	/**
	 * Gets the commerce type.
	 *
	 * @return the commerce type
	 */
	public String getCommerceType() {
		return commerceType;
	}

	/**
	 * Sets the commerce type.
	 *
	 * @param commerceType the new commerce type
	 */
	public void setCommerceType(String commerceType) {
		this.commerceType = commerceType;
	}
	
	/**
	 * Gets the parent product sku.
	 *
	 * @return parent Product Sku
	 */
	public String getParentProductSku() {
		return parentProductSku;
	}

	/**
	 * Sets the parent product sku.
	 *
	 * @param parentProductSku the new parentProductSku
	 */
	public void setParentProductSku(String parentProductSku) {
		this.parentProductSku = parentProductSku;
	}
}

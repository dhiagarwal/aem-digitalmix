package com.dtc.commerce.api;

import org.apache.sling.api.SlingHttpServletRequest;

/**
 * Commerce context interface.
 */
public interface DtcCommerceContext
{

    /**
     * Gets the base store.
     * 
     * @return The base store.
     */
    String getBaseStore();

    /**
     * Gets the catalog identifier.
     * 
     * @return The catalog identifier.
     */
    String getCatalogId();
    
    /**
     * Gets the language.
     *  
     * @return The language.
     */
    String getLanguage();

    /**
     * Gets the base product content path.
     * 
     * @return The base product content path.
     */
    String getBaseProductContentPath();

    /**
     * Gets the base product data path.
     * 
     * @return The base product data path.
     */
    String getBaseProductDataPath();
    
    /**
     * Gets the commerce user.
     * 
     * @return The commerce user.
     */
    CommerceUser getUser();
    

    /**
     * Gets the Sling HTTP servlet request.
     * 
     * @return The Sling HTTP servlet request.
     */
    SlingHttpServletRequest getRequest();
    
    /**
     * Gets the category identifier.
     * 
     * @return The category identifier.
     */
    String getCategoryId();

    /**
     * Gets the category facet identifier.
     * 
     * @return The category facet identifier.
     */
    String getCategoryFacetIdentifier();
    
    /**
     * Get cart identifier name.
     * 
     * @return The cart identifier name.
     */
    String getCartIdentifierName();
    
}

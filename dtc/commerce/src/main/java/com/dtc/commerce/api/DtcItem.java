package com.dtc.commerce.api;

import java.util.Map;
import java.util.Set;

import com.day.cq.tagging.Tag;

/**
 * Dtc  item.
 */
public interface DtcItem
{
    /**
     * Gets the product code.
     * 
     * @return The product code.
     */
    String getCode();
    
    /**
     * Gets the page name.
     * 
     * @return The page name.
     */
    String getPageName();

    /**
     * Gets the product status.
     * 
     * @return The product status.
     */
    String getStatus();

    /**
     * Gets the product title.
     * 
     * @return The product title.
     */
    String getTitle();

    /**
     * Indicates if the stock level is managed.
     * 
     * @return If the stock level is managed.
     */
    boolean isManagedStockLevel();

    /**
     * Gets the product attributes.
     * 
     * @return The product attributes.
     */
    Map<String, DtcAttribute> getAttributes();

    /**
     * Gets the product category tags.
     * 
     * @return The product category tags.
     */
    Tag[] getTags();

    /**
     * Gets the product media items.
     * 
     * @return The product media items.
     */
    Map<String, DtcMediaItem> getMediaItems();
    
    /**
     * Gets the prices.
     * 
     * @return The prices.
     */
    Map<String, DtcPrice> getPrices();
    
    
    /**
     * Gets the product child items.
     * 
     * @return The product child items.
     */
    Set<DtcItem> getChildItems();
    
    /**
     * Gets the product data path.
     * 
     * @return The product data path.
     */
    String getDataPath();
    
}

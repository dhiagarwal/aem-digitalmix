package com.dtc.commerce.common;

import com.day.cq.replication.ReplicationActionType;
import com.day.cq.replication.Replicator;
import com.dtc.commerce.api.XmlProductFeedImporter;
import com.dtc.commerce.products.generated.Market;
import com.dtc.commerce.products.generated.Product;
import com.dtc.commerce.products.generated.ProductMessage;
import java.util.*;
import org.apache.commons.codec.binary.Base64;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.ItemExistsException;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.xml.bind.JAXBContext;
import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


@Service
@Component(metatype=true, label="XmlProductFeedImporter", description="Imports products into the pim from SAP")
public class XmlProductFeedImporterImpl implements XmlProductFeedImporter {
    final Logger logger = LoggerFactory.getLogger("sapFeed");

    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    @Reference
    private Replicator replicator;

    @Override
    public void importProducts(String productXml) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        logger.debug("Received message from sap:"+productXml);

        String version = getVersion(productXml);

        /**
         * If you are wondering what this class loader switching below is about, please read
         * http://dev.day.com/content/kb/home/cq5/CQ5Troubleshooting/OsgiClassLoading3Party.html
         *
         * The code below was not needed in CQ 5.3 but was added during the testing for the CQ 5.4 upgrade
         *
         * Apparently sometimes a class loaded by 3rd party libraries doesnt get found by the default OSGI
         * classloader in which case you can switch out the classloader like we did below.
         */

        ClassLoader tccl = Thread.currentThread().getContextClassLoader();
        Thread.currentThread().setContextClassLoader(JAXBContext.class.getClassLoader());
        Session session = null;
        ResourceResolver resourceResolver = null;
        try {
            JAXBContext context = JAXBContext.newInstance(new Class[]{ProductMessage.class, Product.class, Market.class});
            ProductMessage productMessage = (ProductMessage) context.createUnmarshaller().unmarshal(new ByteArrayInputStream(productXml.getBytes()));

            if(productMessage != null) {
                List<Product> products = productMessage.getProduct();

                resourceResolver = resourceResolverFactory.getAdministrativeResourceResolver(null);
                session = resourceResolver.adaptTo(Session.class);

                Node rootNode = getRootNode(session);

                for (Product product : products) {
                    List <Market> markets = product.getMarket();
                    for (Market market : markets) {
                        String countryCode = market.getId().toUpperCase();
                        Node countryNode = getCountryNode(rootNode, session, countryCode);
                        Node productNode = getProductNode(countryNode, product);
                        boolean saveAndReplicate = processMarket(version, product, productNode, market);
                        if (saveAndReplicate) {
                            session.save();
                            logger.debug("Saved session. About to replicate:"+productNode.getPath()+" session is live:"+session.isLive());
                            session.refresh(true);
                            replicator.replicate(session, ReplicationActionType.ACTIVATE, productNode.getPath());
                            logger.debug("replicated node:"+productNode.getPath());
                        }
                    }
                }
            } else {
                logger.debug("productMessage is null!");
            }
        } catch (Exception e) {
            logger.error("Error during sap product feed import.", e);
        } finally {
            if (session != null && session.isLive()) {
                session.logout();
            }

            if (resourceResolver != null && resourceResolver.isLive()) {
                resourceResolver.close();
            }
            // this line below replaces the class loader back to the OSGI classloader
            Thread.currentThread().setContextClassLoader(tccl);
        }
    }

    /**
     * compute the "version" by taking the MD5 Hash of the xml
     * @param xml
     * @return
     * @throws UnsupportedEncodingException
     * @throws NoSuchAlgorithmException
     */
    private String getVersion(String xml) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(xml.getBytes("UTF-8"));
        byte[] raw = md.digest();
        return new String(Base64.encodeBase64(raw), "ASCII");
    }

    /**
     * Processes the market object into the productNode. If things changed, then it returns true to indicate that a save
     * & replication of the node is needed
     * @param version
     * @param product
     * @param productNode
     * @param market
     * @return
     * @throws RepositoryException
     */
    private boolean processMarket(String version, Product product, Node productNode, Market market) throws RepositoryException {
        boolean saveAndReplicate = false;
        String productVersion = null;
        try {
            productVersion = productNode.getProperty("version").getString();
        } catch (javax.jcr.PathNotFoundException pnf) {
            logger.debug("No version on the productNode. This is new, so we write to it.");
        }

        if (productVersion == null || (productVersion.length() > 0 && !productVersion.equals(version))) {
            boolean purchasable = (market.getStatusCode().equals("10"))?true:false;
            logger.debug("productNode:"+productNode.getPath()+" market status code:"+market.getStatusCode()+" Purchasable:"+purchasable);

            productNode.setProperty("purchasable", Boolean.valueOf(purchasable));

            if(!(productNode.hasProperty("jcr:title"))){
                productNode.setProperty("jcr:title", product.getDescription());
            }

            productNode.setProperty("productName", product.getDescription());
            productNode.setProperty("identifier", product.getSku());
            productNode.setProperty("cq:commerceType", "product");
            productNode.setProperty("sling:resourceType", "commerce/components/product");
            productNode.addMixin("cq:Taggable");
            productNode.setProperty("version", version);
            saveAndReplicate = true;
        } else {
            logger.debug("No changes needed for:"+productNode.getPath());
        }
        return saveAndReplicate;
    }

    /**
     * Returns the root node. Creates it if it doesn't exist
     * @param session
     * @return
     * @throws RepositoryException
     */
    private Node getRootNode(Session session) throws RepositoryException {
        Node rootNode = session.getNode("/etc/commerce/products");
        if (!rootNode.hasNode("dtc")) {
            rootNode = rootNode.addNode("dtc");
            logger.debug("dtc node missing. Adding");
            session.save();
        } else {
            rootNode = rootNode.getNode("dtc");
        }
        return rootNode;
    }

    /**
     * Returns the country node under the rootNode. If it doesn't exist it creates it.
     * @param rootNode
     * @param session
     * @param countryCode
     * @return
     * @throws RepositoryException
     */
    private Node getCountryNode(Node rootNode, Session session, String countryCode) throws RepositoryException {
        Node countryNode;
        if (!rootNode.hasNode(countryCode)) {
            logger.debug(rootNode.getPath()+ " does not have node:"+countryCode);
            // create country code
            countryNode = rootNode.addNode(countryCode);
            logger.debug("Country node missing:"+countryCode+" Adding.");
            session.save();
        } else {
            countryNode = rootNode.getNode(countryCode);
        }
        return countryNode;
    }

    /**
     * Returns the product node the data should be written to. If it doesn't exist it creates it.
     * @param countryNode
     * @param product
     * @return
     * @throws RepositoryException
     */
    private Node getProductNode(Node countryNode, Product product) throws RepositoryException {
        String sku = product.getSku();
        String nodePath = sku.substring(0,2)+"/"+sku.substring(2,4)+"/"+sku.substring(4,6)+"/"+sku.substring(6,8)+"/"+sku;

        Node productNode;
        if (countryNode.hasNode(nodePath)) {
            productNode = countryNode.getNode(nodePath);
        } else {
            Node first = addNode(countryNode, sku.substring(0, 2));
            Node second = addNode(first,sku.substring(2,4));
            Node third = addNode(second,sku.substring(4,6));
            Node fourth = addNode(third,sku.substring(6,8));
            if (fourth.hasNode(sku)) {
                productNode = fourth.getNode(sku);
            } else {
                productNode = fourth.addNode(sku, "nt:unstructured");
            }
            logger.debug("Added product node.");
        }
        return productNode;
    }

    private Node addNode(Node parent, String child) throws RepositoryException {
        Node rv = null;
        if (parent.hasNode(child)) {
            rv = parent.getNode(child);
        } else {
            rv = parent.addNode(child);
        }
        return rv;
    }
}
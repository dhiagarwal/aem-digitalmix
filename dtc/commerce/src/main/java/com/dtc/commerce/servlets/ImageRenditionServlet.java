/**
 * Change History:
 * Revision     Date         Dev         Comments
 * ------------------------------------------------------------------------------------------------
 * 1.0          17.11.2016   Deloitte Digital   MEC-10955 - Using image renditions for all product assets
 */
package com.dtc.commerce.servlets;

import com.dtc.commerce.productdetail.common.ProductDetailConstant;
import com.dtc.commerce.productdetail.model.ProductDetail;
import com.dtc.commerce.productdetail.service.ProductDetailService;
import com.dtc.commerce.productdetail.service.ProductsRenditionService;
import com.dtc.core.services.PageLocaleService;
import com.dtc.core.utils.LocaleUtil;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.servlet.ServletException;
import java.io.IOException;
import java.util.List;
import java.util.Locale;


/**
 * The Class ImageRenditionServlet.
 */
@SuppressWarnings("serial")
@SlingServlet(paths = "/bin/dtc/imageDataServlet", methods = "GET", metatype = true)
public class ImageRenditionServlet extends SlingSafeMethodsServlet {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(ImageRenditionServlet.class);
	
	/** The image names. */
	JSONArray imageNames = null; 

	/** The products rendition service. */
	@Reference
	ProductsRenditionService productsRenditionService;
	
	/** The product detail service. */
	@Reference
	ProductDetailService productDetailService;
	
	private String BASE_SKU = "baseSku";
	
	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		LOGGER.info("Entering: ImageRenditionServlet :  doGet");
		try {
			String skuID = request.getParameter(ProductDetailConstant.CATEGORY_SKUID);
			String pagePath = request.getParameter(ProductDetailConstant.PATH);
			Locale locale = null;
			if(StringUtils.isNotBlank(pagePath)){
				locale = LocaleUtil.getLocaleFromPath(pagePath);
			}
			if (StringUtils.isNotBlank(skuID)) {
				String[] skuIDArray = skuID.split(",");
				if (skuIDArray.length > 0) {
					try {
						imageNames = getImageNames(skuIDArray,request,locale);
					} catch (Exception e) {
						LOGGER.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
					}
				}
			}
		} catch (Exception ex) {
			LOGGER.error(ex.getClass() + ": " + ex.getMessage() + ": " + ex.getCause(), ex);
		}
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json");
		response.getWriter().println(imageNames);

		LOGGER.info("Exiting: ImageRenditionServlet :  doGet");
	}

	/**
	 * Gets the image names.
	 *
	 * @param skuIDArray the sku ID array
	 * @param request the request
	 * @param locale the locale
	 * @return the image names
	 * @throws JSONException the JSON exception
	 * @throws RepositoryException the repository exception
	 */
	private JSONArray getImageNames(String[] skuIDArray,SlingHttpServletRequest request,Locale locale) throws JSONException, RepositoryException {
		LOGGER.info("Entering: ImageRenditionServlet :  getImageNames");

		JSONArray imagesData = new JSONArray();
		try {
			
			for (int i = 0; i < skuIDArray.length; i++) {
				
				JSONObject imageData = new JSONObject();
				imageData.put(ProductDetailConstant.SKU, skuIDArray[i]);
				if(null != locale){
					String productTitle = getProductTitle(skuIDArray[i],locale);
					String productDesc = getProductDesc(skuIDArray[i],locale);
					String baseSku = getBaseSku(skuIDArray[i], locale);
					imageData.put(ProductDetailConstant.PRODUCT_TITLE, productTitle);
					imageData.put(ProductDetailConstant.PROPERTY_DESCRIPTION, productDesc);
					imageData.put(BASE_SKU, baseSku);

				}
				
				String imagesPath = ProductDetailConstant.DAM_BASE_PATH + skuIDArray[i];
				
				if(null != request.getResourceResolver().getResource(imagesPath)){
					Node imagesNodes = request.getResourceResolver().getResource(imagesPath).adaptTo(Node.class);
					NodeIterator nodeIterator = imagesNodes.getNodes();
					
					while (nodeIterator.hasNext()) {
						Node imageNode = nodeIterator.nextNode();
						if (imageNode.getPrimaryNodeType().getName().equals(ProductDetailConstant.DAM_ASSET)){
							imageData.put(ProductDetailConstant.NAME, imageNode.getName());
							imageData.put(ProductDetailConstant.PATH, imageNode.getPath());
							break;
						}
					}
				}
				imagesData.put(imageData);
			}
		} catch (Exception e) {
			LOGGER.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}
		LOGGER.info("Exiting: ImageRenditionServlet :  getImageNames");
		return imagesData;

	}
	
	/**
	 * Gets the product title.
	 *
	 * @param productSku the product sku
	 * @param locale the locale
	 * @return the product title
	 */
	private String getProductTitle(String productSku,Locale locale){
		LOGGER.info("Entering: ImageRenditionServlet :  getProductName");
		String productName = "";
		if(StringUtils.isNotBlank(productSku)){
			List<ProductDetail> productProperties = productDetailService.getProductDetail(productSku, locale);
			for (ProductDetail data : productProperties) {
				productName = data.getJcrTitle();
			}
		}
		LOGGER.info("Exiting: ImageRenditionServlet :  getProductName");
		return productName;
	}
	
	/**
	 * Gets the product description.
	 *
	 * @param productSku the product sku
	 * @param locale the locale
	 * @return the product description
	 */
	private String getProductDesc(String productSku,Locale locale){
		LOGGER.info("Entering: ImageRenditionServlet :  getProductDesc");
		String productDesc = "";
			List<ProductDetail> productProperties = productDetailService.getProductDetail(productSku, locale);
			for (ProductDetail data : productProperties) {
				if(data.getCommerceType().equalsIgnoreCase(ProductDetailConstant.VARIANT)) {
					productDesc = data.getBaseDescription();
				}
				else {
					productDesc = data.getDescription();
				}
			}
		LOGGER.info("Exiting: ImageRenditionServlet :  getProductDesc");
		return productDesc;
	}
	
	
	/**
	 * Gets the base sku.
	 *
	 * @param productSku the product sku
	 * @param locale the locale
	 * @return the baseSku
	 */
	private String getBaseSku(String productSku,Locale locale){
		LOGGER.info("Entering: ImageRenditionServlet :  getBaseSku");
		String baseSku = "";
		if(StringUtils.isNotBlank(productSku)){
			List<ProductDetail> productProperties = productDetailService.getProductDetail(productSku, locale);
			for (ProductDetail data : productProperties) {
				baseSku = data.getBaseProductIdentifier();
			}
		}
		LOGGER.info("Exiting: ImageRenditionServlet :  getBaseSku");
		return baseSku;
	}

}

/**
 * Change History:
 * Revision     Date         Dev         Comments
 * ------------------------------------------------------------------------------------------------
 * 1.0          10.22.2016   Deloitte Digital    MEC-9965 Links in shop navigation do not match links at bottom of cat. landing page
 */
package com.dtc.commerce.productdetail.sightly;

import org.apache.sling.api.resource.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.RepositoryException;
import javax.jcr.Session;

import java.util.HashMap;
import java.util.Map;

import com.adobe.cq.sightly.WCMUsePojo;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.SearchResult;
import com.dtc.commerce.productdetail.common.ProductDetailConstant;

/**
 * The Class CategoryFooterUsePojo.
 */
public class CategoryFooterUsePojo extends WCMUsePojo {

	public CategoryFooterUsePojo() {
		
	}

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(CategoryFooterUsePojo.class);

	/** The header resource path. */
	private String headerResourcePath;
	
	/** The builder. */
	private QueryBuilder builder;
	
	/** The session. */
	private Session session;
	
	/** The footerNavigation. */
	private String footerNavigation;
	
	/** The footerNavigationMobile. */
	private String footerNavigationMobile;
	
	@Override
	public void activate() throws Exception {
		LOGGER.info("Entering CategoryFooterUsePojo:activate");
		try {
			Resource currentResource = getResource();
			
			footerNavigation = getProperties().get(ProductDetailConstant.FOOTER_NAV,ProductDetailConstant.NUTRITION);
			footerNavigationMobile = footerNavigation+ProductDetailConstant.MOBILE;
			
			builder = getSlingScriptHelper().getService(QueryBuilder.class);
			session = getResourceResolver().adaptTo(Session.class);
			
			headerResourcePath = getHeaderResourcePath(currentResource);
		} catch(RepositoryException e){
			LOGGER.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		} 
		LOGGER.info("Exiting CategoryFooterUsePojo:activate");
	}


	/**
	 * Gets the header resource path.
	 *
	 * @param currentResource the current resource
	 * @return the header resource path
	 * @throws RepositoryException the repository exception
	 */
	private String getHeaderResourcePath(Resource currentResource) throws RepositoryException {
		
		String resourcePath ;
		Resource cuResource = currentResource;

		do{
			resourcePath = executeSearch(cuResource);
			cuResource = cuResource.getParent();
		}while(resourcePath == null);

		return resourcePath;
	}

	/**
	 * Execute search.
	 *
	 * @param currentResource the current resource
	 * @return the string
	 */
	private String executeSearch(Resource currentResource) {
		String searchResultPath = null;
		try {
			String currentResourcePath = currentResource.getPath();
			
				Map<String, String> queryString = new HashMap<String, String>();
				queryString.put("type", ProductDetailConstant.NODE_TYPE_NT_UNSTRUCTURED);
				queryString.put("property", ProductDetailConstant.PROPERTY_SLING_RESOURCE_TYPE);
				queryString.put("property.value", ProductDetailConstant.HEADER_COMPONENT_PATH);
				queryString.put("nodename", ProductDetailConstant.HEADER_COMPONENT_NAME);
				queryString.put("path", currentResourcePath);

				PredicateGroup predicateGroup = PredicateGroup.create(queryString);
				Query query = builder.createQuery(predicateGroup, session);
				SearchResult result = query.getResult();
				
				if (!result.getHits().isEmpty()) {
					for (int i = 0; i < result.getHits().size(); i++) {
						if((result.getHits().get(i).getPath()).startsWith(currentResourcePath+"/jcr:content")){
							searchResultPath = result.getHits().get(i).getPath();
							return searchResultPath;
						}
					}
				}
		} catch (RepositoryException e) {
			LOGGER.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}
		return searchResultPath;
	}


	/**
	 * Gets the header resource path.
	 *
	 * @return the header resource path
	 */
	public String getHeaderResourcePath() {
		return headerResourcePath;
	}
	
	/**
	 * Gets the footerNavigation.
	 *
	 * @return the footerNavigation
	 */
	public String getFooterNavigation() {
		return footerNavigation;
	}
	
		/**
	 * Gets the footerNavigation.
	 *
	 * @return the footerNavigation
	 */
	public String getFooterNavigationMobile() {
		return footerNavigationMobile;
	}
}

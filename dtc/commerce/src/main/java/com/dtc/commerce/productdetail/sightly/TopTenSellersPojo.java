/**
 * Change History:
 * Revision     Date         Dev         Comments
 * ------------------------------------------------------------------------------------------------
 * 1.0          09.29.2016   Deloitte Digital   MEC-6590 - Shop Landing Page - Top Ten Sellers (based by market) 
 */
package com.dtc.commerce.productdetail.sightly;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.commons.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;
import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.Rendition;
import com.dtc.commerce.productdetail.model.TopTenProducts;
import com.dtc.commerce.productdetail.service.ProductsRenditionService;
import com.dtc.core.utils.LocaleUtil;
import com.dtc.commerce.productdetail.common.ProductDetailConstant;

/**
 * The Class TopTenSellersPojo.
 */
public class TopTenSellersPojo extends WCMUsePojo {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(TopTenSellersPojo.class);
	
	/** The sellers list. */
	private List<TopTenProducts> sellersList;
	
	/** The product base path. */
	private String productBasePath;
	
	private static final String productContainerPath = "/jcr:content/productContainer";
	
	@Override
	public void activate() throws Exception {
		LOGGER.info("Entering TopTenSellersPojo:activate");

		String[] productsDialogPath = getProperties().get("topSellers", String[].class);
		productBasePath = getProperties().get("pdpBasePath",StringUtils.EMPTY);

		if (productsDialogPath != null) {
			sellersList = getTopTenProperties(productsDialogPath);
		}

		LOGGER.info("Exiting TopTenSellersPojo:activate");
	}

	/**
	 * Gets the top ten properties.
	 *
	 * @param productsDialogPath the products dialog path
	 * @return the top ten properties
	 * @throws JSONException 
	 */
	private List<TopTenProducts> getTopTenProperties(String[] productsDialogPath) throws JSONException {
		
		String pagePath = getCurrentPage().getPath();

		Locale locale = LocaleUtil.getLocaleFromPath(pagePath);
		String localeStr = locale.toString();
		String languageStr = locale.getLanguage();
		
		Resource pmdResource;

		List<TopTenProducts> topSellersList = new ArrayList<TopTenProducts>();
		try {
			int totalProducts = productsDialogPath.length;
			for (int i = 0; i < totalProducts; i++) {
				TopTenProducts topProducts = new TopTenProducts();
				String currentProduct = productsDialogPath[i];
				if(null != getResourceResolver().getResource(currentProduct)){
					
					// Check to see if pmd page exists with full locale first.
					pmdResource = getResourceResolver().getResource(currentProduct + "/" + localeStr + productContainerPath);
					if (pmdResource == null) {
						// full locale pmd node doesn't exist so check to see if it
						// exists with just a language.
						pmdResource = getResourceResolver().getResource(currentProduct + "/" + languageStr + productContainerPath);
					}
					
					if (pmdResource != null) {
						
						Node node = pmdResource.adaptTo(Node.class);
						
						if (node.hasNode("productDetail")) {
							Node pmdDetailNode = node.getNode("productDetail");
							if (pmdDetailNode.hasProperty(ProductDetailConstant.PROPERTY_JCR_TITLE)) {
								topProducts.setProductName(pmdDetailNode.getProperty(ProductDetailConstant.PROPERTY_JCR_TITLE).getString());
							}
							topProducts.setProductSku(node.getParent().getParent().getParent().getName().toUpperCase());
							topProducts.setProductImage(getImageforSku(node.getParent().getParent().getParent().getName().toUpperCase()));
							topProducts.setParentProductSku(node.getParent().getParent().getParent().getName().toUpperCase());
							topProducts.setProductLink(productBasePath + ProductDetailConstant.DOT + node.getParent().getParent().getParent().getName() + ProductDetailConstant.HTML);
						}
						
						// Get the baseSku from the pmdPage so we can build the pimPath
						// correctly
						String baseSku = pmdResource.getValueMap().get("baseSku", String.class);
						if (StringUtils.isNotEmpty(baseSku)) {
							topProducts.setProductLink(productBasePath + ProductDetailConstant.DOT + baseSku + "." + node.getParent().getParent().getParent().getName() + ProductDetailConstant.HTML);
							topProducts.setParentProductSku(baseSku.toUpperCase());
						}
						topSellersList.add(topProducts);
					}
				}
			}
		} catch (RepositoryException e) {
			LOGGER.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}
		return topSellersList;
	}
	
	/**
	 * Gets the imagefor sku.
	 *
	 * @param sku the sku
	 * @return the imagefor sku
	 * @throws RepositoryException the repository exception
	 * @throws JSONException the JSON exception
	 */
	private String getImageforSku(String sku)
			throws RepositoryException, JSONException {
		LOGGER.info("Entering: TopTenSellers :  getImageforSku");
		String imageforSku = "";
		try {
			String imagesPath = ProductDetailConstant.DAM_BASE_PATH + sku;
			String topTenSellersRendition = getSlingScriptHelper().getService(ProductsRenditionService.class).getTopTenSellersRendition();
			ResourceResolver resourceResolver = getResourceResolver();
			if(null != resourceResolver.getResource(imagesPath)){
				Node imagesNodes = resourceResolver.getResource(imagesPath).adaptTo(Node.class);
				NodeIterator nodeIterator = imagesNodes.getNodes();

				while (nodeIterator.hasNext()) {
					Node imageNode = nodeIterator.nextNode();
					if (imageNode.getPrimaryNodeType().getName().equals(ProductDetailConstant.DAM_ASSET)){
						Rendition imageRenditions = resourceResolver.getResource(imageNode.getPath().toString()).adaptTo(Asset.class).getRendition(topTenSellersRendition);
		        		if(null !=  imageRenditions){
		        			imageforSku = imageRenditions.getPath().toString();
		        			break;
		        		}
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}
		LOGGER.info("Exiting: TopTenSellers :  getImageforSku");
		return imageforSku;
	}

	/**
	 * Gets the sellers list.
	 *
	 * @return the sellers list
	 */
	public List<TopTenProducts> getSellersList() {
		return sellersList;
	}

}
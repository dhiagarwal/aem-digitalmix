package com.dtc.commerce.api;

/**
 * Commerce exception.
 */
public class CommerceException extends Exception
{
    private static final long serialVersionUID = 1L;

    /**
     * Constructor with exception message.
     * 
     * @param message The exception message.
     */
    public CommerceException(final String message)
    {
        super(
                message);
    }


    /**
     * Constructor with exception message and throwable exception.
     * 
     * @param message The exception message.
     * @param throwable The throwable exception.
     */
    public CommerceException(final String message, final Throwable throwable)
    {
        super(
                message, throwable);
    }
}

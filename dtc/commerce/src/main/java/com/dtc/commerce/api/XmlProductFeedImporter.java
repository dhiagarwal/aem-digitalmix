package com.dtc.commerce.api;

import org.apache.sling.api.SlingHttpServletRequest;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;


public interface XmlProductFeedImporter {
    public void importProducts(String productXml) throws UnsupportedEncodingException, NoSuchAlgorithmException;
}

package com.dtc.commerce.utils;
import com.dtc.commerce.productdetail.common.ProductDetailConstant;


public class PMDUtil {
	
	

    public static String getPmdPath(String sku) {
        return ProductDetailConstant.PMD_BASE_PATH + tokenizeSku(sku);
    }

    public static String getPimPath(String sku, String countryCode) {
        return ProductDetailConstant.COMMERCE_BASE_PATH + countryCode + "/" + tokenizeFullSku(sku);
    }

    public static String tokenizeSku(String sku) {
        return sku.substring(0,2) + "/" + sku.substring(2,4) + "/" + sku.substring(4,6) + "/" + sku;
    }

    public static String tokenizeFullSku(String sku) {
        String path = "";
        for (int i=0; i<sku.length(); i+=2) {
            if (i+2 <= sku.length()) {
                path += sku.substring(i, i+2) + "/";
            } else {
                path += sku.substring(i, i+1) + "/";
            }
        }
        return path + sku;
    }
    
   
}

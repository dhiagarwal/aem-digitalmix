package com.dtc.commerce.utils;

import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import com.day.cq.commons.jcr.JcrUtil;

/**
 * Commerce utility class.
 */
public final class CommerceUtil
{
    private static final Pattern PRODUCT_CODE_PATTERN = Pattern.compile("(..?)");
    private static final Object COMMERCE_BASE_PATH = "/etc/commerce";
    
    /**
     * Gets the product path.
     * 
     * @param basePath The product base path.
     * @param code The product code.
     * @return The product path.
     */
    public static String getProductPath(final String basePath, final String code)
    {
        return getProductPath(basePath, code, StringUtils.EMPTY);
    }

    /**
     * Gets the product path with suffix.
     * 
     * @param basePath The product base path.
     * @param code The product code.
     * @param pathSuffix The path suffix.
     * @return The product path.
     */
    public static String getProductPath(final String basePath, final String code, final String pathSuffix)
    {
        final String sanitisedCode = JcrUtil.createValidName(code, JcrUtil.HYPHEN_LABEL_CHAR_MAPPING);

        return new StringBuilder().append(basePath).append("/")
                .append(PRODUCT_CODE_PATTERN.matcher(sanitisedCode).replaceAll("$1/")).append(sanitisedCode)
                .append(pathSuffix).toString();
    }
    
    /**
     * Gets the product path.
     * 
     * @param baseStore The base store.
     * @param catalog The catalog.
     * @param language The language.
     * @param code The code.
     * @return The product path.
     */
    public static String getProductPath(final String baseStore, final String catalog, final String language,
            final String code)
    {
        final String sanitisedCode = JcrUtil.createValidName(code, JcrUtil.HYPHEN_LABEL_CHAR_MAPPING);

        return new StringBuilder().append(COMMERCE_BASE_PATH).append("/").append(baseStore).append("/").append(catalog)
                .append("/").append(language).append("/products/")
                .append(PRODUCT_CODE_PATTERN.matcher(sanitisedCode).replaceAll("$1/")).append(sanitisedCode).toString();
    }

    /**
     * Default Constructor.
     * 
     * Private constructor to restrict instantiation.
     */
    private CommerceUtil()
    {

    }

}

package com.dtc.commerce.api;

/**
 * Dtc attribute.
 */
public interface DtcAttribute
{
    /**
     * Gets the attribute key.
     * 
     * @return The attribute key.
     */
    String getKey();

    /**
     * Gets the attribute value.
     * 
     * @return The attribute value.
     */
    String getValue();

    /**
     * Gets the attribute values.
     * 
     * @return The attribute values.
     */
    String[] getValues();

    /**
     * Indicates if the attributes values are multiple.
     * 
     * @return If the attributes values are value.
     */
    boolean isMultiple();

}

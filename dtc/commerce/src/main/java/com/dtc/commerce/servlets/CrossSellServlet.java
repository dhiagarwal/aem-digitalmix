/**
 * Change History:
 * Revision     Date         Dev         Comments
 * ------------------------------------------------------------------------------------------------
 * 1.0          09.17.2016   Deloitte Digital   MEC-3294 - Display cross-sell products on product detail page
 */

package com.dtc.commerce.servlets;

import java.io.IOException;
import java.util.Locale;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dtc.commerce.productdetail.service.ProductDetailService;
import com.dtc.core.utils.LocaleUtil;

/**
 * The Class CrossSellServlet.
 */
@Service(CrossSellServlet.class)
@SlingServlet(paths = { "/bin/dtc/crossSell" }, generateComponent = false, methods = "GET")
@Component(label = "DTC Products crossSell", description = "DTC Products crossSell", enabled = true, immediate = true, metatype = false)
public class CrossSellServlet extends SlingAllMethodsServlet {

	/** The Constant PATH. */
	private static final String PATH = "path";

	/** The Constant SKUID. */
	private static final String SKUID = "skuId";

	/** The service. */
	@Reference
	ProductDetailService service;

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(CrossSellServlet.class);

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -487199529373369499L;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache.
	 * sling.api.SlingHttpServletRequest,
	 * org.apache.sling.api.SlingHttpServletResponse)
	 */
	@Override
	protected void doGet(SlingHttpServletRequest req, SlingHttpServletResponse resp) throws IOException {
		LOGGER.info("Entering: CrossSellServlet :  doGet");
		
		String recommendations = "";
		try {
			
			String pagePath = req.getParameter(PATH);
			String skuid = req.getParameter(SKUID);
			
			JSONArray recos = new JSONArray();
			Locale locale = LocaleUtil.getLocaleFromPath(pagePath);
			recos = service.crossSellInformation(skuid, locale);

			recommendations = recos.toString();
			resp.setCharacterEncoding("UTF-8");
			resp.setContentType("application/json");
			resp.getWriter().println(recommendations);
		} catch (Exception e) {
			LOGGER.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}

	}

}

package com.dtc.commerce.productdetail.model;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

@Model(adaptables=Resource.class)
public class CategoryFooter {
  
  @Inject @Optional
  public Resource h2categorylinks;
  
  @Inject @Optional
  public Resource h3categorylinks;
  
  @Inject @Optional
  public Resource h4categorylinks;

} 
/**
 * Change History:
 * Revision     Date         Dev         Comments
 * ------------------------------------------------------------------------------------------------
 * 1.0          09.11.2016   Deloitte Digital   MEC-5106 - Category Ingredients 
 */
package com.dtc.commerce.productdetail.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;

import org.apache.felix.scr.annotations.Reference;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;

/**
 * The Class ProductsIngredientsServiceImpl.
 */
public class ProductsIngredientsServiceImpl implements ProductsIngredientsService {

	/** The logger. */
	private static final Logger logger = LoggerFactory.getLogger(ProductsIngredientsServiceImpl.class);

	/** The Constant INGREDIENTS. */
	private static final String KEYINGREDIENTS = "keyIng";

	/** The Constant PRODUCTSPATH. */
	private static final String PRODUCTSPATH = "/content/pmd/";

	/** The Constant PRODUCTCONTAINERPATH. */
	private static final String PRODUCTCONTAINERPATH = "/jcr:content/productContainer";

	/** The Constant TAGS. */
	private static final String TAGS = "tags";

	/** The resource resolver factory. */
	@Reference
	private transient ResourceResolverFactory resourceResolverFactory;

	/** The tag manager. */
	@Reference
	TagManager tagManager;

	/**
	 * Instantiates a new products ingredients service impl.
	 */
	public ProductsIngredientsServiceImpl() {
		super();
	}

	/*
	 * @see com.dtc.commerce.productdetail.service.ProductsIngredientsService#
	 * getAllTagsList(java.lang.String,
	 * org.apache.sling.api.resource.ResourceResolver)
	 */
	@Override
	public List<String> getAllTagsList(Locale locale, ResourceResolver resourceResolver) throws Exception {
		logger.info("Entering ProductsIngredientsServiceImpl.getAllTagsList()");
		List<String> tagList = new ArrayList<>();
		Session session = null;
		try {
			session = resourceResolver.adaptTo(Session.class);
			tagManager = resourceResolver.adaptTo(TagManager.class);
			String language = locale.getLanguage();
			if (session != null) {
				QueryManager queryManager = session.getWorkspace().getQueryManager();
				String sqlStatement = "select * from [cq:Page] as s where ISDESCENDANTNODE([" + PRODUCTSPATH
						+ "]) and s.[jcr:content/cq:template] = '/apps/dtc/commerce/template/productMasterDetail'";

				Query query = queryManager.createQuery(sqlStatement, "JCR-SQL2");
				QueryResult result = query.execute();
				NodeIterator nodeIter = result.getNodes();
				while (nodeIter.hasNext()) {
					Node node = nodeIter.nextNode();
					if (language.equalsIgnoreCase(node.getName())) {
						Resource res = resourceResolver.getResource(node.getPath() + PRODUCTCONTAINERPATH);
						Node pmdLocaleNodes = res.adaptTo(Node.class);
						NodeIterator pmdNodesIter = pmdLocaleNodes.getNodes();
						while (pmdNodesIter.hasNext()) {
							Node pmdLocaleNode = pmdNodesIter.nextNode();
							if (pmdLocaleNode.hasProperty(KEYINGREDIENTS)) {
								Set<String> keyIngredientSet = new HashSet<String>();
								Property keyIngredientProperty = pmdLocaleNode.getProperty(KEYINGREDIENTS);
								if (!keyIngredientProperty.isMultiple()) {
									Value val = keyIngredientProperty.getValue();
									if (val.getString().contains(TAGS)) {
										String[] keyIngredients = getTagTitle(val.getString(), locale);
										for (String ingredient : keyIngredients) {
											if(ingredient!=null)
											keyIngredientSet.add(ingredient);
										}
									}
								} else {
									Value[] values = keyIngredientProperty.getValues();
									for (int i = 0; i < values.length; i++) {
										if (values[i].getString().contains(TAGS)) {
											String[] keyIngredients = getTagTitle(values[i].getString(), locale);
											for (String ingredient : keyIngredients) {
												if(ingredient!=null)
												keyIngredientSet.add(ingredient);
											}
										}
									}
								}
								/*keyIngredientSet contains unique list of ingredients for a product.
								 * keyIngredientSet is converted to List uniqueProdIngredienttList
								 */
								if(keyIngredientSet!=null && keyIngredientSet.size()>0){
									List<String> uniqueProdIngredienttList = new ArrayList<String>(keyIngredientSet);
									tagList.addAll(uniqueProdIngredienttList);
								}
							}

						}
					}
				}
			}
		} catch (Exception exception) {
			throw new RepositoryException(exception);
		} finally {
			if (session != null)
				session.logout();

		}
		logger.info("Exit ProductsIngredientsServiceImpl.getAllTagsList()");
		return tagList;
	}

	/**
	 * Gets the tag title.
	 *
	 * @param keyIngredients
	 *            the key ingredients
	 * @param locale
	 *            the locale
	 * @return the tag title
	 * @throws JSONException
	 *             the JSON exception
	 */
	private String[] getTagTitle(String keyIngredients, Locale locale) throws JSONException {
		String[] tagTitle = null;
		if (keyIngredients != null) {
			JSONObject tagsObj = new JSONObject(keyIngredients);
			JSONArray tagsArray = tagsObj.getJSONArray(TAGS);
			tagTitle = new String[tagsArray.length()];
			for (int t = 0; t < tagsArray.length(); t++) {
				Tag tagValue = tagManager.resolve((String) tagsArray.get(t));
				if (null != tagValue)
					tagTitle[t] = tagValue.getTitle(locale);
			}
		}
		return tagTitle;
	}
}

package com.dtc.commerce.productdetail.sightly;

import java.util.Locale;

import org.apache.sling.api.resource.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;
import com.dtc.core.utils.LocaleUtil;

public class FeaturedProductsUsePojo extends WCMUsePojo{
	
	public FeaturedProductsUsePojo() {
		
	}

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(FeaturedProductsUsePojo.class);
	
	public String topSellersPath;
	
	@Override
	public void activate() throws Exception {
		LOGGER.info("Entering FeaturedProductsUsePojo:activate");

			String pagePath = getCurrentPage().getPath();
			
			Locale locale = LocaleUtil.getLocaleFromPath(pagePath);
			String market = locale.getCountry();
			
			if(!market.isEmpty()){
				Resource topSellersResource = getResourceResolver().getResource("/content/dtc/americas/"+ market +"/jcr:content/par/toptensellers");
				if(null != topSellersResource){
					topSellersPath = topSellersResource.getPath();
				}
			}
			LOGGER.info("Entering FeaturedProductsUsePojo:activate");
	}
	
	public String getTopSellersPath() {
		return topSellersPath;
	}
}

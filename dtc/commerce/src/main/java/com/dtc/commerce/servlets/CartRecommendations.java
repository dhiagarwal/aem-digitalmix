/**
 * Change History:
 * Revision     Date         Dev         Comments
 * ------------------------------------------------------------------------------------------------
 * 1.0          08.19.2016   Deloitte Digital   MEC-1517 - Display cross-sell and up-sell products on "Cart" page
 */

package com.dtc.commerce.servlets;

import java.io.IOException;
import java.rmi.ServerException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.ValueFormatException;

import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.Rendition;
import com.day.cq.wcm.api.LanguageManager;
import com.dtc.commerce.productdetail.common.ProductDetailConstant;
import com.dtc.commerce.productdetail.model.ProductDetail;
import com.dtc.commerce.productdetail.service.ProductDetailService;
import com.dtc.commerce.productdetail.service.ProductsRenditionService;
import com.dtc.core.services.PageLocaleService;
import com.dtc.core.utils.LocaleUtil;
/**
 * The Class CartRecommendations.
 * 
 * @author Deloitte Digital
 */
@SlingServlet(paths = "/bin/dtc/cartRecommendations", methods = "GET", metatype = true)
public class CartRecommendations extends SlingSafeMethodsServlet {
	
	public CartRecommendations() {
		
	}

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(CartRecommendations.class);

	/** The product properties. */
	private List<ProductDetail> productProperties;

	/** The Constant PATH. */
	private static final String PATH ="path";
	
	/** The Constant SKUID. */
	private static final String SKUID = "skuId";
	
	/** The Constant PRODUCT. */
	private static final String PRODUCT = "product";

	/** The service. */
	@Reference
	ProductDetailService service;
	
	/** The service. */
	@Reference
	PageLocaleService localeService;
	
	/** The service. */
	@Reference
	LanguageManager languageManager;
	
	/** The service. */
	@Reference
	ProductsRenditionService productsRenditionService;
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServerException, IOException {
		LOGGER.info("Entering: CartRecommendations :  doGet");
		try {
			
			String pagePath = request.getParameter(PATH);
			
			Locale locale = LocaleUtil.getLocaleFromPath(pagePath);
			
			List<String> skuIds =  Arrays.asList(request.getParameter(SKUID).split(","));
			List<String> cartRecommendationsSkuId = getRecommendedSkuid(skuIds, locale, request);
			Set<String> cartProductId = new HashSet<String>(skuIds);
			JSONArray cartRecommendations = new JSONArray();
			if (null != cartRecommendationsSkuId && cartRecommendationsSkuId.size()!=0) {
				Set<String> cartUniqueCrossSkuid = new HashSet<String>(cartRecommendationsSkuId);
			
				Iterator<String> iterator = cartUniqueCrossSkuid.iterator();
				while (iterator.hasNext()) {
					String remove = iterator.next();
					boolean match = false;
					for (String skuId : cartProductId) {
						if (skuId.equalsIgnoreCase(remove))
							match = true;
					}
					if (match)
						iterator.remove();
				}
				
					for (String skuIdentifier : cartUniqueCrossSkuid) {
	
						productProperties = service.getProductDetail(skuIdentifier, locale);
						for (ProductDetail data : productProperties) {
							JSONObject recommendation = new JSONObject();
							StringBuilder variantURL = new StringBuilder();
							if (data.getCommerceType().equalsIgnoreCase(PRODUCT)) {
								variantURL.append(ProductDetailConstant.DOT).append(skuIdentifier).append(ProductDetailConstant.HTML);
								recommendation.put("baseid", skuIdentifier);
							} else {
								variantURL.append(ProductDetailConstant.DOT).append(data.getBaseProductIdentifier()).append(ProductDetailConstant.DOT).append(skuIdentifier).append(ProductDetailConstant.HTML);
								recommendation.put("baseid", data.getBaseProductIdentifier().toUpperCase());
							}
							recommendation.put("skuid", data.getIdentifier());
							recommendation.put("img", getImageforSku(skuIdentifier, request));
							recommendation.put("name", data.getJcrTitle());
							recommendation.put("url", variantURL);
							cartRecommendations.put(recommendation);
						}
					}
			}
			LOGGER.info("Exiting: CartRecommendations :  doGet");
			response.setCharacterEncoding("UTF-8");
			response.setContentType("application/json");
			response.getWriter().println(cartRecommendations);
		}catch (JSONException e) {
			LOGGER.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}catch (RepositoryException re) {
			LOGGER.error(re.getClass() + ": " + re.getMessage() + ": " + re.getCause(), re);
		}
	}

	/**
	 * Gets the recommended skuid.
	 *
	 * @param skuIdentifier the sku identifier
	 * @param countryCode the country code
	 * @param request the request
	 * @return the recommended skuid
	 * @throws ValueFormatException the value format exception
	 * @throws PathNotFoundException the path not found exception
	 * @throws RepositoryException the repository exception
	 */
	private List<String> getRecommendedSkuid(List<String> skuIdentifier, Locale locale, SlingHttpServletRequest request) {
		LOGGER.info("Entering: CartRecommendations :  getRecommendedSkuid");
		List<String> recommendedSkuid = new ArrayList<>();
		try {
			List<String> crossSell = new ArrayList<>();

			ResourceResolver resourceResolver = request.getResourceResolver();

			for (int i = 0; i < skuIdentifier.size(); i++) {
				productProperties = service.getProductDetail(skuIdentifier.get(i), locale);
				for (ProductDetail data : productProperties) {
					crossSell = data.getCrossSell();
				}
				if (null != crossSell && crossSell.size()!=0) {
					for (String strCrossSell : crossSell) {
						
						Resource res = resourceResolver.getResource(strCrossSell);
						if(null != res && null != res.adaptTo(Node.class)){
						Node recoNode = res.adaptTo(Node.class);
						recommendedSkuid.add(recoNode.getName());
						}
					}
				}
			}
		} catch (RepositoryException e) {
			LOGGER.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}
		LOGGER.info("Exiting: CartRecommendations :  getRecommendedSkuid");
		return recommendedSkuid;
	}

	/**
	 * Gets the imagefor sku.
	 *
	 * @param sku the sku
	 * @param request the request
	 * @return the imagefor sku
	 * @throws RepositoryException the repository exception
	 * @throws JSONException the JSON exception
	 */
	private String getImageforSku(String sku, SlingHttpServletRequest request)
			throws RepositoryException, JSONException {
		LOGGER.info("Entering: cartRecommendations :  getImageforSku");
		ResourceResolver resourceResolver = request.getResourceResolver();
		String imageforSku = "";
		try {
			String imagesPath = ProductDetailConstant.DAM_BASE_PATH + sku;
			String cartRecommendationRendition = productsRenditionService.getCartRecommendationRendition();
			
			Resource res = resourceResolver.getResource(imagesPath);
			if(null != res && null != res.adaptTo(Node.class)){
				Node imagesNodes = res.adaptTo(Node.class);
				NodeIterator nodeIterator = imagesNodes.getNodes();

				while (nodeIterator.hasNext()) {
					Node imageNode = nodeIterator.nextNode();
					if (imageNode.getPrimaryNodeType().getName().equals(ProductDetailConstant.DAM_ASSET) && null != resourceResolver.getResource(imageNode.getPath())){
						Rendition imageRenditions = resourceResolver.getResource(imageNode.getPath()).adaptTo(Asset.class).getRendition(cartRecommendationRendition);
		        		if(null !=  imageRenditions){
		        			imageforSku = imageRenditions.getPath();
		        			break;
		        		}
					}
				}
			}
		} catch (RepositoryException e) {
			LOGGER.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}
		LOGGER.info("Exiting: cartRecommendations :  getImageforSku");
		return imageforSku;
	}

}
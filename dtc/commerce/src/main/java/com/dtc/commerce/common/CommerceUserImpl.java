package com.dtc.commerce.common;

import com.dtc.commerce.api.CommerceUser;

/**
 * Commerce user implementation class.
 */
public class CommerceUserImpl implements CommerceUser
{

    private String username;
    private String firstName;
    private String lastName;
    
    @Override
    public final String getUsername()
    {
        return this.username;
    }

    /**
     * Sets the username.
     * 
     * @param username The username.
     */
    public final void setUsername(final String username)
    {
        this.username = username;
    }

    @Override
    public final String getFirstName()
    {
        return this.firstName;
    }

    /**
     * Sets the first name.
     * 
     * @param firstName The first name.
     */
    public final void setFirstName(final String firstName)
    {
        this.firstName = firstName;
    }
    
    @Override
    public final String getLastName()
    {
        return this.lastName;
    }

    /**
     * Sets the last name.
     * 
     * @param lastName The last name.
     */
    public final void setLastName(final String lastName)
    {
        this.lastName = lastName;
    }

}

/**
 * Change History:
 * Revision     Date         Dev         Comments
 * ------------------------------------------------------------------------------------------------
 * 1.0          12.08.2016   Deloitte Digital   
 */
package com.dtc.commerce.productdetail.model;

import java.util.List;

/**
 * The VariantDetail Model Class contains Getter,Setter for the properties of
 * the Variant Type and Value.
 *
 * @author Deloitte Digital
 */
public class VariantDetail {

	/**
	 * Instantiates VariantDetail object.
	 */
	public VariantDetail() {
	}
	
	/** The identifier. */
	private String identifier;
	
	/** The mecVariantType. */
	private String mecVariantType;

	/** The mecVariantValue. */
	private String mecVariantValue;
	
	/** The variantImagePath. */
	private String variantImagePath;
	
	/** The variantImageShadePath. */
	private String variantImageShadePath;
	
	/** The product name. */
	private String productName;
	
	/** String to store image of Supplements. */
	private String suplementImg;
	
	/** The key Ingredients. */
	private List<KeyIngredients> keyIng;
	
	/** The servings. */
	private List<String> servings;
	
	/** The special Ingredients. */
	private List<SpecialIngredients> specialIng;
	
	/** The other Ingredients. */
	private String otherIng;
	
	/** The  nutrition Note. */
	private String nutritionNote;
	
	/** The vitamins. */
	private String vitamins;
	
	/** The json ing. */
	private String jsonIng;

	/** The purchasable. */
	private Boolean purchasable;
	
	/** The size. */
	private String size;
	
	/** The jcr title. */
	private String jcrTitle;
	
	/** The swatch color. */
	private String swatchColor;
	
	/**  The cross sell list. */
	private List<String> crossSell;
	
	/**  The up sell product. */
	private String upSell;
	
	/** The commerce type. */
	private String commerceType;

	/**
	 * Gets the mec variant type.
	 *
	 * @return the mecVariantType
	 */
	public String getMecVariantType() {
		return mecVariantType;
	}

	/**
	 * Sets the mec variant type.
	 *
	 * @param mecVariantType
	 */
	public void setMecVariantType(String mecVariantType) {
		this.mecVariantType = mecVariantType;
	}

	/**
	 * Gets the mec variant value.
	 *
	 * @return the mecVariantValue
	 */
	public String getMecVariantValue() {
		return mecVariantValue;
	}

	/**
	 * Sets the mec variant value.
	 *
	 * @param mecVariantValue
	 */
	public void setMecVariantValue(String mecVariantValue) {
		this.mecVariantValue = mecVariantValue;
	}

	/**
	 * Gets the identifier.
	 *
	 * @return the identifier
	 */
	public String getIdentifier() {
		return identifier;
	}

	/**
	 * Sets the identifier.
	 *
	 * @param identifier the identifier to set
	 */
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	/**
	 * Gets the variant image path.
	 *
	 * @return the variantImagePath
	 */
	public String getVariantImagePath() {
		return variantImagePath;
	}

	/**
	 * Sets the variant image path.
	 *
	 * @param variantImagePath the variantImagePath to set
	 */
	public void setVariantImagePath(String variantImagePath) {
		this.variantImagePath = variantImagePath;
	}

	/**
	 * Gets the product name.
	 *
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}

	/**
	 * Sets the product name.
	 *
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * Gets the variant image shade path.
	 *
	 * @return the variantImageShadePath
	 */
	public String getVariantImageShadePath() {
		return variantImageShadePath;
	}

	/**
	 * Sets the variant image shade path.
	 *
	 * @param variantImageShadePath the variantImageShadePath to set
	 */
	public void setVariantImageShadePath(String variantImageShadePath) {
		this.variantImageShadePath = variantImageShadePath;
	}
	
	/**
	 * Gets the suplement img.
	 *
	 * @return the suplement img
	 */
	public String getSuplementImg() {
		return suplementImg;
	}

	/**
	 * Sets the suplement img.
	 *
	 * @param suplementImg the new suplement img
	 */
	public void setSuplementImg(String suplementImg) {
		this.suplementImg = suplementImg;
	}

	/**
	 * Gets the key ing.
	 *
	 * @return the key ing
	 */
	public List<KeyIngredients> getKeyIng() {
		return keyIng;
	}

	/**
	 * Sets the key ing.
	 *
	 * @param list the new key ing
	 */
	public void setKeyIng(List<KeyIngredients> list) {
		this.keyIng = list;
	}

	/**
	 * Gets the servings.
	 *
	 * @return the servings
	 */
	public List<String> getServings() {
		return servings;
	}

	/**
	 * Sets the servings.
	 *
	 * @param servings the new servings
	 */
	public void setServings(List<String> servings) {
		this.servings = servings;
	}

	/**
	 * Gets the special ing.
	 *
	 * @return the special ing
	 */
	public List<SpecialIngredients> getSpecialIng() {
		return specialIng;
	}

	/**
	 * Sets the special ing.
	 *
	 * @param specialIng the new special ing
	 */
	public void setSpecialIng(List<SpecialIngredients> specialIng) {
		this.specialIng = specialIng;
	}

	/**
	 * Gets the other ing.
	 *
	 * @return the other ing
	 */
	public String getOtherIng() {
		return otherIng;
	}

	/**
	 * Sets the other ing.
	 *
	 * @param otherIng the new other ing
	 */
	public void setOtherIng(String otherIng) {
		this.otherIng = otherIng;
	}

	/**
	 * Gets the nutrition note.
	 *
	 * @return the nutrition note
	 */
	public String getNutritionNote() {
		return nutritionNote;
	}

	/**
	 * Sets the nutrition note.
	 *
	 * @param nutritionNote the new nutrition note
	 */
	public void setNutritionNote(String nutritionNote) {
		this.nutritionNote = nutritionNote;
	}

	/**
	 * Gets the vitamins.
	 *
	 * @return the vitamins
	 */
	public String getVitamins() {
		return vitamins;
	}

	/**
	 * Sets the vitamins.
	 *
	 * @param vitamins the new vitamins
	 */
	public void setVitamins(String vitamins) {
		this.vitamins = vitamins;
	}
	
	/**
	 * Gets the json ing.
	 *
	 * @return the json ing
	 */
	public String getJsonIng() {
		return jsonIng;
	}

	/**
	 * Sets the json ing.
	 *
	 * @param jsonIng the new json ing
	 */
	public void setJsonIng(String jsonIng) {
		this.jsonIng = jsonIng;
	}

	/**
	 * Gets the purchasable.
	 *
	 * @return the purchasable
	 */
	public Boolean getPurchasable() {
		return purchasable;
	}

	/**
	 * Sets the purchasable.
	 *
	 * @param purchasable the purchasable to set
	 */
	public void setPurchasable(Boolean purchasable) {
		this.purchasable = purchasable;
	}
	
	/**
	 * Gets the size.
	 *
	 * @return the size
	 */
	public String getSize() {
		return size;
	}

	/**
	 * Sets the size.
	 *
	 * @param size the new size
	 */
	public void setSize(String size) {
		this.size = size;
	}
	
	/**
	 * Gets the jcr title.
	 *
	 * @return the jcr title
	 */
	public String getJcrTitle() {
		return jcrTitle;
	}

	/**
	 * Sets the jcr title.
	 *
	 * @param jcrTitle the new jcr title
	 */
	public void setJcrTitle(String jcrTitle) {
		this.jcrTitle = jcrTitle;
	}

	/**
	 * Gets the swatchColor.
	 *
	 * @return the swatchColor
	 */
	public String getSwatchColor() {
		return swatchColor;
	}
	
	/**
	 * Sets the swatchColor.
	 *
	 * @param swatchColor the new swatch color
	 */
	public void setSwatchColor(String swatchColor) {
		this.swatchColor = swatchColor;
	}

	/**
	 * Gets the cross sell list.
	 *
	 * @return the cross sell list.
	 */
	public List<String> getCrossSell() {
		return crossSell;
	}

	/**
	 * Sets the cross sell list.
	 *
	 * @param crossSell the new cross sell
	 */
	public void setCrossSell(List<String> crossSell) {
		this.crossSell = crossSell;
	}

	/**
	 * Gets the up sell.
	 *
	 * @return the up sell product.
	 */
	public String getUpSell() {
		return upSell;
	}
	
	/**
	 * Sets the up sell product.
	 *
	 * @param upSell the new up sell
	 */
	public void setUpSell(String upSell) {
		this.upSell = upSell;
	}

	/**
	 * Gets the commerce type.
	 *
	 * @return the commerce type
	 */
	public String getCommerceType() {
		return commerceType;
	}

	/**
	 * Sets the commerce type.
	 *
	 * @param commerceType the new commerce type
	 */
	public void setCommerceType(String commerceType) {
		this.commerceType = commerceType;
	}
	
}

/**
 * Change History:
 * Revision     Date         Dev         Comments
 * ------------------------------------------------------------------------------------------------
 * 1.0          08.23.2016   Deloitte Digital   
 */
package com.dtc.commerce.productdetail.common;


/**
 * The Class ProductDetailConstant contains the constants for ProductDetail.
 *
 * @author Deloitte Digital
 */

public class ProductDetailConstant {

	/** The Constant DTC_BASE_PATH. */
	public static final String COMMERCE_BASE_PATH = "/etc/commerce/products/dtc/";

	public static final String PMD_BASE_PATH = "/content/pmd/";

	public static final String PRODUCT_TEMPLATE_PATH = "/apps/dtc/commerce/template/productMasterDetail";

	public static final String BASE_TEMPLATE_PATH = "/apps/dtc/commerce/template/baseProduct";

	public static final String BLANK_TEMPLATE_PATH = "/apps/dtc/core/templates/blank template";

	public static final String DEFAULT_VARIANT = "defaultVariant";

	/** The Constant Node PRODUCT_DETAIL. */
	public static final String PRODUCT_DETAIL_NODE = "productDetail";

	public static final String PDP_PRODUCT_TESTIMONIALS_NODE = "pdpProductTestimonials";

	public static final String PDP_PRODUCT_SUPPLEMENTS_NODE = "pdpProductSuppliments";

	public static final String PDP_PRODUCT_USAGE_NODE = "pdpProductUsage";

	public static final String PDP_CROSS_SELL_NODE = "pdpCrossSell";

	public static final String PDP_UP_SELL_NODE = "pdpUpSell";

	public static final String PRODUCT_RESOURCES_NODE = "productResources";

	/** The Constant NODE_TYPE_NT_UNSTRUCTURED. */
	public static final String NODE_TYPE_NT_UNSTRUCTURED = "nt:unstructured";

	/** The Constant PROPERTY_IDENTIFIER. */
	public static final String PROPERTY_IDENTIFIER = "identifier";

	/** The Constant PROPERTY_PRODUCT_NAME. */
	public static final String PROPERTY_PRODUCT_NAME = "productName";

	/** The Constant PROPERTY_DESCRIPTION. */
	public static final String PROPERTY_DESCRIPTION = "description";

	/** The Constant PROPERTY_BENEFITS. */
	public static final String PROPERTY_BENEFITS = "benefits";
	
	/** The Constant PROPERTY_MARKETING_MATERIAL_ONE. */
	public static final String PROPERTY_MARKETING_MATERIAL_ONE = "marketingMaterial1";
	
	/** The Constant PROPERTY_MARKETING_MATERIAL_ONE_USER_TYPE. */
	public static final String PROPERTY_MARKETING_MATERIAL_ONE_USER_TYPE = "mm1UserType";
	
	/** The Constant PROPERTY_MARKETING_MATERIAL_ONE_IMAGE_SRC. */
	public static final String PROPERTY_MARKETING_MATERIAL_ONE_IMAGE_SRC = "mm1ImageSrc";
	
	/** The Constant PROPERTY_MARKETING_MATERIAL_TWO. */
	public static final String PROPERTY_MARKETING_MATERIAL_TWO = "marketingMaterial2";
	
	/** The Constant PROPERTY_MARKETING_MATERIAL_TWO_USER_TYPE. */
	public static final String PROPERTY_MARKETING_MATERIAL_TWO_USER_TYPE = "mm2UserType";
	
	/** The Constant PROPERTY_MARKETING_MATERIAL_TWO_IMAGE_SRC. */
	public static final String PROPERTY_MARKETING_MATERIAL_TWO_IMAGE_SRC = "mm2ImageSrc";

	/** The Constant PROPERTY_MARKETING_MATERIAL_THREE. */
	public static final String PROPERTY_MARKETING_MATERIAL_THREE = "marketingMaterial3";
	
	/** The Constant PROPERTY_MARKETING_MATERIAL_THREE_USER_TYPE. */
	public static final String PROPERTY_MARKETING_MATERIAL_THREE_USER_TYPE = "mm3UserType";
	
	/** The Constant PROPERTY_MARKETING_MATERIAL_THREE_IMAGE_SRC. */
	public static final String PROPERTY_MARKETING_MATERIAL_THREE_IMAGE_SRC = "mm3ImageSrc";
	
	/** The Constant PROPERTY_PRODUCT_INFORMATION_ONE. */
	public static final String PROPERTY_PRODUCT_INFORMATION_ONE = "productInformation1";

	/** The Constant PROPERTY_PRODUCT_INFORMATION_ONE_USER_TYPE. */
	public static final String PROPERTY_PRODUCT_INFORMATION_ONE_USER_TYPE = "pi1UserType";
	
	/** The Constant PROPERTY_PRODUCT_INFORMATION_ONE_IMAGE_SRC. */
	public static final String PROPERTY_PRODUCT_INFORMATION_ONE_IMAGE_SRC = "pi1ImageSrc";
	
	/** The Constant PROPERTY_PRODUCT_INFORMATION_TWO. */
	public static final String PROPERTY_PRODUCT_INFORMATION_TWO = "productInformation2";

	/** The Constant PROPERTY_PRODUCT_INFORMATION_TWO_USER_TYPE. */
	public static final String PROPERTY_PRODUCT_INFORMATION_TWO_USER_TYPE = "pi2UserType";
	
	/** The Constant PROPERTY_PRODUCT_INFORMATION_TWO_IMAGE_SRC. */
	public static final String PROPERTY_PRODUCT_INFORMATION_TWO_IMAGE_SRC = "pi2ImageSrc";
	
	/** The Constant PROPERTY_PRODUCT_INFORMATION_THREE. */
	public static final String PROPERTY_PRODUCT_INFORMATION_THREE = "productInformation3";

	/** The Constant PROPERTY_PRODUCT_INFORMATION_THREE_USER_TYPE. */
	public static final String PROPERTY_PRODUCT_INFORMATION_THREE_USER_TYPE = "pi3UserType";
	
	/** The Constant PROPERTY_PRODUCT_INFORMATION_THREE_IMAGE_SRC. */
	public static final String PROPERTY_PRODUCT_INFORMATION_THREE_IMAGE_SRC = "pi3ImageSrc";
	
	/** The Constant PROPERTY_OTHER_SOURCE_ONE. */
	public static final String PROPERTY_OTHER_SOURCE_ONE = "otherSource1";
	
	/** The Constant PROPERTY_OTHER_SOURCE_ONE_USER_TYPE. */
	public static final String PROPERTY_OTHER_SOURCE_ONE_USER_TYPE = "os1UserType";
	
	/** The Constant PROPERTY_OTHER_SOURCE_ONE_IMAGE_SRC. */
	public static final String PROPERTY_OTHER_SOURCE_ONE_IMAGE_SRC = "os1ImageSrc";

	/** The Constant PROPERTY_OTHER_SOURCE_TWO. */
	public static final String PROPERTY_OTHER_SOURCE_TWO = "otherSource2";

	/** The Constant PROPERTY_OTHER_SOURCE_TWO_USER_TYPE. */
	public static final String PROPERTY_OTHER_SOURCE_TWO_USER_TYPE = "os2UserType";
	
	/** The Constant PROPERTY_OTHER_SOURCE_TWO_IMAGE_SRC. */
	public static final String PROPERTY_OTHER_SOURCE_TWO_IMAGE_SRC = "os2ImageSrc";

	/** The Constant PROPERTY_OTHER_SOURCE_THREE. */
	public static final String PROPERTY_OTHER_SOURCE_THREE = "otherSource3";

	/** The Constant PROPERTY_OTHER_SOURCE_THREE_USER_TYPE. */
	public static final String PROPERTY_OTHER_SOURCE_THREE_USER_TYPE = "os3UserType";
	
	/** The Constant PROPERTY_OTHER_SOURCE_THREE_IMAGE_SRC. */
	public static final String PROPERTY_OTHER_SOURCE_THREE_IMAGE_SRC = "os3ImageSrc";

	/** The Constant PROPERTY_URL. */
	public static final String PROPERTY_URL = "url";

	/** The Constant PROPERTY_COUNTRY_CODE. */
	public static final String PROPERTY_COUNTRY_CODE = "countryCode";

	/** The Constant PROPERTY_JCR_TITLE. */
	public static final String PROPERTY_JCR_TITLE = "jcr:title";

	/** The Constant PROPERTY_PURCHASABLE. */
	public static final String PROPERTY_PURCHASABLE = "purchasable";

	/** The Constant PROPERTY_COMMERCE_TYPE. */
	public static final String PROPERTY_COMMERCE_TYPE = "cq:commerceType";

	/** The Constant PROPERTY_RATINGS. */
	public static final String PROPERTY_RATINGS = "totalRatings";

	/** The Constant PROPERTY_SLING_RESOURCE_TYPE. */
	public static final String PROPERTY_SLING_RESOURCE_TYPE = "sling:resourceType";

	/** The Constant PROPERTY_VARIANT_VALUE. */
	public static final String PROPERTY_VARIANT_VALUE = "mecVariantValue";

	/** The Constant PROPERTY_LANGUAGE. */
	public static final String PROPERTY_LANGUAGE = "language";
	
	/** The Constant PROPERTY_MEC_VARIANT_TYPE. */
	public static final String PROPERTY_MEC_VARIANT_TYPE = "mecVariantType";
	
	/** The Constant PROPERTY_MEC_VARIANT_VALUE. */
	public static final String PROPERTY_MEC_VARIANT_VALUE = "mecVariantValue";

	/** The Constant PROPERTY_PRODUCT_VARIANT. */
	public static final String PROPERTY_PRODUCT_VARIANT = "productVariant";

	/** The Constant PROPERTY_DISTRIBUTION_CHANNEL. */
	public static final String PROPERTY_DISTRIBUTION_CHANNEL = "distributionChannel";

	/** The Constant PROPERTY_VARIANT_TYPE. */
	public static final String PROPERTY_VARIANT_TYPE = "productVariant";

	/** The Constant PROPERTY_PRODUCT_VARIANT_AXES. */
	public static final String PROPERTY_PRODUCT_VARIANT_AXES = "cq:productVariantAxes";
	
	/** The Constant PRODUCT_SWATCH_COLOR. */
	public static final String PRODUCT_SWATCH_COLOR = "swatchColor";

	/** The Constant PROPERTY_CROSS_SELL. */
	public static final String PROPERTY_CROSS_SELL = "crossSell";

	/** The Constant PROPERTY_UP_SELL. */
	public static final String PROPERTY_UP_SELL = "upSell";

	/** The Constant DAM_BASE_PATH. */
	public static final String DAM_BASE_PATH = "/content/dam/dtc/products/";

	/** The Constant IMAGES_LARGE. */
	public static final String IMAGES_LARGE = "large";

	/** The Constant IMAGES_SMALL. */
	public static final String IMAGES_SMALL = "small";

	/** The Constant PROPERTY_JCR_CONTENT. */
	public static final String PROPERTY_JCR_CONTENT = "jcr:content";

	/** The Constant PATH. */
	public static final String PATH = "path";

	/** The Constant TYPE. */
	public static final String TYPE = "type";

	/** The Constant PROPERTY. */
	public static final String PROPERTY = "property";

	/** The Constant PROPERTY_VALUE. */
	public static final String PROPERTY_VALUE = "property.value";

	/** The Constant IMAGE_PATH. */
	public static final String IMAGE_PATH = "imagePath";

	/** The Constant PRODUCT_SKU. */
	public static final String PRODUCT_SKU = "productsku";

	/** The Constant PRODUCT_ID_LABEL. */
	public static final String PRODUCT_ID_LABEL = "productidlabel";

	/** The Constant SIZE_LABEL. */
	public static final String SIZE_LABEL = "sizelabel";

	/** The Constant QUANTITY_LABEL. */
	public static final String QUANTITY_LABEL = "quantitylabel";

	/** The Constant BUTTON_LABEL. */
	public static final String BUTTON_LABEL = "buttonlabel";

	/** The Constant FAV_LABEL. */
	public static final String FAV_LABEL = "favlabel";

	/** The Constant PSV_LABEL. */
	public static final String PSV_LABEL = "psvlabel";
	
	/** The Constant PSV_LABEL. */
	public static final String RECURRING_LABEL = "recurringlabel";
	
	/** The Constant Product DESCRIPTION_LABEL. */
	public static final String DESCRIPTION_LABEL = "descriptionlabel";

	
	/** The Constant Product RESOURCES_LABEL. */
	public static final String RESOURCES_LABEL = "resourceslabel";

	/** The Constant Product MARKETING_MATERIAL_LABEL. */
	public static final String MARKETING_MATERIAL_LABEL = "marketingmateriallabel";

	/** The Constant Product PRODUCT_INFORMATION_LABEL. */
	public static final String PRODUCT_INFORMATION_LABEL = "productinformationlabel";

	/** The Constant Product OTHER_SOURCE_LABEL. */
	public static final String OTHER_SOURCE_LABEL = "othersourcelabel";
	
	/** The Constant DEFAULT_MESSAGE. */
	public static final String DEFAULT_MESSAGE = "defaultmessage";

	/** The Constant SLASH. */
	public static final String SLASH = "/";

	/** The Constant HTML. */
	public static final String HTML = ".html";

	/** The Constant DOT. */
	public static final String DOT = ".";

	/** The Constant SEPERATOR. */
	public static final String SEPERATOR = "_";

	/** The Constant UPDATEHEADER. */
	public static final String UPDATEHEADER = "updateHeader";

	/** The Constant COMPONENTHEADING. */
	public static final String COMPONENTHEADING = "componentHeading";

	/** The Constant DEFAULT. */
	public static final String DEFAULT = "default";

	/** The Constant PRODUCT_EXTN. */
	public static final String PRODUCT_EXTN = "products";

	/** The Constant LOCALE. */
	public static final String LOCALE = "en";

	/** The Constant MARKET. */
	public static final String MARKET = "US";

	/** The Constant PRODUCT_CATEGORY. */
	public static final String PRODUCT_CATEGORY = "productdetails";

	/** The Constant COMMERCE_PATH. */
	public static final String COMMERCE_PATH = "/content/dtc";

	/** The Constant UPSELL_KIT. */
	public static final String UPSELL_KIT = "upSellKit";

	/** The Constant HEADER_TEXT. */
	public static final String HEADER_TEXT = "headerText";

	/** The Constant DEFAULT_HEADER_TEXT. */
	public static final String DEFAULT_HEADER_TEXT = "Heading";

	/** The Constant TAGLINE_TEXT. */
	public static final String TAGLINE_TEXT = "tagLine";

	/** The Constant DEFAULT_TAGLINE_TEXT. */
	public static final String DEFAULT_TAGLINE_TEXT = "tag-line";

	/** The Constant BUTTON_TEXT. */
	public static final String BUTTON_TEXT = "buttonText";

	/** The Constant DEFAULT_BUTTON_LABEL. */
	public static final String DEFAULT_BUTTON_LABEL = "ButtonLabel";
	
	/** The Constant SHADE. */
	public static final String SHADE = "Shade";
	
	/** The Constant SIZE. */
	public static final String SIZE = "Size";
	
	public static final String SHADE_IMAGE_NAME = "shades.png";

	/** The Constant PROPERTY_NIGHT_USAGE. */
	public static final String PROPERTY_NIGHT_USAGE = "nightUsage";

	/** The Constant PROPERTY_MORNING_USAGE. */
	public static final String PROPERTY_MORNING_USAGE = "morningUsage";

	/** The Constant PROPERTY_GENERAL_USAGE. */
	public static final String PROPERTY_GENERAL_USAGE = "generalUsage";

	/** The Constant PROPERTY_USAGE_DESC. */
	public static final String PROPERTY_USAGE_DESC = "usageDesc";
	
	/** The Constant PROPERTY_USAGE_NOTE. */
	public static final String PROPERTY_USAGE_NOTE = "usageNote";
	
	/** The Constant PROPERTY_GRAPHIC_INFO. */
	public static final String PROPERTY_GRAPHIC_INFO = "graphicInfo";
	
	/** The Constant PROPERTY_USAGE_NOTE. */
	public static final String PROPERTY_GRAPHIC_DESC = "graphicDesc";
	
	/** The Constant PROPERTY_GRAPHIC_INFO. */
	public static final String PROPERTY_VIDEO_DUR = "videoDuration";
	
	/** The Constant GRAPHIC_TYPE_IMAGE. */
	public static final String GRAPHIC_TYPE_IMAGE = "image";
	
	/** The Constant GRAPHIC_TYPE_VIDEO. */
	public static final String GRAPHIC_TYPE_VIDEO = "video";
	
	
	
	/** The Constant SUPPLEMENT_IMG. */
	public static final String SUPPLEMENT_IMG = "suplementImg";
	
	/** The Constant KEY_INGREDIENTS. */
	public static final String KEY_INGREDIENTS = "keyIng";
	
	/** The Constant KEY_INGREDIENTS_TAGS. */
	public static final String KEY_INGREDIENTS_TAGS = "tags";
	
	/** The Constant KEY_INGREDIENTS_DESC. */
	public static final String KEY_INGREDIENTS_DESC = "desc";
	
	/** The Constant SERVINGS. */
	public static final String SERVINGS = "servings";
	
	/** The Constant OTHER_INGREDIENTS. */
	public static final String OTHER_INGREDIENTS = "otherIng";
	
	/** The Constant NUTRITION_NOTE. */
	public static final String NUTRITION_NOTE = "nutritionNote";
	
	/** The Constant SPECIAL_INGREDIENTS. */
	public static final String SPECIAL_INGREDIENTS = "specialIng";
	
	/** The Constant SPECIAL_INGREDIENTS_NAME. */
	public static final String SPECIAL_INGREDIENTS_NAME = "name";
	
	/** The Constant SPECIAL_INGREDIENTS_VALUE. */
	public static final String SPECIAL_INGREDIENTS_VALUE = "value";
	
	/** The Constant SPECIAL_INGREDIENTS_AMOUNT . */
	public static final String SPECIAL_INGREDIENTS_AMOUNT = "amount";
	
	/** The Constant VITAMINS. */
	public static final String VITAMINS = "vitamins";

	/** The Constant VARIANT. */
	public static final String VARIANT = "variant";
	
	/** The Constant PRODUCT. */
	public static final String PRODUCT = "product";
	
	/** The Constant CATEGORY_SKUID. */
	public static final String CATEGORY_SKUID = "skuid";
	
	/** The Constant HEADER_COMPONENT_PATH. */
	public static final String HEADER_COMPONENT_PATH = "dtc/commerce/components/content/header";
	
	/** The Constant HEADER_COMPONENT_NAME. */
	public static final String HEADER_COMPONENT_NAME = "header";
	
	/** The Constant MOBILE. */
	public static final String MOBILE = "Mobile";
	
	/** The Constant FOOTER_NAV. */
	public static final String FOOTER_NAV = "footerNav";
	
	/** The Constant NUTRITION. */
	public static final String NUTRITION = "nutrition";
	
	/** The Constant JCR_CONTENT. */
	public static final String JCR_CONTENT = "jcr:content";
	
	/** The Constant RENDITIONS. */
	public static final String RENDITIONS = "renditions";
	
	/** The Constant RENDITIONS. */
	public static final String DAM_ASSET = "dam:Asset";
	
	/** The Constant PROPERTY_SIZE. */
	public static final String PROPERTY_SIZE = "size";
		
	/** The Constant MP4_HD. */
	public static final String MP4_HD = "mp4-hd";
	
	/** The Constant JCR_CONTENT_METADATA. */
	public static final String JCR_CONTENT_METADATA = "/jcr:content/metadata";
	
	/** The Constant PRODUCT_TITLE. */
	public static final String PRODUCT_TITLE = "title";
	
	/** The Constant SKU. */
	public static final String SKU = "sku";
	
	/** The Constant NAME. */
	public static final String NAME = "name";

	/**
	 * Instantiates a new product detail constant.
	 */
	private ProductDetailConstant() {

	}
}

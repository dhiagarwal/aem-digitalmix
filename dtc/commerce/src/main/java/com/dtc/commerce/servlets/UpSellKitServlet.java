package com.dtc.commerce.servlets;

import java.io.IOException;
import java.util.Locale;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dtc.commerce.productdetail.service.ProductDetailService;
import com.dtc.core.utils.LocaleUtil;

/**
 * The Class UpsellServlet.
 */
@SlingServlet(paths = { "/bin/dtc/upSellKit" }, generateComponent = false, methods = "GET")
@Component(label = "DTC Products upSellKit", description = "DTC Products upSellKit", enabled = true, immediate = true, metatype = false)

public class UpSellKitServlet extends SlingAllMethodsServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The Constant PATH. */
	private static final String PATH = "path";

	/** The Constant SKUID. */
	private static final String SKUID = "skuId";

	/** The service. */
	@Reference
	ProductDetailService service;

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(UpSellKitServlet.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache.
	 * sling.api.SlingHttpServletRequest,
	 * org.apache.sling.api.SlingHttpServletResponse)
	 */
	@Override
	protected void doGet(SlingHttpServletRequest req, SlingHttpServletResponse resp) throws IOException {
		LOGGER.info("Entering: UpsellServlet :  doGet");
		try {
			String pagePath = req.getParameter(PATH);
			String skuid = req.getParameter(SKUID);
			
			Locale locale = LocaleUtil.getLocaleFromPath(pagePath);
			
			JSONObject upSellKitResponse = new JSONObject();
			
			upSellKitResponse = service.upSellKitInformation(skuid, locale);
			
			resp.setCharacterEncoding("UTF-8");
			resp.setContentType("application/json");
			
			resp.getWriter().println(upSellKitResponse.toString());

		} catch (Exception e) {
			LOGGER.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}

	}

}

package com.dtc.commerce.common;

import java.util.List;

import com.adobe.cq.commerce.api.CommerceFacet;
import com.adobe.cq.commerce.api.CommerceQuery;
import com.adobe.cq.commerce.api.CommerceSort;
import com.adobe.cq.commerce.api.PaginationInfo;
import com.dtc.commerce.api.CommerceResult;
import com.dtc.commerce.api.DtcItem;

/**
 * Default command result.
 */
public class DefaultCommerceResult implements CommerceResult
{
    private PaginationInfo paginationInfo;
    private List<CommerceSort> sorts;
    private List<CommerceFacet> facets;
    private List<DtcItem> products;
    private CommerceQuery originalQuery;
    private CommerceQuery actualQuery;

    @Override
    public final PaginationInfo getPaginationInfo()
    {
        return this.paginationInfo;
    }

    @Override
    public final List<CommerceSort> getSorts()
    {
        return this.sorts;
    }

    @Override
    public final List<DtcItem> getProducts()
    {
        return this.products;
    }

    @Override
    public final CommerceQuery getOriginalQuery()
    {
        return this.originalQuery;
    }

    @Override
    public final CommerceQuery getActualQuery()
    {
        return this.actualQuery;
    }

    @Override
    public final List<CommerceFacet> getFacets()
    {
        return this.facets;
    }

    /**
     * Sets the pagination info.
     * 
     * @param paginationInfo The pagination info.
     */
    public final void setPaginationInfo(final PaginationInfo paginationInfo)
    {
        this.paginationInfo = paginationInfo;
    }

    /**
     * Sets the sorts.
     * 
     * @param sorts The sorts.
     */
    public final void setSorts(final List<CommerceSort> sorts)
    {
        this.sorts = sorts;
    }

    /**
     * Sets the facets.
     * 
     * @param facets The facets.
     */
    public final void setFacets(final List<CommerceFacet> facets)
    {
        this.facets = facets;
    }

    /**
     * Sets the products.
     * 
     * @param products The products.
     */
    public final void setProducts(final List<DtcItem> products)
    {
        this.products = products;
    }

    /**
     * Sets the original query.
     * 
     * @param originalQuery The original query.
     */
    public final void setOriginalQuery(final CommerceQuery originalQuery)
    {
        this.originalQuery = originalQuery;
    }

    /**
     * Sets the actual query.
     * 
     * @param actualQuery The actual query.
     */
    public final void setActualQuery(final CommerceQuery actualQuery)
    {
        this.actualQuery = actualQuery;
    }

}

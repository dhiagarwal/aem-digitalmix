package com.dtc.commerce.productdetail.service;

import com.dtc.commerce.productdetail.common.ProductDetailConstant;
import com.dtc.commerce.utils.PMDUtil;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Session;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;


/**
 *
 * The ProductsServiceImpl implements methods of the ProductsService
 *
 */
@Component(label = "Products Service", metatype = false, description = "Service that queries the PIM and PMD", immediate = true)
@Service(ProductsService.class)
public class ProductsServiceImpl implements ProductsService {
    /**
     * The LOGGER.
     */
    private static final Logger log = LoggerFactory.getLogger(ProductsServiceImpl.class);

    /**
     * Inject a Sling ResourceResolverFactory.
     */
    @Reference
    private ResourceResolverFactory resolverFactory;

    /**
     * The resource resolver.
     */
    private ResourceResolver resourceResolver = null;

    @Override
    public String getAllPimSkus(String countryCode) {
		log.info("Entering ProductsServiceImpl:getAllPimSkus");
        JSONArray skus = new  JSONArray();

        String path = ProductDetailConstant.COMMERCE_BASE_PATH + countryCode;
        try {
            resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);
            log.debug("The path:" + path + " resourceResolver:" + resourceResolver);
            Session session = resourceResolver.adaptTo(Session.class);
            QueryManager queryManager = session.getWorkspace().getQueryManager();
            String sqlStatement = "SELECT * FROM [nt:unstructured] AS s WHERE ISDESCENDANTNODE([" + path + "])";
            Query query = queryManager.createQuery(sqlStatement, "JCR-SQL2");
            QueryResult result = query.execute();
            NodeIterator nodeIter = result.getNodes();

            while (nodeIter.hasNext()) {
                Node node = nodeIter.nextNode();
                log.debug("Node:" + node.getPath());
                if(node.hasProperty(ProductDetailConstant.PROPERTY_COMMERCE_TYPE)){
					JSONObject sku = null;
                	if((ProductDetailConstant.PRODUCT).equalsIgnoreCase(node.getProperty(ProductDetailConstant.PROPERTY_COMMERCE_TYPE).getString())){
                		sku = new JSONObject();
                		sku.put(ProductDetailConstant.NAME,node.getName());
                		sku.put(ProductDetailConstant.PROPERTY_URL,node.getName());
                	} else if((ProductDetailConstant.VARIANT).equalsIgnoreCase(node.getProperty(ProductDetailConstant.PROPERTY_COMMERCE_TYPE).getString())){
                		String baseSku = node.getParent().getName();
                		String variantSku = node.getName();
                		sku = new JSONObject();
                		sku.put(ProductDetailConstant.NAME,variantSku);
                		sku.put(ProductDetailConstant.PROPERTY_URL,baseSku + '.' + variantSku);
                	}
                	if(null != sku){
                		skus.put(sku);
                	}
                }
            }
        } catch (Exception e) {
			log.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
        }
		log.info("Exiting ProductsServiceImpl:getAllPimSkus");
        return skus.toString();
    }

    @Override
	public String getMissingPMDSkus(String countryCode, String language) {
		log.info("Entering ProductsServiceImpl:getMissingPMDSkus");
		JSONArray missing = new JSONArray();
		try {
			JSONArray pimSkus = new JSONArray(getAllPimSkus(countryCode));
			for (int i = 0; i < pimSkus.length(); i++) {
				JSONObject sku = null;
				JSONObject pimObject = new JSONObject(pimSkus.get(i).toString());
				if (pimObject.getString(ProductDetailConstant.NAME).length() >= 6 && resourceResolver
						.getResource(PMDUtil.getPmdPath(pimObject.getString(ProductDetailConstant.NAME)) + "/" + language) == null) {
					sku = new JSONObject();
					sku.put(ProductDetailConstant.NAME, pimObject.get(ProductDetailConstant.NAME));
					sku.put(ProductDetailConstant.PROPERTY_URL, pimObject.get(ProductDetailConstant.PROPERTY_URL));
				}
				if (null != sku) {
					missing.put(sku);
				}
			}
		} catch (JSONException e) {
			log.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}
		log.info("Exiting ProductsServiceImpl:getMissingPMDSkus");
		return missing.toString();
	}

    public String tokenizeSku(String sku) {
        return sku.substring(0, 2) + "/" + sku.substring(2, 4) + "/" + sku.substring(4, 6) + "/" + sku;
    }
}
package com.dtc.commerce.common;

import org.apache.sling.api.SlingHttpServletRequest;

import com.dtc.commerce.api.DtcCommerceContext;
import com.dtc.commerce.api.CommerceUser;

/**
 * Commerce context implementation class.
 */
public class DtcCommerceContextImpl implements DtcCommerceContext
{
    private static final String CATEGORY_FACET_IDENTIIFER = "categoryPath";

    private String baseStore;
    private String catalogId;
    private String language;
    private String baseProductContentPath;
    private String baseProductDataPath;
    private String categoryId;
    private CommerceUser user;
    private SlingHttpServletRequest request;
    private String cartIdentifierName;
        
    @Override
    public final String getBaseStore()
    {
        return baseStore;
    }

    /**
     * Sets the base store.
     * 
     * @param baseStore The base store.
     */
    public final void setBaseStore(final String baseStore)
    {
        this.baseStore = baseStore;
    }
    
    @Override
    public final String getCatalogId()
    {
        return catalogId;
    }

    /**
     * Sets the catalog identifier.
     * 
     * @param catalogId The catalog identifier.
     */
    public final void setCatalogId(final String catalogId)
    {
        this.catalogId = catalogId;
    }
    
    @Override
    public final String getLanguage()
    {
        return language;
    }
    
    /**
     * Sets the language.
     * 
     * @param language The language.
     */
    public final void setLanguage(final String language)
    {
        this.language = language;
    }

    @Override
    public final String getBaseProductContentPath()
    {
        return baseProductContentPath;
    }

    /**
     * Sets the base product content path.
     * 
     * @param baseProductContentPath The base product content path.
     */
    public final void setBaseProductContentPath(final String baseProductContentPath)
    {
        this.baseProductContentPath = baseProductContentPath;
    }
    
    @Override
    public final String getBaseProductDataPath()
    {
        return baseProductDataPath;
    }

    /**
     * Sets the base product data path.
     * 
     * @param baseProductDataPath The the base product data path.
     */
    public final void setBaseProductDataPath(final String baseProductDataPath)
    {
        this.baseProductDataPath = baseProductDataPath;
    }

    @Override
    public final String getCategoryId()
    {
        return categoryId;
    }

    /**
     * Sets the category identifier.
     * 
     * @param categoryId The category identifier.
     */
    public final void setCategoryId(final String categoryId)
    {
        this.categoryId = categoryId;
    }

    @Override
    public final CommerceUser getUser()
    {
        return user;
    }

    /**
     * Sets the commerce user.
     * 
     * @param user The commerce user.
     */
    public final void setUser(final CommerceUser user)
    {
        this.user = user;
    }

    @Override
    public final SlingHttpServletRequest getRequest()
    {
        return request;
    }

    /**
     * Sets the Sling HTTP servlet request.
     * 
     * @param request The Sling HTTP servlet request.
     */
    public final void setRequest(final SlingHttpServletRequest request)
    {
        this.request = request;
    }

    @Override
    public final String getCategoryFacetIdentifier()
    {
        return CATEGORY_FACET_IDENTIIFER;
    }
    
    @Override
    public final String getCartIdentifierName()
    {
        return this.cartIdentifierName;
    }
    
    /**
     * Sets the cart identifier name.
     * 
     * @param cartIdentifierName The cart identifier name.
     */
    public final void setCartIdentifierName(final String cartIdentifierName)
    {
        this.cartIdentifierName = cartIdentifierName;
    }

}

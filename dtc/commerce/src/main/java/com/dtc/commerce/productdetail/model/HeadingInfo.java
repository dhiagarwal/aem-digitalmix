package com.dtc.commerce.productdetail.model;

import java.util.List;

/**
 * The Class HeadingInfo.
 */
public class HeadingInfo {
	
	/**
	 * Instantiates HeadingInfo object.
	 */
	public HeadingInfo() {
	}

	/** The heading name. */
	private String headingName;

	/** The uncategorized links. */
	private List<Link> uncategorizedLinks;

	/** The uncategorized link pairs. */
	private List<LinkPair> uncategorizedLinkPairs;

	/** The subcategory 1 links. */
	private List<Link> subcategory1Links;

	/** The subcategory 2 links. */
	private List<Link> subcategory2Links;

	/** The subcategory 3 links. */
	private List<Link> subcategory3Links;
	
	/** The uncategorized name. */
	private String uncategorizedName;

	/** The subcategory 1 name. */
	private String subcategory1Name;

	/** The subcategory 2 name. */
	private String subcategory2Name;

	/** The subcategory 3 name. */
	private String subcategory3Name;

	/** The promotion. */
	private Promotion promotion;

	/**
	 * Gets the uncategorized link pairs.
	 *
	 * @return the uncategorized link pairs
	 */
	public List<LinkPair> getUncategorizedLinkPairs() {
		return uncategorizedLinkPairs;
	}

	/**
	 * Sets the uncategorized link pairs.
	 *
	 * @param uncategorizedLinkPairs
	 *            the new uncategorized link pairs
	 */
	public void setUncategorizedLinkPairs(List<LinkPair> uncategorizedLinkPairs) {
		this.uncategorizedLinkPairs = uncategorizedLinkPairs;
	}

	/**
	 * Gets the promotion.
	 *
	 * @return the promotion
	 */
	public Promotion getPromotion() {
		return promotion;
	}

	/**
	 * Sets the promotion.
	 *
	 * @param promotion
	 *            the new promotion
	 */
	public void setPromotion(Promotion promotion) {
		this.promotion = promotion;
	}

	/**
	 * Gets the heading name.
	 *
	 * @return the heading name
	 */
	public String getHeadingName() {
		return headingName;
	}

	/**
	 * Sets the heading name.
	 *
	 * @param headingName
	 *            the new heading name
	 */
	public void setHeadingName(String headingName) {
		this.headingName = headingName;
	}

	/**
	 * Gets the uncategorized links.
	 *
	 * @return the uncategorized links
	 */
	public List<Link> getUncategorizedLinks() {
		return uncategorizedLinks;
	}

	/**
	 * Sets the uncategorized links.
	 *
	 * @param uncategorizedLinks
	 *            the new uncategorized links
	 */
	public void setUncategorizedLinks(List<Link> uncategorizedLinks) {
		this.uncategorizedLinks = uncategorizedLinks;
	}

	/**
	 * Gets the subcategory 1 links.
	 *
	 * @return the subcategory 1 links
	 */
	public List<Link> getSubcategory1Links() {
		return subcategory1Links;
	}

	/**
	 * Sets the subcategory 1 links.
	 *
	 * @param subcategory1Links
	 *            the new subcategory 1 links
	 */
	public void setSubcategory1Links(List<Link> subcategory1Links) {
		this.subcategory1Links = subcategory1Links;
	}

	/**
	 * Gets the subcategory 2 links.
	 *
	 * @return the subcategory 2 links
	 */
	public List<Link> getSubcategory2Links() {
		return subcategory2Links;
	}

	/**
	 * Sets the subcategory 2 links.
	 *
	 * @param subcategory2Links
	 *            the new subcategory 2 links
	 */
	public void setSubcategory2Links(List<Link> subcategory2Links) {
		this.subcategory2Links = subcategory2Links;
	}

	/**
	 * Gets the subcategory 3 links.
	 *
	 * @return the subcategory 3 links
	 */
	public List<Link> getSubcategory3Links() {
		return subcategory3Links;
	}

	/**
	 * Sets the subcategory 3 links.
	 *
	 * @param subcategory3Links
	 *            the new subcategory 3 links
	 */
	public void setSubcategory3Links(List<Link> subcategory3Links) {
		this.subcategory3Links = subcategory3Links;
	}

	/**
	 * Gets the uncategorized name.
	 *
	 * @return the uncategorized name
	 */
	public String getUncategorizedName() {
		return uncategorizedName;
	}
	
	/**
	 * Sets the uncategorized name.
	 *
	 * @param uncategorized name
	 *            the new uncategorized name
	 */
	public void setUncategorizedName(String uncategorizedName) {
		this.uncategorizedName = uncategorizedName;
	}

	/**
	 * Gets the subcategory 1 name.
	 *
	 * @return the subcategory 1 name
	 */
	public String getSubcategory1Name() {
		return subcategory1Name;
	}

	/**
	 * Sets the subcategory 1 name.
	 *
	 * @param subcategory1Name
	 *            the new subcategory 1 name
	 */
	public void setSubcategory1Name(String subcategory1Name) {
		this.subcategory1Name = subcategory1Name;
	}

	/**
	 * Gets the subcategory 2 name.
	 *
	 * @return the subcategory 2 name
	 */
	public String getSubcategory2Name() {
		return subcategory2Name;
	}

	/**
	 * Sets the subcategory 2 name.
	 *
	 * @param subcategory2Name
	 *            the new subcategory 2 name
	 */
	public void setSubcategory2Name(String subcategory2Name) {
		this.subcategory2Name = subcategory2Name;
	}

	/**
	 * Gets the subcategory 3 name.
	 *
	 * @return the subcategory 3 name
	 */
	public String getSubcategory3Name() {
		return subcategory3Name;
	}

	/**
	 * Sets the subcategory 3 name.
	 *
	 * @param subcategory3Name
	 *            the new subcategory 3 name
	 */
	public void setSubcategory3Name(String subcategory3Name) {
		this.subcategory3Name = subcategory3Name;
	}

}

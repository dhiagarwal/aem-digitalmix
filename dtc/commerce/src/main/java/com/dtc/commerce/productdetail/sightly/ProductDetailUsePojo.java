/**
 * Change History:
 * Revision     Date         Dev         Comments
 * ------------------------------------------------------------------------------------------------
 * 1.0          08.23.2016   Deloitte Digital   MEC-3522 - Display "How to Use" content on PDP 
 */

package com.dtc.commerce.productdetail.sightly;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;
import com.dtc.commerce.productdetail.common.ProductDetailConstant;
import com.dtc.commerce.productdetail.model.KeyIngredients;
import com.dtc.commerce.productdetail.model.ProductDetail;
import com.dtc.commerce.productdetail.model.SpecialIngredients;
import com.dtc.commerce.productdetail.model.VariantDetail;
import com.dtc.commerce.productdetail.service.ProductDetailService;
import com.dtc.commerce.productdetail.service.ProductsRenditionService;
import com.dtc.core.services.EnvironmentConfig;
import com.dtc.core.utils.LocaleUtil;
import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.Rendition;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;

/**
 * The Class ProductDetailUsePojo for the Product Detail component.
 *
 * @author Deloitte Digital
 */
public class ProductDetailUsePojo extends WCMUsePojo {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(ProductDetailUsePojo.class);

	/** productName. */
	private String productName;

	/** productName. */
	private String categoryProductName;

	/** categoryProductDescription. */
	private String categoryProductDescription;

	/** categoryProductDescription. */
	private String categoryProductUrl;

	/** description. */
	private String description;

	/** benefits. */
	private List<String> benefits;

	/** marketingMaterials. */
	private List<String> marketingMaterials;

	/** productInformation. */
	private List<String> productInformation;

	/** otherSource. */
	private List<String> otherSource;

	/** The product size. */
	private String productSize;

	/** Below values fetch from page URL. */
	private String pagePath;

	/** The selectors. */
	private String[] selectors = null;

	/** The language. */
	private String language;

	/** The countryCode. */
	private String countryCode;

	/** The sku identifier. */
	private String skuIdentifier;

	/** The sku identifier. */
	private String categorySkuId;

	/** The images list. */
	private String imagesList;

	/** The show shade. */
	private boolean showShade = false;

	/** The Morning Usage List. */
	private List<String> morningUsage;

	/** The Night Usage List. */
	private List<String> nightUsage;

	/** The General Usage List. */
	private List<String> generalUsage;

	/** String to store image or video oentered by marketing business user. */
	private String graphicInfo;

	/** The usage note. */
	private String usageNote;

	/** The usage description. */
	private String usageDesc;

	/** The video duration. */
	private String videoDuration;

	/** The graphic description. */
	private String graphicDesc;

	/** The graphic type either image or video. */
	private String graphicType;

	/** The product properties. */
	private List<ProductDetail> productProperties;

	/** The product variant axes. */
	private List<String> productVariantAxes;

	/** The variant detail. */
	private List<VariantDetail> variantDetail;

	/** String to store image of Supplements. */
	private String suplementImg;

	/** The key Ingredients. */
	private List<KeyIngredients> keyIng;

	/** The servings. */
	private List<String> servings;

	/** The special Ingredients. */
	private List<SpecialIngredients> specialIng;

	/** The other Ingredients. */
	private String otherIng;

	/** The nutrition Note. */
	private String nutritionNote;

	/** The vitamins. */
	private String vitamins;

	/** The variantSelectors. */
	private String variantSelectors;

	/** The variantSelectors. */
	private Boolean purchasable;

	/** The property size. */
	private String propertySize;

	/** The productUsageVideoPath. */
	private String productUsageVideoPath;
	
	/** The showKeyIngredients. */
	private Boolean showKeyIngredients;

	@Override
	public void activate() throws Exception {
		LOGGER.info("Entering ProductDetailUsePojo:activate");
		try {
			ValueMap properties = getProperties();

			String pathValues = properties.get(ProductDetailConstant.CATEGORY_SKUID, StringUtils.EMPTY);

			pagePath = getCurrentPage().getPath();

			Locale locale = LocaleUtil.getLocaleFromPath(pagePath);

			ProductDetailService service = getSlingScriptHelper().getService(ProductDetailService.class);
			if (pathValues != "") {
				getCategoryStandardDetails(service, pathValues, locale);
			}
			selectors = getRequest().getRequestPathInfo().getSelectors();
			if (selectors.length > 0) {
				skuIdentifier = selectors[0];
				if (selectors.length > 1)
					variantSelectors = selectors[1];
			}

			productProperties = service.getProductDetail(skuIdentifier, locale);
			for (ProductDetail data : productProperties) {
				skuIdentifier = data.getIdentifier();
				productName = data.getJcrTitle();
				productSize = data.getSize();

				setCommonProperties(data);
				propertySize = data.getSize();
				
				purchasable = data.getPurchasable();
				productVariantAxes = data.getProductVariantAxes();
				variantDetail = data.getVariants();
				suplementImg = data.getSuplementImg();
				servings = data.getServings();
				specialIng = data.getSpecialIng();
				otherIng = data.getOtherIng();
				nutritionNote = data.getNutritionNote();
				vitamins = data.getVitamins();
				
				if(null != data.getKeyIng() && !(data.getKeyIng()).isEmpty())
				{
					keyIng = getKeyIngredientsTitle(data.getKeyIng(),locale);
					showKeyIngredientsDivIfDescrition(keyIng);
				}
				if (graphicInfo != null) {
					if (graphicInfo.contains("mp4")) {
						graphicType = ProductDetailConstant.GRAPHIC_TYPE_VIDEO;
						productUsageVideoPath = getUsageVideoPath(graphicInfo);
					} else {
						graphicType = ProductDetailConstant.GRAPHIC_TYPE_IMAGE;
					}
				}
				if (null != variantDetail && !(variantDetail.isEmpty()) && variantDetail.get(0).getMecVariantType() != null
						&& variantDetail.get(0).getMecVariantType().equalsIgnoreCase(ProductDetailConstant.SHADE)) {
					showShade = true;
				}
				if (null != variantDetail && !(variantDetail.isEmpty())) {
					setProductSupplements(locale);
				}
			}
			
			if(StringUtils.isNotEmpty(variantSelectors)){
				productProperties = service.getProductDetail(variantSelectors, locale);
				
				for (ProductDetail data : productProperties) {
					productName = data.getJcrTitle();
					productSize = data.getSize();
					propertySize = data.getSize();
					purchasable = data.getPurchasable();
					suplementImg = data.getSuplementImg();
					if (null != data.getKeyIng() && !(data.getKeyIng()).isEmpty()) {
						keyIng = getKeyIngredientsTitle(data.getKeyIng(), locale);
						showKeyIngredientsDivIfDescrition(keyIng);
					}
					servings = data.getServings();
					specialIng = data.getSpecialIng();
					otherIng = data.getOtherIng();
					nutritionNote = data.getNutritionNote();
					vitamins = data.getVitamins();
				}
			}
			
			if(StringUtils.isNotEmpty(variantSelectors)){
				imagesList = getCarouselImage(variantSelectors);
			} else {
				imagesList = getCarouselImage(skuIdentifier);
			}
			
		} catch (Exception e) {
			LOGGER.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}
		LOGGER.info("Exiting ProductDetailUsePojo:activate");
	}

	/**
	 * Show key tags ingredients div if descrition.
	 *
	 * @param keyIng the key ing
	 */
	private void showKeyIngredientsDivIfDescrition(List<KeyIngredients> keyIng) {
		LOGGER.info("Entering ProductDetailUsePojo:showKeyIngredientsDivIfDescrition");
		if(keyIng != null) {
			for (KeyIngredients keyIngredients : keyIng) {
				if(keyIngredients.getShowTags() == true) {
					showKeyIngredients = true;
					break;
				}
			}
		}
		LOGGER.info("Exiting ProductDetailUsePojo:showKeyIngredientsDivIfDescrition");		
	}

	/**
	 * Sets the product supplements.
	 *
	 * @param locale the new product supplements
	 * @throws JSONException the JSON exception
	 */
	private void setProductSupplements(Locale locale) throws JSONException {
		LOGGER.info("Entering ProductDetailUsePojo:setProductSupplements");
		for (int index = 0; index < variantDetail.size(); index++) {
			JSONObject supplements = new JSONObject();
			JSONArray sArr = new JSONArray();
			JSONArray kArr = new JSONArray();
			
			supplements.put("othIng", variantDetail.get(index).getOtherIng());
			supplements.put("servings", variantDetail.get(index).getServings());
			supplements.put("imgspl", variantDetail.get(index).getSuplementImg());
			supplements.put("note", variantDetail.get(index).getNutritionNote());
			supplements.put("vitamins", variantDetail.get(index).getVitamins());
			
			if (variantDetail.get(index).getSpecialIng() != null
					&& variantDetail.get(index).getSpecialIng().size() > 0) {
				JSONObject[] obj2 = new JSONObject[variantDetail.get(index).getSpecialIng().size()];

				for (int j = 0; j < variantDetail.get(index).getSpecialIng().size(); j++) {
					obj2[j] = new JSONObject();
					obj2[j].put("sIng", variantDetail.get(index).getSpecialIng().get(j).getIngredient());
					obj2[j].put("amount", variantDetail.get(index).getSpecialIng().get(j).getAmount());
					obj2[j].put("dVal", variantDetail.get(index).getSpecialIng().get(j).getDailyValue());
					sArr.put(obj2[j]);
				}
			}
			
			if (sArr != null && sArr.length() > 0)
				supplements.put("splIng", sArr);
			
			if (variantDetail.get(index).getKeyIng() != null
					&& variantDetail.get(index).getKeyIng().size() > 0) {
				List<KeyIngredients> kList = getKeyIngredientsTitle(variantDetail.get(index).getKeyIng(), locale);
				if (kList != null && kList.size() > 0) {
					JSONObject[] obj3 = new JSONObject[kList.size()];

					for (int k = 0; k < kList.size(); k++) {
						obj3[k] = new JSONObject();
						obj3[k].put("tags", kList.get(k).getTags());
						obj3[k].put("description", kList.get(k).getDescription());
						kArr.put(obj3[k]);
					}
				}
			}
			
			if (kArr != null && kArr.length() > 0)
				supplements.put("keyIng", kArr);

			variantDetail.get(index).setJsonIng(supplements.toString());

			String variantImagePath = getCarouselImage(variantDetail.get(index).getIdentifier());
			if (!variantImagePath.trim().isEmpty() && variantImagePath != null) {
				variantDetail.get(index).setVariantImagePath(variantImagePath);
			}
			
			if (variantDetail.get(index).getSize().isEmpty()) {
				variantDetail.get(index).setSize(propertySize);
			}
		}
		LOGGER.info("Exiting ProductDetailUsePojo:setProductSupplements");
	}
	

	/**
	 * Gets the key ingredients title from the tags.
	 *
	 * @param keyIngList the key ingredients list
	 * @param locale the locale
	 * @return the key ingredients title
	 */
	private List<KeyIngredients> getKeyIngredientsTitle(List<KeyIngredients> keyIngList, Locale locale) {
		LOGGER.info("Entering ProductDetailUsePojo:getKeyIngredientsTitle");

		List<KeyIngredients> keyIngredients = new ArrayList<>();
		TagManager tagManager = getResourceResolver().adaptTo(TagManager.class);
		try {	
			for (int index = 0; index < keyIngList.size(); index++) {
				String[] title = null;
				KeyIngredients ingredients = new KeyIngredients();
				String[] tagsArray = keyIngList.get(index).getTitle();
				if (tagsArray != null) {
					title = new String[tagsArray.length];
					for (int j = 0; j < tagsArray.length; j++) {
						Tag tag = tagManager.resolve(tagsArray[j]);
						title[j] = tag.getTitle(locale);
					}
				}
				if (title != null && title.length > 1) {
					ingredients.setTags(StringUtils.join(title, ", "));
				} else {
					ingredients.setTags(StringUtils.join(title, ""));
				}
				ingredients.setTitle(title);
				ingredients.setDescription(keyIngList.get(index).getDescription());
				if(!ingredients.getDescription().isEmpty()) {
					ingredients.setShowTags(true);
				}
				else {
					ingredients.setShowTags(false);
				}
				keyIngredients.add(ingredients);
			}
		} catch (Exception e) {
			LOGGER.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}
		LOGGER.info("Exiting ProductDetailUsePojo:getKeyIngredientsTitle");
		return keyIngredients;
	}

	/**
	 * Gets the usage video path.
	 *
	 * @param graphicInfoPath
	 *            the graphic info path
	 * @return the usage video path
	 * @throws RepositoryException
	 *             the repository exception
	 */
	private String getUsageVideoPath(String graphicInfoPath) throws RepositoryException {
		LOGGER.info("Entering ProductDetailUsePojo:getUsageVideoPath");
		String usagePath = "";
		if (graphicInfoPath != null) {
			Resource graphicInfoResource = getResourceResolver()
					.getResource(graphicInfoPath + ProductDetailConstant.JCR_CONTENT_METADATA);
			Node videoNode = graphicInfoResource.adaptTo(Node.class);
			if (videoNode != null && videoNode.hasProperty(ProductDetailConstant.MP4_HD)) {
				String hdPath = videoNode.getProperty(ProductDetailConstant.MP4_HD).getString();
				String s3PathFromOSGI = "";//getSlingScriptHelper().getService(EnvironmentConfig.class).getS3BucketURL();
				usagePath = s3PathFromOSGI + hdPath;
			}
		}
		LOGGER.info("Exiting ProductDetailUsePojo:getUsageVideoPath");
		return usagePath;
	}

	/**
	 * Sets the common properties.
	 *
	 * @param baseProduct
	 *            the new common properties
	 */
	private void setCommonProperties(ProductDetail baseProduct) {
		description = baseProduct.getDescription();
		benefits = baseProduct.getBenefits();
		marketingMaterials = baseProduct.getMarketingMaterials();
		productInformation = baseProduct.getProductInformation();
		otherSource = baseProduct.getOtherSource();
		morningUsage = baseProduct.getMorningUsage();
		nightUsage = baseProduct.getNightUsage();
		generalUsage = baseProduct.getGeneralUsage();
		graphicInfo = baseProduct.getGraphicInfo();
		usageNote = baseProduct.getUsageNote();
		usageDesc = baseProduct.getUsageDesc();
		videoDuration = baseProduct.getVideoDuration();
		graphicDesc = baseProduct.getGraphicDesc();
	}

	/**
	 * Gets Category Standard Details.
	 *
	 * @param service            the service
	 * @param pathValues            countryCode
	 * @param locale the locale
	 * @return categoryStandard
	 */
	private void getCategoryStandardDetails(ProductDetailService service, String pathValues, Locale locale) {
		String[] pathValuesArray = pathValues.split(ProductDetailConstant.SLASH);
		categorySkuId = pathValuesArray[pathValuesArray.length - 1];
		List<ProductDetail> categoryStandard = service.getProductDetail(categorySkuId.toUpperCase(), locale);
		for (ProductDetail data : categoryStandard) {
			categoryProductName = data.getJcrTitle();
			categoryProductDescription = data.getDescription();
			if (categoryProductDescription != null && categoryProductDescription.length() > 120) {
				categoryProductDescription = categoryProductDescription.substring(0, 120) + ProductDetailConstant.DOT
						+ ProductDetailConstant.DOT + ProductDetailConstant.DOT;
			}
			if (data.getCommerceType().equalsIgnoreCase(ProductDetailConstant.VARIANT)) {
				categoryProductUrl = ProductDetailConstant.DOT + data.getBaseProductIdentifier()
						+ ProductDetailConstant.DOT + categorySkuId + ProductDetailConstant.HTML;
			} else {
				categoryProductUrl = ProductDetailConstant.DOT + categorySkuId + ProductDetailConstant.HTML;
			}
		}
	}

	/**
	 * Gets the slider image.
	 *
	 * @param skuIdentifier
	 *            the sku identifier
	 * @return the slider image
	 */
	private String getCarouselImage(String skuIdentifier) {
		String imagesString = "";
		LOGGER.info("Entering ProductDetailUsePojo:getCarouselImage");
		String imagesPath = ProductDetailConstant.DAM_BASE_PATH + skuIdentifier;
		String pdpRenditions = getSlingScriptHelper().getService(ProductsRenditionService.class).getProductDetailRendition();
		JSONArray imagesArray = new JSONArray();
		try {
			ResourceResolver resourceResolver = getResourceResolver();
			if(null != resourceResolver.getResource(imagesPath)){
				Node imagesNodes = resourceResolver.getResource(imagesPath).adaptTo(Node.class);
				NodeIterator nodeIterator;
				if (imagesNodes.hasNodes()) {
					nodeIterator = imagesNodes.getNodes();
					while (nodeIterator.hasNext()) {
						Node imageNode = nodeIterator.nextNode();
						JSONObject imagePath = new JSONObject();
						if (imageNode.getPrimaryNodeType().getName().equals(ProductDetailConstant.DAM_ASSET)) {
							String thumbnailPath = "";
							Rendition imageRenditions = resourceResolver.getResource(imageNode.getPath().toString()).adaptTo(Asset.class).getRendition(pdpRenditions);
							if (null != imageRenditions) {
								thumbnailPath = imageRenditions.getPath().toString();
							}
							imagePath.put(ProductDetailConstant.IMAGE_PATH, thumbnailPath);
							imagesArray.put(imagePath);
						}
					}
				}
			}
			imagesString = imagesArray.toString();
		} catch (RepositoryException e) {
			LOGGER.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
			imagesString = "";
		} catch (JSONException e) {
			LOGGER.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
			imagesString = "";
		} catch (Exception e) {
			LOGGER.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
			imagesString = "";
		}
		LOGGER.info("Exiting ProductDetailUsePojo:getCarouselImage");
		return imagesString;
	}

	/**
	 * Gets the product name.
	 *
	 * @return the product name
	 */
	public String getProductName() {
		return productName;
	}

	/**
	 * Gets the categoryProductName.
	 *
	 * @return the categoryProductName
	 */
	public String getCategoryProductName() {
		return categoryProductName;
	}

	/**
	 * Gets the categoryProductDescription.
	 *
	 * @return the categoryProductDescription
	 */
	public String getCategoryProductDescription() {
		return categoryProductDescription;
	}

	/**
	 * Gets the product description.
	 *
	 * @return the product description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Gets the product benefits.
	 *
	 * @return the product benefits
	 */
	public List<String> getBenefits() {
		return benefits;
	}

	/**
	 * Gets the marketingMaterials.
	 *
	 * @return the marketingMaterials
	 */
	public List<String> getMarketingMaterials() {
		return marketingMaterials;
	}

	/**
	 * Gets the productInformation.
	 *
	 * @return the productInformation
	 */
	public List<String> getProductInformation() {
		return productInformation;
	}

	/**
	 * Gets the otherSource.
	 *
	 * @return the otherSource
	 */
	public List<String> getOtherSource() {
		return otherSource;
	}

	/**
	 * Gets the product properties.
	 *
	 * @return the product properties
	 */
	public List<ProductDetail> getProductProperties() {
		return productProperties;
	}

	/**
	 * Gets the page path.
	 *
	 * @return the page path
	 */
	public String getPagePath() {
		return pagePath;
	}

	/**
	 * Gets the language.
	 *
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * Gets the product size.
	 *
	 * @return the product size
	 */
	public String getProductSize() {
		return productSize;
	}

	/**
	 * Gets the sku identifier.
	 *
	 * @return the sku identifier
	 */
	public String getSkuIdentifier() {
		return skuIdentifier;
	}

	/**
	 * Gets the images list.
	 *
	 * @return the images list
	 */
	public String getImagesList() {
		return imagesList;
	}

	/**
	 * Gets the country code.
	 *
	 * @return the country code
	 */
	public String getCountryCode() {
		return countryCode;
	}

	/**
	 * Gets the product variant axes.
	 *
	 * @return the productVariantAxes
	 */
	public List<String> getProductVariantAxes() {
		return productVariantAxes;
	}

	/**
	 * Gets the variant detail.
	 *
	 * @return the variantDetail
	 */
	public List<VariantDetail> getVariantDetail() {
		return variantDetail;
	}

	/**
	 * Gets the show shade.
	 *
	 * @return the showShade
	 */
	public boolean getShowShade() {
		return showShade;
	}

	/**
	 * Gets the morning usage.
	 *
	 * @return the morningUsage
	 */
	public List<String> getMorningUsage() {
		return morningUsage;
	}

	/**
	 * Gets the night usage.
	 *
	 * @return the nightUsage
	 */
	public List<String> getNightUsage() {
		return nightUsage;
	}

	/**
	 * Gets the general usage.
	 *
	 * @return the generalUsage
	 */
	public List<String> getGeneralUsage() {
		return generalUsage;
	}

	/**
	 * Gets the graphic info.
	 *
	 * @return the graphicInfo
	 */
	public String getGraphicInfo() {
		return graphicInfo;
	}

	/**
	 * Gets the usage note.
	 *
	 * @return the usageNote
	 */
	public String getUsageNote() {
		return usageNote;
	}

	/**
	 * Gets the usage desc.
	 *
	 * @return the usageDesc
	 */
	public String getUsageDesc() {
		return usageDesc;
	}

	/**
	 * Gets the video duration.
	 *
	 * @return the videoDuration
	 */
	public String getVideoDuration() {
		return videoDuration;
	}

	/**
	 * Gets the graphic desc.
	 *
	 * @return the graphicDesc
	 */
	public String getGraphicDesc() {
		return graphicDesc;
	}

	/**
	 * Gets the graphic type.
	 *
	 * @return the graphicType
	 */
	public String getGraphicType() {
		return graphicType;
	}

	/**
	 * Gets the suplement img.
	 *
	 * @return the suplement img
	 */
	public String getSuplementImg() {
		return suplementImg;
	}

	/**
	 * Gets the key ing.
	 *
	 * @return the key ing
	 */
	public List<KeyIngredients> getKeyIng() {
		return keyIng;
	}

	/**
	 * Gets the servings.
	 *
	 * @return the servings
	 */
	public List<String> getServings() {
		return servings;
	}

	/**
	 * Gets the special ing.
	 *
	 * @return the special ing
	 */
	public List<SpecialIngredients> getSpecialIng() {
		return specialIng;
	}

	/**
	 * Gets the other ing.
	 *
	 * @return the other ing
	 */
	public String getOtherIng() {
		return otherIng;
	}

	/**
	 * Gets the nutrition note.
	 *
	 * @return the nutrition note
	 */
	public String getNutritionNote() {
		return nutritionNote;
	}

	/**
	 * Gets the vitamins.
	 *
	 * @return the vitamins
	 */
	public String getVitamins() {
		return vitamins;
	}

	/**
	 * Gets the category product url.
	 *
	 * @return the category product url
	 */
	public String getCategoryProductUrl() {
		return categoryProductUrl;
	}

	/**
	 * Gets the variant selectors.
	 *
	 * @return the variantSelectors
	 */
	public String getVariantSelectors() {
		return variantSelectors;
	}

	/**
	 * Gets the purchasable.
	 *
	 * @return the purchasable
	 */
	public Boolean getPurchasable() {
		return purchasable;
	}

	/**
	 * Gets the productUsageVideoPath.
	 *
	 * @return the productUsageVideoPath
	 */
	public String getProductUsageVideoPath() {
		return productUsageVideoPath;
	}

	/**
	 * @return the showKeyIngredients
	 */
	public Boolean getShowKeyIngredients() {
		return showKeyIngredients;
	}

}
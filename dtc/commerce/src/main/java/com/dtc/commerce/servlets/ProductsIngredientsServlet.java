/**
 * Change History:
 * Revision     Date         Dev         Comments
 * ------------------------------------------------------------------------------------------------
 * 1.0          09.11.2016   Deloitte Digital   MEC-5106 - Category Ingredients 
 */
package com.dtc.commerce.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.CopyOnWriteArraySet;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.LanguageManager;
import com.dtc.commerce.productdetail.service.ProductsIngredientsService;
import com.dtc.commerce.productdetail.service.ProductsIngredientsServiceImpl;
import com.dtc.core.services.PageLocaleService;
import com.dtc.core.utils.LocaleUtil;

/**
 * The Class ProductsIngredients.
 */
@Service(ProductsIngredientsServlet.class)
@SlingServlet(paths = { "/bin/dtc/productsIngredients" }, generateComponent = false, methods = "GET")
@Component(label = "DTC Products Ingredients", description = "DTC Products Ingredients", enabled = true, immediate = true, metatype = false)
public class ProductsIngredientsServlet extends SlingAllMethodsServlet {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7039436371575942313L;

	/** The product ingredientservice. */
	ProductsIngredientsService productIngredientservice = new ProductsIngredientsServiceImpl();

	/** The Constant PATH. */
	private static final String PATH = "path";

	/** The logger. */
	private final Logger logger = LoggerFactory.getLogger(ProductsIngredientsServlet.class);

	/** The service. */
	@Reference
	LanguageManager languageManager;

	/** The PageLocaleService. */
	@Reference
	PageLocaleService localeService;

	/**
	 * Do get.
	 *
	 * @param req
	 *            the req
	 * @param resp
	 *            the resp
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Override
	protected void doGet(SlingHttpServletRequest req, SlingHttpServletResponse resp) throws IOException {
		logger.info("Entering ProductsIngredients.doGet()");

		try {
			PrintWriter writer = resp.getWriter();
			String pagePath = req.getParameter(PATH);
			Locale locale = LocaleUtil.getLocaleFromPath(pagePath);
			JSONArray ingredientsArray = new JSONArray();
			List<String> listIngredients = productIngredientservice.getAllTagsList(locale, req.getResourceResolver());
			if (!listIngredients.isEmpty()) {
				Collections.sort(listIngredients);
				CopyOnWriteArraySet<String> concurrentSetIngredients = new CopyOnWriteArraySet<String>(listIngredients);
				List<String> titles = new ArrayList<String>();
				Iterator<String> iterator = concurrentSetIngredients.iterator();
				while (iterator.hasNext()) {
					String ingredient = iterator.next();
					JSONObject jsonObject = new JSONObject();
					String ingredientTitle = ingredient.substring(0,1).toUpperCase();
					if (!titles.contains(ingredientTitle)) {
						titles.add(ingredient.substring(0,1).toUpperCase());
						jsonObject.put("title", "" + ingredient.substring(0,1).toUpperCase());
					}
					JSONArray itemsArray = new JSONArray();
					for (String itemIngredient : concurrentSetIngredients) {
						if (ingredient.substring(0,1).toUpperCase().equalsIgnoreCase(itemIngredient.substring(0,1).toUpperCase())) {
							JSONObject item = new JSONObject();
							item.put("item", itemIngredient);
							item.put("count", String.valueOf(Collections.frequency(listIngredients, itemIngredient)));
							itemsArray.put(item);
							concurrentSetIngredients.remove(itemIngredient);
						}
					}
					jsonObject.put("items", itemsArray);
					ingredientsArray.put(jsonObject);
					iterator = concurrentSetIngredients.iterator();
				}
				logger.info("Exit ProductsIngredients.doGet()");
				resp.setCharacterEncoding("UTF-8");
				resp.setContentType("application/json");
				writer.println(ingredientsArray.toString());

			} else {
				logger.info("Exit ProductsIngredients.doGet()");
				writer.println(ingredientsArray.toString());
			}
		} catch (Exception e) {
			logger.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}
	}
}

/**
 * Change History:
 * Revision     Date         Dev         Comments
 * ------------------------------------------------------------------------------------------------
 * 1.0          20.03.2017   Deloitte Digital   MEC-14814 - Create servlet to expose featured products and featured bundles 
 */
package com.dtc.commerce.servlets;

import java.io.IOException;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Value;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.Rendition;
import com.dtc.commerce.productdetail.common.ProductDetailConstant;
import com.dtc.commerce.productdetail.service.ProductsRenditionService;

/**
 * The Class FeaturedProductsServlet.
 */
@SlingServlet(paths="/dtc/featured/products", methods = HttpConstants.METHOD_GET, metatype=false)
public class FeaturedProductsServlet extends SlingSafeMethodsServlet {
	
	/**
	 * Instantiates a new featured products servlet.
	 */
	public FeaturedProductsServlet() {
		
	}

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(FeaturedProductsServlet.class);
	
	/** The Constant MARKET. */
	private static final String MARKET = "market";
	
	/** The Constant PATH. */
	private static final String LANGUAGE = "language";
	
	/** The Constant PRODUCT_CONTAINER_PATH. */
	private static final String PRODUCT_CONTAINER_PATH = "/jcr:content/productContainer";
	
	/** The Constant TOP_SELLERS_PATH. */
	private static final String TOP_SELLERS_PATH = "/jcr:content/par/toptensellers";
	
	/** The renditions service. */
	@Reference
	ProductsRenditionService renditionsService;
	
	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) {
		LOGGER.info("Entering: FeaturedProductsServlet :  doGet");
		try {
			JSONObject featuredList = new JSONObject(); 
			String market = request.getParameter(MARKET);
			String language = request.getParameter(LANGUAGE);
			ResourceResolver resourceResolver = request.getResourceResolver();
			Node compNode = null;
	
			if(null != resourceResolver.getResource("/content/dtc/americas/"+ market + TOP_SELLERS_PATH)){
					Resource componentResource = resourceResolver.getResource("/content/dtc/americas/"+ market + TOP_SELLERS_PATH);
					if(null != componentResource)
						compNode = componentResource.adaptTo(Node.class);
			}
	
			if (null != compNode) {
				if (compNode.hasProperty("topSellersTitle")) {
					featuredList.put("title", compNode.getProperty("topSellersTitle").getString());
				}
				if (compNode.hasProperty("pdpBasePath")) {
					featuredList.put("basePath", compNode.getProperty("pdpBasePath").getString());
				}
				if (compNode.hasProperty("topSellers")) {
					JSONArray featuredProducts = new JSONArray();
					Property topSellers = compNode.getProperty("topSellers");
					JSONObject productsObj;
					if (topSellers.isMultiple()) {
						Value[] values = topSellers.getValues();
						for (int i = 0; i < values.length; i++) {
							productsObj = getFeaturedDetails(values[i].toString(), resourceResolver, market, language);
							if (productsObj.length() > 0)
								featuredProducts.put(productsObj);
						}
					} else {
						Value val = topSellers.getValue();
						productsObj = getFeaturedDetails(val.toString(), resourceResolver, market, language);
						if (productsObj.length() > 0)
							featuredProducts.put(productsObj);
					}
					featuredList.put("products", featuredProducts);
				}
				if (compNode.hasProperty("bundles")) {
					JSONArray featuredBundles = new JSONArray();
					Property bundles = compNode.getProperty("bundles");
					JSONObject bundlesObj;
					if (bundles.isMultiple()) {
						Value[] values = bundles.getValues();
						for (int i = 0; i < values.length; i++) {
							bundlesObj = getFeaturedDetails(values[i].toString(), resourceResolver, market, language);
							if (bundlesObj.length() > 0)
								featuredBundles.put(bundlesObj);
						}
					} else {
						Value val = bundles.getValue();
						bundlesObj = getFeaturedDetails(val.toString(), resourceResolver, market, language);
						if (bundlesObj.length() > 0)
							featuredBundles.put(bundlesObj);
					}
					featuredList.put("bundles", featuredBundles);
				}
			}
				
			LOGGER.info("Exiting: FeaturedProductsServlet :  doGet");
			response.setCharacterEncoding("UTF-8");
			response.setContentType("application/json");
			response.getWriter().println(featuredList);
		} catch (IOException | JSONException | RepositoryException e) {
			LOGGER.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}

	}

	/**
	 * Gets the featured details.
	 *
	 * @param sku the sku
	 * @param resolver the resolver
	 * @param lang the lang
	 * @return the featured details
	 */
	private JSONObject getFeaturedDetails(String sku, ResourceResolver resolver, String country, String lang) {
		JSONObject top = new JSONObject();
		try {
			Resource productRes = resolver.getResource(sku + "/" + country + PRODUCT_CONTAINER_PATH);
			if (productRes == null) {
				productRes = resolver.getResource(sku + "/" + lang + PRODUCT_CONTAINER_PATH);
			}
			Node node = productRes.adaptTo(Node.class);
			if (node.hasNode("productDetail")) {
				Node pmdDetailNode = node.getNode("productDetail");
				if (pmdDetailNode.hasProperty(ProductDetailConstant.PROPERTY_JCR_TITLE)) {
					top.put("title", pmdDetailNode.getProperty(ProductDetailConstant.PROPERTY_JCR_TITLE).getString());
				}
				top.put("sku", node.getParent().getParent().getParent().getName().toUpperCase());
				top.put("productPath", ProductDetailConstant.DOT + node.getParent().getParent().getParent().getName() + ProductDetailConstant.HTML);
				top.put("path", getImageforSku(node.getParent().getParent().getParent().getName().toUpperCase(), resolver));
				top.put("baseSku", node.getParent().getParent().getParent().getName().toUpperCase());
				
				String baseSku = productRes.getValueMap().get("baseSku", String.class);
				if (StringUtils.isNotEmpty(baseSku)) {
					top.put("productPath", ProductDetailConstant.DOT + baseSku + "."
							+ node.getParent().getParent().getParent().getName() + ProductDetailConstant.HTML);
					top.put("baseSku", baseSku.toUpperCase());
				}
				top.put("commerceType", pmdDetailNode.getProperty(ProductDetailConstant.PROPERTY_COMMERCE_TYPE).getString());

			}
		} catch (Exception e) {
			LOGGER.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}
		return top;
	}
	
	/**
	 * Gets the imagefor sku.
	 *
	 * @param sku the sku
	 * @param res the res
	 * @return the imagefor sku
	 * @throws RepositoryException the repository exception
	 * @throws JSONException the JSON exception
	 */
	private String getImageforSku(String sku,ResourceResolver res)
			throws RepositoryException, JSONException {
		LOGGER.info("Entering FeaturedProductsServlet : getImageforSku");
		String imageforSku = "";
		try {
			String imagesPath = ProductDetailConstant.DAM_BASE_PATH + sku;
			String topTenSellersRendition = renditionsService.getTopTenSellersRendition();
			if(null != res.getResource(imagesPath)){
				Node imagesNodes = res.getResource(imagesPath).adaptTo(Node.class);
				NodeIterator nodeIterator = imagesNodes.getNodes();

				while (nodeIterator.hasNext()) {
					Node imageNode = nodeIterator.nextNode();
					if (imageNode.getPrimaryNodeType().getName().equals(ProductDetailConstant.DAM_ASSET)){
						Rendition imageRenditions = res.getResource(imageNode.getPath().toString()).adaptTo(Asset.class).getRendition(topTenSellersRendition);
		        		if(null !=  imageRenditions){
		        			imageforSku = imageRenditions.getPath().toString();
		        			break;
		        		}
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}
		LOGGER.info("Exiting FeaturedProductsServlet : getImageforSku");
		return imageforSku;
	}

}

package com.dtc.commerce.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Locale;

import javax.jcr.Session;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestPathInfo;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.PathInfo;
import com.day.cq.replication.ReplicationActionType;
import com.day.cq.replication.Replicator;
import com.dtc.commerce.utils.PMDUtil;
import com.dtc.core.utils.LocaleUtil; 


@SlingServlet(paths = "/dtc/replicate", methods = "GET", metatype = true)
public class PMDReplicatorServlet  extends SlingSafeMethodsServlet{
	/*
	 * This Servlet is a Custom Page Activator for PMD Pages
	 * */

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(PMDReplicatorServlet.class);

	private static final String QUERY_PARAM_PATH="path";
	private static final String QUERY_PARAM_COMMAND="command";
	private static final String EXCEPTION_STRING="error";
	private static final String NO_SKU="no-sku";
	private static final String COMMAND_ACTIVATE="Activate";
	private static final String COMMAND_DEACTIVATE="Deactivate";

	@Reference
	private transient Replicator replicator;

	@Reference
	private transient ResourceResolverFactory resolverFactory;

	public PMDReplicatorServlet() {
		super();		
	}

	@SuppressWarnings("deprecation")
	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {

		String path = request.getParameter(QUERY_PARAM_PATH);
		String command= request.getParameter(QUERY_PARAM_COMMAND);
		String responseString=NO_SKU;

		// Using Day provided Utility to get Selectors
		RequestPathInfo requestPathInfo = new PathInfo(path);
		String selectors [] =requestPathInfo.getSelectors();
		
		if(selectors.length!=0)
		{
			String sku = selectors[selectors.length-1];	// Extracting the Child Sku Only.	
			Locale locale = LocaleUtil.getLocaleFromPath(path);	
			String pmdPath = null;
			Session session=null;
			try{			
				ResourceResolver resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);
				session = resourceResolver.adaptTo(Session.class);
				ReplicationActionType type=null;

				if(COMMAND_ACTIVATE.equals(command))
				{
					type=ReplicationActionType.ACTIVATE;
					responseString="Resource Activated";
				}
				else if(COMMAND_DEACTIVATE.equals(command)) 
				{
					type=ReplicationActionType.DEACTIVATE;
					responseString="Resource Deactivated";
				}
				final String pmdBasePath = PMDUtil.getPmdPath(sku);
				pmdPath=pmdBasePath+"/"+locale.toString();
				Resource resource = request.getResourceResolver().getResource(pmdPath);
				if(resource==null)
				{
					pmdPath=pmdBasePath+"/"+locale.getLanguage();
				}			
				log.info("The PMD Path -{}- and Replication Command -{}-",pmdPath,command);
				replicator.replicate(session,type,pmdPath);			
			}
			catch(Exception ex)
			{
				log.error("EXCEPTION OCCURED IN PMD",ex);
				responseString=EXCEPTION_STRING;
			}
			finally{
				if(session!=null)
				{
					session.logout();
				}
			}
		}

		PrintWriter pw =response.getWriter();
		pw.print(responseString);
	}		
}

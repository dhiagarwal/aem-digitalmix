package com.dtc.commerce.productdetail.service;


public interface ProductsService {
    /**
     * Get all skus in the PIM for a given country
     * @param countryCode
     * @return
     */
    public String getAllPimSkus(String countryCode);

    /**
     * Get all skus that are in the PIM but not in the PMD for a given country and language
     * @param countryCode
     * @return
     */
    public String getMissingPMDSkus(String countryCode, String language);

    /**
     * Tokenizes a sku with the pattern dddddddd with the pattern dd/dd/dd/dddddddd
     * @param sku
     * @return
     */
    public String tokenizeSku(String sku);
}

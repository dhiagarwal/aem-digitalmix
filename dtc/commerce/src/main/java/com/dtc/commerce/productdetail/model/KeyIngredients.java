/**
 * Change History:
 * Revision     Date         Dev         Comments
 * ------------------------------------------------------------------------------------------------
 * 1.0          09.12.2016   Deloitte Digital   MEC- 2629 Product Supplements facts table 
 */
package com.dtc.commerce.productdetail.model;

/**
 * The Class KeyIngredients.
 */
public class KeyIngredients {
	
	/** The title. */
	private String[] title;
	
	/** The description. */
	private String description;
	
	/** The tags. */
	private String tags;
	
	/** The showTags. */
	private Boolean showTags;
	
	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String[] getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String[] title) {
		this.title = title;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * Gets the tags.
	 *
	 * @return the tags
	 */
	public String getTags() {
		return tags;
	}
	
	/**
	 * Sets the tags.
	 *
	 * @param tags the new tags
	 */
	public void setTags(String tags) {
		this.tags = tags;
	}

	
	/**
	 * @return the showTags
	 */
	public Boolean getShowTags() {
		return showTags;
	}

	/**
	 * @param showTags the showTags to set
	 */
	public void setShowTags(Boolean showTags) {
		this.showTags = showTags;
	}

}

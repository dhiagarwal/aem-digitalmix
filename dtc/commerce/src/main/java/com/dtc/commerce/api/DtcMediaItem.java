package com.dtc.commerce.api;

/**
 * Nu Skin media item.
 */
public interface DtcMediaItem
{
    /**
     * Gets the media item key.
     * 
     * @return The media item key.
     */
    String getKey();

    /**
     * Gets the media item path.
     * 
     * @return The media item path.
     */
    String getPath();

    /**
     * Gets the media item mime type.
     * 
     * @return The media item mime type.
     */
    String getMimeType();

    /**
     * Gets the media item description.
     * 
     * @return The media item description.
     */
    String getDescription();

}


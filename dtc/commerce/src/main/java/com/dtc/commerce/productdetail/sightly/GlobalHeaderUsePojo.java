package com.dtc.commerce.productdetail.sightly;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Value;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;
import com.dtc.commerce.productdetail.model.HeadingInfo;
import com.dtc.commerce.productdetail.model.Link;
import com.dtc.commerce.productdetail.model.LinkPair;
import com.dtc.commerce.productdetail.model.Promotion;

/**
 * The Class GlobalHeaderPojo for header component.
 * 
 * @author Deloitte Digital
 */
public class GlobalHeaderUsePojo extends WCMUsePojo {
	
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(GlobalHeaderUsePojo.class);

	/** The heading 1. */
	private HeadingInfo heading1;

	/** The heading 2. */
	private HeadingInfo heading2;

	/** The heading 3. */
	private HeadingInfo heading3;
	
	
	private String profileImageRefPathBlue;
	
	private String profileImageRefPathWhite;


	/* (non-Javadoc)
	 * @see com.adobe.cq.sightly.WCMUsePojo#activate()
	 */
	@Override
	public void activate() throws Exception {
		LOGGER.info("Entering GlobalHeaderUsePojo: activate");
		try {
			heading1 = new HeadingInfo();
			heading2 = new HeadingInfo();
			heading3 = new HeadingInfo();
			profileImageRefPathBlue = new String("/etc/clientlibs/dtc/core/main/css/src/assets/images/css/ic-profile-blue.svg");
			profileImageRefPathWhite = new String("/etc/clientlibs/dtc/core/main/css/src/assets/images/css/ic-profile.svg");
			SlingHttpServletRequest request = getRequest();
			String requestPath = request.getRequestPathInfo().getResourcePath();
			Resource resQueryParameterComp = getResourceResolver().getResource(requestPath);
			Node resQueryParameterNode = resQueryParameterComp.adaptTo(Node.class);

			heading1 = getHeadingDetails(resQueryParameterNode, "heading1name");
			heading2 = getHeadingDetails(resQueryParameterNode, "heading2name");
			heading3 = getHeadingDetails(resQueryParameterNode, "heading3name");
			
			profileImageRefPathBlue = getProfileImageReference(resQueryParameterNode, "profileimage","blue");
			profileImageRefPathWhite = getProfileImageReference(resQueryParameterNode, "profileimage","white");
			

		} catch (Exception e) {
			LOGGER.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}
		LOGGER.info("Exiting GlobalHeaderUsePojo: activate");
	}

	private String getProfileImageReference(Node resQueryParameterNode,
			String imageName, String imageType) {
		LOGGER.info("Entering GlobalHeaderUsePojo: getProfileImageReference");
		String profileimagereferencepath = "/etc/clientlibs/dtc/core/main/css/src/assets/images/css/ic-profile-blue.svg";
		try{
			Node imageComponentNode = resQueryParameterNode.getNode(imageName);
			if(imageComponentNode==null && imageType.equals("blue")){
				profileimagereferencepath = "/etc/clientlibs/dtc/core/main/css/src/assets/images/css/ic-profile-blue.svg";
			}
			else if(imageComponentNode==null && imageType.equals("white")){
				profileimagereferencepath = "/etc/clientlibs/dtc/core/main/css/src/assets/images/css/ic-profile.svg";
			}
			else{
				
				Value[] values = getValues(imageComponentNode, "fileReference");
				if(null!=values && values.length>0)
				{
					String imageRefPath = values[0].toString();
					profileimagereferencepath = imageRefPath;
				}
				else{
					if(imageType.equals("blue")){
						profileimagereferencepath = "/etc/clientlibs/dtc/core/main/css/src/assets/images/css/ic-profile-blue.svg";
					}
					if(imageType.equals("white")){
						profileimagereferencepath = "/etc/clientlibs/dtc/core/main/css/src/assets/images/css/ic-profile.svg";
					}	
				}
			}
			
		}
		catch(RepositoryException e){
			LOGGER.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}
		LOGGER.info("Exiting GlobalHeaderUsePojo: getProfileImageReference");
		
		return profileimagereferencepath;
	}

	/**
	 * Gets the heading details.
	 *
	 * @param resQueryParameterNode
	 *            the res query parameter node
	 * @param headingName
	 *            the heading name
	 * @return the heading details
	 */
	private HeadingInfo getHeadingDetails(Node resQueryParameterNode, String headingName) {
		LOGGER.info("Entering GlobalHeaderUsePojo: getHeadingDetails");
		HeadingInfo headingInfo = new HeadingInfo();
		try {
			if (null != getValues(resQueryParameterNode, headingName)
					&& getValues(resQueryParameterNode, headingName).length > 0)
				if (getValues(resQueryParameterNode, headingName).length > 0)
					headingInfo.setHeadingName(getValues(resQueryParameterNode, headingName).length > 0
							? getValues(resQueryParameterNode, headingName)[0].getString() : StringUtils.EMPTY);

			if (null != getPromotion(resQueryParameterNode, "h" + headingName.charAt(7) + "promotion"))
				headingInfo
						.setPromotion(getPromotion(resQueryParameterNode, "h" + headingName.charAt(7) + "promotion"));
			if (headingName.equals("heading1name")) {
				List<Link> links = getLinks(resQueryParameterNode, "heading1links");
				List<LinkPair> linkPairs = new ArrayList<LinkPair>();
				int i = 0;
				if (null != links && links.size() > 0) {
					while (i < links.size()) {
						LinkPair linkPair = new LinkPair();
						linkPair.setLink1Name(links.get(i).getName());
						linkPair.setLink1Url(links.get(i).getUrl().contains(".html") ? links.get(i).getUrl()
								: links.get(i).getUrl().concat(".html"));
						if ((i + 1) <= (links.size() - 1)) {
							linkPair.setLink2Name(links.get(i + 1).getName());
							linkPair.setLink2Url(links.get(i + 1).getUrl().contains(".html") ? links.get(i + 1).getUrl()
									: links.get(i + 1).getUrl().concat(".html"));
						}
						linkPairs.add(linkPair);
						i = i + 2;
					}
				}
				headingInfo.setUncategorizedLinkPairs(linkPairs);
				headingInfo.setUncategorizedLinks(getLinks(resQueryParameterNode, "heading1links"));
			} else {
				String headingString = "h" + headingName.charAt(7);
				headingInfo.setUncategorizedLinks(getLinks(resQueryParameterNode, headingString + "uncategorized"));
				Value[] uncategorizedValues = getValues(resQueryParameterNode, headingString + "uncategorizedName");
				Value[] subcategory1Values = getValues(resQueryParameterNode, headingString + "subcategory1name");
				Value[] subcategory2Values = getValues(resQueryParameterNode, headingString + "subcategory2name");
				Value[] subcategory3Values = getValues(resQueryParameterNode, headingString + "subcategory3name");
				
				if (null != uncategorizedValues && uncategorizedValues.length > 0)
					headingInfo.setUncategorizedName(uncategorizedValues[0].getString());
				
				if (null != subcategory1Values && subcategory1Values.length > 0)
					headingInfo.setSubcategory1Name(subcategory1Values[0].getString());
				if (null != subcategory2Values && subcategory2Values.length > 0)
					headingInfo.setSubcategory2Name(subcategory2Values[0].getString());
				if (null != subcategory3Values && subcategory3Values.length > 0)
					headingInfo.setSubcategory3Name(subcategory3Values[0].getString());
				if (null != getLinks(resQueryParameterNode, headingString + "subcategory1links"))
					headingInfo
							.setSubcategory1Links(getLinks(resQueryParameterNode, headingString + "subcategory1links"));
				if (null != getLinks(resQueryParameterNode, headingString + "subcategory2links"))
					headingInfo
							.setSubcategory2Links(getLinks(resQueryParameterNode, headingString + "subcategory2links"));
				if (null != getLinks(resQueryParameterNode, headingString + "subcategory3links"))
					headingInfo
							.setSubcategory3Links(getLinks(resQueryParameterNode, headingString + "subcategory3links"));
			}
		} catch (RepositoryException e) {
			LOGGER.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}
		LOGGER.info("Exiting GlobalHeaderUsePojo: getHeadingDetails");
		return headingInfo;
	}

	/**
	 * Gets the promotion.
	 *
	 * @param resQueryParameterNode
	 *            the res query parameter node
	 * @param promotionName
	 *            the promotion name
	 * @return the promotion
	 */
	private Promotion getPromotion(Node resQueryParameterNode, String promotionName) {
		LOGGER.info("Entering GlobalHeaderUsePojo: getPromotion");
		Promotion promotion = new Promotion();
		try {
			Value[] values = getValues(resQueryParameterNode, promotionName);
			if (null != values && values.length > 0) {
				JSONObject obj = new JSONObject(values[0].getString());
				promotion.setContent(obj.getString("./" + promotionName.substring(0, 2) + "content"));
				promotion.setHeader(obj.getString("./" + promotionName.substring(0, 2) + "header"));
				promotion.setImageUrl(obj.getString("./" + promotionName.substring(0, 2) + "imageurl"));
				Link link = new Link();
				link.setName(obj.getString("./" + promotionName.substring(0, 2) + "linktext"));
				link.setUrl(obj.getString("./" + promotionName.substring(0, 2) + "linkurl").contains(".html")
						? obj.getString("./" + promotionName.substring(0, 2) + "linkurl")
						: obj.getString("./" + promotionName.substring(0, 2) + "linkurl").concat(".html"));
				promotion.setLink(link);
			}
		} catch (JSONException e) {
			LOGGER.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}catch (RepositoryException re) {
			LOGGER.error(re.getClass() + ": " + re.getMessage() + ": " + re.getCause(), re);
		}
		LOGGER.info("Exiting GlobalHeaderUsePojo: getPromotion");
		return promotion;
	}

	/**
	 * Gets the links.
	 *
	 * @param resQueryParameterNode
	 *            the res query parameter node
	 * @param linksName
	 *            the links name
	 * @return the links
	 */
	private List<Link> getLinks(Node resQueryParameterNode, String linksName) {
		LOGGER.info("Entering GlobalHeaderUsePojo: getLinks");
		List<Link> links = new ArrayList<Link>();
		try {
			if (null != getValues(resQueryParameterNode, linksName) 
					&& getValues(resQueryParameterNode, linksName).length > 0) {
				for (Value val : getValues(resQueryParameterNode, linksName)) {
					JSONObject obj = new JSONObject(val.getString());
					Link link = new Link();
					link.setName(obj.getString("linktext"));
					link.setUrl(obj.getString("linkpath").contains(".html") ? obj.getString("linkpath")
							: obj.getString("linkpath").concat(".html"));
					links.add(link);
				}
			}
		} catch (JSONException e) {
			LOGGER.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}catch (RepositoryException re) {
			LOGGER.error(re.getClass() + ": " + re.getMessage() + ": " + re.getCause(), re);
		}
		LOGGER.info("Exiting GlobalHeaderUsePojo: getLinks");
		return links;
	}

	/**
	 * Gets the values.
	 *
	 * @param resQueryParameterNode
	 *            the res query parameter node
	 * @param propertyName
	 *            the property name
	 * @return the values
	 */
	private Value[] getValues(Node resQueryParameterNode, String propertyName) {
		LOGGER.info("Entering GlobalHeaderUsePojo: getValues");
		Value[] values = null;
		try {
			if (resQueryParameterNode.hasProperty(propertyName)
					&& null != resQueryParameterNode.getProperty(propertyName)) {
				Property property = resQueryParameterNode.getProperty(propertyName);
				if (property != null) {
					if (property.isMultiple()) {
						values = property.getValues();
					} else {
						values = new Value[1];
						values[0] = property.getValue();
					}
				}
			}
		} catch (RepositoryException e) {
			LOGGER.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}
		LOGGER.info("Exiting GlobalHeaderUsePojo: getValues");
		return values;
	}

	/**
	 * Gets the heading 1.
	 *
	 * @return the heading 1
	 */
	public HeadingInfo getHeading1() {
		return heading1;
	}

	/**
	 * Gets the heading 2.
	 *
	 * @return the heading 2
	 */
	public HeadingInfo getHeading2() {
		return heading2;
	}

	/**
	 * Gets the heading 3.
	 *
	 * @return the heading 3
	 */
	public HeadingInfo getHeading3() {
		return heading3;
	}

	/**
	 * @return the profileImageRefPathBlue
	 */
	public String getProfileImageRefPathBlue() {
		return profileImageRefPathBlue;
	}

	/**
	 * @return the profileImageRefPathWhite
	 */
	public String getProfileImageRefPathWhite() {
		return profileImageRefPathWhite;
	}
	
	
	
}

/**
 * Change History:
 * Revision     Date         Dev         Comments
 * ------------------------------------------------------------------------------------------------
 * 1.0          08.23.2016   Deloitte Digital   MEC-3522 - Display "How to Use" content on PDP 
 */
package com.dtc.commerce.productdetail.model;

import java.util.List;

/**
 * The ProductDetail Model Class contains Getter,Setter for all the properties of the Product.
 *
 * @author Deloitte Digital
 */
public class ProductDetail {

	/**
	 * Instantiates ProductDetail object.
	 */
	public ProductDetail() {
	}
	
	/** The identifier. */
	private String identifier;

	/** The product name. */
	private String productName;
	
	/** The product description. */
	private String description;
	
	/** The product benefits. */
	private List<String> benefits;

	/** The marketingMaterials. */
	private List<String> marketingMaterials;
	
	/** The productInformation. */
	private List<String> productInformation;
	
	/** The otherSource. */
	private List<String> otherSource;
	
	/** The url. */
	private String url;

	/** The purchasable. */
	private Boolean purchasable;

	/** The jcr title. */
	private String jcrTitle;

	/** The sling resource type. */
	private String slingResourceType;

	/** The mec variant value. */
	private String mecVariantValue;

	/** The language. */
	private String language;

	/** The product variant. */
	private String productVariant;

	/** The distribution channel. */
	private String distributionChannel;

	/** The mec variant type. */
	private String mecVariantType;

	/** The country code. */
	private String countryCode;

	/** The commerce type. */
	private String commerceType;

	/** The product variant axes. */
	private List<String> productVariantAxes;

	/** The average rating. */
	private String averageRating;

	/** The cross sell. */
	private List<String> crossSell;

	/** The up sell. */
	private String upSell;
	
	/** The up sell kit. */
	private String upSellKit;
	
	/** The base product identifier. */
	private String baseProductIdentifier;


	/** The variants. */
	private List<VariantDetail> variants;
	
	/** String to store image , video or text entered by marketing business user. */
	private String graphicInfo;
	
	/** The usage note. */
	private String usageNote;
	
	/** The general usage. */
	private List<String> generalUsage;
	
	/** The up sell kit. */
	private List<String> morningUsage;
	
	/** The up sell kit. */
	private List<String> nightUsage;
	
	/** The usage description. */
	private String usageDesc;
	
	/** The  video duration. */
	private String videoDuration;
	
	/** The graphic description. */
	private String graphicDesc;
	
	/** String to store image of Supplements. */
	private String suplementImg;
	
	/** The key Ingredients. */
	private List<KeyIngredients> keyIng;
	
	/** The servings. */
	private List<String> servings;
	
	/** The special Ingredients. */
	private List<SpecialIngredients> specialIng;
	
	/** The other Ingredients. */
	private String otherIng;
	
	/** The  nutrition Note. */
	private String nutritionNote;
	
	/** The vitamins. */
	private String vitamins;
	
	/** The size. */
	private String size ;
	
	/** The baseDescription. */
	private String baseDescription ;
	
	
	/**
	 * Gets the identifier.
	 *
	 * @return the identifier
	 */
	public String getIdentifier() {
		return identifier;
	}

	/**
	 * Sets the identifier.
	 *
	 * @param identifier the new identifier
	 */
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	/**
	 * Gets the product name.
	 *
	 * @return the product name
	 */
	public String getProductName() {
		return productName;
	}

	/**
	 * Sets the product name.
	 *
	 * @param productName the new product name
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * Gets the product description.
	 *
	 * @return the product description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the product description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * Gets the product benefits.
	 *
	 * @return the product benefits
	 */
	public List<String> getBenefits() {
		return benefits;
	}

	/**
	 * Sets the product benefits.
	 *
	 * @param list the new benefits
	 */
	public void setBenefits(List<String> list) {
		this.benefits = list;
	}
	
	/**
	 * Gets the product MarketingMaterials.
	 *
	 * @return the product MarketingMaterials
	 */
	public List<String> getMarketingMaterials() {
		return marketingMaterials;
	}
	
	/**
	 * Sets the product MarketingMaterials.
	 *
	 * @param marketingMaterials the new marketing materials
	 */
	public void setMarketingMaterials(List<String> marketingMaterials) {
		this.marketingMaterials = marketingMaterials;
	}
	
	/**
	 * Gets the productInformation.
	 *
	 * @return the productInformation
	 */
	public List<String> getProductInformation() {
		return productInformation;
	}
	
	/**
	 * Sets the productInformation.
	 *
	 * @param productInformation the new productInformation
	 */
	public void setProductInformation(List<String> productInformation) {
		this.productInformation = productInformation;
	}
	
	/**
	 * Gets the otherSource.
	 *
	 * @return the otherSource
	 */
	public List<String> getOtherSource() {
		return otherSource;
	}
	
	/**
	 * Sets the otherSource.
	 *
	 * @param otherSource the new otherSource
	 */
	public void setOtherSource(List<String> otherSource) {
		this.otherSource = otherSource;
	}
	/**
	 * Gets the url.
	 *
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * Sets the url.
	 *
	 * @param url the new url
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * Gets the purchasable.
	 *
	 * @return the purchasable
	 */
	public Boolean getPurchasable() {
		return purchasable;
	}

	/**
	 * Sets the purchasable.
	 *
	 * @param purchasable the new purchasable
	 */
	public void setPurchasable(Boolean purchasable) {
		this.purchasable = purchasable;
	}

	/**
	 * Gets the jcr title.
	 *
	 * @return the jcr title
	 */
	public String getJcrTitle() {
		return jcrTitle;
	}

	/**
	 * Sets the jcr title.
	 *
	 * @param jcrTitle the new jcr title
	 */
	public void setJcrTitle(String jcrTitle) {
		this.jcrTitle = jcrTitle;
	}

	/**
	 * Gets the sling resource type.
	 *
	 * @return the sling resource type
	 */
	public String getSlingResourceType() {
		return slingResourceType;
	}

	/**
	 * Sets the sling resource type.
	 *
	 * @param slingResourceType the new sling resource type
	 */
	public void setSlingResourceType(String slingResourceType) {
		this.slingResourceType = slingResourceType;
	}

	/**
	 * Gets the mec variant value.
	 *
	 * @return the mec variant value
	 */
	public String getMecVariantValue() {
		return mecVariantValue;
	}

	/**
	 * Sets the mec variant value.
	 *
	 * @param mecVariantValue the new mec variant value
	 */
	public void setMecVariantValue(String mecVariantValue) {
		this.mecVariantValue = mecVariantValue;
	}

	/**
	 * Gets the language.
	 *
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * Sets the language.
	 *
	 * @param language the new language
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	/**
	 * Gets the product variant.
	 *
	 * @return the product variant
	 */
	public String getProductVariant() {
		return productVariant;
	}

	/**
	 * Sets the product variant.
	 *
	 * @param productVariant the new product variant
	 */
	public void setProductVariant(String productVariant) {
		this.productVariant = productVariant;
	}

	/**
	 * Gets the distribution channel.
	 *
	 * @return the distribution channel
	 */
	public String getDistributionChannel() {
		return distributionChannel;
	}

	/**
	 * Sets the distribution channel.
	 *
	 * @param distributionChannel the new distribution channel
	 */
	public void setDistributionChannel(String distributionChannel) {
		this.distributionChannel = distributionChannel;
	}

	/**
	 * Gets the mec variant type.
	 *
	 * @return the mec variant type
	 */
	public String getMecVariantType() {
		return mecVariantType;
	}

	/**
	 * Sets the mec variant type.
	 *
	 * @param mecVariantType the new mec variant type
	 */
	public void setMecVariantType(String mecVariantType) {
		this.mecVariantType = mecVariantType;
	}

	/**
	 * Gets the country code.
	 *
	 * @return the country code
	 */
	public String getCountryCode() {
		return countryCode;
	}

	/**
	 * Sets the country code.
	 *
	 * @param countryCode the new country code
	 */
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	/**
	 * Gets the commerce type.
	 *
	 * @return the commerce type
	 */
	public String getCommerceType() {
		return commerceType;
	}

	/**
	 * Sets the commerce type.
	 *
	 * @param commerceType the new commerce type
	 */
	public void setCommerceType(String commerceType) {
		this.commerceType = commerceType;
	}

	/**
	 * Gets the average rating.
	 *
	 * @return the average rating
	 */
	public String getAverageRating() {
		return averageRating;
	}

	/**
	 * Sets the average rating.
	 *
	 * @param averageRating the new average rating
	 */
	public void setAverageRating(String averageRating) {
		this.averageRating = averageRating;
	}

	/**
	 * Gets the up sell.
	 *
	 * @return the up sell
	 */
	public String getUpSell() {
		return upSell;
	}

	/**
	 * Sets the up sell.
	 *
	 * @param upSell the new up sell
	 */
	public void setUpSell(String upSell) {
		this.upSell = upSell;
	}
	
	/**
	 * Gets the product variant axes.
	 *
	 * @return the productVariantAxes
	 */
	public List<String> getProductVariantAxes() {
		return productVariantAxes;
	}

	/**
	 * Sets the product variant axes.
	 *
	 * @param list the productVariantAxes to set
	 */
	public void setProductVariantAxes(List<String> list) {
		this.productVariantAxes = list;
	}

	/**
	 * Gets the cross sell.
	 *
	 * @return the crossSell
	 */
	public List<String> getCrossSell() {
		return crossSell;
	}

	/**
	 * Sets the cross sell.
	 *
	 * @param list the crossSell to set
	 */
	public void setCrossSell(List<String> list) {
		this.crossSell = list;
	} 
	
	/**
	 * Gets the up sell kit.
	 *
	 * @return the up sell kit
	 */
	public final String getUpSellKit() {
		return upSellKit;
	}

	/**
	 * Sets the up sell kit.
	 *
	 * @param upSellKit the new up sell kit
	 */
	public final void setUpSellKit(String upSellKit) {
		this.upSellKit = upSellKit;
	}
	
	/**
	 * Gets the base product identifier.
	 *
	 * @return the base product identifier
	 */
	public String getBaseProductIdentifier() {
		return baseProductIdentifier;
	}

	/**
	 * Sets the base product identifier.
	 *
	 * @param baseProductIdentifier the new base product identifier
	 */
	public void setBaseProductIdentifier(String baseProductIdentifier) {
		this.baseProductIdentifier = baseProductIdentifier;
	}


	/**
	 * Gets the variants.
	 *
	 * @return the variants
	 */
	public List<VariantDetail> getVariants() {
		return variants;
	}

	/**
	 * Sets the variants.
	 *
	 * @param variants the variants to set
	 */
	public void setVariants(List<VariantDetail> variants) {
		this.variants = variants;
	}
	
	/**
	 * Gets the graphic info.
	 *
	 * @return the graphicInfo
	 */
	public String getGraphicInfo() {
		return graphicInfo;
	}
	/**
	 * Sets the graphicInfo.
	 *
	 * @param graphicInfo the new graphicInfo
	 */
	public void setGraphicInfo(String graphicInfo) {
		this.graphicInfo = graphicInfo;
	}
	
	/**
	 * Gets the usage note.
	 *
	 * @return the usageNote
	 */
	public String getUsageNote() {
		return usageNote;
	}
	/**
	 * Sets the usageNote.
	 *
	 * @param usageNote the new usageNote
	 */
	public void setUsageNote(String usageNote) {
		this.usageNote = usageNote;
	}
	
	/**
	 * Gets the general usage.
	 *
	 * @return the generalUsage
	 */
	public List<String> getGeneralUsage() {
		return generalUsage;
	}
	/**
	 * Sets the generalUsage.
	 *
	 * @param generalUsage the new generalUsage
	 */
	public void setGeneralUsage(List<String> generalUsage) {
		this.generalUsage = generalUsage;
	}
	
	/**
	 * Gets the morning usage.
	 *
	 * @return the morningUsage
	 */
	public List<String> getMorningUsage() {
		return morningUsage;
	}
	/**
	 * Sets the morningUsage.
	 *
	 * @param morningUsage the new morningUsage
	 */
	public void setMorningUsage(List<String> morningUsage) {
		this.morningUsage = morningUsage;
	}
	
	/**
	 * Gets the night usage.
	 *
	 * @return the nightUsage
	 */
	public List<String> getNightUsage() {
		return nightUsage;
	}
	/**
	 * Sets the nightUsage.
	 *
	 * @param nightUsage the new nightUsage
	 */
	public void setNightUsage(List<String> nightUsage) {
		this.nightUsage = nightUsage;
	}
	
	/**
	 * Gets the usage desc.
	 *
	 * @return the usageDesc
	 */
	public String getUsageDesc() {
		return usageDesc;
	}
	/**
	 * Sets the usage Desc.
	 *
	 * @param usageDesc the new usage Desc
	 */
	public void setUsageDesc(String usageDesc) {
		this.usageDesc = usageDesc;
	}
	
	/**
	 * Gets the video duration.
	 *
	 * @return the videoDuration
	 */
	public String getVideoDuration() {
		return videoDuration;
	}
	/**
	 * Sets the video Duration.
	 *
	 * @param videoDuration the new video Duration
	 */
	public void setVideoDuration(String videoDuration) {
		this.videoDuration = videoDuration;
	}
	
	/**
	 * Gets the graphic desc.
	 *
	 * @return the graphicDesc
	 */
	public String getGraphicDesc() {
		return graphicDesc;
	}
	/**
	 * Sets the graphic Desc.
	 *
	 * @param graphicDesc the new up graphic Desc
	 */
	public void setGraphicDesc(String graphicDesc) {
		this.graphicDesc = graphicDesc;
	}	
	
	/**
	 * Gets the suplement img.
	 *
	 * @return the suplement img
	 */
	public String getSuplementImg() {
		return suplementImg;
	}

	/**
	 * Sets the suplement img.
	 *
	 * @param suplementImg the new suplement img
	 */
	public void setSuplementImg(String suplementImg) {
		this.suplementImg = suplementImg;
	}

	/**
	 * Gets the key ing.
	 *
	 * @return the key ing
	 */
	public List<KeyIngredients> getKeyIng() {
		return keyIng;
	}

	/**
	 * Sets the key ing.
	 *
	 * @param keyIng the new key ing
	 */
	public void setKeyIng(List<KeyIngredients> keyIng) {
		this.keyIng = keyIng;
	}

	/**
	 * Gets the servings.
	 *
	 * @return the servings
	 */
	public List<String> getServings() {
		return servings;
	}

	/**
	 * Sets the servings.
	 *
	 * @param servings the new servings
	 */
	public void setServings(List<String> servings) {
		this.servings = servings;
	}

	/**
	 * Gets the special ing.
	 *
	 * @return the special ing
	 */
	public List<SpecialIngredients> getSpecialIng() {
		return specialIng;
	}

	/**
	 * Sets the special ing.
	 *
	 * @param specialIng the new special ing
	 */
	public void setSpecialIng(List<SpecialIngredients> specialIng) {
		this.specialIng = specialIng;
	}

	/**
	 * Gets the other ing.
	 *
	 * @return the other ing
	 */
	public String getOtherIng() {
		return otherIng;
	}

	/**
	 * Sets the other ing.
	 *
	 * @param otherIng the new other ing
	 */
	public void setOtherIng(String otherIng) {
		this.otherIng = otherIng;
	}

	/**
	 * Gets the nutrition note.
	 *
	 * @return the nutrition note
	 */
	public String getNutritionNote() {
		return nutritionNote;
	}

	/**
	 * Sets the nutrition note.
	 *
	 * @param nutritionNote the new nutrition note
	 */
	public void setNutritionNote(String nutritionNote) {
		this.nutritionNote = nutritionNote;
	}

	/**
	 * Gets the vitamins.
	 *
	 * @return the vitamins
	 */
	public String getVitamins() {
		return vitamins;
	}

	/**
	 * Sets the vitamins.
	 *
	 * @param vitamins the new vitamins
	 */
	public void setVitamins(String vitamins) {
		this.vitamins = vitamins;
	}
	
	/**
	 * Gets the size.
	 *
	 * @return the size
	 */
	public String getSize() {
		return size;
	}

	/**
	 * Sets the size.
	 *
	 * @param size the new size
	 */
	public void setSize(String size) {
		this.size = size;
	}
	
	/**
	 * Gets the baseDescription.
	 *
	 * @return the baseDescription
	 */
	public String getBaseDescription() {
		return baseDescription;
	}

	/**
	 * Sets the baseDescription.
	 *
	 * @param baseDescription the new baseDescription
	 */
	public void setBaseDescription(String baseDescription) {
		this.baseDescription = baseDescription;
	}

}

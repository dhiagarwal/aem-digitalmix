/**
 * Change History:
 * Revision     Date         Dev         Comments
 * ------------------------------------------------------------------------------------------------
 * 1.0          17.11.2016   Deloitte Digital   MEC-10955 - Using image renditions for all product assets
 */
package com.dtc.commerce.productdetail.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Dictionary;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;

/**
 * The Class ProductsRenditionServiceImpl.
 */
@Component(label = "DTC Commerce Dam Renditions Configuration", metatype = true, immediate = true, description = "Service that handles dam rendition Configurations for products")
@Service({ ProductsRenditionService.class })
public class ProductsRenditionServiceImpl implements ProductsRenditionService {
	
	/**
	 * Instantiates ProductsRenditionServiceImpl object.
	 */
	public ProductsRenditionServiceImpl() {
	}

	/** The logger. */
	public static final Logger LOGGER = LoggerFactory.getLogger(ProductsRenditionServiceImpl.class);

	/** The Constant PDP. */
	@Property(label = "Product Detail Rendition")
	private static final String PDP = "rendition.pdp";

	/** The Constant CROSS_SELL. */
	@Property(label = "CrossSell Rendition")
	private static final String CROSS_SELL = "rendition.crosssell";

	/** The Constant UP_SELL. */
	@Property(label = "Up Sell Rendition")
	private static final String UP_SELL = "rendition.upsell";

	/** The Constant UPSELL_KIT. */
	@Property(label = "UpSell Kit Rendition")
	private static final String UPSELL_KIT = "rendition.upsellKit";

	/** The Constant CART_RECOMMENDATION. */
	@Property(label = "CartRecommendation Rendition")
	private static final String CART_RECOMMENDATION = "rendition.cartRecommendation";

	/** The Constant TOP_TEN_SELLERS. */
	@Property(label = "Top 10 Sellers Rendition")
	private static final String TOP_TEN_SELLERS = "rendition.toptenSellers";

	/** The Constant RECENTLY_VIEWED. */
	@Property(label = "Recently Viewed Rendition")
	private static final String RECENTLY_VIEWED = "rendition.recentlyViewed";

	/** The Constant CATERGORY_STANDARD. */
	@Property(label = "Category Standard Rendition")
	private static final String CATERGORY_STANDARD = "rendition.categoryStandard";

	/** The Constant SHOPPING_CART. */
	@Property(label = "Shopping Cart Rendition")
	private static final String SHOPPING_CART = "rendition.shoppingCart";

	/** The Constant ORDER_REVIEW. */
	@Property(label = "Order Review Rendition")
	private static final String ORDER_REVIEW = "rendition.orderReview";

	/** The Constant PRODUCT_SEARCH_LIST. */
	@Property(label = "Product Search List View Rendition")
	private static final String PRODUCT_SEARCH_LIST = "rendition.productSearchList";

	/** The Constant PRODUCT_SEARCH_GRID. */
	@Property(label = "Product Search Grid View Rendition")
	private static final String PRODUCT_SEARCH_GRID = "rendition.productSearchGrid";

	/** The product detail rendition. */
	private String productDetailRendition;

	/** The cross sell rendition. */
	private String crossSellRendition;

	/** The up sell rendition. */
	private String upSellRendition;

	/** The up sell kit rendition. */
	private String upSellKitRendition;

	/** The cart recommendation rendition. */
	private String cartRecommendationRendition;

	/** The top ten sellers rendition. */
	private String topTenSellersRendition;

	/** The recently viewed rendition. */
	private String recentlyViewedRendition;

	/** The category standard rendition. */
	private String categoryStandardRendition;

	/** The shopping cart rendition. */
	private String shoppingCartRendition;

	/** The order review rendition. */
	private String orderReviewRendition;

	/** The product search list rendition. */
	private String productSearchListRendition;

	/** The product search grid rendition. */
	private String productSearchGridRendition;

	/**
	 * Activate.
	 *
	 * @param context the context
	 */
	@Activate
	protected void activate(final ComponentContext context) {
		LOGGER.info("DTC Commerce Dam Renditions Configuration: activating configuration service");
		final Dictionary<?, ?> config = context.getProperties();

		readProperties(config);
	}

	/**
	 * Read properties.
	 *
	 * @param config
	 *            the properties
	 */
	protected void readProperties(Dictionary<?, ?> config) {
		LOGGER.info("Entering ProductsRenditionServiceImpl : readProperties");

		setProductDetailRendition(PropertiesUtil.toString(config.get(PDP), null));

		setCrossSellRendition(PropertiesUtil.toString(config.get(CROSS_SELL), null));

		setUpSellRendition(PropertiesUtil.toString(config.get(UP_SELL), null));

		setUpSellKitRendition(PropertiesUtil.toString(config.get(UPSELL_KIT), null));

		setCartRecommendationRendition(PropertiesUtil.toString(config.get(CART_RECOMMENDATION), null));

		setTopTenSellersRendition(PropertiesUtil.toString(config.get(TOP_TEN_SELLERS), null));

		setRecentlyViewedRendition(PropertiesUtil.toString(config.get(RECENTLY_VIEWED), null));

		setCategoryStandardRendition(PropertiesUtil.toString(config.get(CATERGORY_STANDARD), null));

		setShoppingCartRendition(PropertiesUtil.toString(config.get(SHOPPING_CART), null));

		setOrderReviewRendition(PropertiesUtil.toString(config.get(ORDER_REVIEW), null));

		setProductSearchListRendition(PropertiesUtil.toString(config.get(PRODUCT_SEARCH_LIST), null));

		setProductSearchGridRendition(PropertiesUtil.toString(config.get(PRODUCT_SEARCH_GRID), null));

		LOGGER.info("Exiting ProductsRenditionServiceImpl : readProperties");

	}

	public String getProductDetailRendition() {
		return productDetailRendition;
	}

	/**
	 * Sets the product detail rendition.
	 *
	 * @param productDetailRendition the new product detail rendition
	 */
	public void setProductDetailRendition(String productDetailRendition) {
		this.productDetailRendition = productDetailRendition;
	}

	public String getCrossSellRendition() {
		return crossSellRendition;
	}

	/**
	 * Sets the cross sell rendition.
	 *
	 * @param crossSellRendition the new cross sell rendition
	 */
	public void setCrossSellRendition(String crossSellRendition) {
		this.crossSellRendition = crossSellRendition;
	}

	public String getUpSellRendition() {
		return upSellRendition;
	}

	/**
	 * Sets the up sell rendition.
	 *
	 * @param upSellRendition the new up sell rendition
	 */
	public void setUpSellRendition(String upSellRendition) {
		this.upSellRendition = upSellRendition;
	}

	public String getUpSellKitRendition() {
		return upSellKitRendition;
	}

	/**
	 * Sets the up sell kit rendition.
	 *
	 * @param upSellKitRendition the new up sell kit rendition
	 */
	public void setUpSellKitRendition(String upSellKitRendition) {
		this.upSellKitRendition = upSellKitRendition;
	}

	public String getCartRecommendationRendition() {
		return cartRecommendationRendition;
	}

	/**
	 * Sets the cart recommendation rendition.
	 *
	 * @param cartRecommendationRendition the new cart recommendation rendition
	 */
	public void setCartRecommendationRendition(String cartRecommendationRendition) {
		this.cartRecommendationRendition = cartRecommendationRendition;
	}

	public String getTopTenSellersRendition() {
		return topTenSellersRendition;
	}

	/**
	 * Sets the top ten sellers rendition.
	 *
	 * @param topTenSellersRendition the new top ten sellers rendition
	 */
	public void setTopTenSellersRendition(String topTenSellersRendition) {
		this.topTenSellersRendition = topTenSellersRendition;
	}

	public String getRecentlyViewedRendition() {
		return recentlyViewedRendition;
	}

	/**
	 * Sets the recently viewed rendition.
	 *
	 * @param recentlyViewedRendition the new recently viewed rendition
	 */
	public void setRecentlyViewedRendition(String recentlyViewedRendition) {
		this.recentlyViewedRendition = recentlyViewedRendition;
	}

	public String getCategoryStandardRendition() {
		return categoryStandardRendition;
	}

	/**
	 * Sets the category standard rendition.
	 *
	 * @param categoryStandardRendition the new category standard rendition
	 */
	public void setCategoryStandardRendition(String categoryStandardRendition) {
		this.categoryStandardRendition = categoryStandardRendition;
	}

	public String getShoppingCartRendition() {
		return shoppingCartRendition;
	}

	/**
	 * Sets the shopping cart rendition.
	 *
	 * @param shoppingCartRendition the new shopping cart rendition
	 */
	public void setShoppingCartRendition(String shoppingCartRendition) {
		this.shoppingCartRendition = shoppingCartRendition;
	}

	public String getOrderReviewRendition() {
		return orderReviewRendition;
	}

	/**
	 * Sets the order review rendition.
	 *
	 * @param orderReviewRendition the new order review rendition
	 */
	public void setOrderReviewRendition(String orderReviewRendition) {
		this.orderReviewRendition = orderReviewRendition;
	}

	public String getProductSearchListRendition() {
		return productSearchListRendition;
	}

	/**
	 * Sets the product search list rendition.
	 *
	 * @param productSearchListRendition the new product search list rendition
	 */
	public void setProductSearchListRendition(String productSearchListRendition) {
		this.productSearchListRendition = productSearchListRendition;
	}

	public String getProductSearchGridRendition() {
		return productSearchGridRendition;
	}

	/**
	 * Sets the product search grid rendition.
	 *
	 * @param productSearchGridRendition the new product search grid rendition
	 */
	public void setProductSearchGridRendition(String productSearchGridRendition) {
		this.productSearchGridRendition = productSearchGridRendition;
	}

}

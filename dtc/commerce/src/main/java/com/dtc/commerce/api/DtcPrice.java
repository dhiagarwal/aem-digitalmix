package com.dtc.commerce.api;

/**
 * Nu Skin price.
 */
public interface DtcPrice
{
    /**
     * Gets the price value.
     * 
     * @return The price value.
     */
    String getValue();

    /**
     * Gets the price currency symbol.
     * 
     * @return The price currency symbol.
     */
    String getCurrencySymbol();
    
    /**
     * Gets the price currency ISO code.
     * 
     * @return The price currency ISO code.
     */
    String getCurrencyCode();

}

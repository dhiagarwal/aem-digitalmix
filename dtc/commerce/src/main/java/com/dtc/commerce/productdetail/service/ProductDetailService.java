/**
 * Change History:
 * Revision     Date         Dev         Comments
 * ------------------------------------------------------------------------------------------------
 * 1.0          06.22.2016  
 */
package com.dtc.commerce.productdetail.service;

import com.dtc.commerce.productdetail.model.ProductDetail;

import java.util.List;
import java.util.Locale;

import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONObject;

/**
 * The OSGI service Interface ProductDetailService to Query the product in AEM.
 *
 * @author Deloitte Digital
 */
public interface ProductDetailService {
	
    /**
     * Gets the product detail.
     *
     * @param baseSku the sku identifier
     * @param locale the locale
     * @return the product detail
     */
    public List<String> getVariantList(String baseSku, Locale locale);
    //public List<ProductDetail> getProductDetail(String skuIdentifier, String countryCode);
    public List<ProductDetail> getProductDetail(String skuIdentifier, Locale locale);

    public JSONArray crossSellInformation(String sku, Locale locale);
    
    public JSONArray upSellInformation(String sku, Locale locale);
    
    public JSONObject upSellKitInformation(String sku, Locale locale);
}

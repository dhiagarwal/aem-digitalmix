/**
 * Change History:
 * Revision     Date         Dev         Comments
 * ------------------------------------------------------------------------------------------------
 * 1.0          17.11.2016   Deloitte Digital   MEC-10955 - Using image renditions for all product assets
 */
package com.dtc.commerce.productdetail.service;


/**
 * The Interface ProductsRenditionService.
 */
public interface ProductsRenditionService {
	
	/**
	 * Gets the product detail rendition.
	 *
	 * @return the product detail rendition
	 */
	public String getProductDetailRendition();
	
	/**
	 * Gets the cross sell rendition.
	 *
	 * @return the cross sell rendition
	 */
	public String getCrossSellRendition();
	
	/**
	 * Gets the up sell rendition.
	 *
	 * @return the up sell rendition
	 */
	public String getUpSellRendition();
	
	/**
	 * Gets the up sell kit rendition.
	 *
	 * @return the up sell kit rendition
	 */
	public String getUpSellKitRendition();
	
	/**
	 * Gets the cart recommendation rendition.
	 *
	 * @return the cart recommendation rendition
	 */
	public String getCartRecommendationRendition();
	
	/**
	 * Gets the top ten sellers rendition.
	 *
	 * @return the top ten sellers rendition
	 */
	public String getTopTenSellersRendition();
	
	/**
	 * Gets the recently viewed rendition.
	 *
	 * @return the recently viewed rendition
	 */
	public String getRecentlyViewedRendition();
	
	/**
	 * Gets the category standard rendition.
	 *
	 * @return the category standard rendition
	 */
	public String getCategoryStandardRendition();
	
	/**
	 * Gets the shopping cart rendition.
	 *
	 * @return the shopping cart rendition
	 */
	public String getShoppingCartRendition();
	
	/**
	 * Gets the order review rendition.
	 *
	 * @return the order review rendition
	 */
	public String getOrderReviewRendition();
	
	/**
	 * Gets the product search list rendition.
	 *
	 * @return the product search list rendition
	 */
	public String getProductSearchListRendition();
	
	/**
	 * Gets the product search grid rendition.
	 *
	 * @return the product search grid rendition
	 */
	public String getProductSearchGridRendition();
}

/**
 * Change History:
 * Revision     Date         Dev         Comments
 * ------------------------------------------------------------------------------------------------
 * 1.0          09.12.2016   Deloitte Digital   MEC- 2629 Product Supplements facts table 
 */
package com.dtc.commerce.productdetail.model;

/**
 * The SpecialIngredients Model Class contains Getter,Setter for all the properties of the SpecialIngredients.
 *
 * @author Deloitte Digital
 */
public class SpecialIngredients {
	
	/**
	 * Instantiates SpecialIngredients object.
	 */
	public SpecialIngredients() {
	}
	
	/** The ingredient. */
	private String ingredient;
	
	/** The amount. */
	private String amount;
	
	/** The dailyValue. */
	private String dailyValue;
	
	/**
	 * @return the ingredient
	 */
	public String getIngredient() {
		return ingredient;
	}
	
	/**
	 * @param ingredient
	 *            the ingredient to set
	 */
	public void setIngredient(String ingredient) {
		this.ingredient = ingredient;
	}
	
	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}
	
	/**
	 * @param amount
	 *            the amount to set
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}
	
	/**
	 * @return the dailyValue
	 */
	public String getDailyValue() {
		return dailyValue;
	}
	
	/**
	 * @param dailyValue
	 *            the dailyValue to set
	 */
	public void setDailyValue(String dailyValue) {
		this.dailyValue = dailyValue;
	}
	
}

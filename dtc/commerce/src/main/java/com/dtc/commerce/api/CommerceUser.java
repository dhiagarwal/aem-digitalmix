package com.dtc.commerce.api;

/**
 * Commerce user interface.
 */
public interface CommerceUser
{
    /**
     * Gets the username.

     * @return The username.
     */
    String getUsername();
    
    /**
     * Gets the first name.
     * 
     * @return The first name.
     */
    String getFirstName();
    
    /**
     * Gets the last name.
     * 
     * @return The last name.
     */
    String getLastName();
    
}

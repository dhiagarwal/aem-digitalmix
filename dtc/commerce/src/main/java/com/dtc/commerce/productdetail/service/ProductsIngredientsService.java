package com.dtc.commerce.productdetail.service;

import java.util.List;
import java.util.Locale;

import org.apache.sling.api.resource.ResourceResolver;

/**
 * The Interface ProductsIngredientsService.
 */
public interface ProductsIngredientsService {

	/**
	 * Gets the all tags list.
	 *
	 * @param Locale locale
	 * @param resourceResolver the resource resolver
	 * @return the all tags list
	 * @throws Exception the exception
	 */
	public List<String> getAllTagsList(Locale locale,ResourceResolver resourceResolver) throws Exception;
}

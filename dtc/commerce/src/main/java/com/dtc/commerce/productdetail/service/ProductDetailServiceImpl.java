/**
 * Change History:
 * Revision     Date         Dev         Comments
 * ------------------------------------------------------------------------------------------------
 * 1.0          08.23.2016   Deloitte Digital   MEC-3522 - Display "How to Use" content on PDP 
 */
package com.dtc.commerce.productdetail.service;

import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.Rendition;
import com.day.cq.search.QueryBuilder;
import com.dtc.commerce.productdetail.common.ProductDetailConstant;
import com.dtc.commerce.productdetail.model.KeyIngredients;
import com.dtc.commerce.productdetail.model.ProductDetail;
import com.dtc.commerce.productdetail.model.SpecialIngredients;
import com.dtc.commerce.productdetail.model.VariantDetail;
import com.dtc.commerce.utils.PMDUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ResourceUtil;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.*;
import java.util.*;

/**
 * 
 * The OSGI Service implementation Class ProductDetailServiceImpl.
 * 
 * @author Deloitte Digital
 *
 */
@Component(label = "Product Detail Service", metatype = false, description = "Service that query the products from AEM", immediate = true)
@Service(ProductDetailService.class)
public class ProductDetailServiceImpl implements ProductDetailService {
	/** The LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(ProductDetailServiceImpl.class);

	/** The Constant productContainerPath. */
	private static final String productContainerPath = "/jcr:content/productContainer";

	/** The session. */
	private Session session;

	/** Inject a Sling ResourceResolverFactory. */
	@Reference
	private ResourceResolverFactory resolverFactory;

	/** The resource resolver. */
	private ResourceResolver resourceResolver = null;

	/** Inject a Sling QueryBuilder. */
	@Reference
	private QueryBuilder builder;
	
	/** The productsRenditionService. */
	@Reference
	ProductsRenditionService productsRenditionService;

	@Override
	public List<String> getVariantList(String baseSku, Locale locale) {
		List<String> variants = new ArrayList<String>();
		try {
			Resource pimResource = resourceResolver.getResource(PMDUtil.getPimPath(baseSku, locale.getCountry().toString()));
			Iterator<Resource> iterator = pimResource.listChildren();
			while (iterator.hasNext()) {
				Node variant = iterator.next().adaptTo(Node.class);
				// If the child is a variant add the child to the list.
				if (variant.getProperty("cq:commerceType").getString().equals("variant")) {
					// lookup the variant in the pmd and check to see if it is
					// default. If it is default add it to the beginning of the
					// list
					String variantSku = variant.getProperty("identifier").getString();
					variants.add(variantSku);
				}
			}

		} catch (Exception e) {
			System.out.println(e.getStackTrace());
		}
		if (variants.size() == 0) {
			variants = null;
		}
		return variants;
	}

	@SuppressWarnings("deprecation")
	@Override
	public List<ProductDetail> getProductDetail(String skuIdentifier, Locale locale) {
		LOGGER.info("Entering ProductDetailServiceImpl:getProductDetail");
		List<ProductDetail> productDetail = new ArrayList<ProductDetail>();
		try {
			resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);
			session = resourceResolver.adaptTo(Session.class);
			Resource pmdResource, pimResource;
			String localeStr = locale.toString();

			// Check to see if pmd page exists with full locale first.
			pmdResource = resourceResolver.getResource(PMDUtil.getPmdPath(skuIdentifier) + "/" + localeStr + productContainerPath);
			if (pmdResource == null) {
				// full locale pmd node doesn't exist so check to see if it
				// exists with just a language.
				localeStr = locale.getLanguage();
				pmdResource = resourceResolver.getResource(PMDUtil.getPmdPath(skuIdentifier) + "/" + localeStr + productContainerPath);
			}

			if (pmdResource != null) {
				// Get the baseSku from the pmdPage so we can build the pimPath
				// correctly
				String baseSku = pmdResource.getValueMap().get("baseSku", String.class);
				if (baseSku == null || baseSku.equals("")) {
					// There is no base sku so we can get the pimNode using the
					// skuIdentifier
					pimResource = resourceResolver.getResource(PMDUtil.getPimPath(skuIdentifier, locale.getCountry()));
				} else {
					pimResource = resourceResolver.getResource(PMDUtil.getPimPath(baseSku, locale.getCountry())+ "/" + skuIdentifier);
				}
				
				productDetail = populateProductDetail(skuIdentifier, locale, pmdResource, pimResource);
			}
		} catch (Exception e) {
			LOGGER.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}
		LOGGER.info("Exiting ProductDetailServiceImpl:getProductDetail");
		return productDetail;
	}

	/**
	 * Populates the product detail list.
	 *
	 * @param skuIdentifier the sku identifier
	 * @param locale the locale
	 * @param pmdResource the pmd resource
	 * @param pimResource the pim resource
	 * @return the list
	 */
	public List<ProductDetail> populateProductDetail(String skuIdentifier, Locale locale, Resource pmdResource, Resource pimResource) {

		List<ProductDetail> productList = new ArrayList<ProductDetail>();
		ProductDetail prodDetail = new ProductDetail();
		try {
			prodDetail.setIdentifier(skuIdentifier);
			
			List<String> variants = new ArrayList<String>();
			if(pimResource!=null){
				Node pimNode = pimResource.adaptTo(Node.class);
				if(pimNode.hasProperty("cq:commerceType") && pimNode.getProperty("cq:commerceType").getString().equals("product")){
					variants = getVariantList(skuIdentifier, locale);
				}
				
				prodDetail.setIdentifier(pimNode.getProperty("identifier").getString());
			}
			
			Node pmdNode = pmdResource.adaptTo(Node.class);

			if (pmdResource != null) {
				
				if(StringUtils.isNotEmpty("baseSku")){
					prodDetail.setBaseProductIdentifier(pmdNode.getProperty("baseSku").getString());
				}
				
				if (pmdNode.hasNode("productDetail")) {
					Node pmdDetailNode = pmdNode.getNode("productDetail");
					if(pmdDetailNode.hasProperty("baseSku") && StringUtils.isNotEmpty(pmdNode.getProperty("baseSku").getString())){
						prodDetail.setCommerceType(ProductDetailConstant.VARIANT);
					}
					if (pmdDetailNode.hasProperty(ProductDetailConstant.PROPERTY_JCR_TITLE)) {
						prodDetail.setJcrTitle(pmdDetailNode.getProperty(ProductDetailConstant.PROPERTY_JCR_TITLE).getString());
					}
					if (pmdDetailNode.hasProperty(ProductDetailConstant.PROPERTY_DESCRIPTION)) {
						prodDetail.setDescription(pmdDetailNode.getProperty(ProductDetailConstant.PROPERTY_DESCRIPTION).getString());
					}
					if (pmdDetailNode.hasProperty(ProductDetailConstant.PROPERTY_BENEFITS)) {
						prodDetail.setBenefits(setMultiProperty(pmdDetailNode.getProperty(ProductDetailConstant.PROPERTY_BENEFITS)));
					}
					if (pmdDetailNode.hasProperty(ProductDetailConstant.PROPERTY_PURCHASABLE)) {
						prodDetail.setPurchasable(pmdDetailNode.getProperty(ProductDetailConstant.PROPERTY_PURCHASABLE).getBoolean());
					}
					if (pmdDetailNode.hasProperty(ProductDetailConstant.PROPERTY_COMMERCE_TYPE)) {
						prodDetail.setCommerceType(pmdDetailNode.getProperty(ProductDetailConstant.PROPERTY_COMMERCE_TYPE).getString());
					}
					if (pmdDetailNode.hasProperty(ProductDetailConstant.PROPERTY_SIZE)) {
						prodDetail.setSize(pmdDetailNode.getProperty(ProductDetailConstant.PROPERTY_SIZE).getString());
					} else{
						prodDetail.setSize("");
					}
				} else {
					Node pmdDetailNode = pmdNode.addNode("productDetail");
					pmdDetailNode.setProperty("sling:resourceType", "dtc/commerce/components/content/productDetail");
					if(pimResource != null) {
						Node pimNode = pimResource.adaptTo(Node.class);
						if (pimNode.hasProperty(ProductDetailConstant.PROPERTY_JCR_TITLE)) {
							pmdDetailNode.setProperty(ProductDetailConstant.PROPERTY_JCR_TITLE, pimNode.getProperty(ProductDetailConstant.PROPERTY_JCR_TITLE).getString());
							prodDetail.setJcrTitle(pimNode.getProperty(ProductDetailConstant.PROPERTY_JCR_TITLE).getString());
						}
						if (pimNode.hasProperty(ProductDetailConstant.PROPERTY_DESCRIPTION)) {
							pmdDetailNode.setProperty(ProductDetailConstant.PROPERTY_DESCRIPTION, pimNode.getProperty(ProductDetailConstant.PROPERTY_DESCRIPTION).getString());
							prodDetail.setDescription(pimNode.getProperty(ProductDetailConstant.PROPERTY_DESCRIPTION).getString());
						}
						if (pimNode.hasProperty(ProductDetailConstant.PROPERTY_BENEFITS)) {
							pmdDetailNode.setProperty(ProductDetailConstant.PROPERTY_BENEFITS, pimNode.getProperty(ProductDetailConstant.PROPERTY_BENEFITS).getValues());
							prodDetail.setBenefits(setMultiProperty(pimNode.getProperty(ProductDetailConstant.PROPERTY_BENEFITS)));
						}
						if (pimNode.hasProperty("categories")) {
							pmdDetailNode.setProperty("categories", pimNode.getProperty("categories").getValues());
						}
						if (pimNode.hasProperty("searchTerms")) {
							pmdDetailNode.setProperty("searchTerms", pimNode.getProperty("searchTerms").getValues());
						}
						if (pimNode.hasProperty(ProductDetailConstant.PROPERTY_PURCHASABLE)) {
							pmdDetailNode.setProperty(ProductDetailConstant.PROPERTY_PURCHASABLE, pimNode.getProperty(ProductDetailConstant.PROPERTY_PURCHASABLE).getBoolean());
							prodDetail.setPurchasable(pimNode.getProperty(ProductDetailConstant.PROPERTY_PURCHASABLE).getBoolean());
						}
						if (pimNode.hasProperty(ProductDetailConstant.PROPERTY_COMMERCE_TYPE)) {
							pmdDetailNode.setProperty(ProductDetailConstant.PROPERTY_COMMERCE_TYPE, pimNode.getProperty(ProductDetailConstant.PROPERTY_COMMERCE_TYPE).getString());
							prodDetail.setCommerceType(pimNode.getProperty(ProductDetailConstant.PROPERTY_COMMERCE_TYPE).getString());
						}
						if (pimNode.hasProperty(ProductDetailConstant.PROPERTY_SIZE)) {
							pmdDetailNode.setProperty(ProductDetailConstant.PROPERTY_SIZE, pimNode.getProperty(ProductDetailConstant.PROPERTY_SIZE).getString());
							prodDetail.setSize(pimNode.getProperty(ProductDetailConstant.PROPERTY_SIZE).getString());
						} else {
							prodDetail.setSize("");
						}
					}
				}
				
				if(pmdNode.hasNode("pdpProductUsage")){
					Node pmdUsageNode = pmdNode.getNode("pdpProductUsage");
					
					setUsage(prodDetail, pmdUsageNode, null);
				}
				else{
					Node pmdUsageNode = pmdNode.addNode("pdpProductUsage");
					pmdUsageNode.setProperty("sling:resourceType", "dtc/commerce/components/content/pdpProductUsage");
					if(pimResource != null) {
						Node pimNode = pimResource.adaptTo(Node.class);
						
						setUsage(prodDetail, pimNode, pmdUsageNode);
					}
				}
				if(pmdNode.hasNode("pdpProductResources")){
					Node pmdResourcesNode = pmdNode.getNode("pdpProductResources");
					generatePdpResources(prodDetail, pmdResourcesNode, null);
				}
				else{
					Node pmdResourcesNode = pmdNode.addNode("pdpProductResources");
					pmdResourcesNode.setProperty("sling:resourceType", "dtc/commerce/components/content/productResources");
					
					if(pimResource != null) {
						Node pimNode = pimResource.adaptTo(Node.class);
						generatePdpResources(prodDetail, pimNode, pmdResourcesNode);
					}
				}
				if(pmdNode.hasNode("pdpCrossSell")){
					Node pmdCrossSellNode = pmdNode.getNode("pdpCrossSell");
					
					if (pmdCrossSellNode.hasProperty(ProductDetailConstant.PROPERTY_CROSS_SELL)) {
						prodDetail.setCrossSell(setMultiProperty(pmdCrossSellNode.getProperty(ProductDetailConstant.PROPERTY_CROSS_SELL)));
					}
				}else{
					if(pimResource != null) {
						Node pimNode = pimResource.adaptTo(Node.class);
						
						if (pimNode.hasProperty(ProductDetailConstant.PROPERTY_CROSS_SELL)) {
							prodDetail.setCrossSell(setMultiProperty(pimNode.getProperty(ProductDetailConstant.PROPERTY_CROSS_SELL)));
							
							Node pmdCrossSellNode = pmdNode.addNode("pdpCrossSell");
							pmdCrossSellNode.setProperty("sling:resourceType", "dtc/commerce/components/content/pdpCrossSell");
							pmdCrossSellNode.setProperty(ProductDetailConstant.PROPERTY_CROSS_SELL, pimNode.getProperty(ProductDetailConstant.PROPERTY_CROSS_SELL).getValues());
						}
					}
				}
				if(pmdNode.hasNode("pdpProductSupplements")){
					
					Node pmdIngredientsNode = pmdNode.getNode("pdpProductSupplements");
					
					prodDetail = setProductIngredients(prodDetail, pmdIngredientsNode, null);
					
				}else{
					Node pmdIngredientsNode = pmdNode.addNode("pdpProductSupplements");
					pmdIngredientsNode.setProperty("sling:resourceType", "dtc/commerce/components/content/pdpProductSupplements");
					if(pimResource != null){
						Node pimNode = pimResource.adaptTo(Node.class);
						
						prodDetail = setProductIngredients(prodDetail, pimNode, pmdIngredientsNode);
					}
				}
			}
			
			if (variants != null && !(variants.isEmpty())) {
				List<VariantDetail> variantList = new ArrayList<VariantDetail>();
				for(String variant : variants){
					String localeStr = locale.toString();
					//Check if full locale pmd node exists
					pmdResource = resourceResolver.getResource(PMDUtil.getPmdPath(variant) + "/" + localeStr + productContainerPath);
					if (pmdResource == null) {
						// full locale pmd node doesn't exist so check to see if it exists with just a language.
						localeStr = locale.getLanguage();
						pmdResource = resourceResolver.getResource(PMDUtil.getPmdPath(variant) + "/" + localeStr + productContainerPath);
					}
					if (pmdResource != null) {
						// Get the baseSku from the pmdPage so we can build the pimPath correctly
						String baseSku = pmdResource.getValueMap().get("baseSku", String.class);
						pimResource = resourceResolver.getResource(PMDUtil.getPimPath(baseSku, locale.getCountry())+ "/" + variant);
					}
					populateVariantData(variantList, variant, pmdResource, pimResource);
				}
				if(variantList.size() != 0){
					prodDetail.setVariants(variantList);
				}
			}
			productList.add(prodDetail);
			session.save();
		} catch (Exception e) {
			LOGGER.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}
		return productList;
	}
	
	/**
	 * Populate variant data.
	 *
	 * @param variantList the variant list
	 * @param variant the variant
	 * @param pmdResource the pmd resource
	 * @param pimResource the pim resource
	 */
	public void populateVariantData(List<VariantDetail> variantList, String variant, Resource pmdResource, Resource pimResource){
		
		try{
			VariantDetail variantDetail = new VariantDetail();
			
			Node pmdNode = pmdResource.adaptTo(Node.class);
			
			variantDetail.setIdentifier(variant);
			
			if (pmdResource != null) {
				
				if (pmdNode.hasNode("productDetail")) {
					
						Node pmdDetailNode = pmdNode.getNode("productDetail");
					
						if (pmdDetailNode.hasProperty(ProductDetailConstant.PROPERTY_JCR_TITLE)) {
							variantDetail.setJcrTitle(pmdDetailNode.getProperty(ProductDetailConstant.PROPERTY_JCR_TITLE).getString());
						}
						if (pmdDetailNode.hasProperty(ProductDetailConstant.PRODUCT_SWATCH_COLOR)) {
							variantDetail.setSwatchColor(pmdDetailNode.getProperty(ProductDetailConstant.PRODUCT_SWATCH_COLOR).getString());
						}
						if (pmdDetailNode.hasProperty(ProductDetailConstant.PROPERTY_MEC_VARIANT_TYPE)) {
							variantDetail.setMecVariantType(pmdDetailNode.getProperty(ProductDetailConstant.PROPERTY_MEC_VARIANT_TYPE).getString());
						}
						if (pmdDetailNode.hasProperty(ProductDetailConstant.PROPERTY_VARIANT_VALUE)) {
							variantDetail.setMecVariantValue(pmdDetailNode.getProperty(ProductDetailConstant.PROPERTY_MEC_VARIANT_VALUE).getString());
						}
						if (pmdDetailNode.hasProperty(ProductDetailConstant.PROPERTY_PURCHASABLE)) {
							variantDetail.setPurchasable(pmdDetailNode.getProperty(ProductDetailConstant.PROPERTY_PURCHASABLE).getBoolean());
						}
						if (pmdDetailNode.hasProperty(ProductDetailConstant.PROPERTY_COMMERCE_TYPE)) {
							variantDetail.setCommerceType(pmdDetailNode.getProperty(ProductDetailConstant.PROPERTY_COMMERCE_TYPE).getString());
						}
						if (pmdDetailNode.hasProperty(ProductDetailConstant.PROPERTY_SIZE)) {
							variantDetail.setSize(pmdDetailNode.getProperty(ProductDetailConstant.PROPERTY_SIZE).getString());
						} else {
							variantDetail.setSize("");
						}
				}else {
					Node pmdDetailNode = pmdNode.addNode("productDetail");
					pmdDetailNode.setProperty("sling:resourceType", "dtc/commerce/components/content/productDetail");
					if(pimResource != null){
						Node pimNode = pimResource.adaptTo(Node.class);
						
						if (pimNode.hasProperty(ProductDetailConstant.PROPERTY_JCR_TITLE)) {
							pmdDetailNode.setProperty(ProductDetailConstant.PROPERTY_JCR_TITLE, pimNode.getProperty(ProductDetailConstant.PROPERTY_JCR_TITLE).getString());
							variantDetail.setJcrTitle(pimNode.getProperty(ProductDetailConstant.PROPERTY_JCR_TITLE).getString());
						}
						if (pimNode.hasProperty(ProductDetailConstant.PRODUCT_SWATCH_COLOR)) {
							pmdDetailNode.setProperty(ProductDetailConstant.PRODUCT_SWATCH_COLOR, pimNode.getProperty(ProductDetailConstant.PRODUCT_SWATCH_COLOR).getString());
							variantDetail.setSwatchColor(pimNode.getProperty(ProductDetailConstant.PRODUCT_SWATCH_COLOR).getString());
						}
						if (pimNode.hasProperty(ProductDetailConstant.PROPERTY_MEC_VARIANT_TYPE)) {
							pmdDetailNode.setProperty(ProductDetailConstant.PROPERTY_MEC_VARIANT_TYPE, pimNode.getProperty(ProductDetailConstant.PROPERTY_MEC_VARIANT_TYPE).getString());
							variantDetail.setMecVariantType(pimNode.getProperty(ProductDetailConstant.PROPERTY_MEC_VARIANT_TYPE).getString());
						}
						if (pimNode.hasProperty(ProductDetailConstant.PROPERTY_VARIANT_VALUE)) {
							pmdDetailNode.setProperty(ProductDetailConstant.PROPERTY_MEC_VARIANT_VALUE, pimNode.getProperty(ProductDetailConstant.PROPERTY_MEC_VARIANT_VALUE).getString());
							variantDetail.setMecVariantValue(pimNode.getProperty(ProductDetailConstant.PROPERTY_MEC_VARIANT_VALUE).getString());
						}
						if (pimNode.hasProperty(ProductDetailConstant.PROPERTY_PURCHASABLE)) {
							pmdDetailNode.setProperty(ProductDetailConstant.PROPERTY_PURCHASABLE, pimNode.getProperty(ProductDetailConstant.PROPERTY_PURCHASABLE).getBoolean());
							variantDetail.setPurchasable(pimNode.getProperty(ProductDetailConstant.PROPERTY_PURCHASABLE).getBoolean());
						}
						if (pimNode.hasProperty(ProductDetailConstant.PROPERTY_COMMERCE_TYPE)) {
							pmdDetailNode.setProperty(ProductDetailConstant.PROPERTY_COMMERCE_TYPE, pimNode.getProperty(ProductDetailConstant.PROPERTY_COMMERCE_TYPE).getString());
							variantDetail.setCommerceType(pimNode.getProperty(ProductDetailConstant.PROPERTY_COMMERCE_TYPE).getString());
						}
						if (pimNode.hasProperty(ProductDetailConstant.PROPERTY_SIZE)) {
							pmdDetailNode.setProperty(ProductDetailConstant.PROPERTY_SIZE, pimNode.getProperty(ProductDetailConstant.PROPERTY_SIZE).getString());
							variantDetail.setSize(pimNode.getProperty(ProductDetailConstant.PROPERTY_SIZE).getString());
						} else {
							variantDetail.setSize("");
						}
					}
				}
				if(pmdNode.hasNode("pdpProductSupplements")){
					
					Node pmdIngredientsNode = pmdNode.getNode("pdpProductSupplements");
					
					variantDetail = setIngredients(variantDetail, pmdIngredientsNode, null);
					
				}else{
					Node pmdIngredientsNode = pmdNode.addNode("pdpProductSupplements");
					pmdIngredientsNode.setProperty("sling:resourceType", "dtc/commerce/components/content/pdpProductSupplements");
					if(pimResource != null){
						Node pimNode = pimResource.adaptTo(Node.class);
						
						variantDetail = setIngredients(variantDetail, pimNode, pmdIngredientsNode);
					}
				}
				if(pmdNode.hasNode("pdpCrossSell")){
					Node pmdCrossSellNode = pmdNode.getNode("pdpCrossSell");
					
					if (pmdCrossSellNode.hasProperty(ProductDetailConstant.PROPERTY_CROSS_SELL)) {
						variantDetail.setCrossSell(setMultiProperty(pmdCrossSellNode.getProperty(ProductDetailConstant.PROPERTY_CROSS_SELL)));
					}
				}else{
					if(pimResource != null) {
						Node pimNode = pimResource.adaptTo(Node.class);
						
						if (pimNode.hasProperty(ProductDetailConstant.PROPERTY_CROSS_SELL)) {
							variantDetail.setCrossSell(setMultiProperty(pimNode.getProperty(ProductDetailConstant.PROPERTY_CROSS_SELL)));
							
							Node pmdCrossSellNode = pmdNode.addNode("pdpCrossSell");
							pmdCrossSellNode.setProperty("sling:resourceType", "dtc/commerce/components/content/pdpCrossSell");
							pmdCrossSellNode.setProperty(ProductDetailConstant.PROPERTY_CROSS_SELL, pimNode.getProperty(ProductDetailConstant.PROPERTY_CROSS_SELL).getValues());
						}
					}
				}
				if(pmdNode.hasNode("pdpUpSell")){
					Node pmdUpSellNode = pmdNode.getNode("pdpUpSell");
					
					if (pmdUpSellNode.hasProperty(ProductDetailConstant.PROPERTY_UP_SELL)) {
						variantDetail.setUpSell(pmdUpSellNode.getProperty(ProductDetailConstant.PROPERTY_UP_SELL).getString());
					} 
				}else{
					if(pimResource != null) {
						Node pimNode = pimResource.adaptTo(Node.class);
						
						if (pimNode.hasProperty(ProductDetailConstant.PROPERTY_UP_SELL)) {
							variantDetail.setUpSell(pimNode.getProperty(ProductDetailConstant.PROPERTY_UP_SELL).getString());
							
							Node pmdCrossSellNode = pmdNode.addNode("pdpUpSell");
							pmdCrossSellNode.setProperty("sling:resourceType", "dtc/commerce/components/content/pdpUpSell");
							pmdCrossSellNode.setProperty(ProductDetailConstant.PROPERTY_UP_SELL, pimNode.getProperty(ProductDetailConstant.PROPERTY_UP_SELL).getString());
						}
					}
				}
				if(pmdNode.hasNode("pdpUpSellKit")){
					Node pmdUpSellNode = pmdNode.getNode("pdpUpSellKit");
					
					if (pmdUpSellNode.hasProperty("upSellKit")) {
						variantDetail.setUpSell(pmdUpSellNode.getProperty("upSellKit").getString());
					} 
				}else{
					if(pimResource != null) {
						Node pimNode = pimResource.adaptTo(Node.class);
						
						if (pimNode.hasProperty("upSellKit")) {
							variantDetail.setUpSell(pimNode.getProperty("upSellKit").getString());
							
							Node pmdCrossSellNode = pmdNode.addNode("pdpUpSellKit");
							pmdCrossSellNode.setProperty("sling:resourceType", "dtc/commerce/components/content/pdpUpSellKit");
							pmdCrossSellNode.setProperty("upSellKit", pimNode.getProperty("upSellKit").getString());
						}
					}
				}
			}
			variantList.add(variantDetail);
		}
		catch(Exception e){
			LOGGER.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}		
	}
	
	/**
	 * Sets the ingredients.
	 *
	 * @param variantDetail the variant detail
	 * @param sourceNode the source node
	 * @param destinationNode the destination node
	 * @return the variant detail
	 */
	public VariantDetail setIngredients(VariantDetail variantDetail, Node sourceNode, Node destinationNode){
		try{
			if (sourceNode.hasProperty(ProductDetailConstant.SUPPLEMENT_IMG)) {
				variantDetail.setSuplementImg(sourceNode.getProperty(ProductDetailConstant.SUPPLEMENT_IMG).getString());
				if(null != destinationNode){
					destinationNode.setProperty(ProductDetailConstant.SUPPLEMENT_IMG, sourceNode.getProperty(ProductDetailConstant.SUPPLEMENT_IMG).getString());
				}
			}
			if (sourceNode.hasProperty(ProductDetailConstant.KEY_INGREDIENTS)) {
				variantDetail.setKeyIng(setMultiPropertyKeyIng(sourceNode.getProperty(ProductDetailConstant.KEY_INGREDIENTS)));
				if(null != destinationNode){
					destinationNode.setProperty(ProductDetailConstant.KEY_INGREDIENTS, sourceNode.getProperty(ProductDetailConstant.KEY_INGREDIENTS).getValues());
				}
			}
			if (sourceNode.hasProperty(ProductDetailConstant.SERVINGS)) {
				variantDetail.setServings(setMultiProperty(sourceNode.getProperty(ProductDetailConstant.SERVINGS)));
				if(null != destinationNode){
					destinationNode.setProperty(ProductDetailConstant.SERVINGS, sourceNode.getProperty(ProductDetailConstant.SERVINGS).getValues());
				}
			}
			if (sourceNode.hasProperty(ProductDetailConstant.SPECIAL_INGREDIENTS)) {
				variantDetail.setSpecialIng(setMultiPropertySplIng(sourceNode.getProperty(ProductDetailConstant.SPECIAL_INGREDIENTS)));
				if(null != destinationNode){
					destinationNode.setProperty(ProductDetailConstant.SPECIAL_INGREDIENTS, sourceNode.getProperty(ProductDetailConstant.SPECIAL_INGREDIENTS).getValues());
				}
			}
			if (sourceNode.hasProperty(ProductDetailConstant.OTHER_INGREDIENTS)) {
				variantDetail.setOtherIng(sourceNode.getProperty(ProductDetailConstant.OTHER_INGREDIENTS).getString());
				if(null != destinationNode){
					destinationNode.setProperty(ProductDetailConstant.OTHER_INGREDIENTS, sourceNode.getProperty(ProductDetailConstant.OTHER_INGREDIENTS).getString());
				}
			}
			if (sourceNode.hasProperty(ProductDetailConstant.NUTRITION_NOTE)) {
				variantDetail.setNutritionNote(sourceNode.getProperty(ProductDetailConstant.NUTRITION_NOTE).getString());
				if(null != destinationNode){
					destinationNode.setProperty(ProductDetailConstant.NUTRITION_NOTE, sourceNode.getProperty(ProductDetailConstant.NUTRITION_NOTE).getString());
				}
			}
			if (sourceNode.hasProperty(ProductDetailConstant.VITAMINS)) {
				variantDetail.setVitamins(sourceNode.getProperty(ProductDetailConstant.VITAMINS).getString());
				if(null != destinationNode){
					destinationNode.setProperty(ProductDetailConstant.VITAMINS, sourceNode.getProperty(ProductDetailConstant.VITAMINS).getString());
				}
			}
		}
		catch(Exception e){
			LOGGER.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}
		return variantDetail;
	}
	
	/**
	 * Sets the product ingredients.
	 *
	 * @param proDetail the pro detail
	 * @param sourceNode the source node
	 * @param destinationNode the destination node
	 * @return the product detail
	 */
	public ProductDetail setProductIngredients(ProductDetail proDetail, Node sourceNode, Node destinationNode){
		try{
			if (sourceNode.hasProperty(ProductDetailConstant.SUPPLEMENT_IMG)) {
				proDetail.setSuplementImg(sourceNode.getProperty(ProductDetailConstant.SUPPLEMENT_IMG).getString());
				if(null != destinationNode){
					destinationNode.setProperty(ProductDetailConstant.SUPPLEMENT_IMG, sourceNode.getProperty(ProductDetailConstant.SUPPLEMENT_IMG).getString());
				}
			}
			if (sourceNode.hasProperty(ProductDetailConstant.KEY_INGREDIENTS)) {
				proDetail.setKeyIng(setMultiPropertyKeyIng(sourceNode.getProperty(ProductDetailConstant.KEY_INGREDIENTS)));
				if(null != destinationNode){
					destinationNode.setProperty(ProductDetailConstant.KEY_INGREDIENTS, sourceNode.getProperty(ProductDetailConstant.KEY_INGREDIENTS).getValues());
				}
			}
			if (sourceNode.hasProperty(ProductDetailConstant.SERVINGS)) {
				proDetail.setServings(setMultiProperty(sourceNode.getProperty(ProductDetailConstant.SERVINGS)));
				if(null != destinationNode){
					destinationNode.setProperty(ProductDetailConstant.SERVINGS, sourceNode.getProperty(ProductDetailConstant.SERVINGS).getValues());
				}
			}
			if (sourceNode.hasProperty(ProductDetailConstant.SPECIAL_INGREDIENTS)) {
				proDetail.setSpecialIng(setMultiPropertySplIng(sourceNode.getProperty(ProductDetailConstant.SPECIAL_INGREDIENTS)));
				if(null != destinationNode){
					destinationNode.setProperty(ProductDetailConstant.SPECIAL_INGREDIENTS, sourceNode.getProperty(ProductDetailConstant.SPECIAL_INGREDIENTS).getValues());
				}
			}
			if (sourceNode.hasProperty(ProductDetailConstant.OTHER_INGREDIENTS)) {
				proDetail.setOtherIng(sourceNode.getProperty(ProductDetailConstant.OTHER_INGREDIENTS).getString());
				if(null != destinationNode){
					destinationNode.setProperty(ProductDetailConstant.OTHER_INGREDIENTS, sourceNode.getProperty(ProductDetailConstant.OTHER_INGREDIENTS).getString());
				}
			}
			if (sourceNode.hasProperty(ProductDetailConstant.NUTRITION_NOTE)) {
				proDetail.setNutritionNote(sourceNode.getProperty(ProductDetailConstant.NUTRITION_NOTE).getString());
				if(null != destinationNode){
					destinationNode.setProperty(ProductDetailConstant.NUTRITION_NOTE, sourceNode.getProperty(ProductDetailConstant.NUTRITION_NOTE).getString());
				}
			}
			if (sourceNode.hasProperty(ProductDetailConstant.VITAMINS)) {
				proDetail.setVitamins(sourceNode.getProperty(ProductDetailConstant.VITAMINS).getString());
				if(null != destinationNode){
					destinationNode.setProperty(ProductDetailConstant.VITAMINS, sourceNode.getProperty(ProductDetailConstant.VITAMINS).getString());
				}
			}
		}
		catch(Exception e){
			LOGGER.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}
		return proDetail;
	}
	
	/**
	 * Generate pdp resources.
	 *
	 * @param prodDetail the prod detail
	 * @param sourceNode the source node
	 * @param destinationNode the destination node
	 */
	public void generatePdpResources(ProductDetail prodDetail, Node sourceNode, Node destinationNode){
		try{
			
			List<String> marketingList = new ArrayList<String>();
			
			setResources(marketingList, sourceNode, destinationNode, ProductDetailConstant.PROPERTY_MARKETING_MATERIAL_ONE, ProductDetailConstant.PROPERTY_MARKETING_MATERIAL_ONE_USER_TYPE, ProductDetailConstant.PROPERTY_MARKETING_MATERIAL_ONE_IMAGE_SRC);
			setResources(marketingList, sourceNode, destinationNode, ProductDetailConstant.PROPERTY_MARKETING_MATERIAL_TWO, ProductDetailConstant.PROPERTY_MARKETING_MATERIAL_TWO_USER_TYPE, ProductDetailConstant.PROPERTY_MARKETING_MATERIAL_TWO_IMAGE_SRC);
			setResources(marketingList, sourceNode, destinationNode, ProductDetailConstant.PROPERTY_MARKETING_MATERIAL_THREE, ProductDetailConstant.PROPERTY_MARKETING_MATERIAL_THREE_USER_TYPE, ProductDetailConstant.PROPERTY_MARKETING_MATERIAL_THREE_IMAGE_SRC);
			
			prodDetail.setMarketingMaterials(marketingList);
			
			List<String> productInformation = new ArrayList<String>();
			
			setResources(productInformation, sourceNode, destinationNode, ProductDetailConstant.PROPERTY_PRODUCT_INFORMATION_ONE, ProductDetailConstant.PROPERTY_PRODUCT_INFORMATION_ONE_USER_TYPE, ProductDetailConstant.PROPERTY_PRODUCT_INFORMATION_ONE_IMAGE_SRC);
			setResources(productInformation, sourceNode, destinationNode, ProductDetailConstant.PROPERTY_PRODUCT_INFORMATION_TWO, ProductDetailConstant.PROPERTY_PRODUCT_INFORMATION_TWO_USER_TYPE, ProductDetailConstant.PROPERTY_PRODUCT_INFORMATION_TWO_IMAGE_SRC);
			setResources(productInformation, sourceNode, destinationNode, ProductDetailConstant.PROPERTY_PRODUCT_INFORMATION_THREE, ProductDetailConstant.PROPERTY_PRODUCT_INFORMATION_THREE_USER_TYPE, ProductDetailConstant.PROPERTY_PRODUCT_INFORMATION_THREE_IMAGE_SRC);
			
			prodDetail.setProductInformation(productInformation);
			
			List<String> otherSource = new ArrayList<String>();
			
			setResources(otherSource, sourceNode, destinationNode, ProductDetailConstant.PROPERTY_OTHER_SOURCE_ONE, ProductDetailConstant.PROPERTY_OTHER_SOURCE_ONE_USER_TYPE, ProductDetailConstant.PROPERTY_OTHER_SOURCE_ONE_IMAGE_SRC);
			setResources(otherSource, sourceNode, destinationNode, ProductDetailConstant.PROPERTY_OTHER_SOURCE_TWO, ProductDetailConstant.PROPERTY_OTHER_SOURCE_TWO_USER_TYPE, ProductDetailConstant.PROPERTY_OTHER_SOURCE_TWO_IMAGE_SRC);
			setResources(otherSource, sourceNode, destinationNode, ProductDetailConstant.PROPERTY_OTHER_SOURCE_THREE, ProductDetailConstant.PROPERTY_OTHER_SOURCE_THREE_USER_TYPE, ProductDetailConstant.PROPERTY_OTHER_SOURCE_THREE_IMAGE_SRC);
			
			prodDetail.setOtherSource(otherSource);

		}
		catch(Exception e){
			LOGGER.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}
	}
	
	/**
	 * Sets the resources.
	 *
	 * @param resourceList the resource list
	 * @param sourceNode the source node
	 * @param destinationNode the destination node
	 * @param resource the resource
	 * @param resourceUserType the resource user type
	 * @param resourceImageSrc the resource image src
	 */
	public void setResources(List<String> resourceList, Node sourceNode, Node destinationNode, String resource, String resourceUserType, String resourceImageSrc){
		try{
			if (sourceNode.hasProperty(resource)) {
				resourceList.add(getJSON(
						sourceNode.getProperty(resource).getString(),
						sourceNode.getProperty(resourceUserType).getString(),
						sourceNode.hasProperty(resourceImageSrc) ?
								sourceNode.getProperty(resourceImageSrc).getString() : StringUtils.EMPTY));
				if(null != destinationNode){
					destinationNode.setProperty(resource, sourceNode.getProperty(resource).getString());
					destinationNode.setProperty(resourceUserType, sourceNode.getProperty(resourceUserType).getString());
					if (sourceNode.hasProperty(resourceImageSrc)){
						destinationNode.setProperty(resourceImageSrc, sourceNode.getProperty(resourceImageSrc).getString());
					}
				}
			}
		}
		catch(Exception e){
			LOGGER.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}
	}
	
	/**
	 * Sets the usage.
	 *
	 * @param prodDetail the prod detail
	 * @param sourceNode the source node
	 * @param destinationNode the destination node
	 */
	public void setUsage(ProductDetail prodDetail, Node sourceNode, Node destinationNode){
		try{
			if (sourceNode.hasProperty(ProductDetailConstant.PROPERTY_GENERAL_USAGE)) {
				prodDetail.setGeneralUsage(setMultiProperty(sourceNode.getProperty(ProductDetailConstant.PROPERTY_GENERAL_USAGE)));
				if(null != destinationNode){
					destinationNode.setProperty(ProductDetailConstant.PROPERTY_GENERAL_USAGE, sourceNode.getProperty(ProductDetailConstant.PROPERTY_GENERAL_USAGE).getValues());
				}
			}
			if (sourceNode.hasProperty(ProductDetailConstant.PROPERTY_MORNING_USAGE)) {
				prodDetail.setMorningUsage(setMultiProperty(sourceNode.getProperty(ProductDetailConstant.PROPERTY_MORNING_USAGE)));
				if(null != destinationNode){
					destinationNode.setProperty(ProductDetailConstant.PROPERTY_MORNING_USAGE, sourceNode.getProperty(ProductDetailConstant.PROPERTY_MORNING_USAGE).getValues());
				}
			}
			if (sourceNode.hasProperty(ProductDetailConstant.PROPERTY_NIGHT_USAGE)) {
				prodDetail.setNightUsage(setMultiProperty(sourceNode.getProperty(ProductDetailConstant.PROPERTY_NIGHT_USAGE)));
				if(null != destinationNode){
					destinationNode.setProperty(ProductDetailConstant.PROPERTY_NIGHT_USAGE, sourceNode.getProperty(ProductDetailConstant.PROPERTY_NIGHT_USAGE).getValues());
				}
			}
			if (sourceNode.hasProperty(ProductDetailConstant.PROPERTY_GRAPHIC_INFO)) {
				prodDetail.setGraphicInfo(sourceNode.getProperty(ProductDetailConstant.PROPERTY_GRAPHIC_INFO).getString());
				if(null != destinationNode){
					destinationNode.setProperty(ProductDetailConstant.PROPERTY_GRAPHIC_INFO, sourceNode.getProperty(ProductDetailConstant.PROPERTY_GRAPHIC_INFO).getString());
				}
			}
			if (sourceNode.hasProperty(ProductDetailConstant.PROPERTY_USAGE_DESC)) {
				prodDetail.setUsageDesc(sourceNode.getProperty(ProductDetailConstant.PROPERTY_USAGE_DESC).getString());
				if(null != destinationNode){
					destinationNode.setProperty(ProductDetailConstant.PROPERTY_USAGE_DESC, sourceNode.getProperty(ProductDetailConstant.PROPERTY_USAGE_DESC).getString());
				}
			}
			if (sourceNode.hasProperty(ProductDetailConstant.PROPERTY_USAGE_NOTE)) {
				prodDetail.setUsageNote(sourceNode.getProperty(ProductDetailConstant.PROPERTY_USAGE_NOTE).getString());
				if(null != destinationNode){
					destinationNode.setProperty(ProductDetailConstant.PROPERTY_USAGE_NOTE, sourceNode.getProperty(ProductDetailConstant.PROPERTY_USAGE_NOTE).getString());
				}
			}
			if (sourceNode.hasProperty(ProductDetailConstant.PROPERTY_VIDEO_DUR)) {
				prodDetail.setVideoDuration(sourceNode.getProperty(ProductDetailConstant.PROPERTY_VIDEO_DUR).getString());
				if(null != destinationNode){
					destinationNode.setProperty(ProductDetailConstant.PROPERTY_VIDEO_DUR, sourceNode.getProperty(ProductDetailConstant.PROPERTY_VIDEO_DUR).getString());
				}
			}
			if (sourceNode.hasProperty(ProductDetailConstant.PROPERTY_GRAPHIC_DESC)) {
				prodDetail.setGraphicDesc(sourceNode.getProperty(ProductDetailConstant.PROPERTY_GRAPHIC_DESC).getString());
				if(null != destinationNode){
					destinationNode.setProperty(ProductDetailConstant.PROPERTY_GRAPHIC_DESC, sourceNode.getProperty(ProductDetailConstant.PROPERTY_GRAPHIC_DESC).getString());
				}
			}
			
		}
		catch(Exception e){
			LOGGER.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}
	}

	/**
	 * Gets the json.
	 *
	 * @param file the file
	 * @param userType the user type
	 * @param imageSrc the image src
	 * @return the json
	 */
	private String getJSON(String file, String userType, String imageSrc) {
		LOGGER.info("Entering ProductDetailServiceImpl:getJSON");
		JSONObject resourcesJSON = new JSONObject();
		try {
			resourcesJSON.put("file", file);
			resourcesJSON.put("userType", userType);
			resourcesJSON.put("imageSrc", imageSrc);
			Resource damRes = resourceResolver.getResource(file);

			if (damRes != null) {
				Resource metadataResource = damRes.getChild("jcr:content/metadata");
				ValueMap properties = ResourceUtil.getValueMap(metadataResource);
				resourcesJSON.put("title", properties.get("dc:title"));
			}
		} catch (JSONException e) {
			LOGGER.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}

		LOGGER.info("Exiting ProductDetailServiceImpl:getJSON");
		return resourcesJSON.toString();
	}

	/**
	 * String setMultiProperty.
	 *
	 * @param propertyNode the property node
	 * @return the list
	 */
	public static List<String> setMultiProperty(Property propertyNode) {
		LOGGER.info("Entering ProductDetailServiceImpl:setMultiProperty");
		List<String> multiValue = new ArrayList<>();
		try {
			if (!propertyNode.isMultiple()) {
				Value val = propertyNode.getValue();
				multiValue.add(val.getString());

			} else {
				Value[] values = propertyNode.getValues();
				for (int i = 0; i < values.length; i++) {
					multiValue.add(values[i].getString());
				}
			}
		} catch (Exception e) {
			LOGGER.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}
		LOGGER.info("Exiting ProductDetailServiceImpl:setMultiProperty");
		return multiValue;
	}
	
	/**
	 * String setMultiPropertyKeyIng.
	 *
	 * @param propertyNode the property node
	 * @return KeyIngmultiValue
	 */
	public static List<KeyIngredients> setMultiPropertyKeyIng(Property propertyNode) {
		LOGGER.info("Entering ProductDetailServiceImpl:setMultiPropertyKeyIng");
		List<KeyIngredients> keyMultiValue = new ArrayList<>();
		try {

			if (!propertyNode.isMultiple()) {
				KeyIngredients keyIng = new KeyIngredients();
				Value val = propertyNode.getValue();
				keyIng = getKeyIngObj(keyIng, val.getString());
				keyMultiValue.add(keyIng);

			} else {
				Value[] values = propertyNode.getValues();
				KeyIngredients[] keyIng = new KeyIngredients[values.length];
				for (int i = 0; i < values.length; i++) {
					keyIng[i] = new KeyIngredients();
					keyIng[i] = getKeyIngObj(keyIng[i], values[i].getString());
					keyMultiValue.add(keyIng[i]);
				}
			}
		} catch (Exception e) {
			LOGGER.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}
		LOGGER.info("Exiting ProductDetailServiceImpl:setMultiPropertyKeyIng");
		return keyMultiValue;
	}
	
	/**
	 * get getKeyIngObj.
	 *
	 * @param ing the ing
	 * @param str the str
	 * @return the key ing obj
	 */
	@SuppressWarnings("null")
	public static KeyIngredients getKeyIngObj(KeyIngredients ing, String str) {
		LOGGER.info("Entering ProductDetailServiceImpl:getKeyIngObj");
		JSONObject keyIngJson;
		try {
			keyIngJson = new JSONObject(str);
			if (keyIngJson.has(ProductDetailConstant.KEY_INGREDIENTS_TAGS)) {
				String[] tagsArray = null;
				JSONArray tagsJsonArray = new JSONArray(keyIngJson.getString(ProductDetailConstant.KEY_INGREDIENTS_TAGS));
				 if(tagsJsonArray != null && tagsJsonArray.length() > 0){
					 tagsArray = new String[tagsJsonArray.length()];
					for (int i = 0; i < tagsJsonArray.length(); i++) {
						tagsArray[i] = (String) tagsJsonArray.get(i);
					}
				 }
				ing.setTitle(tagsArray);
			}
			if (keyIngJson.has(ProductDetailConstant.KEY_INGREDIENTS_DESC)) {
				ing.setDescription(keyIngJson.getString(ProductDetailConstant.KEY_INGREDIENTS_DESC));
			}
		} catch (Exception e) {
			LOGGER.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}
		LOGGER.info("Exiting ProductDetailServiceImpl:getKeyIngObj");
		return ing;
	}
	

	/**
	 * String setMultiPropertySplIng.
	 *
	 * @param propertyNode the property node
	 * @return the list
	 */
	public static List<SpecialIngredients> setMultiPropertySplIng(Property propertyNode) {
		LOGGER.info("Entering ProductDetailServiceImpl:setMultiPropertySplIng");
		List<SpecialIngredients> multiValue = new ArrayList<>();
		try {

			if (!propertyNode.isMultiple()) {
				SpecialIngredients splIng = new SpecialIngredients();
				Value val = propertyNode.getValue();
				splIng = getSplIngObj(splIng, val.getString());
				multiValue.add(splIng);

			} else {
				Value[] values = propertyNode.getValues();
				SpecialIngredients[] splIng = new SpecialIngredients[(values.length)];
				for (int i = 0; i < values.length; i++) {
					splIng[i] = new SpecialIngredients();
					splIng[i] = getSplIngObj(splIng[i], values[i].getString());
					multiValue.add(splIng[i]);
				}
			}
		} catch (Exception e) {
			LOGGER.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}
		LOGGER.info("Exiting ProductDetailServiceImpl:setMultiPropertySplIng");
		return multiValue;
	}
	
	@Override
	public JSONArray crossSellInformation(String sku, Locale locale){
		
		JSONArray recommendations = new JSONArray();
		try{
			resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);
			Resource pmdResource;
			String localeStr = locale.toString();
			
			String languageStr = locale.getLanguage();
			
			// Check to see if pmd page exists with full locale first.
			pmdResource = resourceResolver.getResource(PMDUtil.getPmdPath(sku) + "/" + localeStr + productContainerPath);
			if (pmdResource == null) {
				// full locale pmd node doesn't exist so check to see if it
				// exists with just a language.
				pmdResource = resourceResolver.getResource(PMDUtil.getPmdPath(sku) + "/" + languageStr + productContainerPath);
			}
			Node pmdNode = pmdResource.adaptTo(Node.class);
			
			if(pmdNode.hasNode("pdpCrossSell")){
				Node pmdCrossSellNode = pmdNode.getNode("pdpCrossSell");
				
				if (pmdCrossSellNode.hasProperty(ProductDetailConstant.PROPERTY_CROSS_SELL)) {
					
					List<String> crossSell = new ArrayList<String>();
					
					crossSell = setMultiProperty(pmdCrossSellNode.getProperty(ProductDetailConstant.PROPERTY_CROSS_SELL));
					
					for(String product : crossSell){
						
						StringBuilder variantURL = new StringBuilder();
						
						JSONObject reco = new JSONObject();
						
						product = product.substring(product.lastIndexOf("/")+1);
						
						Resource prodResource;
						
						prodResource = resourceResolver.getResource(PMDUtil.getPmdPath(product) + "/" + localeStr + productContainerPath);
						if (prodResource == null) {
							// full locale pmd node doesn't exist so check to see if it
							// exists with just a language.
							
							prodResource = resourceResolver.getResource(PMDUtil.getPmdPath(product) + "/" + languageStr + productContainerPath);
							
							Node prodNode = prodResource.adaptTo(Node.class);
							
							reco.put("baseid", product);
							reco.put("skuid", product);
							reco.put("name", prodNode.getNode("productDetail").getProperty(ProductDetailConstant.PROPERTY_JCR_TITLE).getString());
							reco.put("url", variantURL.append(ProductDetailConstant.DOT + product + ProductDetailConstant.HTML));
							reco.put("img", getImageforSku(product, productsRenditionService.getCrossSellRendition()));
							reco.put("shortDesc", prodNode.getNode("productDetail").hasProperty(ProductDetailConstant.PROPERTY_DESCRIPTION) ? prodNode.getNode("productDetail").getProperty(ProductDetailConstant.PROPERTY_DESCRIPTION).getString() : "");
						}
						
						String baseSku = prodResource.getValueMap().get("baseSku", String.class);
						if (StringUtils.isNotEmpty(baseSku)) {
							
							prodResource = resourceResolver.getResource(PMDUtil.getPmdPath(baseSku) + "/" + localeStr + productContainerPath);
							if (prodResource == null) {
								
								StringBuilder url = new StringBuilder();
								
								// full locale pmd node doesn't exist so check to see if it
								// exists with just a language.
								prodResource = resourceResolver.getResource(PMDUtil.getPmdPath(baseSku) + "/" + languageStr + productContainerPath);
								
								Node prodNode = prodResource.adaptTo(Node.class);
								
								reco.put("baseid", baseSku.toUpperCase());
								reco.put("skuid", product);
								reco.put("url", url.append(ProductDetailConstant.DOT + baseSku + ProductDetailConstant.DOT + product + ProductDetailConstant.HTML));
								reco.put("shortDesc", prodNode.getNode("productDetail").hasProperty(ProductDetailConstant.PROPERTY_DESCRIPTION) ? prodNode.getNode("productDetail").getProperty(ProductDetailConstant.PROPERTY_DESCRIPTION).getString() : "");
							}
						}
						recommendations.put(reco);
					}
				}
			}
		}
		catch(Exception e){
			LOGGER.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}
		return recommendations;
	}
	
	@Override
	public JSONArray upSellInformation(String sku, Locale locale){
		
		JSONArray upSellInfo = new JSONArray();
		try{
			resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);
			Resource pmdResource;
			String localeStr = locale.toString();
			
			String languageStr = locale.getLanguage();
			
			// Check to see if pmd page exists with full locale first.
			pmdResource = resourceResolver.getResource(PMDUtil.getPmdPath(sku) + "/" + localeStr + productContainerPath);
			if (pmdResource == null) {
				// full locale pmd node doesn't exist so check to see if it
				// exists with just a language.
				pmdResource = resourceResolver.getResource(PMDUtil.getPmdPath(sku) + "/" + languageStr + productContainerPath);
			}
			
			if(null != pmdResource){
				Node pmdNode = pmdResource.adaptTo(Node.class);
				
				if(pmdNode.hasNode("pdpUpSell")){
					
					JSONObject base = new JSONObject();
					
					base = getProductInformation(sku, locale);
					
					upSellInfo.put(base);
					
					String upsellProduct = pmdNode.getNode("pdpUpSell").getProperty(ProductDetailConstant.PROPERTY_UP_SELL).getString();
					
					upsellProduct = upsellProduct.substring(upsellProduct.lastIndexOf("/")+1);
					
					JSONObject upSellProdJSON = getProductInformation(upsellProduct, locale);
					
					upSellInfo.put(upSellProdJSON);
					
				}
			}
		}
		catch(Exception e){
			LOGGER.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}
		return upSellInfo;
	}
	
	@Override
	public JSONObject upSellKitInformation(String sku, Locale locale){
		
		JSONObject upSellKitInfo = new JSONObject();
		try{
			resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);
			Resource pmdResource;
			String localeStr = locale.toString();
			
			String languageStr = locale.getLanguage();
			
			// Check to see if pmd page exists with full locale first.
			pmdResource = resourceResolver.getResource(PMDUtil.getPmdPath(sku) + "/" + localeStr + productContainerPath);
			if (pmdResource == null) {
				// full locale pmd node doesn't exist so check to see if it
				// exists with just a language.
				pmdResource = resourceResolver.getResource(PMDUtil.getPmdPath(sku) + "/" + languageStr + productContainerPath);
			}
			
			if(null != pmdResource){
				Node pmdNode = pmdResource.adaptTo(Node.class);
				
				if(pmdNode.hasNode("pdpUpSellKit")){
					
					String upsellKitProduct = pmdNode.getNode("pdpUpSellKit").getProperty("upSellKit").getString();
					
					upsellKitProduct = upsellKitProduct.substring(upsellKitProduct.lastIndexOf("/")+1);
					
					upSellKitInfo = getProductInformation(upsellKitProduct, locale);
				}
			}
		}
		catch(Exception e){
			LOGGER.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}
		return upSellKitInfo;
	}
	
	/**
	 * Gets the product information.
	 *
	 * @param sku the sku
	 * @param locale the locale
	 * @return the product information
	 */
	public JSONObject getProductInformation(String sku, Locale locale){
		JSONObject object = new JSONObject();
		try{
			resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);
			Resource pmdResource;
			String localeStr = locale.toString();
			
			String languageStr = locale.getLanguage();
			
			// Check to see if pmd page exists with full locale first.
			pmdResource = resourceResolver.getResource(PMDUtil.getPmdPath(sku) + "/" + localeStr + productContainerPath);
			if (pmdResource == null) {
				// full locale pmd node doesn't exist so check to see if it
				// exists with just a language.
				pmdResource = resourceResolver.getResource(PMDUtil.getPmdPath(sku) + "/" + languageStr + productContainerPath);
			}
			
			if(null != pmdResource){
				Node pmdNode = pmdResource.adaptTo(Node.class);
				
				StringBuilder prodUrl = new StringBuilder();
				
				object.put("productName", pmdNode.getNode("productDetail").getProperty(ProductDetailConstant.PROPERTY_JCR_TITLE).getString());
				object.put("skuid", sku);
				object.put("imgProduct", getImageforSku(sku, productsRenditionService.getUpSellRendition()));
				object.put("url", prodUrl.append(ProductDetailConstant.DOT).append(sku).append(ProductDetailConstant.HTML));
				object.put("shortDescription", pmdNode.getNode("productDetail").hasProperty(ProductDetailConstant.PROPERTY_DESCRIPTION) ? pmdNode.getNode("productDetail").getProperty(ProductDetailConstant.PROPERTY_DESCRIPTION).getString() : "");
				object.put("type", "product");
				object.put("purchasable", pmdNode.getNode("productDetail").getProperty(ProductDetailConstant.PROPERTY_PURCHASABLE).getBoolean());
				object.put("countryCode", languageStr);
				
				String baseSku = pmdResource.getValueMap().get("baseSku", String.class);
				if (StringUtils.isNotEmpty(baseSku)) {
					pmdResource = resourceResolver.getResource(PMDUtil.getPmdPath(baseSku) + "/" + localeStr + productContainerPath);
					if (pmdResource == null) {
						// full locale pmd node doesn't exist so check to see if it
						// exists with just a language.
						pmdResource = resourceResolver.getResource(PMDUtil.getPmdPath(baseSku) + "/" + languageStr + productContainerPath);
						
						StringBuilder variantUrl = new StringBuilder();
						
						if(null != pmdResource){
							pmdNode = pmdResource.adaptTo(Node.class);
							object.put("type", "variant");
							object.put("url", variantUrl.append(ProductDetailConstant.DOT).append(baseSku).append(ProductDetailConstant.DOT).append(sku).append(ProductDetailConstant.HTML));
							object.put("shortDescription", pmdNode.getNode("productDetail").hasProperty(ProductDetailConstant.PROPERTY_DESCRIPTION) ? pmdNode.getNode("productDetail").getProperty(ProductDetailConstant.PROPERTY_DESCRIPTION).getString() : "");
						}
					}
				}
			}
		}
		catch(Exception e){
			LOGGER.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}
		return object;
	}
	
	/**
	 * Gets the imagefor sku.
	 *
	 * @param sku            the sku
	 * @param rendition the rendition
	 * @return the imagefor sku
	 * @throws RepositoryException             the repository exception
	 * @throws JSONException             the JSON exception
	 */
	private String getImageforSku(String sku, String rendition) throws RepositoryException, JSONException {
		LOGGER.info("Entering: CrossSellServlet :  getImageforSku");
		String imageforSku = "";
		try {
			String imagesPath = ProductDetailConstant.DAM_BASE_PATH + sku;
			
			if(null != resourceResolver.getResource(imagesPath)){
				Node imagesNodes = resourceResolver.getResource(imagesPath).adaptTo(Node.class);
				NodeIterator nodeIterator = imagesNodes.getNodes();

				while (nodeIterator.hasNext()) {
					Node imageNode = nodeIterator.nextNode();
					if (imageNode.getPrimaryNodeType().getName().equals(ProductDetailConstant.DAM_ASSET)){
						Rendition imageRenditions = resourceResolver.getResource(imageNode.getPath().toString()).adaptTo(Asset.class).getRendition(rendition);
		        		if(null !=  imageRenditions){
		        			imageforSku = imageRenditions.getPath().toString();
		        			break;
		        		}
					}
				}
			}
			
		} catch (Exception e) {
			LOGGER.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}
		LOGGER.info("Exiting: CrossSellServlet :  getImageforSku");
		return imageforSku;
	}

	/**
	 * get getSplIngObj.
	 *
	 * @param ing the ing
	 * @param str the str
	 * @return the spl ing obj
	 */
	public static SpecialIngredients getSplIngObj(SpecialIngredients ing, String str) {
		JSONObject speclIngJson;
		try {
			speclIngJson = new JSONObject(str);
			if (speclIngJson.has(ProductDetailConstant.SPECIAL_INGREDIENTS_NAME)) {
				ing.setIngredient(speclIngJson.getString(ProductDetailConstant.SPECIAL_INGREDIENTS_NAME));
			}
			if (speclIngJson.has(ProductDetailConstant.SPECIAL_INGREDIENTS_AMOUNT)) {
				ing.setAmount(speclIngJson.getString(ProductDetailConstant.SPECIAL_INGREDIENTS_AMOUNT));
			} 
			if (speclIngJson.has(ProductDetailConstant.SPECIAL_INGREDIENTS_VALUE)) {
				ing.setDailyValue(speclIngJson.getString(ProductDetailConstant.SPECIAL_INGREDIENTS_VALUE));
			}
		} catch (Exception e) {
			LOGGER.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}
		LOGGER.info("Exiting ProductDetailServiceImpl:getSplIngObj");
		return ing;
	}
}
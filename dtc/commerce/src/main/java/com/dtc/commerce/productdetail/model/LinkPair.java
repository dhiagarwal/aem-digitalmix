package com.dtc.commerce.productdetail.model;

/**
 * The Class LinkPair.
 */
public class LinkPair {

	/**
	 * Instantiates LinkPair object.
	 */
	public LinkPair() {
	}
	
	/** The link 1 name. */
	private String link1Name;

	/** The link 1 url. */
	private String link1Url;

	/** The link 2 name. */
	private String link2Name;

	/** The link 2 url. */
	private String link2Url;

	/**
	 * Gets the link 1 name.
	 *
	 * @return the link 1 name
	 */
	public String getLink1Name() {
		return link1Name;
	}

	/**
	 * Sets the link 1 name.
	 *
	 * @param link1Name
	 *            the new link 1 name
	 */
	public void setLink1Name(String link1Name) {
		this.link1Name = link1Name;
	}

	/**
	 * Gets the link 1 url.
	 *
	 * @return the link 1 url
	 */
	public String getLink1Url() {
		return link1Url;
	}

	/**
	 * Sets the link 1 url.
	 *
	 * @param link1Url
	 *            the new link 1 url
	 */
	public void setLink1Url(String link1Url) {
		this.link1Url = link1Url;
	}

	/**
	 * Gets the link 2 name.
	 *
	 * @return the link 2 name
	 */
	public String getLink2Name() {
		return link2Name;
	}

	/**
	 * Sets the link 2 name.
	 *
	 * @param link2Name
	 *            the new link 2 name
	 */
	public void setLink2Name(String link2Name) {
		this.link2Name = link2Name;
	}

	/**
	 * Gets the link 2 url.
	 *
	 * @return the link 2 url
	 */
	public String getLink2Url() {
		return link2Url;
	}

	/**
	 * Sets the link 2 url.
	 *
	 * @param link2Url
	 *            the new link 2 url
	 */
	public void setLink2Url(String link2Url) {
		this.link2Url = link2Url;
	}

}

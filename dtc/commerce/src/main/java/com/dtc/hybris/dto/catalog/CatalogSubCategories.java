package com.dtc.hybris.dto.catalog;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Catalog sub-categories. Used for unmarshalling the Hybris REST API response.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
        name = "", propOrder = { "category" })
@XmlRootElement(
        name = "subcategories")
@SuppressWarnings("PMD.BeanMembersShouldSerialize")
public class CatalogSubCategories
{
    private List<CatalogCategory> category;

    /**
     * Gets the categories.
     * 
     * @return The categories.
     */
    public final List<CatalogCategory> getCategory()
    {
        if (category == null)
        {
            category = new ArrayList<CatalogCategory>();
        }
        return this.category;
    }
}

package com.dtc.hybris.connection;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpStatus;

/**
 * Hybris command result.
 */
public class HybrisCommandResult implements CommandResult
{
    private final transient int status;
    private final transient HttpMethod request;
    private final transient Map<String, String> newSessionData;

    /**
     * Command result.
     * 
     * @param status The HTTP status.
     * @param request The HTTP request.
     * @param newSessionData The new session data.
     */
    public HybrisCommandResult(final int status, final HttpMethod request, final Map<String, String> newSessionData)
    {
        this.status = status;
        this.request = request;
        this.newSessionData = newSessionData;
    }

    @Override
    public final boolean success()
    {
        return (this.status == HttpStatus.SC_OK) || (this.status == HttpStatus.SC_CREATED)
                || (this.status == HttpStatus.SC_ACCEPTED);
    }

    @Override
    public final int getStatus()
    {
        return this.status;
    }

    @Override
    public final InputStream getBody()
    {
        try
        {
            return this.request.getResponseBodyAsStream();
        } catch (IOException e)
        {
            return null;
        }

    }

    @Override
    public final Map<String, String> getNewSessionData()
    {
        return this.newSessionData;
    }

    @Override
    public final void release()
    {
        this.request.releaseConnection();
    }

    /**
     * Finalize.
     * 
     * @throws Throwable Throwable exception.
     */
    protected final void finalize() throws Throwable
    {
        release();
        super.finalize();
    }
}

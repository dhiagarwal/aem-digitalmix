package com.dtc.hybris.connection.cmd;

import static com.dtc.hybris.constants.Constants.V1_FLAG;

import com.dtc.hybris.connection.AbstractHybrisCommand;

/**
 * Get Dtc catalog version command.
 */
public class GetDtcCatalogVersionCommand extends AbstractHybrisCommand
{
    /**
     * Constructor.
     * 
     * @param catalog The catalog.
     * @param catalogVersion The catalog version.
     * @param language The language.
     */
    public GetDtcCatalogVersionCommand(final String catalog, final String catalogVersion, final String language)
    {
        setPath(new StringBuilder().append(V1_FLAG).append("/catalogs/").append(catalog).append("/")
                .append(catalogVersion).toString());
        addParameter("options", "CATEGORIES,SUBCATEGORIES");
        addParameter("lang", language);
    }

}

package com.dtc.hybris.servlets;

import java.io.IOException;

import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;

import com.dtc.hybris.importer.HybrisImporter;

/**
 * Catalog import servlet.
 */
@SlingServlet(
        paths = { "/bin/dtc/hybris/catalogImport" }, methods = { "POST" })
public class CatalogImportServlet extends SlingAllMethodsServlet
{

    private static final long serialVersionUID = 1L;

    @Reference
    private transient HybrisImporter importer;

    @Override
    protected final void doPost(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
            throws ServletException, IOException
    {
        importer.importCatalog(request, response);
    }

}

package com.dtc.hybris.dto.catalog;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Catalog categories. Used for unmarshalling the Hybris REST API response.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
        name = "", propOrder = { "category" })
@XmlRootElement(
        name = "categories")
@SuppressWarnings("PMD.BeanMembersShouldSerialize")
public class CatalogCategories
{
    private List<CatalogCategory> category;

    /**
     * Gets the catalog categories.
     * 
     * @return category The catalog categories.
     */
    public final List<CatalogCategory> getCategory()
    {
        if (category == null)
        {
            category = new ArrayList<CatalogCategory>();
        }
        return this.category;
    }
}

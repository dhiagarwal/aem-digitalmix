package com.dtc.hybris.dto.product;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Item class. Used for unmarshalling the Hybris REST API response.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
        name = "", propOrder = { "itemId" })
public class Item
{

    @XmlElement(
            required = true)
    private String itemId;
    
    /**
     * Gets the item identifier.
     * 
     * @return The item identifier.
     */
    public final String getItemId()
    {
        return itemId;
    }

    /**
     * Sets item identifier.
     * 
     * @param itemId The item identifier.
     */
    public final void setItemId(final String itemId)
    {
        this.itemId = itemId;
    }

}

package com.dtc.hybris.dto.catalog;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * Catalog version. Used for unmarshalling the Hybris REST API response.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
        name = "catalogVersionType", propOrder = { "id", "lastModified", "url", "categories" })
@XmlRootElement(
        name = "catalogVersion")
@SuppressWarnings({ "PMD.ShortVariable" })
public class CatalogVersion
{

    private String id;
    @XmlSchemaType(
            name = "dateTime")
    private XMLGregorianCalendar lastModified;
    private String url;
    private CatalogCategories categories;

    /**
     * Gets the identifier.
     * 
     * @return The identifier.
     */
    public final String getId()
    {
        return id;
    }

    /**
     * Sets the identifier.
     * 
     * @param value The identifier.
     */
    public final void setId(final String value)
    {
        this.id = value;
    }

    /**
     * Gets the last modified date and time.
     * 
     * @return The last modified date and time.
     */
    public final XMLGregorianCalendar getLastModified()
    {
        return lastModified;
    }

    /**
     * Sets the last modified date and time.
     * 
     * @param value The last modified date and time.
     */
    public final void setLastModified(final XMLGregorianCalendar value)
    {
        this.lastModified = value;
    }

    /**
     * Gets the URL.
     * 
     * @return The URL.
     */
    public final String getUrl()
    {
        return url;
    }

    /**
     * Sets the URL.
     * 
     * @param value The URL.
     */
    public final void setUrl(final String value)
    {
        this.url = value;
    }

    /**
     * Gets the categories.
     * 
     * @return categories The categories.
     */
    public final CatalogCategories getCategories()
    {
        return categories;
    }

    /**
     * Sets the categories.
     * 
     * @param value
     *            The value.
     */
    public final void setCategories(final CatalogCategories value)
    {
        this.categories = value;
    }

}

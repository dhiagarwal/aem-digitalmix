package com.dtc.hybris.connection;

import java.io.InputStream;
import java.util.List;
import org.apache.commons.httpclient.NameValuePair;

/**
 * Hybris command interface.
 */
public interface HybrisCommand
{
    /**
     * GET method.
     */
    String METHOD_GET = "GET";

    /**
     * POST method.
     */
    String METHOD_POST = "POST";

    /**
     * PUT method.
     */
    String METHOD_PUT = "PUT";

    /**
     * DELETE method.
     */
    String METHOD_DELETE = "DELETE";

    /**
     * Gets the method.
     * 
     * @return The method.
     */
    String getMethod();

    /**
     * Get the path.
     * 
     * @return The path.
     */
    String getPath();

    /**
     * Gets the headers.
     * 
     * @return The headers.
     */
    List<NameValuePair> getHeaders();

    /**
     * Gets the request body.
     * 
     * @return The request body.
     */
    String getRequestBody();

    /**
     * Gets the request content type.
     * 
     * @return The request content type.
     */
    String getRequestContentType();

    /**
     * Gets the request body stream.
     * 
     * @return The request body stream.
     */
    InputStream getRequestBodyStream();

    /**
     * Gets the query string.
     * 
     * @return The query string.
     */
    String getQueryString();

    /**
     * Indicates if server authentication is required.
     * 
     * @return If server authentication is required.
     */
    boolean needsServerAuth();
}

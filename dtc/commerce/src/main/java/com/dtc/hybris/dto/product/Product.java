package com.dtc.hybris.dto.product;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Product class. Used for unmarshalling the Hybris REST API response.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "purchasable", "name", "baseOptions", "code", "url", "categories", "images",
		"manufacturer", "averageRating", "baseProduct", "variantType", "productSize", "mecVariantValue", "countryCode",
		"distributionChannel", "statusMessage", "mecVariantType", "language" })
@XmlRootElement(name = "product")
public class Product {

	/** The purchasable. */
	private String purchasable;

	/** The name. */
	private String name;

	/** The base options. */
	private BaseOptions baseOptions;

	/** The code. */
	private String code;

	/** The url. */
	private String url;

	/** The categories. */
	private ProductCategories categories;

	/** The images. */
	private Images images;

	/** The manufacturer. */
	private String manufacturer;

	/** The average rating. */
	private String averageRating;

	/** The base product. */
	private String baseProduct;

	/** The variant type. */
	private String variantType;

	/** The product size. */
	private String productSize;

	/** The variant value. */
	private String mecVariantValue;

	/** The country code. */
	private CountryCode countryCode;

	/** The distribution channel. */
	private DistributionChannel distributionChannel;

	/** The status message. */
	private String statusMessage;

	/** The mec variant type. */
	private MecVariantType mecVariantType;

	/** The language. */
	private String language;

	/**
	 * Gets the purchasable indicator.
	 *
	 * @return The purchasable indicator.
	 *
	 */
	public final String getPurchasable() {
		return purchasable;
	}

	/**
	 * Sets the purchasable indicator.
	 *
	 * @param value
	 *            The purchasable indicator.
	 *
	 */
	public final void setPurchasable(final String value) {
		this.purchasable = value;
	}

	/**
	 * Gets the manufacturer.
	 *
	 * @return the manufacturer
	 */
	public final String getManufacturer() {
		return manufacturer;
	}

	/**
	 * Sets the Manufacturer indicator.
	 * 
	 * @param value
	 *            The Manufacturer indicator.
	 * 
	 */
	public final void setManufacturer(final String value) {
		this.manufacturer = value;
	}

	/**
	 * Gets the variant type.
	 *
	 * @return the variant type
	 */
	public final String getVariantType() {
		return variantType;
	}

	/**
	 * Sets the Variant Type.
	 *
	 * @param value
	 *            The Variant Type.
	 */
	public final void setVariantType(final String value) {
		this.variantType = value;
	}

	/**
	 * Gets the base product.
	 *
	 * @return the base product
	 */
	public final String getBaseProduct() {
		return baseProduct;
	}

	/**
	 * Sets the BaseProduct.
	 *
	 * @param value
	 *            The BaseProduct value.
	 */
	public final void setBaseProduct(final String value) {
		this.baseProduct = value;
	}

	/**
	 * Gets the average rating.
	 *
	 * @return the average rating
	 */
	public final String getAverageRating() {
		return averageRating;
	}

	/**
	 * Sets the averageRating indicator.
	 * 
	 * @param value
	 *            The averageRating indicator.
	 * 
	 */
	public final void setAverageRating(final String value) {
		this.averageRating = value;
	}

	/**
	 * Gets the name.
	 * 
	 * @return The name.
	 * 
	 */
	public final String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 * 
	 * @param name
	 *            The name.
	 * 
	 */
	public final void setName(final String name) {
		this.name = name;
	}

	/**
	 * Gets the code.
	 * 
	 * @return The code.
	 * 
	 */
	public final String getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 * 
	 * @param code
	 *            The code.
	 * 
	 */
	public final void setCode(final String code) {
		this.code = code;
	}

	/**
	 * Gets the URL.
	 * 
	 * @return The URL.
	 * 
	 */
	public final String getUrl() {
		return url;
	}

	/**
	 * Sets the URL.
	 * 
	 * @param url
	 *            The URL.
	 * 
	 */
	public final void setUrl(final String url) {
		this.url = url;
	}

	/**
	 * Gets the categories.
	 * 
	 * @return The categories.
	 */
	public final ProductCategories getCategories() {
		return this.categories;
	}

	/**
	 * Sets the categories.
	 * 
	 * @param categories
	 *            The categories.
	 */
	public final void setCategories(final ProductCategories categories) {
		this.categories = categories;
	}

	/**
	 * Gets the BaseOptions.
	 * 
	 * @return The baseOptions.
	 */
	public final BaseOptions getBaseOptions() {
		return this.baseOptions;
	}

	/**
	 * Sets the BaseOptions.
	 * 
	 * @param baseOptions
	 *            The BaseOptions.
	 */
	public final void setBaseOptions(final BaseOptions baseOptions) {
		this.baseOptions = baseOptions;
	}

	/**
	 * Gets the categories.
	 * 
	 * @return The categories.
	 */
	public final Images getImages() {
		return this.images;
	}

	/**
	 * Sets the images.
	 * 
	 * @param images
	 *            The images.
	 */
	public final void setImages(final Images images) {
		this.images = images;
	}

	/**
	 * Gets the product size.
	 *
	 * @return the product size
	 */
	public final String getProductSize() {
		return productSize;
	}

	/**
	 * Sets the product size.
	 *
	 * @param productSize
	 *            the new product size
	 */
	public final void setProductSize(String productSize) {
		this.productSize = productSize;
	}

	/**
	 * Gets the mec variant value.
	 *
	 * @return the mec variant value
	 */
	public final String getMecVariantValue() {
		return mecVariantValue;
	}

	/**
	 * Sets the mec variant value.
	 *
	 * @param mecVariantValue
	 *            the new mec variant value
	 */
	public final void setMecVariantValue(String mecVariantValue) {
		this.mecVariantValue = mecVariantValue;
	}

	/**
	 * Gets the country code.
	 *
	 * @return the country code
	 */
	public final CountryCode getCountryCode() {
		return countryCode;
	}

	/**
	 * Sets the country code.
	 *
	 * @param countryCode
	 *            the new country code
	 */
	public final void setCountryCode(CountryCode countryCode) {
		this.countryCode = countryCode;
	}

	/**
	 * Gets the distribution channel.
	 *
	 * @return the distribution channel
	 */
	public final DistributionChannel getDistributionChannel() {
		return distributionChannel;
	}

	/**
	 * Sets the distribution channel.
	 *
	 * @param distributionChannel
	 *            the new distribution channel
	 */
	public final void setDistributionChannel(DistributionChannel distributionChannel) {
		this.distributionChannel = distributionChannel;
	}

	/**
	 * Gets the status message.
	 *
	 * @return the status message
	 */
	public final String getStatusMessage() {
		return statusMessage;
	}

	/**
	 * Sets the status message.
	 *
	 * @param statusMessage
	 *            the new status message
	 */
	public final void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	/**
	 * Gets the mec variant type.
	 *
	 * @return the mec variant type
	 */
	public final MecVariantType getMecVariantType() {
		return mecVariantType;
	}

	/**
	 * Sets the mec variant type.
	 *
	 * @param mecVariantType
	 *            the new mec variant type
	 */
	public final void setMecVariantType(MecVariantType mecVariantType) {
		this.mecVariantType = mecVariantType;
	}

	/**
	 * Gets the language.
	 *
	 * @return the language
	 */
	public final String getLanguage() {
		return language;
	}

	/**
	 * Sets the language.
	 *
	 * @param language
	 *            the new language
	 */
	public final void setLanguage(String language) {
		this.language = language;
	}

}

package com.dtc.hybris.parser;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;

import com.dtc.hybris.connection.CommandResult;

/**
 * Result unmarshaller service.
 */
public interface ResultUnmarshallerService
{
    /**
     * Unmarshalls the command result to JAXB mapped class.
     * 
     * @param result The command result.
     * @param <T> The JAXB mapped class.
     * @param declaredType The declared type.
     * @return The newly created object.
     * @throws JAXBException Error while parsing XML.
     * @throws XMLStreamException    Error while handling XML.
     */
    <T extends Object> T unmarshalResult(final CommandResult result, final Class<T> declaredType) throws JAXBException,
            XMLStreamException;
}


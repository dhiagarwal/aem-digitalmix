package com.dtc.hybris.dto.product;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Attribute class. Used for unmarshalling the Hybris REST API response.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
        name = "", propOrder = { "values", "value", "key" })
@XmlRootElement(name = "attribute")
public class Attribute
{
    private AttributeValues values;

    private String value;

    @XmlElement(required = true)
    private String key;

    /**
     * Gets the attribute key.
     * 
     * @return The attribute key.
     */
    public final String getKey()
    {
        return key;
    }

    /**
     * Sets the attribute key.
     * 
     * @param key The attribute key.
     */
    public final void setKey(final String key)
    {
        this.key = key;
    }

    /**
     * Gets the attribute value.
     * 
     * @return The attribute value.
     */
    public final String getValue()
    {
        return value;
    }

    /**
     * Sets the attribute value.
     * 
     * @param value
     *            The attribute value.
     */
    public final void setValue(final String value)
    {
        this.value = value;
    }

    /**
     * Gets the attribute values.
     * 
     * @return The attribute values.
     */
    public final AttributeValues getValues()
    {
        return values;
    }

    /**
     * Sets the attribute values.
     * 
     * @param values
     *            The attribute values.
     */
    public final void setValues(final AttributeValues values)
    {
        this.values = values;
    }

}

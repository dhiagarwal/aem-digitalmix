package com.dtc.hybris.api;

import javax.jcr.RepositoryException;

import com.adobe.cq.commerce.api.CommerceException;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.WCMException;

/**
 * Nu Skin Catalog Generator.
 */
public interface DtcCatalogGenerator
{
    /**
     * Creates the catalog.
     *
     * @param blueprint The blueprint.
     * @param destinationPath The destination path.
     * @throws RepositoryException Error while modifying JCR repository.
     * @throws CommerceException Error while creating catalog.
     * @throws WCMException Error while using page manager.
     */
    void createCatalog(Page blueprint, String destinationPath)
            throws RepositoryException, CommerceException, WCMException;

}

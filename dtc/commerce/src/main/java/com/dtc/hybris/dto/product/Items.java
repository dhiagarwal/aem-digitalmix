package com.dtc.hybris.dto.product;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Items class. Used for unmarshalling the Hybris REST API response.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
        name = "", propOrder = { "item" })
@SuppressWarnings("PMD.BeanMembersShouldSerialize")
public class Items
{
    @XmlElement(required = true)
    private List<Item> item;

    /**
     * Gets the items.
     * 
     * @return The items.
     */
    public final List<Item> getItem()
    {
        if (item == null)
        {
            item = new ArrayList<Item>();
        }

        return this.item;
    }

}

package com.dtc.hybris.utils;

import java.util.HashMap;
import java.util.Map;

import com.dtc.commerce.api.CommerceUser;

/**
 * Command utility class.
 */
public final class CommandUtil
{
    /**
     * Gets user headers.
     * 
     * @param commerceUser The commerce user.
     * @return headers The headers.
     */
    public static Map<String, String> getUserHeaders(final CommerceUser commerceUser)
    {
        final Map<String, String> headers = new HashMap<String, String>();

        headers.put("username", commerceUser.getUsername());
        headers.put("lastname", commerceUser.getLastName());
        headers.put("firstname", commerceUser.getFirstName());

        return headers;
    }
    
    /**
     * Default Constructor.
     * 
     * Private constructor to restrict instantiation.
     */
    private CommandUtil()
    {
    }

}

package com.dtc.hybris.dto.product;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Attribute values class. Used for unmarshalling the Hybris REST API response.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
        name = "", propOrder = { "string" })
@XmlRootElement(
        name = "values")
@SuppressWarnings("PMD.BeanMembersShouldSerialize")
public class AttributeValues
{
    private List<String> string;

    /**
     * Gets the string values.
     * 
     * @return The string values.
     */
    public final List<String> getString()
    {
        if (string == null)
        {
            string = new ArrayList<String>();
        }
        
        return this.string;
    }

}

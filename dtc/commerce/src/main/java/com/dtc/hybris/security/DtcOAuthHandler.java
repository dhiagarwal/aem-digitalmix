package com.dtc.hybris.security;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Deactivate;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.granite.crypto.CryptoException;
import com.adobe.granite.crypto.CryptoSupport;
import com.dtc.hybris.connection.HybrisConnection;

/**
 * The DTC OAuth handler.
 */
@Component(
        label = "Dtc Commerce Hybris OAuth2 Handler",
        description = "Authenticates REST calls made to the Hybris API using OAuth2.", metatype = true)
@Service(HybrisOAuthHandler.class)
public class DtcOAuthHandler implements HybrisOAuthHandler
{
    private static final Logger LOG = LoggerFactory.getLogger(DtcOAuthHandler.class);

    private static final String REFRESH_TOKEN = "refresh_token";
    private static final String ACCESS_TOKEN_EXPIRES = "expires";
    private static final String ACCESS_TOKEN = "access_token";
    private static final String EXPIRES_IN = "expires_in";

    private static final String PARAM_GRANT_TYPE = "grant_type";
    private static final String PARAM_USERNAME = "username";
    private static final String PARAM_PASSW = "password";
    private static final String PARAM_CLIENT_SECRET = "client_secret";
    private static final String PARAM_CLIENT_ID = "client_id";

    private static final String DEFAULT_OAUTH_TOKEN_URI = "/meccommercewebservices/oauth/token";
    private static final String DEFAULT_TRUSTED_CLIENT_ID = "trusted_client";
    private static final String DEFAULT_TRUSTED_CLIENT_SECRET = "secret";

    private static final int DEFAULT_TIMEOUT_MILLISECONDS = 1000;

    @Property(
            label = "Trusted Client ID", description = "OAuth trusted client ID to use.",
            value = { DEFAULT_TRUSTED_CLIENT_ID })
    private static final String PROPERTY_TRUSTED_CLIENT_ID = "hybris.oauth.trusted.client_id";

    @Property(
            label = "Trusted Client Secret", description = "OAuth client secret for the above trusted client ID.",
            value = { DEFAULT_TRUSTED_CLIENT_SECRET })
    private static final String PROPERTY_TRUSTED_CLIENT_SECRET = "hybris.oauth.trusted.client_secret";

    @Property(
            label = "OAuth token URI",
            description = "The URI for OAuth token requests (default: /webservice/oauth/token)",
            value = { DEFAULT_OAUTH_TOKEN_URI })
    private static final String OAUTH_TOKEN_URI = "hybris.oauth.token.uri";

    @Reference
    private transient CryptoSupport cryptoSupport;

    private transient String oauthTokenURI;
    private transient String trustedClientId;
    private transient String trustedClientSecret;
    private transient Map<String, String> authenticationData;

    public DtcOAuthHandler() {
		super();		
	}

	/**
     * Authenticate user data.
     * 
     * @param connection The connection.
     * @param username The username.
     * @param password The password.
     * @return The authentication user data.
     */
    private Map<String, String> authenticateUser(final HybrisConnection connection, final String username,
            final String password)
    {

        PostMethod method = null;
        try
        {

            final HttpClient client = connection.getConfiguredHttpClient();
            method = createPostMethod(connection);
            method.setDoAuthentication(false);
            method.addParameter(PARAM_CLIENT_ID, getUnprotectedValue(this.trustedClientId));
            method.addParameter(PARAM_CLIENT_SECRET, getUnprotectedValue(this.trustedClientSecret));
            method.addParameter(PARAM_GRANT_TYPE, "client_credentials");
            method.addParameter(PARAM_USERNAME, username);
            method.addParameter(PARAM_PASSW, getUnprotectedValue(password));

            final int status = client.executeMethod(method);

            LOG.debug("Response: " + method.getResponseBodyAsString());

            if ((status >= HttpStatus.SC_OK) && (status < HttpStatus.SC_MULTIPLE_CHOICES))
            {
                this.authenticationData = extractAuthData(username, method.getResponseBodyAsString());

                return this.authenticationData;
            }
            LOG.info("Server responded with {} -> authentication failed.",status );
        } catch (JSONException | IOException | CryptoException e)
        {
            throw new IllegalStateException(e);
        } finally
        {
            if (method != null)
            {
                method.releaseConnection();
            }
        }

        return null;
    }

    /**
     * Creates POST method.
     * 
     * @param connection The connection.
     * @return The post method.
     */
    private PostMethod createPostMethod(final HybrisConnection connection)
    {
        return new PostMethod(connection.getServerUrl() + this.oauthTokenURI);
    }

    @Override
    public final Map<String, String> executeAuthenticated(final HybrisConnection connection, final HttpClient client,
            final HttpMethod method, final String username, final String password) throws IOException
    {
        final Map<String, String> authData = authenticateUser(connection, username, password);
        executeAuthenticated(client, method, authData);

        return authData;
    }

    @Override
    public final Map<String, String> executeAuthenticated(final HttpClient client, final HttpMethod method,
            final Map<String, String> sessionData) throws IOException
    {
        final Map<String, String> newSessionData = null;
        method.setRequestHeader("Authorization", "Bearer " + sessionData.get(ACCESS_TOKEN));
        client.executeMethod(method);
        return newSessionData;
    }

    /**
     * Extracts authentication data from the JSON response.
     * 
     * @param username The username.
     * @param jsonString The JSON string.
     * @return The authentication data extracted from the JSON response.
     * @throws JSONException The JSONException.
     */
    private Map<String, String> extractAuthData(final String username, final String jsonString) throws JSONException
    {
        final Map<String, String> authData = new HashMap<String, String>();
        final JSONObject authenticationJSON = new JSONObject(jsonString);

        authData.put(PARAM_USERNAME, username);
        authData.put(ACCESS_TOKEN, authenticationJSON.getString(ACCESS_TOKEN));

        if (authenticationJSON.has(REFRESH_TOKEN))
        {
            authData.put(REFRESH_TOKEN, authenticationJSON.getString(REFRESH_TOKEN));
        }

        if (authenticationJSON.has(EXPIRES_IN))
        {
            final int expiresIn = authenticationJSON.getInt(EXPIRES_IN);
            final long expires = System.currentTimeMillis() + expiresIn * DEFAULT_TIMEOUT_MILLISECONDS;

            authData.put(ACCESS_TOKEN_EXPIRES, String.valueOf(expires));
        }

        return authData;
    }

    /**
     * Initializes the component on startup.
     * 
     * @param properties The OSGi properties.
     */
    @Activate
    protected final void activate(final Map<String, String> properties)
    {
        this.trustedClientId = PropertiesUtil.toString(properties.get(PROPERTY_TRUSTED_CLIENT_ID),
                DEFAULT_TRUSTED_CLIENT_ID);
        this.trustedClientSecret = PropertiesUtil.toString(properties.get(PROPERTY_TRUSTED_CLIENT_SECRET),
                DEFAULT_TRUSTED_CLIENT_SECRET);
        this.oauthTokenURI = PropertiesUtil.toString(properties.get(OAUTH_TOKEN_URI), DEFAULT_OAUTH_TOKEN_URI);
    }

    /**
     * Destroys the component.
     */
    @Deactivate
    protected final void deactivate()
    {
        this.trustedClientId = null;
        this.trustedClientSecret = null;
    }

    /**
     * Gets unprotected value.
     * 
     * @param value The value.
     * @return value the value.
     * @throws CryptoException Error while decrypting user name or password.
     */
    private String getUnprotectedValue(final String value) throws CryptoException
    {
        if (cryptoSupport.isProtected(value))
        {
            return (cryptoSupport.unprotect(value));
        }

        return value;
    }

}

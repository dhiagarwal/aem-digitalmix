package com.dtc.hybris.dto.product;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Product categories class. Used for unmarshalling the Hybris REST API response.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
        name = "", propOrder = { "baseOption" })
@XmlRootElement(
        name = "baseOptions")
@SuppressWarnings("PMD.BeanMembersShouldSerialize")
public class BaseOptions
{
    private List<BaseOption> baseOption;

    /**
     * Gets the category list.
     * 
     * @return The category list.
     */
    public final List<BaseOption> getBaseOption()
    {
        if (baseOption == null)
        {
        	baseOption = new ArrayList<BaseOption>();
        }
        return this.baseOption;
    }

}

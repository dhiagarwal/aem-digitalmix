package com.dtc.hybris.dto.product;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Product category class. Used for unmarshalling the Hybris REST API response.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "catalogCategory", propOrder = {
        "code",
        "url"
})
@XmlRootElement(name = "category")
public class ProductCategory
{
    private String code;
    private String url;
    
    /**
     * Gets the category code.
     * 
     * @return The category code.
     */
    public final String getCode()
    {
        return code;
    }
    /**
     * Sets the category code.
     * 
     * @param code The category code.
     */
    public final void setCode(final String code)
    {
        this.code = code;
    }
    
    /**
     * Gets the category URL.
     *
     * @return The category URL.
     */
    public final String getUrl()
    {
        return url;
    }

    /**
     * Sets the category URL.
     *
     * @param value The category URL.
     */
    public final void setUrl(final String value)
    {
        this.url = value;
    }
    
}


package com.dtc.hybris.importer;

import com.day.cq.commons.jcr.JcrUtil;
import com.dtc.hybris.dto.product.Product;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.apache.jackrabbit.util.Text;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

/**
 * Dtc Hybris import handler.
 */
@Component
@Service(DtcImportHandler.class)
@SuppressWarnings("PMD.TooManyMethods")
public class DtcHybrisImportHandler implements DtcImportHandler {
	private static final String CATEGORY_NAME_PATH = "categoryNamePath";
	private static final String CATEGORY_PATH = "categoryPath";

	private static final String ACTION_MODIFY = "M";
	private static final String ACTION_ADD = "A";

	private static final String NT_UNSTRUCTED_PRIMARY_TYPE = "nt:unstructured";
	private static final String DAM_COMMERCE_BASE_PATH = "/content/dam/commerce";
	private static final String GENERIC_VARIANT = "MECGenericVariantProduct";

	@Override
	public final void importProduct(final ImporterContext importerContext, final Product product)
			throws RepositoryException {
		
		if (StringUtils.isNotBlank(product.getBaseProduct())) {
			if(product.getBaseOptions().getBaseOption().get(0).getVariantType().equals(GENERIC_VARIANT)){
				createProduct(importerContext, product);
			}
			else{
				createVariant(importerContext, product);
			}
		} else {
			if(!(product.getVariantType().equalsIgnoreCase(GENERIC_VARIANT))){
				createProduct(importerContext, product);
			}
		}

	}

	@Override
	public final void createProduct(final ImporterContext importerContext, final Product product)
			throws RepositoryException {
		final Session session = importerContext.getSession();
		final String productPath = importerContext
				.getProductPath(JcrUtil.createValidName(product.getCode(), JcrUtil.HYPHEN_LABEL_CHAR_MAPPING));

		String actionType = ACTION_ADD;
		Node productNode = null;
		if (session.nodeExists(productPath)) {
			productNode = session.getNode(productPath);
		} else if (!(importerContext.isPublishRunMode())){
			String parentPath = Text.getRelativeParent(productPath, 1);
			Node parent = JcrUtil.createPath(parentPath, false, "sling:Folder", "sling:Folder", session, false);
			productNode = JcrUtil.createUniqueNode(parent, Text.getName(productPath), NT_UNSTRUCTED_PRIMARY_TYPE,
					session);
		}

		// Update product
		if(null != productNode){
			updateProduct(productNode, importerContext, product);

			importerContext.info(actionType, String.format("Product at path %s", productNode.getPath()));
		}
	}

	@Override
	public final void createVariant(final ImporterContext importerContext, final Product product)
			throws RepositoryException {
		final Session session = importerContext.getSession();
		final String baseProductPath = importerContext
				.getProductPath(JcrUtil.createValidName(product.getBaseProduct(), JcrUtil.HYPHEN_LABEL_CHAR_MAPPING));

		if (!(session.nodeExists(baseProductPath)) && (!(importerContext.isPublishRunMode()))) {
			createBaseProductNode(importerContext, product);
		}
		String actionType = ACTION_ADD;

		String variantPath = baseProductPath + "/"
				+ JcrUtil.createValidName(product.getCode(), JcrUtil.HYPHEN_LABEL_CHAR_MAPPING);

		String parentPath = Text.getRelativeParent(variantPath, 1);
		Node parent = JcrUtil.createPath(parentPath, NT_UNSTRUCTED_PRIMARY_TYPE, session);
		
		Node variantNode = null;
		if(session.nodeExists(variantPath)){
		  variantNode = session.getNode(variantPath);
		}
		else if (!(importerContext.isPublishRunMode())){
		  variantNode = JcrUtil.createUniqueNode(parent, Text.getName(variantPath), NT_UNSTRUCTED_PRIMARY_TYPE,session);
		}

		// Update Variant
		if(null != variantNode){
			updateVariant(variantNode, importerContext, product);

			importerContext.info(actionType, String.format("Product at path %s", variantNode.getPath()));
		}
	}

	public final void createBaseProductNode(final ImporterContext importerContext, final Product product)
			throws RepositoryException {
		final Session session = importerContext.getSession();
		final String baseProductPath = importerContext
				.getProductPath(JcrUtil.createValidName(product.getBaseProduct(), JcrUtil.HYPHEN_LABEL_CHAR_MAPPING));

		String parentPath = Text.getRelativeParent(baseProductPath, 1);
		Node parent = JcrUtil.createPath(parentPath, false, "sling:Folder", "sling:Folder", session, false);

		JcrUtil.createUniqueNode(parent, Text.getName(baseProductPath), "nt:unstructured", session);
	}

	@Override
	public final void updateProduct(final Node productNode, final ImporterContext importerContext,
			final Product product) throws RepositoryException {
		productNode.setProperty("purchasable", Boolean.valueOf(product.getPurchasable()));
		if(!(productNode.hasProperty("jcr:title"))){
		  productNode.setProperty("jcr:title", product.getName());  
		}
		productNode.setProperty("productName", product.getName());
		productNode.setProperty("identifier", product.getCode());
		productNode.setProperty("url", product.getUrl());

		if(!(product.getBaseOptions().getBaseOption().isEmpty())){
			if (null != product.getBaseOptions().getBaseOption().get(0).getSelected().getPriceData()){
				productNode.setProperty("priceData", product.getBaseOptions().getBaseOption().get(0).getSelected().getPriceData().getValue());
			}
			if (null != product.getBaseOptions().getBaseOption().get(0).getSelected().getPvValue()){
				productNode.setProperty("pvValue", product.getBaseOptions().getBaseOption().get(0).getSelected().getPvValue().getValue());
			}
		}
		
		productNode.setProperty("cq:commerceType", "product");
		productNode.setProperty("sling:resourceType", "commerce/components/product");
		if(null!=product.getVariantType()){
			productNode.setProperty("cq:productVariantAxes", new String[] { product.getVariantType() });	
		}
		productNode.addMixin("cq:Taggable");

		importerContext.getSession().save();
	}

	public final void updateVariant(final Node productNode, final ImporterContext importerContext,
			final Product product) throws RepositoryException {
		productNode.setProperty("purchasable", Boolean.valueOf(product.getPurchasable()));
		if(!(productNode.hasProperty("jcr:title"))){
		  productNode.setProperty("jcr:title", product.getName());  
		}
		productNode.setProperty("productName", product.getName());
		productNode.setProperty("identifier", product.getCode());
		productNode.setProperty("url", product.getUrl());

		if(!(product.getBaseOptions().getBaseOption().isEmpty())){
			if (null != product.getBaseOptions().getBaseOption().get(0).getSelected().getPriceData()){
				productNode.setProperty("priceData", product.getBaseOptions().getBaseOption().get(0).getSelected().getPriceData().getValue());
			}
			if (null != product.getBaseOptions().getBaseOption().get(0).getSelected().getPvValue()){
				productNode.setProperty("pvValue", product.getBaseOptions().getBaseOption().get(0).getSelected().getPvValue().getValue());
			}
		}

		productNode.setProperty("cq:commerceType", "variant");
		productNode.setProperty("productVariant", true);
		productNode.setProperty("sling:resourceType", "commerce/components/product");
		productNode.addMixin("cq:Taggable");
		productNode.setProperty("productSize", product.getProductSize());

		if (null != product.getBaseOptions().getBaseOption().get(0).getSelected().getVariantOptionQualifiers().getVariantOptionQualifier().get(0).getName()) {
			productNode.setProperty("mecVariantType", product.getBaseOptions().getBaseOption().get(0).getSelected().getVariantOptionQualifiers().getVariantOptionQualifier().get(0).getName());
		}
		if (null != product.getBaseOptions().getBaseOption().get(0).getSelected().getVariantOptionQualifiers().getVariantOptionQualifier().get(0).getValue()) {
			productNode.setProperty("mecVariantValue", product.getBaseOptions().getBaseOption().get(0).getSelected().getVariantOptionQualifiers().getVariantOptionQualifier().get(0).getValue());
		}
		if (null != product.getCountryCode()) {
			productNode.setProperty("countryCode", product.getCountryCode().getCodeLowerCase());
		}
		if (null != product.getDistributionChannel()) {
			productNode.setProperty("distributionChannel", product.getDistributionChannel().getCodeLowerCase());
		}

		productNode.setProperty("language", product.getLanguage());

		importerContext.getSession().save();
	}

}

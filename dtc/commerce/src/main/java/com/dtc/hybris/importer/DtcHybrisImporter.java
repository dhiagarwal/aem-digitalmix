package com.dtc.hybris.importer;

import static com.dtc.hybris.constants.Constants.COMMERCE_BASE_PATH;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicStampedReference;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.apache.sling.settings.SlingSettingsService;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.granite.xss.XSSAPI;
import com.day.cq.commons.inherit.ComponentInheritanceValueMap;
import com.day.cq.commons.jcr.JcrUtil;
import com.dtc.commerce.api.CommerceException;
import com.dtc.hybris.api.HybrisConfigurationService;
import com.dtc.hybris.connection.CommandResult;
import com.dtc.hybris.connection.HybrisConnection;
import com.dtc.hybris.connection.cmd.GetDtcProductsCommand;
import com.dtc.hybris.dto.product.Export;
import com.dtc.hybris.dto.product.Product;
import com.dtc.hybris.dto.product.Products;
import com.dtc.hybris.parser.ResultUnmarshallerService;

/**
 * Dtc Hybris importer.
 */
@Component(
        metatype = true, label = "Dtc  Hybris Catalog Importer")
@Service(HybrisImporter.class)
public class DtcHybrisImporter implements HybrisImporter
{
    private static final Logger LOG = LoggerFactory.getLogger(DtcHybrisImporter.class);

    private static final int DEFAULT_PAGE_SIZE = 30;

    /**
     * Page size property.
     */
    @Property(
            intValue = { DEFAULT_PAGE_SIZE })
    public static final String PAGE_SIZE = "hybris.import.pagesize";

    @Reference
    private transient DtcImportHandler handler;

    @Reference
    private transient HybrisConfigurationService configuration;

    @Reference
    private transient HybrisConnection connection;

    @Reference
    private transient ResultUnmarshallerService resultUnmarshallerService;

    @Reference
    private transient XSSAPI xssAPpi;

    private final transient AtomicStampedReference<Thread> invokerThreadRef;
    private transient int pageSize;

    /**
     * Default constructor.
     */
    public DtcHybrisImporter()
    {
        this.invokerThreadRef = new AtomicStampedReference<Thread>(null, 0);
    }
    
    @Override
    public final void importCatalog(final ResourceResolver resourceResolver, final String baseStore, final String catalog, final String country)
            throws IOException
    {

        final String targetPath = new StringBuilder().append(COMMERCE_BASE_PATH).append("/").append("dtc").append("/").append(country).toString();

        Resource base = resourceResolver.getResource(targetPath);
        if (null == base)
        {
            base = resourceResolver.getResource(COMMERCE_BASE_PATH);
            LOG.error("E Cannot do incremental update as catalog " + catalog + "does not exists currently");
            return;
        }

        importCatalog(base, baseStore, catalog, country, null, true);
    }

    @Override
    public final void importCatalog(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
            throws IOException
    {
        final String baseStore = xssAPpi.filterHTML(request.getParameter("store"));
        final String catalog = xssAPpi.filterHTML(request.getParameter("catalog"));
        final String country = xssAPpi.filterHTML(request.getParameter("country"));
        final boolean incremental = Boolean.valueOf(request.getParameter("incrementalImport"));

        /*final String targetPath = new StringBuilder().append(COMMERCE_BASE_PATH).append("/").append("dtc")
                .append("/").append(catalog).append("/").append(language).toString();*/
        
        final String targetPath = new StringBuilder().append(COMMERCE_BASE_PATH).append("/").append("dtc").append("/").append(country).toString();

        Resource base = request.getResourceResolver().getResource(targetPath);
        if (null == base)
        {
            base = request.getResourceResolver().getResource(COMMERCE_BASE_PATH);
            if (incremental)
            {
                response.getWriter().append("E Cannot do incremental update as catalog ").append(catalog)
                        .append(" does not exists currently.");
                response.getWriter().flush();

                return;
            }
        }

        importCatalog(base, baseStore, catalog, country, response.getWriter(), incremental);
    }

    /**
     * Import catalog.
     * 
     * @param base The base resource.
     * @param baseStore The base store.
     * @param catalog The catalog.
     * @param language The language.
     * @param writer The writer.
     * @param incremental Indicates if the import is incremental or not.
     */
    private void importCatalog(final Resource base, final String baseStore, final String catalog,
            final String country, final PrintWriter writer, final boolean incremental)
    {
        if (!enterExclusiveImport())
        {
            return;
        }

        final long beginMillis = System.currentTimeMillis();

        Date lastImported = null;
        if (incremental)
        {
            lastImported = (Date) new ComponentInheritanceValueMap(base).getInherited("cq:hybrisLastImported",
                    Date.class);
        }
        
        BundleContext bundleContext = FrameworkUtil.getBundle(DtcHybrisImporter.class).getBundleContext();
        ServiceReference settingsRef = bundleContext.getServiceReference(SlingSettingsService.class.getName());
        SlingSettingsService settings = (SlingSettingsService) bundleContext.getService(settingsRef);
        Set<String> runmodes =  settings.getRunModes();

        final ImporterContext importerContext = new ImporterContext(base, baseStore, catalog,
                configuration.getCatalogVersion(), country, writer, runmodes.contains("publish"));

        try
        {
            // Import catalog
            //importCatalogVersion(importerContext);

            // Import products
            importProducts(importerContext, lastImported, country);

            final long importMillis = System.currentTimeMillis() - beginMillis;

            if (importerContext.isHasErrors())
            {
                importerContext.info("END",
                        String.format("Catalog import finished with errors in %s milliseconds", importMillis));
            } else
            {
                importerContext.info("END",
                        String.format("Catalog imported successfully in %s milliseconds", importMillis));
            }

        } finally
        {
            exitExclusiveImport();
        }

    }

    /**
     * Imports products from catalog.
     * 
     * @param importerContext The importer context.
     * @param lastImported The last modified.
     * @throws com.mec.commerce.api.CommerceException
     * @throws CommerceException
     */
    @SuppressWarnings("PMD.AvoidInstantiatingObjectsInLoops")
    private void importProducts(final ImporterContext importerContext, final Date lastImported, final String country)
    {
        int page = 0;
        boolean hasProducts = false;
        do
        {
            try
            {
                final GetDtcProductsCommand cmd = new GetDtcProductsCommand(importerContext.getCatalog(),
                        importerContext.getCatalogVersion(), Integer.valueOf(page),
                        Integer.valueOf(this.pageSize), lastImported);

                HybrisConnection.ConnectionOptions connectionOptions = new HybrisConnection.ConnectionOptions(
                        importerContext.getBaseStore(), null);

                final CommandResult result = this.connection.execute(cmd, connectionOptions);
                
                LOG.info("Result -->" + result.getBody());

                connectionOptions = updateConnectionOptions(connectionOptions, importerContext.getBaseStore(), result);

                final Export data = resultUnmarshallerService.unmarshalResult(result, Export.class);
                final Products productList = data.getProducts();
                final List<Product> products = productList.getProduct();
                hasProducts = CollectionUtils.isNotEmpty(products);

                if (hasProducts)
                {
                  String targetPath = new StringBuilder().append(COMMERCE_BASE_PATH).append("/").append("dtc").append("/").append(country).toString();
                  
                  Node baseNode = JcrUtil.createPath(targetPath, "sling:Folder", importerContext.getSession());
                  
                  baseNode.setProperty("cq:hybrisLastImported", Calendar.getInstance());
                    for (final Product product : products)
                    {
                        //handler.createProduct(importerContext, product);
                    	handler.importProduct(importerContext, product);
                    }
                }

            } catch (CommerceException | JAXBException | XMLStreamException | RepositoryException e)
            {
                importerContext.error("Error importing product.");

                LOG.error("Error creating product :: ", e);
            }

            page++;

        } while (hasProducts);

    }

    /**
     * Updates connection options.
     * 
     * @param connectionOptions The connection options.
     * @param baseStore The base store.
     * @param result The command result.
     * @return The Hybris connection options.
     */
    private HybrisConnection.ConnectionOptions updateConnectionOptions(
            final HybrisConnection.ConnectionOptions connectionOptions, final String baseStore,
            final CommandResult result)
    {
        final Map<String, String> newSessionData = result.getNewSessionData();

        if (MapUtils.isNotEmpty(newSessionData))
        {
            return (new HybrisConnection.ConnectionOptions(baseStore, newSessionData, null));
        }

        return connectionOptions;
    }

    /**
     * Activate.
     * 
     * @param props The OSGi properties.
     */
    @Activate
    protected final void activate(final Map<String, Object> props)
    {
        this.pageSize = PropertiesUtil.toInteger(props.get(PAGE_SIZE), DEFAULT_PAGE_SIZE);
    }

    /**
     * Enter exclusive import.
     * 
     * @return If the import is exclusive.
     */
    @SuppressWarnings("PMD.AvoidThrowingRawExceptionTypes")
    private boolean enterExclusiveImport()
    {
        final Thread invokerThread = (Thread) this.invokerThreadRef.getReference();
        if ((invokerThread != null) && (!invokerThread.isAlive()))
        {
            this.invokerThreadRef.set(null, 0);
        }

        boolean ret = false;

        final Thread currentThread = Thread.currentThread();
        final int[] stamp = new int[1];
        if (this.invokerThreadRef.compareAndSet(null, currentThread, 0, 1))
        {
            ret = true;
        } else if (this.invokerThreadRef.get(stamp) == currentThread)
        {
            if (this.invokerThreadRef.attemptStamp(currentThread, stamp[0] + 1))
            {
                ret = true;
            } else
            {
                throw new RuntimeException("Importer stamp attempt failed");
            }
        }

        if (!ret)
        {
            LOG.warn("Concurrent importer invocation cancelled.");
        }

        return ret;
    }

    /**
     * Exit exclusive import.
     */
    @SuppressWarnings("PMD.AvoidThrowingRawExceptionTypes")
    private void exitExclusiveImport()
    {
        final int stamp = this.invokerThreadRef.getStamp();
        if (stamp == 1)
        {
            this.invokerThreadRef.set(null, 0);
        } else if (stamp > 1)
        {
            if (!this.invokerThreadRef.attemptStamp(Thread.currentThread(), stamp - 1))
            {
                throw new RuntimeException("Importer stamp attempt failed");
            }
        } else
        {
            throw new RuntimeException("Invalid importer stamp: " + stamp);
        }
    }

}

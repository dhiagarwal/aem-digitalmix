package com.dtc.hybris.schedulers;

import java.util.Dictionary;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dtc.hybris.importer.HybrisImporter;

import org.apache.felix.scr.annotations.Properties;

@Component(immediate = true, metatype = true, label = "Scheduled Hybris Incremental Import")
@Service(value = Runnable.class)
@Properties({
    @Property(label = "Cron expression defining when this Scheduled Service will run", description = "[every minute = 0 * * * * ?] Visit www.cronmaker.com to generate cron expressions.", name = "scheduler.expression", value = "0 * * * * ?"),
    @Property(label = "Allow concurrent executions", description = "Allow concurrent executions of this Scheduled Service. This is almost always false.", name = "scheduler.concurrent", propertyPrivate = true, boolValue = false),
    @Property(name = "prop.enabled", boolValue = false),
    @Property(name = "baseStore", value = "changeme"),
    @Property(name = "catalog", value = "changeme"),
    @Property(name = "country", value = "changeme")})

public class ProductImportScheduler implements Runnable {

  private static final Logger log = LoggerFactory.getLogger(ProductImportScheduler.class);

  private String baseStore;
  private String catalog;
  private String country;
  private Boolean enabled;
  
  private static final String BASESTORE = "baseStore";

  private static final String CATALOG = "catalog";
  
  private static final String COUNTRY = "country";
  
  /** The resolver factory. */
  @Reference
  private ResourceResolverFactory resolverFactory;

  /** The resource resolver. */
  private ResourceResolver resourceResolver;

  protected void activate(ComponentContext componentContext) {
    log.debug("Start Product Sync Configuration");
    configure(componentContext.getProperties());
  }

  public void configure(Dictionary<?, ?> properties) {
    this.baseStore = PropertiesUtil.toString(properties.get(BASESTORE), null);
    this.catalog = PropertiesUtil.toString(properties.get(CATALOG), null);
    this.country = PropertiesUtil.toString(properties.get(COUNTRY), null);
    this.enabled = PropertiesUtil.toBoolean(properties.get("prop.enabled"), false);
    log.debug("Finish Product Sync Configuration");
  }
  
  @Reference
  private transient HybrisImporter importer;

  @Override
  public void run() {
    if (this.enabled) {
      try {
        
        resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);
        
        importer.importCatalog(resourceResolver, baseStore, catalog, country);

      } catch (Exception e) {
        log.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
      }
    }

  }

}

package com.dtc.hybris.dto.product;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * The Class Selected.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "code", "stock", "url", "priceData", "variantOptionQualifiers", "pvValue"})
@XmlRootElement(name = "selected")
public class Selected {

	/** The variant option qualifiers. */
	private VariantOptionQualifiers variantOptionQualifiers;

	/** The url. */
	private String url;

	/** The code. */
	private String code;

	/** The stock. */
	private Stock stock;

	// The retail price
	private PriceData priceData;

	// The pv value
	private PvValue pvValue;

	/**
	 * Gets the categories.
	 * 
	 * @return The categories.
	 */
	public final VariantOptionQualifiers getVariantOptionQualifiers() {
		return this.variantOptionQualifiers;
	}

	/**
	 * Sets the variantOptionQualifiers.
	 * 
	 * @param variantOptionQualifiers
	 *            The variantOptionQualifiers.
	 */
	public final void setVariantOptionQualifiers(final VariantOptionQualifiers variantOptionQualifiers) {
		this.variantOptionQualifiers = variantOptionQualifiers;
	}

	/**
	 * Gets the url.
	 *
	 * @return the url
	 */
	public final String getUrl() {
		return url;
	}

	/**
	 * Sets the url.
	 *
	 * @param uRL
	 *            the new url
	 */
	public final void setUrl(String uRL) {
		url = uRL;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public final String getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code
	 *            the new code
	 */
	public final void setCode(String code) {
		this.code = code;
	}

	/**
	 * Gets the stock.
	 *
	 * @return the stock
	 */
	public final Stock getStock() {
		return stock;
	}

	/**
	 * Sets the stock.
	 *
	 * @param stock
	 *            the new stock
	 */
	public final void setStock(Stock stock) {
		this.stock = stock;
	}

	public final PriceData getPriceData() {
		return priceData;
	}

	public final void setPriceData(PriceData priceData) {
		this.priceData = priceData;
	}

	public final PvValue getPvValue() {
		return pvValue;
	}

	public final void setPvValue(PvValue pvValue) {
		this.pvValue = pvValue;
	}
}

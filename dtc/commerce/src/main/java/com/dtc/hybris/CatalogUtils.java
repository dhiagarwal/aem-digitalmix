package com.dtc.hybris;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.query.Query;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.commerce.api.CommerceException;
import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.commons.jcr.JcrUtil;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.WCMException;
import com.day.cq.wcm.msm.api.RolloutConfig;
import com.day.cq.wcm.msm.api.RolloutConfigManager;

/**
 *  Catalog utils.
 */
@SuppressWarnings("PMD.AvoidCatchingGenericException")
public final class CatalogUtils
{
    private static final Logger LOG = LoggerFactory.getLogger(CatalogUtils.class);

    /**
     * Gets sanitised name for pages.
     *
     * @param name The page name.
     * @return The sanitised name.
     */
    public static String mangleName(final String name)
    {
        return JcrUtil.createValidName(name, JcrUtil.HYPHEN_LABEL_CHAR_MAPPING);
    }

    /**
     * Gets the template page.
     *
     * @param blueprint The blueprint.
     * @param name The name.
     * @return The template page.
     */
    public static Page getTemplatePage(final Page blueprint, final String name)
    {
        final InheritanceValueMap blueprintProps = new HierarchyNodeInheritanceValueMap(blueprint.getContentResource());
        final String template = blueprintProps.getInherited("templates/" + name, String.class);
        if (StringUtils.isEmpty(template))
        {
            LOG.warn("Template property {} not found in blueprint {}.", "templates/" + name, blueprint.getPath());
            return null;
        }

        return blueprint.getPageManager().getPage(template);
    }

    /**
     * Creates a page.
     *
     * @param pageManager The page manager.
     * @param parent The parent node.
     * @param name The page name.
     * @return The created page.
     * @throws RepositoryException Error while modifying JCR repository.
     */
    public static Page createPage(final PageManager pageManager, final Node parent, final String name)
            throws RepositoryException
    {
        final Node pageNode = JcrUtil.createUniqueNode(parent, name, "cq:Page", parent.getSession());

        return pageManager.getPage(pageNode.getPath());
    }

    /**
     * Creates the page content.
     *
     * @param page The page.
     * @param templatePage The template page.
     * @return The Page content node.
     * @throws RepositoryException Error while modifying JCR repository.
     */
    public static Node createPageContent(final Page page, final Page templatePage) throws RepositoryException
    {
        final Node pageNode = page.adaptTo(Node.class);
        final Resource templateContent = templatePage.getContentResource();
        if (null != templateContent)
        {
            return JcrUtil.copy(templateContent.adaptTo(Node.class), pageNode, null);
        }

        return pageNode.addNode("jcr:content", "nt:unstructured");
    }

    /**
     * Gets the categories.
     *
     * @param blueprint The blueprint.
     * @return The categories found.
     * @throws CommerceException Error while creating a catalog.
     * @throws RepositoryException Error while modifying JCR repository.
     */
    public static List<Resource> getCategories(final Page blueprint) throws CommerceException, RepositoryException
    {
        final ResourceResolver resolver = blueprint.adaptTo(Resource.class).getResourceResolver();
        final InheritanceValueMap blueprintProps = new HierarchyNodeInheritanceValueMap(blueprint.getContentResource());
        final String baseCategoryPath = blueprintProps.getInherited("filter/baseCategoryPath", String.class);

        return findCategories(resolver, baseCategoryPath);
    }

    /**
     * Find categories.
     *
     * @param resolver The resource resolver.
     * @param baseCategoryPath The base category path.
     * @return The category list.
     * @throws CommerceException Error while creating a catalog.
     * @throws RepositoryException Error while modifying JCR content.
     */
    protected static List<Resource> findCategories(final ResourceResolver resolver, final String baseCategoryPath)
            throws CommerceException, RepositoryException
    {
        final StringBuilder queryString = new StringBuilder(100);
        queryString.append("SELECT * FROM [nt:base] WHERE ISDESCENDANTNODE('").append(baseCategoryPath)
                .append("') AND [cq:hybrisId] IS NOT NULL");

        final List<Resource> categoryList = new ArrayList<Resource>();

        final Iterator<Resource> resources = resolver.findResources(queryString.toString(), Query.JCR_SQL2);
        while (resources.hasNext())
        {
            final Resource categoryResource = resources.next();
            categoryList.add(categoryResource);
        }

        return categoryList;
    }

    /**
     * Gets catalog roll out configuration.
     *
     * @param master The master page.
     * @param target The target page.
     * @return The instance.
     */
    public static String[] getCatalogRolloutConfigs(final Page master, final Page target)
    {
        String[] rollOutConfigs = {};
        try
        {
            final String catalogPath = target.getPath();
            final Resource syncConfig = master.getContentResource("cq:CatalogSyncConfig");
            for (final Iterator<Resource> instanceIterator = syncConfig.listChildren(); instanceIterator.hasNext();)
            {
                final ValueMap instance = instanceIterator.next().adaptTo(ValueMap.class);
                if (instance.get("cq:catalogPath", "").equals(catalogPath))
                {
                    rollOutConfigs = instance.get("cq:rolloutConfigs", String[].class);
                    return rollOutConfigs;
                }
            }
            LOG.error("Catalog instance {} not found in blueprint {}.", catalogPath, master.getPath());
        } catch (final Exception e)
        {
            LOG.error("Failed to fetch catalogInstances for blueprint {}.", master.getPath(), e);
        }

        return rollOutConfigs;
    }
    
    /**
     * Gets the rollout configurations.
     * 
     * @param resolver The resolver.
     * @param configPaths The configuration paths.
     * @return The rollout configurations.
     * @throws WCMException Error while reading rollout configurations.
     */
    public static RolloutConfig[] getConfigs(final ResourceResolver resolver, final String[] configPaths)
            throws WCMException
    {
        final List<RolloutConfig> rolloutConfigurations = new ArrayList<RolloutConfig>();

        final RolloutConfigManager rolloutConfigManager = resolver.adaptTo(RolloutConfigManager.class);
        if (configPaths != null)
        {
            for (final String path : configPaths)
            {
                final RolloutConfig rolloutConfig = rolloutConfigManager.getRolloutConfig(path);
                if (rolloutConfig != null)
                {
                    rolloutConfigurations.add(rolloutConfig);
                }
            }
        }

        return rolloutConfigurations.toArray(new RolloutConfig[rolloutConfigurations.size()]);
    }

    /**
     * Default Constructor.
     *
     * Private constructor to restrict instantiation.
     */
    private CatalogUtils()
    {

    }

}

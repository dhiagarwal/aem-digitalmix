package com.dtc.hybris.dto.product;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Media items class. Used for unmarshalling the Hybris REST API response.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
        name = "", propOrder = { "mediaItem" })
@XmlRootElement(
        name = "mediaItems")
@SuppressWarnings("PMD.BeanMembersShouldSerialize")
public class MediaItems
{
    private List<MediaItem> mediaItem;

    /**
     * Gets the media items.
     * 
     * @return The media items.
     */
    public final List<MediaItem> getMediaItem()
    {
        if (mediaItem == null)
        {
            mediaItem = new ArrayList<MediaItem>();
        }

        return this.mediaItem;
    }

}

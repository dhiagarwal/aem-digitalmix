package com.dtc.hybris.common;

import com.dtc.commerce.api.CommerceException;

/**
 * Hybris exception.
 */
public class HybrisException extends CommerceException
{
    private static final long serialVersionUID = 1L;

    private final transient int status;
    private final transient String body;

    /**
     * Hybris exception with message, status and body.
     * 
     * @param message The message.
     * @param status The status.
     * @param body The body.
     */
    public HybrisException(final String message, final int status, final String body)
    {
        super(
                message + "; hybris response status: [" + status + "], body: [" + body + "]");
        this.status = status;
        this.body = body;
    }

    /**
     * Gets the status.
     * 
     * @return The status.
     */
    public final int getStatus()
    {
        return this.status;
    }

    /**
     * Gets the body.
     * 
     * @return The body.
     */
    public final String getBody()
    {
        return this.body;
    }
}

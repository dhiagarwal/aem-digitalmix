package com.dtc.hybris.importer;

import static com.dtc.hybris.constants.Constants.COMMERCE_BASE_PATH;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Session;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dtc.hybris.DtcHybrisUtil;

/**
 * Import context.
 */
public class ImporterContext
{
    private static final Logger LOG = LoggerFactory.getLogger(ImporterContext.class);
    private final transient ResourceResolver resourceResolver;
    private final transient String catalogVersion;
    private final transient String country;
    private final transient PrintWriter writer;

    private final transient Resource baseResource;
    private final transient String baseStore;
    private final transient String catalog;
    private final transient boolean expressUpdate;

    private final transient String baseStorePath;
    private final transient String countryPath;
    private final transient String productBasePath;
    //private final transient String categoryBasePath;

    private transient Map<String, String> categories;
    private transient Map<String, String> categoryTags;
    
    private final transient boolean publishRunMode;

    private transient boolean hasErrors;

    /**
     * Constructor for importer context.
     * 
     * @param baseResource The base resource.
     * @param baseStore The base store.
     * @param catalog The catalog.
     * @param catalogVersion The catalog version.
     * @param language The language.
     * @param writer The Writer.
     */
    public ImporterContext(final Resource baseResource, final String baseStore, final String catalog,
            final String catalogVersion, final String language, final PrintWriter writer, final boolean publishRunMode)
    {
        this(baseResource, baseStore, catalog, catalogVersion, language, writer, false, publishRunMode);
    }

    /**
     * Constructor for importer context.
     * 
     * @param baseResource The base resource.
     * @param baseStore The base store.
     * @param catalog The catalog.
     * @param catalogVersion The catalog version.
     * @param language The language.
     * @param writer The writer.
     * @param expressUpdate The express update.
     */
    public ImporterContext(final Resource baseResource, final String baseStore, final String catalog,
            final String catalogVersion, final String country, final PrintWriter writer, final boolean expressUpdate, final boolean publishRunMode)
    {
        this.resourceResolver = baseResource.getResourceResolver();
        this.baseResource = baseResource;
        this.baseStore = baseStore;
        this.catalog = catalog;
        this.catalogVersion = catalogVersion;
        this.country = country;
        this.writer = writer;
        this.expressUpdate = expressUpdate;
        this.publishRunMode = publishRunMode;

        this.baseStorePath = new StringBuilder().append(COMMERCE_BASE_PATH).append("/").append(baseStore).toString();

        /*this.languagePath = new StringBuilder().append(baseStorePath).append("/").append(catalog).append("/")
                .append(language).toString();*/
        
        this.countryPath = new StringBuilder().append(COMMERCE_BASE_PATH).append("/").append("dtc").append("/").append(country).toString();

        /*this.productBasePath = new StringBuilder().append(languagePath).append("/").append("products").toString();*/
        this.productBasePath = new StringBuilder().append(COMMERCE_BASE_PATH).append("/").append("dtc").append("/").append(country).toString();
        /*this.categoryBasePath = new StringBuilder().append(languagePath).append("/").append("categories").toString();*/

        this.categories = new HashMap<String, String>();
        this.categoryTags = new HashMap<String, String>();
    }

    /**
     * Gets the base store path.
     * 
     * @return The base story path.
     */
    public final String getBaseStorePath()
    {
        return this.baseStorePath;
    }

    /**
     * Gets the language path.
     * 
     * @return The language path.
     */
    /*public final String getLanguagePath()
    {
        return this.languagePath;
    }*/

    /**
     * Gets product base path.
     * 
     * @return The product base path.
     */
    public final String getProductBasePath()
    {
        return this.productBasePath;
    }

    /**
     * Gets the category base path.
     * 
     * @return The category base path.
     */
    /*public final String getCategoryBasePath()
    {
        return this.categoryBasePath;
    }*/

    /**
     * Gets the product path.
     * 
     * @param code The product code.
     * @return The product path.
     */
    public final String getProductPath(final String code)
    {
        return DtcHybrisUtil.getProductPath(getProductBasePath(), code);
    }

    /**
     * Gets the resource resolver.
     * 
     * @return The resource resolver.
     */
    public final ResourceResolver getResourceResolver()
    {
        return this.resourceResolver;
    }

    /**
     * Gets the session.
     * 
     * @return The session.
     */
    public final Session getSession()
    {
        return getResourceResolver().adaptTo(Session.class);
    }

    /**
     * Adds the tag.
     * 
     * @param categoryCode The category code.
     * @param tagID The tag Id.
     */
    public final void addTag(final String categoryCode, final String tagID)
    {
        this.categoryTags.put(categoryCode, tagID);
    }

    /**
     * Gets the category code.
     * 
     * @param categoryCode The category code.
     * @return The category tag id.
     */
    public final String getCategoryTagID(final String categoryCode)
    {
        return this.categoryTags.get(categoryCode);
    }

    /**
     * Gets the parent categories.
     * 
     * @param categoryCode The category code.
     * @return The parent categories.
     */
    public final List<String> getParentCategories(final String categoryCode)
    {
        final List<String> parentCategories = new ArrayList<String>();

        String parentCategoryCode = categoryCode;

        while (this.categories.containsKey(parentCategoryCode))
        {
            parentCategoryCode = this.categories.get(parentCategoryCode);
            parentCategories.add(parentCategoryCode);
        }

        return parentCategories;
    }

    /**
     * Gets the base resource.
     * 
     * @return The base resource.
     */
    public final Resource getBaseResource()
    {
        return this.baseResource;
    }

    /**
     * Indicates if its an express update.
     * 
     * @return If its an express update.
     */
    public final boolean isExpressUpdate()
    {
        return this.expressUpdate;
    }

    /**
     * Gets the catalog.
     * 
     * @return The catalog.
     */
    public final String getCatalog()
    {
        return this.catalog;
    }

    /**
     * Gets the base store.
     * 
     * @return The base store.
     */
    public final String getBaseStore()
    {
        return this.baseStore;
    }

    /**
     * Gets the catalog version.
     * 
     * @return The catalog version.
     */
    public final String getCatalogVersion()
    {
        return this.catalogVersion;
    }
    
    
    /**
     * Check if it is publish environment.
     * 
     * @return True if environment is publish else false.
     */
    public boolean isPublishRunMode() {
		return publishRunMode;
	}

    /**
     * Gets the language.
     * 
     * @return The language.
     */
    /*public final String getLanguage()
    {
        return this.language;
    }*/

    /**
     * Prints error message.
     * 
     * @param message The message.
     */
    public final void error(final String message)
    {
        if (this.writer != null)
        {
            this.writer.append("E ").append(message).append("\n");
            this.writer.flush();
        }
        else{
          LOG.error("E " + message + "\n");
        }

        setHasErrors(true);
    }

    /**
     * Prints info message.
     * 
     * @param operation The operation.
     * @param message The message.
     */
    public final void info(final String operation, final String message)
    {
        if (this.writer != null)
        {
            this.writer.append(operation).append(" ").append(message).append("\n");
            this.writer.flush();
        }
        else{
          LOG.info(operation + " " + message + "\n");
        }
    }

    /**
     * Indicates  when error occurred during import.
     * 
     * @return The check to indicate when error occurred during import.
     */
    public final boolean isHasErrors()
    {
        return hasErrors;
    }

    /**
     * Sets when error occurred during import.
     * 
     * @param hasErrors The check to indicate when error occurred during import.
     */
    public final void setHasErrors(final boolean hasErrors)
    {
        this.hasErrors = hasErrors;
    }

}

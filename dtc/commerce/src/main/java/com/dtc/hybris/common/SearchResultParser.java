package com.dtc.hybris.common;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.adobe.cq.commerce.api.CommerceFacet;
import com.adobe.cq.commerce.api.CommerceFacetValue;
import com.adobe.cq.commerce.api.CommerceQuery;
import com.adobe.cq.commerce.api.CommerceSort;
import com.adobe.cq.commerce.api.PaginationInfo;
import com.adobe.cq.commerce.common.DefaultCommerceFacet;
import com.adobe.cq.commerce.common.DefaultCommerceFacetValue;
import com.adobe.cq.commerce.common.DefaultCommerceSort;
import com.adobe.cq.commerce.common.DefaultPaginationInfo;
import com.dtc.commerce.api.DtcCommerceContext;
import com.dtc.commerce.api.CommerceResult;
import com.dtc.commerce.api.DtcItem;
import com.dtc.commerce.common.DefaultCommerceResult;
import com.dtc.hybris.factories.DtcItemFactory;

//CHECKSTYLE:OFF
@SuppressWarnings({ "PMD.CyclomaticComplexity", "PMD.NPathComplexity", "PMD.NcssMethodCount",
        "PMD.ExcessiveMethodLength", "PMD.AvoidStringBufferField", "PMD.TooManyFields", "PMD.AccessorClassGeneration",
        "PMD.GodClass", "PMD.ConfusingTernary" })
public class SearchResultParser
{
    protected static final Logger LOG = LoggerFactory.getLogger(SearchResultParser.class);

    protected transient DtcItemFactory dtcItemFactory;
    protected transient DtcCommerceContext commerceContext;
    protected transient List<DtcItem> products;
    protected transient List<CommerceSort> sorts;
    protected transient List<CommerceFacet> facets;
    protected transient PaginationInfo paginationInfo;
    protected transient CommerceQuery actualQuery;

    public SearchResultParser(final DtcItemFactory dtcItemFactory, final DtcCommerceContext commerceContext)
    {
        this.dtcItemFactory = dtcItemFactory;
        this.commerceContext = commerceContext;
    }

    public CommerceResult parse(final CommerceQuery query, final InputStream inputStream) throws SAXException,
            IOException, ParserConfigurationException
    {
        this.products = new ArrayList<DtcItem>();
        this.sorts = new ArrayList<CommerceSort>();
        this.facets = new ArrayList<CommerceFacet>();

        SAXParserFactory.newInstance().newSAXParser().parse(inputStream, new ParseHandler());

        final DefaultCommerceResult result = new DefaultCommerceResult();
        result.setActualQuery(this.actualQuery);
        result.setFacets(this.facets);
        result.setOriginalQuery(query);
        result.setPaginationInfo(this.paginationInfo);
        result.setProducts(this.products);
        result.setSorts(this.sorts);

        return result;
    }

    private static class HybrisBreadcrumb
    {

        private String facetId;
        private String facetName;
        private String valueId;
        private String valueName;

        public String getFacetId()
        {
            return this.facetId;
        }

        public String getFacetName()
        {
            return this.facetName;
        }

        public String getValueId()
        {
            return this.valueId;
        }

        public String getValueName()
        {
            return this.valueName;
        }

        public void setFacetId(final String facetId)
        {
            this.facetId = facetId;
        }

        public void setFacetName(final String facetName)
        {
            this.facetName = facetName;
        }

        public void setValueId(final String valueId)
        {
            this.valueId = valueId;
        }

        public void setValueName(final String valueName)
        {
            this.valueName = valueName;
        }
    }

    private class ParseHandler extends DefaultHandler
    {
        private static final String PRODUCT_TAG = "product";
        private static final String URL_TAG = "url";
        private static final String SORT_TAG = "sort";
        private static final String CODE_TAG = "code";
        private static final String NAME_TAG = "name";
        private static final String SELECTED_TAG = "selected";
        private static final String PAGINATION_TAG = "pagination";
        private static final String PAGESIZE_TAG = "pageSize";
        private static final String CURRENTPAGE_TAG = "currentPage";
        private static final String TOTALPAGES_TAG = "totalPages";
        private static final String TOTALRESULTS_TAG = "totalResults";
        private static final String CURRENTQUERY_TAG = "currentQuery";
        private static final String FACETS_TAG = "facets";
        private static final String FACET_TAG = "facet";
        private static final String MULTISELECT_TAG = "multiSelect";
        private static final String TOPVALUES_TAG = "topValues";
        private static final String VALUES_TAG = "values";
        private static final String VALUE_TAG = "value";
        private static final String COUNT_TAG = "count";
        private static final String QUERY_TAG = "query";
        private static final String BREADCRUMBS_TAG = "breadcrumbs";
        private static final String BREADCRUMB_TAG = "de.hybris.platform.commerceservices.search.facetdata.BreadcrumbData";
        private static final String FACETCODE_TAG = "facetCode";
        private static final String FACETNAME_TAG = "facetName";
        private static final String VALUECODE_TAG = "facetValueCode";
        private static final String VALUENAME_TAG = "facetValueName";
        private static final String BOOLEAN_TRUE = "true";
        private static final String URL_REGEX = "/([^/]+)$";
        private transient boolean inProduct;
        private transient boolean inUrl;
        private transient boolean inSort;
        private transient boolean inCode;
        private transient boolean inName;
        private transient boolean inSelected;
        private transient boolean inPagination;
        private transient boolean inCurrentQuery;
        private transient boolean inFacets;
        private transient boolean inFacet;
        private transient boolean inFacetTopValues;
        private transient boolean inFacetValues;
        private transient boolean inValue;
        private transient boolean inBreadcrumbs;
        private transient boolean inBreadcrumb;
        private transient DefaultCommerceSort currentSort;
        private transient DefaultPaginationInfo pagination;
        private transient DefaultCommerceFacet currentFacet;
        private transient String currentFacetId;
        private transient DefaultCommerceFacetValue currentValue;
        private transient List<SearchResultParser.HybrisBreadcrumb> breadcrumbs;
        private transient SearchResultParser.HybrisBreadcrumb currentBreadcrumb;
        private transient final StringBuilder cdata = new StringBuilder();

        private ParseHandler()
        {
        }

        public void startElement(final String uri, final String localName, final String qName,
                final Attributes attributes) throws SAXException
        {
            if (PRODUCT_TAG.equalsIgnoreCase(qName))
            {
                this.inProduct = true;
            } else if (URL_TAG.equalsIgnoreCase(qName))
            {
                this.inUrl = true;
            } else if ((SORT_TAG.equalsIgnoreCase(qName)) && (!this.inPagination))
            {
                this.inSort = true;
                this.currentSort = new DefaultCommerceSort();
            } else if (CODE_TAG.equalsIgnoreCase(qName))
            {
                this.inCode = true;
            } else if (NAME_TAG.equalsIgnoreCase(qName))
            {
                this.inName = true;
            } else if (SELECTED_TAG.equalsIgnoreCase(qName))
            {
                this.inSelected = true;
            } else if (PAGINATION_TAG.equalsIgnoreCase(qName))
            {
                this.inPagination = true;
                this.pagination = new DefaultPaginationInfo();
            } else if (CURRENTQUERY_TAG.equalsIgnoreCase(qName))
            {
                this.inCurrentQuery = true;
            } else if (FACETS_TAG.equalsIgnoreCase(qName))
            {
                this.inFacets = true;
            } else if ((FACET_TAG.equalsIgnoreCase(qName)) && (this.inFacets))
            {
                this.inFacet = true;
                this.currentFacet = new DefaultCommerceFacet();
                this.currentFacet.setValues(new ArrayList<CommerceFacetValue>());
            } else if ((TOPVALUES_TAG.equalsIgnoreCase(qName)) && (this.inFacet))
            {
                this.inFacetTopValues = true;
            } else if ((VALUES_TAG.equalsIgnoreCase(qName)) && (this.inFacet))
            {
                this.inFacetValues = true;
            } else if ((VALUE_TAG.equalsIgnoreCase(qName)) && (this.inFacetValues))
            {
                this.inValue = true;
                this.currentValue = new DefaultCommerceFacetValue();
            } else if (BREADCRUMBS_TAG.equalsIgnoreCase(qName))
            {
                this.inBreadcrumbs = true;
                this.breadcrumbs = new ArrayList<HybrisBreadcrumb>();
            } else if ((BREADCRUMB_TAG.equalsIgnoreCase(qName)) && (this.inBreadcrumbs))
            {
                this.inBreadcrumb = true;
                this.currentBreadcrumb = new SearchResultParser.HybrisBreadcrumb();
            }

            this.cdata.setLength(0);
        }

        public void characters(final char[] characters, final int start, final int length) throws SAXException
        {
            if ((this.inProduct) && (this.inUrl))
            {
                this.cdata.append(new String(characters, start, length));
            }
            if ((this.inSort) && ((this.inCode) || (this.inName) || (this.inSelected)))
            {
                this.cdata.append(new String(characters, start, length));
            }
            if (this.inPagination)
            {
                this.cdata.append(new String(characters, start, length));
            }
            if (this.inCurrentQuery)
            {
                this.cdata.append(new String(characters, start, length));
            }
            if (this.inFacet)
            {
                this.cdata.append(new String(characters, start, length));
            }
            if (this.inBreadcrumb)
            {
                this.cdata.append(new String(characters, start, length));
            }
        }

        public void endElement(final String uri, final String localName, final String qName) throws SAXException
        {
            if (this.inProduct)
            {
                if (URL_TAG.equalsIgnoreCase(qName))
                {
                    final String url = this.cdata.toString();
                    final Matcher matcher = Pattern.compile(URL_REGEX).matcher(url);
                    if (matcher.find())
                    {
                        final String productId = matcher.group(1);
                        
                        final DtcItem product = SearchResultParser.this.dtcItemFactory.create(
                                commerceContext.getRequest(), commerceContext.getBaseProductDataPath(), productId);
                        
                        if (product != null)
                        {
                            SearchResultParser.this.products.add(product);
                        } else
                        {
                            SearchResultParser.LOG.warn("Could not find product ({}:{}) returned by search", url,
                                    productId);
                        }
                    } else
                    {
                        SearchResultParser.LOG.warn("Could not find product id ({})", url);
                    }
                    this.inUrl = false;
                } else if (PRODUCT_TAG.equalsIgnoreCase(qName))
                {
                    this.inProduct = false;
                }
            } else if (this.inSort)
            {
                if (CODE_TAG.equalsIgnoreCase(qName))
                {
                    this.currentSort.setId(this.cdata.toString());
                    this.inCode = false;
                } else if (NAME_TAG.equalsIgnoreCase(qName))
                {
                    this.currentSort.setDisplayName(this.cdata.toString());
                    this.inName = false;
                } else if (SELECTED_TAG.equalsIgnoreCase(qName))
                {
                    this.currentSort.setSelected(BOOLEAN_TRUE.equalsIgnoreCase(this.cdata.toString()));
                    this.inSelected = false;
                } else if (SORT_TAG.equalsIgnoreCase(qName))
                {
                    SearchResultParser.this.sorts.add(this.currentSort);
                    this.inSort = false;
                }
            } else if (this.inPagination)
            {
                if (PAGESIZE_TAG.equalsIgnoreCase(qName))
                {
                    this.pagination.setPageSize(Integer.parseInt(this.cdata.toString()));
                } else if (CURRENTPAGE_TAG.equalsIgnoreCase(qName))
                {
                    this.pagination.setCurrentPage(Integer.parseInt(this.cdata.toString()));
                } else if (TOTALPAGES_TAG.equalsIgnoreCase(qName))
                {
                    this.pagination.setTotalPages(Integer.parseInt(this.cdata.toString()));
                } else if (TOTALRESULTS_TAG.equalsIgnoreCase(qName))
                {
                    this.pagination.setTotalResults(Integer.parseInt(this.cdata.toString()));
                } else if (PAGINATION_TAG.equalsIgnoreCase(qName))
                {
                    SearchResultParser.this.paginationInfo = this.pagination;
                    this.inPagination = false;
                }
            } else if (this.inCurrentQuery)
            {
                if (CURRENTQUERY_TAG.equalsIgnoreCase(qName))
                {
                    final String query = this.cdata.toString();
                    final String[] queryParts = query.split(":");
                    final CommerceQuery.Builder builder = new CommerceQuery.Builder();
                    builder.setQueryText(queryParts[0]);
                    if (queryParts.length > 1)
                    {
                        builder.setSortId(queryParts[1]);
                    }
                    SearchResultParser.this.actualQuery = builder.build();
                    this.inCurrentQuery = false;
                }
            } else if (this.inFacets)
            {
                if (this.inFacet)
                {
                    if (this.inFacetValues)
                    {
                        if (this.inValue)
                        {
                            if (NAME_TAG.equalsIgnoreCase(qName))
                            {
                                this.currentValue.setDisplayName(this.cdata.toString());
                            } else if (COUNT_TAG.equalsIgnoreCase(qName))
                            {
                                if (StringUtils.isNumeric(this.cdata.toString()))
                                {
                                    this.currentValue.setProductCount(Integer.parseInt(this.cdata.toString()));
                                }
                            } else if (QUERY_TAG.equalsIgnoreCase(qName))
                            {
                                final String[] parts = this.cdata.toString().split(":");
                                if (this.currentFacet.getId() == null)
                                {
                                    this.currentFacetId = parts[(parts.length - 2)];
                                }
                                this.currentValue.setId(parts[(parts.length - 1)]);
                            } else if (SELECTED_TAG.equalsIgnoreCase(qName))
                            {
                                this.currentValue.setSelected(BOOLEAN_TRUE.equalsIgnoreCase(this.cdata.toString()));
                            } else if (VALUE_TAG.equalsIgnoreCase(qName))
                            {
                                this.inValue = false;

                                if ((this.currentFacet.getId() == null) && (this.currentFacetId != null)
                                        && (!this.currentValue.isSelected()))
                                {
                                    this.currentFacet.setId(this.currentFacetId);
                                }
                                this.currentFacet.getValues().add(this.currentValue);
                                this.currentFacetId = null;
                            }
                        } else if (VALUES_TAG.equalsIgnoreCase(qName))
                        {
                            this.inFacetValues = false;
                        }
                    } else if (this.inFacetTopValues)
                    {
                        if (TOPVALUES_TAG.equalsIgnoreCase(qName))
                        {
                            this.inFacetTopValues = false;
                        }
                    } else if (NAME_TAG.equalsIgnoreCase(qName))
                    {
                        this.currentFacet.setDisplayName(this.cdata.toString());
                    } else if (MULTISELECT_TAG.equalsIgnoreCase(qName))
                    {
                        this.currentFacet.setMultiSelect(BOOLEAN_TRUE.equalsIgnoreCase(this.cdata.toString()));
                    } else if (FACET_TAG.equalsIgnoreCase(qName))
                    {
                        SearchResultParser.this.facets.add(this.currentFacet);
                        this.inFacet = false;
                    }
                } else if (FACETS_TAG.equalsIgnoreCase(qName))
                {
                    this.inFacets = false;
                }
            } else if (this.inBreadcrumbs)
            {
                if (this.inBreadcrumb)
                {
                    if (FACETCODE_TAG.equalsIgnoreCase(qName))
                    {
                        this.currentBreadcrumb.setFacetId(this.cdata.toString());
                    } else if (FACETNAME_TAG.equalsIgnoreCase(qName))
                    {
                        this.currentBreadcrumb.setFacetName(this.cdata.toString());
                    } else if (VALUENAME_TAG.equalsIgnoreCase(qName))
                    {
                        this.currentBreadcrumb.setValueName(this.cdata.toString());
                    } else if (VALUECODE_TAG.equalsIgnoreCase(qName))
                    {
                        this.currentBreadcrumb.setValueId(this.cdata.toString());
                    } else if (BREADCRUMB_TAG.equalsIgnoreCase(qName))
                    {
                        this.breadcrumbs.add(this.currentBreadcrumb);
                        this.inBreadcrumb = false;
                    }
                } else if (BREADCRUMBS_TAG.equalsIgnoreCase(qName))
                {
                    this.inBreadcrumbs = false;
                }
            }
        }

        public void endDocument() throws SAXException
        {
            SearchResultParser.HybrisBreadcrumb breadcrumb;
            for (final Iterator<SearchResultParser.HybrisBreadcrumb> i = this.breadcrumbs.iterator(); i.hasNext();)
            {
                breadcrumb = (SearchResultParser.HybrisBreadcrumb) i.next();
                for (final CommerceFacet facet : SearchResultParser.this.facets)
                {
                    if (facet.getDisplayName().equals(breadcrumb.getFacetName()))
                    {
                        for (final CommerceFacetValue value : facet.getValues())
                        {
                            if (value.getDisplayName().equals(breadcrumb.getValueName()))
                            {
                                ((DefaultCommerceFacetValue) value).setId(breadcrumb.getValueId());
                            }
                        }

                        if (facet.getId() != null)
                        {
                            break;
                        }

                        ((DefaultCommerceFacet) facet).setId(breadcrumb.getFacetId());
                        break;
                    }
                }
            }
        }
    }
}
//CHECKSTYLE:ON

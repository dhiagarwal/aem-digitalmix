package com.dtc.hybris.connection;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.EncoderException;
import org.apache.commons.codec.net.URLCodec;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Transformer;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.lang3.StringUtils;

/**
 * Abstract Hybris command.
 */
@SuppressWarnings({ "PMD.EmptyMethodInAbstractClassShouldBeAbstract" })
public abstract class AbstractHybrisCommand implements HybrisCommand
{
    /**
     * URL Codec.
     */
    protected static final URLCodec URL_CODEC = new URLCodec();

    private final transient ArrayList<NameValuePair> parameters = new ArrayList<NameValuePair>();
    private final transient ArrayList<NameValuePair> headers = new ArrayList<NameValuePair>();
    private String method;
    private String path;

    /**
     * Abstract Hybris command.
     */
    public AbstractHybrisCommand()
    {
        setMethod("GET");
    }

    // CHECKSTYLE:OFF
    @Override
    // CHECKSTYLE:ON
    public String getMethod()
    {
        return this.method;
    }

    // CHECKSTYLE:OFF
    @Override
    // CHECKSTYLE:ON
    public String getPath()
    {
        return this.path;
    }

    // CHECKSTYLE:OFF
    @Override
    // CHECKSTYLE:ON
    public List<NameValuePair> getHeaders()
    {
        return this.headers;
    }

    // CHECKSTYLE:OFF
    @Override
    // CHECKSTYLE:ON
    public String getRequestBody()
    {
        return "";
    }

    // CHECKSTYLE:OFF
    @Override
    // CHECKSTYLE:ON
    public InputStream getRequestBodyStream()
    {
        return null;
    }

    // CHECKSTYLE:OFF
    @Override
    // CHECKSTYLE:ON
    public String getRequestContentType()
    {
        return "application/x-www-form-urlencoded";
    }

    // CHECKSTYLE:OFF
    @Override
    // CHECKSTYLE:ON
    public boolean needsServerAuth()
    {
        return false;
    }

    // CHECKSTYLE:OFF
    @Override
    // CHECKSTYLE:ON
    public String getQueryString()
    {
        final LinkedList<NameValuePair> output = new LinkedList<NameValuePair>();

        CollectionUtils.collect(this.parameters, new Transformer()
        {
            public Object transform(final Object input)
            {
                final NameValuePair pair = (NameValuePair) input;
                return AbstractHybrisCommand.this.urlEncode(pair.getName()) + "="
                        + AbstractHybrisCommand.this.urlEncode(pair.getValue());
            }
        }, output);

        return StringUtils.join(output.toArray(), "&");
    }

    /**
     * Add a parameter.
     * 
     * @param key The parameter key.
     * @param value The parameter value.
     */
    protected final void addParameter(final String key, final String value)
    {
        if (StringUtils.isBlank(key))
        {
            throw new IllegalArgumentException("key may not be blank");
        }

        if (StringUtils.isNotBlank(value))
        {
            this.parameters.add(new NameValuePair(key, value));
        }
    }

    /**
     * Add a header.
     * 
     * @param key The header key.
     * @param value The header value.
     */
    protected final void addHeader(final String key, final String value)
    {
        if (StringUtils.isBlank(key))
        {
            throw new IllegalArgumentException("key may not be blank");
        }

        if (StringUtils.isNotBlank(value))
        {
            this.headers.add(new NameValuePair(key, value));
        }
    }

    /**
     * Sets the method.
     * 
     * @param method The method.
     */
    protected final void setMethod(final String method)
    {
        this.method = method;
    }

    /**
     * Sets the path.
     * 
     * @param path The path.
     */
    protected final void setPath(final String path)
    {
        this.path = path;
    }

    /**
     * Encode URL.
     * 
     * @param urlString The URL string.
     * @return The encoded URL string.
     */
    protected final String urlEncode(final String urlString)
    {
        try
        {
            return URL_CODEC.encode(urlString);
        } catch (EncoderException e)
        {
            return urlString;
        }
    }

    /**
     * Add user headers.
     * 
     * @param userHeaders The user header.
     */
    protected final void addUserHeaders(final Map<String, String> userHeaders)
    {
        addHeader("lastName", userHeaders.get("lastname"));
        addHeader("firstname", userHeaders.get("firstname"));
    }

}

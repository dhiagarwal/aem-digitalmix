package com.dtc.hybris.importer;

import java.io.IOException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;

/**
 * Hybris importer.
 */
public interface HybrisImporter
{
    /**
     * Import catalog.
     * 
     * @param request The Sling HTTP servlet request.
     * @param response The Sling HTTP servlet response.
     * @throws IOException Error writing response.
     */
    void importCatalog(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException;
    
    void importCatalog(ResourceResolver resolver, String baseStore, String catalog, String country) throws IOException;

}

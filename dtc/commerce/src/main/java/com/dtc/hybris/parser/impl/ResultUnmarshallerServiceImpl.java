package com.dtc.hybris.parser.impl;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Deactivate;
import org.apache.felix.scr.annotations.Service;

import com.dtc.hybris.connection.CommandResult;
import com.dtc.hybris.parser.ResultUnmarshallerService;

/**
 * Service for unmarhsalling results to JAXB mapped class.
 */
@Service(ResultUnmarshallerService.class)
@Component
public final class ResultUnmarshallerServiceImpl implements ResultUnmarshallerService
{
    private transient XMLInputFactory xmlFactory;

    @Override
    @SuppressWarnings("unchecked")
    public <T extends Object> T unmarshalResult(final CommandResult result, final Class<T> declaredType)
            throws JAXBException, XMLStreamException
    {
        XMLEventReader xml = null;

        try
        {
        	
            xml = xmlFactory.createXMLEventReader(result.getBody());

            final JAXBContext jaxbContext = JAXBContext.newInstance(declaredType);
            final Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
           

            return (T) jaxbUnmarshaller.unmarshal(xml);

        } finally
        {
            if (xml != null)
            {
                xml.close();
            }
            if (result != null)
            {
                result.release();
            }
        }
    }

    /**
     * Activate method.
     */
    @Activate
    public void activate()
    {
        this.xmlFactory = XMLInputFactory.newInstance();
    }

    /**
     * Deactivate method.
     */
    @Deactivate
    public void deactivate()
    {
        this.xmlFactory = null;
    }

}

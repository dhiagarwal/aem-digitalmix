package com.dtc.hybris.connection;

import static com.dtc.hybris.constants.Constants.V1_FLAG;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.codec.net.URLCodec;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.DeleteMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.InputStreamRequestEntity;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.PutMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Deactivate;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.commerce.api.NotAuthorizedException;
import com.dtc.commerce.api.CommerceException;
import com.dtc.hybris.common.HybrisException;
import com.dtc.hybris.security.HybrisOAuthHandler;

/**
 * Dtc Hybris connection.
 */
@Component(
        metatype = true, label = "Dtc Commerce Hybris Connection Handler")
@Service(HybrisConnection.class)
@SuppressWarnings({ "PMD.GodClass", "PMD.TooManyMethods" })
public class DtcHybrisConnection implements HybrisConnection
{
    private static final Logger LOG = LoggerFactory.getLogger(DtcHybrisConnection.class);

    private static final URLCodec URL_CODEC = new URLCodec();

    private static final String V1_REST_END_POINT = "/dtcwebservices/v1/";
    private static final String V2_REST_END_POINT = "/dtcwebservices/v2/";
    private static final String DEFAULT_SERVER_URL = "http://localhost:9002";
    private static final String DEFAULT_SERVER_USERNAME = "admin";   
    private static final boolean DEFAULT_SERVER_SSL_TRUST_ALL_CERTS = true;
    private static final int DEFAULT_CONNECTION_TIMEOUT = 30000;
    private static final int DEFAULT_SOCKET_TIMEOUT = 30000;
    private static final int DEFAULT_MAX_CONNECTIONS = 20;
    private static final int DEFAULT_MAX_CONNECTIONS_PER_HOST = 2;
    private static final int CONNECTION_MANAGER_TIMEOUT = 3000;

    /**
     * Server URL property.
     */
    @Property({ DEFAULT_SERVER_URL })
    public static final String SERVER_URL = "hybris.server.url";

    /**
     * Server username property.
     */
    @Property({ DEFAULT_SERVER_USERNAME })
    public static final String SERVER_USERNAME_LABEL = "hybris.server.username";

    /**
     * Server password property.
     */
    @Property
    public static final String SERVER_PASSW_LABEL = "hybris.server.password";

    /**
     * Server SSL trust all certificates.
     */
    @Property(
            boolValue = { DEFAULT_SERVER_SSL_TRUST_ALL_CERTS })
    public static final String SERVER_SSL_TRUST_ALL_CERTS = "hybris.server.ssl.trust_all_certs";

    /**
     * Rest endpoint property.
     */
    @Property({ V1_REST_END_POINT })
    public static final String V1_REST_ENDPOINT = "hybris.server.v1_rest_endpoint";
    
    /**
     * Rest endpoint property.
     */
    @Property({ V2_REST_END_POINT })
    public static final String REST_ENDPOINT = "hybris.server.rest_endpoint";

    /**
     * Connection timeout property.
     */
    @Property(
            intValue = { DEFAULT_CONNECTION_TIMEOUT })
    public static final String CONNECTION_TIMEOUT = "hybris.http.timeout.connection";

    /**
     * Socket timeout property.
     */
    @Property(
            intValue = { DEFAULT_SOCKET_TIMEOUT })
    public static final String SOCKET_TIMEOUT = "hybris.http.timeout.socket";

    /**
     * Maximum connections property.
     */
    @Property(
            intValue = { DEFAULT_MAX_CONNECTIONS })
    public static final String MAX_CONNECTIONS = "hybris.http.max_connections";

    /**
     * Maximum connections per host property.
     */
    @Property(
            intValue = { DEFAULT_MAX_CONNECTIONS_PER_HOST })
    public static final String MAX_CONNECTIONS_PER_HOST = "hybris.http.max_connections_per_host";

    @Reference
    private transient HybrisOAuthHandler authHandler;

    private transient MultiThreadedHttpConnectionManager connectionManager;
    private transient String serverUrl;
    private transient String serverUsername;
    private transient String serverPassword;
    private transient String endpoint;
    private transient String v1endpoint;
    private transient Integer connectionTimeout;
    private transient Integer socketTimeout;
    private transient SSLContext originalSSLContext;
    
    

    public DtcHybrisConnection() {
		super();		
	}

	@Override
    public final HttpClient getConfiguredHttpClient()
    {
        final HttpClient http = new HttpClient(this.connectionManager);

        http.getParams().setParameter("http.connection.timeout", this.connectionTimeout);
        http.getParams().setParameter("http.socket.timeout", this.socketTimeout);
        http.getParams().setConnectionManagerTimeout(CONNECTION_MANAGER_TIMEOUT);

        return http;
    }

    /**
     * Gets the command path.
     * 
     * @param command The command path.
     * @param baseStore The base store.
     * @return The command path.
     */
    private String getCommandPath(final HybrisCommand command, final String baseStore)
    {
        final String commandPath = command.getPath();
        if (commandPath.startsWith(V1_FLAG))
        {
            return new StringBuffer().append(this.v1endpoint).append(baseStore)
                    .append(commandPath.replaceFirst(V1_FLAG, "")).toString();
        }

        return new StringBuffer().append(this.endpoint).append(baseStore).append(commandPath).toString();
    }

    /**
     * Activate.
     * 
     * @param props The OSGi configuration properties.
     */
    @Activate
    protected final void activate(final Map<String, Object> props)
    {
        final Integer maxConnectionsPerHost = Integer.valueOf(PropertiesUtil.toInteger(
                props.get(MAX_CONNECTIONS_PER_HOST), DEFAULT_MAX_CONNECTIONS_PER_HOST));
        final HttpConnectionManagerParams params = new HttpConnectionManagerParams();
        params.setDefaultMaxConnectionsPerHost(maxConnectionsPerHost.intValue());
        params.setMaxTotalConnections(PropertiesUtil.toInteger(props.get(MAX_CONNECTIONS), DEFAULT_MAX_CONNECTIONS));
        this.connectionManager = new MultiThreadedHttpConnectionManager();
        this.connectionManager.setParams(params);

        this.serverUrl = PropertiesUtil.toString(props.get(SERVER_URL), DEFAULT_SERVER_URL);
        this.serverUsername = PropertiesUtil.toString(props.get(SERVER_USERNAME_LABEL), DEFAULT_SERVER_USERNAME);
        this.serverPassword = PropertiesUtil.toString(props.get(SERVER_PASSW_LABEL),null);
        this.endpoint = PropertiesUtil.toString(props.get(REST_ENDPOINT), V2_REST_END_POINT);
        this.v1endpoint = PropertiesUtil.toString(props.get(V1_REST_ENDPOINT), V1_REST_END_POINT);
        this.connectionTimeout = Integer.valueOf(PropertiesUtil.toInteger(props.get(CONNECTION_TIMEOUT),
                DEFAULT_CONNECTION_TIMEOUT));
        this.socketTimeout = Integer
                .valueOf(PropertiesUtil.toInteger(props.get(SOCKET_TIMEOUT), DEFAULT_SOCKET_TIMEOUT));

        final boolean trustAllCerts = PropertiesUtil.toBoolean(props.get(SERVER_SSL_TRUST_ALL_CERTS),
                DEFAULT_SERVER_SSL_TRUST_ALL_CERTS);

        if (trustAllCerts)
        {
            final TrustManager[] trustManager = {new X509TrustManager()
            {
                public X509Certificate[] getAcceptedIssuers()
                {
                    return new X509Certificate[] {};
                }

                public void checkClientTrusted(final X509Certificate[] certs, final String authType)
                {
                    // Do nothing when check client is trusted.
                }

                public void checkServerTrusted(final X509Certificate[] certs, final String authType)
                {
                    // Do nothing when check sever is trusted.
                }
            } };
            try
            {
                final SSLContext sslContext = SSLContext.getInstance("TLS");
                sslContext.init(null, trustManager, new SecureRandom());
                this.originalSSLContext = SSLContext.getDefault();
                SSLContext.setDefault(sslContext);
            } catch (NoSuchAlgorithmException | KeyManagementException e)
            {
                LOG.warn("Exception occurred while installing custom trust manager", e);
            }

        }
    }

    /**
     * Deactivate.
     */
    @Deactivate
    protected final void deactivate()
    {
        this.connectionManager.shutdown();

        if (this.originalSSLContext != null)
        {
            SSLContext.setDefault(this.originalSSLContext);
            this.originalSSLContext = null;
        }
    }

    @Override
    public final CommandResult execute(final HybrisCommand command, final String baseStore) throws CommerceException
    {
        final Map<String, Object> context = new HashMap<String, Object>();
        final HybrisConnection.ConnectionOptions options = new HybrisConnection.ConnectionOptions(baseStore, context);
        return execute(command, options);
    }

    @Override
    @SuppressWarnings({ "PMD.ConfusingTernary", "PMD.NPathComplexity", "PMD.CyclomaticComplexity" })
    public final CommandResult execute(final HybrisCommand command, final ConnectionOptions options)
            throws CommerceException
    {
        final HttpClient httpClient = getConfiguredHttpClient();

        HttpMethod request = null;
        boolean releaseConnection = true;
        try
        {
            request = createRequestMethod(command, options.getBaseStore(), options.getContext());

            final long currentTimeMillis = System.currentTimeMillis();
            Map<String, String> newSessionData;

            if (null != options.getSessionData() && !options.getSessionData().isEmpty())
            {
                LOG.debug("Using session data to authenticate request.");
                newSessionData = this.authHandler.executeAuthenticated(httpClient, request, options.getSessionData());
            } else
            {
                if (command.needsServerAuth())
                {
                    LOG.debug("Using server username/password to authenticate request.");
                    newSessionData = this.authHandler.executeAuthenticated(this, httpClient, request,
                            this.serverUsername, this.serverPassword);
                } else
                {
                    LOG.debug("Not authenticating request.");
                    newSessionData = null;
                    httpClient.executeMethod(request);
                }
            }

            final int status = request.getStatusCode();

            LOG.debug(
                    "Sending {} to {} returned with status {} in {} ms",
                    new Object[] {command.getClass().getSimpleName(), request.getURI(), Integer.valueOf(status),
                            Long.valueOf(System.currentTimeMillis() - currentTimeMillis) });

            if (LOG.isTraceEnabled())
            {
                LOG.trace(request.getResponseBodyAsString());
            }

            if ((status == HttpStatus.SC_OK) || (status == HttpStatus.SC_CREATED) || (status == HttpStatus.SC_ACCEPTED))
            {
            	final CommandResult result = createCommandResult(newSessionData, request, status);
            	releaseConnection = false;
            	return result;
            }

            if ((status == HttpStatus.SC_FORBIDDEN) || (status == HttpStatus.SC_UNAUTHORIZED))
            {
                throw new NotAuthorizedException("You are not authorized.");
            }

            if (status == HttpStatus.SC_NOT_FOUND)
            {
                final CommandResult result = createCommandResult(newSessionData, request, status);               
                releaseConnection = false;                
                return result;
            }

            final String body = request.getResponseBodyAsString();
            final String error = "hybris response status: [" + status + "], body: [" + body + "]";
            LOG.error("execute: {}",error);
            throw new HybrisException("error executing command", status, body);
        } catch (HybrisException | NotAuthorizedException | IOException e)
        {
            throw new CommerceException("error executing command", e);
        } finally
        {
            if ((releaseConnection) && (request != null))
            {
                request.releaseConnection();
            }
        }
    }

    /**
     * Create POST method.
     * 
     * @param url The URL.
     * @param command The command.
     * @return The POST HTTP method.
     * @throws UnsupportedEncodingException Error creating method.
     */
    private HttpMethod postMethod(final String url, final HybrisCommand command) throws UnsupportedEncodingException
    {
        try
        {
            final PostMethod post = new PostMethod(url);
            RequestEntity entity;
            if (command.getRequestBodyStream() == null)
            {
                if (StringUtils.isNotBlank(command.getRequestBody()))
                {
                    entity = new StringRequestEntity(command.getRequestBody(),
                            command.getRequestContentType(), "utf-8");
                } else
                {
                    entity = null;
                }

            } else
            {
                entity = new InputStreamRequestEntity(command.getRequestBodyStream(), command.getRequestContentType());
            }

            if (entity != null)
            {
                post.setRequestEntity(entity);
            }

            return post;
        } catch (IllegalArgumentException e)
        {
            LOG.error("Error creating POST method, see debug log for details.");

            // Using debug log to prevent users email getting added to the error log.
            LOG.debug("Error creating POST method", e);
        }

        return null;
    }

    /**
     * Create PUT method.
     * 
     * @param url The URL.
     * @param command The command.
     * @return The PUT HTTP method.
     * @throws UnsupportedEncodingException Error creating method.
     */
    private HttpMethod putMethod(final String url, final HybrisCommand command) throws UnsupportedEncodingException
    {
        try
        {
            final PutMethod put = new PutMethod(url);
            RequestEntity entity;
            if (command.getRequestBodyStream() == null)
            {
                if (StringUtils.isNotBlank(command.getRequestBody()))
                {
                    entity = new StringRequestEntity(command.getRequestBody(),
                            command.getRequestContentType(), "utf-8");
                } else
                {
                    entity = null;
                }

            } else
            {
                entity = new InputStreamRequestEntity(command.getRequestBodyStream(), command.getRequestContentType());

            }
            if (entity != null)
            {
                put.setRequestEntity(entity);
            }
            put.setRequestEntity(entity);

            return put;
        } catch (IllegalArgumentException e)
        {
            LOG.error("Error creating PUT method, see debug log for details.");

            // Using debug log to prevent users email getting added to the error log.
            LOG.debug("Error creating PUT method", e);
        }

        return null;
    }

    /**
     * Creates GET method.
     * 
     * @param url The URL.
     * @return The GET HTTP method.
     */
    private GetMethod getMethod(final String url)
    {
        try
        {
            return new GetMethod(url);
        } catch (IllegalArgumentException e)
        {
            LOG.error("Error creating GET method, see debug log for details.");

            // Using debug log to prevent users email getting added to the error log.
            LOG.debug("Error creating GET method", e);
        }

        return null;

    }

    /**
     * Creates DELETE method.
     * 
     * @param url The URL.
     * @return The DELETE HTTP method.
     */
    private DeleteMethod deleteMethod(final String url)
    {
        try
        {
            return new DeleteMethod(url);
        } catch (IllegalArgumentException e)
        {
            LOG.error("Error creating DELETE method, see debug log for details.");

            // Using debug log to prevent users email getting added to the error log.
            LOG.debug("Error creating DELETE method", e);
        }

        return null;
    }

    /**
     * Create request method.
     * 
     * @param command The command.
     * @param baseStore The base story.
     * @param context The context.
     * @return The HTTP method.
     * @throws UnsupportedEncodingException Error creating method.
     */
    private HttpMethod createRequestMethod(final HybrisCommand command, final String baseStore,
            final Map<String, Object> context) throws UnsupportedEncodingException
    {
        final String url = this.serverUrl + getCommandPath(command, baseStore);
        HttpMethod request;

        switch (command.getMethod())
        {
            case "POST" :
                request = postMethod(url, command);
                break;
            case "PUT" :
                request = putMethod(url, command);
                break;
            case "DELETE" :
                request = deleteMethod(url);
                break;
            default :
                request = getMethod(url);
        }

        final StringBuilder queryString = new StringBuilder(command.getQueryString());
        if (context != null)
        {
            for (final Map.Entry<String, Object> entry : context.entrySet())
            {
                if (queryString.length() > 0)
                {
                    queryString.append('&');
                }
                queryString.append(entry.getKey() + "=" + URL_CODEC.encode((String) entry.getValue(), "utf-8"));
            }

        }

        if(request!=null){
        	request.setQueryString(queryString.toString());

        	request.addRequestHeader("Accept", "application/xml");

        	for (final NameValuePair header : command.getHeaders())
        	{
        		request.addRequestHeader(header.getName(), header.getValue());
        	}
        }
        
        return request;
    }

    /**
     * Create command result.
     * 
     * @param newSessionData The new session data.
     * @param request The request.
     * @param status The status code.
     * @return The command result.
     * @throws CommerceException Error creating command result.
     */
    private CommandResult createCommandResult(final Map<String, String> newSessionData, final HttpMethod request,
            final int status) throws CommerceException
    {

        final CommandResult result = new HybrisCommandResult(status, request, newSessionData);

        if (!result.success())
        {
            LOG.error("command failed to execute: status: [{}]", Integer.valueOf(status));
            throw new CommerceException("the command failed to execute");
        }
        return result;
    }

    @Override
    public final String getServerUrl()
    {
        return this.serverUrl;
    }

}

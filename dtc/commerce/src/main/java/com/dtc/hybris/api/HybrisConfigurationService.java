package com.dtc.hybris.api;

/**
 * Hybris configuration service.
 */
public interface HybrisConfigurationService
{
    /**
     * Gets the catalog version.
     * 
     * @return The catalog version.
     */
    String getCatalogVersion();
}

package com.dtc.hybris.dto.product;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Attributes class. Used for unmarshalling the Hybris REST API response.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
        name = "", propOrder = { "attribute" })
@XmlRootElement(
        name = "attributes")
@SuppressWarnings("PMD.BeanMembersShouldSerialize")
public class Attributes
{
    private List<Attribute> attribute;

    /**
     * Gets the attribute list.
     * 
     * @return The attribute list.
     */
    public final List<Attribute> getAttribute()
    {
        if (attribute == null)
        {
            attribute = new ArrayList<Attribute>();
        }

        return this.attribute;
    }

}

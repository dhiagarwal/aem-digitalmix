package com.dtc.hybris.dto.product;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Product categories class. Used for unmarshalling the Hybris REST API response.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
        name = "", propOrder = { "category" })
@XmlRootElement(
        name = "categories")
@SuppressWarnings("PMD.BeanMembersShouldSerialize")
public class ProductCategories
{
    private List<ProductCategory> category;

    /**
     * Gets the category list.
     * 
     * @return The category list.
     */
    public final List<ProductCategory> getCategory()
    {
        if (category == null)
        {
            category = new ArrayList<ProductCategory>();
        }
        return this.category;
    }

}

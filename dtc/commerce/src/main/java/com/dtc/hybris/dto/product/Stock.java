package com.dtc.hybris.dto.product;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * The Class Stock.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "stockLevelStatus", "stockLevel" })
@XmlRootElement(name = "stock")
public class Stock {

	/** The stock level status. */
	private StockLevelStatus stockLevelStatus;

	/** The stock level. */
	private String stockLevel;

	/**
	 * Gets the stock level.
	 *
	 * @return the stock level
	 */
	public final String getStockLevel() {
		return stockLevel;
	}

	/**
	 * Sets the stock level.
	 *
	 * @param stockLevel
	 *            the new stock level
	 */
	public final void setStockLevel(String stockLevel) {
		this.stockLevel = stockLevel;
	}

	/**
	 * Gets the stock level status.
	 *
	 * @return the stock level status
	 */
	public final StockLevelStatus getStockLevelStatus() {
		return stockLevelStatus;
	}

	/**
	 * Sets the stock level status.
	 *
	 * @param stockLevelStatus
	 *            the new stock level status
	 */
	public final void setStockLevelStatus(StockLevelStatus stockLevelStatus) {
		this.stockLevelStatus = stockLevelStatus;
	}

}

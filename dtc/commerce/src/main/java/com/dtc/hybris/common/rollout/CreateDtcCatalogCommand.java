package com.dtc.hybris.common.rollout;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.HtmlResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.granite.xss.XSSAPI;
import com.day.cq.commons.servlets.HtmlStatusResponseHelper;
import com.day.cq.i18n.I18n;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.commands.WCMCommand;
import com.day.cq.wcm.api.commands.WCMCommandContext;
import com.day.text.Text;
import com.dtc.hybris.api.DtcCatalogGenerator;

/**
 * Create Dtc catalog command.
 */
@Component
@Service
@Properties({ @Property(
        name = "service.description", value = "WCM command which creates product and catgeory pages") })
@SuppressWarnings("PMD.AvoidCatchingGenericException")
public class CreateDtcCatalogCommand implements WCMCommand
{
    private static final Logger LOG = LoggerFactory.getLogger(CreateDtcCatalogCommand.class);
    
    @Reference
    private transient XSSAPI xssApi;
    
    @Reference
    private transient DtcCatalogGenerator catalogGenerator;

    /**
     * Gets the command name.
     * 
     * @return The command name.
     */
    public final String getCommandName()
    {
        return "createDtcCatalog";
    }

    /**
     * Performs the command.
     *
     * @param ctx The WCM command context.
     * @param request The sling HTTP servlet request.
     * @param response The sling HTTP servlet response.
     * @param pageManager The page manager.
     * @return The HTML response.
     */
    @SuppressWarnings("deprecation")
    public final HtmlResponse performCommand(final WCMCommandContext ctx, final SlingHttpServletRequest request,
            final SlingHttpServletResponse response, final PageManager pageManager)
    {
        try
        {
            final ResourceResolver resolver = request.getResourceResolver();

            final String destPath = xssApi.filterHTML(request.getParameter("destPath"));
            if ((StringUtils.isEmpty(destPath)) || (resolver.getResource(destPath) == null))
            {
                return HtmlStatusResponseHelper.createStatusResponse(false,
                        I18n.get(request, "Destination {0} not found.", "", new Object[] {destPath }));
            }

            Page master = null;
            String srcPath = xssApi.filterHTML(request.getParameter("srcPath"));

            if (StringUtils.isNotEmpty(srcPath))
            {
                if (srcPath.endsWith("/jcr:content"))
                {
                    srcPath = srcPath.substring(0, srcPath.length() - "/jcr:content".length());
                }
                master = pageManager.getPage(srcPath);
            }

            if (master == null)
            {
                return HtmlStatusResponseHelper.createStatusResponse(false,
                        I18n.get(request, "Blueprint {0} not found.", "", new Object[] {srcPath }));
            }

            if (Text.isDescendantOrEqual(srcPath, destPath))
            {
                return HtmlStatusResponseHelper.createStatusResponse(false,
                        I18n.get(request, "Destination path cannot contain source path."));
            }

            this.catalogGenerator.createCatalog(master, destPath);

            return HtmlStatusResponseHelper.createStatusResponse(true, I18n.get(request, "Catalog created"),
                    destPath);
        } catch (final Exception e)
        {
            final String msg = I18n.get(request, "Error during Catalog creation.");
            LOG.error("Error creating catalog :: ", e);
            
            return HtmlStatusResponseHelper.createStatusResponse(false, msg);
        }
    }

}

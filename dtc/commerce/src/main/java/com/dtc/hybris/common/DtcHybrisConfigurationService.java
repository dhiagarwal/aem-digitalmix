package com.dtc.hybris.common;

import java.util.Map;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.commons.osgi.PropertiesUtil;

import com.dtc.hybris.api.HybrisConfigurationService;

/**
 * Nu Skin Hybris configuration service.
 */
@Component(
        metatype = true, label = "Nu Skin Commerce Hybris Configuration")
@Service(HybrisConfigurationService.class)
public class DtcHybrisConfigurationService implements HybrisConfigurationService
{
    /**
     * Default catalog version.
     */
    public static final String DEFAULT_CATALOG_VERSION = "Online";

    /**
     * Catalog version.
     */
    @Property({ DEFAULT_CATALOG_VERSION })
    public static final String CATALOG_VERSION = "hybris.catalog.version";

    private transient String catalogVersion;

    /**
     * Gets the catalog version.
     * 
     * @return The catalog version.
     */
    public final String getCatalogVersion()
    {
        return this.catalogVersion;
    }

    /**
     * @param properties The properties.
     */
    @Activate
    protected final void activate(final Map<Object, Object> properties)
    {
        this.catalogVersion = PropertiesUtil
                .toString(properties.get("hybris.catalog.version"), DEFAULT_CATALOG_VERSION);
    }

}

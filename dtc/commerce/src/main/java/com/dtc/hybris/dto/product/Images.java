package com.dtc.hybris.dto.product;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Product categories class. Used for unmarshalling the Hybris REST API response.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
        name = "", propOrder = { "image" })
@XmlRootElement(
        name = "images")
@SuppressWarnings("PMD.BeanMembersShouldSerialize")
public class Images
{
    private List<Image> image;

    /**
     * Gets the category list.
     * 
     * @return The category list.
     */
    public final List<Image> getImage()
    {
        if (image == null)
        {
        	image = new ArrayList<Image>();
        }
        return this.image;
    }

}

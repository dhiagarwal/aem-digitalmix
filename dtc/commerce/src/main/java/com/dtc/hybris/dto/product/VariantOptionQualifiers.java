package com.dtc.hybris.dto.product;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Product categories class. Used for unmarshalling the Hybris REST API response.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
        name = "", propOrder = { "variantOptionQualifier" })
@XmlRootElement(
        name = "variantOptionQualifiers")
@SuppressWarnings("PMD.BeanMembersShouldSerialize")
public class VariantOptionQualifiers
{
    private List<VariantOptionQualifier> variantOptionQualifier;

    /**
     * Gets the category list.
     * 
     * @return The category list.
     */
    public final List<VariantOptionQualifier> getVariantOptionQualifier()
    {
        if (variantOptionQualifier == null)
        {
        	variantOptionQualifier = new ArrayList<VariantOptionQualifier>();
        }
        return this.variantOptionQualifier;
    }

}

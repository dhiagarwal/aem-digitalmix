package com.dtc.hybris;

import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;

import com.day.cq.commons.jcr.JcrUtil;

/**
 * Nuskin Hybris utility class.
 */
public final class DtcHybrisUtil
{
    private static final Pattern PRODUCT_CODE_PATTERN = Pattern.compile("(..?)");
    
    /**
     * Gets the product path.
     * 
     * @param basePath The product base path.
     * @param code The product code.
     * @return The product path.
     */
    public static String getProductPath(final String basePath, final String code)
    {
        return getProductPath(basePath, code, StringUtils.EMPTY);
    }

    /**
     * Gets the product path with suffix.
     * 
     * @param basePath The product base path.
     * @param code The product code.
     * @param pathSuffix The path suffix.
     * @return The product path.
     */
    public static String getProductPath(final String basePath, final String code, final String pathSuffix)
    {
        final String sanitizedCode = JcrUtil.createValidName(code, JcrUtil.HYPHEN_LABEL_CHAR_MAPPING);

        return new StringBuilder().append(basePath).append("/")
                .append(PRODUCT_CODE_PATTERN.matcher(sanitizedCode).replaceAll("$1/")).append(sanitizedCode)
                .append(pathSuffix).toString();
    }
    
    /**
     * Indicates if the resource is a product or not.
     * 
     * @param resource The resource.
     * @return If the resource is a product or not.
     */
    public static boolean isProduct(final Resource resource)
    {
        if (resource != null)
        {
            return resource.getValueMap().containsKey("cq:hybrisProductId");
        }

        return false;
    }

    /**
     * Find the catalog identifier.
     * 
     * @param resource The resource.
     * @return The catalog identifier.
     */
    @SuppressWarnings({ "PMD.ConfusingTernary" })
    public static String findCatalogId(final Resource resource)
    {
        if (resource != null)
        {
            ValueMap properties = null;

            if (resource.getChild("jcr:content") == null)
            {
                properties = resource.getValueMap();
            } else
            {
                properties = resource.getChild("jcr:content").getValueMap();
            }

            if ((properties != null) && (properties.containsKey("cq:hybrisCatalogId")))
            {
                return properties.get("cq:hybrisCatalogId", String.class);
            }

            return findCatalogId(resource.getParent());
        }

        return null;
    }

    /**
     * Default constructor.
     */
    private DtcHybrisUtil()
    {

    }

}

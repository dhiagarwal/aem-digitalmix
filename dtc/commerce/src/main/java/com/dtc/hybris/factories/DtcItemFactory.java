package com.dtc.hybris.factories;

import org.apache.sling.api.SlingHttpServletRequest;

import com.dtc.commerce.api.DtcItem;

/**
 * Nuskin Item factory.
 */
public interface DtcItemFactory
{
    /**
     * Creates a Dtc Item.
     * 
     * @param request The Sling HTTP request.
     * @param baseProductPath The base product path.
     * @param productCode The product code.
     * @return The Dtc Item.
     */
    DtcItem create(SlingHttpServletRequest request, String baseProductPath, String productCode);

}

package com.dtc.hybris.connection;

import java.io.InputStream;
import java.util.Map;


/**
 * Command results.
 */
public interface CommandResult
{
    /**
     * Indicates if the result was successful.
     * 
     * @return If the result was successful.
     */
    boolean success();

    /**
     * Gets the HTTP status.
     * 
     * @return The HTTP status.
     */
    int getStatus();
    
    /**
     * Gets the body.
     * 
     * @return The body.
     */
    InputStream getBody();
    
    /**
     * Gets the new session data.
     * 
     * @return The new session data.
     */
    Map<String, String> getNewSessionData();
    
    /**
     * Release the connection.
     */
    void release();
}

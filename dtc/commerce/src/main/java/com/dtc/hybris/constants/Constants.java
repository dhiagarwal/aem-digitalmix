package com.dtc.hybris.constants;

/**
 * Constants.
 */
public final class Constants
{
    /**
     * The commerce base path.
     */
    public static final String COMMERCE_BASE_PATH = "/etc/commerce/products";

    /**
     * The commerce provider name.
     */
    public static final String COMMERCE_PROVIDER = "nuskin-hybris";
    
    /**
     * The v1 webservice flag.
     */
    public static final String V1_FLAG = "V1";
    
    /**
     * User name.
     */
    public static final String USERNAME = "username";
    
    /**
     * Anonymous user.
     */
    public static final String ANONYMOUS = "anonymous";
    
    /**
     * The product id property.
     */
    public static final String CQ_HYBRIS_PRODUCTS_ID = "cq:hybrisProductId";
    
    /**
     * Default Constructor.
     * 
     * Private constructor to restrict instantiation.
     */
    private Constants()
    {
        
    }

}

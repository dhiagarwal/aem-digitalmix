package com.dtc.hybris.dto.product;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Product class. Used for unmarshalling the Hybris REST API response.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
        name = "", propOrder = { "qualifier" , "name" , "value"})
@XmlRootElement(
        name = "variantOptionQualifier")
public class VariantOptionQualifier
{
    
    private String qualifier;
    
    private String name;
    
    private String value;
    
    
    public final String getQualifier()
    {
        return qualifier;
    }

    /**
     * Sets the Qualifier
     * 
     * @param value The Qualifier.
     * 
     */
    public final void setQualifier(final String value)
    {
        this.qualifier = value;
    }
    
    public final String getName()
    {
        return name;
    }

    /**
     * Sets the Name of the variant
     * 
     * @param value The Name.
     * 
     */
    public final void setName(final String value)
    {
        this.name = value;
    }
    
	/**
	 * @return the value
	 */
	public final String getValue() {
		return value;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public final void setValue(final String value) {
		this.value = value;
	}
    
}

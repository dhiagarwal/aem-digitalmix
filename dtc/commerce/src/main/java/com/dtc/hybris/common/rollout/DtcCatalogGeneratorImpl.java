package com.dtc.hybris.common.rollout;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;

import com.adobe.cq.commerce.api.CommerceException;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.WCMException;
import com.day.cq.wcm.msm.api.LiveRelationshipManager;
import com.day.cq.wcm.msm.api.RolloutConfig;
import com.dtc.hybris.CatalogUtils;
import com.dtc.hybris.api.DtcCatalogGenerator;

/**
 * Nu Skin catalog generator implementation.
 */
@Component
@Service
@Properties({ @Property(
        name = "service.description", value = "Creates product catalog pages based on a catalog blueprint") })
public class DtcCatalogGeneratorImpl implements DtcCatalogGenerator
{
    private static final String[] DEFAULT_ROLLOUT_CONFIGS = {"/etc/msm/rolloutconfigs/default" };
    
    @Reference
    private transient LiveRelationshipManager liveRelationshipManager;

    @Override
    public final void createCatalog(final Page blueprint, final String destinationPath) throws RepositoryException,
            CommerceException, WCMException
    {
        final ResourceResolver resolver = blueprint.adaptTo(Resource.class).getResourceResolver();

        String[] rolloutConfigurations = blueprint.getContentResource().adaptTo(ValueMap.class)
                .get("cq:BlueprintSyncConfig/cq_rolloutConfigs", String[].class);

        if (null == rolloutConfigurations || (rolloutConfigurations.length == 0)
                || (rolloutConfigurations[0].isEmpty()))
        {
            rolloutConfigurations = DEFAULT_ROLLOUT_CONFIGS;
        }

        createCategoryPages(resolver, resolver.adaptTo(PageManager.class), blueprint, rolloutConfigurations,
                destinationPath);

        resolver.adaptTo(PageManager.class).getPage(destinationPath).adaptTo(Node.class).getSession().save();
    }

    /**
     * Creates the Category Pages.
     *
     * @param resolver The resource resolver.
     * @param pageManager The page manager.
     * @param blueprint The page blueprint.
     * @param rolloutConfigurations The rollout configurations.
     * @param destinationPath The language page.
     * @throws RepositoryException Error while modifying JCR repository.
     * @throws CommerceException Error while creating catalog.
     * @throws WCMException Error while creating pages.
     */
    private void createCategoryPages(final ResourceResolver resolver, final PageManager pageManager,
            final Page blueprint, final String[] rolloutConfigurations, final String destinationPath)
            throws RepositoryException, CommerceException, WCMException
    {
        final Page templatePage = CatalogUtils.getTemplatePage(blueprint, "category");
        final Node destinationPathNode = resolver.getResource(destinationPath).adaptTo(Node.class);

        final RolloutConfig[] rolloutConfigs = CatalogUtils.getConfigs(resolver, rolloutConfigurations);

        for (final Resource category : CatalogUtils.getCategories(blueprint))
        {
            final ValueMap categoryProperties = category.getValueMap();
            final String parentCategoryPath = StringUtils.substringBeforeLast(destinationPathNode.getPath()
                    + categoryProperties.get("categoryNamePath", String.class), "/");

            Node categoryParentNode = null;
            
            if (parentCategoryPath.equals(destinationPathNode.getPath()))
            {
                categoryParentNode = resolver.getResource(destinationPathNode.getPath()).adaptTo(Node.class);

            } else
            {
                categoryParentNode = resolver.getResource(parentCategoryPath).adaptTo(Node.class);
            }

            final String categoryName = CatalogUtils.mangleName(categoryProperties.get("name", String.class));
            if (null == pageManager.getPage(parentCategoryPath + "/" + categoryName))
            {
                final Page categoryPage = CatalogUtils.createPage(pageManager, categoryParentNode, categoryName);
                final Node categoryContentNode = CatalogUtils.createPageContent(categoryPage, templatePage);

                liveRelationshipManager.establishRelationship(templatePage, categoryPage, false, false, rolloutConfigs);
                categoryContentNode.addMixin("cq:CatalogSyncAction");
                categoryContentNode.setProperty("jcr:title", categoryProperties.get("name", String.class));
                categoryContentNode.setProperty("hybrisCategoryName", categoryProperties.get("name", String.class));
                categoryContentNode.setProperty("hybrisCategoryPath",
                        categoryProperties.get("categoryPath", String.class));

                categoryContentNode.getSession().save();
            }
            
            categoryParentNode.getSession().save();

        }

    }
}

package com.dtc.hybris.adapters;

import static com.dtc.hybris.constants.Constants.COMMERCE_BASE_PATH;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.adapter.AdapterFactory;
import org.apache.sling.api.resource.Resource;

import com.day.cq.commons.inherit.ComponentInheritanceValueMap;
import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.wcm.api.LanguageManager;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.dtc.commerce.api.DtcCommerceContext;
import com.dtc.commerce.common.DtcCommerceContextImpl;
import com.dtc.commerce.common.CommerceUserImpl;
import com.dtc.hybris.constants.Constants;
/**
 * Hybris commerce adapter factory.
 */
@Component(
        metatype = false)
@Service(
        value = AdapterFactory.class)
public class HybrisCommerceAdapterFactory implements AdapterFactory
{
    private static final String HYBRIS_CATEGORY_PATH = "hybrisCategoryPath";
    
    @Reference
    private transient LanguageManager languageManager;

    private static final Class<DtcCommerceContext> COMMERCE_CONTEXT = DtcCommerceContext.class;

    /**
     * The classes that can be adapted.
     */
    @Property(
            name = "adapters")
    public static final String[] ADAPTER_CLASSES = {COMMERCE_CONTEXT.getName()};

    /**
     * The classes that are adaptable.
     */
    @Property(
            name = "adaptables")
    public static final String[] ADAPTABLE_CLASSES = {SlingHttpServletRequest.class.getName()};

    @SuppressWarnings("unchecked")
    @Override
    public final <AdapterType> AdapterType getAdapter(final Object adaptable, final Class<AdapterType> type)
    {
        final SlingHttpServletRequest request = (SlingHttpServletRequest) adaptable;
        final Resource requestResource = request.getResource();

        final Page page = requestResource.getResourceResolver().adaptTo(PageManager.class)
                .getContainingPage(requestResource);
        InheritanceValueMap properties;

        if (page == null)
        {
            properties = new ComponentInheritanceValueMap(requestResource);
        } else
        {
            properties = new HierarchyNodeInheritanceValueMap(page.getContentResource());
        }

        final CommerceUserImpl commerceUser = new CommerceUserImpl();
        if (request.getHeader("username") == null)
        {
            commerceUser.setUsername(Constants.ANONYMOUS);
        } else
        {
            commerceUser.setUsername(request.getHeader("username").toLowerCase());
            commerceUser.setFirstName(request.getHeader("firstname"));
            commerceUser.setLastName(request.getHeader("lastname"));
        }

        final DtcCommerceContextImpl commerceContext = new DtcCommerceContextImpl();

        commerceContext.setBaseStore(properties.getInherited("cq:hybrisBaseStore", String.class));
        commerceContext.setCatalogId(properties.getInherited("cq:hybrisCatalogId", String.class));
        commerceContext.setCartIdentifierName(properties.getInherited("cartIdentifierName", String.class));

        commerceContext.setLanguage(languageManager.getCqLanguage(requestResource).getLanguageCode());
        commerceContext.setBaseProductContentPath(StringUtils.EMPTY);
        commerceContext.setBaseProductDataPath(new StringBuilder().append(COMMERCE_BASE_PATH).append("/")
                .append(commerceContext.getBaseStore()).append("/").append(commerceContext.getCatalogId())
                .append("/").append(commerceContext.getLanguage()).append("/").append("products").toString());
        commerceContext.setCategoryId(properties.get(HYBRIS_CATEGORY_PATH, String.class));
        commerceContext.setUser(commerceUser);
        commerceContext.setRequest(request);

        return (AdapterType) validateCommerceContext(commerceContext);

    }

    /**
     * Validates the commerce context.
     * 
     * @param commerceContext The commerce context.
     * @return The validated commerce context.
     */
    private DtcCommerceContext validateCommerceContext(final DtcCommerceContext commerceContext)
    {
        if (StringUtils.isNotBlank(commerceContext.getBaseStore())
                && StringUtils.isNotBlank(commerceContext.getCatalogId())
                && StringUtils.isNotBlank(commerceContext.getLanguage()))
        {
            return commerceContext;
        }

        return null;
    }

}

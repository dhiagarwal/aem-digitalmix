package com.dtc.hybris.dto.product;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Media item class. Used for unmarshalling the Hybris REST API response.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
        name = "", propOrder = { "description", "realFileName", "mimeType", "key" })
@XmlRootElement(
        name = "mediaItem")
@SuppressWarnings("PMD.BeanMembersShouldSerialize")
public class MediaItem
{
    private String description;
    private String realFileName;
    private String key;
    private String mimeType;

    /**
     * Gets the attribute key.
     * 
     * @return The attribute key.
     */
    public final String getKey()
    {
        return key;
    }

    /**
     * Sets the attribute key.
     * 
     * @param key The attribute key.
     */
    public final void setKey(final String key)
    {
        this.key = key;
    }

    /**
     * Gets the media item mime type.
     * 
     * @return The media item mime type.
     */
    public final String getMimeType()
    {
        return mimeType;
    }

    /**
     * Sets the media item mime type.
     * 
     * @param mimeType The media item mime type.
     */
    public final void setMimeType(final String mimeType)
    {
        this.mimeType = mimeType;
    }

    /**
     * Gets the media item description.
     * 
     * @return The media item description.
     */
    public final String getDescription()
    {
        return description;
    }

    /**
     * Sets the media item description.
     * 
     * @param description The media item description.
     */
    public final void setDescription(final String description)
    {
        this.description = description;
    }

    /**
     * Gets the media item real filename.
     * 
     * @return The media item real filename.
     */
    public final String getRealFilename()
    {
        return realFileName;
    }

    /**
     * Sets the media item real filename.
     * 
     * @param realFileName The media item real filename.
     */
    public final void setRealFilename(final String realFileName)
    {
        this.realFileName = realFileName;
    }

}

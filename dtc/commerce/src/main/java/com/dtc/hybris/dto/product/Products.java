package com.dtc.hybris.dto.product;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Products class. Used for unmarshalling the Hybris REST API response.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
        name = "", propOrder = { "product" })
@XmlRootElement(
        name = "products")
@SuppressWarnings("PMD.BeanMembersShouldSerialize")
public class Products
{
    private List<Product> product;

    /**
     * Gets the product.
     * 
     * @return The product.
     */
    public final List<Product> getProduct()
    {
        if (product == null)
        {
            product = new ArrayList<Product>();
        }
        return this.product;
    }

}

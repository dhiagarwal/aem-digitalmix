package com.dtc.hybris.dto.catalog;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * Catalog category. Used for unmarshalling the Hybris REST API response.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
        name = "category", propOrder = { "id", "lastModified", "name", "url", "subcategories" })
@SuppressWarnings({ "PMD.ShortVariable" })
public class CatalogCategory
{
    private String id;
    @XmlSchemaType(
            name = "dateTime")
    private XMLGregorianCalendar lastModified;
    private String name;
    private String url;
    private CatalogSubCategories subcategories;

    /**
     * Gets the identifier.
     * 
     * @return The identifier.
     */
    public final String getId()
    {
        return id;
    }

    /**
     * Sets the identifier.
     * 
     * @param id The identifier.
     */
    public final void setId(final String id)
    {
        this.id = id;
    }

    /**
     * Gets the last modified date and time.
     * 
     * @return The last modified date and time.
     */
    public final XMLGregorianCalendar getLastModified()
    {
        return lastModified;
    }

    /**
     * Sets the last modified date and time.
     * 
     * @param lastModified The last modified date and time.
     */
    public final void setLastModified(final XMLGregorianCalendar lastModified)
    {
        this.lastModified = lastModified;
    }

    /**
     * Gets the name.
     * 
     * @return The name.
     */
    public final String getName()
    {
        return name;
    }

    /**
     * Sets the name.
     * 
     * @param name The name.
     */
    public final void setName(final String name)
    {
        this.name = name;
    }

    /**
     * Gets the URL.
     * 
     * @return The URL.
     */
    public final String getUrl()
    {
        return url;
    }

    /**
     * Sets the URL.
     * 
     * @param url The URL.
     */
    public final void setUrl(final String url)
    {
        this.url = url;
    }

    /**
     * Gets the catalog sub-categories.
     * 
     * @return The catalog sub-categories.
     */
    public final CatalogSubCategories getSubcategories()
    {
        return subcategories;
    }

    /**
     * Sets the catalog sub-categories.
     * 
     * @param subcategories The catalog sub-categories.
     */
    public final void setSubcategories(final CatalogSubCategories subcategories)
    {
        this.subcategories = subcategories;
    }

}

package com.dtc.hybris.dto.product;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Product class. Used for unmarshalling the Hybris REST API response.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "variantType", "selected" })
@XmlRootElement(name = "baseOption")
public class BaseOption {

	/** The variant type. */
	private String variantType;

	/** The selected. */
	private Selected selected;

	/**
	 * Gets the variant type.
	 *
	 * @return the variant type
	 */
	public final String getVariantType() {
		return variantType;
	}

	/**
	 * Sets the Variant Type.
	 *
	 * @param value
	 *            The Variant Type.
	 */
	public final void setVariantType(final String value) {
		this.variantType = value;
	}

	/**
	 * Gets the selected.
	 *
	 * @return the selected
	 */
	public final Selected getSelected() {
		return selected;
	}

	/**
	 * Sets the selected.
	 *
	 * @param selected
	 *            the new selected
	 */
	public final void setSelected(Selected selected) {
		this.selected = selected;
	}

}

package com.dtc.hybris.importer;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import com.dtc.hybris.dto.product.Product;

/**
 * Dtc import handler.
 */
public interface DtcImportHandler
{
    /**
     * Create a catalog.
     * 
     * @param importerContext The importer context.
     * @return The catalog resource path.
     * @throws RepositoryException Error while modifying JCR repository.
     */
    //String createCatalog(ImporterContext importerContext) throws RepositoryException;
    
    /**
     * Import a product.
     * 
     * @param importerContext The importer context.
     * @param product The product.
     * @throws RepositoryException Error while modifying JCR repository.
     */
    void importProduct(ImporterContext importerContext, Product product)
            throws RepositoryException;

    /**
     * Create a product.
     * 
     * @param importerContext The importer context.
     * @param product The product.
     * @throws RepositoryException Error while modifying JCR repository.
     */
    void createProduct(ImporterContext importerContext, Product product)
            throws RepositoryException;
    
    /**
     * Create a variant.
     * 
     * @param importerContext The importer context.
     * @param product The product.
     * @throws RepositoryException Error while modifying JCR repository.
     */
    void createVariant(ImporterContext importerContext, Product product)
            throws RepositoryException;

    /**
     * Updates the product.
     * 
     * @param node The product node.
     * @param importerContext The importer context.
     * @param product The product.
     * @throws RepositoryException Error while modifying JCR repository.
     */
    void updateProduct(Node node, ImporterContext importerContext, Product product)
            throws RepositoryException;

    /**
     * Creates category.
     * 
     * @param categoryBasePath The base category path.
     * @param category The category.
     * @param importerContext The importer context.
     * @return The category path.
     * @throws RepositoryException Error while modifying JCR repository.
     * @throws InvalidTagFormatException Invalid tag error.
     * @throws TagException Tag error.
     */
    /*String createCategory(String categoryBasePath, CatalogCategory category,
            ImporterContext importerContext) throws RepositoryException, InvalidTagFormatException, TagException;*/

    /**
     * Creates attributes.
     * 
     * @param productNode The product node.
     * @param product The product.
     * @throws RepositoryException Error while modifying JCR repository.
     */
    //void createAttributes(Node productNode, Product product) throws RepositoryException;

    /**
     * Deletes categories.
     * 
     * @param importerContext The importer context.
     * @param categoryBasePath The category base path.
     * @throws RepositoryException Error while modifying JCR repository.
     */
    //void deleteCategories(ImporterContext importerContext, String categoryBasePath) throws RepositoryException;
    
    
}

package com.dtc.hybris.connection;

import java.util.Map;

import org.apache.commons.httpclient.HttpClient;

import com.dtc.commerce.api.CommerceException;

/**
 * Hybris connection interface.
 */
public interface HybrisConnection
{
    /**
     * Gets the configured HTTP client.
     * 
     * @return The configured HTTP client.
     */
    HttpClient getConfiguredHttpClient();

    /**
     * Execute command.
     * 
     * @param command The command.
     * @param baseStore The base store.
     * @return The command result.
     * @throws CommerceException Error executing the command.
     */
    CommandResult execute(final HybrisCommand command, final String baseStore) throws CommerceException;

    /**
     * Execute command.
     * 
     * @param command The command.
     * @param options The connection options.
     * @return The command result.
     * @throws CommerceException Error executing the command.
     */
    CommandResult execute(final HybrisCommand command, final ConnectionOptions options) throws CommerceException;

    /**
     * Gets the server URL.
     * 
     * @return The server URL.
     */
    String getServerUrl();

    /**
     * Connection options.
     */
    @SuppressWarnings({ "PMD.ImmutableField" })
    class ConnectionOptions
    {
        private final String baseStore;
        private final Map<String, Object> context;
        private Map<String, String> sessionData;

        /**
         * Connection options.
         * 
         * @param baseStore The base store.
         * @param context The context.
         */
        public ConnectionOptions(final String baseStore, final Map<String, Object> context)
        {
            this.baseStore = baseStore;
            this.context = context;
            this.sessionData = null;
        }

        /**
         * Connection options.
         * 
         * @param baseStore The base store.
         * @param sessionData The session data.
         * @param context The context.
         */
        public ConnectionOptions(final String baseStore, final Map<String, String> sessionData,
                final Map<String, Object> context)
        {
            this(
                    baseStore, context);
            this.sessionData = sessionData;
        }

        /**
         * Gets the base store.
         * 
         * @return The base store.
         */
        public String getBaseStore()
        {
            return this.baseStore;
        }

        /**
         * Gets the context.
         * 
         * @return The context.
         */
        public Map<String, Object> getContext()
        {
            return this.context;
        }

        /**
         * Gets the session data.
         * 
         * @return The session data.
         */
        public Map<String, String> getSessionData()
        {
            return this.sessionData;
        }
    }
}

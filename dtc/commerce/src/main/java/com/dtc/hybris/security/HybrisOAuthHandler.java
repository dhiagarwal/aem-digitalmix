package com.dtc.hybris.security;

import java.io.IOException;
import java.util.Map;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;

import com.dtc.hybris.connection.HybrisConnection;

/**
 * Hybris authentication handler interface.
 */
public interface HybrisOAuthHandler
{
    /**
     * Execute authentication request.
     * 
     * @param connection The connection.
     * @param client The HTTP client.
     * @param method The HTTP method.
     * @param username The username.
     * @param password The password.
     * @return The authenticated response.
     * @throws IOException Error executing the request.
     */
    Map<String, String> executeAuthenticated(HybrisConnection connection, final HttpClient client,
            final HttpMethod method, final String username, final String password) throws IOException;

    /**
     * Execute authentication request.
     * 
     * @param client The HTTP client.
     * @param method The HTTP method.
     * @param sessionData The session data.
     * @return The authenticated response.
     * @throws IOException Error executing the request.
     */
    Map<String, String> executeAuthenticated(final HttpClient client, final HttpMethod method,
            final Map<String, String> sessionData) throws IOException;
}

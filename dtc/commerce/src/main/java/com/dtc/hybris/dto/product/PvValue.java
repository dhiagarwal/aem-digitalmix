package com.dtc.hybris.dto.product;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * The Class PvValue.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "currencyIso", "value", "formattedValue" })
@XmlRootElement(name = "pvValue")
public class PvValue {
	// The currency
	private String currencyIso;

	// The pv value
	private String value;

	// The formatted pv value
	private String formattedValue;

	public final String getCurrencyIso() {
		return currencyIso;
	}

	public final void setCurrencyIso(String currencyIso) {
		this.currencyIso = currencyIso;
	}

	public final String getValue() {
		return value;
	}

	public final void setValue(String value) {
		this.value = value;
	}

	public final String getFormattedValue() {
		return formattedValue;
	}

	public final void setFormattedValue(String formattedValue) {
		this.formattedValue = formattedValue;
	}
}

package com.dtc.hybris.connection.cmd;

import static com.dtc.hybris.constants.Constants.V1_FLAG;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.dtc.hybris.connection.AbstractHybrisCommand;

/**
 * Get Dtc products command.
 */
public class GetDtcProductsCommand extends AbstractHybrisCommand
{
    /**
     * Constructor.
     * 
     * @param catalog The catalog.
     * @param catalogVersion The catalog version.
     * @param language The language.
     * @param page The pagination page.
     * @param pageSize The page size.
     * @param since The time stamp for last imported.
     */
    @SuppressWarnings("PMD.SimpleDateFormatNeedsLocale")
    public GetDtcProductsCommand(final String catalog, final String catalogVersion,
            final Integer page, final Integer pageSize, final Date since)
    {
        if (since == null)
        {
            setPath(V1_FLAG + "/products/export/full");
        } else
        {
            setPath(V1_FLAG + "/products/export/incremental");

            final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
            addParameter("timestamp", dateFormatter.format(since));
        }

        addParameter("catalog", catalog);
        addParameter("version", catalogVersion);
        //addParameter("lang", language);
        addParameter("currentPage", page.toString());
        addParameter("pageSize", pageSize.toString());
    }
   
}

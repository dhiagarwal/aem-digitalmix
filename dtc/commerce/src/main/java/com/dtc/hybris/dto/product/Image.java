package com.dtc.hybris.dto.product;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Item class. Used for unmarshalling the Hybris REST API response.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
        name = "", propOrder = { "altText", "format", "imageType", "url" })
@XmlRootElement(name = "image")
public class Image
{

    private String altText;
    
    private String format;
    
    private String imageType;
    
    private String url;
    
    /**
     * Gets the item identifier.
     * 
     * @return The item identifier.
     */
    public final String getAltText()
    {
        return altText;
    }

    /**
     * Sets image altText.
     * 
     * @param altText The altText.
     */
    public final void setAltText(final String altText)
    {
        this.altText = altText;
    }
    
    /**
     * Gets the item identifier.
     * 
     * @return The item identifier.
     */
    public final String getFormat()
    {
        return format;
    }

    /**
     * Sets image format.
     * 
     * @param format The image format.
     */
    public final void setFormat(final String format)
    {
        this.format = format;
    }
    
    /**
     * Gets the item identifier.
     * 
     * @return The item identifier.
     */
    public final String getImageType()
    {
        return imageType;
    }

    /**
     * Sets item identifier.
     * 
     * @param imageType The item type.
     */
    public final void setImageType(final String imageType)
    {
        this.imageType = imageType;
    }
    
    /**
     * Gets the item identifier.
     * 
     * @return The item identifier.
     */
    public final String getUrl()
    {
        return url;
    }

    /**
     * Sets image url.
     * 
     * @param url The image url.
     */
    public final void setUrl(final String url)
    {
        this.url = url;
    }

}

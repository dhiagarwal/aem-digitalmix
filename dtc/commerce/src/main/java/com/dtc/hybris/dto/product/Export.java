package com.dtc.hybris.dto.product;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Export class. Used for unmarshalling the Hybris REST API response.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
        name = "",
        propOrder = { "totalPageCount", "currentPage", "products", "totalProductCount"})
@XmlRootElement(
        name = "export")
@SuppressWarnings("PMD.BeanMembersShouldSerialize")
public class Export
{
    private byte totalPageCount;

    private byte totalProductCount;

    private byte currentPage;

    @XmlElement(
            required = true)
    private Products products;

    /**
     * Gets the total page count.
     * 
     * @return The total page count.
     */
    public final byte getTotalPageCount()
    {
        return totalPageCount;
    }

    /**
     * Sets the total page count.
     * 
     * @param value The total page count.
     */
    public final void setTotalPageCount(final byte value)
    {
        this.totalPageCount = value;
    }


    /**
     * Gets the total product count.
     * 
     * @return The total product count.
     */
    public final byte getTotalProductCount()
    {
        return totalProductCount;
    }

    /**
     * Sets the total product count.
     * 
     * @param value The total product count.
     * 
     */
    public final void setTotalProductCount(final byte value)
    {
        this.totalProductCount = value;
    }

    /**
     * Gets the value of the currentPage property.
     * 
     * @return The current page.
     */
    public final byte getCurrentPage()
    {
        return currentPage;
    }

    /**
     * Sets the the current page.
     * 
     * @param value The current page.
     */
    public final void setCurrentPage(final byte value)
    {
        this.currentPage = value;
    }


    /**
     * Gets the products.
     * 
     * @return The products.
     */
    public final Products getProducts()
    {
        return products;
    }

    /**
     * Sets the products.
     * 
     * @param value The products.
     */
    public final void setNuskinProductList(final Products value)
    {
        this.products = value;
    }
}

package com.dtc.hybris.dto.product;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "code", "codeLowerCase" })
@XmlRootElement(name = "stockLevelStatus")
public class StockLevelStatus {

	/** The code. */
	private String code;

	/** The code lower case. */
	private String codeLowerCase;

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public final String getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code
	 *            the new code
	 */
	public final void setCode(String code) {
		this.code = code;
	}

	/**
	 * Gets the code lower case.
	 *
	 * @return the code lower case
	 */
	public final String getCodeLowerCase() {
		return codeLowerCase;
	}

	/**
	 * Sets the code lower case.
	 *
	 * @param codeLowerCase
	 *            the new code lower case
	 */
	public final void setCodeLowerCase(String codeLowerCase) {
		this.codeLowerCase = codeLowerCase;
	}

}

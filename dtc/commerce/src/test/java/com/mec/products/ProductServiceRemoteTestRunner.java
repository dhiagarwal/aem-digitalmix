package com.mec.products;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.URI;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.StringWriter;

public class ProductServiceRemoteTestRunner {
    private static String[] skus = new String[]{"01101255"};
    //private static String[] skus = new String[]{"00000090"};
    //private static String[] skus = new String[]{"01003600"};
    //private static String[] skus = new String[]{"99999999"};
    //private static String[] skus = new String[]{"01001255", "01001249", "01001250", "01001251", "01001252", "01003680", "01003600", "01003601", "01003602", "01003603","02001255", "02001249", "02001250", "02001251", "02001252", "02003698", "02003652", "02003651", "02003654", "02003648"};

    private static final String XML_PREFIX =
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                    "<tns:productMessage operation=\"update\" xmlns:tns=\"http://day.nse.com/products\">\n" +
                    "  <tns:product labOffice=\"new\" description=\"Test description 7.\" sku=\"";
    private static final String XML_SUFFIX = "\">\n" +
            "    <tns:market id=\"us\" statusCode=\"10\" statusMessage=\"the message\" webEnabled=\"true\">\n" +
            "      <orderType>order</orderType>\n" +
            "      <orderType>adr</orderType>\n" +
            "      <custType>10</custType>\n" +
            "      <custType>20</custType>\n" +
            "      <custType>30</custType>\n" +
            "    </tns:market>\n" +
            "    <tns:market id=\"ca\" statusCode=\"10\" statusMessage=\"the message\" webEnabled=\"true\">\n" +
            "      <orderType>order</orderType>\n" +
            "      <custType>10</custType>\n" +
            "      <custType>20</custType>\n" +
            "    </tns:market>\n" +
            "    <tns:componentProduct sku=\"01101249\" description=\"Test description.\"/>\n" +
            "    <tns:componentProduct sku=\"01101250\" description=\"Test description.\"/>\n" +
            "    <tns:componentProduct sku=\"01101251\" description=\"Test description.\"/>\n" +
            "    <tns:componentProduct sku=\"01101252\" description=\"Test description.\"/>\n" +
            "  </tns:product>\n" +
            "</tns:productMessage>";


    private static final String TEST_XML = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
            "<tns:productMessage operation=\"update\" xmlns:tns=\"http://day.nse.com/products\">" +
            "<tns:product description=\"Conductor Scalp Galvanic II Replacement\" labOffice=\"NS Treatment\" sku=\"00000098\">" +
            "  <tns:market id=\"XX\" statusCode=\"10\" scanQualified=\" 0\" statusMessage=\"\" webEnabled=\"false\">" +
            "    <orderType>adr</orderType><orderType>order</orderType>" +
            "  </tns:market>" +
            "  <tns:market id=\"US\" statusCode=\"10\" scanQualified=\"0\" statusMessage=\"Discontinued\" webEnabled=\"false\">" +
            "    <orderType>adr</orderType><orderType>order</orderType>" +
            "  </tns:market>" +
            "  <tns:market id=\"MX\" statusCode=\"02\" scanQualified=\"0\" statusMessage=\"Discontinued\" webEnabled=\"false\">" +
            "    <orderType>adr</orderType><orderType>order</orderType>" +
            "  </tns:market>" +
            "  <tns:market id=\"CA\" statusCode=\"02\" scanQualified=\"0\" statusMessage=\"Discontinued\" webEnabled=\"false\">" +
            "    <orderType>adr</orderType><orderType>order</orderType>" +
            "  </tns:market><tns:market id=\"NC\" statusCode=\"02\" scanQualified=\"0\" statusMessage=\"Discontinued\" webEnabled=\"false\">" +
            "    <orderType>adr</orderType><orderType>order</orderType>" +
            "  </tns:market>" +
            "  <tns:market id=\"NZ\" statusCode=\"02\" scanQualified=\"0\" statusMessage=\"Discontinued\" webEnabled=\"false\">" +
            "    <orderType>adr</orderType><orderType>order</orderType>" +
            "  </tns:market>" +
            "  <tns:market id=\"PF\" statusCode=\"02\" scanQualified=\"0\" statusMessage=\"Discontinued\" webEnabled=\"false\">" +
            "    <orderType>adr</orderType><orderType>order</orderType>" +
            "  </tns:market>" +
            "  <tns:market id=\"AU\" statusCode=\"02\" scanQualified=\"0\" statusMessage=\"Discontinued\" webEnabled=\"false\">" +
            "    <orderType>adr</orderType><orderType>order</orderType>" +
            "  </tns:market>" +
            "  <tns:market id=\"DK\" statusCode=\"02\" scanQualified=\"0\" statusMessage=\"Discontinued\" webEnabled=\"false\">" +
            "    <orderType>adr</orderType><orderType>order</orderType>" +
            "  </tns:market><tns:market id=\"FI\" statusCode=\"02\" scanQualified=\"0\" statusMessage=\"Discontinued\" webEnabled=\"false\">" +
            "    <orderType>adr</orderType><orderType>order</orderType>" +
            "  </tns:market>" +
            "  <tns:market id=\"GB\" statusCode=\"02\" scanQualified=\"0\" statusMessage=\"Discontinued\" webEnabled=\"false\">" +
            "    <orderType>adr</orderType><orderType>order</orderType>" +
            "  </tns:market>" +
            "  <tns:market id=\"IE\" statusCode=\"02\" scanQualified=\"0\" statusMessage=\"Discontinued\" webEnabled=\"false\">" +
            "    <orderType>adr</orderType><orderType>order</orderType>" +
            "  </tns:market>" +
            "  <tns:market id=\"SE\" statusCode=\"02\" scanQualified=\"0\" statusMessage=\"Discontinued\" webEnabled=\"false\">" +
            "    <orderType>adr</orderType><orderType>order</orderType>" +
            "  </tns:market>" +
            "  <tns:market id=\"NL\" statusCode=\"02\" scanQualified=\"0\" statusMessage=\"Discontinued\" webEnabled=\"false\">" +
            "    <orderType>adr</orderType><orderType>order</orderType>" +
            "  </tns:market>" +
            "  <tns:market id=\"AT\" statusCode=\"02\" scanQualified=\"0\" statusMessage=\"Discontinued\" webEnabled=\"false\">" +
            "    <orderType>adr</orderType><orderType>order</orderType>" +
            "  </tns:market>" +
            "  <tns:market id=\"DE\" statusCode=\"02\" scanQualified=\"0\" statusMessage=\"Discontinued\" webEnabled=\"false\">" +
            "    <orderType>adr</orderType><orderType>order</orderType>" +
            "  </tns:market>" +
            "  <tns:market id=\"FR\" statusCode=\"02\" scanQualified=\"0\" statusMessage=\"Discontinued\" webEnabled=\"false\">" +
            "    <orderType>adr</orderType><orderType>order</orderType>" +
            "  </tns:market>" +
            "  <tns:market id=\"BE\" statusCode=\"02\" scanQualified=\"0\" statusMessage=\"Discontinued\" webEnabled=\"false\">" +
            "    <orderType>adr</orderType><orderType>order</orderType>" +
            "  </tns:market>" +
            "  <tns:market id=\"CH\" statusCode=\"02\" scanQualified=\"0\" statusMessage=\"Discontinued\" webEnabled=\"false\">" +
            "    <orderType>adr</orderType><orderType>order</orderType>" +
            "  </tns:market>" +
            "  <tns:market id=\"LU\" statusCode=\"02\" scanQualified=\"0\" statusMessage=\"Discontinued\" webEnabled=\"false\">" +
            "    <orderType>adr</orderType><orderType>order</orderType>" +
            "  </tns:market>" +
            "  <tns:market id=\"PT\" statusCode=\"02\" scanQualified=\"0\" statusMessage=\"Discontinued\" webEnabled=\"false\">" +
            "    <orderType>adr</orderType><orderType>order</orderType>" +
            "  </tns:market>" +
            "  <tns:market id=\"ES\" statusCode=\"02\" scanQualified=\"0\" statusMessage=\"Discontinued\" webEnabled=\"false\">" +
            "    <orderType>adr</orderType><orderType>order</orderType>" +
            "  </tns:market>" +
            "  <tns:market id=\"IT\" statusCode=\"02\" scanQualified=\"0\" statusMessage=\"Discontinued\" webEnabled=\"false\">" +
            "    <orderType>adr</orderType><orderType>order</orderType>" +
            "  </tns:market>" +
            "  <tns:market id=\"NO\" statusCode=\"02\" scanQualified=\"0\" statusMessage=\"Discontinued\" webEnabled=\"false\">" +
            "    <orderType>adr</orderType><orderType>order</orderType>" +
            "  </tns:market>" +
            "  <tns:market id=\"RO\" statusCode=\"02\" scanQualified=\"0\" statusMessage=\"Discontinued\" webEnabled=\"false\">" +
            "    <orderType>adr</orderType><orderType>order</orderType>" +
            "  </tns:market>" +
            "  <tns:market id=\"HU\" statusCode=\"02\" scanQualified=\"0\" statusMessage=\"Discontinued\" webEnabled=\"false\">" +
            "    <orderType>adr</orderType><orderType>order</orderType>" +
            "  </tns:market>" +
            "  <tns:market id=\"SK\" statusCode=\"02\" scanQualified=\"0\" statusMessage=\"Discontinued\" webEnabled=\"false\">" +
            "    <orderType>adr</orderType><orderType>order</orderType>" +
            "  </tns:market>" +
            "  <tns:market id=\"PL\" statusCode=\"02\" scanQualified=\"0\" statusMessage=\"Discontinued\" webEnabled=\"false\">" +
            "    <orderType>adr</orderType><orderType>order</orderType>" +
            "  </tns:market>" +
            "  <tns:market id=\"TR\" statusCode=\"02\" scanQualified=\"0\" statusMessage=\"Discontinued\" webEnabled=\"false\">" +
            "    <orderType>adr</orderType><orderType>order</orderType>" +
            "  </tns:market>" +
            "  <tns:market id=\"IS\" statusCode=\"02\" scanQualified=\"0\" statusMessage=\"Discontinued\" webEnabled=\"false\">" +
            "    <orderType>adr</orderType><orderType>order</orderType>" +
            "  </tns:market>" +
            "  <tns:market id=\"CZ\" statusCode=\"02\" scanQualified=\"0\" statusMessage=\"Discontinued\" webEnabled=\"false\">" +
            "    <orderType>adr</orderType><orderType>order</orderType>" +
            "  </tns:market>" +
            "  <tns:market id=\"HK\" statusCode=\"10\" scanQualified=\"0\" statusMessage=\"Fully Releasd 4Sale\" webEnabled=\"true\">" +
            "    <orderType>adr</orderType><orderType>order</orderType><custType>10</custType>" +
            "  </tns:market>" +
            "  <tns:market id=\"MO\" statusCode=\"10\" scanQualified=\"0\" statusMessage=\"Fully Releasd 4Sale\" webEnabled=\"true\">" +
            "    <orderType>adr</orderType><orderType>order</orderType><custType>10</custType><custType>20</custType>" +
            "  </tns:market>" +
            "  <tns:market id=\"JA\" statusCode=\"\" scanQualified=\"0\" statusMessage=\"\" webEnabled=\"false\">" +
            "    <orderType>adr</orderType><orderType>order</orderType>" +
            "  </tns:market>" +
            "  <tns:market id=\"KR\" statusCode=\"\" scanQualified=\"0\" statusMessage=\"\" webEnabled=\"false\">" +
            "    <orderType>adr</orderType><orderType>order</orderType>" +
            "  </tns:market>" +
            "  <tns:market id=\"TH\" statusCode=\"02\" scanQualified=\"0\" statusMessage=\"Discontinued\" webEnabled=\"true\">" +
            "    <orderType>adr</orderType><orderType>order</orderType><custType>10</custType>" +
            "  </tns:market>" +
            "  <tns:market id=\"PH\" statusCode=\"10\" scanQualified=\"0\" statusMessage=\"Fully Releasd 4Sale\" webEnabled=\"true\">" +
            "    <orderType>adr</orderType><orderType>order</orderType><custType>10</custType>" +
            "  </tns:market>" +
            "  <tns:market id=\"SG\" statusCode=\"02\" scanQualified=\"0\" statusMessage=\"Discontinued\" webEnabled=\"false\">" +
            "    <orderType>adr</orderType><orderType>order</orderType>" +
            "  </tns:market>" +
            "  <tns:market id=\"MY\" statusCode=\"10\" scanQualified=\"0\" statusMessage=\"Fully Releasd 4Sale\" webEnabled=\"false\">" +
            "    <orderType>adr</orderType><orderType>order</orderType>" +
            "  </tns:market>" +
            "  <tns:market id=\"IL\" statusCode=\"10\" scanQualified=\"0\" statusMessage=\"Fully Releasd 4 Sale\" webEnabled=\"true\">" +
            "    <orderType>adr</orderType><orderType>order</orderType><custType>10</custType><custType>20</custType>" +
            "  </tns:market>" +
            "  <tns:market id=\"ZA\" statusCode=\"10\" scanQualified=\" 0\" statusMessage=\"Fully Releasd 4 Sale\" webEnabled=\"false\">" +
            "    <orderType>adr</orderType><orderType>order</orderType>" +
            "  </tns:market>" +
            "</tns:product></tns:productMessage>";


    // BE - 97138623
    public static final String xmlSample = "<?xml version=\"1.0\" encoding=\"utf-8\"?><tns:productMessage operation=\"update\" xmlns:tns=\"http://day.nse.com/products\"><tns:product description=\"TR 90 Programme month1 (IT)\" labOffice=\"PX ageLOC Kits\" sku=\"97138623\"><tns:market id=\"BE\" statusCode=\"10\" scanQualified=\" 0\" statusMessage=\"Fully Releasd 4 Sale\" webEnabled=\"true\" seashoppe=\"\"><orderType>adr</orderType><custType>10</custType><custType>20</custType></tns:market><tns:market id=\"CH\" statusCode=\"10\" scanQualified=\" 0\" statusMessage=\"Fully Releasd 4 Sale\" webEnabled=\"true\" seashoppe=\"\"><orderType>adr</orderType><custType>10</custType><custType>20</custType></tns:market><tns:market id=\"LU\" statusCode=\"10\" scanQualified=\" 0\" statusMessage=\"Fully Releasd 4 Sale\" webEnabled=\"true\" seashoppe=\"\"><orderType>adr</orderType><custType>10</custType><custType>20</custType></tns:market><tns:market id=\"PT\" statusCode=\"10\" scanQualified=\" 0\" statusMessage=\"Fully Releasd 4 Sale\" webEnabled=\"true\" seashoppe=\"\"><orderType>adr</orderType><custType>10</custType><custType>20</custType></tns:market><tns:market id=\"ES\" statusCode=\"10\" scanQualified=\" 0\" statusMessage=\"Fully Releasd 4 Sale\" webEnabled=\"true\" seashoppe=\"\"><orderType>adr</orderType><custType>10</custType><custType>20</custType></tns:market><tns:market id=\"IT\" statusCode=\"10\" scanQualified=\" 0\" statusMessage=\"Fully Releasd 4 Sale\" webEnabled=\"true\" seashoppe=\"\"><orderType>adr</orderType><custType>10</custType><custType>20</custType></tns:market></tns:product></tns:productMessage>";

    public static final String jaMultiXml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<ns1:productMessage xmlns:ns1=\"http://day.nse.com/products\" operation=\"update\"><ns1:product description=\"ﾗｲﾌﾊﾟｯｸ/ｶﾌﾟｾﾙ\" labOffice=\"PX Nutritionals\" sku=\"03003088\"><ns1:market id=\"JP\" salesOrganization=\"3015\" statusCode=\"10\" scanQualified=\"0 \" statusMessage=\"Fully Releasd 4 Sale\" webEnabled=\"true\" seashoppe=\"003\"><orderType>adr</orderType><orderType>order</orderType><orderType>ZPFC</orderType><custType>10</custType><custType>20</custType></ns1:market></ns1:product><ns1:product description=\"ﾗｲﾌﾊﾟｯｸ ﾅﾉ EX\" labOffice=\"PX Nutritionals\" sku=\"03003610\"><ns1:market id=\"JP\" salesOrganization=\"3015\" statusCode=\"10\" scanQualified=\"0 \" statusMessage=\"Fully Releasd 4 Sale\" webEnabled=\"true\" seashoppe=\"002\"><orderType>adr</orderType><orderType>order</orderType><orderType>ZPFC</orderType><custType>10</custType><custType>20</custType></ns1:market></ns1:product><ns1:product description=\"ｼﾞｪﾝﾛｯｸ ｱｰﾙｽｸｴｱ\" labOffice=\"PX ageLOC Product\" sku=\"03003901\"><ns1:market id=\"JP\" salesOrganization=\"3015\" statusCode=\"10\" scanQualified=\"0 \" statusMessage=\"Fully Releasd 4 Sale\" webEnabled=\"true\" seashoppe=\"002\"><orderType>adr</orderType><orderType>order</orderType><orderType>ZPFC</orderType><custType>10</custType><custType>20</custType></ns1:market></ns1:product><ns1:product description=\"ﾌｪｲｽ ｸﾚﾝｼﾞﾝｸﾞ ﾌｫｰﾑ 2008\" labOffice=\"NS Face\" sku=\"03003961\"><ns1:market id=\"JP\" salesOrganization=\"3015\" statusCode=\"10\" scanQualified=\"0 \" statusMessage=\"Fully Releasd 4 Sale\" webEnabled=\"true\" seashoppe=\"001\"><orderType>adr</orderType><orderType>order</orderType><orderType>ZPFC</orderType><custType>10</custType><custType>20</custType></ns1:market></ns1:product><ns1:product description=\"ｸﾘｰﾐｰ ｸﾚﾝｼﾞﾝｸﾞ ﾛｰｼｮﾝ\" labOffice=\"NS Face\" sku=\"03110310\"><ns1:market id=\"JP\" salesOrganization=\"3015\" statusCode=\"10\" scanQualified=\"0 \" statusMessage=\"Fully Releasd 4 Sale\" webEnabled=\"true\" seashoppe=\"008\"><orderType>adr</orderType><orderType>order</orderType><orderType>ZPFC</orderType><custType>10</custType><custType>20</custType></ns1:market></ns1:product><ns1:product description=\"AP-24 ﾎﾜｲﾄﾆﾝｸﾞ ﾄｩｰｽﾍﾟｰｽﾄ\" labOffice=\"NS Oral Care\" sku=\"03111155\"><ns1:market id=\"JP\" salesOrganization=\"3015\" statusCode=\"10\" scanQualified=\"0 \" statusMessage=\"Fully Releasd 4 Sale\" webEnabled=\"true\" seashoppe=\"001\"><orderType>adr</orderType><orderType>order</orderType><orderType>ZPFC</orderType><custType>10</custType><custType>20</custType></ns1:market></ns1:product></ns1:productMessage>";

    // test for matt chiang's sku
    public static final String matt = "<?xml version=\"1.0\" encoding=\"utf-8\"?><tns:productMessage operation=\"update\" xmlns:tns=\"http://day.nse.com/products\"><tns:product description=\"Creamy Cleansing Lotion (US)\" labOffice=\"NS Face\" sku=\"01110310\"><tns:market id=\"XX\" statusCode=\"9\" scanQualified=\" 0\" statusMessage=\"\" webEnabled=\"false\" seashoppe=\"\"><orderType>adr</orderType><orderType>order</orderType></tns:market><tns:market id=\"US\" statusCode=\"9\" scanQualified=\" 0\" statusMessage=\"Fully Releasd 4 Sale\" webEnabled=\"true\" seashoppe=\"\"><orderType>adr</orderType><orderType>order</orderType><custType>10</custType><custType>20</custType></tns:market></tns:product></tns:productMessage>";

    public static void send(String host, String user, String pass, String message) throws IOException {
        PostMethod method = new PostMethod();
        //method.setURI(new URI("http://localhost:4502/apps/products/services/productService"));
        method.setRequestBody(message);
        //GetMethod method = new GetMethod();
        System.out.println("host:"+host);
        method.setURI(new URI(host+"/apps/products/services/sapProductService"));

        HttpClient http = new HttpClient();
        http.getParams().setAuthenticationPreemptive(true);
        http.getState().setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(user, pass));
        http.getHttpConnectionManager().getParams().setConnectionTimeout(5000);
        int responseCode = http.executeMethod(method);

        System.out.println("body:"+new String(IOUtils.toString(method.getResponseBodyAsStream())));
        System.out.println("response code: " + responseCode);
    }

    public static void sendToDev(String message) throws IOException {
        send("http://devauth:4502", "admin", "admin", message);
    }

    public static void sendToTest(String message) throws IOException {
        send("http://testauth:4502", "emoore", "abc123", message);
    }

    public static void sendToLocalhost(String message) throws IOException {
        send("http://localhost:4502", "admin", "admin", message);
    }

    public static void main(String[] args) throws IOException {
        //sendToDev(xmlSample);
        //sendToTest(xmlSample);
        for (String sku : skus) {
            //sendToLocalhost(XML_PREFIX + sku + XML_SUFFIX);
            sendToLocalhost(TEST_XML);
        }
    }
}

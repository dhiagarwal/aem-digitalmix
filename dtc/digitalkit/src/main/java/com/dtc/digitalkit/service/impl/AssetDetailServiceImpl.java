package com.dtc.digitalkit.service.impl;

import javax.jcr.Node;
import javax.jcr.Session;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dtc.core.constants.MetadataConstants;
import com.dtc.digitalkit.service.AssetDetailService;


@Component(label = "Asset Detail Service")
@Service(value = AssetDetailService.class)
public class AssetDetailServiceImpl implements AssetDetailService {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	
	@Override
	public JSONObject getAssetDetailByAssetId(String assetId, ResourceResolver resourceResolver) {
		logger.info("In AssetDetailServiceImpl.getAssetDetailByAssetId()");
		JSONObject assetDetail = new JSONObject();
		Session session = null;
		try{
			session = resourceResolver.adaptTo(Session.class);	
			
			javax.jcr.query.QueryManager queryManager = session.getWorkspace().getQueryManager();
			    
			String sqlStatement = "select * from [nt:unstructured] as d where ISDESCENDANTNODE([/content/dam/consumer-direct]) AND d.[dc:assetId] IS NOT NULL AND d.[dc:assetId] = '" +assetId + "'";
			
			javax.jcr.query.Query query = queryManager.createQuery(sqlStatement,"JCR-SQL2");
			 
			javax.jcr.query.QueryResult result = query.execute();
			 
			javax.jcr.NodeIterator nodeIter = result.getNodes();
			logger.info("nodeIter size : "+nodeIter.getSize());
			
			String assetPath = StringUtils.EMPTY;
			String assetName = StringUtils.EMPTY;
			String title = StringUtils.EMPTY;
			String description = StringUtils.EMPTY;
			String contributor = "Deloitte";
			String isSocial = "false";
			String contentType = StringUtils.EMPTY;
			String contentIcon = StringUtils.EMPTY;
			String sprinklrAssetPath = StringUtils.EMPTY;
			
			while ( nodeIter.hasNext() ) {	 				
				javax.jcr.Node node = nodeIter.nextNode();
				assetPath = node.getPath();	
				int len = assetPath.indexOf("/jcr:content/metadata");
				Node assetNode = session.getNode(assetPath.substring(0, len));
				assetName = assetNode.getName();
				if(node.hasProperty(MetadataConstants.DCTITLE)){
					title = node.getProperty(MetadataConstants.DCTITLE).getString();
    			}
    			if(node.hasProperty(MetadataConstants.DCDESCRIPTION)){
    				description = node.getProperty(MetadataConstants.DCDESCRIPTION).getString();
    			}
    			if(node.hasProperty("dc:social")){				
    				isSocial = node.getProperty("dc:social").getString();
    			}
    			if(node.hasProperty("sprinklrAssetPath")){				
    				sprinklrAssetPath = node.getProperty("sprinklrAssetPath").getString();
    			}
    			if(node.hasProperty("dc:format")){
    				String format = node.getProperty("dc:format").getString();
    				if(format.contains("image")){
    					contentType = "Image";
    				}else if(format.contains("video")){
    					contentType = "Video";
    				}else if(format.contains("ms-powerpoint")){
    					contentType = "Presentation";
    				}else if(format.contains("pdf")){
    					contentType = "PDF";
    				}else if(format.contains("audio")){
    					contentType = "Audio";
    				}
    			}
    			if(contentType.contains("Image")){
    				contentIcon = "/etc/clientlibs/dtc/core/main/css/src/assets/images/css/ic-image-asset-copy-19@3x.png";
				}else if(contentType.contains("Video")){
					contentIcon = "/etc/clientlibs/dtc/core/main/css/src/assets/images/css/ic-video-asset-copy-16@3x.png";
				}else if(contentType.contains("Presentation")){
					contentIcon = "/etc/clientlibs/dtc/core/main/css/src/assets/images/css/ic-presentation-asset-copy-18@3x.png";
				}else if(contentType.contains("PDF")){
					contentIcon = "/etc/clientlibs/dtc/core/main/css/src/assets/images/css/ic-pdf-asset-copy-19@3x.png";
				}else if(contentType.contains("Audio")){
					contentIcon = "/etc/clientlibs/dtc/core/main/css/src/assets/images/css/ic-audio-asset-copy-10@3x.png";
				}
			}
			
			assetDetail.put("assetPath", assetPath);
			assetDetail.put("assetName", assetName);
			assetDetail.put("title", title);
			assetDetail.put("description", description);
			assetDetail.put("contributor", contributor);
			assetDetail.put("isSocial", isSocial);
			assetDetail.put("contentType", contentType);
			assetDetail.put("contentIcon", contentIcon);
			assetDetail.put("sprinklrAssetPath", sprinklrAssetPath);
			
		}catch(Exception ex) {
			logger.error(ex.getClass() + ": " + ex.getMessage() + ": " + ex.getCause(), ex);
		}finally{
			session.logout();
		}
		logger.info("Out AssetDetailServiceImpl.getAssetDetailByAssetId()");
		return assetDetail;
	}

}

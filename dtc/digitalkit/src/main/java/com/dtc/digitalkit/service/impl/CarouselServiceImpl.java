package com.dtc.digitalkit.service.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.jcr.ValueFormatException;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dtc.core.constants.MetadataConstants;
import com.dtc.digitalkit.carousel.service.CarouselService;
import com.dtc.digitalkit.digitalkitVO.AssetTileDetails;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


@Service(value = CarouselService.class)
@Component(immediate = true, metatype = true)
public class CarouselServiceImpl implements CarouselService{
	
	protected final Logger LOG = LoggerFactory.getLogger(this.getClass());
	
	
	@Reference
	private ResourceResolverFactory resolverFactory;
	
	
	private ResourceResolver resourceResolver;
	
	public List<AssetTileDetails> getCarouselList(String category) {
		List<AssetTileDetails> assetTileDetailsList = new ArrayList<AssetTileDetails>();
		Session session = null;
		
		try{
			session = resourceResolver.adaptTo(Session.class);	
			
			javax.jcr.query.QueryManager queryManager = session.getWorkspace().getQueryManager();
			    
			String sqlStatement = "select * from [nt:unstructured] as d where ISDESCENDANTNODE([/content/dam/consumer-direct/"+category+"]) AND d.[dc:assetId] IS NOT NULL ORDER BY d.[jcr:lastModified] DESC";
			
			javax.jcr.query.Query query = queryManager.createQuery(sqlStatement,"JCR-SQL2");
			 
			javax.jcr.query.QueryResult result = query.execute();
			 
			javax.jcr.NodeIterator nodeIter = result.getNodes();
			LOG.info("nodeIter size : "+nodeIter.getSize());
			
						
			while ( nodeIter.hasNext() && assetTileDetailsList.size()<=9 ) {
				AssetTileDetails assetTileDetails = null; 
				String assetPath = StringUtils.EMPTY;
				String assetName = StringUtils.EMPTY;
				String assetId = StringUtils.EMPTY;
				String assetSource = StringUtils.EMPTY;
				String title = StringUtils.EMPTY;
				String description = StringUtils.EMPTY;
				String contributor = "Deloitte";
				boolean isSocial = false;
				Date lastModifiedDate =  new Date();
				String contentType = StringUtils.EMPTY;
				String contentIcon = StringUtils.EMPTY;
				String sprinklrAssetPath = StringUtils.EMPTY;
				
				javax.jcr.Node node = nodeIter.nextNode();
				assetPath = node.getPath();	
				int len = assetPath.indexOf("/jcr:content/metadata");
				Node assetNode = session.getNode(assetPath.substring(0, len));
				assetName = assetNode.getName();
				if(node.hasProperty("dc:assetId")){				
    				assetId = node.getProperty("dc:assetId").getString();
    			}
				if(node.hasProperty(MetadataConstants.DCTITLE)){
					title = node.getProperty(MetadataConstants.DCTITLE).getString();
    			}
    			if(node.hasProperty(MetadataConstants.DCDESCRIPTION)){
    				description = node.getProperty(MetadataConstants.DCDESCRIPTION).getString();
    			}
    			if(node.hasProperty("dc:social")){				
    				 if(node.getProperty("dc:social").getString().equals("Yes")){
    					 isSocial = true;
    				 }
    			}
    			if(node.hasProperty("sprinklrAssetPath")){				
    				sprinklrAssetPath = node.getProperty("sprinklrAssetPath").getString();
    			}
    			if(node.hasProperty("dc:format")){
    				String format = node.getProperty("dc:format").getString();
    				if(format.contains("image")){
    					contentType = "Image";
    				}else if(format.contains("video")){
    					contentType = "Video";
    				}else if(format.contains("ms-powerpoint")){
    					contentType = "Presentation";
    				}else if(format.contains("pdf")){
    					contentType = "PDF";
    				}else if(format.contains("audio")){
    					contentType = "Audio";
    				}
    			}
    			if(contentType.contains("Image")){
    				contentIcon = "/etc/clientlibs/dtc/core/main/css/src/assets/images/css/ic-image-asset-copy-19@3x.png";
    				assetPath = assetPath.substring(0, len);
    			}else if(contentType.contains("Video")){
					contentIcon = "/etc/clientlibs/dtc/core/main/css/src/assets/images/css/ic-video-asset-copy-16@3x.png";
					assetPath = "/etc/clientlibs/dtc/core/main/css/src/assets/images/css/ic-video-asset-copy-16@3x.png";
    			}else if(contentType.contains("Presentation")){
					contentIcon = "/etc/clientlibs/dtc/core/main/css/src/assets/images/css/ic-presentation-asset-copy-18@3x.png";
					assetPath = "/etc/clientlibs/dtc/core/main/css/src/assets/images/css/ic-presentation-asset-copy-18@3x.png";
    			}else if(contentType.contains("PDF")){
					contentIcon = "/etc/clientlibs/dtc/core/main/css/src/assets/images/css/ic-pdf-asset-copy-19@3x.png";
					assetPath = "/etc/clientlibs/dtc/core/main/css/src/assets/images/css/ic-pdf-asset-copy-19@3x.png";
    			}else if(contentType.contains("Audio")){
					contentIcon = "/etc/clientlibs/dtc/core/main/css/src/assets/images/css/ic-audio-asset-copy-10@3x.png";
					assetPath = "/etc/clientlibs/dtc/core/main/css/src/assets/images/css/ic-audio-asset-copy-10@3x.png";
    			}
    			assetTileDetails = new AssetTileDetails();
				assetTileDetails.setAssetId(assetId);
				assetTileDetails.setAssetSource(assetSource);
				assetTileDetails.setContentType(contentType);
				assetTileDetails.setThumbnailUrl(assetPath);
				assetTileDetails.setProductDescription(description);
				assetTileDetails.setProductName(title);
				assetTileDetails.setProductLogo(contentIcon);
				assetTileDetails.setFavorite(false);
				assetTileDetails.setLastModifiedDate(lastModifiedDate);
				assetTileDetails.setSocialAssetShareURL(sprinklrAssetPath);
				assetTileDetails.setHasSocialShareURL(isSocial);
				assetTileDetailsList.add(assetTileDetails);
			}
					
			
		}catch(Exception ex) {
			LOG.error(ex.getClass() + ": " + ex.getMessage() + ": " + ex.getCause(), ex);
		}finally{
			session.logout();
		}
		return assetTileDetailsList;
	}
	
	public String getCarouselCategory(String requestPath) {
			
				String category = StringUtils.EMPTY;
				Property property = null;
				try{
					this.resourceResolver = this.resolverFactory.getAdministrativeResourceResolver(null);
					Session session = resourceResolver.adaptTo(Session.class);
					if(null!=session){
						Resource resQuizComponent = resourceResolver.getResource(requestPath);
						Node resQuizNode = resQuizComponent.adaptTo(Node.class);
						if (resQuizNode.hasProperty("categoryheaderlabel")) {
							property = resQuizNode.getProperty("categoryheaderlabel");
						}
						if (property != null) {
							JSONObject obj = null;
							Value[] values = null;
							if (property.isMultiple()) {
								values = property.getValues();
							} else {
								values = new Value[1];
								values[0] = property.getValue();
							}
							for (Value val : values) {
								category = val.toString();

							}
						
							
						}
							
						}
						
					}
		
				catch(Exception e){
					LOG.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
				}			
			
		return category;
	}

	

}

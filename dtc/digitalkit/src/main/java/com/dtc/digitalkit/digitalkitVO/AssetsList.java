package com.dtc.digitalkit.digitalkitVO;

import java.util.ArrayList;
import java.util.List;
import com.dtc.digitalkit.digitalkitVO.AssetTileDetails;;


public class AssetsList {
	
	private List<AssetTileDetails> assetTileDetailList = new ArrayList<AssetTileDetails>();

	/**
	 * @return the assetTileDetailList
	 */
	public List<AssetTileDetails> getAssetTileDetailList() {
		return assetTileDetailList;
	}

	/**
	 * @param assetTileDetailList the assetTileDetailList to set
	 */
	public void setAssetTileDetailList(List<AssetTileDetails> assetTileDetailList) {
		this.assetTileDetailList = assetTileDetailList;
	}
	
	

}

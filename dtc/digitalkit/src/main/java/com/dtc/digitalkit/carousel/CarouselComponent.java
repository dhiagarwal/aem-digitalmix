package com.dtc.digitalkit.carousel;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.Property;
import javax.jcr.ValueFormatException;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.felix.scr.annotations.Reference;

import com.adobe.cq.sightly.WCMUsePojo;
import com.dtc.digitalkit.carousel.service.CarouselService;
import com.dtc.digitalkit.digitalkitVO.AssetTileDetails;
import com.dtc.digitalkit.digitalkitVO.AssetsList;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.GetMethod;


public class CarouselComponent extends WCMUsePojo{
	
	protected final Logger LOG = LoggerFactory.getLogger(this.getClass());
	
	private String assetTileList = StringUtils.EMPTY;
	
	private AssetsList carouselList = new AssetsList();


	@Reference
	private ResourceResolverFactory resolverFactory;
	
	private ResourceResolver resourceResolver;

	/**
	 * @return the carouselList
	 */
	
	
	public String getAssetTileList() {
		return assetTileList;
	}
	/**
	 * @return the carouselList
	 */
	public AssetsList getCarouselList() {
		return carouselList;
	}
	
	
	@Override
	public void activate() throws Exception {
		
		try{
			String category = "empty";
			SlingHttpServletRequest request = getRequest();
			String requestPath = request.getRequestPathInfo().getResourcePath();
			String requestURL = request.getRequestURI().toString(); 
			
			CarouselService carouselService = getSlingScriptHelper().getService(CarouselService.class);
			category = carouselService.getCarouselCategory(requestPath);
			category = category.toLowerCase();
			category = category.replace(" ","-");
			
			List<AssetTileDetails> assetTileDetailsList = carouselService.getCarouselList(category);
			carouselList.setAssetTileDetailList(assetTileDetailsList);
			Gson gson = new Gson();
			assetTileList = gson.toJson(carouselList);		
		}catch(Exception e){
			LOG.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}
		
	}	
	
	
	
}

package com.dtc.digitalkit.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.rmi.ServerException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dtc.digitalkit.service.AssetDetailService;


@Service(AssetDetailServlet.class)
@SlingServlet(paths = { "/bin/dtc/getAssetDetail" }, generateComponent = false)
@Component(label = "Filter Data", description = "Filter Data", enabled = true, immediate = true, metatype = false)
public class AssetDetailServlet extends SlingAllMethodsServlet{

	private static final long serialVersionUID = 3682786132631517602L;
	
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Reference
	AssetDetailService assetDetailService;
	
	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServerException, IOException {
		logger.info("In AssetDetailServlet.doGet()");
		PrintWriter writer = response.getWriter();
		JSONObject assetDetail = new JSONObject();
		try {
			String assetId = request.getParameter("assetId");
			assetDetail = assetDetailService.getAssetDetailByAssetId(assetId, request.getResourceResolver());			
		} catch (Exception ex) {
			logger.error(ex.getClass() + ": " + ex.getMessage() + ": " + ex.getCause(), ex);
		}
		writer.println(assetDetail.toString());
		logger.info("Out AssetDetailServlet.doGet()");
	}

}

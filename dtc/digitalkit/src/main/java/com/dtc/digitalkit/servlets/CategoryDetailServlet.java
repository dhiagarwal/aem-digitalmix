package com.dtc.digitalkit.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.rmi.ServerException;
import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dtc.digitalkit.digitalkitVO.AssetTileDetails;
import com.dtc.digitalkit.digitalkitVO.AssetsList;
import com.dtc.digitalkit.service.AssetDetailService;
import com.dtc.digitalkit.service.CategoryService;
import com.google.gson.Gson;


@Service(CategoryDetailServlet.class)
@SlingServlet(paths = { "/bin/dtc/getCategoryDetail" }, generateComponent = false)
@Component(label = "Full Category", description = "Full Category", enabled = true, immediate = true, metatype = false)
public class CategoryDetailServlet extends SlingAllMethodsServlet{

	private static final long serialVersionUID = 3682786132631517602L;
	
	private final Logger logger = LoggerFactory.getLogger(getClass());
	private AssetsList categoryList = new AssetsList();
	
	@Reference
	CategoryService categoryService;
	
	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServerException, IOException {
		logger.info("In CategoryDetailServlet.doGet()");
		PrintWriter writer = response.getWriter();
		try {
			String category = request.getParameter("category");
			category =  category.toLowerCase();
			category = category.replace(" ","-");
			List<AssetTileDetails> assetTileDetailsList = categoryService.getCategoryList(category, request.getResourceResolver());			
			categoryList.setAssetTileDetailList(assetTileDetailsList);
			Gson gson = new Gson();
			writer.println(gson.toJson(categoryList));
		} catch (Exception ex) {
			logger.error(ex.getClass() + ": " + ex.getMessage() + ": " + ex.getCause(), ex);
		}
		
		logger.info("Out AssetDetailServlet.doGet()");
	}

}

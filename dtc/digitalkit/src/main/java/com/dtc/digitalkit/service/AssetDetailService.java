package com.dtc.digitalkit.service;

import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.commons.json.JSONObject;

public interface AssetDetailService {
	
	public JSONObject getAssetDetailByAssetId(String assetId, ResourceResolver resourceResolver);

}

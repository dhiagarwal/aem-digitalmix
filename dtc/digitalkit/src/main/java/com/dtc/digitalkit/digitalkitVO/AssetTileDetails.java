package com.dtc.digitalkit.digitalkitVO;

import java.util.Date;

public class AssetTileDetails {

	private String assetId;
	private String assetSource;
	private String contentType;
	private String thumbnailUrl;
	private String productDescription;
	private String productName;
	private String productLogo;
	private boolean isFavorite;
	private Date lastModifiedDate;
	private String isSocial;
	private String uniqueId;
	private String socialAssetPreviewURL;
	private String socialAssetShareURL;
	private String socialAssetID;
	private boolean hasSocialShareURL;
	
	/**
	 * @return the assetId
	 */
	public String getAssetId() {
		return assetId;
	}
	/**
	 * @param assetId the assetId to set
	 */
	public void setAssetId(String assetId) {
		this.assetId = assetId;
	}
	/**
	 * @return the assetSource
	 */
	public String getAssetSource() {
		return assetSource;
	}
	/**
	 * @param assetSource the assetSource to set
	 */
	public void setAssetSource(String assetSource) {
		this.assetSource = assetSource;
	}
	/**
	 * @return the contentType
	 */
	public String getContentType() {
		return contentType;
	}
	/**
	 * @param contentType the contentType to set
	 */
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	/**
	 * @return the thumbnailUrl
	 */
	public String getThumbnailUrl() {
		return thumbnailUrl;
	}
	/**
	 * @param thumbnailUrl the thumbnailUrl to set
	 */
	public void setThumbnailUrl(String thumbnailUrl) {
		this.thumbnailUrl = thumbnailUrl;
	}
	/**
	 * @return the productDescription
	 */
	public String getProductDescription() {
		return productDescription;
	}
	/**
	 * @param productDescription the productDescription to set
	 */
	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}
	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}
	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}
	/**
	 * @return the productLogo
	 */
	public String getProductLogo() {
		return productLogo;
	}
	/**
	 * @param productLogo the productLogo to set
	 */
	public void setProductLogo(String productLogo) {
		this.productLogo = productLogo;
	}
	/**
	 * @return the isFavorite
	 */
	public boolean isFavorite() {
		return isFavorite;
	}
	/**
	 * @param isFavorite the isFavorite to set
	 */
	public void setFavorite(boolean isFavorite) {
		this.isFavorite = isFavorite;
	}
	/**
	 * @return the lastModifiedDate
	 */
	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}
	/**
	 * @param lastModifiedDate the lastModifiedDate to set
	 */
	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
	public String isSocial() {
		return isSocial;
	}
	public void setSocial(String isSocial) {
		this.isSocial = isSocial;
	}
	public String getUniqueId() {
		return uniqueId;
	}
	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}
	public String getSocialAssetPreviewURL() {
		return socialAssetPreviewURL;
	}
	public void setSocialAssetPreviewURL(String socialAssetPreviewURL) {
		this.socialAssetPreviewURL = socialAssetPreviewURL;
	}
	public String getSocialAssetShareURL() {
		return socialAssetShareURL;
	}
	public void setSocialAssetShareURL(String socialAssetShareURL) {
		this.socialAssetShareURL = socialAssetShareURL;
	}
	public boolean isHasSocialShareURL() {
		return hasSocialShareURL;
	}
	public void setHasSocialShareURL(boolean hasSocialShareURL) {
		this.hasSocialShareURL = hasSocialShareURL;
	}
	public String getSocialAssetID() {
		return socialAssetID;
	}
	public void setSocialAssetID(String socialAssetID) {
		this.socialAssetID = socialAssetID;
	}
	
	
}

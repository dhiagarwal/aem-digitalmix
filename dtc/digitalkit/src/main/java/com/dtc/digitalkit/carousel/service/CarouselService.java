package com.dtc.digitalkit.carousel.service;

import java.util.List;

import com.dtc.digitalkit.digitalkitVO.AssetTileDetails;


public interface CarouselService {
	
	public String getCarouselCategory(String requestPath);
	public List<AssetTileDetails> getCarouselList(String category);
	
	
}

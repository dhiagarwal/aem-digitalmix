package com.dtc.digitalkit.service;

import java.util.List;

import org.apache.sling.api.resource.ResourceResolver;

import com.dtc.digitalkit.digitalkitVO.AssetTileDetails;


public interface CategoryService {
	
	public List<AssetTileDetails> getCategoryList(String category, ResourceResolver resourceResolver);
	
	
}

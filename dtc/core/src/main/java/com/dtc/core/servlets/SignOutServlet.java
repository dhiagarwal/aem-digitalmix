package com.dtc.core.servlets;

import java.io.IOException;

import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dtc.core.constants.Constants;
import com.dtc.core.helpers.CookieUtils;

@SlingServlet(paths = "/bin/signOutDtc")
public class SignOutServlet extends SlingSafeMethodsServlet {

	private static final long serialVersionUID = 1L;

	private static final Logger log = LoggerFactory.getLogger(SignOutServlet.class);

	public void doGet(SlingHttpServletRequest servletRequest, SlingHttpServletResponse servletResponse)
			throws ServletException, IOException {
		signOut(servletRequest, servletResponse);
	}

	public void doPost(SlingHttpServletRequest servletRequest, SlingHttpServletResponse servletResponse)
			throws ServletException, IOException {
		signOut(servletRequest, servletResponse);
	}

	/**
	 * @param SlingHttpServletRequest
	 * @param SlingHttpServletResponse
	 */
	private void signOut(SlingHttpServletRequest servletRequest, SlingHttpServletResponse servletResponse) {
		CookieUtils.clearCookies(servletRequest, servletResponse, Constants.COOKIE_NAME);
	}
}

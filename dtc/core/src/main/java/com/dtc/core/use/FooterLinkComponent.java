package com.dtc.core.use;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.Value;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;
import com.day.cq.wcm.api.Page;
import com.dtc.core.pojo.FooterLink;
import com.dtc.core.pojo.FooterList;
import com.dtc.core.pojo.FooterNavList;
import com.dtc.core.pojo.LanguageList;


/**
 * This component would fetch the multifield values for the Footer sub sections
 *
 * @author Deloitte Digital
 *
 */
public class FooterLinkComponent extends WCMUsePojo {

	private static final String FOOTER_SUBSECTION_INFO = "detailSubsection";
	private static final Logger LOG = LoggerFactory.getLogger(FooterLinkComponent.class);
	List<FooterLink> footerLink = new ArrayList<FooterLink>();
	private FooterNavList footerNavList;
	private List<LanguageList> languageList = new ArrayList<LanguageList>();
	private List<LanguageList> regionList = new ArrayList<LanguageList>();

	public List<LanguageList> getRegionList() {
		return regionList;
	}

	public List<LanguageList> getLanguageList() {
		return languageList;
	}

	public FooterNavList getFooterNavList() {
		return footerNavList;
	}



	@Override
	public void activate() throws Exception {

		footerNavList = new FooterNavList();
		Property property = null;
		Node currentNode = getResource().adaptTo(Node.class);

		footerNavList = getFooterLinkList(currentNode, property);

		String currentPagePath = getCurrentPage().getPath();
		languageList = getLanguagesList(currentPagePath);
		regionList = getRegionList(currentPagePath);
	}


	private List<LanguageList> getRegionList(String currentPagePath) {
		List<LanguageList> regionList = new ArrayList<LanguageList>();
		Resource pagePathResource = getResourceResolver().getResource(currentPagePath);
		if(pagePathResource != null){
			Page pagePath = pagePathResource.adaptTo(Page.class);
			Page path = pagePath.getAbsoluteParent(1);
			Iterator<Page> regionPages = path.listChildren();
			while(regionPages.hasNext()){
				Page regionPage = regionPages.next();
				Iterator<Page> marketPages = regionPage.listChildren();
				while(marketPages.hasNext()){
					LanguageList list = new LanguageList();
					Page marketPage = marketPages.next();
					String marketPageLength = marketPage.getName();
					if(marketPageLength.length()==2){
						list.setLangCode(marketPage.getName());
						list.setLangName(marketPage.getTitle());
						regionList.add(list);
					}

				}
			}
		}
		return regionList;
	}

	private List<LanguageList> getLanguagesList(String currentPagePath) {
		List<LanguageList> languageList = new ArrayList<LanguageList>();

		Resource pagePathResource = getResourceResolver().getResource(currentPagePath);
			if(pagePathResource != null){
					Page pagePath = pagePathResource.adaptTo(Page.class);
					Page marketPath = pagePath.getAbsoluteParent(3);
					Iterator<Page> languagePages = marketPath.listChildren();
					while(languagePages.hasNext()){
						LanguageList list = new LanguageList();
						Page languagePage = languagePages.next();
						String languagePageLength = languagePage.getName();
						if(languagePageLength.length()==2){
							Locale locale = new Locale(languagePage.getName());
							list.setLangCode(languagePage.getName());
							if(languagePage.getPageTitle()!=null){
								list.setLangName(languagePage.getPageTitle());
							} else {
								list.setLangName(locale.getDisplayLanguage(new Locale(languagePage.getName())));
							}
							languageList.add(list);
						}

					}

				}
		return languageList;
	}

	private FooterNavList getFooterLinkList(Node currentNode, Property property) {
		FooterNavList footerNavList = new FooterNavList();
		List<FooterList> footerLists = new ArrayList<FooterList>();
		try {

			if (currentNode.hasProperty(FOOTER_SUBSECTION_INFO)) {
				property = currentNode.getProperty(FOOTER_SUBSECTION_INFO);

				if (property != null) {
					JSONObject obj = null;
					Value[] values = null;

					if (property.isMultiple()) {
						values = property.getValues();
					} else {
						values = new Value[1];
						values[0] = property.getValue();
					}
					for (Value val : values) {
						FooterList footerList = new FooterList();
						String sectionLabel = StringUtils.EMPTY;
						String sectionId=StringUtils.EMPTY;
						obj = new JSONObject(val.getString());
						if (null != obj.get("sectionLabel")) {
							sectionLabel = obj.get("sectionLabel").toString();
							sectionId=obj.get("sectionLabel").toString().replace(' ', '-');
						}
						footerList.setSectionLabel(sectionLabel);
						footerList.setSectionId(sectionId);
						if (null != obj.get("Links")) {
							JSONArray linksArray = new JSONArray(obj.get("Links").toString());
							List<FooterLink> links = new ArrayList<FooterLink>();
							for (int eachLink = 0; eachLink < linksArray.length(); eachLink++) {
								FooterLink footerLink = new FooterLink();
								JSONObject linkObj = linksArray.getJSONObject(eachLink);
								if (null != linkObj.get("linkLabel")) {
									footerLink.setLinkLabel(linkObj.get("linkLabel").toString());
								}
								if (null != linkObj.get("linkUrl")) {
									footerLink.setLinkUrl(linkObj.get("linkUrl").toString());
								}
								links.add(footerLink);
							}
							footerList.setLinks(links);
						}

						footerLists.add(footerList);
					}

				}
				footerNavList.setFooterList(footerLists);

			}

		} catch (Exception e) {
			LOG.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}
		return footerNavList;
	}

}

package com.dtc.core.services.impl;

import com.dtc.core.services.EnvironmentConfig;
import java.util.Dictionary;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(label = "Environment Configurations", metatype = true, description = "Service that handles Environment Configurations", immediate = true)
@Service(value = EnvironmentConfig.class)
public class EnvironmentConfigImpl implements EnvironmentConfig {

private static final Logger log = LoggerFactory.getLogger(EnvironmentConfigImpl.class);
	
	@Property(label = "Sprinklr API Key")
	private static final String SPRINKLR_API_KEY = "sprinklr_api_key";
	
	@Property(label = "Sprinklr Token")
	private static final String SPRINKLR_TOKEN = "sprinklr_token";
	
	@Property(label = "Sprinklr USER Create Client ID")
	private static final String SPRINKLR_USER_CREATE_CLIENT_ID = "sprinklr_user_create_client_id";
	
	@Property(label = "Sprinklr USER Create End Point")
	private static final String SPRINKLR_USER_CREATE_END_POINT = "sprinklr_user_create_end_point";
	
	@Property(label = "Sprinklr Upload End Point")
	private static final String SPRINKLR_UPLOAD_END_POINT = "sprinklr_upload_end_point";
	
	@Property(label = "Sprinklr Create End Point")
	private static final String SPRINKLR_CREATE_END_POINT = "sprinklr_create_end_point";
	
	@Property(label = "Sprinklr App Access End Point")
	private static final String SPRINKLR_APP_ACCESS_END_POINT = "sprinklr_app_access_end_point";
	
	@Property(label = "Sprinklr Upload DAM End Point")
	private static final String SPRINKLR_DAM_UPLOAD_END_POINT = "sprinklr_DAM_Upload_End_Point";
	
	@Property(label = "Sprinklr Token End Point")
	private static final String SPRINKLR_TOKEN_END_POINT = "sprinklr_Token_End_Point";
	
	@Property(label = "Sprinklr Base URL")
	private static final String SPRINKLR_BASE_URL = "sprinklrBaseURL";
	
	@Property(label = "Mulesoft Hybris End Point")
	private static final String MULESOFT_HYBRIS_ENDPOINT = "mulesoft.hybris_endpoint";
	
	@Property(label = "Mulesoft Hybris Guest End Point")
	private static final String MULESOFT_HYBRIS_GUEST_ENDPOINT = "mulesoft.hybris_guest_endpoint";
	
	@Property(label = "Mulesoft Solr End Point")
	private static final String MULESOFT_SOLR_ENDPOINT = "mulesoft.solr_endpoint";	
	
	@Property(label = "Mulesoft Storefront End Point")
	private static final String MULESOFT_STOREFRONT_ENDPOINT = "mulesoft.storefront_endpoint";
	
	@Property(label = "Mulesoft MEC Storefront End Point")
	private static final String MULESOFT_MEC_STOREFRONT_ENDPOINT = "mulesoft.mec_storefront_endpoint";
	
	@Property(label = "Mulesoft Validate Token End Point")
	private static final String MULESOFT_TOKEN_ENDPOINT = "mulesoft.token_endpoint";
	
	@Property(label = "Mulesoft Client Id")
	private static final String MULESOFT_CLIENT_ID = "mulesoft.client_id";
	
	@Property(label = "Mulesoft Client Secret")
	private static final String MULESOFT_CLIENT_SECRET = "mulesoft.client_secret";
	
	@Property(label = "S3 Bucket URL")
	private static final String S3_BUCKET_URL = "s3_bucket_url";
	
	@Property(label = "Atlas Service URL")
	private static final String ATLAS_SERVICE_URL = "atlas_service_url";

	@Property(label = "Shorten Service URL")
	private static final String SHORTEN_URL_SERVICE_URL = "shortenURL_service_url";

	@Property(label = "Shorten Service Key")
	private static final String SHORTEN_URL_SERVICE_KEY = "shortenURL_service_key";
	
	@Property(label = "QAS Service URL")
	private static final String QAS_SERVICE_URL = "qas_service_url";
	
	@Property(label = "Analytics Js URL")
	private static final String ANALYTICS_JS_URL = "analytics_js_url";
	
	@Property(label = "Google Api Key")
	private static final String GOOGLE_API_KEY = "googleApiKey";
	
	private String mulesoft_hybris_endpoint;
	
	private String mulesoft_hybris_guest_endpoint;

	private String mulesoft_solr_endpoint;
	
	private String mulesoft_storefront_endpoint;
	
	private String mulesoft_mec_storefront_endpoint;
	
	private String mulesoft_token_endpoint;
	
	private String mulesoft_client_id;
	
	private String mulesoft_client_secret;
	
	private String s3_bucket_url;
	
	private String atlas_service_url;

	private String shortenURL_service_url;

	private String shortenURL_service_key;
	
	private String qas_service_url;
	
	private String analytics_js_url;
	
	private String googleApiKey;


	
	private String sprinklr_api_key;
	
	private String sprinklr_token;
	
	private String sprinklr_user_create_client_id;
	
	private String sprinklr_user_create_end_point;
	
	private String sprinklr_upload_end_point;
	
	private String sprinklr_create_end_point;
	
	private String sprinklr_app_access_end_point;
	
	private String sprinklr_DAM_Upload_End_Point;
	
	private String sprinklr_Token_End_Point;
	
	private String sprinklrBaseURL;
	
	@Activate
	protected void activate(final ComponentContext context) {
		final Dictionary<?, ?> config = context.getProperties();
			
		setSprinklr_api_key(PropertiesUtil.toString(config.get(SPRINKLR_API_KEY), null));
		setSprinklr_token(PropertiesUtil.toString(config.get(SPRINKLR_TOKEN), null));
		setSprinklr_user_create_client_id(PropertiesUtil.toString(config.get(SPRINKLR_USER_CREATE_CLIENT_ID), null));
		setSprinklr_user_create_end_point(PropertiesUtil.toString(config.get(SPRINKLR_USER_CREATE_END_POINT), null));
		setSprinklr_upload_end_point(PropertiesUtil.toString(config.get(SPRINKLR_UPLOAD_END_POINT), null));		
		setSprinklr_create_end_point(PropertiesUtil.toString(config.get(SPRINKLR_CREATE_END_POINT), null));		
		setSprinklr_app_access_end_point(PropertiesUtil.toString(config.get(SPRINKLR_APP_ACCESS_END_POINT), null));
		setSprinklrDAMUploadEndPoint(PropertiesUtil.toString(config.get(SPRINKLR_DAM_UPLOAD_END_POINT), null));
		setSprinklr_Token_End_Point(PropertiesUtil.toString(config.get(SPRINKLR_TOKEN_END_POINT), null));
		setSprinklrBaseURL(PropertiesUtil.toString(config.get(SPRINKLR_BASE_URL), null));
		setMulesoftHybrisEndpoint(PropertiesUtil.toString(config.get(MULESOFT_HYBRIS_ENDPOINT), null));
		setMulesoftHybrisGuestEndpoint(PropertiesUtil.toString(config.get(MULESOFT_HYBRIS_GUEST_ENDPOINT), null));
		setMulesoftSolrEndpoint(PropertiesUtil.toString(config.get(MULESOFT_SOLR_ENDPOINT), null));		
		setMulesoftStorefrontEndpoint(PropertiesUtil.toString(config.get(MULESOFT_STOREFRONT_ENDPOINT), null));
		setMulesoftMECStorefrontEndpoint(PropertiesUtil.toString(config.get(MULESOFT_MEC_STOREFRONT_ENDPOINT), null));
		setMulesoftTokenEndpoint(PropertiesUtil.toString(config.get(MULESOFT_TOKEN_ENDPOINT), null));
		setMulesoftClientId(PropertiesUtil.toString(config.get(MULESOFT_CLIENT_ID), null));
		setMulesoftClientSecret(PropertiesUtil.toString(config.get(MULESOFT_CLIENT_SECRET),null));
		setS3BucketURL(PropertiesUtil.toString(config.get(S3_BUCKET_URL),null));
		setAtlas_service_url(PropertiesUtil.toString(config.get(ATLAS_SERVICE_URL), null));
		setShortenURL_service_url(PropertiesUtil.toString(config.get(SHORTEN_URL_SERVICE_URL), null));
		setShortenURL_service_key(PropertiesUtil.toString(config.get(SHORTEN_URL_SERVICE_KEY), null));
		setQas_service_url(PropertiesUtil.toString(config.get(QAS_SERVICE_URL),null));
		setAnalytics_js_url(PropertiesUtil.toString(config.get(ANALYTICS_JS_URL), null));
		setGoogleApiKey(PropertiesUtil.toString(config.get(GOOGLE_API_KEY), null));
		
	}

	public void setSprinklr_api_key(String sprinklr_api_key) {
		this.sprinklr_api_key = sprinklr_api_key;
	}

	public void setSprinklr_token(String sprinklr_token) {
		this.sprinklr_token = sprinklr_token;
	}
	
	public void setSprinklr_user_create_client_id(String sprinklr_user_create_client_id) {
		this.sprinklr_user_create_client_id = sprinklr_user_create_client_id;
	}
	
	public void setSprinklr_user_create_end_point(String sprinklr_user_create_end_point) {
		this.sprinklr_user_create_end_point = sprinklr_user_create_end_point;
	}

	public void setSprinklr_upload_end_point(String sprinklr_upload_end_point) {
		this.sprinklr_upload_end_point = sprinklr_upload_end_point;
	}

	public void setSprinklr_create_end_point(String sprinklr_create_end_point) {
		this.sprinklr_create_end_point = sprinklr_create_end_point;
	}

	public void setSprinklr_app_access_end_point(String sprinklr_app_access_end_point) {
		this.sprinklr_app_access_end_point = sprinklr_app_access_end_point;
	}
	
	public void setSprinklrDAMUploadEndPoint(String sprinklr_DAM_Upload_End_Point) {
		this.sprinklr_DAM_Upload_End_Point = sprinklr_DAM_Upload_End_Point;
	}
	
	public void setSprinklr_Token_End_Point(String sprinklr_Token_End_Point) {
		this.sprinklr_Token_End_Point = sprinklr_Token_End_Point;
	}
	
	public void setSprinklrBaseURL(String sprinklrBaseURL) {
		this.sprinklrBaseURL = sprinklrBaseURL;
	}

	public String getSprinklrDAMUploadEndPoint() {
		return sprinklr_DAM_Upload_End_Point;
	}

	public String getSprinklrAPIKey() {
		return sprinklr_api_key;
	}

	public String getSprinklrToken() {
		return sprinklr_token;
	}
	
	public String getSprinklrUserCreateClientId() {
		return sprinklr_user_create_client_id;
	}
	
	public String getSprinklrUserCreateEndPoint() {
		return sprinklr_user_create_end_point;
	}

	public String getSprinklrUploadEndPoint() {
		return sprinklr_upload_end_point;
	}

	public String getSprinklrCreateEndPoint() {
		return sprinklr_create_end_point;
	}

	public String getSprinklrAppAccessEndPoint() {
		return sprinklr_app_access_end_point;
	}

	public String getSprinklrBaseURL() {
		return sprinklrBaseURL;
	}

	public String getSprinklrTokenEndPoint() {
		return sprinklr_Token_End_Point;
	}
	
	public void setMulesoftHybrisEndpoint(final String endpoint) {
		this.mulesoft_hybris_endpoint = endpoint;
	}

	public void setMulesoftHybrisGuestEndpoint(String mulesoft_guest_endpoint) {
		this.mulesoft_hybris_guest_endpoint = mulesoft_guest_endpoint;
	}
	
	public void setMulesoftSolrEndpoint(String mulesoft_endpoint) {
		this.mulesoft_solr_endpoint = mulesoft_endpoint;
	}
	
	public void setMulesoftStorefrontEndpoint(String mulesoft_endpoint) {
		this.mulesoft_storefront_endpoint = mulesoft_endpoint;
	}
	
	public void setMulesoftMECStorefrontEndpoint(String mulesoft_mec_storefront_endpoint) {
		this.mulesoft_mec_storefront_endpoint = mulesoft_mec_storefront_endpoint;
	}
	
	public void setMulesoftTokenEndpoint(String mulesoft_endpoint) {
		this.mulesoft_token_endpoint= mulesoft_endpoint;
	}
	
	public void setMulesoftClientId(String mulesoft_client_id) {
		this.mulesoft_client_id = mulesoft_client_id;
	}

	public void setMulesoftClientSecret(String mulesoft_client_secret) {
		this.mulesoft_client_secret = mulesoft_client_secret;
	}
	
	public void setS3BucketURL(final String url) {
		this.s3_bucket_url = url;
	}
	
	public void setAtlas_service_url(String atlas_service_url) {
		this.atlas_service_url = atlas_service_url;
	}

	public void setShortenURL_service_url(String shortenURL_service_url) {
		this.shortenURL_service_url = shortenURL_service_url;
	}

	public void setShortenURL_service_key(String shortenURL_service_key) {
		this.shortenURL_service_key = shortenURL_service_key;
	}
	
	public void setQas_service_url(String qas_service_url) {
		this.qas_service_url = qas_service_url;
	}
	
	public void setAnalytics_js_url(String analytics_js_url) {
		this.analytics_js_url = analytics_js_url;
	}
	
	public void setGoogleApiKey(String googleApiKey) {
		this.googleApiKey = googleApiKey;
	}
	

	public String getMulesoftHybrisEndpoint() {
		return mulesoft_hybris_endpoint;
	}

	public String getMulesoftHybrisGuestEndpoint() {
		return mulesoft_hybris_guest_endpoint;
	}
	

	public String getMulesoftSolrEndpoint(){
		return mulesoft_solr_endpoint;
	}
	

	public String getMulesoftStorefrontEndpoint(){
		return mulesoft_storefront_endpoint;
	}
	

	public String getMulesoftStoreMECfrontEndpoint(){
		return mulesoft_mec_storefront_endpoint;
	}

	public String getMulesoftTokenEndpoint(){
		return mulesoft_token_endpoint;
	}
	

	public String getMulesoftClientId(){
		return mulesoft_client_id;
	}
	

	public String getMulesoftClientSecret(){
		return mulesoft_client_secret;
	}
	

	public String getS3BucketURL(){
		return s3_bucket_url;
	}
	

	public String getAtlas_service_url() {
		return atlas_service_url;
	}


	public String getShortenServiceUrl() {
		return this.shortenURL_service_url;
	}


	public String getShortenServiceKey() {
		return this.shortenURL_service_key;
	}
	

	public String getQas_service_url() {
		return qas_service_url;
	}

	public String getAnalytics_js_url() {
		return analytics_js_url;
	}
	

	public String getGoogleApiKey() {
		return googleApiKey;
	}

}

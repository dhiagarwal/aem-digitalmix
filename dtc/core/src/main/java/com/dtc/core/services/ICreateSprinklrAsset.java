package com.dtc.core.services;

import java.io.InputStream;
import java.util.Date;
import org.json.simple.JSONObject;

import org.apache.http.HttpResponse;

public interface ICreateSprinklrAsset {
	
	public JSONObject createUser( String sprinklrUserCreateEndPoint, String sprinklrUserCreateClientId, String userName, String familyName, String givenName);
	public HttpResponse uploadAsset( String fileName, String contentType, InputStream is);
	public String createAsset(String type, String response_string, String assetName, String assetDesc, Date assetExpiry, String createdBy, String[] productTags, String[] industryTags, String[] sectorTags, String[] tags);
	public boolean deleteAsset(String id);

}

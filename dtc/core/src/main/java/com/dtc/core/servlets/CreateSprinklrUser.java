package com.dtc.core.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dtc.core.services.EnvironmentConfig;
import com.dtc.core.services.impl.CreateSprinklrAssetImpl;
import org.json.simple.JSONObject;

@SlingServlet(paths = "/bin/createSprinklrUser")
public class CreateSprinklrUser extends SlingSafeMethodsServlet{
	
	
	private static final long serialVersionUID = 5992267943659688399L;

	@Reference
	private transient EnvironmentConfig environmentConfig;
	
	private static final Logger log = LoggerFactory.getLogger(SignUpServlet.class);
	
	public void doGet(SlingHttpServletRequest servletRequest, SlingHttpServletResponse servletResponse)
			throws ServletException, IOException {
		log.info("In Servlet.doGet()");
		
		String sam_app = environmentConfig.getSprinklrAppAccessEndPoint();		
		String accessTokenValue = "Bearer " + environmentConfig.getSprinklrToken();
		String key = environmentConfig.getSprinklrAPIKey();
		String sprinklrEndPoint = environmentConfig.getSprinklrCreateEndPoint();
		String sprinklrDAMUploadEndPoint = environmentConfig.getSprinklrDAMUploadEndPoint();
		String sprinklrUserCreateEndPoint = environmentConfig.getSprinklrUserCreateEndPoint();
		String sprinklrUserCreateClientId = environmentConfig.getSprinklrUserCreateClientId();
		String userName = servletRequest.getParameter("userName");
		String familyName = servletRequest.getParameter("familyName");
		String givenName = servletRequest.getParameter("givenName");
		
		log.info("sam_app : "+sam_app);
		log.info("accessTokenValue : "+accessTokenValue);
		log.info("key : "+key);
		log.info("sprinklrEndPoint : "+sprinklrEndPoint);
		log.info("sprinklrDAMUploadEndPoint : "+sprinklrDAMUploadEndPoint);
		log.info("sprinklrUserCreateEndPoint : "+sprinklrUserCreateEndPoint);
		log.info("sprinklrUserCreateClientId : "+sprinklrUserCreateClientId);
		log.info("userName : "+userName);
		log.info("familyName : "+familyName);
		log.info("givenName : "+givenName);
		
		CreateSprinklrAssetImpl sprinklrAssetObj = new CreateSprinklrAssetImpl(accessTokenValue, key,sprinklrEndPoint , sprinklrDAMUploadEndPoint);
		JSONObject response = sprinklrAssetObj.createUser(sprinklrUserCreateEndPoint, sprinklrUserCreateClientId, userName, familyName, givenName);
		PrintWriter writer = servletResponse.getWriter();
		writer.println(response.toString());
		
		log.info("out Servlet.doGet()");
	}
	
	public void doPost(SlingHttpServletRequest servletRequest, SlingHttpServletResponse servletResponse)
			throws ServletException, IOException {
		//signUp(servletRequest, servletResponse);
	}

}

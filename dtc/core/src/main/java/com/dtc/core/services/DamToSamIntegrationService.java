package com.dtc.core.services;

public interface DamToSamIntegrationService {
	
	public boolean damToSamIntegrate(String assetPath, String replicationType);

}

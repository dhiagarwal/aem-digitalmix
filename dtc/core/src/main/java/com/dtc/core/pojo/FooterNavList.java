
package com.dtc.core.pojo;

import java.util.ArrayList;
import java.util.List;

public class FooterNavList {

	private List<FooterList> footerList = new ArrayList<FooterList>();

	/**
	 *
	 * @return The footerList
	 */
	public List<FooterList> getFooterList() {
		return footerList;
	}

	/**
	 *
	 * @param footerList
	 *            The FooterList
	 */
	public void setFooterList(List<FooterList> footerList) {
		this.footerList = footerList;
	}

}

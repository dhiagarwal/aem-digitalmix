package com.dtc.core.schedulers;

import com.dtc.core.services.EnvironmentConfig;
import com.dtc.core.utils.SolrUtils;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.scheduler.Scheduler;
import org.apache.sling.settings.SlingSettingsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

/**
 * Created by mctebbs on 2/8/17.
 */
@Component(metatype=true, label="Solr Product Update Processor", description="Solr Posts product updates and deletes")
public class SolrProductUpdateProcessor implements Runnable {
    private static final Logger log = LoggerFactory.getLogger(SolrProductUpdateProcessor.class);

    @Reference
    private Scheduler scheduler;

    @Reference
    private SlingSettingsService slingSettingsService;

    @Reference
    private EnvironmentConfig environmentConfig;

    @Property(name = "enableSolrUpdate", description = "Enable the Solr update processing queue", boolValue = false)
    private boolean enableSolrUpdate;

    @Property(name = "queuePeriod", description = "The period of the poller in seconds.", longValue = 30)
    private long period; // In seconds

    @Override
    public void run() {
        if (enableSolrUpdate) {
            try {
                updateSolrProducts();
            } catch (Exception e) {
                log.error("Exception occurred in run method", e);
            }
        }
    }

    public void updateSolrProducts(){
        JSONArray productUpdates = SolrProductsJSONObject.getProducts();

        // Update the Solr products.
        if(productUpdates != null && productUpdates.length() > 0) {
            try {
                SolrUtils.writeProducts(productUpdates, environmentConfig);
            } catch (JSONException e) {
                log.error("Update Solr products failed - JSONException");
            } catch (IOException e) {
                log.error("Update Solr products failed - IOException");
            }
        }
    }

    //-------------------------------------------------------------------------------
    //
    //                                Protected Methods
    //
    //-------------------------------------------------------------------------------
    /**
     * Called when the bundle is activated. Schedules the Solr product update cron job.
     * @param config The configuration.
     */
    @Activate
    protected void activate(Map config) {
        try {
            enableSolrUpdate = (boolean) config.get("enableSolrUpdate");
            period = (Long) config.get("queuePeriod");
            log.debug("period from config:" + period);

            Set<String> runModes = slingSettingsService.getRunModes();

            // We don't want to schedule anything on author. Only one pub server should have a listener for pmd updates.
            if (runModes.contains("publish")) { // Good code
//            if (runModes.contains("author")) {    // Test code
                scheduler.addPeriodicJob(getClass().getName(), this, null, period, false);
                log.debug("Added periodic job. Period:" + period);
            }
        } catch (Exception e) {
            log.error("Exception during activate of FirebaseProcessingQueue.", e);
        }
    }

    /**
     * Called when the bundle is deactivated. Removes the Solr product update cron job.
     * @param config The configuration.
     */
    @Deactivate
    protected void deactivate(Map<String, String> config) {
        scheduler.removeJob(getClass().getName());
    }
}

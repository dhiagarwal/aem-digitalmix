package com.dtc.core.services.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dtc.core.constants.MetadataConstants;
import com.dtc.core.services.ICreateSprinklrAsset;

public class CreateSprinklrAssetImpl implements ICreateSprinklrAsset {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	String accessToken = StringUtils.EMPTY;
	String key = StringUtils.EMPTY;
	String sprinklrEndPoint = StringUtils.EMPTY;
	String sprinklrDAMUploadEndPoint = StringUtils.EMPTY;
	
	public CreateSprinklrAssetImpl(String accessToken, String key , String sprinklrEndPoint , String sprinklrDAMUploadEndPoint) {
		logger.info("In CreateSprinklrAssetImpl constructor");
		this.accessToken = accessToken;
		this.key = key;
		this.sprinklrEndPoint = sprinklrEndPoint;
		this.sprinklrDAMUploadEndPoint = sprinklrDAMUploadEndPoint;
		logger.info("Out CreateSprinklrAssetImpl constructor");
	}
	
	public JSONObject createUser(String sprinklrUserCreateEndPoint, String sprinklrUserCreateClientId, String userName, String familyName, String givenName){
		logger.info("In CreateSprinklrAssetImpl.createUser()");
		HttpResponse response = null;
		JSONObject res = null;
		//String responseAfterCreate = StringUtils.EMPTY;
		try{
			CloseableHttpClient httpClient = HttpClients.createDefault();
			//String url = "https://api2.sprinklr.com/api/v1/scim/v2/Users";
			String url = sprinklrUserCreateEndPoint;
			
			String f = "familyName";
			String g = "givenName";
			String c = "clientId";
			String b = "businessCategory";
			String t = "true";
			String email = "\"userName\":\"" + userName + "\"";
			String name = "\"name\":{\"" + f + "\":\"" + familyName + "\",\"" + g + "\":\"" + givenName + "\"}";
			String active = "\"active\":" + t;
			//String clientAttributes = "\"clientAttributes\":[{\"" + c + "\":" + 3454 + ",\"" + b + "\":\"DISTRIBUTED\"" + "}]";
			String clientAttributes = "\"clientAttributes\":[{\"" + c + "\":" + sprinklrUserCreateClientId + ",\"" + b + "\":\"DISTRIBUTED\"" + "}]";
			
			
			HttpPost userCreate = new HttpPost(url);
			userCreate.addHeader("Authorization", accessToken);
			userCreate.addHeader("key", key);
			userCreate.addHeader("Content-Type", "application/scim+json");

			StringEntity input = new StringEntity(
					"{" + email + "," + name + "," + active + "," + clientAttributes + "}");
			logger.info("input in create --- "+"{" + email + "," + name + "," + active + "," + clientAttributes + "}");
			userCreate.setEntity(input);

			logger.info("Executing User Create service:");
			response = httpClient.execute(userCreate);
			
			int statusCode = response.getStatusLine().getStatusCode();
			res = new JSONObject();
			res.put("statusCode", statusCode);
			if(statusCode != 200)
				res.put("reason", response.getStatusLine().getReasonPhrase());
			
			/*if (statusCode == 200) {
				logger.info("status == 200");
				responseAfterCreate = EntityUtils.toString(response.getEntity());
				logger.info("responseAfterCreate - "+responseAfterCreate);
				responseAfterCreate = responseAfterCreate.substring(1, responseAfterCreate.length() - 1);
				logger.info("responseAfterCreate1 - "+responseAfterCreate);
				res = new JSONObject();
				res.put("statusCode", statusCode);
				res.put("reason", response.getStatusLine().getReasonPhrase());
			}else{
				logger.info("statusCode == "+statusCode);
				logger.info("status == "+response.getStatusLine().getReasonPhrase());
			}*/
			
		}catch (ClientProtocolException e) {
			logger.info("ClientProtocolException in Create User: " + e);
		}catch (IOException e) {
			logger.info("IOException in Create User: " + e);
		}
		logger.info("Out CreateSprinklrAssetImpl.createUser()");
		//return responseAfterCreate;
		return res;
	}
	
	public HttpResponse uploadAsset(String fileName, String contentType, InputStream is) {
		logger.info("In CreateSprinklrAssetImpl.uploadAsset()");
		HttpResponse response = null;
		try {
			CloseableHttpClient httpClient = HttpClients.createDefault();
			logger.info("sprinklrDAMUploadEndPoint - "+sprinklrDAMUploadEndPoint);
			final String uuid = UUID.randomUUID().toString().replaceAll("-", "");

			String uploadTrackerId = uuid;
			String url = sprinklrDAMUploadEndPoint + contentType + "&uploadTrackerId="
					+ uploadTrackerId;
			logger.info("url - "+url);
			HttpPost assetUpload = new HttpPost(url);
			assetUpload.addHeader("Authorization", accessToken);
			assetUpload.addHeader("key", key);

			MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			
			logger.info("fileName - "+fileName);
			if(null == is)
				logger.info("IS is null - ");
			else
				logger.info("IS is not null - ");
			
			if(MetadataConstants.VIDEOCAP.equals(contentType))
				fileName = fileName + ".mp4";

			builder.addBinaryBody("file", is, ContentType.APPLICATION_OCTET_STREAM, fileName);
			HttpEntity multipart = builder.build();

			assetUpload.setEntity(multipart);
			response = httpClient.execute(assetUpload);

		} catch (ClientProtocolException e) {
			logger.info("ClientProtocolException in uploading Asset: " + e);
		} catch (IOException e) {
			logger.info("IOException in uploading Asset: " + e);
		}
		logger.info("Out CreateSprinklrAssetImpl.uploadAsset()");
		return response;

	}

	public String createAsset(String Type, String response_string, String assetName, String assetDesc, Date assetExpiry, String createdBy, String[] productTags, String[] industryTags, String[] sectorTags, String[] tags) {
		logger.info("In CreateSprinklrAssetImpl.createAsset()");
		CloseableHttpClient httpClient = HttpClients.createDefault();
		String responseAfterCreate = StringUtils.EMPTY;
		String createdAssetId = StringUtils.EMPTY;
		String proTag = getPartnerCustomFields(productTags);
		String indTag = getPartnerCustomFields(industryTags);
		String secTag = getPartnerCustomFields(sectorTags);
		try {
			logger.info("sprinklrEndPoint in create - "+sprinklrEndPoint);
			JSONParser parser = new JSONParser();
			JSONObject json = (JSONObject) parser.parse(response_string);
			String contentName = "\"name\":\"" + assetName + "\"";
			String contentDesc = "\"description\":\"" + assetDesc + "\"";
			String expiry = "\"expiryTime\":\"" + assetExpiry.getTime() + "\"";
			String id = "\"uploadedContentId\":\"" + (String) json.get("id") + "\"";
			String contentType = "\"assetType\":" + "\"" + Type + "\"";
			String partnerCustomFields = "\"partnerCustomFields\":{\"" + MetadataConstants.SAMBOARDINDUSTRY + "\":" + indTag + ",\"" + MetadataConstants.SAMBOARDPRODUCT + "\":" + proTag + ",\"" + MetadataConstants.SAMBOARDSECTOR + "\":" + secTag + "}";
			
			logger.info("partnerCustomFields - "+partnerCustomFields);
			
			HttpPost assetCreate = new HttpPost(sprinklrEndPoint);
			assetCreate.addHeader("Authorization", accessToken);
			assetCreate.addHeader("key", key);
			assetCreate.addHeader("Content-Type", "application/json");

			StringEntity input = new StringEntity(
					"{" + contentName + "," + contentDesc + "," + expiry + "," + contentType + "," + id + "," + partnerCustomFields + "}");
			logger.info("input in create --- "+"{" + contentName + "," + contentDesc + "," + expiry + "," + contentType + "," + id + "," + partnerCustomFields + "}");
			assetCreate.setEntity(input);

			logger.info("Executing Create service:");
			HttpResponse response = httpClient.execute(assetCreate);
			
			int statusCode = response.getStatusLine().getStatusCode();
			if (statusCode == 200) {
				logger.info("status == 200");
				responseAfterCreate = EntityUtils.toString(response.getEntity());
				logger.info("responseAfterCreate - "+responseAfterCreate);
				responseAfterCreate = responseAfterCreate.substring(1, responseAfterCreate.length() - 1);
				JSONParser newParser = new JSONParser();
				JSONObject jsonID = (JSONObject) newParser.parse(responseAfterCreate);
				createdAssetId = (String) jsonID.get("id");
			}

		} catch (ClientProtocolException e) {
			logger.info(e.toString());
		} catch (IOException e) {
			logger.info(e.toString()+ e);
		} catch (JSONException e) {
			logger.info(e.toString());
		} catch (ParseException e) {
			logger.info(e.toString());
		}
		logger.info("Out CreateSprinklrAssetImpl.createAsset()");
		return createdAssetId;
	}

	public boolean deleteAsset(String id) {
		CloseableHttpClient httpClient = HttpClients.createDefault();
		boolean isAssetDeleted = false;
		try {
			logger.info("sprinklrEndPoint in delete - "+sprinklrEndPoint);
			String url = sprinklrEndPoint + "/" + id; 
			HttpDelete assetDelete = new HttpDelete(url);
			assetDelete.addHeader("Authorization", accessToken);
			assetDelete.addHeader("key", key);

			logger.debug("Executing Image delete service:");
			HttpResponse response = httpClient.execute(assetDelete);
			int statusCode = response.getStatusLine().getStatusCode();

			if (statusCode == 200) {
				isAssetDeleted = true;
			}

			logger.info("response after delete - " + EntityUtils.toString(response.getEntity()));

		} catch (ClientProtocolException e) {
			logger.info(e.toString());
		} catch (IOException e) {
			logger.info(e.toString());
		} catch (JSONException e) {
			logger.info(e.toString());
		}
		return isAssetDeleted;
	}
	
	public String getPartnerCustomFields(String[] tags) {
		String tag = "[";
		for (int i = 0; i < tags.length; i++) {

			if (i != tags.length - 1) {
				tag = tag + "\"" + tags[i] + "\",";
			} else {
				tag = tag + "\"" + tags[i] + "\"";
			}
		}
		tag = tag + "]";
		return tag;
	}

}

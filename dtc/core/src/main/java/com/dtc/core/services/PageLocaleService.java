package com.dtc.core.services;

import org.apache.sling.commons.json.JSONObject;

public interface PageLocaleService {

	public JSONObject getLocaleFromPage(String currentPageVal);
	
}

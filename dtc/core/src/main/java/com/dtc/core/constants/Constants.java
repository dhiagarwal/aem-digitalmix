package com.dtc.core.constants;

/**
 * The class contains common application constants.
 */
public interface Constants {
	/**
	 * The default path.
	 */
	public static final String DEFAULT_PATH = "/";

	/**
	 * The Cookie max age.
	 */
	public static final int COOKIE_MAX_AGE = 5184000;

	/**
	 * Login Cookie Name
	 */
	public static final String COOKIE_NAME = "dtclogin";

	/**
	 * Hybris endpoint protocol
	 */
	public static final String HYBRIS_ENDPOINT_PROTOCOL = "http://";

	/**
	 * Hybris endpoint server
	 */
	public static final String HYBRIS_ENDPOINT_SERVER = "10.118.44.206:";

	/**
	 * Hybris endpoint port
	 */
	public static final String HYBRIS_ENDPOINT_PORT = "9001";

	/**
	 * Hybris endpoint domain
	 */
	public static final String HYBRIS_ENDPOINT_DOMAIN = "/meccommercewebservices/v2/cosmetics";

	/**
	 * Hybris endpoint for signup service
	 */
	public static final String HYBRIS_ENDPOINT_SIGNUP_SERVICE = "/users/anonymous/signUp";


	/**
	 * Hybris endpoint for signin service
	 */
	public static final String HYBRIS_ENDPOINT_SIGNIN_SERVICE = "/users/anonymous/signIn";
}

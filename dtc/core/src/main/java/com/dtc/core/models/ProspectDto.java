package com.dtc.core.models;

public class ProspectDto {

	String sponsorId;
	String gigyaId;
	String country;
	String language;
	String userName;
	String email;
	String phone;
	String firstName;
	String lastName;
	String accountNumber;
	String userRole;
	String profilePictureUrl;
	String profileThumbnailUrl;
	boolean preferredCustomer;
	String inviterId;
	boolean alphaAccountEnabled;
	String emailVerifiedSubject;
	String emailVerifiedContent;
	String userPreferences;

	public String getSponsorId() {
		return sponsorId;
	}

	public void setSponsorId(String sponsorId) {
		this.sponsorId = sponsorId;
	}

	public String getGigyaId() {
		return gigyaId;
	}

	public void setGigyaId(String gigyaId) {
		this.gigyaId = gigyaId;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	public String getProfilePictureUrl() {
		return profilePictureUrl;
	}

	public void setProfilePictureUrl(String profilePictureUrl) {
		this.profilePictureUrl = profilePictureUrl;
	}

	public String getProfileThumbnailUrl() {
		return profileThumbnailUrl;
	}

	public void setProfileThumbnailUrl(String profileThumbnailUrl) {
		this.profileThumbnailUrl = profileThumbnailUrl;
	}

	public boolean isPreferredCustomer() {
		return preferredCustomer;
	}

	public void setPreferredCustomer(boolean preferredCustomer) {
		this.preferredCustomer = preferredCustomer;
	}

	public String getInviterId() {
		return inviterId;
	}

	public void setInviterId(String inviterId) {
		this.inviterId = inviterId;
	}

	public boolean isAlphaAccountEnabled() {
		return alphaAccountEnabled;
	}

	public void setAlphaAccountEnabled(boolean alphaAccountEnabled) {
		this.alphaAccountEnabled = alphaAccountEnabled;
	}

	public String getEmailVerifiedSubject() {
		return emailVerifiedSubject;
	}

	public void setEmailVerifiedSubject(String emailVerifiedSubject) {
		this.emailVerifiedSubject = emailVerifiedSubject;
	}

	public String getEmailVerifiedContent() {
		return emailVerifiedContent;
	}

	public void setEmailVerifiedContent(String emailVerifiedContent) {
		this.emailVerifiedContent = emailVerifiedContent;
	}

	public String getUserPreferences() {
		return userPreferences;
	}

	public void setUserPreferences(String userPreferences) {
		this.userPreferences = userPreferences;
	}

}

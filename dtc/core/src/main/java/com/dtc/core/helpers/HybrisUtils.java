package com.dtc.core.helpers;

import com.dtc.core.constants.Constants;
import com.dtc.core.models.HybrisUserModel;
import com.dtc.core.models.ProspectDto;
import com.dtc.core.models.UserModel;

public class HybrisUtils {

	public static String getEndPointForSignUp() {
		StringBuilder hybrisEndpoint = new StringBuilder();
		hybrisEndpoint.append(Constants.HYBRIS_ENDPOINT_PROTOCOL);
		hybrisEndpoint.append(Constants.HYBRIS_ENDPOINT_SERVER);
		hybrisEndpoint.append(Constants.HYBRIS_ENDPOINT_PORT);
		hybrisEndpoint.append(Constants.HYBRIS_ENDPOINT_DOMAIN);
		hybrisEndpoint.append(Constants.HYBRIS_ENDPOINT_SIGNUP_SERVICE);
		return hybrisEndpoint.toString();
	}
	
	public static String getEndPointForSignIn() {
		StringBuilder hybrisEndpoint = new StringBuilder();
		hybrisEndpoint.append(Constants.HYBRIS_ENDPOINT_PROTOCOL);
		hybrisEndpoint.append(Constants.HYBRIS_ENDPOINT_SERVER);
		hybrisEndpoint.append(Constants.HYBRIS_ENDPOINT_PORT);
		hybrisEndpoint.append(Constants.HYBRIS_ENDPOINT_DOMAIN);
		hybrisEndpoint.append(Constants.HYBRIS_ENDPOINT_SIGNIN_SERVICE);
		return hybrisEndpoint.toString();
	}
	
	public static HybrisUserModel convertUserModelToHybrisModel(UserModel userModel){
		HybrisUserModel hybrisUserModel = new HybrisUserModel();
		ProspectDto prospectDto = new ProspectDto();
		prospectDto.setEmail(userModel.getEmail());
		prospectDto.setFirstName(userModel.getGivenName());
		prospectDto.setLastName(userModel.getFamilyName());
		prospectDto.setLanguage("EN");
		prospectDto.setCountry("US");
		prospectDto.setUserPreferences("EMAIL");
		prospectDto.setUserName(userModel.getIdentifier());
		prospectDto.setGigyaId(userModel.getEmail());
		hybrisUserModel.setProspectDto(prospectDto);
		return hybrisUserModel;
	}

}

package com.dtc.core.utils;

import org.apache.sling.commons.json.JSONArray;

/**
 * Created by Deloitte Digital.
 */
public class JSONObjectUtil {
    //---------------------------------------------------------------------------
    //
    //                           Public Methods
    //
    //---------------------------------------------------------------------------

    /**
     * Convert a array of strings into a JSONArray.
     * @param array
     * @return
     */
    public static JSONArray toJSONArray(String[] array){
        JSONArray jsonArray = new JSONArray();

        for (int i=0;i< array.length;i++){
            jsonArray.put(array[i]);
        }

        return jsonArray;
    }

    /**
     * Convert a JSONArray of Strings into a String array.
     * @param jsonArray
     * @return
     */
    public static String[] toStringArray(JSONArray jsonArray) {
        String[] stringArray = null;
        int length = jsonArray.length();

        if(jsonArray!=null){
            stringArray = new String[length];
            for(int i=0;i<length;i++){
                stringArray[i]= jsonArray.optString(i);
            }
        }

        if (stringArray == null){
            stringArray = new String[0];
        }

        return stringArray;
    }
}

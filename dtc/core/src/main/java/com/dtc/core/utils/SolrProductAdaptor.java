package com.dtc.core.utils;

import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;

/**
 * Created by Deloitte Digital.
 */
public class SolrProductAdaptor {
    private JSONObject solrProduct;
    private JSONArray languageAndMarkets;

    //---------------------------------------------------------------------------
    //
    //                            Constructors
    //
    //---------------------------------------------------------------------------
    public SolrProductAdaptor(JSONObject solrProduct) throws JSONException {
        this.solrProduct = solrProduct;
        this.languageAndMarkets = solrProduct.getJSONArray(SolrUtils.CHILD_DOCS);
    }

    //---------------------------------------------------------------------------
    //
    //                            Public Methods
    //
    //---------------------------------------------------------------------------
    public String getIndex() throws JSONException {
        return solrProduct.getString(SolrUtils.PATH_EXACT);
    }

    public String getSku() throws JSONException {
        return solrProduct.getJSONArray(SolrUtils.SKU).getString(0);
    }

    public String[] getMarkets() throws JSONException {
        return JSONObjectUtil.toStringArray(solrProduct.getJSONArray(SolrUtils.MARKETS));
    }

    public String[] getLanguages() throws JSONException {
        return JSONObjectUtil.toStringArray(solrProduct.getJSONArray(SolrUtils.LANGUAGES));
    }

    public String getVersion() throws JSONException {
        return solrProduct.getString(SolrUtils.VERSION);
    }

    public SolrProductLanguageAdaptor getLanguageChild(String lang) throws JSONException {
        SolrProductLanguageAdaptor languageAdaptor = null;
        JSONObject languageObject;

        for (int i=0;i<languageAndMarkets.length();i++){
            languageObject = languageAndMarkets.getJSONObject(i);
            if (languageObject.getString(SolrUtils.PATH_EXACT).equals(SolrUtils.PRODUCT_INDEX_PREFIX + getSku() + "/" + lang.toLowerCase())){
                languageAdaptor = new SolrProductLanguageAdaptor(languageObject);
                break;
            }
        }

        return languageAdaptor;
    }
    public SolrProductMarketAdaptor getMarketChild(String market) throws JSONException {
        SolrProductMarketAdaptor marketAdaptor = null;
        JSONObject marketObject;

        for (int i=0;i<languageAndMarkets.length();i++){
            marketObject = languageAndMarkets.getJSONObject(i);
            if (marketObject.getString(SolrUtils.PATH_EXACT).equals(SolrUtils.PRODUCT_INDEX_PREFIX + getSku() + "/" + market.toUpperCase())){
                marketAdaptor = new SolrProductMarketAdaptor(marketObject);
                break;
            }
        }

        return marketAdaptor;
    }

    public class SolrProductLanguageAdaptor{
        private JSONObject productLanguage;

        //---------------------------------------------------------------------------
        //
        //                            Constructors
        //
        //---------------------------------------------------------------------------
        public SolrProductLanguageAdaptor(JSONObject productLanguage){
            this.productLanguage = productLanguage;
        }

        //---------------------------------------------------------------------------
        //
        //                            Public Methods
        //
        //---------------------------------------------------------------------------
        public String getIndex() throws JSONException {
            return productLanguage.getString(SolrUtils.PATH_EXACT);
        }

        public String getSku() throws JSONException {
            return productLanguage.getJSONArray(SolrUtils.SKU).getString(0);
        }

        public String[] getMarkets() throws JSONException {
            return JSONObjectUtil.toStringArray(productLanguage.getJSONArray(SolrUtils.MARKETS));
        }

        public String getTitle() throws JSONException {
            return productLanguage.getJSONArray(SolrUtils.TITLE).getString(0);
        }

        public String getDescription() throws JSONException {
            return productLanguage.getJSONArray(SolrUtils.DESCRIPTION).getString(0);
        }

        public String[] getIngredients() throws JSONException {
            return JSONObjectUtil.toStringArray(productLanguage.getJSONArray(SolrUtils.KEY_INGREDIENTS));
        }

        public String[] getCategories() throws JSONException {
            return JSONObjectUtil.toStringArray(productLanguage.getJSONArray(SolrUtils.CATEGORIES));
        }

        public String[] getSearchTerms() throws JSONException {
            return JSONObjectUtil.toStringArray(productLanguage.getJSONArray(SolrUtils.SEARCH_TERMS));
        }
    }

    public class SolrProductMarketAdaptor{
        private JSONObject productMarket;

        //---------------------------------------------------------------------------
        //
        //                            Constructors
        //
        //---------------------------------------------------------------------------
        public SolrProductMarketAdaptor(JSONObject productMarket){
            this.productMarket = productMarket;
        }

        //---------------------------------------------------------------------------
        //
        //                            Public Methods
        //
        //---------------------------------------------------------------------------
        public String getIndex() throws JSONException {
            return productMarket.getString(SolrUtils.PATH_EXACT);
        }

        public String getSku() throws JSONException {
            return productMarket.getJSONArray(SolrUtils.SKU).getString(0);
        }

        public boolean getPurchasable() throws JSONException {
            return Boolean.parseBoolean(productMarket.getJSONArray(SolrUtils.PURCHASABLE).getString(0));
        }

        public String getImageUrl() throws JSONException {
            return productMarket.getJSONArray(SolrUtils.IMAGE_URL).getString(0);
        }
    }
}

package com.dtc.core.servlets;

import java.io.IOException;

import javax.jcr.Session;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dtc.core.services.PageLocaleService;

@Service(PageLocaleServlet.class)
@SlingServlet(paths = { "/bin/dtc/pageLocaleValue" }, generateComponent = false, methods = "GET")
@Component(label = "Page Locale Value", description = "Page Locale Value", enabled = true, immediate = true, metatype = false)
public class PageLocaleServlet extends SlingAllMethodsServlet {
	protected static final Logger LOGGER = LoggerFactory.getLogger(PageLocaleServlet.class);
	
	@Reference
	private PageLocaleService pageLocaleService;
	
	@Override
	protected void doGet(final SlingHttpServletRequest req, final SlingHttpServletResponse resp)
			throws ServletException, IOException {
			try {
				
				JSONObject pageLocaleVal= new JSONObject();
				String currentPageVal="";
				if (req.getParameter("resourcePath")!=null) {
					currentPageVal = req.getParameter("resourcePath");
		        }
				pageLocaleVal=pageLocaleService.getLocaleFromPage(currentPageVal);
				resp.setContentType("application/json");
				pageLocaleVal.write(resp.getWriter());
				
			}  catch (JSONException e) {
				LOGGER.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
			}	
		
	}
}

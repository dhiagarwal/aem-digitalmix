package com.dtc.core.schedulers;

import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Deloitte Digital.
 */
public class SolrProductsJSONObject {
    private static final Logger log = LoggerFactory.getLogger(SolrProductsJSONObject.class);
    private static JSONArray productsToUpdate = null;

    //-------------------------------------------------------------------------------
    //
    //                         Public Add Node Methods
    //
    //-------------------------------------------------------------------------------
    public static synchronized void addProduct(JSONObject product) {
        if(productsToUpdate == null) {
            productsToUpdate = new JSONArray();
        }
        productsToUpdate.put(product);
    }

    // Get the nodesToAddJSON object for update.
    public static synchronized JSONArray getProducts() {
        JSONArray productsCopy = productsToUpdate;

        // Clear the updates.
        productsToUpdate = null;

        return productsCopy;
    }
}

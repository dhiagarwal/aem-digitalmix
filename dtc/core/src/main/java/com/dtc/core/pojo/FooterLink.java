
package com.dtc.core.pojo;

public class FooterLink {

	private String linkLabel;
	private String linkUrl;

	/**
	 *
	 * @return The linkLabel
	 */
	public String getLinkLabel() {
		return linkLabel;
	}

	/**
	 *
	 * @param linkLabel
	 *            The linkLabel
	 */
	public void setLinkLabel(String linkLabel) {
		this.linkLabel = linkLabel;
	}

	/**
	 *
	 * @return The linkUrl
	 */
	public String getLinkUrl() {
		return linkUrl;
	}

	/**
	 *
	 * @param linkUrl
	 *            The linkUrl
	 */
	public void setLinkUrl(String linkUrl) {
		this.linkUrl = linkUrl;
	}

}

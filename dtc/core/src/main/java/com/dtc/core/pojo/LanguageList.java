package com.dtc.core.pojo;

public class LanguageList {

	private String langName;
	private String langCode;

	/**
	 *
	 * @return langName
	 */
	public String getLangName() {
		return langName;
	}

	/**
	 *
	 * @param langName
	 *            The langName
	 */
	public void setLangName(String langName) {
		this.langName = langName;
	}

	/**
	 *
	 * @return langCode
	 */
	public String getLangCode() {
		return langCode;
	}

	/**
	 *
	 * @param langCode
	 *            The langCode
	 */
	public void setLangCode(String langCode) {
		this.langCode = langCode;
	}

}

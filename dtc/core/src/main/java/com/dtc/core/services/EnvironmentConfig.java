package com.dtc.core.services;

public interface EnvironmentConfig {
		
	String getSprinklrAPIKey();
	
	String getSprinklrToken();
	
	String getSprinklrUserCreateClientId();
	
	String getSprinklrUserCreateEndPoint();
	
	String getSprinklrUploadEndPoint();
	
	String getSprinklrBaseURL();
	
	String getSprinklrCreateEndPoint();
	
	String getSprinklrAppAccessEndPoint();
	
	String getSprinklrDAMUploadEndPoint();
	
	String getSprinklrTokenEndPoint();	
	
	String getMulesoftHybrisEndpoint();
	
	String getMulesoftHybrisGuestEndpoint();
	
	String getMulesoftSolrEndpoint();
	
	String getMulesoftStorefrontEndpoint();
	
	String getMulesoftStoreMECfrontEndpoint();
	
	String getMulesoftTokenEndpoint();
	
	String getMulesoftClientId();
	
	String getMulesoftClientSecret();
	
	String getS3BucketURL();
	
	String getAtlas_service_url();

	String getShortenServiceUrl();

	String getShortenServiceKey();
	
	String getQas_service_url();
	
	String getAnalytics_js_url();
	
	String getGoogleApiKey();

}

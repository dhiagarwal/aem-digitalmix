package com.dtc.core.workflow;

import com.day.cq.replication.Replicator;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.workflow.WorkflowException;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.WorkItem;
import com.day.cq.workflow.exec.WorkflowData;
import com.day.cq.workflow.exec.WorkflowProcess;
import com.day.cq.workflow.metadata.MetaDataMap;
import com.day.cq.workflow.model.WorkflowModel;
import org.apache.felix.scr.annotations.*;
import org.apache.felix.scr.annotations.Properties;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Value;
import java.io.IOException;
import java.util.*;


@Component(
        label = "DTC GlobalLink Create Submission",
        description = "DTC GlobalLink Workflow Process implementation"
)
@Properties({
        @Property(
                name = Constants.SERVICE_DESCRIPTION,
                value = "DTC GlobalLink Create Submission",
                propertyPrivate = true
        ),
        @Property(
                label = "DTC GlobalLink Create Submission",
                name = "process.label",
                value = "DTC GlobalLink Create Submission",
                description = "Label which will appear in the AEM Workflow interface; This should be unique across "
                        + "Workflow Processes",
                propertyPrivate = true
        )
})
@Service
public class GlobalLinkPMD implements WorkflowProcess {
    private static final Logger logger = LoggerFactory.getLogger(GlobalLinkPMD.class);

    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    @Reference
    private Replicator replicator;


    //---------------------------------------------------------------------------
    //
    //                            Public Methods
    //
    //---------------------------------------------------------------------------
    public void execute(WorkItem workItem, WorkflowSession workflowSession, MetaDataMap args) {
        try {
            Node sourceNode = getSourceNode(workflowSession, workItem);

            if (sourceNode != null) {
                String processArg = args.get("PROCESS_ARGS", "Single"); // Default to single

                boolean allowLocaleLanguageNodes = false;

                if (processArg.equals("Recursive")) {
                    WorkflowUtils workflowUtils = new WorkflowUtils();
                    ArrayList<Node> sourceNodes = workflowUtils.recursiveNodeSearch(sourceNode);

                    // Make sure we include the current node.
                    sourceNodes.add(sourceNode);

                    Iterator<Node> sourceNodeItr = sourceNodes.iterator();

                    while (sourceNodeItr.hasNext()){
                        processProductDetailPage(workItem, workflowSession, sourceNodeItr.next(), allowLocaleLanguageNodes);
                    }
                } else {
                    processProductDetailPage(workItem, workflowSession, sourceNode, allowLocaleLanguageNodes);
                }
            }
        } catch (Exception e) {
            logger.error("Error executing TDC workflow step", e);
        }
    }

    //---------------------------------------------------------------------------
    //
    //                            Private Methods
    //
    //---------------------------------------------------------------------------
    /**
     * This method is the process step that takes the strings that were translated in the Product Detail Page or PMD,
     * and sends the translations to TDC for translation. BTW, if changes were made in the Product Detail Page they will
     * be writen to the PMD and then the PMD will be activated.
     * @param sourceNode
     * @throws RepositoryException
     * @throws IOException
     */
    private void processProductDetailPage(WorkItem workItem, WorkflowSession workflowSession, Node sourceNode, boolean allowLocaleLanguageNodes) throws RepositoryException, IOException {
        WorkflowUtils.PageType pageType = WorkflowUtils.getPageType(sourceNode);

        // Detect a PMD translation request.
        if (pageType == WorkflowUtils.PageType.PRODUCT_MASTER_DATA) {
            // Send the the laguage node out for translation

            try {
                ResourceResolver resolver = resourceResolverFactory.getAdministrativeResourceResolver(null);
                final PageManager pageMgr = resolver.adaptTo(PageManager.class);

                // Get source and destination nodes.
                String srcLangPath = sourceNode.getPath();
                String destLangPath = workItem.getWorkflowData().getPayload().toString();

                // Get the markets for the PMD. This is used for Solr.
                String[] pmdMarkets = getPMDMarkets(srcLangPath, workflowSession);

                // Submit language for translation.
                String title = pageMgr.getPage(srcLangPath).getTitle();
//                submitLanguageForTranslation(title, srcLangPath, destLangPath, workflowSession);

                // Write records to Solr only on activate PMD.

//                Page original = null;
//                String sourcePMDPath = "";
//                String targetPMDPath = "";
//                String sku = "";

//                original = pageMgr.getPage(srcLangPath);

//                if (original != null) {
//                    // For translation the PMD is created automactically with the source language but not the target
//                    // language. We need to do the following:
//                    //
//                    // 1. Copy the PMD source language to the PMD destination language
//                    // 2. Submit the PMD distination language for translation.
//                    // 3. Write the PMD to SOLR.
//
//                    // When the PMD source language is translated we need to write the translation to SOLR. This
//                    // would be a separate process step.
//
//                    String title = original.getTitle();
//
//                    if (title == null) {
//                        title = original.getName();
//                    }
//
//                    Page dstPage = pageMgr.getPage(targetPMDPath);
//                    if (dstPage == null) {
//                        logger.info("translation target {} does not exist, creating copy.", targetPMDPath);
//
//                        // Copy original page to new destination.
//                        // (page, destination, before name (the name of the next page), shallow = false, resolveConflict = true, autoSave = true)
//                        pageMgr.copy(original, targetPMDPath, sourcePMDPath.substring(1, sourcePMDPath.length()), false, true, true);
//
//                        // Set the jcr title of the target.
//                        String targetJcrContentPath = targetPMDPath + "/jcr:content";
//                        Node targetNode = (Node) workflowSession.getSession().getItem(targetJcrContentPath);
//
//                        if (targetNode != null) {
//                            targetNode.setProperty("jcr:title", toLocaleLang);
//                            workflowSession.getSession().save();
//                        }
//
//                        // We need to activate this page to create it's full structure.
//                        replicator.replicate(workflowSession.getSession(), ReplicationActionType.ACTIVATE, targetPMDPath);
//                    } else {
//                        // Delete the "legacyImport" node from the longdescription if it exists from the PMD.
//                        // This is a leftover node from the legacy custom translation code.
//                        Node targetNode = (Node) workflowSession.getSession().getItem(targetPMDPath);
//
//                        if (targetNode.hasNode("jcr:content/longDescription/legacyImport")) {
//                            Node legacyImportNode = targetNode.getNode("jcr:content/longDescription/legacyImport");
//                            legacyImportNode.remove();
//                        }
//                    }
//
//                    // This is the path to the TDC (global link adaptor)
//                    String modelPath = "/etc/workflow/models/globallink-adaptor/globallink-translation-single/jcr:content/model";
//
//                    // This code will take the source PMD and kick off a translation for it. Once the translation is done
//                    // It should be written to the target PMD with the translated strings and then it is activated. Once activated
//                    // a launcher should be kicked off from a pub service which writes the translated PMD to SOLR.
//                    WorkflowModel wfModel = modelPath == null ? null : workflowSession.getModel(modelPath);
//                    if (wfModel != null) {
//                        WorkflowData data = workflowSession.newWorkflowData("JCR_PATH", targetPMDPath);
//                        Map<String, Object> metaData = new HashMap<>();
//                        metaData.put("srcPath", sourcePMDPath);
//                        metaData.put("startComment", "Translate: " + title);
//                        try {
//                            // Kick off the TDC workflow to translate the source PMD node.
//                            workflowSession.startWorkflow(wfModel, data, metaData);
//                        } catch (WorkflowException e) {
//                            logger.error("Unable to start workflow.", e);
//                        }
//                    }
//
//                    //Write the product data to SOLR. This data is a combination of the Product Detail Page and the PMD.
//                    SolrUtils.writeProductData(sku, sourcePMDPath, workflowSession.getSession(), fromLocale);
//                }
            }
            catch (Exception e){
                logger.error("Error during operation.", e);
            }
        }
    }

    private Node getSourceNode(WorkflowSession workflowSession, WorkItem workItem) {
        logger.info("Trying to get source node...");
        Node node = null;
        String srcPath = null;
        try {
            Dictionary<String, String> dict = workItem.getWorkflow().getMetaData();
            srcPath = dict.get("srcPath");
            logger.info("Getting node from workflow metadata srcPath={}...", srcPath);
            node = (Node) workflowSession.getSession().getItem(srcPath);
            if (node.getPrimaryNodeType().getName().equals("cq:Page")) {
                logger.info("Source page path is " + node.getPath());
            } else {
                logger.warn("Source node is not a page. Returning null!");
                node = null;
            }
        } catch (RepositoryException e) {
            logger.error("error getting node with path " + srcPath, e);
        }
        return node;
    }

    private String[] getPMDMarkets(String srcLangPath, WorkflowSession workflowSession) throws RepositoryException {
        String[] markets = null;

        String srcJcrContentPath = srcLangPath + "/jcr:content/productContainer";
        Node srcContentNode = (Node) workflowSession.getSession().getItem(srcJcrContentPath);
        javax.jcr.Property pmdMarkets = srcContentNode.getProperty("markets");

        if (pmdMarkets != null){
            Value[] values = pmdMarkets.getValues();
            markets = new String[values.length];

            for (int i=0;i<values.length;i++){
                markets[i] = values[i].getString();
            }
        }

        if (markets == null){
            markets = new String[0];
        }

        return markets;
    }

    private void submitLanguageForTranslation(String title, String srcLangPath, String destLangPath, WorkflowSession workflowSession) throws WorkflowException{
        // This is the path to the TDC (global link adaptor)
        String modelPath = "/etc/workflow/models/globallink-adaptor/globallink-translation-single/jcr:content/model";

        // This code will take the source PMD and kick off a translation for it. Once the translation is done
        // It should be written to the target PMD with the translated strings and then it is activated. Once activated
        // a launcher should be kicked off from a pub service which writes the translated PMD to SOLR.
        WorkflowModel wfModel = modelPath == null ? null : workflowSession.getModel(modelPath);
        if (wfModel != null) {
            WorkflowData data = workflowSession.newWorkflowData("JCR_PATH", destLangPath);
            Map<String, Object> metaData = new HashMap<>();
            metaData.put("srcPath", srcLangPath);
            metaData.put("startComment", "Translate: " + title);
            try {
                // Kick off the TDC workflow to translate the source PMD node.
                workflowSession.startWorkflow(wfModel, data, metaData);
            } catch (WorkflowException e) {
                logger.error("Unable to start workflow.", e);
            }
        }
    }
}

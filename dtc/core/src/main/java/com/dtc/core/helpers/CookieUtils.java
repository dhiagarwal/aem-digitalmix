package com.dtc.core.helpers;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.dtc.core.constants.Constants;



/**
 * Utility class for cookie handling.
 */
public final class CookieUtils
{
    /**
     * Add a cookie (based on a name and value) to the response object.
     *
     * @param response The Sling HTTP response.
     * @param name The name of the cookie.
     * @param value The cookie value.
     */
    public static void addCookie(final HttpServletResponse response, final String name, final String value)
    {
        addCookie(response, name, value, Constants.DEFAULT_PATH, Constants.COOKIE_MAX_AGE);
    }

    /**
     * Add a cookie (based on a name, value, path and maxAge) to the response object.
     *
     * @param response The Sling HTTP response.
     * @param name The name of the cookie.
     * @param value The cookie value.
     * @param path The cookie path.
     * @param maxAge The maximum age to keep the cookie (auto-expire).
     */
    public static void addCookie(final HttpServletResponse response, final String name, final String value,
            final String path, final int maxAge)
    {
        final Cookie cookie = createCookie(name, value, path, maxAge);

        response.addCookie(cookie);
    }

    /**
     * Create a cookie with a name, value, path and age. This will just create the cookie object and return it.
     * It will not be added to the response, if that's required, use the "addCookie" methods.
     *
     * @param name The name of the cookie.
     * @param value The cookie value.
     * @param path The cookie path.
     * @param maxAge The maximum age to keep the cookie (auto-expire).
     * @return The cookie.
     */
    public static Cookie createCookie(final String name, final String value, final String path, final int maxAge)
    {
        final Cookie cookie = new Cookie(name, value);
        cookie.setPath(path);
        cookie.setMaxAge(maxAge);

        return cookie;
    }
    
    /**
     * Clears cookie by resetting the cookie age and path to the default path and age equal to 0.
     * When age is '0', this means the cookie will be 'end of life'
     *
     * @param request The Sling HTTP request.
     * @param response The Sling HTTP response.
     * @param names The names of the Cookies to be cleared.
     */
    public static void clearCookies(final SlingHttpServletRequest request, final SlingHttpServletResponse response,
            final String... names)
    {
        if (names != null)
        {
            for (final String name : names)
            {
                final Cookie cookie = request.getCookie(name);
                if (cookie != null)
                {
                    cookie.setPath(Constants.DEFAULT_PATH);
                    cookie.setMaxAge(0);
                    response.addCookie(cookie);
                }
            }
        }
    }
    
    /**
     * Default Constructor.
     * 
     * This is private constructor to restrict instantiation.
     */
    private CookieUtils()
    {
       // Restrict instantiation
    }

}

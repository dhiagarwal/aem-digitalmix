package com.dtc.core.workflow;

import com.day.cq.replication.ReplicationException;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.WorkItem;
import com.day.cq.workflow.exec.WorkflowProcess;
import com.day.cq.workflow.metadata.MetaDataMap;
import com.dtc.core.schedulers.SolrProductsJSONObject;
import com.dtc.core.services.EnvironmentConfig;
import com.dtc.core.utils.SolrUtils;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.commons.json.JSONException;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.RepositoryException;
import java.io.IOException;

/**
 * Created by Deloitte Digital.
 */
@Component(
        label = "DTC Delete Solr Product",
        description = "DTC Delete Solr Product Workflow Process implementation"
)
@Properties({
        @Property(
                name = Constants.SERVICE_DESCRIPTION,
                value = "DTC Delete Solr Product",
                propertyPrivate = true
        ),
        @Property(
                label = "DTC Delete Solr Product",
                name = "process.label",
                value = "DTC Delete Solr Product",
                description = "Label which will appear in the AEM Workflow interface; This should be unique across "
                        + "Workflow Processes",
                propertyPrivate = true
        )
})
@Service
public class DeleteSolrProducts implements WorkflowProcess {
    private static final Logger logger = LoggerFactory.getLogger(GlobalLinkPMD.class);

    private static final String PMD_PATH_PREFIX = "/content/pmd";
    private static final String PIM_PATH_PREFIX = "/etc/commerce/products/dtc";

    private static final int PMD_PATH_COMPONENTS_QTY = 7;

    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    @Reference
    private EnvironmentConfig environmentConfig;

    //---------------------------------------------------------------------------
    //
    //                            Public Methods
    //
    //---------------------------------------------------------------------------
    public void execute(WorkItem workItem, WorkflowSession workflowSession, MetaDataMap args) {
        try {
            String sourceNodePath = getSourceNodePath(workItem);
            deleteSolrProduct(workItem, workflowSession, sourceNodePath);
        } catch (Exception e) {
            logger.error("Error executing Delete Solr Products workflow step", e);
        }
    }

    //---------------------------------------------------------------------------
    //
    //                            Private Methods
    //
    //---------------------------------------------------------------------------
    /**
     * Delete a solr product based on the sku.
     *
     * @param sourceNodePath
     * @throws RepositoryException
     * @throws IOException
     */
    private void deleteSolrProduct(WorkItem workItem, WorkflowSession workflowSession, String sourceNodePath) throws RepositoryException, IOException {
        try {
            // Check to see if it is a PMD path
            // Example: /content/pmd/01/00/36/01003610/en.html
            if (sourceNodePath.startsWith(PMD_PATH_PREFIX)) {
                String[] pathComponents = sourceNodePath.split("/");

                // Handle deactivation from the node that has the full sku name.
                if (pathComponents.length == PMD_PATH_COMPONENTS_QTY){
                    String sku = pathComponents[PMD_PATH_COMPONENTS_QTY -1];
                    SolrUtils.deleteMatchingSkus(sku, environmentConfig);
                }

                // Handle language nodes that are being deactivated individually. These are batched up.
                else if (pathComponents.length > PMD_PATH_COMPONENTS_QTY) {
                    String sku = pathComponents[PMD_PATH_COMPONENTS_QTY -1];

                    // Write the product data to SOLR. This data is a combination of the Product Detail Page and the PMD.
                    SolrProductsJSONObject.addProduct(SolrUtils.createPMDDocument(sku, workflowSession.getSession()));
                }

                // Handle multiple sku de-activations from a tree - we have to form the sku matching pattern.
                else{
                    int INDEX_OF_SKU_BEGINING = 3;
                    int SKU_LENGTH = 8;

                    String skuMatchingPattern = "";
                    int i = 0;

                    while (skuMatchingPattern.length() < SKU_LENGTH){
                        if (INDEX_OF_SKU_BEGINING + i < pathComponents.length){
                            skuMatchingPattern += pathComponents[INDEX_OF_SKU_BEGINING + i];
                        }
                        else {
                            skuMatchingPattern += "?";
                        }
                        i++;
                    }

                    // Delete the sku matches from Solr. This is one wire hit that executes immediatly and removes all
                    // products from Solr that match the skuMatchingPattern.
                    SolrUtils.deleteMatchingSkus(skuMatchingPattern, environmentConfig);
                }
            }

            // Check to see if it is a PIM path
            // Example: /etc/commerce/products/dtc/US/01/00/36/10/01003610
            else if (sourceNodePath.startsWith(PIM_PATH_PREFIX)) {
                String[] pathComponents = sourceNodePath.split("/");

                if (pathComponents.length >= 10) {
                    String sku = pathComponents[9];
                    String market = pathComponents[4];

                    // Write the product data to SOLR. This data is a combination of the Product Detail Page and the PMD.
                    SolrProductsJSONObject.addProduct(SolrUtils.createPMDDocument(sku, workflowSession.getSession()));
                }
            }
        }
        catch(JSONException e){
            logger.error("Error in DeleteSolrProducts.deleteSolrProduct ", e);
        } catch (ReplicationException e) {
            logger.error("Error in DeleteSolrProducts.deleteSolrProduct ", e);
        }
    }

    private String getSourceNodePath(WorkItem workItem) {
        logger.info("Trying to get source path that has been removed");
        String srcPath = workItem.getWorkflowData().getPayload().toString();

        return srcPath.replace("jcr:content",""); // Remove jcr:content
    }
}
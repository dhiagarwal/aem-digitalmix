package com.dtc.core.servlets;

import java.io.IOException;

import javax.jcr.Credentials;
import javax.jcr.LoginException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.SimpleCredentials;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.jcr.api.SlingRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dtc.core.constants.Constants;
import com.dtc.core.helpers.CookieUtils;
import com.dtc.core.models.UserModel;
import com.dtc.core.services.EnvironmentConfig;
import com.dtc.core.services.UserManagementService;
import com.dtc.core.services.impl.CreateSprinklrAssetImpl;
import com.google.gson.Gson;
import org.json.simple.JSONObject;

@SlingServlet(paths = "/bin/signUpDtc")
public class SignUpServlet extends SlingSafeMethodsServlet {

	@Reference
	private SlingRepository repository;

	@Reference
	ResourceResolverFactory resourceResolverFactory;

	ResourceResolver resourceResolver;

	@Reference
	private UserManagementService userService;
	
	@Reference
	private transient EnvironmentConfig environmentConfig;

	private static final long serialVersionUID = 1L;

	private static final Logger log = LoggerFactory.getLogger(SignUpServlet.class);

	public void doGet(SlingHttpServletRequest servletRequest, SlingHttpServletResponse servletResponse)
			throws ServletException, IOException {
		signUp(servletRequest, servletResponse);
	}

	public void doPost(SlingHttpServletRequest servletRequest, SlingHttpServletResponse servletResponse)
			throws ServletException, IOException {
		signUp(servletRequest, servletResponse);
	}

	/**
	 * @param SlingHttpServletRequest
	 * @param SlingHttpServletResponse
	 */
	private void signUp(SlingHttpServletRequest servletRequest, SlingHttpServletResponse servletResponse) {
		try {
			UserModel userModel = new Gson().fromJson(servletRequest.getParameter("user"), UserModel.class);
			if (null != userService.createAccount(userModel)) {
						
				String accessTokenValue = "Bearer " + environmentConfig.getSprinklrToken();
				String key = environmentConfig.getSprinklrAPIKey();
				String sprinklrEndPoint = environmentConfig.getSprinklrCreateEndPoint();
				String sprinklrDAMUploadEndPoint = environmentConfig.getSprinklrDAMUploadEndPoint();
				String sprinklrUserCreateEndPoint = environmentConfig.getSprinklrUserCreateEndPoint();
				String sprinklrUserCreateClientId = environmentConfig.getSprinklrUserCreateClientId();
				String userName = userModel.getEmail();
				String familyName = (userModel.getFamilyName() != null && userModel.getFamilyName() != "") ? userModel.getFamilyName() : "";
				String givenName = (userModel.getGivenName() != null && userModel.getGivenName() != "") ? userModel.getGivenName() : "";
				
				CreateSprinklrAssetImpl sprinklrAssetObj = new CreateSprinklrAssetImpl(accessTokenValue, key,sprinklrEndPoint , sprinklrDAMUploadEndPoint);
				JSONObject response = sprinklrAssetObj.createUser(sprinklrUserCreateEndPoint, sprinklrUserCreateClientId, userName, familyName, givenName);
				
				if(response != null && (int)response.get("statusCode") != 200){
					log.error("User creation on Sprinklr unsuccessful. [Status Code - " + (int)response.get("statusCode") + ", Reason - " + (String)response.get("reason") + "]");
					userService.removeUser(userModel.getIdentifier());
					servletResponse.sendError(500, "Error Creating user in Sprinklr. Hence Signup Rejected. [Status Code - " + (int)response.get("statusCode") + ", Reason - " + (String)response.get("reason") + "]");
				} else {
					Object statusAccountCreationOnHybris = userService.createAccountOnHybris(userModel);
					if (null == statusAccountCreationOnHybris) {
						log.error("User creation on hybris unsuccessful.");
						userService.removeUser(userModel.getIdentifier());
						servletResponse.sendError(500, "Error Creating user in Hybris. Hence Signup Rejected.");
					} else {
						Credentials cd = new SimpleCredentials(userModel.getIdentifier(),
								userModel.getPassword().toCharArray());
						Session login = repository.login(cd);
						log.info("Login Successful");
						resourceResolver = resourceResolverFactory.getAdministrativeResourceResolver(null);
						Authorizable authorizable = null;
						authorizable = userService.findUser(userModel.getIdentifier());
						if (null != authorizable) {
							Resource resource = resourceResolver.getResource(authorizable.getPath() + "/profile");
							UserModel userInfo = resource.adaptTo(UserModel.class);
							servletRequest.getSession().invalidate();
							servletRequest.getSession().setAttribute("dtcLogin", true);
							servletResponse.addCookie(CookieUtils.createCookie(Constants.COOKIE_NAME,
									userModel.getIdentifier(), Constants.DEFAULT_PATH, Constants.COOKIE_MAX_AGE));
							servletResponse.getWriter().write(new Gson().toJson(statusAccountCreationOnHybris));
						}
					}
				}
			}
		} catch (LoginException e) {
			servletResponse.setStatus(servletResponse.SC_FORBIDDEN);
			log.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		} catch (RepositoryException e) {
			servletResponse.setStatus(servletResponse.SC_INTERNAL_SERVER_ERROR);
			log.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		} catch (Exception e) {
			servletResponse.setStatus(servletResponse.SC_INTERNAL_SERVER_ERROR);
			log.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}
	}
}

package com.dtc.core.services.impl;

import javax.jcr.Session;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.Language;
import com.day.cq.wcm.api.LanguageManager;
import com.dtc.core.services.PageLocaleService;

@Component(label = "Page Locale", metatype = true, description = "Service that retrieves Page Locale", immediate = true)
@Service(value = PageLocaleService.class)
public class PageLocaleServiceImpl implements PageLocaleService {
	protected static final Logger LOGGER = LoggerFactory.getLogger(PageLocaleServiceImpl.class);
	/** The service. */
	@Reference
	private ResourceResolverFactory resolverFactory;
	
	/** The ResourceResolver */
	private ResourceResolver resourceResolver;
	/** The service. */
	@Reference
	LanguageManager languageManager;

	/** The language. */
	private String language;
	
	/** The market code. */
	private String market = "";
	
	@Override
	public JSONObject getLocaleFromPage(String currentPageVal) {
		JSONObject pageLangAndMarket= new JSONObject();
		try {
			this.resourceResolver = this.resolverFactory.getAdministrativeResourceResolver(null);
			String marketVal="";
			Language languageResource = languageManager.getCqLanguage(resourceResolver.getResource(currentPageVal));
	        language=languageResource.getLanguageCode();
	        if(StringUtils.isEmpty(languageResource.getCountryCode())){
	        	String[] pagePath=currentPageVal.split("/");
	            for (int i = 0; i < pagePath.length; i++) {
	     		if(language.equals(pagePath[i])){
	     			marketVal=pagePath[i-1];
	     			break;
	     		}
	     	}
	            if(marketVal.length()==2){
	         	   market=marketVal;
	            }
	        }
	        else{
	        	market=languageResource.getCountryCode();
	        }
	        pageLangAndMarket.put("language", language);
			pageLangAndMarket.put("market", market);
	        
			
		} catch (LoginException ex) {
			LOGGER.error("Exception : " + ex);
			ex.printStackTrace();
		}
		catch (JSONException e) {
			LOGGER.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}
		 return pageLangAndMarket;
       
	}
	
	/**
	 * Gets the language.
	 *
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}

	/**
	 * Sets the language.
	 *
	 */
	public void setLanguage(String language) {
		this.language = language;
	}

	/**
	 * Gets the market.
	 *
	 * @return the market
	 */
	public String getMarket() {
		return market;
	}

	/**
	 * Sets the market.
	 *
	 */
	public void setMarket(String market) {
		this.market = market;
	}
	}

package com.dtc.core.workflow;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.WorkItem;
import com.day.cq.workflow.exec.WorkflowProcess;
import com.day.cq.workflow.metadata.MetaDataMap;
import com.dtc.core.schedulers.SolrProductsJSONObject;
import com.dtc.core.utils.SkuPathUtil;
import com.dtc.core.utils.SolrUtils;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import java.io.IOException;

/**
 * Created by Deloitte Digital.
 */
@Component(
        label = "DTC Update Solr Product",
        description = "DTC Update Solr Product Workflow Process implementation"
)
@Properties({
        @Property(
                name = Constants.SERVICE_DESCRIPTION,
                value = "DTC Update Solr Product",
                propertyPrivate = true
        ),
        @Property(
                label = "DTC Update Solr Product",
                name = "process.label",
                value = "DTC Update Solr Product",
                description = "Label which will appear in the AEM Workflow interface; This should be unique across "
                        + "Workflow Processes",
                propertyPrivate = true
        )
})
@Service
public class UpdateSolrProducts implements WorkflowProcess {
    private static final Logger logger = LoggerFactory.getLogger(GlobalLinkPMD.class);

    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    //---------------------------------------------------------------------------
    //
    //                            Public Methods
    //
    //---------------------------------------------------------------------------
    public void execute(WorkItem workItem, WorkflowSession workflowSession, MetaDataMap args) {
        try {
            Node sourceNode = getSourceLangNode(workflowSession, workItem);
            updateSolrProduct(workItem, workflowSession, sourceNode);
        } catch (Exception e) {
            logger.error("Error executing TDC workflow step", e);
        }
    }

    //---------------------------------------------------------------------------
    //
    //                            Private Methods
    //
    //---------------------------------------------------------------------------
    /**
     * This method is the process step that takes the strings that were translated, updates the PMD language node, and
     * then writes the Solr record.
     * @param sourceNode
     * @throws RepositoryException
     * @throws IOException
     */
    private void updateSolrProduct(WorkItem workItem, WorkflowSession workflowSession, Node sourceNode) throws RepositoryException, IOException {
        if (workItem != null && workflowSession != null && sourceNode != null) {
            WorkflowUtils.PageType pageType = WorkflowUtils.getPageType(sourceNode);

            if (pageType == WorkflowUtils.PageType.PRODUCT_MASTER_DATA) {
                try {
                    ResourceResolver resolver = resourceResolverFactory.getAdministrativeResourceResolver(null);
                    final PageManager pageMgr = resolver.adaptTo(PageManager.class);

                    String localeLang = getPMDLanguage(workItem);

                    String sourcePMDPath = sourceNode.getPath();
                    Page original = pageMgr.getPage(sourcePMDPath);

                    String sku = sourceNode.getParent().getName();

                    if (original == null) {
                        // This is the path to the PMD source language node.
                        // Example: /content/pmd/01/00/36/01003610/en.html
                        String skuPmdPath = SkuPathUtil.getPMDPathToSku(sku);
                        sourcePMDPath = skuPmdPath + localeLang;
                    }

                    original = pageMgr.getPage(sourcePMDPath);

                    if (original != null) {
                        // Write the product data to SOLR. This data is a combination of the Product Detail Page and the PMD.
                        SolrProductsJSONObject.addProduct(SolrUtils.createPMDDocument(sku, workflowSession.getSession()));
                    }
                } catch (Exception e) {
                    logger.error("Error in UpdateSolrProducts.updateSolrProduct for PMD ", e);
                }
            }

            // If it is a PIM node, the sling:resourceType = "commerce/components/product"
            else if (sourceNode.hasProperty("sling:resourceType") &&
                    sourceNode.getProperty("sling:resourceType").getString().equals("commerce/components/product")) {
                // If there is a PMD node for this PIM record we need to update Solr.
                try {
                    ResourceResolver resolver = resourceResolverFactory.getAdministrativeResourceResolver(null);
                    final PageManager pageMgr = resolver.adaptTo(PageManager.class);

                    String sku = sourceNode.getName();
                    String skuPmdPath = SkuPathUtil.getPMDPathToSku(sku);
                    Page pmd = pageMgr.getPage(skuPmdPath);

                    // If the PMD path exists we write the record to Solr
                    if (pmd != null) {
                        // Write the product data to SOLR. This data is a combination of the Product Detail Page and the PMD.
                        SolrProductsJSONObject.addProduct(SolrUtils.createPMDDocument(sku, workflowSession.getSession()));
                    }
                } catch (Exception e) {
                    logger.error("Error in UpdateSolrProducts.updateSolrProduct for PIM ", e);
                }
            }
        }
    }

    private String getPMDLanguage(WorkItem workItem) {
        String path = workItem.getWorkflowData().getPayload().toString();
        int indexOfJcrContent = path.indexOf("/jcr:content");

        if (indexOfJcrContent > -1){
            path = path.substring(0,indexOfJcrContent);
        }

        logger.info("toPath : {}", path);
        return path.substring(path.length()-2);
    }

    private Node getSourceLangNode(WorkflowSession workflowSession, WorkItem workItem) {
        logger.info("Trying to get source node...");
        Node node = null;
        String srcPath = workItem.getWorkflowData().getPayload().toString();
        int indexOfJcrContent = srcPath.indexOf("/jcr:content");

        if (indexOfJcrContent > -1){
            srcPath = srcPath.substring(0,indexOfJcrContent);
        }

        try {
            logger.info("Getting node from workflow metadata srcPath={}...", srcPath);
            node = (Node) workflowSession.getSession().getItem(srcPath);
            if (node.getPrimaryNodeType().getName().equals("cq:Page")) {
                logger.info("Source page path is " + node.getPath());
            } else if (node.hasProperty("sling:resourceType") && node.getProperty("sling:resourceType").getString().equals("commerce/components/product")) {
                logger.info("Source node path is " + node.getPath());
            } else {
                logger.warn("Source node is not a page. Returning null!");
                node = null;
            }
        } catch (RepositoryException e) {
            logger.error("error getting node with path " + srcPath, e);
        }

        return node;
    }
}

package com.dtc.core.servlets;

import java.io.IOException;

import javax.jcr.Credentials;
import javax.jcr.LoginException;
import javax.jcr.RepositoryException;
import javax.jcr.SimpleCredentials;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.jcr.api.SlingRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dtc.core.constants.Constants;
import com.dtc.core.helpers.CookieUtils;
import com.dtc.core.models.UserModel;
import com.dtc.core.services.UserManagementService;
import com.google.gson.Gson;

@SlingServlet(paths = "/bin/loginDtc")
public class LoginServlet extends SlingAllMethodsServlet {
	private static final long serialVersionUID = 1L;

	private static final Logger log = LoggerFactory.getLogger(LoginServlet.class);

	@Reference
	private SlingRepository repository;

	@Reference
	ResourceResolverFactory resourceResolverFactory;

	ResourceResolver resourceResolver;

	@Reference
	private UserManagementService userService;

	public void doPost(SlingHttpServletRequest servletRequest, SlingHttpServletResponse servletResponse)
			throws ServletException, IOException {
		login(servletRequest, servletResponse);
	}

	public void doGet(SlingHttpServletRequest servletRequest, SlingHttpServletResponse servletResponse)
			throws ServletException, IOException {
		login(servletRequest, servletResponse);
	}

	/**
	 * @param SlingHttpServletRequest
	 * @param SlingHttpServletResponse
	 */
	private void login(SlingHttpServletRequest servletRequest, SlingHttpServletResponse servletResponse) {
		try {
			UserModel userModel = new Gson().fromJson(servletRequest.getParameter("user"), UserModel.class);
			Credentials cd = new SimpleCredentials(userModel.getIdentifier(), userModel.getPassword().toCharArray());
			repository.login(cd);
			log.info("Login Successful");
			resourceResolver = resourceResolverFactory.getAdministrativeResourceResolver(null);
			Authorizable authorizable = null;
			authorizable = userService.findUser(userModel.getIdentifier());
			if (null != authorizable) {
				Resource resource = resourceResolver.getResource(authorizable.getPath() + "/profile");
				UserModel userInfo = resource.adaptTo(UserModel.class);
				servletRequest.getSession().invalidate();
				servletRequest.getSession().setAttribute("dtcLogin", true);
				servletResponse.addCookie(CookieUtils.createCookie(Constants.COOKIE_NAME, userModel.getIdentifier(),
						Constants.DEFAULT_PATH, Constants.COOKIE_MAX_AGE));
				servletResponse.getWriter()
						.write(new Gson().toJson(userService.findUserOnHybris(userInfo)));
			}
		} catch (LoginException e) {
			servletResponse.setStatus(servletResponse.SC_FORBIDDEN);
			log.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		} catch (RepositoryException e) {
			servletResponse.setStatus(servletResponse.SC_INTERNAL_SERVER_ERROR);
			log.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		} catch (Exception e) {
			servletResponse.setStatus(servletResponse.SC_INTERNAL_SERVER_ERROR);
			log.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}
	}
}

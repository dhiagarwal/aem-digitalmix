package com.dtc.core.services.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.jackrabbit.api.JackrabbitSession;
import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.Group;
import org.apache.jackrabbit.api.security.user.User;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.jcr.api.SlingRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dtc.core.helpers.HybrisUtils;
import com.dtc.core.models.UserModel;
import com.dtc.core.services.UserManagementService;
import com.google.gson.Gson;

@Component(metatype = false, immediate = true)
@Service
@Properties(value = { @Property(name = "service.vendor", value = "Deloitte"),
		@Property(name = "service.label", value = "UserManagement Service"),
		@Property(name = "service.description", value = "UserManagement Service for Streammix") })

public class UserManagementServiceImpl implements UserManagementService {

	@Reference
	private SlingRepository repository;

	private static final Logger log = LoggerFactory.getLogger(UserManagementServiceImpl.class);

	Session session;
	UserManager userManager;

	@Override
	public User createAccount(UserModel userDetails) throws RepositoryException {
		User userObj = null;

		userManager = getUserManager();
		Group group = null;
		if (null != findUser("DTCI")) {
			group = (Group) userManager.getAuthorizable("DTCI");
		} else {
			group = userManager.createGroup("DTCI");
		}
		userObj = userManager.createUser(userDetails.getIdentifier(), userDetails.getPassword());
		addDetailsToCrx(userObj, userDetails);
		group.addMember(userObj);
		session.save();
		return userObj;
	}

	@Override
	public User updateAccount(UserModel userDetails) {
		User userObj = null;
		try {
			userManager = getUserManager();
			userObj = (User) findUser(userDetails.getIdentifier());
			addDetailsToCrx(userObj, userDetails);
			session.save();
		} catch (Exception e) {
			log.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}
		return userObj;
	}

	@Override
	public Authorizable findUser(String userId) {
		Authorizable authorizable = null;
		try {
			userManager = getUserManager();
			authorizable = userManager.getAuthorizable(userId);
			if (authorizable != null) {
				log.info("user alredy exists" + authorizable.getPath());
				return authorizable;
			}
		} catch (Exception e) {
			log.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}
		return authorizable;
	}

	@Override
	public Authorizable removeUser(String userId) {
		Authorizable authorizable = null;
		try {
			userManager = getUserManager();
			authorizable = userManager.getAuthorizable(userId);
			if (authorizable != null) {
				log.info("Deleting user at - " + authorizable.getPath());
				authorizable.remove();
			}
		} catch (Exception e) {
			log.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}
		return authorizable;
	}

	/**
	 * @return
	 */
	private synchronized UserManager getUserManager() {
		if (userManager == null) {
			try {
				if (session == null) {
					session = repository.loginAdministrative(null);
				}
				if (session instanceof JackrabbitSession) {
					userManager = ((JackrabbitSession) session).getUserManager();
				}
			} catch (RepositoryException re) {
				log.error(re.getClass() + ": " + re.getMessage() + ": " + re.getCause(), re);
			}
		}
		return userManager;
	}

	private synchronized void addDetailsToCrx(User user, UserModel userDetails) {
		try {
			if (null != user) {
				setUserProperty(user, "email", userDetails.getEmail());
				// TODO: To be implemented once Data Structure for user is
				// confirmed.
				setUserProperty(user, "familyName", userDetails.getFamilyName());
				log.info("User Details Added");
			}
		} catch (Exception e) {
			log.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}
	}

	private void setUserProperty(User user, String propertyName, String propertyValue) {
		try {
			if (!user.hasProperty(propertyName)) {
				user.setProperty("profile/" + propertyName, session.getValueFactory().createValue(propertyValue));
			} else if (user.hasProperty(propertyName)
					&& !user.getProperty(propertyName)[0].getString().equals(propertyValue)) {
				user.setProperty("profile/" + propertyName, session.getValueFactory().createValue(propertyValue));
			}
		} catch (Exception e) {
			log.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}
	}

	private String getProperty(User userObj, String propertyName) {

		String propertyValue = "";
		try {
			if (userObj != null && userObj.hasProperty(propertyName)) {
				if (userObj.getProperty(propertyName)[0] != null) {
					propertyValue = userObj.getProperty(propertyName)[0].getString();
				}
			}
		} catch (Exception e) {
			log.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}

		return propertyValue;
	}

	@Override
	public Object createAccountOnHybris(UserModel userModel) throws RepositoryException {
		try {
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpPost postRequest = new HttpPost(HybrisUtils.getEndPointForSignUp());
			StringEntity input = new StringEntity(
					new Gson().toJson(HybrisUtils.convertUserModelToHybrisModel(userModel)));
			input.setContentType("application/json");
			postRequest.setEntity(input);

			HttpResponse httpResponse = httpClient.execute(postRequest);

			if (httpResponse.getStatusLine().getStatusCode() != 201) {
				throw new RuntimeException("Failed : HTTP error code : " + httpResponse.getStatusLine().getStatusCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((httpResponse.getEntity().getContent())));

			String output;
			StringBuilder response = new StringBuilder();
			while ((output = br.readLine()) != null) {
				log.error("*****" + output + "*****");
				response.append(output);
			}

			httpClient.getConnectionManager().shutdown();
			return response.toString();

		} catch (Exception e) {
			log.error("Error creating user on Hybris instance");
			return null;

		}
	}

	@Override
	public Object findUserOnHybris(UserModel userModel) throws RepositoryException {
		try {
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpPost postRequest = new HttpPost(HybrisUtils.getEndPointForSignIn());
			StringEntity input = new StringEntity("{\"gigyaId\":\""+userModel.getEmail()+"\"}");
			input.setContentType("application/json");
			postRequest.setEntity(input);

			HttpResponse httpResponse = httpClient.execute(postRequest);

			if (httpResponse.getStatusLine().getStatusCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + httpResponse.getStatusLine().getStatusCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((httpResponse.getEntity().getContent())));

			String output;
			StringBuilder response = new StringBuilder();
			while ((output = br.readLine()) != null) {
				log.error("*****" + output + "*****");
				response.append(output);
			}

			httpClient.getConnectionManager().shutdown();
			return response.toString();

		} catch (Exception e) {
			log.error("Error finding user on Hybris instance");
			return null;

		}
	}
}

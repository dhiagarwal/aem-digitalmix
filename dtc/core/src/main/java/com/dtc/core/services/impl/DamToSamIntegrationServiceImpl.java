package com.dtc.core.services.impl;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.Session;
import javax.jcr.Value;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dtc.core.constants.MetadataConstants;
import com.dtc.core.services.DamToSamIntegrationService;
import com.dtc.core.services.EnvironmentConfig;

@Component(label = "DAM to SAM Integration Service", metatype = true, description = "DAM to SAM Integration Service", immediate = true)
@Service(value = DamToSamIntegrationService.class)
public class DamToSamIntegrationServiceImpl implements DamToSamIntegrationService {
	
	private static final Logger logger = LoggerFactory.getLogger(DamToSamIntegrationServiceImpl.class);
	
	@Reference
	ResourceResolverFactory resourceResolverFactory;
	
	@Reference
	private transient EnvironmentConfig environmentConfig;
	
	@Override
	public boolean damToSamIntegrate(String assetPath, String replicationType) {
		logger.info(" SprinklrIntegrationProcess Execution Started");
		ResourceResolver resourceResolver = null;
		try{
			resourceResolver = resourceResolverFactory.getAdministrativeResourceResolver(null);		
			Session session = resourceResolver.adaptTo(Session.class);
			Node metadataNode = session.getNode(assetPath + MetadataConstants.JCRMETADATA);
			
			if(!metadataNode.hasProperty(MetadataConstants.DCSOCIAL)){
				logger.warn("social property is not set for the asset");
                return true;
			}else{
				String fileName = StringUtils.EMPTY;
    			String extension = StringUtils.EMPTY;
    			String format = StringUtils.EMPTY;
    			String contentType = StringUtils.EMPTY;
    			String createdBy = StringUtils.EMPTY;
    			String response_string = StringUtils.EMPTY;
    			String social = StringUtils.EMPTY;
    			String sprinklrAssetPath = StringUtils.EMPTY;
    			String assetName = StringUtils.EMPTY;
    			String assetDescription = StringUtils.EMPTY;
    			Date assetExpiry = DateUtils.addDays(new Date(), 30);
    			String[] productTags = null;
    			String[] sectorTags = null;
    			String[] industryTags = null;
                int statusCode = 0;
                boolean flag = false;
    			String sam_app = environmentConfig.getSprinklrAppAccessEndPoint();		
    			String accessTokenValue = "Bearer " + environmentConfig.getSprinklrToken();
    			String key = environmentConfig.getSprinklrAPIKey();
    			String sprinklrEndPoint = environmentConfig.getSprinklrCreateEndPoint();
    			String sprinklrDAMUploadEndPoint = environmentConfig.getSprinklrDAMUploadEndPoint();
    			logger.info("sam_app - "+sam_app);
    			logger.info("accessTokenValue - "+accessTokenValue);
    			logger.info("key - "+key);
    			logger.info("sprinklrEndPoint - "+sprinklrEndPoint);
    			logger.info("sprinklrDAMUploadEndPoint - "+sprinklrDAMUploadEndPoint);
    			
    			Node assetNode = session.getNode(assetPath);
    			fileName = assetNode.getName();  			
    			format = metadataNode.getProperty(MetadataConstants.DCFORMAT).getString();
    			
    			Node fileNode = session.getNode(assetPath + MetadataConstants.ORIGINALJCRPATH);
    			InputStream is = fileNode.getProperty(MetadataConstants.JCRDATA).getStream();
    			social = metadataNode.getProperty(MetadataConstants.DCSOCIAL).getString();
    			
    			if(metadataNode.hasProperty(MetadataConstants.SPRINKLR_ASSET_PATH)){
    				sprinklrAssetPath = metadataNode.getProperty(MetadataConstants.SPRINKLR_ASSET_PATH).getString();
    			}
    			if(metadataNode.hasProperty(MetadataConstants.DCTITLE)){
    				assetName = metadataNode.getProperty(MetadataConstants.DCTITLE).getString();
    			}
    			if(metadataNode.hasProperty(MetadataConstants.DCDESCRIPTION)){
    				assetDescription = metadataNode.getProperty(MetadataConstants.DCDESCRIPTION).getString();
    			}
    			if(metadataNode.hasProperty(MetadataConstants.EXPIREDATE)){
    				Calendar cal = metadataNode.getProperty(MetadataConstants.EXPIREDATE).getDate();
    				assetExpiry = cal.getTime();
    			}
    			if(metadataNode.hasProperty(MetadataConstants.DCPRODUCT)){
    				List<String> productList = new ArrayList<String>();
    				if(metadataNode.getProperty(MetadataConstants.DCPRODUCT).isMultiple()){
    					Value[] values = metadataNode.getProperty(MetadataConstants.DCPRODUCT).getValues();
        				for(Value v : values){
        					logger.info("product value - "+v.getString());
        					productList.add(v.getString());
        				}
    				}else{
    					productList.add(metadataNode.getProperty(MetadataConstants.DCPRODUCT).getString());
    				}   				
    				Object[] objTags = productList.toArray();
					productTags = Arrays.copyOf(objTags, objTags.length, String[].class);
    			}
    			if(metadataNode.hasProperty(MetadataConstants.DCINDUSTRY)){
    				List<String> industryList = new ArrayList<String>();
    				if(metadataNode.getProperty(MetadataConstants.DCINDUSTRY).isMultiple()){
    					Value[] values = metadataNode.getProperty(MetadataConstants.DCINDUSTRY).getValues();
        				for(Value v : values){
        					logger.info("industry value - "+v.getString());
        					industryList.add(v.getString());
        				}
    				}else{
    					industryList.add(metadataNode.getProperty(MetadataConstants.DCINDUSTRY).getString());
    				}
    				Object[] objTags = industryList.toArray();
					industryTags = Arrays.copyOf(objTags, objTags.length, String[].class);
    			}
    			if(metadataNode.hasProperty(MetadataConstants.DCSECTOR)){
    				List<String> sectorList = new ArrayList<String>();
    				if(metadataNode.getProperty(MetadataConstants.DCSECTOR).isMultiple()){
    					Value[] values = metadataNode.getProperty(MetadataConstants.DCSECTOR).getValues();
        				for(Value v : values){
        					logger.info("sector value - "+v.getString());
        					sectorList.add(v.getString());
        				}
    				}else{
    					sectorList.add(metadataNode.getProperty(MetadataConstants.DCSECTOR).getString());
    				}
    				Object[] objTags = sectorList.toArray();
					sectorTags = Arrays.copyOf(objTags, objTags.length, String[].class);
    			}
    			logger.info("sprinklrAssetPath - "+sprinklrAssetPath);
    			
    			if(StringUtils.isNotEmpty(format) && StringUtils.isNotEmpty(fileName) && fileName.contains(".")){
  			  		if(format.toLowerCase().contains(MetadataConstants.IMAGE)){
  			  			extension = MetadataConstants.PHOTOCAP;
  			  			contentType = MetadataConstants.IMAGECAP;
  			  			flag = true;
  			  			logger.info("flag set to true as image");
  			  		}
  			  		else if(format.toLowerCase().contains(MetadataConstants.VIDEO)) {
  			  			extension = MetadataConstants.VIDEOCAP;
  			  			contentType = MetadataConstants.VIDEOCAP;
  			  			flag = true;
  			  			logger.info("flag set to true as video");
  			  		}
  			  	}
    			
    			CreateSprinklrAssetImpl sprinklrAssetObj = null;
    			String uploadCreate = StringUtils.EMPTY;
    			
    			if((StringUtils.isNotEmpty(sprinklrAssetPath) && MetadataConstants.ACTIVATE.equals(replicationType) && MetadataConstants.NO.equals(social))
    					|| (StringUtils.isNotEmpty(sprinklrAssetPath) && MetadataConstants.DEACTIVATE.equals(replicationType) && MetadataConstants.YES.equals(social))
    					|| (StringUtils.isNotEmpty(sprinklrAssetPath) && MetadataConstants.ACTIVATE.equals(replicationType) && MetadataConstants.YES.equals(social))){
    				logger.info("[asset exists in SAM - "+sprinklrAssetPath+"][ social - "+social + "][ replicationType - " + replicationType);
    				
    				if(metadataNode.hasProperty(MetadataConstants.SPRINKLR_ASSET_ID)){
    					String socialAssetId = metadataNode.getProperty(MetadataConstants.SPRINKLR_ASSET_ID).getString();
    					logger.info("going to delete asset with id - "+socialAssetId);
    					sprinklrAssetObj = new CreateSprinklrAssetImpl(accessTokenValue, key,sprinklrEndPoint , sprinklrDAMUploadEndPoint);
    					boolean isAssetDeleted = sprinklrAssetObj.deleteAsset(socialAssetId);
    					if(isAssetDeleted){
    						logger.info("asset deleted");
  			  				metadataNode.getProperty(MetadataConstants.SPRINKLR_ASSET_ID).remove();
  			  				metadataNode.getProperty(MetadataConstants.PREVIEW_URL).remove();
  			  				metadataNode.getProperty(MetadataConstants.SPRINKLR_ASSET_PATH).remove();
  			  				metadataNode.getProperty(MetadataConstants.SPRINKLR_ASSET_CREATED).remove();
  			  				session.save();
  			  				logger.info("asset properties deleted");
    					}
    				}
    				
    				if(StringUtils.isNotEmpty(sprinklrAssetPath) && MetadataConstants.ACTIVATE.equals(replicationType) && MetadataConstants.YES.equals(social))
						uploadCreate = "MetadataUpdate";
    			} 
    			
    			if("MetadataUpdate".equals(uploadCreate) || (StringUtils.isEmpty(sprinklrAssetPath) && MetadataConstants.ACTIVATE.equals(replicationType) && MetadataConstants.YES.equals(social))){
    				logger.info("asset is not in SAM - " + "[ social - "+social + "][ replicationType - " + replicationType);
    				if(assetNode.hasProperty(MetadataConstants.JCRCREATEDBY)){
  			  			createdBy = assetNode.getProperty(MetadataConstants.JCRCREATEDBY).getString();
  			  		}
  			  		logger.info("contentType - "+contentType+" flag - "+flag);
  			  		if(flag && !MetadataConstants.WORKFLOW_PROCESS.equals(createdBy) && MetadataConstants.YES.equals(social)){
  			  			logger.info("going to create new sprinklr properties now");
			  			sprinklrAssetObj = new CreateSprinklrAssetImpl(accessTokenValue, key,sprinklrEndPoint , sprinklrDAMUploadEndPoint);
			  			HttpResponse response = sprinklrAssetObj.uploadAsset(assetName, contentType, is);
			  			if(null!=response){
			  				response_string = EntityUtils.toString(response.getEntity());
			  				statusCode = response.getStatusLine().getStatusCode();
			  				logger.info("response_string - "+response_string);
			  			}
			  			if(statusCode != 200){
			  				int count = 3;
			  				while (count-- > 0 && statusCode != 200) {
			  					response = sprinklrAssetObj.uploadAsset(assetName, contentType, is);
			  					if(null!=response){
			  						response_string = EntityUtils.toString(response.getEntity());
			  						statusCode = response.getStatusLine().getStatusCode();
			  						logger.info("response_string - "+response_string);
			  					}
			  				}
			  			} else{
			  				String createdAssetId = sprinklrAssetObj.createAsset(extension, response_string, assetName, assetDescription, assetExpiry, createdBy, productTags, industryTags, sectorTags, null);
			  				logger.info("createdAssetId after return - "+createdAssetId);
			  				if (StringUtils.EMPTY.equals(createdAssetId)) {
			  					int count = 3;
			  					while (count-- > 0 && StringUtils.EMPTY.equals(createdAssetId)) {
			  						createdAssetId = sprinklrAssetObj.createAsset(extension, response_string, assetName, assetDescription, assetExpiry, createdBy, productTags, industryTags, sectorTags, null);
			  					}
			  				}
			  				
			  				if(StringUtils.isNotEmpty(createdAssetId)){
			  					JSONParser parser = new JSONParser();
			  					JSONObject json = (JSONObject) parser.parse(response_string);												
			  					metadataNode.setProperty(MetadataConstants.SPRINKLR_ASSET_ID, createdAssetId);
			  					metadataNode.setProperty(MetadataConstants.PREVIEW_URL, (String)json.get(MetadataConstants.PREVIEW_URL));
			  					metadataNode.setProperty(MetadataConstants.SPRINKLR_ASSET_PATH, sam_app+createdAssetId);
			  					metadataNode.setProperty(MetadataConstants.SPRINKLR_ASSET_CREATED, true);
			  					session.save();
			  				}
			  			}
  			  		}	
    			}
			}
		} catch(Exception ex){
			logger.error(ex.getClass() + ": " + ex.getMessage() + ": " + ex.getCause(), ex);
		} finally {
                if (resourceResolver != null && resourceResolver.isLive()) {
                resourceResolver.close();
            }
        }
		logger.info(" SprinklrIntegrationProcess Execution Ends");
		return false;
	}

}

package com.dtc.core.utils;

import com.day.cq.replication.ReplicationException;
import com.dtc.core.schedulers.SolrProductsJSONObject;
import com.dtc.core.services.EnvironmentConfig;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.jackrabbit.commons.flat.TreeTraverser;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Deloitte Digital.
 */
public class SolrUtils {
    private static final Logger logger = LoggerFactory.getLogger(SolrUtils.class);

    // Solr language node properties to search on;
    private enum ProductSearchFieldsEnum{
        SKU, TITLE, DESCRIPTION, KEY_INGREDIENTS, CATEGORIES, SEARCH_TERMS
    }

    // Solr product keys - !!! Warning, don't change the string values. They map directly to the pmd productDetail node. !!!
    public static final String PATH_EXACT = "path_exact";
    public static final String SKU = "sku";
    public static final String MARKETS = "markets";
    public static final String LANGUAGES = "languages";
    public static final String TITLE = "title";
    public static final String JCR_TITLE = "jcr:title";
    public static final String DESCRIPTION = "description";
    public static final String PRODUCTS = "products";
    public static final String KEY_INGREDIENTS = "keyIng";
    public static final String CATEGORIES = "categories";
    public static final String SEARCH_TERMS = "searchTerms";
    public static final String PURCHASABLE = "purchasable";
    public static final String IMAGE_URL = "url";
    public static final String PV = "pv";
    public static final String RETAIL_PRICE = "rtl";
    public static final String BASE_SKU = "baseSku";
    public static final String ROOT = "_root_";
    public static final String CHILD_DOCS = "_childDocuments_";
    public static final String VERSION = "_version_";


    // Solr query parameters
    public static final String PRODUCT_INDEX_PREFIX = "product/";
    private static final String AND = "%20AND%20";
    private static final String OR = "%20OR%20";
    public static final String SKU_WILDCARD = "????????";
    public static final String BASE_SKU_WILDCARD = "????????????????";
    private static final String PRODUCT_INDEX_QUERY_PARAM = PATH_EXACT + ":" + PRODUCT_INDEX_PREFIX;
    private static final String CHILD_FILTER = "%20childFilter=";
    private static final String PARENT_FILTER = "child%20parentFilter=";
    private static final String MAX_RESULTS_TO_RETURN = "&rows=5";
    private static final String RETURN_JSON = "&wt=json";


    // Solr url path stuff
    public static final String SOLR_SELECT_URI = "oak/select?fl=";
    public static final String JCR_CONTENT = "jcr:content";
    public static final String PMD_PATH_PREFIX = "/content/pmd";
    private static final String SOLR_UPDATE_SUFFIX_URI  = "oak/update/json?wt=json&commit=true";
    private static final String SOLR_DELETE_SUFFIX_URI  = "oak/update?commit=true%20-H%20--data-binary";

    // Other
    private static final String CLIENT_ID = "client_id";
    private static final String CLIENT_SECRET = "client_secret";

    private static final int SKU_LENGTH = 8;


    //---------------------------------------------------------------------------
    //
    //                           Public Methods - Queries
    //
    //---------------------------------------------------------------------------

    /**
     * Get all products for the given language and market. (Note, we restrict the number of rows returned by "MAX_RESULTS_TO_RETURN").
     *
     * @param language
     * @param market
     * @return
     * @throws IOException
     * @throws JSONException
     */
    public static SolrProductAdaptor[] getProducts(String language, String market, EnvironmentConfig environmentConfig) throws IOException, JSONException {
        return getProductBySku(SKU_WILDCARD, language, market, environmentConfig);
    }

    /**
     * Get the product for the given sku, language, and market.
     *
     * @param sku
     * @param language
     * @param market
     * @return
     * @throws IOException
     * @throws JSONException
     */
    public static SolrProductAdaptor[] getProductBySku(String sku, String language, String market, EnvironmentConfig environmentConfig) throws IOException, JSONException {
        String solrUrl =
            environmentConfig.getMulesoftSolrEndpoint() +
            SOLR_SELECT_URI + "*," +
            getChildrenFilter(sku, language.toLowerCase()) + "," +
            getChildrenFilter(sku, market.toUpperCase()) +
            getResultFilter(PRODUCT_INDEX_QUERY_PARAM + sku);

        return toSolrProductArray(executeQuery(solrUrl));
    }

    /**
     * Get all products found by matching the given searchTerm to several language fields for the given language and
     * market. (Note, we restrict the number of rows returned by "MAX_RESULTS_TO_RETURN").
     *
     * @param searchTerm
     * @param language
     * @param market
     * @return
     * @throws IOException
     * @throws JSONException
     */
    public static SolrProductAdaptor[] getProductsBySearchTerm(String searchTerm, String language, String market, EnvironmentConfig environmentConfig) throws IOException, JSONException {
        ArrayList<ProductSearchFieldsEnum> productSearchFields = new ArrayList<>();

        productSearchFields.add(ProductSearchFieldsEnum.SKU);
        productSearchFields.add(ProductSearchFieldsEnum.TITLE);
        productSearchFields.add(ProductSearchFieldsEnum.DESCRIPTION);
        productSearchFields.add(ProductSearchFieldsEnum.KEY_INGREDIENTS);
        productSearchFields.add(ProductSearchFieldsEnum.CATEGORIES);
        productSearchFields.add(ProductSearchFieldsEnum.SEARCH_TERMS);

        return getProductsBySearchTerm(searchTerm, productSearchFields, language, market, environmentConfig);
    }

    /**
     * Get all products found by matching the given searchTerms to the fields found in the ArrayList for the given language and
     * market. (Note, we restrict the number of rows returned by "MAX_RESULTS_TO_RETURN").
     * @param searchTerm
     * @param productSearchFields
     * @param language
     * @param market
     * @return
     * @throws IOException
     * @throws JSONException
     */
    public static SolrProductAdaptor[] getProductsBySearchTerm(String searchTerm, ArrayList<ProductSearchFieldsEnum> productSearchFields, String language, String market, EnvironmentConfig environmentConfig) throws IOException, JSONException {
        // Example solrUrl: http://test2-solr-1:8983/solr/oak/select?fl=_root_&fq=-baseSku:[*%20TO%20*]%20&&%20markets:US&indent=on&q=path_exact:product/01003611/en&wt=json
        String solrUrl =
            environmentConfig.getMulesoftSolrEndpoint() +
            SOLR_SELECT_URI + ROOT +
            "&fq=" + "-baseSku:[*%20TO%20*]%20&&%20" + MARKETS + ":" + market.toUpperCase() +
            getSearchTermQuery(searchTerm, productSearchFields) +
            getResultFilter(PRODUCT_INDEX_QUERY_PARAM + SKU_WILDCARD + "/" + language.toLowerCase());

        // Search for language nodes for the given market that match the search term.
        JSONArray matchingRootProducts = executeQuery(solrUrl);

        // Get the products for the matching root nodes
        String rootNodesQuery = "";
        for (int i=0;i<matchingRootProducts.length();i++){
            String[] temp = JSONObjectUtil.toStringArray(matchingRootProducts.getJSONObject(0).getJSONArray(ROOT));
            rootNodesQuery += PATH_EXACT + ":" + temp[0];

            if (i < matchingRootProducts.length() - 1){
                rootNodesQuery += OR;
            }
        }

        // Return the complete records for the given matches
        // Example solrUrl: http://test2-solr-1:8983/solr/oak/select?fl=*,%20[child%20parentFilter=path_exact:product/????????%20childFilter=path_exact:product/????????/en],%20[child%20parentFilter=path_exact:product/????????%20childFilter=path_exact:product/????????/US]&fq=_root_:product/01003635%20OR%20_root_:product/01003611&indent=on&q=path_exact:product/????????&rows=5&wt=json
        solrUrl =
            environmentConfig.getMulesoftSolrEndpoint() +
            SOLR_SELECT_URI + "*," +
            getChildrenFilter(SKU_WILDCARD, language.toLowerCase()) + "," +
            getChildrenFilter(SKU_WILDCARD, market.toUpperCase()) +
            getResultFilter(rootNodesQuery);

        return toSolrProductArray(executeQuery(solrUrl));
    }

    //---------------------------------------------------------------------------
    //
    //                           Public Methods - Other
    //
    //---------------------------------------------------------------------------

    /**
     * Create the Solr document for a given Sku.
     *
     * @param sku
     * @param session
     * @return
     * @throws JSONException
     * @throws RepositoryException
     * @throws ReplicationException
     */
    public static JSONObject createPMDDocument(String sku, Session session) throws JSONException, RepositoryException, ReplicationException {
        JSONObject pmdDocument = new JSONObject();
        JSONArray marketJSONArray;
        JSONArray langArray;
        JSONArray childDocuments = new JSONArray();

        String[] languages = getLanguages(sku, session);

        // Set all of the PMD document properties
        pmdDocument.put(PATH_EXACT, PRODUCT_INDEX_PREFIX + sku);
        pmdDocument.put(SKU, sku);

        if (languages.length > 0) {
            JSONObject jsonObj;
            JSONObject productContainerInfo = getProductContainerInfo(sku, languages[0], session);

            if (productContainerInfo.has(MARKETS)){
                Object markets = productContainerInfo.get(MARKETS);
                if (markets instanceof JSONArray){
                    marketJSONArray = (JSONArray)markets;
                }
                else if (markets instanceof String){
                    marketJSONArray = JSONObjectUtil.toJSONArray(new String[]{(String)markets});
                }
                else{
                    marketJSONArray = new JSONArray();
                }
            }
            else{
                marketJSONArray = new JSONArray();
            }

            langArray = JSONObjectUtil.toJSONArray(languages);
            String baseSku = productContainerInfo.has(BASE_SKU) ? productContainerInfo.getString(BASE_SKU) : "";

            // Only write the base sku if there is a value.
            if (baseSku != null && baseSku.length() > 0) {
                pmdDocument.put(BASE_SKU, baseSku);
            }

            // Create all of the language child documents
            for (int i = 0; i < languages.length; i++) {
                try {
                    jsonObj = createLanguageDocument(sku, languages[i], marketJSONArray, session);
                    childDocuments.put(jsonObj);
                }
                catch (PathNotFoundException e){
                    logger.error("Couldn't find PMD product detail path for sku: " + sku + " and language: " + languages[i]);
                }
            }

            // Create all of the market child documents
            String[] markets = JSONObjectUtil.toStringArray(marketJSONArray);
            for (int i = 0; i < markets.length; i++) {
                try {
                    jsonObj = createMarketDocument(sku, markets[i], session);
                    childDocuments.put(jsonObj);
                }
                catch (PathNotFoundException e){
                    logger.error("Couldn't find PIM for sku: " + sku + " and market: " + markets[i]);
                }
            }

            // Set the markets and languages arrays.
            pmdDocument.put(MARKETS, marketJSONArray);
            pmdDocument.put(LANGUAGES, langArray);

            // Set the children (language and market documents).
            pmdDocument.put(CHILD_DOCS, childDocuments);
        }
        else{
            JSONObject noChild = new JSONObject();
            noChild.put(PATH_EXACT, PRODUCT_INDEX_PREFIX + "noChild");
            childDocuments.put(noChild);

            // Set the children (language and market documents).
            pmdDocument.put(CHILD_DOCS, childDocuments);
            pmdDocument.put(ROOT, PRODUCT_INDEX_PREFIX + sku);
        }

        return pmdDocument;
    }

    /**
     * This is the preferred way to write products to solr. This method adds products to a JSONArray for batch updates.
     * The updates are done from a schduler.
     *
     * @param products - Use SolrUtils.createPMDDocument() to create each product. Then add the products to a JSONArray for the "products" param.
     * @param environmentConfig
     * @throws JSONException
     * @throws IOException
     */
    public static void writeProducts(JSONArray products, EnvironmentConfig environmentConfig) throws JSONException, IOException {
        String solrUrl = environmentConfig.getMulesoftSolrEndpoint() + SOLR_UPDATE_SUFFIX_URI;
        String client_id = environmentConfig.getMulesoftClientId();
        String client_secret = environmentConfig.getMulesoftClientSecret();

        // For now just write something into SOLR
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost postRequest = new HttpPost(solrUrl);

        StringEntity input = new StringEntity(products.toString());
        input.setContentType("application/json");
        postRequest.setEntity(input);
        postRequest.addHeader(CLIENT_ID, client_id);
        postRequest.addHeader(CLIENT_SECRET, client_secret);
        HttpResponse response = httpClient.execute(postRequest);

        if (response.getStatusLine().getStatusCode() != 200){
            logger.error("Error: Could not write record to SOLR");
        }
    }

    /**
     * Write the PMD record for the given sku to Solr. This method will make sure the languages and markets are
     * obtained from the PMD of the sku.
     *
     * Note: this hits the wire for every write.
     *  @param sku
     * @param session
     */
    public static void writeProductData(String sku, EnvironmentConfig environmentConfig, Session session) {
        try {
            JSONObject doc;
            JSONObject langJsonObj = createPMDDocument(sku, session);

            doc = new JSONObject();
            doc.put("doc", langJsonObj);
            writeProductData(doc, environmentConfig);
        }
        catch(JSONException e){
            logger.error("Error - JSONException: Could not write record to SOLR. ", e);
        } catch (IOException e) {
            logger.error("Error - IOException: Could not write record to SOLR. ", e);
        } catch (RepositoryException e) {
            logger.error("Error - RepositoryException: Could not write record to SOLR. ", e);
        } catch (ReplicationException e) {
            logger.error("Error - ReplicationException: Could not write record to SOLR. ", e);
        }
    }

    /**
     * Write the PMD record for the given sku to Solr. The languages and markets must be the supported languages and
     * markets on the PMD.
     *
     * Note: This method is used by the Solr update Preprocessor. We may want to delete this at some point.
     *
     * @param sku
     * @param languages
     * @param markets
     * @param session
     */
    public static void writeProductData(String sku, String[] languages, String[] markets, EnvironmentConfig environmentConfig, Session session) {
        try {
            JSONObject doc;
            JSONObject langJsonObj = createPMDDocument(sku, languages, markets, session);

            doc = new JSONObject();
            doc.put("doc", langJsonObj);
            writeProductData(doc, environmentConfig);
        }
        catch(JSONException e){
            logger.error("Error - JSONException: Could not write record to SOLR. ", e);
        } catch (IOException e) {
            logger.error("Error - IOException: Could not write record to SOLR. ", e);
        } catch (RepositoryException e) {
            logger.error("Error - RepositoryException: Could not write record to SOLR. ", e);
        }
    }

    /**
     * This method is used to update all the Solr products. The method will Go through all the PMD records and update
     * them into Solr.
     *
     * This will update just the "activated language nodes" for this sku because the updateAllProducts method
     * should only be called from a pub server. Pub servers only have activiated langages.
     *
     * @param session
     * @throws RepositoryException
     * @throws JSONException
     * @throws ReplicationException
     * @throws IOException
     */
    public static void updateAllProducts(Session session) throws RepositoryException, JSONException, ReplicationException, IOException {
        // We need to walk the PMD tree down to the SKU record "Product Master Detail" record and get the sku. Then
        // write the record to Solr. We don't have to walk the language nodes.
        Node pmdNode = (Node) session.getItem(PMD_PATH_PREFIX);
        Iterator<Node> childItr = new TreeTraverser(pmdNode).iterator();

        String childNodeName;
        String sku;

        // This walks the entire PMD Tree.
        while (childItr.hasNext()){
            childNodeName = childItr.next().getName();

            if (childNodeName.length() == 8 && StringUtils.isNumeric(childNodeName)) {
                sku = childNodeName;
                SolrProductsJSONObject.addProduct(createPMDDocument(sku, session));
            }
        }
    }

    /**
     * Delete all Solr PMD documents and their children.
     *
     * @param environmentConfig
     * @throws JSONException
     * @throws IOException
     */
    public static void deleteAllProducts(EnvironmentConfig environmentConfig) throws JSONException, IOException {
        // Delete regular product and variant products.
        deleteMatchingSkus(SKU_WILDCARD, environmentConfig);

        // Delete base products (These products have variants)
        deleteMatchingSkus(BASE_SKU_WILDCARD, environmentConfig);

//        // Delete orphan products (Product that don't have any children) if they exist.
//        deleteOrphanProducts(SKU_WILDCARD, environmentConfig);
//        deleteOrphanProducts(BASE_SKU_WILDCARD, environmentConfig);
    }

//    /**
//     * Delete Orphan products in Solr. These are products that don't have any Children.
//     * todo This method is not working. It returns a 400.
//     *
//     * @param skuMatchingPattern
//     * @param environmentConfig
//     * @throws JSONException
//     * @throws IOException
//     */
//    public static void deleteOrphanProducts(String skuMatchingPattern, EnvironmentConfig environmentConfig)  throws JSONException, IOException{
//        String solrUrl = environmentConfig.getMulesoftSolrEndpoint() + SOLR_DELETE_SUFFIX_URI;
//        String client_id = environmentConfig.getMulesoftClientId();
//        String client_secret = environmentConfig.getMulesoftClientSecret();
//
//        // For now just write something into SOLR
//        HttpClient httpClient = new DefaultHttpClient();
//        HttpPost postRequest = new HttpPost(solrUrl);
//
//        JSONObject delete = new JSONObject();
//        JSONObject query = new JSONObject();
//
//        query.put("query", PATH_EXACT + ":" + PRODUCT_INDEX_PREFIX + skuMatchingPattern + AND + "-_root_:*");
//        delete.put("delete", query);
//
//        StringEntity input = new StringEntity(delete.toString());
//        input.setContentType("application/json");
//        postRequest.setEntity(input);
//        postRequest.addHeader(CLIENT_ID, client_id);
//        postRequest.addHeader(CLIENT_SECRET, client_secret);
//        HttpResponse response = httpClient.execute(postRequest);
//
//        if (response.getStatusLine().getStatusCode() != 200){
//            logger.error("Error: Could not delete record to SOLR");
//        }
//    }

    /**
     * Delete the sku matches from Solr. This is one wire hit that executes immediatly and removes all
     * products from Solr that match the skuMatchingPattern.
     *
     * @param skuMatchingPattern
     * @param environmentConfig
     * @throws JSONException
     * @throws IOException
     */
    public static void deleteMatchingSkus(String skuMatchingPattern, EnvironmentConfig environmentConfig)  throws JSONException, IOException{
        String solrUrl = environmentConfig.getMulesoftSolrEndpoint() + SOLR_DELETE_SUFFIX_URI;
        String client_id = environmentConfig.getMulesoftClientId();
        String client_secret = environmentConfig.getMulesoftClientSecret();

        // For now just write something into SOLR
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost postRequest = new HttpPost(solrUrl);

        JSONObject delete = new JSONObject();
        JSONObject query = new JSONObject();

        // This query will select both the parent and all the children of the parent and delete them.
        query.put("query", ROOT + ":" + PRODUCT_INDEX_PREFIX + skuMatchingPattern);
        delete.put("delete", query);

        StringEntity input = new StringEntity(delete.toString());
        input.setContentType("application/json");
        postRequest.setEntity(input);
        postRequest.addHeader(CLIENT_ID, client_id);
        postRequest.addHeader(CLIENT_SECRET, client_secret);
        HttpResponse response = httpClient.execute(postRequest);

        if (response.getStatusLine().getStatusCode() != 200){
            logger.error("Error: Could not delete record to SOLR");
        }
    }

    /**
     * Convert a JSONArray containing Solr products into a SolrProductAdaptor array.
     * @param solrProductArray
     * @return
     * @throws JSONException
     */
    public static SolrProductAdaptor[] toSolrProductArray(JSONArray solrProductArray) throws JSONException {
        SolrProductAdaptor[] products = new SolrProductAdaptor[solrProductArray.length()];

        for (int i=0;i<solrProductArray.length();i++){
            products[i] = new SolrProductAdaptor(solrProductArray.getJSONObject(i));
        }

        return products;
    }

    //---------------------------------------------------------------------------
    //
    //                            Private Methods
    //
    //---------------------------------------------------------------------------

    /**
     * Build a portion of the Solr query for the fields to search for and the given search term.
     *
     * @param searchTerm
     * @param productSearchFields
     * @return
     */
    private static String getSearchTermQuery(String searchTerm, ArrayList<ProductSearchFieldsEnum> productSearchFields){
        String queryString = "";

        if (productSearchFields.size() > 0) {
            queryString += "&fq=";

            if (productSearchFields.contains(ProductSearchFieldsEnum.SKU)) {
                queryString += SKU + ":" + searchTerm + OR;
            }
            if (productSearchFields.contains(ProductSearchFieldsEnum.TITLE)) {
                queryString += TITLE + ":" + searchTerm + OR;
            }
            if (productSearchFields.contains(ProductSearchFieldsEnum.DESCRIPTION)) {
                queryString += DESCRIPTION + ":" + searchTerm + OR;
            }
            if (productSearchFields.contains(ProductSearchFieldsEnum.KEY_INGREDIENTS)) {
                queryString += KEY_INGREDIENTS + ":" + searchTerm + OR;
            }
            if (productSearchFields.contains(ProductSearchFieldsEnum.CATEGORIES)) {
                queryString += CATEGORIES + ":" + searchTerm + OR;
            }
            if (productSearchFields.contains(ProductSearchFieldsEnum.SEARCH_TERMS)) {
                queryString += SEARCH_TERMS + ":" + searchTerm + OR;
            }

            // Remove the last "OR".
            int lastIndexOfOr = queryString.lastIndexOf(OR);

            if (lastIndexOfOr != -1) {
                queryString = queryString.substring(0, lastIndexOfOr);
            }
        }

        return queryString;
    }

    /**
     * Build a portion of the Solr query which returns nested children documents (language or market documents).
     * @param sku
     * @param childIndexSuffix
     * @return
     */
    private static String getChildrenFilter(String sku, String childIndexSuffix){
        return "[" + PARENT_FILTER + PRODUCT_INDEX_QUERY_PARAM + sku + CHILD_FILTER + PRODUCT_INDEX_QUERY_PARAM + sku + "/" + childIndexSuffix + "]";
    }

    /**
     * Build a portion of the Solr query which returns the number and type of the results.
     * @param queryParam
     * @return
     */
    private static String getResultFilter(String queryParam){
        return "&q=" + queryParam + MAX_RESULTS_TO_RETURN + RETURN_JSON;
    }

    /**
     * Execute an Http request for the query specified by the queryUrl.
     *
     * @param queryUrl
     * @return
     * @throws IOException
     * @throws JSONException
     */
    private static JSONArray executeQuery(String queryUrl) throws IOException, JSONException {
        JSONArray retObj;
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(queryUrl);
        request.addHeader("accept", "application/json");
        HttpResponse response = client.execute(request);

        if (response.getStatusLine().getStatusCode() != 200){
            logger.error("Error: Could not read SOLR records");
            retObj = new JSONArray();
        }
        else {
            String json = IOUtils.toString(response.getEntity().getContent());
            JSONObject responseObj = (new JSONObject(json)).getJSONObject("response");
            retObj = responseObj.getJSONArray("docs");
        }

        return retObj;
    }

    /**
     * Return the languages for the given Sku. The languages are retrieved from the child nodes of the PMD.
     *
     * @param sku
     * @param session
     * @return
     * @throws RepositoryException
     * @throws ReplicationException
     */
    private static String[] getLanguages(String sku, Session session) throws RepositoryException, ReplicationException {
        String[] languages;
        ArrayList<String> children = new ArrayList<>();

        String pmdPath = SkuPathUtil.getPMDPathToSku(sku);
        Node pmdNode = JcrUtils.getNodeIfExists(pmdPath, session);

        if (pmdNode != null) {
            NodeIterator childItr = pmdNode.getNodes();

            String childName;

            // Only add children where the child language node is activated.
            while (childItr.hasNext()) {
                childName = childItr.nextNode().getName();
                if (!childName.equals(JCR_CONTENT)) {
                    children.add(childName);
                }
            }

            languages = new String[children.size()];
            children.toArray(languages);
        }
        else{
            languages = new String[0];
        }

        return languages;
    }

    /**
     * Create the Solr document for a given Sku.
     *
     * Note: This method is used by the Solr update Preprocessor. We may want to delete this at some point.
     *
     * @param sku
     * @param languages
     * @param markets
     * @param session
     * @return
     * @throws JSONException
     * @throws RepositoryException
     */
    private static JSONObject createPMDDocument(String sku, String[] languages, String[] markets, Session session) throws JSONException, RepositoryException {
        JSONObject pmdDocument = new JSONObject();

        JSONArray childDocuments = new JSONArray();
        JSONArray marketsArray = JSONObjectUtil.toJSONArray(markets);
        JSONArray languagesArray = JSONObjectUtil.toJSONArray(languages);
        JSONObject jsonObj;

        // Set all of the PMD document properties
        pmdDocument.put(PATH_EXACT, PRODUCT_INDEX_PREFIX + sku);
        pmdDocument.put(SKU, sku);
        pmdDocument.put(MARKETS, marketsArray);
        pmdDocument.put(LANGUAGES, languagesArray);

        // Create all of the language child documents
        for (int i = 0; i < languages.length; i++) {
            try {
                jsonObj = createLanguageDocument(sku, languages[i], marketsArray, session);
                childDocuments.put(jsonObj);
            }
            catch (PathNotFoundException e){
                logger.error("Couldn't find PMD product detail path for sku: " + sku + " and language: " + languages[i]);
            }
        }

        // Create all of the market child documents
        for (int i = 0; i < markets.length; i++) {
            try{
                jsonObj = createMarketDocument(sku, markets[i], session);
                childDocuments.put(jsonObj);
            }
            catch (PathNotFoundException e){
                logger.error("Couldn't find PIM for sku: " + sku + " and market: " + markets[i]);
            }
        }

        pmdDocument.put(CHILD_DOCS, childDocuments);

        return pmdDocument;
    }

    /**
     * Create the language document for the given sku. This will be a child document for the Solr record for the sku.
     *
     * @param sku
     * @param language
     * @param marketJSONArray
     * @param session
     * @return
     * @throws JSONException
     * @throws RepositoryException
     */
    private static JSONObject createLanguageDocument(String sku, String language, JSONArray marketJSONArray, Session session) throws JSONException, RepositoryException {
        JSONObject languageDocument = new JSONObject();

        // PMD product detail path (Example: /content/pmd/01/00/36/01003610/en/jcr:content/productContainer/productDetail)
        String pmdProductDetailPath = SkuPathUtil.getPMDPathToSkuProductDetail(sku, language);
        String pmdProductSupplementsPath = SkuPathUtil.getPMDPathToSkuProductSupplements(sku, language);

        // Set the path exact and sku property
        languageDocument.put(PATH_EXACT, PRODUCT_INDEX_PREFIX + sku + "/" + language);
        languageDocument.put(SKU, sku);
        languageDocument.put(MARKETS, marketJSONArray);

        // Write the string properties from the PMD Page
        Node productDetailPageNode = (Node) session.getItem(pmdProductDetailPath);
        Node productSupplementsPageNode = (Node) session.getItem(pmdProductSupplementsPath);

        languageDocument = addStringProperties(productDetailPageNode, TITLE, JCR_TITLE, languageDocument);
        languageDocument = addStringProperties(productDetailPageNode, DESCRIPTION, DESCRIPTION, languageDocument);
        languageDocument = addStringPropertiesFullTagPath(productSupplementsPageNode, KEY_INGREDIENTS, KEY_INGREDIENTS, languageDocument);
        languageDocument = addStringPropertiesFullTagPath(productDetailPageNode, CATEGORIES, CATEGORIES, languageDocument);
        languageDocument = addStringProperties(productDetailPageNode, SEARCH_TERMS, SEARCH_TERMS, languageDocument);

        return languageDocument;
    }

    /**
     * Create the market document for the given sku. This will be a child document for the Solr record for the sku.
     *
     * @param sku
     * @param market
     * @param session
     * @return
     * @throws JSONException
     * @throws RepositoryException
     */
    private static JSONObject createMarketDocument(String sku, String market, Session session) throws JSONException, RepositoryException {
        JSONObject marketDocument = new JSONObject();

        // PIM Path Example: /etc/commerce/products/dtc/US/01/00/36/11/01003611
        String pmdPath = SkuPathUtil.getPIMPathToSku(sku, market);

        // Set the path exact and sku property
        marketDocument.put(PATH_EXACT, PRODUCT_INDEX_PREFIX + sku + "/" + market);
        marketDocument.put(SKU, sku);

        // Write the string properties from the PIM
        Node pimNode = (Node) session.getItem(pmdPath);

        marketDocument = addStringProperties(pimNode, PURCHASABLE, PURCHASABLE, marketDocument);
        marketDocument = addStringProperties(pimNode, IMAGE_URL, IMAGE_URL, marketDocument);     // todo This needs to be added to the PMD language node.
        marketDocument = addStringProperties(pimNode, RETAIL_PRICE, RETAIL_PRICE, marketDocument);  // todo This needs to be added to the PIM
        marketDocument = addStringProperties(pimNode, PV, PV, marketDocument);            // todo This needs to be added to the PIM

        return marketDocument;
    }

    /**
     * Write a single product document defined by doc to Solr.
     *
     * @param doc
     * @param environmentConfig
     * @throws JSONException
     * @throws IOException
     */
    private static void writeProductData(JSONObject doc, EnvironmentConfig environmentConfig) throws JSONException, IOException {
        String solrUrl = environmentConfig.getMulesoftSolrEndpoint() + SOLR_UPDATE_SUFFIX_URI;
        String client_id = environmentConfig.getMulesoftClientId();
        String client_secret = environmentConfig.getMulesoftClientSecret();

        // For now just write something into SOLR
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost postRequest = new HttpPost(solrUrl);

        JSONObject add = new JSONObject();

        add.put("add", doc);

        StringEntity input = new StringEntity(add.toString());
        input.setContentType("application/json");
        postRequest.setEntity(input);
        postRequest.addHeader(CLIENT_ID, client_id);
        postRequest.addHeader(CLIENT_SECRET, client_secret);
        HttpResponse response = httpClient.execute(postRequest);

        if (response.getStatusLine().getStatusCode() != 200){
            logger.error("Error: Could not write record to SOLR");
        }
    }

    /**
     * Helper method for adding a property of type String or String[] to the given properties.
     *
     * @param productDetailPageNode
     * @param solrPropertyName
     * @param jcrPropertyName
     * @param properties
     * @return
     * @throws RepositoryException
     * @throws JSONException
     */
    private static JSONObject addStringProperties(Node productDetailPageNode, String solrPropertyName, String jcrPropertyName, JSONObject properties) throws RepositoryException, JSONException {
        if (productDetailPageNode.hasProperty(jcrPropertyName)){
            javax.jcr.Property property = productDetailPageNode.getProperty(jcrPropertyName);

            if (property != null){
                // Is the property multivalued (an array).
                if (property.isMultiple()){
                    String[] tempVals;
                    Value[] values = property.getValues();
                    JSONArray array = new JSONArray();

                    for (int i = 0; i < values.length; i++) {
                        tempVals = values[i].getString().split("/");
                        if (tempVals.length > 0) {
                            array.put(tempVals[tempVals.length - 1]);
                        } else {
                            array.put(values[i].getString());
                        }
                    }
                    properties.put(solrPropertyName, array);
                }

                // Single valued property.
                else {
                    properties.put(solrPropertyName, property.getString());
                }
            }
        }

        return properties;
    }


    /**
     * Helper method for adding a property of type String or String[] to the given properties. This keeps the full tag path.
     * Example Full tag path: "mecommerce:categories/personal-care/ageloc" vs "ageloc".
     *
     * @param productDetailPageNode
     * @param jcrPropertyName
     * @param properties
     * @return
     * @throws RepositoryException
     * @throws JSONException
     */
    private static JSONObject addStringPropertiesFullTagPath(Node productDetailPageNode, String solrPropertyName, String jcrPropertyName, JSONObject properties) throws RepositoryException, JSONException {
        if (productDetailPageNode.hasProperty(jcrPropertyName)) {
            javax.jcr.Property property = productDetailPageNode.getProperty(jcrPropertyName);

            if (property != null) {
                // Is the property multivalued (an array).
                if (property.isMultiple()) {
                    Value[] values = property.getValues();
                    JSONArray array = new JSONArray();
                    for (int i = 0; i < values.length; i++) {
                        array.put(values[i].getString());
                    }
                    properties.put(solrPropertyName, array);
                }

                // Single valued property.
                else {
                    properties.put(solrPropertyName, property.getString());
                }
            }
        }
        return properties;
    }

    /**
     * Get the markets and baseSku for the products PMD.
     *
     * @param sku
     * @param lang
     * @param session
     * @return
     * @throws RepositoryException
     */
    private static JSONObject getProductContainerInfo (String sku, String lang, Session session) throws RepositoryException, JSONException {
        JSONObject productConatinerInfo = new JSONObject();

        String pmdPathToProductContainer = SkuPathUtil.getPMDPathToSkuProductContainer(sku, lang);
        Node srcContentNode = (Node) session.getItem(pmdPathToProductContainer);

        productConatinerInfo = addStringProperties(srcContentNode, MARKETS, MARKETS, productConatinerInfo);
        productConatinerInfo = addStringProperties(srcContentNode, BASE_SKU, BASE_SKU, productConatinerInfo);

        return productConatinerInfo;
    }
}
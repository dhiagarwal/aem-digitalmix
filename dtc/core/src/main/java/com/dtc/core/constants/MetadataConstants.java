package com.dtc.core.constants;

public class MetadataConstants {
	
	public MetadataConstants(){}
	
	public static final String JCRMETADATA = "/jcr:content/metadata";
	public static final String JCRUUID = "jcr:uuid";
	public static final String DCASSETID = "dc:assetId";
	public static final String DCASSETSOURCE = "dc:assetSource";
	public static final String DCDOWNLOADCOUNT = "dc:downloadCount";
	public static final String DCRATINGSCOUNT = "dc:ratingsCount";
	public static final String DCAVGRATING = "dc:avgRating";
	public static final String DCSOCIAL = "dc:social";
	public static final String DCFORMAT = "dc:format";
	public static final String DCTITLE = "dc:title";
	public static final String DCDESCRIPTION = "dc:description";
	public static final String DCPRODUCT = "dc:product";
	public static final String DCCATEGORY = "dc:category";
	public static final String DCINDUSTRY = "dc:industry";
	public static final String DCSECTOR = "dc:sector";
	public static final String EXPIREDATE = "prism:expirationDate";
	public static final String SPRINKLR_ASSET_PATH = "sprinklrAssetPath";
	public static final String ORIGINALJCRPATH = "/jcr:content/renditions/original/jcr:content";
	public static final String JCR = "jcr:content";
	public static final String JCRDATA = "jcr:data";
	public static final String JCRCREATEDBY = "jcr:createdBy";
	public static final String IMAGE = "image";
	public static final String VIDEO = "video";
	public static final String VIDEOCAP = "VIDEO";
	public static final String IMAGECAP = "IMAGE";
	public static final String PHOTOCAP = "PHOTO";
	public static final String NO = "No";
	public static final String YES = "Yes";
	public static final String SPRINKLR_ASSET_ID = "spriklrAssetid";
	public static final String PREVIEW_URL = "previewUrl";
	public static final String SPRINKLR_ASSET_CREATED = "sprinklrAssetCreated";
	public static final String WORKFLOW_PROCESS = "workflow-process-service";
	public static final String ACTIVATE = "Activate";
	public static final String DEACTIVATE = "Deactivate";
	public static final String SAMBOARDINDUSTRY = "58e6d8a1e4b0948beb448df7";
	public static final String SAMBOARDPRODUCT = "56818c30647d8b66c2000002";
	public static final String SAMBOARDSECTOR = "58e6d853e4b0948beb448d16";
	

}

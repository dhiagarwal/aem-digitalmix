package com.dtc.core.models;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;

@Model(adaptables = Resource.class)
public class UserModel {

	@Inject
	@Optional
	private String email;

	@Inject
	@Optional
	private String givenName;

	@Inject
	@Optional
	private String familyName;

	@Inject
	@Optional
	private String identifier;

	@Inject
	@Optional
	private String address1;

	@Inject
	@Optional
	private String address2;

	@Inject
	@Optional
	private String city;

	@Inject
	@Optional
	private String country;

	@Inject
	@Optional
	private String state;

	@Inject
	@Optional
	private String zipCode;

	@Inject
	@Optional
	private String password;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getEmail() {
		return email;
	}

	public String getGivenName() {
		return givenName;
	}

	public String getFamilyName() {
		return familyName;
	}

	public String getIdentifier() {
		return identifier;
	}

	public String getAddress1() {
		return address1;
	}

	public String getAddress2() {
		return address2;
	}

	public String getCity() {
		return city;
	}

	public String getCountry() {
		return country;
	}

	public String getState() {
		return state;
	}

	public String getZipCode() {
		return zipCode;
	}

}

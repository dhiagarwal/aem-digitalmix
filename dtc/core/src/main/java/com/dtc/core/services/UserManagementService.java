package com.dtc.core.services;

import javax.jcr.RepositoryException;

import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.User;

import com.dtc.core.models.UserModel;

public interface UserManagementService {

	/**
	 * @param com.mec.core.models.UserModel
	 * @return org.apache.jackrabbit.api.security.user.User
	 * @throws RepositoryException
	 */
	public User createAccount(UserModel userInfo) throws RepositoryException;
	
	/**
	 * @param com.mec.core.models.UserModel
	 * @return org.apache.jackrabbit.api.security.user.User
	 * @throws RepositoryException
	 */
	public Object createAccountOnHybris(UserModel userInfo) throws RepositoryException;

	/**
	 * @param userId
	 * @return
	 */
	public Authorizable findUser(String userId);

	/**
	 * @param com.mec.core.models.UserModel
	 * @return org.apache.jackrabbit.api.security.user.User
	 */
	public User updateAccount(UserModel userInfo);

	/**
	 * @param userId
	 * @return
	 */
	public Authorizable removeUser(String userId);

	/**
	 * @param userModel
	 * @return
	 * @throws RepositoryException
	 */
	public Object findUserOnHybris(UserModel userModel) throws RepositoryException;

}

package com.dtc.core.workflow;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import java.util.ArrayList;

public class WorkflowUtils {
    private static final Logger log = LoggerFactory.getLogger(WorkflowUtils.class);
    private static final String WORKFLOW_RESOURCE_COLLECTION_PAGE_RESOURCE_TYPE = "cq/workflow/components/collection/page";

    public enum PageType {
        PRODUCT_DETAIL_PAGE, PRODUCT_MASTER_DATA, PIM, OTHER
    }

    protected static PageType getPageType(Node sourceNode) throws RepositoryException {
        PageType pageType = PageType.OTHER;

        if (sourceNode.hasNode("jcr:content")) {
            Node jcrContentNode = sourceNode.getNode("jcr:content");

            if (jcrContentNode != null) {
                if (jcrContentNode.hasProperty("sling:resourceType")) {
                    if (jcrContentNode.getProperty("sling:resourceType").getString().endsWith("product")) {
                        pageType = PageType.PRODUCT_DETAIL_PAGE;
                    }
                    else if (jcrContentNode.getProperty("sling:resourceType").getString().endsWith("productMasterDetail")) {
                        pageType = PageType.PRODUCT_MASTER_DATA;
                    }
                }
            }
        }

        return pageType;
    }

    public ArrayList<Node> recursiveNodeSearch(Node parentNode) {
        ArrayList<Node> result = new ArrayList<Node>();
        try {
            NodeIterator childrenNodes = parentNode.getNodes();
            while (childrenNodes.hasNext()) {
                Node node = childrenNodes.nextNode();
                result.add(node);
                if (node.hasNodes()) {
                    ArrayList<Node> deeperList = recursiveNodeSearch(node);
                    result.addAll(deeperList);
                }
            }
        } catch (Exception ex) {
            System.out.print("Exception : " + ex.toString());
        }
        return result;
    }
}
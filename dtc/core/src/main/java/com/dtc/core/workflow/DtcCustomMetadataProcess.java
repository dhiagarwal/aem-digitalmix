package com.dtc.core.workflow;

import javax.jcr.Node;
import javax.jcr.Session;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.WorkflowData;
import com.adobe.granite.workflow.exec.WorkflowProcess;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.dtc.core.constants.MetadataConstants;

@Component(label = "Custom Metadata Update Process", description = "Custom Metadata Update Process", metatype = true, immediate = true)
@Properties({ @Property(name = "service.description", value = "Custom Metadata Update Process", propertyPrivate = true)})
@Service
public class DtcCustomMetadataProcess implements WorkflowProcess {
	
	@Reference
	ResourceResolverFactory resourceResolverFactory;
	
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	public void execute(WorkItem item, WorkflowSession session, MetaDataMap args) throws WorkflowException {
		
		logger.info("Custom Metadata Update Process Started");		
		try{
			ResourceResolver resourceResolver = resourceResolverFactory.getAdministrativeResourceResolver(null);
			
			final WorkflowData workflowData = item.getWorkflowData();
	        
			String path = workflowData.getPayload().toString();
	        
	        Session jcrSession = session.adaptTo(Session.class); 
			
			Resource jcrcontentRes = resourceResolver.getResource(path);
			ValueMap jcrcontent = jcrcontentRes.adaptTo(ValueMap.class);
			String uuid = jcrcontent.get(MetadataConstants.JCRUUID).toString();
			
			Node assetNode = jcrSession.getNode(path + MetadataConstants.JCRMETADATA);
			
			assetNode.setProperty(MetadataConstants.DCASSETID, uuid);
			assetNode.setProperty(MetadataConstants.DCASSETSOURCE, "DAM");
			assetNode.setProperty(MetadataConstants.DCDOWNLOADCOUNT, 0);
			assetNode.setProperty(MetadataConstants.DCRATINGSCOUNT, 0);
			assetNode.setProperty(MetadataConstants.DCAVGRATING, 0);

			jcrSession.save();
		}
		catch(Exception e){
			logger.error(e.getClass() + ": " + e.getMessage() + ": " + e.getCause(), e);
		}  	
        logger.info("Custom Metadata Update Process Ends");
    }

}

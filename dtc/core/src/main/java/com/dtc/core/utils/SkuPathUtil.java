package com.dtc.core.utils;

/**
 * Created by Deloitte Digital.
 */
public class SkuPathUtil {
    public static final String PMD_PATH_PREFIX = "/content/pmd";
    public static final String PMD_PRODUCT_CONTAINER_SUFFIX = "/jcr:content/productContainer";
    public static final String PMD_PRODUCT_DETAIL_SUFFIX = "/jcr:content/productContainer/productDetail";
    public static final String PMD_PRODUCT_SUPPLEMENTS_SUFFIX = "/jcr:content/productContainer/pdpProductSupplements";

    public static final String PIM_PATH_PREFIX = "/etc/commerce/products/dtc/";

    //---------------------------------------------------------------------------
    //
    //                           Public Methods
    //
    //---------------------------------------------------------------------------
    /**
     * Get the PMD path for the given sku.
     *
     * @param sku
     *
     * @return PMD path (Example: /content/pmd/01/00/36/01003610)
     */
    public static String getPMDPathToSku(String sku){
        return PMD_PATH_PREFIX + "/" + sku.substring(0, 2) + "/" + sku.substring(2, 4) + "/" + sku.substring(4, 6) + "/" + sku;
    }

    /**
     * Get the PMD product container path for the given sku and language.
     *
     * @param sku
     * @param lang
     *
     * @return PMD product container path (Example: /content/pmd/01/00/36/01003610/en/jcr:content/productContainer)
     */
    public static String getPMDPathToSkuProductContainer(String sku, String lang){
        return getPMDPathToSku(sku) + "/" + lang.toLowerCase() + PMD_PRODUCT_CONTAINER_SUFFIX;
    }

    /**
     * Get the PMD product detail path for a given sku and language.
     *
     * @param sku
     * @param lang
     *
     * @return PMD product detail path (Example: /content/pmd/01/00/36/01003610/en/jcr:content/productContainer/productDetail)
     */
    public static String getPMDPathToSkuProductDetail(String sku, String lang){
        return getPMDPathToSku(sku) + "/" + lang.toLowerCase() + PMD_PRODUCT_DETAIL_SUFFIX;
    }


    public static String getPMDPathToSkuProductSupplements(String sku, String lang) {
        return getPMDPathToSku(sku) + "/" + lang.toLowerCase() + PMD_PRODUCT_SUPPLEMENTS_SUFFIX;
    }

    /**
     * Get the PIM path for a given sku and market.
     *
     * @param sku
     * @param market
     *
     * @return PIM Path (Example: /etc/commerce/products/dtc/US/01/00/36/11/01003611)
     */
    public static String getPIMPathToSku(String sku, String market){
        return PIM_PATH_PREFIX + market.toUpperCase() + "/" + sku.substring(0, 2) + "/" + sku.substring(2, 4) + "/" + sku.substring(4, 6) + "/" + sku;
    }
}
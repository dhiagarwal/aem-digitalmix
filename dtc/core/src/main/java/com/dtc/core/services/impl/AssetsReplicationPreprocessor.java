package com.dtc.core.services.impl;

import javax.jcr.Node;
import javax.jcr.Session;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.replication.Preprocessor;
import com.day.cq.replication.ReplicationAction;
import com.day.cq.replication.ReplicationActionType;
import com.day.cq.replication.ReplicationException;
import com.day.cq.replication.ReplicationOptions;
import com.dtc.core.constants.MetadataConstants;
import com.dtc.core.services.DamToSamIntegrationService;


@Component(label = "DTC Assets Replication Preprocessor")
@Service
public class AssetsReplicationPreprocessor implements Preprocessor {

	private static final Logger logger = LoggerFactory.getLogger(AssetsReplicationPreprocessor.class);
	
	@Reference
    private ResourceResolverFactory resourceResolverFactory;
	
	@Reference
	private DamToSamIntegrationService damToSamIntegrationService;
	
	@Override
	public void preprocess(ReplicationAction replicationAction, ReplicationOptions arg1) throws ReplicationException {
		logger.info("In AssetsReplicationPreprocessor.preprocess()");
		
		String type = StringUtils.EMPTY;
		
		if (replicationAction != null && (ReplicationActionType.ACTIVATE.equals(replicationAction.getType()) 
				|| ReplicationActionType.DEACTIVATE.equals(replicationAction.getType()))) {
			if(ReplicationActionType.ACTIVATE.equals(replicationAction.getType())){
				type = MetadataConstants.ACTIVATE;
			} else if(ReplicationActionType.DEACTIVATE.equals(replicationAction.getType())){
				type = MetadataConstants.DEACTIVATE;
			}
        } else {
        	logger.info("Replication Action is null or not Activate not Deactivate");
            return;
        }
		
        final String path = replicationAction.getPath();             
        logger.info("payload path : " + path);
        
        ResourceResolver resourceResolver = null;
        
        try{
        	resourceResolver = resourceResolverFactory.getAdministrativeResourceResolver(null);
        	Session session = resourceResolver.adaptTo(Session.class);
        	Node metadataNode = session.getNode(path + MetadataConstants.JCRMETADATA);
        	
        	if (metadataNode == null) {
                logger.warn("Could not find metadata node for resource");
                return;
            }else{
            	//DAM to SAM workflow
            	if(!metadataNode.hasProperty(MetadataConstants.DCSOCIAL)){
            		logger.warn("social property is not set for the asset");
                    return;
            	}
            	else{
            		//Trigger DAM to SAM workflow
            		damToSamIntegrationService.damToSamIntegrate(path, type);
            	}	
            }
        }catch(Exception ex){
			logger.error(ex.getClass() + ": " + ex.getMessage() + ": " + ex.getCause(), ex);
		} finally {
                if (resourceResolver != null && resourceResolver.isLive()) {
                resourceResolver.close();
            }
        }
        	
		logger.info("Out AssetsReplicationPreprocessor.preprocess()");
	}

}

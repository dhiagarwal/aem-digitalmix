package com.dtc.core.models;

public class HybrisUserModel {

	ProspectDto prospectDto;
	String gigyaTimestamp;
	String gigyaSignature;
	String userId;

	public ProspectDto getProspectDto() {
		return prospectDto;
	}

	public void setProspectDto(ProspectDto prospectDto) {
		this.prospectDto = prospectDto;
	}

	public String getGigyaTimestamp() {
		return gigyaTimestamp;
	}

	public void setGigyaTimestamp(String gigyaTimestamp) {
		this.gigyaTimestamp = gigyaTimestamp;
	}

	public String getGigyaSignature() {
		return gigyaSignature;
	}

	public void setGigyaSignature(String gigyaSignature) {
		this.gigyaSignature = gigyaSignature;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}

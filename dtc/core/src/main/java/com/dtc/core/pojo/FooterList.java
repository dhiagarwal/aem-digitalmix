
package com.dtc.core.pojo;

import java.util.ArrayList;
import java.util.List;

public class FooterList {

	private String sectionLabel;
	private String sectionId;

	private List<FooterLink> links = new ArrayList<FooterLink>();

	/**
	 *
	 * @return The sectionLabel
	 */
	public String getSectionLabel() {
		return sectionLabel;
	}

	/**
	 *
	 * @param sectionLabel
	 *            The sectionLabel
	 */
	public void setSectionLabel(String sectionLabel) {
		this.sectionLabel = sectionLabel;
	}

	/**
	 *
	 * @return The links
	 */
	public List<FooterLink> getLinks() {
		return links;
	}

	/**
	 *
	 * @param links
	 *            The Links
	 */
	public void setLinks(List<FooterLink> links) {
		this.links = links;
	}

	public String getSectionId() {
		return sectionId;
	}

	public void setSectionId(String sectionId) {
		this.sectionId = sectionId;
	}

}

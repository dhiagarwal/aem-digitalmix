package com.dtc.core.pojo;

public class Promotion {

	/**
	 * Instantiates Promotion object.
	 */
	public Promotion() {
	}

	private String imageUrl;

	private String header;

	private String content;

	private Link link;

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Link getLink() {
		return link;
	}

	public void setLink(Link link) {
		this.link = link;
	}

}

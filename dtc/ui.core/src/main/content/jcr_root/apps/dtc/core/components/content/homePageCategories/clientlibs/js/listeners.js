(function ($, $document) {
    "use strict";
 
    $(document).on("click", ".cq-dialog-submit", function (e) {
        var isCategoryMultifield = document.getElementsByClassName("category-multifield");
		 var $multifield = $("div.categories-multifield"),
            minLimit = 3,

             count = $multifield.find(".coral-Multifield-input").length;

        if($multifield.length!=0 &&count<3){
             $(window).adaptTo("foundation-ui").alert("Close","please add a minimum of three categories");
		e.stopPropagation();
        e.preventDefault();}


    });


})($, $(document));
"use strict";

var global = this;

use(["/libs/wcm/foundation/components/utils/AuthoringUtils.js",
    "/libs/wcm/foundation/components/utils/ResourceUtils.js",
    "/libs/sightly/js/3rd-party/q.js"], function (AuthoringUtils, ResourceUtils, Q){

 	var customActions = resource.getChild("nvgCustomActions");

    var customComponents = [];

    if(null!=customActions){
		var children = customActions.listChildren();


        for(var index in children){
              var comp = children[index];
        	  var compProperties = comp.adaptTo(Packages.org.apache.sling.api.resource.ValueMap);

        	  var item = {};

      
            item.customButtonLabel = compProperties.get('nvgcustomactionbutlbl');
            item.customButtonActionURL = compProperties.get('nvgcustomactionpath');
            item.index = index;
			customComponents.push(item);
        }
    }

    var slyComponent = {};

    slyComponent.customComponents = customComponents;

    return slyComponent;
});
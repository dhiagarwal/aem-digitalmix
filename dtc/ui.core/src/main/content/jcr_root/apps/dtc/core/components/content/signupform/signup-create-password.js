"use strict";
use(function () {

    var terms = {};
     
    var termsAndUseContentPath = properties.get("termsAndUseContentPath") || '';
    var privacyPolicyContentPath = properties.get("privacyPolicyContentPath") || '';
    var language="";
    var market="";
    var cookies_collection = request.getCookies();
    if (cookies_collection != null) {
    	for (index in cookies_collection) {
    		if (cookies_collection[index].getName().equals("language")) {
           //concat with string to typecast from object , so as we can return as string
    			language = cookies_collection[index].getValue()+"";
    		}
    		if (cookies_collection[index].getName().equals("market")) {
    	           //concat with string to typecast from object , so as we can return as string
    	    	market = cookies_collection[index].getValue()+"";
    	    }
    	}
    }
   
    if(termsAndUseContentPath != ""){
    	terms.termsAndUseContentPath = termsAndUseContentPath+"/"+market+"/termsAndUse_"+language+".html";
    }
    if(privacyPolicyContentPath != ""){
    	terms.privacyPolicyContentPath = privacyPolicyContentPath+"/"+market+"/privacyPolicy_"+language+".html";
    }

    return terms;
});
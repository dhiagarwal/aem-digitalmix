use(function(){
    var primaryLink = properties.get('primarylink');
    primaryLink = modifyURL(primaryLink);
    var secondaryLink = properties.get('secondarylink');
    secondaryLink = modifyURL(secondaryLink);
    return{
        primaryLink : primaryLink,
        secondaryLink : secondaryLink
    };
});
function modifyURL(url){
    if(url){
    	if(url.endsWith('/')){
    		url = url.substring(0,url.length()-1);
    	}
        var isDamPath = url.startsWith('/content/dam');
        var isInternalLink = url.startsWith('/content');
        if(isInternalLink && !isDamPath){
			url = url + '.html';
        }
    }
	return url;
}
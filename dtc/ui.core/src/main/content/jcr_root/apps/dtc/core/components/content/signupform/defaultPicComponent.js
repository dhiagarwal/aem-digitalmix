/*******************************************************************************
* Author id : Deloitte Digital
* File information : Profile Image Path.
*******************************************************************************/

"use strict";

var global = this;

use(["/libs/wcm/foundation/components/utils/AuthoringUtils.js",
    "/libs/wcm/foundation/components/utils/ResourceUtils.js",
    "/libs/sightly/js/3rd-party/q.js"], function (AuthoringUtils, ResourceUtils, Q){

    var CONST = {
    };

    var slyComponent = {};

		

        // Getting the inmage jcr node of the component
		var _imagesResource1 = resource.getChild("profileimage");
		
		if(_imagesResource1!=null && _imagesResource1!=""){	
		   //determine the path of each image from the jcr resource
			var resourceRefImageProperties1 = _imagesResource1.adaptTo(Packages.org.apache.sling.api.resource.ValueMap);
			var damFileReferencePath1 = resourceRefImageProperties1.get('fileReference');
			if(damFileReferencePath1!=null && damFileReferencePath1!="")
			damFileReferencePath1 = damFileReferencePath1.replace(" ","%20");
			
			slyComponent.distributorSetupImage={};
			slyComponent.distributorSetupImage.path=damFileReferencePath1;
			
		}



    return slyComponent;
});
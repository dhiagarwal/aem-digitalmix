(function() {
"use strict";

dtcBannerImageModule.controller('dtcBannerImageController', ['dtcBannerImageService', '$timeout', '$scope', '$q', '$window', 'userProfileManager', function (dtcBannerImageService,$timeout,$scope,$q,$window,userProfileManager){
   
    var self = this;
	console.log('in controller');

    var heroimages = angular.element($('#heroimages')).val();
    console.log('type of heroimages : '+ typeof heroimages);
    console.log('heroimages : '+ heroimages);
    var datas = eval("[" + heroimages + "]")
    console.log('type of datas : '+ typeof datas);
    console.log('datas : '+ datas);

    this.showImageUrl = angular.element($('#defcategoryImage')).val();
    this.categorylabel = angular.element($('#defcategorylabel')).val();
    this.headline = angular.element($('#defheadline')).val();
    this.description = angular.element($('#defdescription')).val();

    function getQueryParam(param) {
        var result =  window.location.search.match(
            new RegExp("(\\?|&)" + param + "(\\[\\])?=([^&]*)")
        );

        return result ? result[3] : false;
    }

    this.inParamVal=getQueryParam("param");
	
    if(!this.inParamVal && getCookie('dtcParam')){
		this.inParamVal = getCookie('dtcParam');
    }
	
    this.init = function(){
        console.log('typeof this.inParamVal : '+typeof this.inParamVal);
        if(this.inParamVal){
            console.log('in init');
            console.log('this.inParamVal : '+this.inParamVal);
            console.log('datas.length : '+datas.length);
    
            for(var i = 0; i < datas.length; i++) {
                var obj = datas[i];
                console.log(obj.categoryText);
                console.log(obj.imageUrl);
                if(this.inParamVal == obj.categoryText){
                    this.showImageUrl = obj.imageUrl;
                    this.categorylabel = obj.categorylabel;
                    this.headline = obj.headline;
                    this.description = obj.description;
                    break;
                }
            }
            console.log('this.showImageUrl : '+this.showImageUrl);
        }
    };


    this.openLink = function(url){
        console.log('url : '+url);
        if(url.indexOf('http') != 0 && url.indexOf('.html') == -1){
            url = url + '.html';
        }
        $window.open(url, "_self");
    };


}])

}());
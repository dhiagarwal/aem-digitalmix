(function() {
	"use strict";


	commerceModule.service('analyticsService', ['$cookies','userProfileManager',function($cookies,userProfileManager) {
		
		this.updateProductStringOnAddToBag = function (entry) {
			/* Here the entry will always be an Array with one input. 
			Hence there is no need of a iteration arr[0] will work*/

			var skuId = entry.cartModifications[0].entry.product.code;
			var quantity = entry.cartModifications[0].entry.quantity;
			var totalPrice = entry.cartModifications[0].entry.totalPrice.value;
			var productString = $cookies.get('productString');

			if(productString==undefined || productString=='' && productString==null)
			{
				productString='';
			}
			var productStringEntries = productString.split(',');
			var entryPresent = false;
			for(var i=0;i<productStringEntries.length;i++)
			{
				if(productStringEntries[i].indexOf(skuId)!=-1)
				{
					var array=productStringEntries[i].split(';');
					array[2]=quantity;
					array[3]=totalPrice;
					productStringEntries[i] = array.join(';');
					entryPresent=true;
					break;
				}			
			}
			if(entryPresent)
			{
				productString = productStringEntries.join();
			}
			else
			{
				if(productString.length==0)
				{
					productString= productString+';'+skuId+';'+quantity+';'+totalPrice;
				}
				else
				{
					productString= productString+',;'+skuId+';'+quantity+';'+totalPrice;
				}
			}			
			updateCookie(productString);
			return ;
		}

		this.updateProductString = function(entries){
			//Category;Product;Quantity;Price
			var productString='';
			if(entries){
				for(var index=0;index<entries.length;index++)
				{
					if(index!=0)
					{
						productString=productString+',';
					}
					productString=productString+';'+entries[index].product.code+';'+entries[index].quantity+';'+entries[index].totalPrice.value;
				}
				updateCookie(productString);				
			}
			else{				
				$cookies.remove('productString',{path: '/' });				
			}	
		}
		
		function updateCookie (productString){
			if(userProfileManager.isUserLoggedIn())
			{
				$cookies.put('productString',productString,{path: '/' });
			}
			else
			{
				var expDate = new Date();
				expDate.setDate(expDate.getDate() + 1);
				$cookies.put('productString',productString,{path: '/',expires: expDate });
			}			
		}
		
		this.clearProductString = function(){
			$cookies.remove('productString',{path: '/' });			
		}
	}]);
}());
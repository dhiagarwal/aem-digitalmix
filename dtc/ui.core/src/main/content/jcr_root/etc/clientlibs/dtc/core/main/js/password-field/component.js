'use strict';

passwordFieldController.$inject = ['$scope', '$sce'];
passwordFieldModule.directive('passwordField', function () {

    return {
        restrict: 'E',
        scope: {
            credentials: '=',
            isRequired: '<',
            isSubmitted: '<',
        },
        templateUrl: '/etc/clientlibs/dtc/core/main/js/password-field/password-field.html',
        controller: passwordFieldController,
        controllerAs: 'ctrl',
        bindToController: true
    }
})
function passwordFieldController($scope, $sce) {
    var self = this;
    this.name = 'passwordField';
    this.credentials = this.credentials || {};
    this.isRequired = this.isRequired || false;
    this.passwordempty= false;
    this.signInPasswordLabel = $('password-field-values').data('sign-in-password-label');
    this.signInPasswordMandatoryLabel = $('password-field-values').data('sign-in-password-mandatory-field');
    this.credentials.passwordImage = {
        "text": "/etc/clientlibs/dtc/core/main/css/src/assets/images/css/ic-password-show.png",
        "mask": "/etc/clientlibs/dtc/core/main/css/src/assets/images/css/ic-password-hide.png"
    };
    this.credentials.enterPasswordState = 'password';

    this.changeImage = function () {
        this.credentials.enterPasswordState = this.credentials.enterPasswordState === 'text' ? 'password' : 'text';
    };

    this.enterpassword =function(value){
        if(value){
            this.passwordempty= false;
        } else {
            this.passwordempty = true;
        }
    }

    this.enterpassword(this.credentials.enterPassword);
};
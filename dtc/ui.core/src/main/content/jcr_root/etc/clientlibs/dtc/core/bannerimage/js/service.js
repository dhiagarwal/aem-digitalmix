(function() {
"use strict";

dtcBannerImageModule.service('dtcBannerImageService', ['$http', '$q', function($http, $q) {

    var service = this;

    this.getAssetDetail = function(assetId) {
        console.log('assetId in service : '+assetId);
        var url = 'http://10.118.44.206:4503/bin/dtc/getAssetDetail?assetId=' + assetId;
		var deferred = $q.defer();
    	$http.get(url).then(function(status) {
			deferred.resolve(status);
    	});
    	return deferred.promise;
  	}


}])
}());
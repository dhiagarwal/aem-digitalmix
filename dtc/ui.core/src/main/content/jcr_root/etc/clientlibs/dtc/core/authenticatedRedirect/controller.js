(function() {
"use strict";

	var app = angular.module('meCommerce');
	app.controller('authRedirectCtrl', ['userProfileManager', function(userProfileManager) {
	
	    function redirectUser() {
	    	if(!(userProfileManager.isUserLoggedIn())){
	            if(angular.element('input[name=authenticatedRedirectPage]').val() != undefined && angular.element('input[name=authenticatedRedirectPage]').val() != ""){
	                window.location.href = angular.element('input[name=authenticatedRedirectPage]').val() + ".html?redirectTo=" + window.location.pathname;
	            }
	            else if(angular.element('#nvgsigninpath').val() != undefined && angular.element('#nvgsigninpath').val() != ""){
	                window.location.href = angular.element('#nvgsigninpath').val() + ".html?redirectTo=" + window.location.pathname;
	            }
	    	}    	
	    }
		
		function init() {
			redirectUser();
	    }
	    init();
	
	}]);
	
}());

'use strict';

var formUtilsModule = angular.module('formUtilsModule', []);

formUtilsModule.filter('validMonths', function () {
    return function (months, year) {
        var filtered = [];
		var now = new Date();
		var over18Month = now.getUTCMonth() + 1;
		var over18Year = now.getUTCFullYear() - 18;
		if(year != ""){
			if(year == over18Year){
				angular.forEach(months, function (month) {
					if (month.id <= over18Month) {
						filtered.push(month);
					}
				});
			}
			else{
				angular.forEach(months, function (month) {
						filtered.push(month);
				});
			}
		}
        return filtered;
    };
});
formUtilsModule.filter('daysInMonth', function () {
    return function (days, year, month) {
        var filtered = [];
				angular.forEach(days, function (day) {
					if (month != ""){
						if (month.id == 1 || month.id == 3 || month.id == 5 || month.id == 7 || month.id == 8 || month.id == 10 || month.id == 12) {
							filtered.push(day);
						}
						else if ((month.id == 4 || month.id == 6 || month.id == 9 || month.id == 11) && day.id <= 30){
							filtered.push(day);
						}
						else if (month.id == 2){
							if (year % 4 == 0 && day.id <= 29){
								filtered.push(day);
							}
							else if (day.id <= 28){
								filtered.push(day);
							}
						}
					}
				});
        return filtered;
    };
});
formUtilsModule.filter('validDays', function () {
    return function (days, year, month) {
        var filtered = [];
		var now = new Date();
		var over18Day = now.getUTCDate();
		var over18Month = now.getUTCMonth() + 1;
		var over18Year = now.getUTCFullYear() - 18;
		if(year == over18Year && month.id == over18Month){
			angular.forEach(days, function (day) {
				if (day.id <= over18Day) {
					filtered.push(day);
				}
			});
		}
		else{
			angular.forEach(days, function (day) {
					filtered.push(day);
			});
		}
        return filtered;
    };
});

formUtilsModule.directive('allowPattern', function() {
    return {
        compile: function (tElement, tAttrs) {
            return function (scope, element, attrs) {
                // handle key events
                element.bind("keypress", function (event) {
                    var keyCode = event.which || event.keyCode; // get the keyCode pressed from the event.
                    var keyCodeChar = String.fromCharCode(keyCode); //determine the char from the keyCode.
                    var specialChar = /^[\\\"\'\;\:\>\|\[\]\-_=+~`!€£¥₹₹@#\$%^<.,?{}/&*\(\)]$/i;
                    if (keyCodeChar) {
                        if (tAttrs.allowPattern) {
                            switch (tAttrs.allowPattern) {
                                case 'number-only':
                                    if ((keyCode >= 65 && keyCode <= 91) || (keyCode >= 97 && keyCode <= 122)) { /* Restrict Alphabets */
                                        return false;
                                    }
                                    break;
                                case 'alphabets-only':
                                    if (keyCode >= 48 && keyCode <= 57) { /* Restrict Numbers */
                                        return false;
                                    }
                                    break;
                            }
                        }
                        return !specialChar.test(keyCodeChar); /* Restrict Special Characters */
                    }
                });
            };
        }
    };
});

formUtilsModule.directive('allowData', function () {
    return {
        require: 'ngModel',
        link: function link(scope, elem, attrs, ngModel) {
            ngModel.$parsers.push(function (viewValue) {
                var reg = null;
                switch(attrs.allowData) {
                    case 'numbers-only':
                    reg = /^[^`~!€£¥₹₹@#$%\^&*()_+={}|[\]\\:';"<>?,./A-Za-z/-]*$/;
                    break;
                    case 'alphabets-only':
                    reg = /^[^`~!€£¥₹₹@#$%\^&*()_+={}|[\]\\:';"<>?,./0-9/-]*$/;
                    break;
                    default:
                    reg = /^[^`~!€£¥₹₹@#$%\^&*()_+={}|[\]\\:';"<>?,./]*$/;
                    break;
                }
                if (viewValue.match(reg)) {
                    return viewValue;
                }
                var transformedValue = ngModel.$modelValue;
                ngModel.$setViewValue(transformedValue);
                ngModel.$render();
                return transformedValue;
            });
        }
    }
});

formUtilsModule.directive('addMinWidth', function($window) {
    return {
        link: function (scope, ele, attr) {
            var viewPortWidth = angular.element($window).width();
            if (viewPortWidth && viewPortWidth < 768) {
                ele.css({ 'min-width': (viewPortWidth / 16) + 'rem' });
            }
        }
    }
});

formUtilsModule.directive('selectOnClick', function () {
    // Linker function
    return function (scope, element, attrs) {
      element.on('click', function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        this.setSelectionRange(0, 9999);
      });
    };
});

formUtilsModule.filter("ssnCheck", function() {
        return function (value) {
            if (value) {
                if (value.length >= 9) {
                    return value.substr(0, 3) + "-" + value.substr(3, 2) + "-" + value.substr(5);
                } else if (value.length >= 5) {
                    return value.substr(0, 3) + "-" + value.substr(3, 2) + "-" + value.substr(5);
                } else if (value.length >= 3) {
                    return value.substr(0, 3) + "-" + value.substr(3);
                }

            }
            return value;
        };
    });



formUtilsModule.directive('zipMask', function(){
 return {
        restrict: 'A',
        require: 'ngModel',
        scope: {       
            options: '=',       
            model: '=ngModel'     
        },
         

        link: function(scope, element, attrs, ngModelCtrl) {           
            scope.$watch("model", function() {               
                linker(scope.model, element, ngModelCtrl);          
            }); 

            element.bind("keypress keyup touchstart", function(event) {
                var keyCode = event.which || event.keyCode; // get the keyCode pressed from the event.
                var keyCodeChar = String.fromCharCode(keyCode); //determine the char from the keyCode.

                if (navigator.userAgent.match('Android')) {
                    var zipVal = this.value;
                    var keyCodeCharAndroid = this.value.charCodeAt(zipVal.length - 1);

                    if ((keyCodeCharAndroid >= 65 && keyCodeCharAndroid <= 91) || (keyCodeCharAndroid >= 97 && keyCodeCharAndroid <= 122) || keyCodeCharAndroid == 32) { /* Restrict Alphabets including both lowercase and uppercase and white space */
                        this.value = zipVal.replace(/\D/g, '');
                        this.value = zipVal.replace(/\s/g, '');
                        return false;
                    }
                    var charSpecial = String.fromCharCode(keyCodeCharAndroid); //Android
                    var specialRegex = /[\\\"\'\;\:\>\|\[\]\-_=+~`!€£¥₹₹@#\$%^<.,?{}/&*\(\)]$/i;
                    var isValid = specialRegex.test(charSpecial);
                    if (isValid) {
                        this.value = zipVal.replace(/[\\\"\'\;\:\>\|\[\]\-_=+~`!€£¥₹₹@#\$%^<.,?{}/&*\(\)]$/i, '');
                        return false;
                    }
                } else {
                    if (keyCodeChar) {
                        if ((keyCode >= 65 && keyCode <= 91) || (keyCode >= 97 && keyCode <= 122) || keyCode == 32) { /* Restrict Alphabets including both lowercase and uppercase */
                            return false;
                        }
                        return !/^[\\\"\'\;\:\>\|\[\]\-_=+~`!€£¥₹₹@#\$%^<.,?{}/&*\(\)]$/i.test(keyCodeChar); /* Restrict Special Characters except '-' */
                    }
                }

            });
            ngModelCtrl.$parsers.unshift(function(viewValue) {
                return linker(viewValue, element, ngModelCtrl);


            });
        }
    };

    function linker(viewValue, element, ngModelCtrl) {
        viewValue = viewValue ? viewValue : element.val();
        viewValue = viewValue.replace(/\D/g, ''); //Remove alphabets
        viewValue = viewValue.replace(/[\\\"\'\;\:\>\|\[\]\-_=+~`!€£¥₹₹@#\$%^<.,?{}/&*\(\)]$/i, ''); //Remove special symbols
        viewValue = viewValue.replace(/\s/g, ''); //Remove space

        if (viewValue && viewValue.length >= 10) { /* Restrict on entering more than 10 characters */
            var updatedValue = viewValue;
            updatedValue = viewValue.substr(0, 10);
            element.val(updatedValue);
        } else { /* Add '-' when the Zipcode passes 5 characters */
            var isValidZipcode = viewValue && (viewValue.length != 5 && viewValue.length != 10);
            var updatedValue = viewValue;
            if (viewValue && viewValue.length > 5 && viewValue.indexOf('-') < 0) {
                updatedValue = viewValue.slice(0, 5) + "-" + viewValue.slice(5);
                viewValue = updatedValue;
            }
            element.val(updatedValue);


            var tmppp = element.val();
            var valLength = tmppp.length;
            element.val(tmppp);

            //Resolving the focus shift issue in Android
            setTimeout((function(element, strLength) {
                return function() {
                    if (element.setSelectionRange !== undefined) {
                        element.setSelectionRange(strLength, strLength);
                        element.blur();
                    } else {
                        $(element).val(element.val());

                    }
                }
            }(element, valLength)), 5);
        }
        if (viewValue && viewValue.length == 6) { /* Remove the '-' on delete */
            var updatedValue = viewValue;
            if (viewValue.indexOf('-') >= 0) {
                updatedValue = viewValue.substr(0, viewValue.indexOf('-'));
                viewValue = updatedValue;
                element.val(updatedValue);
            }
        }
        var isZipCodeValid = viewValue && (viewValue.length == 5 || viewValue.length == 10);
        if (isZipCodeValid == "") {
            isZipCodeValid = false;
        }
        ngModelCtrl.$setValidity('isZipCodeValid', isZipCodeValid);
        return viewValue;
    }

});

formUtilsModule.directive('fieldspinner', function() {
    return {
        restrict: 'A',
        require: 'ngModel',
        scope: {
            display: '='
        },
        link: function(scope, elem, attrs, ctrl) {
            scope.$watch('display', function() {
                if (scope.display) {
                    elem.addClass('loading-spinner');
                    angular.element(document).find("img#passwordImage").css('display', 'none');
                } else if (scope.display == false) {
                    elem.removeClass('loading-spinner');
                    angular.element(document).find("img#passwordImage").css('display', 'inline-block');
                }
            })
        }
    }
});

formUtilsModule.directive('equiHeight', function ($timeout) {
    return function (scope, element, attrs) {
        $timeout(function(){
            var allButtons = angular.element(element).find('.parent-equi-height .equi-height');
            var tempHeight=0;
            angular.forEach(allButtons, function(k, v){
                tempHeight = tempHeight < allButtons[v].offsetHeight ? allButtons[v].offsetHeight : tempHeight;
            });
            angular.element(element).find('.parent-equi-height .equi-height').css('height',tempHeight/16+'rem');
            
        }, 0);
       
    }
});
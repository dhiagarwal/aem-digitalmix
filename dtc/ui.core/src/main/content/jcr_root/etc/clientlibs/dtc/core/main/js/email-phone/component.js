'use strict';

emailPhoneController.$inject = ['$scope','$timeout', '$rootScope'];
emailPhoneModule.directive('emailPhone',function(){
	return{
		restrict: 'E',
        scope: {
            credentials: '=',
            title: '@',
            isEmailOrPhone: '<',
            isEmail:'<',
            isPhone:'<',
            isRequired: '=',
            isSubmitted: '<',
            isEmailDuplicate: '=',
            isFromPersonal: '<',
			profileEmail:'=',
            isSkipBlurValidation: '<'
        },
	    templateUrl : '/etc/clientlibs/dtc/core/main/js/email-phone/email-phone.html',
	    controller: emailPhoneController,
	    controllerAs: 'ctrl',
	    bindToController : true
	}
})

function emailPhoneController($scope, $timeout, $rootScope) {
    var vmp = this;
    this.emailDuplicatedError = angular.element('#emailDuplicateError') ? angular.element('#emailDuplicateError').val() : '';
    this.credentials = this.credentials || {};
    this.isRequired = this.isRequired || false;
    this.isSubmitted = this.isSubmitted || false;
    this.credentials.userDetailsField = this.credentials.userDetailsField || "";
    this.credentials.userEmailField = this.credentials.userEmailField || "";
    this.credentials.userPhoneField = this.credentials.userPhoneField || "";
    this.credentials.emptyEmailError = this.credentials.emptyEmailError || true;
    this.isSkipBlurValidation = this.isSkipBlurValidation || false;
    this.displayFieldSpinner = false;
    this.result = {};
    this.name = 'email.phone';
    this.maskFormat = '';
    this.maskOptions = {
        allowInvalidValue: true,
        clearOnBlur: false
    };

    this.patterns = {
        email: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        phone: /^[1-9]{1}[0-9]{9}$/
    };


    this.onChange = function(form, flag) {
        if(this.isSkipBlurValidation) {
            this.isRequired = false;
            return;
        }
        var formField;
        if( this.isPhone ){
            this.maskFormat = '(999) 999-9999';
            formField = form.userPhoneField;
        } else {
            if(this.isEmail){
                formField = form.userEmailField;
            }else{
                formField = form.userDetailField;
            }
            var modelValue = formField.$modelValue;
            if(!modelValue) {
                this.maskFormat = '';
            }

            if( modelValue && modelValue.length == 1 ) {
                if(!isNaN(modelValue)){
                    this.maskFormat = '(999) 999-9999';
                }
            }
        }

        this.credentials.emptyEmailError = false;

        var isPhone, value;
        if(this.isPhone){
            value = this.credentials.userPhoneField;
        } else if (this.isEmail){
            value = this.credentials.userEmailField;
        } else {
            value = this.credentials.userDetailsField;
        }
        if (!value) {
            this.credentials.emptyEmailError = this.isRequired ? true : false;
            $rootScope.$emit('eventIsDuplicate', this.isRequired);
            return;
        }

        value = value.replace(/[\-()]/g, '');
        var isPhoneNumber = this.patterns.phone.test(value);
        if(isPhoneNumber && value.length == 10) {
            return false;
        }

        if(!this.isEmail) {
            if(value.length > 3 && !isNaN(value)) {
                isPhone = true;
            }
        }


        if(isPhone) {
            var telephoneValue = value.replace(/[^0-9]/g, '').slice(0,10);
            var city, remainingNumber;

            switch (telephoneValue.length) {
                case 1:
                case 2:
                case 3:
                    city = telephoneValue;


                default:
                    city = "(" + telephoneValue.slice(0, 3) + ")";
                    remainingNumber = telephoneValue.slice(3);
            }

            if(remainingNumber) {
                if(remainingNumber.length < 7){
                }
                if(remainingNumber.length>3){
                    remainingNumber = remainingNumber.slice(0, 3) + "-" + remainingNumber.slice(3,7);
                }
                else{
                    remainingNumber = remainingNumber;
                }

                value =  ( city  + remainingNumber).trim();
            }
            else{
                return "(" + city;
            }
            this.credentials.isPhoneNumber = true;
        } else {
            if(this.isPhone) {
                this.credentials.isPhoneNumber = true;
            } else {
                this.credentials.isPhoneNumber = false;
                if(flag) {
                    var isEmailValid = this.patterns.email.test(value);
                }
            }
        }

        /*if(!isPhone){
            form.$$parentForm.$setValidity("cemail", false);
        } else {
            form.$$parentForm.$setValidity("cemail", true);
        }*/
    };

    this.checkUserName = function(form){
        if(vmp.isFromPersonal && this.profileEmail != this.credentials.userEmailField){
            vmp.displayFieldSpinner = true;
            gigya.accounts.isAvailableLoginID({
                loginID : this.credentials.userEmailField,
                callback : function(response){
                	$timeout(function(){
                		$scope.$apply(function() {
                    	vmp.displayFieldSpinner = false;
                       if(!response.isAvailable) {
                           vmp.isEmailDuplicate = true;
                       	}	
                		});
                	},0);
                 }
            });
        }
    }
}
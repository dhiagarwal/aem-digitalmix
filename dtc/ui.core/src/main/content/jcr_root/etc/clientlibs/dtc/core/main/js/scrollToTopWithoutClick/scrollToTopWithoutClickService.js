'use strict';

scrollToTopWithoutClickModule.service('scrollToTopWithoutClickService', ['$http', '$q', function ($http,$q){
	 this.$http = $http;
	    this.$q = $q;

        this.scrollToTop = function (selector, scrollTopPosition, scrollSpeed) {
        var selector = selector ? selector : "html, body";
        var scrollTopPosition = scrollTopPosition ? scrollTopPosition : 0;
        var scrollSpeed = scrollSpeed ? scrollSpeed : 400;    
        angular.element(selector).animate({
            scrollTop: scrollTopPosition
        }, scrollSpeed);
    };




}])
var app = angular.module('meCommerce');
app.provider("userProfileManager", function() {
 	var JWT_KEY = 'USER_PROFILE_JWT';
    var USER_PROFILE_MIN__KEY = 'USER_PROFILE_MIN_INFO';
    var USER_PROFILE_MASTER__KEY = 'USER_PROFILE_MASTER_INFO';
    var RETURN_URL_KEY = 'PAGE_RETURN_URL';
    var SPONSOR_RETURN_URL_KEY = 'SPONSOR_RETURN_URL_KEY';
    var INVITE_URL_KEY = 'INVITE_RETURN_URL';
    var SIGNUP_USER_TYPE = 'SGUP_ROLE_USER';
    var GIGYA_DETAILS_KEY = 'GIGYA_DETAILS';
    var CUST_TO_IND_DIST_RET_PAGE = 'CUST_TO_IND_DIST_RET_PAGE';
    var HOST_INFO = 'HOST_INFO';
    var GUID = 'GUID';
    var LANGUAGE='language';
    var MARKET='market';

    return {
    $get : function() {
    return {
        determinUserType: function (userData){
            var userType = null;
            if(userData['customerClass']){
                if(userData['customerClass'] === CUST_CLASS.RETAIL){
                    userType = USER_TYPE.RETAIL_CUSTOMER
                }else if(userData['customerClass'] === CUST_CLASS.PMPC){
                    userType = USER_TYPE.PREFERED_CUSTOMER
                }else if(userData['customerClass'] === CUST_CLASS.PERMANENT){
                    if(userData['userRole'] === USER_ROLE.PRIMARY){
                        userType = USER_TYPE.PRIMARY_DISTRIBUTOR;
                    }else if(userData['userRole'] === USER_ROLE.ASSOCIATE){
                        userType = USER_TYPE.ASSOCIATE;
                    }else if(userData['userRole'] === USER_ROLE.FAMILY_MEMBER){
                        userType = USER_TYPE.FAMILY_MEMBER;
                    }
                }
            }
            return userType;
        },
		setUserProfile:function(metaData, gigyaObj){
             	var jwt =  metaData.jwtToken;
				var userProfileMinInfo =  metaData.profileDto;
				metaData.userData['userType'] = this.determinUserType(metaData.userData);
             	var userProfileMasterInfo =  metaData.userData;
            	setCookie(GIGYA_DETAILS_KEY, JSON.stringify(gigyaObj));
               	setCookie(JWT_KEY, JSON.stringify(jwt));
            	if(userProfileMinInfo.profileUserInfo!=null || userProfileMinInfo.profileUserInfo!="" || userProfileMinInfo.profileUserInfo!=undefined){
                   	userProfileMinInfo.profileUserInfo = ""; //Avoiding profileUerInfo Data to store in Cookies.
                   	}
                setCookie(USER_PROFILE_MIN__KEY, JSON.stringify(userProfileMinInfo));
                setCookie(USER_PROFILE_MASTER__KEY, JSON.stringify(userProfileMasterInfo));
        },
        deleteUserProfile:function(){
            	 setCookie(JWT_KEY, "", -1);
               	 setCookie(USER_PROFILE_MIN__KEY, "", -1);
                 setCookie(USER_PROFILE_MASTER__KEY, "", -1);
                 setCookie(RETURN_URL_KEY, "", -1);
             	 setCookie(SIGNUP_USER_TYPE, "", -1);
                 setCookie(GIGYA_DETAILS_KEY, "", -1);
            	 setCookie(CUST_TO_IND_DIST_RET_PAGE, "", -1);
            	 setCookie(GUID, "", -1);
            	 setCookie(HOST_INFO, "", -1); 
        },
        getJWT : function(){
            var jwt = getCookie(JWT_KEY);
            if(null != jwt && undefined != jwt && 0 < jwt.length){
				return JSON.parse(getCookie(JWT_KEY));
           	}else{
                return null;
           	}

        },
        getUserProfileDetailInfo : function(){
             var data = getCookie(USER_PROFILE_MIN__KEY);
            if(null != data && undefined != data && 0 < data.length){
                 return JSON.parse(getCookie(USER_PROFILE_MIN__KEY));
            }else{
                return null;
           	}

        },
        setUserProfileDetailInfo : function(userProfileMinInfo){
            setCookie(USER_PROFILE_MIN__KEY, JSON.stringify(userProfileMinInfo));
        },
        getGigyaDetails : function(){
             var data = getCookie(GIGYA_DETAILS_KEY);
            if(null != data && undefined != data && 0 < data.length){
                 return JSON.parse(getCookie(GIGYA_DETAILS_KEY));
            }else{
                return null;
           	}

        },
        setGigyaDetails : function(gigyaDetails){
            setCookie(GIGYA_DETAILS_KEY, JSON.stringify(gigyaDetails));
        },
        getUserProfileMasterInfo : function(){
              var data = getCookie(USER_PROFILE_MASTER__KEY);
            if(null != data && undefined != data && 0 < data.length){
                 return JSON.parse(getCookie(USER_PROFILE_MASTER__KEY));
            }else{
                return null;
           	}

        },
        setUserProfileMasterInfo : function(userProfileMasterInfo){
            setCookie(USER_PROFILE_MASTER__KEY, JSON.stringify(userProfileMasterInfo));
        },
        getHostInfo : function(){
            var data = getCookie(HOST_INFO);
          if(null != data && undefined != data && 0 < data.length){
               return JSON.parse(getCookie(HOST_INFO));
          }else{
              return null;
         	}

        },
        isUserLoggedIn : function(){
			var jwt = this.getJWT();
			if(null != jwt && undefined != jwt && 0 < jwt.length && this.isSignInMandatoryCookiesPresent()){
                return true;
            }
            return false;
        },
        setPageReturnURL : function(url){
             setCookie(RETURN_URL_KEY, JSON.stringify(url));
        },
        getPageReturnURL : function(){
             var data = getCookie(RETURN_URL_KEY);
             if(null != data && undefined != data && 0 < data.length){
                 return JSON.parse(getCookie(RETURN_URL_KEY));
             }else{
                return null;
           	 }
        },
        setInviteUrl : function(url){
            setCookie(INVITE_URL_KEY, JSON.stringify(url));
       },
       getInviteUrl : function(){
            var data = getCookie(INVITE_URL_KEY);
            if(null != data && undefined != data && 0 < data.length){
                return JSON.parse(getCookie(INVITE_URL_KEY));
            }else{
               return null;
          	 }
       },
        setCustomerConvertionReturnURL : function(url){
             setCookie(CUST_TO_IND_DIST_RET_PAGE, JSON.stringify(url));
        },
        getCustomerConvertionReturnURL : function(){
             var data = getCookie(CUST_TO_IND_DIST_RET_PAGE);
             if(null != data && undefined != data && 0 < data.length){
                 return JSON.parse(getCookie(CUST_TO_IND_DIST_RET_PAGE));
             }else{
                return null;
           	 }
        },
        setSignUpUserType: function(role){
            setCookie(SIGNUP_USER_TYPE, role);
        },
        getSignUpUserType: function(){
             var data = getCookie(SIGNUP_USER_TYPE);
             if(null != data && undefined != data && 0 < data.length){
                 return getCookie(SIGNUP_USER_TYPE);
             }else{
                return null;
           	 }
        },
        setGuid: function(guid){
            setCookie(GUID, guid);
        },
        getGuid: function(){
             var data = getCookie(GUID);
             if(null != data && undefined != data && 0 < data.length){
                 return getCookie(GUID);
             }else{
                return null;
           	 }
        },
       setSponsorPageReturnURL : function(url){
            setCookie(SPONSOR_RETURN_URL_KEY, JSON.stringify(url));
       },
       getSponsorPageReturnURL : function(){
            var data = getCookie(SPONSOR_RETURN_URL_KEY);
            if(null != data && undefined != data && 0 < data.length){
                return JSON.parse(getCookie(SPONSOR_RETURN_URL_KEY));
            }else{
               return null;
          	 }
       },
       setPageLocaleVal: function(data){
           setCookie(LANGUAGE, data.language);
           setCookie(MARKET, data.market);
       },
       getPageLocaleVal: function(data){
           var languageVal= getCookie(LANGUAGE);
           var marketVal=getCookie(MARKET);
			if(null != languageVal && undefined != languageVal && 0 < languageVal.length && null != marketVal && undefined != marketVal && 0 < marketVal.length){
               return {
                   language: getCookie(LANGUAGE),
                   market: getCookie(MARKET)
               };
            }else{
               return null;
          	 }
       },

       getJWTKey: function(){
           return JWT_KEY;
       },

       deleteUserProfileCookiesAndSignOut: function(actionDesc,index,isOpenInNewWindow){
           var URL = angular.element($('#'+actionDesc+index)).val();
               userProfileManager.deleteUserProfile();
               setCookie('cart_guid',"",-1);
               gigya.accounts.logout({
                 forceProvidersLogout : true
               });

           if(undefined != URL && null != URL && '' != URL){
               if(undefined != isOpenInNewWindow && isOpenInNewWindow){
                   window.open(URL);
               }else{
                   window.location = window.location.protocol+'//'+window.location.host+ URL + '.html';
               }
			  }
		},
      isMandatoryInfoAvailable: function(hybrisResponse){

			var isMandatoryInfoPresent = false;
			var userData = hybrisResponse.userData;

			if(this.isAccountInfoMissing(userData) && this.isUserInfoMissing(userData)){

				isMandatoryInfoPresent = true;
			}

			return isMandatoryInfoPresent;

		},

		isAccountInfoMissing: function(userData){

			if(null != userData.accountId && undefined !=userData.accountId && null != userData.emailId && undefined !=userData.emailId && userData.alphaAccount ){

				return true;
			}

			return false;

		},

		isUserInfoMissing: function(userData){

			if(null != userData.userId && undefined !=userData.userId && null != userData.userRole && undefined !=userData.userRole && null != userData['customerClass'] && undefined !=userData['customerClass'] ){

				return true;
			}

			return false;

		},
		getProfileImageReference :  function (){
			var profileImageRef = "";
			profileImageRef = angular.element($("#profileImageReferencePath")).val();
			return profileImageRef;

		},
		isSignInMandatoryCookiesPresent :  function (){

			var gigyaDetails = this.getGigyaDetails();
			var userProfileMaster = this.getUserProfileMasterInfo();
			var userProfileMiniInfo = this.getUserProfileDetailInfo();

			if("" != gigyaDetails && undefined !=gigyaDetails && null!=gigyaDetails && "" != userProfileMaster && undefined !=userProfileMaster && null!=userProfileMaster && "" != userProfileMiniInfo && undefined !=userProfileMiniInfo && null!=userProfileMiniInfo){

				return true;
			}
			else{
				return false;
			}

		}
    }
    }
    }
});
var USER_ROLE = {
    PRIMARY : 'Primary',
    ASSOCIATE : 'Associate',
    FAMILY_MEMBER : 'FamilyMember'
};
var USER_TYPE = {
    RETAIL_CUSTOMER : 'Retail_Customer',
    PREFERED_CUSTOMER : 'Prefered_Customer',
    PRIMARY_DISTRIBUTOR : 'Primary_Distributor',
    ASSOCIATE : 'Associate',
    FAMILY_MEMBER : 'FamilyMember'
};
var CUST_CLASS = {
    RETAIL : 'Retail',
    PMPC : 'PMPC',
    PERMANENT : 'Permanent'
};
var INVITE_STATUS = {
	    PENDING : 'ACCOUNT PENDING',
	    COMPLETE : 'Complete'
};
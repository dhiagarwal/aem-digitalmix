'use strict';

pageLevelErrorModule.directive('pageLevelErrorComponent',function(){

	return {
		restrict: 'E',
		scope: {
			  errorMessage: '=',
		      dangerType: '=',
		      successMessage: '=',
		      successType: '=',
		      isDisplayErrorMessage: '=',
		      isShowInfoIconOnError: '=',
		      successDescription: '='
		    },
	    templateUrl: '/etc/clientlibs/dtc/core/main/js/page-level-error-component/page-level-error-component.html',
	    controller: pageLevelErrorController,
	    controllerAs: 'ctrl',
	    bindToController : true
	}
})
function pageLevelErrorController(){
	/*var self = this;
    this.hideMessage = true;
    this.dangerAlert = "danger";
    console.log("Error Message is "+this.errorMessage);
    console.log("Success Message is "+ this.successMessage);
    this.hideToastMessage = function(){
      this.hideMessage = false;
    }*/
	var self = this;
    this.dangerAlert = "danger";
    self.modifyDisplayErrorMessage = function() {
        if(!self.isShowInfoIconOnError) {
            self.isDisplayErrorMessage = !self.isDisplayErrorMessage;
        }
    };
};

  	


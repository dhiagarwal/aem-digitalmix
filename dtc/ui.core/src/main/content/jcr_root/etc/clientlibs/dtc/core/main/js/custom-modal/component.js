'use strict';

customModalController.$inject = ['$scope', '$uibModal', '$sce'];
customModalModule.directive('customModalComponent',function(){
	 
	return {
        templateUrl: '/etc/clientlibs/dtc/core/main/js/custom-modal/custom-modal.html',
		restrict: 'E',
		scope:{
    	isModalCall: '@',
        srcUrl: '@',
		callbackOnSuccess: '@',
		modalHeading: '@',
		back: '@',
		goBack: '@'
    },
    controller: customModalController,
	controllerAs: 'ctrl',
        bindToController : true
	}
});

function customModalController($scope, $uibModal, $sce) {
    this.$uibModal = $uibModal;
    var self = this;
    self.modalPopupInstance = function(modalTemplateVariable, controller, windowClass) {
        var modalInstance = $uibModal.open({
            animation: true,
            backdrop: 'static',
            keyboard: false,
            templateUrl: modalTemplateVariable,
            controller: ['$uibModalInstance', function($uibModalInstance) {
                var modalSelf = this;
                modalSelf.model = {};

                modalSelf.model.addressList = self.addressList;

                modalSelf.addressData = self.addressData;

                modalSelf.srcUrl = self.srcUrl;
                
                modalSelf.back = self.back;
                
                modalSelf.goBack = self.goBack;

                modalSelf.modalHeading = self.modalHeading;

                modalSelf.ok = function() {
                    $uibModalInstance.close();
                };

                modalSelf.trustUrl = function(url) {
                    return $sce.trustAsResourceUrl(url);
                }

                modalSelf.cancel = function() {
                    $uibModalInstance.dismiss();
                };

                modalSelf.goBackAndEdit = function() {
                    modalSelf.cancel();
                    var resetBooleanVariable = '$scope.$parent.' + self.callbackOnSuccess + '()';
                    eval(resetBooleanVariable);
                };

            }],
            controllerAs: 'ctrl',
            windowClass: windowClass,
            size: 'md'
        });
    };

    self.trustUrl = function(url) {
        return $sce.trustAsResourceUrl(url);
    }

    if (self.isModalCall) {
        self.modalPopupInstance("/etc/clientlibs/dtc/core/main/js/custom-modal/custom-modal.html", self.customModalController, 'custom-modal-popup');
    }

};
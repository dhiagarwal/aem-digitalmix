var sessionStoreManager = (function() {
 	var SESSION_STORY_USER_PROFILE_KEY = 'userProfile';
    return {
        populateUserProfile:function (metaData){

            	var profile = getCookie(SESSION_STORY_USER_PROFILE_KEY);
            
                if(null != profile && undefined != profile){
					deleteCookie(SESSION_STORY_USER_PROFILE_KEY);
                }

                var profileData =  metaData.user;

            	setCookie(SESSION_STORY_USER_PROFILE_KEY, JSON.stringify(profileData));
        },
        getUserProfile : function (){
            return JSON.parse(getCookie(SESSION_STORY_USER_PROFILE_KEY));
        },
        removeUserProfile : function (){
            if(null != profile){
                deleteCookie(SESSION_STORY_USER_PROFILE_KEY);
            }
        }
     }
})();
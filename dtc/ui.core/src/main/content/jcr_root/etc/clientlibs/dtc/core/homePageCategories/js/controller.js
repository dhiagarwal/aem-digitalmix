	(function() {
"use strict";

homePageCategoriesModule.controller('homePageCategoriesController', ['$q', '$window', 'userProfileManager', function ($q,$window,userProfileManager){

    var self = this;

    self.categoriesValues = [];
	self.headerText = angular.element($('#headerText')).val();
    self.descriptionText = angular.element($('#descriptionText')).val();

    var categoriesFromDialog = angular.element($('#categories')).val();
    var headerParamsFromDialog = angular.element($('#headerParams')).val();
    var categories = eval("[" + categoriesFromDialog + "]");
    var headerParams = eval("[" + headerParamsFromDialog + "]");

    function getQueryParam(param) {
        var result =  window.location.search.match(
            new RegExp("(\\?|&)" + param + "(\\[\\])?=([^&]*)")
        );

        return result ? result[3] : false;
    }

    self.inParamVal = getQueryParam("param");
	
	if(!self.inParamVal && getCookie('dtcParam')){
        self.inParamVal = getCookie('dtcParam');
    }

    self.init = function(){

        for(var i = 0; i < categories.length; i++) {
            var obj1 = categories[i];
            if(self.inParamVal){
                if(obj1.categoryParams.includes(self.inParamVal)){
                    self.categoriesValues.push({
                        imageUrl : obj1.imageUrl, 
                        categoryText : obj1.categoryText,
                        categoryUrl : obj1.categoryUrl 
                    });
                }
            } else if(obj1.isDefault){
                self.categoriesValues.push({
                    imageUrl : obj1.imageUrl, 
                    categoryText : obj1.categoryText,
                    categoryUrl : obj1.categoryUrl 
                });
            }
        }

        if(self.inParamVal){
            for(var j=0; j < headerParams.length; j++){
                var obj2 = headerParams[j];
                if(obj2.params.includes(self.inParamVal)){
                    self.headerText = obj2.headerText;
                    self.descriptionText = obj2.descriptionText;
                    break;
                }
                
            }
        }
    };

}])

}());
'use strict';
 
birthdayController.$inject = ['birthdayFactory', 'birthdayService', '$scope', 'userProfileManager'];
birthdayModule.directive('birthdayPicker',function(){
 
              return {
                             restrict: 'E',
 
    templateUrl: '/etc/clientlibs/dtc/core/main/js/birthday/birthday-picker.html',
    scope:{
        month: '=',
        day: '=',
        year: '=',
        validDateCheck: '&',
        isSubmitted: '<'
    },
        controller: birthdayController,
        controllerAs : 'ctrl',
        bindToController : true
    }
})
var ageLimit=0;
function birthdayController(birthdayFactory,birthdayService,$scope, userProfileManager){
 
    var self = this;
    this.errorMessage = angular.element('#birthdayInvalidError').val();
 
    this.birthdayFactory = birthdayFactory;
    this.monthsList = birthdayFactory.getMonths();
    this.daysList = birthdayFactory.getDaysListDefault();
 
    var locales=userProfileManager. getPageLocaleVal();
    var getYears = birthdayService.getAgeLimit();
    getYears.then(function(data){
             
       data.forEach(function(item){
           if(item.countryCode == locales.market){
              ageLimit = item.minAge;
           }
        });
      self.yearsList = birthdayFactory.getYears(ageLimit);
      $scope.yearsList = birthdayFactory.getYears(ageLimit); 
    })
 
    this.error = {hasError : false, message : ''};
 
    this.getMonth = function() {
        return this.month;
    }
 
 
   
    function isValidDate(str) {
        var d = moment(str,'MM/DD/YYYY');
        if(d == null || !d.isValid()) return false;
       
        return str.indexOf(d.format('M-D-YYYY')) >= 0
            || str.indexOf(d.format('MM-DD-YYYY')) >= 0
            || str.indexOf(d.format('M-D-YY')) >= 0
            || str.indexOf(d.format('MM-DD-YY')) >= 0;
      }
 
    this.getDaysList = function(monthCode){
        this.daysList = this.birthdayFactory.getDaysForMonth(monthCode);
        this.leapYearValidation();
    }
    $scope.$watch(this.getMonth.bind(this), this.getDaysList.bind(this));


    this.checkDate = function () {
        if((!this.day || this.day == 'DD') || (!this.month || this.month == 'MM') || (!this.year || this.year == 'YYYY')) {
        this.error.hasError = true;  
          this.error.message = angular.element('#birthdayInvalidError').val();
          angular.element(document).find('select').addClass("red-border");
          angular.element(berror).removeClass("hide"); 
          this.validDateCheck();
          return; 
        }
    }
    this.checkFocus = function() {
        this.error.hasError = false;
        this.error.message = '';
        angular.element(document).find('select').removeClass("red-border");
        return;
    }

    this.leapYearValidation = function(){
 
        if(this.year%4 !== 0 && this.month == 2 && this.day == 29){
            this.error.hasError = true;
            this.error.message = angular.element('#birthdayInvalidError').val();
            return;
        }
   //minimum age check
        var currentDate = new Date();
        var selectedDate = new Date(this.year, this.month-1, this.day);
        var validAgeDate = new Date(currentDate.getFullYear()-ageLimit,currentDate.getMonth(),currentDate.getDate());
        if(selectedDate>validAgeDate){
            this.error.hasError = true;
            this.validdatecheck=true;
            this.error.message = angular.element('#birthdayInvalidError').val();
            return;
        }      
 
        if (!isValidDate (this.month + "-" + this.day + "-" + this.year))
        {
            if ((this.month.length>0 && this.month != 'MM')&& (this.day.length>0 && this.day != 'DD') && (this.year.length>0 && this.year != 'YY'))
            {
                this.error.hasError = true;
                this.validdatecheck=true;
                this.error.message = angular.element('#birthdayInvalidError').val();
                return;
            }
        }
        this.error.hasError = false;
        if (this.month && this.day && this.year){
          this.validDateCheck();
		}
    };
 
};
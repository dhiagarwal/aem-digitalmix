angular.module('dtc', ['common', 'core', 'signup', 'digitalKit', 'prospect', 'commerce']).config(['$provide', '$httpProvider', function ($provide, $httpProvider) {
// Intercepting HTTP calls with AngularJS.


}]);

function remove(arr, what) {
    var found = arr.indexOf(what);

    while (found !== -1) {
        arr.splice(found, 1);
        found = arr.indexOf(what);
    }
}

angular.module('common', ['ngCookies', 'ui.router', 'ngSanitize', 'ui.mask', 'formUtilsModule', 'ngAnimate', 'ui.bootstrap', 'ui.event', 'slickCarousel', 'birthdayModule', 'emailPhoneModule', 'passwordPatternModule', 'passwordFieldModule', 'ngMessages', 'nuskin.typeahead', 'ngFileUpload', 'cardsModule', 'formErrorScrollModule','addessVerificationComponentModule', 'pageLevelErrorModule', 'loaderModule','scrollToTopWithoutClickModule','customModalModule']);

var coreModule = angular.module('core',[]);

var signupModule = angular.module('signup',[]);

var digitalKitModule = angular.module('digitalKit',[]);

var prospectModule = angular.module('prospect',[]);

var commerceModule = angular.module('commerce',[]);
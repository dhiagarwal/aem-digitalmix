'use strict';

passwordPatternController.$inject = ['$scope', '$sce'];
passwordPatternModule.directive('passwordPattern',function(){

	return {
		restrict: 'E',
	    scope:{
            credentials: '=',
            isRequired: '<',
            isSubmitted: '<',
            passwordHint: '=',
            createPasswordTitle: '=',
            confirmPasswordTitle: '=',
            minCharTitle: '=',
            emptyPasswordError: '=',
            invalidPasswordError: '=',
            confirmPasswordError: '=',
            passwordDoNotMatchError: '=',
            fixToContinueTitle: '=',
            clickToHidePasswordTitle: '=',
            clickToShowPasswordTitle: '='
	    },
	    templateUrl: '/etc/clientlibs/dtc/core/main/js/password-pattern/password-pattern.html',
	    controller: passwordPatternController,
	    controllerAs: 'ctrl',
	    bindToController : true
	}
})
function passwordPatternController($scope, $sce){
	var self = this;
    this.name = 'passwordPattern';
    this.credentials = this.credentials || {};
    this.credentials.passwordImage = {"text":"/etc/clientlibs/dtc/core/main/css/src/assets/images/css/ic-password-show.png",
                            "mask":"/etc/clientlibs/dtc/core/main/css/src/assets/images/css/ic-password-hide.png"};
    this.credentials.currentPasswordState = 'password';
    this.credentials.confirmPasswordState = 'password';
    this.credentials.userPassword = "";
    this.credentials.confirmPassword = "";
    
    this.changeImage = function(flag) {
      if(flag){
        this.credentials.currentPasswordState = this.credentials.currentPasswordState === 'text' ? 'password': 'text';
      } else {
        this.credentials.confirmPasswordState = this.credentials.confirmPasswordState === 'text' ? 'password': 'text';
      }
   	};

  	this.verifyPassword = function(form){
      this.credentials.emptyConfirmPasswordError = !this.credentials.confirmPassword;
  		this.credentials.isInvalidConfirmPassword = this.credentials.userPassword !== this.credentials.confirmPassword;
      this.setFormValidity(form);
  	};

    this.analyzePassword = function(form) {
	  if(!this.isRequired && (this.credentials.userPassword == "undefined" || this.credentials.userPassword == "")) {
	        form.$$parentForm.$setValidity("password", true);
	        return true;
	      }
      var password = this.credentials.userPassword;
      var strength = this.isSatisfied(password && password.length >= 8) &&
        (this.isSatisfied(password && /[\@\#\$\%\^\&\*\(\)\_\+\!]/.test(password)) +  
              this.isSatisfied(password && /[a-z]/.test(password)) + 
              this.isSatisfied(password && /[A-Z]/.test(password)) + 
              this.isSatisfied(password && /[0-9]/.test(password))); 
      this.credentials.isInvalidPassword = strength < 3;
      this.credentials.emptyPasswordError = !password;
      this.setFormValidity(form);
    };

    this.setFormValidity = function(form) {
      if( this.credentials.isInvalidPassword || this.credentials.isInvalidConfirmPassword ){
            form.$$parentForm.$setValidity("password", false);
        } else {
            form.$$parentForm.$setValidity("password", true);
        }
    };

    this.isSatisfied = function(criteria) { 
        return criteria ? 1 : 0; 
    };
    if(this.passwordHint) {
        this.passwordHintMessage = $sce.trustAsHtml(this.passwordHint);
    }
};
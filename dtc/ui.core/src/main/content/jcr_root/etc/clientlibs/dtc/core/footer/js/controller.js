'use strict';

;(function(){
footerModule.controller('footerController', ['footerservice', '$filter', '$window', '$timeout', 'userProfileManager', function (footerservice, $filter,$window,$timeout,userProfileManager){

 var self=this;
 	self.model={}
    this.distributorPhone='';
    this.logoUrl="/etc/clientlibs/dtc/core/main/css/src/assets/images/css/ic-deloittelogo-white.svg";
    this.facebookLogo="/etc/clientlibs/dtc/core/main/css/src/assets/images/css/facebook.svg";
    this.twitterLogo="/etc/clientlibs/dtc/core/main/css/src/assets/images/css/twitter.svg";
    this.instagramLogo="/etc/clientlibs/dtc/core/main/css/src/assets/images/css/instagram.svg";
    this.pinterestLogo="/etc/clientlibs/dtc/core/main/css/src/assets/images/css/pintrest.svg";
	var scroll_top_duration = 700;
	var offset = 300;
	var backToTop = angular.element(document.querySelector('.back-to-top-icon'));

	angular.element(window).scroll(function(){
        if(angular.element(document.querySelector('.footer-nav')).offset().top){
            var scrollTop = angular.element(document).scrollTop();
            var windowHeight = angular.element(window).height();

            var footerElement = angular.element(document.querySelector('.footer-nav')).offset().top;

            ( angular.element(document).scrollTop() > offset ) ? backToTop.addClass('back-to-top-is-visible') : backToTop.removeClass('back-to-top-is-visible');

            if((scrollTop + windowHeight) > footerElement){
                backToTop.addClass('stick-to-footer');
            }else{
                backToTop.removeClass('stick-to-footer');
            }
        }
    });

	this.hideBackToTop=function(){
        backToTop.removeClass('back-to-top-is-visible');
    };

    this.backToTopClick = function (event) {
        angular.element('html,body').animate({
               scrollTop: 0
            }, scroll_top_duration
        );
    };
    angular.element(document).ready(function(){
    var languagePromise=footerservice.getDefaultCommPrefs();

    languagePromise.then(function(data) {
      	self.languageData = data;
      	$timeout(function(){
        self.model.regionList = userProfileManager.getPageLocaleVal().market;
		self.model.languageList = userProfileManager.getPageLocaleVal().language;
      	},0);
    });
    self.currentPageVal=angular.element("#currentPath").val();
    });

    self.regionChange = function(){

        var regionVal = angular.element("#footerRegion").val();
        var defaultLang = $filter('filter')(self.languageData,{ countryCode: regionVal.toLowerCase()})[0].languages;
        defaultLang = defaultLang.substr(0,defaultLang.indexOf('-'));
		var promise = footerservice.regionLang(regionVal,'',self.currentPageVal,defaultLang);

         promise.then(function(data) {
           $window.location.href =	$window.location.protocol + '//'
							+ $window.location.host
							+ data.regionPath+".html";

                        });
    }

    self.langChange = function(){
        var langVal = angular.element("#footerLanguage").val();
        var promise = footerservice.regionLang('',langVal, self.currentPageVal,'');

        promise.then(function(data) {
        	 $window.location.href =	$window.location.protocol + '//'
				+ $window.location.host
				+ data.langPath+".html";

                        });
    }

     self.redirectPage = function(url){
    	 var pattern = /^https?:\/\//i;
   	     if (pattern.test(url))
           {
               $window.open(url, "_self");
			}
         else{
              var redirectUrl=$window.location.protocol+'//'+$window.location.host+ url + '.html';
              $window.open(redirectUrl, "_self");
         }
    }

     if(userProfileManager.isUserLoggedIn()){
          var profile=userProfileManager.getUserProfileDetailInfo();
     var master=userProfileManager.getUserProfileMasterInfo();
    if(profile && master){
        if(master.userType==USER_TYPE.PRIMARY_DISTRIBUTOR||master.userType==USER_TYPE.ASSOCIATE||master.userType==USER_TYPE.FAMILY_MEMBER){
            this.isDistributor=true;
            this.distributorName=profile.profileHeader.firstName+" "+profile.profileHeader.lastName;
            this.profileUrl=profile.profileHeader.profilePictureUrl;
            this.distributorEmail=profile.profileContactInfo.email;
            if(profile.profileContactInfo.phone!=undefined){
                var phone=profile.profileContactInfo.phone;
                 this.distributorPhone= phone.substr(0,3) + "-" + phone.substr(3,3) + "-" +phone.substr(6);
            }
        }
        else if(master.userType==USER_TYPE.RETAIL_CUSTOMER||master.userType==USER_TYPE.PREFERED_CUSTOMER){
            this.isCustomer=true;
            this.customerName=profile.profileHeader.firstName+" "+profile.profileHeader.lastName;
            this.profileUrl=profile.profileHeader.profilePictureUrl;
            this.customerEmail=profile.profileContactInfo.email;
            if(profile.profileContactInfo.phone!=undefined){
                var phone=profile.profileContactInfo.phone;
                this.customerPhone=phone.substr(0,3) + "-" + phone.substr(3,3) + "-" +phone.substr(6);
            }
        }
    	}
     }
    else{
        this.isAnonymous=true;
    }

}]);
})();

'use strict';

cardsModule.service('cardsService' ,['$http', '$q', '$window', 'userProfileManager', function($http,$q,$window,userProfileManager){

    this.$http = $http;
    var deferred = $q.defer();

    this.removeCard = function(slideId,useType) {

    	var requestBody = new Object;
    	var userId = userProfileManager.getUserProfileMasterInfo().userId;
    	requestBody.cardIds = slideId.toString();
    	requestBody.createdBy = 'NuSkin';
    	requestBody.userId = userId;
        if(undefined != useType){
             requestBody.cardType =useType ;
        }

    	console.log(requestBody);

    	var postUrl = mulesoftStorefrontEndpoint+'create/usercards';
		$http.post(postUrl,requestBody).then(function(response) {
			console.log(response.data);
            deferred.resolve(response.data);
    	});
        return deferred.promise;
    }

}])

'use strict';
;
(function() {
	signInModuleComponent
			.controller(
					'signInModuleComponentController',
					[
							'signInModuleComponentService',
							'$window',
							'$cookies',
							'$timeout',
							'userProfileManager',
							function(signInModuleComponentService, $window,
									$cookies, $timeout, userProfileManager) {

								var self = this;

								this.result = {};
								this.service = signInModuleComponentService;
								this.name = 'signin';
								this.clickedsignin = false;
								this.usernameempty = false;
								this.passwordempty = false;
								this.remembermechk = false;
								this.isRememberMe = false;
								this.attempts = 3;
								var attemptsDeduct = 3;
								var attemptsCount = 1;
								var userId = "";

								var patterns = {
									email : /^[^\s]+$/,
									phone : /^[1-9]{1}[0-9]{9}$/
								};

								this.initLoginForm = function() {
								}

								this.submitlogin = function() {
									this.clickedsignin = true;
									if (this.username == null
											|| this.username == "") {
										this.usernameempty = true;
									}
									if (this.credentials.enterPassword == ""
											|| this.credentials.enterPassword == null
											|| this.credentials.enterPassword == undefined) {
										this.passwordempty = true;
									} else {
										this.passwordempty = false;
									}

									if (this.usernameempty == false
											&& this.passwordempty == false
											&& this.match == true) {
										var user = new Object();
										user.identifier = this.username;
										user.password = this.credentials.enterPassword;
										gigya.accounts.login({
						                    loginID: user.identifier,
						                    password: user.password,
						                    format: 'jsonp',
						                    callback: function(response) {
												console.log("Gigya login Successful");
												$.ajax({
													url : '/bin/loginDtc',
													method : 'GET',
													dataType : 'json',
													async : false,
													data : {
														user : JSON
																.stringify(user)
													},
													success : function(
															response) {
														userProfileManager.setUserProfile(JSON.parse(response),this.username);
														self.redirectOnSuccess();
														
													},
													error : function(
															response) {
														console.log("Error in Signin Service");
														var incorrectUPMsg = angular.element.find('#incorrectUPMsg')[0];
														incorrectUPMsg.style.display="block";
													}
												});
						                    }
						                });
										return true;
									}
									return false;
								}
								
								this.redirectsignup = function() {
									var URL = angular.element(
											$('#sihninSignUpPagePath')).val();
									var joinDTCPagePath = angular.element(
											$('#joinDTCPagePath')).val();
									if (undefined != URL && null != URL
											&& '' != URL
											&& undefined != joinDTCPagePath
											&& null != joinDTCPagePath
											&& '' != joinDTCPagePath) {
										var guid = userProfileManager.getGuid();
										var checkoutPageURL = userProfileManager
												.getPageReturnURL();
										// if the user comes through an invite
										// or is in the pre-checkout flow
										// redirect him to the sign up page.
										if (null != guid || (checkoutPageURL)) {
											$window.location = $window.location.protocol
													+ '//'
													+ $window.location.host
													+ URL + '.html';
										} else {
											$window.location = $window.location.protocol
													+ '//'
													+ $window.location.host
													+ joinDTCPagePath
													+ '.html';
										}
									}
								}

								this.redirectforgotpassword = function() {
									var URL = angular.element(
											$('#sihninForgotPasswordPagePath'))
											.val();
									if (undefined != URL && null != URL
											&& '' != URL) {
										$window.location = $window.location.protocol
												+ '//'
												+ $window.location.host
												+ URL + '.html';
									}
								}

								this.redirectforgotusername = function() {
									var URL = angular.element(
											$('#forgotUserNamePath')).val();
									if (undefined != URL && null != URL
											&& '' != URL) {
										$window.location = $window.location.protocol
												+ '//'
												+ $window.location.host
												+ URL + '.html';
									}
								}

								this.entervalue = function(value) {
									$timeout(function() {
										self.clickedsignin = false;
										self.usernameempty = false;
										if (value) {
											self.match = patterns.email
													.test(value);
										} else {
											self.match = false;
										}
									}, 200);
								}

								this.enterpassword = function(value) {
									this.clickedsignin = false;
									this.passwordempty = false;
								}

								this.socialLogin = function(socialLoginProvider) {
									if (undefined != socialLoginProvider
											&& '' != socialLoginProvider) {
										gigya.accounts
												.socialLogin({
													provider : socialLoginProvider,
													conflictHandling : 'saveProfileAndFail',
													callback : this.loginPostProcessor
												});
									}
								}

								function processLogin(eventObj) {

									var chkbox = angular.element
											.find('#rememberme')[0];
									var errorComp = angular.element
											.find('#loginErrorMsgText')[0];
									var incorrectUPMsg = angular.element
											.find('#incorrectUPMsg')[0];
									var attemptsMsg = angular.element
											.find('#attemptsMsg')[0];
									var accountLockedMsg = angular.element
											.find('#accountLockedMsg')[0];
									var userName = angular.element(
											$('#userName')).val();
									if (eventObj.errorCode == 0) {
										if (chkbox.checked
												&& eventObj.loginProvider === 'site') {
											var expDate = new Date();
											expDate
													.setDate(expDate.getDate() + 365);
											$cookies.put("uname", userName, {
												expires : expDate,
												path : '/'
											});
										}
										if (!chkbox.checked) {
											$cookies.remove("uname", {
												path : '/'
											});
										}

										errorComp.style.display = "none";
										var promise = signupModuleSharedService
												.signIn(eventObj.user);
										promise
												.then(function(response) {

													var guestGUID = "";
													if ($cookies
															.get("cart_guid") != undefined
															&& $cookies
																	.get("cart_guid") != "") {
														guestGUID = $cookies
																.get("cart_guid");
													}
													if (undefined != guestGUID
															&& null != guestGUID
															&& '' != guestGUID) {
														self
																.mergeUserCart(guestGUID);
													} else {
														guestGUID = null;
														self
																.mergeUserCart(guestGUID);
													}

												})

									} else {
										var betaMsg = angular.element
												.find('#betaMsg')[0];
										if (null != betaMsg
												&& undefined != betaMsg) {
											betaMsg.style.display = "none";
										}
										userId = eventObj.requestParams.loginID;
										self.attempts = attemptsDeduct;
										if (eventObj.errorCode == 403042
												&& attemptsCount == 1) {
											incorrectUPMsg.style.display = "block";
											attemptsMsg.style.display = "none";
											accountLockedMsg.style.display = "none";
										}
										if (eventObj.errorCode == 403042
												&& attemptsCount > 1) {
											incorrectUPMsg.style.display = "none";
											attemptsMsg.style.display = "block";
											accountLockedMsg.style.display = "none";
											attemptsDeduct = attemptsDeduct - 1;
										}
										if (eventObj.errorCode == 403041) { // Error
											// meesage
											// for
											// inactive
											// accounts
											incorrectUPMsg.style.display = "none";
											attemptsMsg.style.display = "block";
											accountLockedMsg.style.display = "none";
											attemptsDeduct = attemptsDeduct - 1;
										}
										/*
										 * commenting number of attempts part
										 * for Alpha release
										 * if(eventObj.errorCode == 403042 &&
										 * attemptsCount>=3){
										 * incorrectUPMsg.style.display="none";
										 * attemptsMsg.style.display="block";
										 * accountLockedMsg.style.display="none";
										 * attemptsDeduct = attemptsDeduct-1; }
										 */
										if (eventObj.errorCode == 403120) {// Need
											// to
											// check
											// the
											// error
											// or
											// msg
											incorrectUPMsg.style.display = "none";
											attemptsMsg.style.display = "none";
											accountLockedMsg.style.display = "block";
										}
										attemptsCount = attemptsCount + 1;

									}
								}

								this.loginPostProcessor = function(eventObj) {
									if (!eventObj.newUser) {
										processLogin(eventObj);
									}
								}

								function getParameterByName(name, url) {
									name = name.replace(/[\[\]]/g, "\\$&");
									var regex = new RegExp("[?&]" + name
											+ "(=([^&#]*)|&|#|$)"), results = regex
											.exec(url);
									if (!results)
										return null;
									if (!results[2])
										return '';
									return decodeURIComponent(results[2]
											.replace(/\+/g, " "));
								}

								this.redirectOnSuccess = function() {
									var homePagePathURL = angular.element(
											$('#sihninHomePagePath')).val();
									var checkoutPathURL = userProfileManager
											.getPageReturnURL();

									if (location.search.indexOf('redirectTo') > 0) {
										$window.location.href = location.search
												.split('?redirectTo=')[1];
										return;
									}

									// Redirecting User to previous url if he
									// signed out due to Login expiration
									var userID = userProfileManager
											.getUserProfileMasterInfo().userId;
									var returnUrlFor401Error = "";

									if ($cookies.get(userID
											+ 'returnUrlFor401Error') != undefined
											&& $cookies.get(userID
													+ 'returnUrlFor401Error') != "") {
										returnUrlFor401Error = $cookies
												.get(userID
														+ 'returnUrlFor401Error');
									}

									if (undefined != returnUrlFor401Error
											&& null != returnUrlFor401Error
											&& '' != returnUrlFor401Error) {
										if (returnUrlFor401Error.toLowerCase()
												.indexOf('.html') >= 0) {
											$window.location = returnUrlFor401Error;
										} else {
											$window.location = returnUrlFor401Error
													+ '.html';
										}
									} else if (undefined != checkoutPathURL
											&& null != checkoutPathURL
											&& '' != checkoutPathURL) {
										if (checkoutPathURL.toLowerCase()
												.indexOf('.html') >= 0) {
											$window.location = checkoutPathURL;
										} else {
											$window.location = $window.location.protocol
													+ '//'
													+ $window.location.host
													+ checkoutPathURL + '.html';
										}
									} else {
										if (undefined != homePagePathURL
												&& null != homePagePathURL
												&& '' != homePagePathURL) {

											$window.location = $window.location.protocol
													+ '//'
													+ $window.location.host
													+ homePagePathURL + '.html';
										}
									}
								}

								this.getCartMerged = function(userID, guestGUID) {
									var mergeCartResponse = cartSharedService
											.mergeCart(userID, guestGUID);
									mergeCartResponse
											.then(
													function(data) {
														if (data
																.hasOwnProperty("code")) {
															var expDate = new Date();
															expDate
																	.setDate(expDate
																			.getDate() + 1);
															$cookies
																	.put(
																			"cart_guid",
																			data.code,
																			{
																				path : '/'
																			});
															$cookies
																	.put(
																			"totalItems",
																			data.totalUnitCount,
																			{
																				path : '/'
																			});
															analyticsService
																	.updateProductString(data.entries);
															self
																	.redirectOnSuccess();
														} else {
															analyticsService
																	.clearProductString();
															self
																	.redirectOnSuccess();
														}
													},
													function(response) {
														analyticsService
																.clearProductString();
														self
																.redirectOnSuccess();
													});
								}

								this.mergeUserCart = function(guestGUID) {
									var userID = userProfileManager
											.getUserProfileMasterInfo().userId;
									var getUserCartResponse = cartSharedService
											.getUserCart(userID)
									getUserCartResponse
											.then(
													function(data) {
														if (data
																.hasOwnProperty("carts")
																&& data.carts.length > 0) {
															analyticsService
																	.updateProductString(data.carts[0].entries);
															if (guestGUID != null) {
																self
																		.getCartMerged(
																				userID,
																				guestGUID);
															} else {
																var expDate = new Date();
																expDate
																		.setDate(expDate
																				.getDate() + 1);
																$cookies
																		.put(
																				"cart_guid",
																				data.carts[0].code,
																				{
																					path : '/'
																				});
																$cookies
																		.put(
																				"totalItems",
																				data.carts[0].totalUnitCount,
																				{
																					path : '/'
																				});
																self
																		.redirectOnSuccess();
															}
														} else if (guestGUID != null) {
															var createCartResponse = cartSharedService
																	.createCart(userID);
															createCartResponse
																	.then(
																			function(
																					data) {
																				if (data
																						.hasOwnProperty("code")) {
																					self
																							.getCartMerged(
																									userID,
																									guestGUID);
																				} else {
																					analyticsService
																							.clearProductString();// If
																					// Cart
																					// Not
																					// created
																					// then
																					// clear
																					// product
																					// Strings
																					self
																							.redirectOnSuccess();
																				}
																			},
																			function(
																					response) {
																				analyticsService
																						.clearProductString();
																				self
																						.redirectOnSuccess();
																			});
														} else {
															analyticsService
																	.clearProductString();
															self
																	.redirectOnSuccess();
														}
													},
													function(response) {
														analyticsService
																.clearProductString();
														self
																.redirectOnSuccess();
													});
								}

							} ]);
})();
'use strict';
;
(function() {
	signupComponent
			.controller(
					'signUpController',
					[
							'$scope',
							'$window',
							'signupService',
							'$http',
							'userProfileManager',
							'$cookies',
							function($scope, $window, signupService, $http,
									userProfileManager, $cookies) {

								var vmp = $scope;
								var self = this;

								this.userDetailField = '';
								this.countryInput = '';
								this.languageInput = '';
								this.emailCredentials = {};
								this.phoneCredentials = {};
								this.isEmail = true;
								this.hide = true;
								this.hideChooseSponsor = true;
								this.hideChangeSponsor = true;
								this.sponsorId = null;
								$scope.duplicateEmail = false;
								self.defaultthumbnailURL = "/etc/clientlibs/dtc/core/main/css/src/assets/images/css/ic-profile.svg";
								self.fromInvite = false;
								self.isCustomer = false;
								self.isDistributor = false;
								self.isAssociate = false;
								self.isFrndsFamily = false;
								self.isDefaultPic = false;
								self.fromInviteLink = false;
								self.displayFieldSpinner = false;

								this.lastName = '';
								this.firstName = '';
								this.emailCredentials.userEmailField = '';
								this.isPhone = true;
								this.credentials = {};
								this.isPhoneVisible = false;
								this.isRequired = true;
								this.isSponsorPresent = false;

								// Initialize variables/functions related to the
								// privacy/terms pop ups
								angular
										.element(document)
										.ready(
												function() {
													self.termsAndUsePopUpHeading = angular
															.element(
																	'#termsAndUsePopUpHeading')
															.val();
													self.privacyPolicyPopUpHeading = angular
															.element(
																	'#privacyPolicyPopUpHeading')
															.val();
													self.termsAndUseContentPath = angular
															.element(
																	'#termsAndUseContentPath')
															.val();
													self.privacyPolicyContentPath = angular
															.element(
																	'#privacyPolicyContentPath')
															.val();
													self.popUpBackButtonLabel = angular
															.element(
																	'#popUpBackButtonLabel')
															.val();
													self.popUpGoBackButtonLabel = angular
															.element(
																	'#popUpGoBackButtonLabel')
															.val();
												});

								this.showModal = false;
								this.showUsernameInvalidError = "";
								this.emailPattern = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

								this.knowMore = function(linkType) {
									if (linkType == 'termsUse') {
										self.modalHeading = this.termsAndUsePopUpHeading;
										self.modalURL = this.termsAndUseContentPath;
									} else if (linkType == 'privacyPolicy') {
										self.modalHeading = this.privacyPolicyPopUpHeading;
										self.modalURL = this.privacyPolicyContentPath;
									}
									self.back = this.popUpBackButtonLabel;
									self.goBack = this.popUpGoBackButtonLabel;
									this.showModal = true;
								};
								this.hideCustomModal = function() {
									this.showModal = false;
								};

								// Initialize the page
								self.init = function() {
									// Redirect the user to home page if he is
									// already logged in.
									if (userProfileManager.isUserLoggedIn()) {
										var loggedInRedirectPath = angular
												.element(
														$('#loggedInRedirectionPagePathSignUp'))
												.val();
										var loggedInRedirectURL = $window.location.protocol
												+ '//'
												+ $window.location.host
												+ loggedInRedirectPath
												+ '.html';
										$window.location = loggedInRedirectURL;
									}

									self.isSubmitAllowed = true;

									// If sponsor is selected using the Sponsor
									// search page , Set up the sponsor data in
									// hostInfo.
									self.hostInfo = userProfileManager
											.getHostInfo();
									if (undefined != self.hostInfo
											&& null != self.hostInfo
											&& '' != self.hostInfo) {
										self.hideChangeSponsor = false;
										self.hideChooseSponsor = true;

										self.isSponsorPresent = true;

										if (self.hostInfo.userEmailField) {
											self.emailCredentials.userEmailField = self.hostInfo.userEmailField;
										}
										if (self.hostInfo.guestLastName) {
											self.lastName = self.hostInfo.guestLastName;
										}
										if (self.hostInfo.guestFirstName) {
											self.firstName = self.hostInfo.guestFirstName;
										}
										if (self.hostInfo.guestAccountId) {
											self.guestAccountId = self.hostInfo.guestAccountId;
										}
										if (self.hostInfo.hostProfilePhotoUrl) {
											self.hostProfilePhotoUrl = self.hostInfo.hostProfilePhotoUrl;
											self.isDefaultPic = false;
										}
										if (self.hostInfo.hostUid) {
											self.hostUserId = self.hostInfo.hostUid;
										}
									} else {
										self.guestAccountId = null;
										self.hostUserId = null;
									}

									// In case the User Type is empty, Set it as
									// Retail Customer
									var signupType = userProfileManager
											.getSignUpUserType();
									if (null == signupType) {
										userProfileManager
												.setSignUpUserType(USER_TYPE.RETAIL_CUSTOMER);
										signupType = USER_TYPE.RETAIL_CUSTOMER;
									}

									// In case the user comes from an invite
									// link, fetch the sponsor information from
									// guid.
									var guid = self.getQueryStringValue('guid');
									if (undefined != guid && null != guid
											&& 0 < guid.length) {
										$cookies.remove("HOST_INFO", {
											path : '/'
										});
										self.fromInviteLink = true;
										userProfileManager.setGuid(guid);
										self.processGUID(guid, signupType);
									} else {
										guid = null;
									}

									// Redirect to sponsor search page, in case
									// the user has not yet selected a sponsor.
									var sponsorSelected = self
											.getQueryStringValue('sponsorSelected');

									if ((signupType == USER_TYPE.RETAIL_CUSTOMER || signupType == USER_TYPE.PRIMARY_DISTRIBUTOR)
											&& (undefined == self.guestAccountId
													|| null == self.guestAccountId || '' == self.guestAccountId)
											&& null == guid
											&& "'true'" != angular.element(
													"#editMode").val()) {

										if (undefined == sponsorSelected
												|| null == sponsorSelected
												|| '' == sponsorSelected
												|| sponsorSelected == 'false') {
											var SponsorSearchPagePath = angular
													.element(
															"#sponsorsearchpath")
													.val();
											var url = $window.location.href;
											userProfileManager
													.setSponsorPageReturnURL(url);
											if (undefined != SponsorSearchPagePath
													&& null != SponsorSearchPagePath
													&& '' != SponsorSearchPagePath) {
												$window.location = $window.location.protocol
														+ '//'
														+ $window.location.host
														+ SponsorSearchPagePath
														+ '.html';
											}
										}
									}

									// Display change/choose sponsor option,
									// based on whether the sponsor is already
									// selected or not.
									if (signupType == USER_TYPE.RETAIL_CUSTOMER
											|| signupType == USER_TYPE.PRIMARY_DISTRIBUTOR
											|| signupType == USER_TYPE.PREFERED_CUSTOMER) {
										if (undefined != self.guestAccountId
												&& null != self.guestAccountId
												&& '' != self.guestAccountId) {
											self.hideChangeSponsor = false;
											self.hideChooseSponsor = true;
										} else {
											self.hideChangeSponsor = true;
											self.hideChooseSponsor = false;
										}
									}

									// If a default profile picture for the
									// header is authored, show that profile
									// picture
									var authoredProfileImageRef = angular
											.element($('#defaultPicPath'))
											.val();

									if (authoredProfileImageRef != null
											&& authoredProfileImageRef != ""
											&& authoredProfileImageRef != undefined) {
										self.defaultthumbnailURL = authoredProfileImageRef;
										self.isDefaultPic = false;
									} else if (self.hostProfilePhotoUrl) {
										self.isDefaultPic = false;
									} else {
										self.defaultthumbnailURL = "/etc/clientlibs/dtc/core/main/css/src/assets/images/css/ic-profile.svg";
										self.isDefaultPic = true;
									}

									// Display the header info
									if (userProfileManager.getGuid()) {
										self.fromInvite = true;
										if (!self.fromInviteLink) {
											self
													.setInviteHeaderText(signupType);
										}
									} else {
										// Show the basic header
										self.fromInvite = false;
									}

									// Pre Populate User Information in the
									// form, when he had redirected to sponsor
									// search page
									var uName = self
											.getQueryStringValue("uName");
									var fName = self
											.getQueryStringValue("fName");
									var lName = self
											.getQueryStringValue("lName");
									if (uName) {
										self.emailCredentials.userEmailField = uName;
									}
									if (fName) {

										self.firstName = fName;
									}
									if (lName) {
										self.lastName = lName;
									}
								}

								// function to fetch url query parameters
								self.getQueryStringValue = function(key) {
									return unescape($window.location.search
											.replace(
													new RegExp(
															"^(?:.*[&\\?]"
																	+ escape(
																			key)
																			.replace(
																					/[\.\+\*]/g,
																					"\\$&")
																	+ "(?:\\=([^&]*))?)?.*$",
															"i"), "$1"));
								};

								// function to fetch data from guid of the
								// invite link
								this.processGUID = function(guid, signupType) {

									var promise = signupService.getMember(guid);
									promise
											.then(function(data) {

												if (data.hostAccountId) {
													self.hasGuid = true;
												}

												self.emailCredentials.userEmailField = data.guestEmailId;
												self.hostInfo = data;
												self.hostInfo.userEmailField = self.hostInfo.guestEmailId;
												self.guestAccountId = self.hostInfo.guestAccountId;
												if (self.hostInfo.guestLastName) {
													self.lastName = self.hostInfo.guestLastName;
												}
												if (self.hostInfo.guestFirstName) {
													self.firstName = self.hostInfo.guestFirstName;
												}
												if (self.hostInfo.hostUid) {
													self.hostUserId = self.hostInfo.hostUid;
												}
												if (self.hostInfo.hostProfilePhotoUrl) {
													self.hostProfilePhotoUrl = self.hostInfo.hostProfilePhotoUrl;
													self.isDefaultPic = false;
												}
												userProfileManager
														.setSignUpUserType(data.invitationType);
												var inviteSentDate = new Date(
														data.inviteSentOn);
												var currentDate = new Date();
												if (inviteSentDate
														.getFullYear() !== currentDate
														.getFullYear()
														|| inviteSentDate
																.getMonth() !== currentDate
																.getMonth() + 1
														|| inviteSentDate
																.getDate() !== currentDate
																.getDate()) {
													inviteSentDate = inviteSentDate
															.setDate(inviteSentDate
																	.getDate() + 7);
													currentDate = currentDate
															.getTime();
													if (inviteSentDate < currentDate) {
														signupType = userProfileManager
																.getSignUpUserType();
														if ((signupType == USER_TYPE.RETAIL_CUSTOMER
																|| signupType == USER_TYPE.PRIMARY_DISTRIBUTOR || signupType == USER_TYPE.PREFERED_CUSTOMER)
																&& guid != null) {
															self.linkExpired = false;
														} else if ((signupType == USER_TYPE.ASSOCIATE || signupType == USER_TYPE.FAMILY_MEMBER)
																&& guid != null) {
															self.linkExpired = true;
															var expiredLink = angular
																	.element(
																			$('#expiredLink'))
																	.val();
															$window.location = $window.location.origin
																	+ expiredLink
																	+ ".html";

														}
													}
												}

												self
														.setInviteHeaderText(data.invitationType);

												$cookies.remove("HOST_INFO", {
													path : '/'
												});
												$cookies
														.put(
																"HOST_INFO",
																JSON
																		.stringify(self.hostInfo),
																{
																	path : '/'
																});

												if ((data.invitationType == USER_TYPE.RETAIL_CUSTOMER
														|| data.invitationType == USER_TYPE.PRIMARY_DISTRIBUTOR || data.invitationType == USER_TYPE.PREFERED_CUSTOMER)
														&& guid != null) {
													self.hideChangeSponsor = false;
													self.hideChooseSponsor = true;
												}
											});
								};

								self.setInviteHeaderText = function(signupType) {
									if (signupType == USER_TYPE.RETAIL_CUSTOMER
											|| signupType == USER_TYPE.PREFERED_CUSTOMER) {
										// Show the customer invitation text in
										// the header
										self.isCustomer = true;

									} else if (signupType == USER_TYPE.PRIMARY_DISTRIBUTOR) {
										// Show the distributor invitation text
										// in the header
										self.isDistributor = true;
									} else if (signupType == USER_TYPE.ASSOCIATE) {
										// Show the associate invitation text in
										// the header and hide the sponsor
										// selection
										self.isAssociate = true;
										self.hideChangeSponsor = true;
										self.hideChooseSponsor = true;
									} else if (signupType == USER_TYPE.FAMILY_MEMBER) {
										// Show the friends and family
										// invitation text in the header and
										// hide the sponsor selection
										self.isFrndsFamily = true;
										self.hideChangeSponsor = true;
										self.hideChooseSponsor = true;
									} else {
										// Show the basic header
										self.fromInvite = false;
									}
								}

								// Function to handle site login
								this.getStartedShow = function(form) {
									if (!form.$valid) {
										if (!self.credentials.userPassword) {
											self.credentials.emptyPasswordError = true;
										}
										if (!self.credentials.confirmPassword) {
											self.credentials.emptyConfirmPasswordError = true;
										}
										return;
									}

									$scope.isSubmitted = true;
									var value = self.emailCredentials.userEmailField;
									if (!self.emailCredentials.userEmailField) {
										$scope.emptyError = true;
										return;
									}

									value = value.replace(/[\-()]/g, '');

									if (patterns.email.test(value)) {
										usernameAvaiMgr();
									} else {
										this.invalidError = true;
									}
								}

								self.goToUrl = function(targetUrl) {
									if (targetUrl) {
										$window.location = $window.location.origin
												+ targetUrl + ".html";
									}
								};

								self.setUsernameValidity = function(validity) {
									$scope.duplicateEmail = validity;
									// $scope.$apply();
								}

								self.validateEmail = function($form) {
									var value = self.emailCredentials.userEmailField;
									var errorComp = angular.element
											.find('#verifyUsernameAval')[0];

									if (value) {
										self.displayFieldSpinner = true;
										// Checks whether a certain login
										// identifier - email is available
										self.displayFieldSpinner = false;
										errorComp.style.display = "none";
										$form.$setValidity("username", true);
										self.setUsernameValidity(false);
									} else {
										errorComp.style.display = "none";
									}
								}

								self.hideDuplicateError = function() {
									var errorComp = angular
											.element('#verifyUsernameAval');
									errorComp.hide();
								}

								function usernameAvaiMgr() {
									var errorComp = angular.element
											.find('#verifyUsernameAval')[0];
									if (!self.isSubmitAllowed) {
										return;
									}
									this.isSubmitted = true;
									if (!self.credentials.userPassword) {
										self.credentials.emptyPasswordError = true;
									}
									if (!self.credentials.confirmPassword) {
										self.credentials.emptyConfirmPasswordError = true;
									}
									if ((self.credentials && (self.credentials.userPassword || self.credentials.confirmPassword))
											&& (self.credentials.userPassword == self.credentials.confirmPassword)) {
										self.isSubmitAllowed = false;

										var emailId = self.emailCredentials.userEmailField;
										var phoneNo = self.phoneCredentials.userPhoneField;
										var user = new Object();

										user.givenName = self.firstName ? self.firstName
												.toUpperCase()
												: '';
										user.familyName = self.lastName ? self.lastName
												.toUpperCase()
												: '';
										user.identifier = emailId;
										user.email = emailId;
										user.password = self.credentials.userPassword;
										function createUser(response) {
											if(response.isAvailable){
												var data = {
									                    "email": emailId,
									                    "password": self.credentials.userPassword
									                }
												var promise = signupService.registerGigyaUser(data);
								                promise.then(function (eventObj) {
													$.ajax({
																url : '/bin/signUpDtc',
																method : 'GET',
																dataType : 'json',
																async : false,
																data : {
																	user : JSON
																			.stringify(user)
																},
																success : function(
																		response) {
																	console
																			.log("SignUp Successful");
																	userProfileManager.setUserProfile(JSON.parse(response),null);
																	gigya.accounts.login({
													                    loginID: user.email,
													                    password: user.password,
													                    format: 'jsonp',
													                    callback: function(response) {
																			console.log("Gigya login Successful");
																			self.redirectOnSuccess();
													                    }
													                });
																},
																error : function(
																		response) {
																	console
																			.log("SignUp Failed.");
																	signUpErrorHandler();
																}
																
													})
								                });
												
											} else {
												console.log("SignUp Failed.");
												signUpErrorHandler("Email Id already Exists");
											}
									    }
										var idParams = {
								            loginID:emailId,
								            callback: createUser,
								        };
								        gigya.accounts.isAvailableLoginID(idParams);
									}
									errorComp.style.display = "none";
									$scope.duplicateEmail = false;

								}

								self.changeInviteStatus = function(data) {
									var config = {
										headers : {
											'content-type' : 'application/json',
											'showgloballoader' : true
										}
									}
									var reqData = {
										"guestFirstName" : data.guestFirstName,
										"guestLastName" : data.guestLastName,
										"hostAccountId" : data.hostAccountId,
										"hostFirstName" : data.hostFirstName,
										"hostLastName" : data.hostLastName,
										"hostType" : data.hostType,
										"hostUid" : data.hostUid,
										"invitationStatus" : "Complete",
										"invitationType" : data.invitationType
									};
									reqData = JSON.stringify(reqData);
									var postUrl = mulesoftStorefrontEndpoint
											+ 'accountupdate/'
											+ data.invitationGuId;
									$http
											.post(postUrl, reqData, config)
											.then(
													function successCallback(
															response) {
														// On invite update
														// being successful
														// merge cart if needed
														// and then redirect
														var guestGUID = "";
														if ($cookies
																.get("cart_guid") != undefined
																&& $cookies
																		.get("cart_guid") != "") {
															guestGUID = $cookies
																	.get("cart_guid");
														}
														if (undefined != guestGUID
																&& null != guestGUID
																&& '' != guestGUID) {
															self
																	.mergeUserCart(guestGUID);

														} else {
															self
																	.redirectOnSuccess();
														}

													},
													function errorCallback(
															response) {
														// TODO Global Error
														// handling
													});

								}

								this.sponsor = function(SponsorSearchPagePath) {
									var userInfoString = "";
									var url = $window.location.href;
									userProfileManager
											.setSponsorPageReturnURL(url);

									// Add User Information in the url while
									// redirecting to sponsor search page
									if (self.emailCredentials.userEmailField) {
										userInfoString = "uName="
												+ self.emailCredentials.userEmailField;
									}
									if (self.firstName) {
										if (userInfoString.length > 0) {
											userInfoString = userInfoString
													+ "&fName="
													+ self.firstName;
										} else {
											userInfoString = "fName="
													+ self.firstName;
										}
									}
									if (self.lastName) {
										if (userInfoString.length > 0) {
											userInfoString = userInfoString
													+ "&lName=" + self.lastName;
										} else {
											userInfoString = "lName="
													+ self.lastName;
										}
									}
									if (undefined != SponsorSearchPagePath
											&& null != SponsorSearchPagePath
											&& '' != SponsorSearchPagePath) {
										$window.location = $window.location.protocol
												+ '//'
												+ $window.location.host
												+ SponsorSearchPagePath
												+ '.html?' + userInfoString;
									}
								}

								self.redirectOnSuccess = function() {
									var homePagePathURL = null;
									var returnPageURL = userProfileManager
											.getPageReturnURL();
									var userType = userProfileManager
											.getSignUpUserType();
									if (userType === USER_TYPE.PRIMARY_DISTRIBUTOR) {
										if (returnPageURL) {
											// redirect to Minimal Info page in
											// case of Pre Checkout
											// Authentication flow
											homePagePathURL = angular.element(
													$('#minimalInfo')).val();
										} else {
											homePagePathURL = angular.element(
													$('#distributorRedirect'))
													.val();
										}

									} else if (userType === USER_TYPE.ASSOCIATE) {
										homePagePathURL = angular.element(
												$('#associateRedirect')).val();
									} else if (userType === USER_TYPE.FAMILY_MEMBER
											|| userType === USER_TYPE.PREFERED_CUSTOMER
											|| userType === USER_TYPE.RETAIL_CUSTOMER) {
										if (returnPageURL) {
											homePagePathURL = returnPageURL;
										} else {
											homePagePathURL = angular.element(
													$('#customerRedirect'))
													.val();
										}
									}
									if (undefined != homePagePathURL
											&& null != homePagePathURL
											&& '' != homePagePathURL) {
										var path = homePagePathURL;

										if (homePagePathURL.toLowerCase()
												.indexOf('.html') < 0) {
											path = $window.location.protocol
													+ '//'
													+ $window.location.host
													+ homePagePathURL + '.html';
										}

										if (userType === USER_TYPE.FAMILY_MEMBER
												|| userType === USER_TYPE.PREFERED_CUSTOMER
												|| userType === USER_TYPE.RETAIL_CUSTOMER) {
											path = path + '?newUser=true';
										}

										$window.location = path;
									}
								}

								// Function to merge shopping cart
								self.mergeUserCart = function(guestGUID) {
									var userID = userProfileManager
											.getUserProfileMasterInfo().userId;
									var createCartResponse = cartSharedService
											.createCart(userID);
									createCartResponse
											.then(
													function(data) {
														if (data
																.hasOwnProperty("code")) {
															var mergeCartResponse = cartSharedService
																	.mergeCart(
																			userID,
																			guestGUID);
															mergeCartResponse
																	.then(
																			function(
																					data) {
																				if (data
																						.hasOwnProperty("code")) {
																					var expDate = new Date();
																					expDate
																							.setDate(expDate
																									.getDate() + 1);
																					$cookies
																							.put(
																									"cart_guid",
																									data.code,
																									{
																										expires : expDate,
																										path : '/'
																									});
																					self
																							.redirectOnSuccess();
																				} else {
																					self
																							.redirectOnSuccess();
																				}
																			},
																			function(
																					response) {
																				self
																						.redirectOnSuccess();
																			});
														} else {
															self
																	.redirectOnSuccess();
														}
													},
													function(response) {
														self
																.redirectOnSuccess();
													});
								}

								// Text Messages Related to password component
								angular
										.element(document)
										.ready(
												function() {
													self.passwordDisclaimer = angular
															.element(
																	"#passwordDisclaimer")
															.val();
													self.createPasswordTitle = angular
															.element(
																	"#createPasswordTitle")
															.val();
													self.confirmPasswordTitle = angular
															.element(
																	"#confirmPasswordTitle")
															.val();
													self.minCharTitle = angular
															.element(
																	"#minCharTitle")
															.val();
													self.emptyPasswordError = angular
															.element(
																	"#emptyPasswordError")
															.val();
													self.invalidPasswordError = angular
															.element(
																	"#invalidPasswordError")
															.val();
													self.confirmPasswordError = angular
															.element(
																	"#confirmPasswordError")
															.val();
													self.passwordDoNotMatchError = angular
															.element(
																	"#passwordDoNotMatchError")
															.val();
													self.fixToContinueTitle = angular
															.element(
																	"#fixToContinueTitle")
															.val();
													self.clickToHidePasswordTitle = angular
															.element(
																	"#clickToHidePasswordTitle")
															.val();
													self.clickToShowPasswordTitle = angular
															.element(
																	"#clickToShowPasswordTitle")
															.val();
												});

								/***********************************************
								 * Social login : The Functions and variables
								 * related to social login are given below
								 **********************************************/

								$scope.service = signupService;
								$scope.name = 'social.signup';
								$scope.socialLogin = function() {
									// function to integrate social media api
								}

								$scope.maskFormat = '';
								$scope.maskOptions = {
									allowInvalidValue : true,
									clearOnBlur : false
								}

								var patterns = {
									email : /^([_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,5}))/i,
									phone : /^[1-9]{1}[0-9]{9}$/
								};

								vmp.onChange = onChange;
								$scope.confirmCode = function(
										confirmationCodeValue) {
									if (confirmationCodeValue == 111) {
										// TODO
									}
								}

								function onChange(form, modelValue) {

									$scope.isSubmitted = false;

									if (!form.userDetailField.$modelValue) {
										$scope.maskFormat = '';
									}
									if (form.userDetailField.$modelValue
											&& form.userDetailField.$modelValue.length == 1) {
										if (!isNaN(modelValue)) {
											$scope.maskFormat = '(999) 999-9999';
										}
									}

									$scope.isSubmitted = false;
									$scope.invalidError = false;
									$scope.emptyError = false;

									var isPhone;
									var value = $scope.userDetailField;
									if (!value) {
										return;
									}
									value = value.replace(/[\-()]/g, '');
									var isPhoneNumber = patterns.phone
											.test(value);
									if (isPhoneNumber && value.length == 10) {
										return false;
									}
									var match = patterns.email.test(value)
											|| patterns.phone.test(value);
									if (value.length > 3 && !isNaN(value)) {
										isPhone = true;
									}
									if (isPhone) {
										var telephoneValue = value.replace(
												/[^0-9]/g, '').slice(0, 10);
										var city, remainingNumber;

										switch (telephoneValue.length) {
										case 1:
										case 2:
										case 3:
											city = telephoneValue;

										default:
											city = "("
													+ telephoneValue
															.slice(0, 3) + ")";
											remainingNumber = telephoneValue
													.slice(3);
										}

										if (remainingNumber) {
											if (remainingNumber.length > 3) {
												remainingNumber = remainingNumber
														.slice(0, 3)
														+ "-"
														+ remainingNumber
																.slice(3, 7);
											} else {
												remainingNumber = remainingNumber;
											}
											$scope.userDetailField = (city + remainingNumber)
													.trim();
										} else {
											return "(" + city;
										}
									} else {
									}
								}

								// Function to handle social login
								$scope.socialLogin = function(
										socialLoginProvider) {
									if (undefined != socialLoginProvider
											&& '' != socialLoginProvider) {
										gigya.accounts
												.socialLogin({
													provider : socialLoginProvider,
													extraFields : 'languages,address,phones,education,honors,publications,patents,certifications,professionalHeadline,bio,industry,specialties,work,skills,religion,politicalView,interestedIn,relationshipStatus,hometown,favorites,likes,followersCount,followingCount,username,locale,verified,irank,timezone,samlData',
													facebookExtraPermissions : 'user_birthday,email',
													callback : $scope.socialLoginPostProcessor
												});
									}
								}

								// Callback provided for Sign Up instead of
								// global listener
								// gigya.socialize.addEventHandlers({
								// onLogin:loginPostProcessor,
								// onLogout:logoutPostProcessor });

								$scope.socialLoginPostProcessor = function(
										eventObj) {
									var genericErrorMsg = angular.element(
											$('#genericerrormsg')).val();
									var existingUserErrMsg = angular.element(
											$('#socialexistingusererrmsg'))
											.val();
									if (eventObj.newUser) {
										var promise = $scope.signupModuleSharedService
												.signUp(eventObj.user);
										promise
												.then(function(data) {
													if (data.status === 400) {
														socialSignUpErrorHandler(genericErrorMsg);
													} else if (data.status === 201) {

														var guestGUID = "";
														if ($cookies
																.get("cart_guid") != undefined
																&& $cookies
																		.get("cart_guid") != "") {
															guestGUID = $cookies
																	.get("cart_guid");
														}
														if (undefined != guestGUID
																&& null != guestGUID
																&& '' != guestGUID) {
															self
																	.mergeUserCart(guestGUID);
														} else {
															self
																	.redirectOnSuccess();
														}

													} else if (data.status === 200) {
														socialSignUpErrorHandler(existingUserErrMsg);
													}
												});
									} else {
										socialSignUpErrorHandler(existingUserErrMsg);
									}
								}

								function signUpErrorHandler(message) {
									if (undefined == message
											|| message.length == 0) {
										message = 'Error Occurred!';
									}
									angular.element
											.find('#signupSocialErrorBlock')[0].style.display = "block";
									angular.element
											.find('#signupSocialErrorMsg')[0].innerHTML = message;
									userProfileManager.deleteUserProfile();
								}

							} ]);
})();
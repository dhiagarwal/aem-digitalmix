'use strict';

addessVerificationController.$inject = ['$scope', 'addessVerificationComponentService', '$uibModal', '$rootScope'];
addessVerificationComponentModule.directive('addressVerificationComponent',function(){
	 
	return {
		restrict: 'E',
		scope:{
    	isModalCall: '@',
		modalOpenCount: '=',
		addressData: '=',
		callbackOnSuccess: '@',
		booleanVariableToInvokeModal: '@'
    },
    controller: addessVerificationController,
	controllerAs: 'avCtrl',
        bindToController : true
	}
})
function addessVerificationController($scope, addessVerificationComponentService, $uibModal,$rootScope) {
    this.$uibModal = $uibModal;
    var self = this;
    self.isAddressListVisible = false; 
    self.addressList =[];

   /* var promise = addessVerificationComponentService.getJSONData();
    promise.then(function (response) {
        self.addressList = [modalSelf.addressData];//response.modalData.addressList;
    });*/
    
  /*  model.qasAddress.shippingAddress1 = model.enterAddress.line1;
	  model.qasAddress.shippingAddress2 = model.enterAddress.line2;
	  model.qasAddress.shippingCity = model.enterAddress.city;
	  if(null != model.enterAddress.state && undefined != model.enterAddress.state){
		  var addressStateName = model.enterAddress.state.toLowerCase();
	  model.languageList.forEach(function(entry){
		  if(entry.code === addressStateName){
			  model.qasAddress.shippingState = entry.name;
		  }
	  });
	  }
	 // model.qasAddress.shippingState = model.enterAddress.state;
	  model.qasAddress.shippingPostalCode = model.enterAddress.postalCode;*/
    
    $rootScope.qasCallback = function QAScalled(addresses){
    	self.addressList
    	var tempAddress = {};
    	
    	if(addresses instanceof Array){
    	
    	addresses.forEach(function(item){
    		try{
    		if(null != item.addressText && undefined != item.addressText){
    		var addressLines = item.addressText.split(",");
    		if(addressLines.length == 3){
    		tempAddress.addressLine1 = addressLines[0];
    		tempAddress.addressLine2 = addressLines[1];
    		
    		var cityStateZip = addressLines[2].trim();
			tempAddress.zip = cityStateZip.match('([0-9]+)(-[0-9]+)?')[0];
			cityStateZip = cityStateZip.substring(0, cityStateZip.indexOf(tempAddress.zip));
			tempAddress.state = cityStateZip.substring(cityStateZip.length-2);
			tempAddress.city = cityStateZip.substring(0, cityStateZip.indexOf(tempAddress.state));
    		}
    		self.addressList.push(tempAddress)
    		}
    		
    		}
    		catch(err){
    			console.log(JSON.stringify(item));
    		}
    	});
	    }
	    else if(addresses == "InvalidAddress"){
	    	self.addressInvalid = true;
	    	self.isAddressListVisible = false;
	    }
	    else if(addresses == "InteractionRequired"){
	    	self.isAddressListVisible = false;
	    }
		else if(addresses && addresses instanceof Object){
			self.isAddressListVisible = false;
			modalSelf.useEnteredAddress(addressData);
			$uibModalInstance.close();
		}
    }  
	  
	  callQAS(self.addressData,self.addressList,$rootScope.qasCallback);
	  console.log(JSON.stringify(self.addressList));
    

    self.modalPopupInstance = function (modalTemplateVariable, controller, windowClass) {
        var modalInstance = $uibModal.open({
            animation: true,
            backdrop: 'static',
            keyboard: false,
            templateUrl: modalTemplateVariable,
            controller: controller,
            controllerAs: 'ctrl',
            windowClass: windowClass
        });
    };

    self.addressVerificationModalController = function ($uibModalInstance) {
        var modalSelf = this;
        modalSelf.model = {};

        modalSelf.model.addressList = self.addressList;

        modalSelf.addressData = self.addressData;
        modalSelf.ok = function () {
            $uibModalInstance.close();
        };

        modalSelf.cancel = function () {
            $uibModalInstance.dismiss();
        };

        modalSelf.goBackAndEdit = function() {
            var resetBooleanVariable = '$scope.$parent.' + self.booleanVariableToInvokeModal + ' = false;';
            eval(resetBooleanVariable);
            modalSelf.cancel();
        };

        modalSelf.goWithSelectedAddress = function (isValid, selectedObject) {
            if (isValid) {
                modalSelf.useEnteredAddress(selectedObject);
            }
        };

        modalSelf.useEnteredAddress = function (addressDataObj) {
            var resetBooleanVariable = '$scope.$parent.' + self.booleanVariableToInvokeModal + ' = false;';
            eval(resetBooleanVariable);
            modalSelf.finalAddressData = addressDataObj ? addressDataObj : modalSelf.addressData;
            modalSelf.ok();
            var setCallBackMethod = '$scope.$parent.' + self.callbackOnSuccess + '(modalSelf.finalAddressData)';
            eval(setCallBackMethod);
        };
    };

    if (self.isModalCall) {
        self.modalPopupInstance("/etc/clientlibs/dtc/core/main/js/address-verification/address-verification.component.modal.verification.html", self.addressVerificationModalController, 'app-modal-window address-verification-popup');
    } 

};
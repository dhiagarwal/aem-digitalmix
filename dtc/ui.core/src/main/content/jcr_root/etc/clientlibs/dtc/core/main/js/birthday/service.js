'use strict';

birthdayModule.service('birthdayService' ,['$http', '$q', '$window', function($http,$q,$window){

    this.$http = $http;
    var deferred = $q.defer();

    this.getAgeLimit = function() {

    	var getUrl = $window.location.protocol+'//'+$window.location.host+'/dtc/signup/config';
		$http.get(getUrl).then(function(response) {
            deferred.resolve(response.data);
    	});
        return deferred.promise;
    }
   
}])

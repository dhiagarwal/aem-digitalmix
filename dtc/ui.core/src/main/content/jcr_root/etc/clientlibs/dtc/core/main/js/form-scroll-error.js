'use strict';

var formErrorScrollModule = angular.module('formErrorScrollModule', []);

formErrorScrollModule.directive('scrollForError', function(){
	return {
        restrict: 'A',
        require: '^form',
        link: function(scope, ele, attr, formCtrl){
            ele.bind('submit', function(){
                var errorField;
                if(formCtrl.$error.required && formCtrl.$error.required.length) {
                   var fieldIdentified = angular.element(document.querySelector('input.ng-invalid')).offset().top;
                   angular.element(document.querySelector('body')).animate({
				            scrollTop: (fieldIdentified-30)
				        }, 1000
				    );
                }
            });
        }
    }
});
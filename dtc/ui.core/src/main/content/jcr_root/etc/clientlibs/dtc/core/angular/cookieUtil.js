function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    if(exdays != 0){
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
    }
    $.cookie(cname,cvalue,{path: '/',expires: d});
}

function getCookie(cname) {
    if(cname){
        if($.cookie(cname)){
            return $.cookie(cname);
        }else{
            return "";
        }
    }
}

function deleteCookie(cname){
    $.removeCookie(cname, { path: '/' });
}
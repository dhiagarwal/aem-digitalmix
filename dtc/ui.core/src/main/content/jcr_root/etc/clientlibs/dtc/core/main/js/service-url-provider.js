app.provider("serviceURL", function () {
	
	var cartURL = '{0}v2/{1}/users/{2}/carts/{3}?fields=FULL';
	
	var solrProductDataURL = mulesoftSolrEndpoint + 'oak/select?q={0}';
	
		
	var ratingsURL = gigyaCommentsDomain + 'comments.getComments?format=jsonp&callback=JSON_CALLBACK&apiKey=' + gigyaApiKey + '&categoryID=Products&streamID={0}&includeStreamInfo=true&threadLimit=1';
	
	var cartRecommendationsURL = '/bin/dtc/cartRecommendations?skuId={0}&path={1}';
	
	var productsPriceURL = '{0}v2/{1}/users/{2}/prices/multiple/all?productCodes={3}';
	
	var commentsURL = ratingsURL; // Same as rating URL
	
	var hasRatedURL = gigyaCommentsDomain + 'comments.getUserComments?format=jsonp&callback=JSON_CALLBACK&senderUID={0}' +'&apiKey=' +	gigyaApiKey + 
	 '&categoryID={2}' + '&streamID={1}';
	
	var getImageDataURL='/bin/dtc/imageDataServlet?skuid={0}';
	
	var getImageDataPathParamURL='/bin/dtc/imageDataServlet?skuid={0}&path={1}';
	
	var applyVoucherURL ='{0}v2/{1}/users/{2}/carts/{3}/vouchers?voucherId={4}';
	
	var removeVoucherURL='{0}v2/{1}/users/{2}/carts/{3}/vouchers/{4}';
	
	var getDeliveryOptionsURL='{0}v2/{1}/users/{2}/carts/{3}/deliverymodes';
	
	var postDeliveryOptionsURL='{0}v2/{1}/users/{2}/carts/{3}/deliverymode?deliveryModeId={4}';
	
	var fetchCardsURL=mulesoftStorefrontEndpoint+'usercards/{0}?now={1}';
	
	var getPageLocaleValURL='/bin/dtc/pageLocaleValue?resourcePath={0}';
	
	var getQuickSearchDataURL=mulesoftSolrEndpoint + 'oak/prodctSuggest?spellcheck.q={0}';
	
	var createCartURL='{0}v2/{1}/users/{2}/carts';
	
	var addProductToCartURL='{0}v2/{1}/users/{2}/carts/{3}/multipleEntries';
	
	var placeOrderURL='{0}v2/{1}/users/{2}/orders?cartId={3}&securityCode={4}';
	
	var getOrderDetailsURL = '{0}v2/{1}/users/{2}/orders/{3}?fields=FULL';
	
	var subCategoriesUrl='/etc/tags/{0}';
	
	/*Billing URL*/
	var paymentDetailsQueryParams='accountHolderName={4}&cardNumber={5}&cardType={6}&expiryMonth={7}&expiryYear={8}&billingAddress.firstName={9}'+
	'&billingAddress.lastName={10}&billingAddress.line1={11}&billingAddress.line2={12}&billingAddress.postalCode={13}'+
	'&billingAddress.town={14}&billingAddress.country.isocode={15}&billingAddress.region.isocode={16}';	
	var setPaymentDetailsURL ='{0}v2/{1}/users/{2}/carts/{3}/paymentdetails?' + paymentDetailsQueryParams;
	/**/
	
	/*Shipping URL*/
	var shippingQueryParams= 'accountHolderName={4}&cardNumber={5}&cardType={6}&expiryMonth={7}&expiryYear={8}&sameAsDeliveryAddress={9}';
	var setPaymentShippingURL ='{0}v2/{1}/users/{2}/carts/{3}/paymentdetails?' + shippingQueryParams;
	/**/
	
	var getIngredientsURL='/bin/dtc/productsIngredients?path={0}';
	
	var getCrossSellProductsURL='/bin/dtc/crossSell?skuId={0}&path={1}';
	
	var getUpSellProductsURL='/bin/dtc/upSell?skuId={0}&path={1}';

    var getUpSellKitProductURL='/bin/dtc/upSellKit?skuId={0}&path={1}';
	
	var stockLevelURL='{0}v2/{1}/users/{2}/stockLevel';
	
    var ratingsDescOrderURL=ratingsURL+'&sort=dateDesc&timestamp={1}';
	
	var getTestimonialIconData='{0}/{1}/_jcr_content.list.json';
	
	/* DS storefront URL */
	var getFeaturedProductsURL = '/dtc/featured/products?market={0}&language={1}';
	
	/*Save Address*/
	var saveAddressQueryParams='firstName={4}&lastName={5}&titleCode={6}&line1={7}&line2={8}&town={9}&postalCode={10}&phone={11}&email={12}'
		+ '&country.isocode={13}&region.isocode={14}';
	var saveAddress='{0}v2/{1}/users/{2}/carts/{3}/addresses/delivery?' + saveAddressQueryParams;
	/**/
	
	var removeProductURL='{0}v2/{1}/users/{2}/carts/{3}/removeProduct/{4}';
	
	var updateQuantityURL='{0}v2/{1}/users/{2}/carts/{3}/updateQuantity/{4}';
	
	var mbsSaveContinueURL='{0}brandstory/{1}';
	
	var mbsUpdateURL='{0}updatebrandstory/{1}';
	
	var mbsGetStoryURL='{0}userbrandstory/{1}';
	
	var getAssetDetailsURL ='{0}oak/select';
	
	var updateDownloadCountURL = '/bin/digitalkit/media-center/updateDownloadCount?path={0}'

	var setFavoriteURL ='{0}create/favourite';
	
	var unsetFavoriteURL ='{0}assets/undofavourite';	
	
	var getRatingsURL= gigyaCommentsDomain +'comments.getComments?format=jsonp&callback=JSON_CALLBACK&apiKey=' + gigyaApiKey +'&categoryID=Assets&streamID={0}&includeStreamInfo=true&threadLimit=1&sort=dateDesc&timestamp={1}';
	
	var setRatingsURL= gigyaCommentsDomain +'comments.postComment?format=jsonp&callback=JSON_CALLBACK&uid={0}&apiKey=' + gigyaApiKey +'&userKey=' + gigyaUserKey
					+ '&secret=' + gigyaSecretKey + '&categoryID=Assets&streamID={1}&ratings={\'_overall\': {2}}';
	
	var setProductRatingsURL = setRatingsURL.replace("Assets", "Products")+'&commentText={3}&tags=[\'Recommended{4}\']';
	
	var updateRatingsURL = '/bin/digitalkit/media-center/updateRatings?path={0}&ratingsCount={1}&avgRating={2}';
	
	var getGigyaPropsURL = '/bin/gigyaProperties';
	
	var getRenditionsURL = '/bin/digitalkit/media-center/getRenditions?path={0}';
	
	var getAssetDetailsUGCurl='{0}asset/{1}';
	
	var getUserPreferredNameURL='{0}v2/dtc_uk/users/sponsor_1/profile/personal';
	
	var deleteAssetURL = '{0}asset/{1}/{2}';
	
	var getContibutorAssetSizeURL = '{0}assets/size/{1}';
	
	var updateAssetMetadataURL = '{0}updateasset/{1}';
	
	var getTagsSuggestionListURL = '/bin/digitalkit/media-center/tagsTypeahead?queryTag={0}';
	
	var callEncodingManagerURL = '/bin/digitalkit/media-center/callUGCEncoder?fileKey={0}';
	
	var getS3PropsURL = '/bin/digitalkit/media-center/s3Properties';
	
	var fetchCardsURL = '{0}usercards/{1}';
	
	var removeCardURL = '{0}create/usercards';
	
	var removeSprinklrAssetURL = '/bin/dtc/deleteAsset?editItem=true&SprinklrAssetid={0}';
	
	var facetedSearchURL = '/dtc/facetedSearch';
	
	var callnextsetdataURL = '/bin/digitalkit/media-center/loadMoreData?counter={0}&firsttime={1}&primaryPath={2}&category={3}&initialCounter={4}&rows={5}&carouselAssetIds={6}'
	
	var callmycontentnextsetdataURL = '/bin/digitalkit/mycontent/loadMoreData?counter={0}&initialCounter={1}&rows={2}';

	var getQuickSearchDataURL_mediaCenter = '{0}oak/suggest_phrase?spellcheck.q={1}';
	
	var getSearchResultsURL = '/dtc/search';
	
	var callQueryCarouselURL = '{0}oak/select';
	
	var postQuizURL = '{0}v1/users/{1}/quizzes';
	
	var updateQuizURL = '{0}v1/users/{1}/quizzes/{2}';
	
	var storeQuizURL = '{0}v2/dtc_uk/users/{1}/storequiz';

	var storefrontOwnerURL = '/users/{0}.json';

    var getPitchURL = '/users/{0}/pitches/{1}.json';
	
	var callnextsettoprateddataURL='/bin/digitalkit/top-rated/loadMoreData?counter={0}&fullpage={1}&initialCounter={2}&rows={3}';
	
		return {
		    $get: function () {
		      return {
		          GetServiceURL: function(serviceConstantURLName, paramsArray){

		          	var requestURL = eval(serviceConstantURLName);

		          	for(var i = 1; i <= paramsArray.length; i++) {
                        requestURL = requestURL.replace(new RegExp('\\{' + (i-1) + '\\}', 'gi'), paramsArray[i-1]);
                    }

    				return requestURL;
		          }
		      }
		    }
		};
});
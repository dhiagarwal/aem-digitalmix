'use strict';
;(function(){
 videoComponent.controller('videoController', ['videoComponentService', '$timeout', '$rootScope', 'userProfileManager', function (videoComponentService, $timeout, $rootScope, userProfileManager) {
  var self = this;
  this.$timeout = $timeout;
  self.videoPlaying = false;
  self.videoBannerShow = true;
  self.cardDismissShow = false;
  self.useType = "signup";

  //self.name = userProfileManager.getUserProfileDetailInfo()['profileHeader']['firstName'];

  var videoControl = angular.element.find('#video-1');
  videoControl[0].controls = false;

  self.onVideoEnd = function($event) {
    self.closeVideo("close");
  };

  self.closeVideo = function(accessFrom) {
    if(accessFrom == "close"){
      self.videoBannerShow = false;
    } else{
      self.videoPlaying = false;
      self.videoBannerShow = true;
      videoControl[0].controls=false;
    }
  };

  self.togglePlay = function() {
      var video = angular.element.find('#video-1');
      if (video[0].paused) {
          video[0].play();
          video[0].controls=true;
          self.videoPlaying = true;
      } else {
    	  $timeout(function(){video[0].pause();},10)
          video[0].controls=false;
          self.videoPlaying = false;
      }
  };
}])
})();



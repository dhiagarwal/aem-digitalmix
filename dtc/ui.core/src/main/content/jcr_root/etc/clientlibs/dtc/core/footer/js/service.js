;(function(){
footerModule.service('footerservice', ['$http', '$q', '$window', 'userProfileManager', function ($http,$q,$window,userProfileManager){
  this.$http = $http;

  var deferred = $q.defer();

			this.getDefaultCommPrefs = function() {

						var getUrl = $window.location.protocol + '//'
								+ $window.location.host
								+ '/mecommerce/signup/config';
						$http.get(getUrl).then(function(response) {
							deferred.resolve(response.data);
						});
						return deferred.promise;
					}
			
			this.getDefaultCommPrefs = function() {

				var getUrl = $window.location.protocol + '//'
						+ $window.location.host
						+ '/mecommerce/signup/config';
				$http.get(getUrl).then(function(response) {
					deferred.resolve(response.data);
				});
				return deferred.promise;
			}

			 this.regionLang = function(regionVal,langVal,currentPageVal,defaultLangVal) {
				   var deferred = $q.defer();
			        $http({
			            method: 'GET',
			            url: '/bin/mecommerce/RegionLangPath',
			            params : {
							regionVal : regionVal,
							langVal: langVal,
							currentPageVal : currentPageVal,
							currentPageLang : defaultLangVal,
							currentPageRegion: userProfileManager. getPageLocaleVal().market
						}
			        }).then(function successCallback(response) {
			            deferred.resolve(response.data);
			        });
			        return deferred.promise;
			    }
}])
})();

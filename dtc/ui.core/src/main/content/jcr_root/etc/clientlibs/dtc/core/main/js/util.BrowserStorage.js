(function () {
	'use strict';

	var LOGGER = log4javascript.getLogger('util.BrowserStorage');

	eBizCommons.namespace('util');

	/**
	 * Provides an easy way to store properties to local storage and an api to consult for expiration
	 * times to renew properties values with flexible configuration for several usages. Uses jQuery Total Storage
	 *
	 * @see https://github.com/Upstatement/jquery-total-storage
	 * @see http://log4javascript.org/
	 * @author Matias Favale
	 */
	eBizCommons.util.BrowserStorage = (function () {
		var _STORAGE_RESOURCES = {
			LOCAL_STORAGE: 'local_storage',
			SESSION_STORAGE: 'session_storage'
		};

		var _EXP_STRATEGIES = {
			/**
			 * Regular Timeout expiration
			 */
			TIMEOUT: 'timeout',
			/**
			 * This would be to store it only when user is logged in and have it removed when logged out.
			 */
			USER_SCOPE: 'user_scope',
			/**
			 * No expiration at all
			 */
			NO_EXPIRATION: 'no_expiration'
		};

		var DEFAULT_NATIVE_OPTIONS = {
			storageResource: _STORAGE_RESOURCES.LOCAL_STORAGE,
			expStrategy: _EXP_STRATEGIES.NO_EXPIRATION,
			// 30 minutes
			expTimeout: 1800000,
			// Function to obtain the User login time for expiration strategy USER_SCOPE
			// This is REQUIRED to use this strategy, there is no native library implementation
			expGetUserLoginDatetime: null,
			// Determines whether to remove a certain property from storage if validation fails. True will remove it.
			removeWhenInvalid: true
		};

		var _options;
		var _instance;

		function _logAndThrowError(msg) {
			LOGGER.error(msg);
			throw new Error(msg);
		}

		function _logAndThrowTypeError(msg) {
			LOGGER.error(msg);
			throw new TypeError(msg);
		}

		function _createInstance(instanceOpts) {
			LOGGER.info('Creating BrowserStorage instance ...');
			if (!_.isUndefined(instanceOpts) && !_.isNull(instanceOpts) && !_.isPlainObject(instanceOpts)) {
				_logAndThrowTypeError('Instance configuration must be an object.');
			}

			LOGGER.trace('Native options: ' + JSON.stringify(DEFAULT_NATIVE_OPTIONS, null, 4));
			_options = _.clone(DEFAULT_NATIVE_OPTIONS);

			var validInstanceOpts = _getValidOptions(DEFAULT_NATIVE_OPTIONS, instanceOpts);
			LOGGER.debug('Overriding native options with the following valid instance options: ',
				JSON.stringify(validInstanceOpts, null, 4));
			_.assign(_options, validInstanceOpts);

			LOGGER.trace('Instance options: ' + JSON.stringify(DEFAULT_NATIVE_OPTIONS, null, 4));

			LOGGER.info('Instance created successfully.');
			return publicAttribs;
		}

		function _isParamTypeValid(parameter, type) {
			switch (type) {
				case 'string':
					return _.isString(parameter);
				case 'non-empty-string':
					return (_.isString(parameter) && !_.isEmpty(parameter));
				case 'array':
					return _.isArray(parameter);
				case 'object':
					return _.isObject(parameter);
				case 'number':
					return _.isNumber(parameter);
				case 'integer':
					return _.isInteger(parameter);
				case 'positive-integer':
					return (_.isInteger(parameter) && parameter > 0);
				case 'date':
					return _.isDate(parameter);
				case 'function':
					return _.isFunction(parameter);
				case 'boolean':
					return _.isBoolean(parameter);
				default:
					LOGGER.warn('Unrecognized type ' + type + ' when validating type of ' + parameter);
					return false;
			}
		}

		function _validateRequiredParam(paramName, value) {
			if (_isNil(value)) {
				_logAndThrowError('Parameter ' + paramName + ' ' + 'is required');
			}
			return true;
		}

		function _validateInputParamsTypes(parameters, validTypes) {
			var params = _.isArray(parameters) && !_.isEmpty(parameters) ? parameters : [parameters];
			var vTypes = _.isArray(validTypes) && !_.isEmpty(validTypes) ? validTypes : [validTypes];

			_.forEach(params, function (param) {
				var valid = false;
				_.forEach(vTypes, function (type) {
					if (_isParamTypeValid(param, type)) {
						valid = true;
					}
				});
				if (!valid) {
					_logAndThrowTypeError('Invalid parameter. Failed validations: \n parameter with value ' + param +
						' is not type(s) {' + vTypes.join(',') + '}.\n');
				}
			});
		}

		// private function to build the unique property key from name and namespace.
		function _buildPropKey(keyName, keyNameSpace) {
			var prefix = (!_.isUndefined(keyNameSpace) && !_.isEmpty(keyNameSpace)) ?
				keyNameSpace + '-' : '';
			return prefix + keyName;
		}

		function _store(propKey, value, callOptions) {
			var now = Date.now();
			var objToStore = {
				storedTimestamp: new Date(now),
				payload: value
			};

			var validOptions = _getValidOptions(_options, callOptions);
			if (!_isNil(validOptions) && !_.isEmpty(validOptions)) {
				objToStore.options = validOptions;
			}

			var _callOpts = _getOptionsForProp(objToStore);

			if (!_isNil(_callOpts.expStrategy) && _callOpts.expStrategy !== _EXP_STRATEGIES.NO_EXPIRATION) {
				if ((_.isArray(_callOpts.expStrategy) &&
					_.findIndex(_callOpts.expStrategy, _EXP_STRATEGIES.TIMEOUT) > 0) ||
					(_.isString(_callOpts.expStrategy) && _callOpts.expStrategy === _EXP_STRATEGIES.TIMEOUT)) {
					var expTimeInMillis = new Date(now + _callOpts.expTimeout);
					objToStore.expTimestamp = new Date(expTimeInMillis);
				}
			}

			LOGGER.info('Storing property ' + propKey + ' on ' + _callOpts.storageResource + '...');
			LOGGER.debug(propKey + ' storing value: ' + JSON.stringify(objToStore, null, 4));
			if (_callOpts.storageResource === _STORAGE_RESOURCES.LOCAL_STORAGE) {
				$.totalStorage(propKey, objToStore);
			} else if (_callOpts.storageResource === _STORAGE_RESOURCES.SESSION_STORAGE) {
				window.sessionStorage.setItem(propKey, JSON.stringify(objToStore));
			} else {
				_logAndThrowError('Unexpected storage resource configuration ' + _callOpts.storageResource +
					', could not store ' + propKey);
			}
		}

		function _getStoredDate(property) {
			if (_.isUndefined(property) || _.isNull(property) || _.isUndefined(property.storedTimestamp) ||
				_.isNull(property.storedTimestamp)) {
				return null;
			} else {
				return new Date(property.storedTimestamp);
			}
		}

		function _getExpirationDate(prop) {
			if (_isNil(prop) || _isNil(prop.expTimestamp)) {
				return null;
			} else {
				return new Date(prop.expTimestamp);
			}
		}

		// Checks if the property value was created by this BrowserStorage, respecting the format used.
		function _respectsFormat(prop) {
			return (_.isObject(prop) && prop.hasOwnProperty('storedTimestamp') && prop.hasOwnProperty('payload'));
		}

		function _validateExpirationStrategyTimeout(prop) {
			LOGGER.debug('Validating expiration for property ' + JSON.stringify(prop, null, 4));
			var expDate = _getExpirationDate(prop);
			var now = Date.now();
			LOGGER.trace('Comparing expiration datetime <' + expDate + '> vs now <' + (new Date(now)) + '>.');
			return (!_isNil(expDate) && (((new Date(expDate)).getTime() - now) >= 0));
		}

		function _validateExpirationStrategyUserScope(prop, callOptions) {
			var userLoginTime = _getUserLoginDatetime(callOptions);
			var storedDate = _getStoredDate(prop);
			return (!_isNil(userLoginTime) && _.isDate(userLoginTime)
				&& !_isNil(storedDate) && _.isDate(storedDate) && (storedDate.getTime() - userLoginTime) > 0);
		}

		function _validateStoringOptions(storingOptions) {
			if (storingOptions.hasOwnProperty('expGetUserLoginDatetime')) {
				_logAndThrowError('Property expGetUserLoginDatetime can only be set as instance configuration, not when' +
					' storing a property.');
			}
		}

		function _isExpired(propKey, property) {
			if (!_isStoredAndRespectsFormat(property)) {
				return true;
			}
			var _callOpts = _getOptionsForProp(property);
			var strategies = _.isArray(_callOpts.expStrategy) ? _callOpts.expStrategy :
				[_callOpts.expStrategy];

			if (_.includes(strategies, _EXP_STRATEGIES.NO_EXPIRATION)) {
				LOGGER.warn('Property ' + propKey + ' has a ' + _EXP_STRATEGIES.NO_EXPIRATION +
					' strategy, all other strategies are ignored.');
				return false;
			} else {
				var isExpired = false;
				_.forEach(strategies, function (strategy) {
					switch (strategy) {
						case _EXP_STRATEGIES.TIMEOUT:
							if (!_validateExpirationStrategyTimeout(property)) {
								LOGGER.warn('Property ' + propKey +
									' is expired due to timeout expiration strategy. Timeout interval: ' +
									_callOpts.expTimeout + ' ms.');
								isExpired = true;
							}
							break;
						case _EXP_STRATEGIES.USER_SCOPE:
							if (!_validateExpirationStrategyUserScope(property, _callOpts)) {
								LOGGER.warn('Property ' + propKey +
									' is expired due to user scope expiration strategy.');
								isExpired = true;
							}
							break;
						default:
							LOGGER.warn('Unrecognized expiration strategy ' + strategy + ', ignoring.');
							break;
					}
				});
				return isExpired;
			}
		}

		function _isStored(property) {
			return !_isNil(property);
		}

		function _isStoredAndRespectsFormat(property) {
			return !_isNil(property) && _respectsFormat(property);
		}

		function _isValid(propKey, property) {
			var _callOpts = _getOptionsForProp(property);
			if (!_isStored(property)) {
				LOGGER.warn('Property ' + propKey + ' is not stored.');
				return false;
			}
			if (!_respectsFormat(property)) {
				if (_callOpts.removeWhenInvalid) {
					LOGGER.warn('Property ' + propKey + ' does not respects BrowserStorage format. Removing from LS.');
					_remove(propKey, _callOpts.storageResource);
				}
				return false;
			}
			if (_isExpired(propKey, property)) {
				if (_callOpts.removeWhenInvalid) {
					LOGGER.warn('Property ' + propKey + ' is expired. Removing from LS.');
					_remove(propKey);
				}
				return false;
			}
			return true;
		}

		function _getUserLoginDatetime(callOpts) {
			if (_isNil(callOpts.expGetUserLoginDatetime)) {
				_logAndThrowError('expGetUserLoginDatetime parameter is required for USER_SCOPE expiration strategy and ' +
					'there is no native library configuration.');
			}
			return callOpts.expGetUserLoginDatetime();
		}

		function _getProperty(propKey, storageResource) {
			storageResource = storageResource || _options.storageResource;
			LOGGER.info('Obtaining property with key ' + propKey + ' from resource ' + storageResource);
			switch (storageResource) {
				case _STORAGE_RESOURCES.LOCAL_STORAGE:
					return $.totalStorage(propKey);
				case _STORAGE_RESOURCES.SESSION_STORAGE:
					return JSON.parse(window.sessionStorage.getItem(propKey));
				default:
					return null;
			}
		}

		/**
		 * Removes a property with given `propKey` and a `storageResource`. Also removes metadata for that propKey
		 * in _callOptions.
		 *
		 * @private
		 * @param {String} propKey The complete property key.
		 * @param {String} storageResource The storage resource.
		 */
		function _remove(propKey, storageResource) {
			var _storageResource = storageResource || _options.storageResource;
			LOGGER.info('Removing ' + propKey + ' from ' + _storageResource);
			if (_storageResource === _STORAGE_RESOURCES.LOCAL_STORAGE) {
				if ($.totalStorage.removeItem) {
					$.totalStorage.removeItem(propKey);
				} else if (window.localStorage && window.localStorage.removeItem) {
					localStorage.removeItem(propKey);
				} else {
					$.totalStorage(propKey, null);
				}
			} else if (_storageResource === _STORAGE_RESOURCES.SESSION_STORAGE) {
				window.sessionStorage.removeItem(propKey);
			}
		}

		/**
		 * Removes all properties that starts with given `keyNameSpace` and `storageResource(s)`.
		 *
		 * @private
		 * @param {String} keyNameSpace The property key namespace.
		 * @param {String|Array} storageResource The storage(s) resource(s) to remove from.
		 */
		function _removeAll(keyNameSpace, storageResource) {
			LOGGER.debug('Removing all properties' +
				(!_isNil(keyNameSpace) ? ' starting with \'' + keyNameSpace + '\'' : '')
				+ (!_isNil(storageResource) ? ' in ' + storageResource : '') + '...');
			if (_isNil(keyNameSpace) || _.isEmpty(keyNameSpace)) {
				if (_isNil(storageResource) || _.isEmpty(storageResource)) {
					_clearLocalStorage();
					_clearSessionStorage();
				} else if (storageResource === _STORAGE_RESOURCES.LOCAL_STORAGE) {
					_clearLocalStorage();
				} else if (storageResource === _STORAGE_RESOURCES.SESSION_STORAGE) {
					_clearSessionStorage();
				}
			} else {
				if (_isNil(storageResource)) {
					_.forOwn(window.localStorage, function (value, key) {
						if (_startsWith(key, keyNameSpace)) {
							_remove(key, _STORAGE_RESOURCES.LOCAL_STORAGE);
						}
					});
					_.forOwn(window.sessionStorage, function (value, key) {
						if (_startsWith(key, keyNameSpace)) {
							_remove(key, _STORAGE_RESOURCES.SESSION_STORAGE);
						}
					});
				} else {
					var srs = _.isArray(storageResource) ? storageResource : [storageResource];
					_.forEach(srs, function (sr) {
						switch (sr) {
							case _STORAGE_RESOURCES.LOCAL_STORAGE:
								_.forOwn(window.localStorage, function (value, key) {
									if (_startsWith(key, keyNameSpace)) {
										_remove(key, _STORAGE_RESOURCES.LOCAL_STORAGE);
									}
								});
								break;
							case _STORAGE_RESOURCES.SESSION_STORAGE:
								_.forOwn(window.sessionStorage, function (value, key) {
									if (_startsWith(key, keyNameSpace)) {
										_remove(key, _STORAGE_RESOURCES.SESSION_STORAGE);
									}
								});
								break;
						}
					});
				}
			}
		}

		/**
		 * Clears all local storage properties.
		 *
		 * @private
		 */
		function _clearLocalStorage() {
			LOGGER.debug('Clearing all local storage properties.');
			window.localStorage.clear();
		}

		/**
		 * Clears all session storage properties.
		 *
		 * @private
		 */
		function _clearSessionStorage() {
			LOGGER.debug('Clearing all local session properties.');
			window.sessionStorage.clear();
		}

		function _getPropertyValue(propKey, storageResource) {
			var _storageResource = storageResource || _options.storageResource;

			var property = _getProperty(propKey, _storageResource);
			if (!_isValid(propKey, property)) {
				LOGGER.error('Cannot get property ' + propKey + ', is invalid.');
				return null;
			} else {
				return property.payload;
			}
		}

		function _getValidOptions(currentOptions, newOptions) {
			var validOptions = {};
			_.forEach(newOptions, function (optionObject, optionKey) {
				if (currentOptions.hasOwnProperty(optionKey)) {
					if (currentOptions[optionKey] !== optionObject) {
						validOptions[optionKey] = optionObject;
					}
				} else {
					LOGGER.warn('Unrecognized configuration with key ' + optionKey);
				}
			});
			return validOptions;
		}

		function _mergeOptions(target, source) {
			return _.assign(_.clone(target), source);
		}

		function _getOptionsForProp(property) {
			if (_isNil(property)) {
				return null;
			} else if (_isNil(property).options || _.isEmpty(property.options)) {
				return _options;
			} else {
				return _mergeOptions(_options, property.options);
			}
		}

		/*
		 Underscore/lodash wrapped functions
		 These should be removed and use the lodash native functions once we are using the lodash library instead of underscore
		 BEGIN
		 */
		function _isNil(object) {
			if (_.hasOwnProperty('isNil')) {
				return _.isNil(object);
			} else {
				return (_.isUndefined(object) || _.isNull(object));
			}
		}

		function _startsWith(string, partial) {
			if (_.hasOwnProperty('_startsWith')) {
				return _.startsWith(string, partial);
			} else {
				return new RegExp('^' + partial).test(string);
			}
		}

		/* LODASH WRAPPERS END */

		var publicAttribs = {
			/**
			 * Store a property by providing it's value.
			 *
			 * @public
			 * @param {String} keyName the key name for the property.
			 * @param {*} value the value of the property to be stored
			 * @param {String} [keyNameSpace=NoNamespace] Optional namespace to assign to the property.
			 * @param {Object} [options] Options object. @see <options> documentation.
			 * @throws Error when required parameters are not provided, null or undefined.
			 * @throws TypeError when input parameters types are incorrect.
			 */
			store: function (keyName, value, keyNameSpace, options) {
				_validateRequiredParam('keyName', keyName);
				_validateRequiredParam('value', value);
				_validateInputParamsTypes(_isNil(keyNameSpace) ? keyName : [keyName, keyNameSpace],
					'non-empty-string');
				if (!_isNil(options)) {
					_validateInputParamsTypes(options, 'object');
					_validateStoringOptions(options);
				}

				_store(_buildPropKey(keyName, keyNameSpace), value, options);
			},

			/**
			 * Remove property with given name and namespace from local storage.
			 *
			 * @public
			 * @param {String} keyName the key name for the property.
			 * @param {String} [keyNameSpace=None] Optional namespace to assign to the property.
			 * @param {String|Array} [storageResource] Optional Storage resource to remove the property from.
			 * @throws Error when required parameters are not provided, null or undefined.
			 * @throws TypeError when input parameters types are incorrect.
			 */
			remove: function (keyName, keyNameSpace, storageResource) {
				_validateRequiredParam('keyName', keyName);
				_validateInputParamsTypes(_isNil(keyNameSpace) ? keyName : [keyName, keyNameSpace],
					'non-empty-string');
				if (!_isNil(storageResource)) {
					_validateInputParamsTypes(storageResource, ['non-empty-string', 'array']);
				}
				//TODO: validate storageResource values

				var propKey = _buildPropKey(keyName, keyNameSpace);
				if (_.isArray(storageResource)) {
					_removeAll(propKey, storageResource);
				} else {
					_remove(propKey, storageResource);
				}
			},

			/**
			 * Remove several properties with given namespace and/or storage(s) resource(s).
			 *
			 * @public
			 * @param {String} [keyNameSpace=All] Optional namespace to assign to the property. If not specified
			 * remove all properties from the storage(s) resource(s).
			 * @param {String|Array} [storageResource] Optional Storage resource to remove the properties from. If not specified
			 * remove properties from all storage(s) resource(s).
			 * @throws TypeError when input parameters types are incorrect.
			 */
			removeAll: function (keyNameSpace, storageResource) {
				if (!_isNil(keyNameSpace)) {
					_validateInputParamsTypes(keyNameSpace, 'non-empty-string');
				}
				if (!_isNil(storageResource)) {
					_validateInputParamsTypes(storageResource, ['non-empty-string', 'array']);
				}

				_removeAll(keyNameSpace, storageResource);
			},

			/**
			 * Get property by key name, key namespace and/or optionally the storage resource validating if property is valid based on
			 * {@link isValid} method. If it is invalid, property may be cleared based on `PropertyOptions.removeWhenInvalid` option.
			 *
			 * @public
			 * @param {String} keyName the key name for the property.
			 * @param {String} [keyNameSpace=None] Optional namespace to assign to the property.
			 * @param {String} [storageResource=InstanceOptions.storageResource] Optional Storage resource to get the property from.
			 * @return {*} the property value or null if it is not valid (see {@link isValid}).
			 * @throws Error when input parameters are invalid and when property is expired.
			 * @throws TypeError when input parameters types are incorrect.
			 */
			getProperty: function (keyName, keyNameSpace, storageResource) {
				_validateRequiredParam('keyName', keyName);
				_validateInputParamsTypes(_isNil(keyNameSpace) ? keyName : [keyName, keyNameSpace],
					'non-empty-string');
				if (!_isNil(storageResource)) {
					_validateInputParamsTypes(storageResource, 'non-empty-string');
				}
				return _getPropertyValue(_buildPropKey(keyName, keyNameSpace), storageResource);
			},

			/**
			 * Returns a boolean whether the property is stored on local storage or not.
			 *
			 * @public
			 * @param {String} keyName the key name for the property.
			 * @param {String} [keyNameSpace=None] Optional namespace to assign to the property.
			 * @param {String} [storageResource=InstanceOptions.storageResource] Optional Storage resource from where to check if stored. If not sent,
			 * assumes the current instance options default storage resource.
			 * @returns {boolean}
			 * @throws Error when input parameters are invalid.
			 * @throws TypeError when input parameters types are incorrect.
			 */
			isStored: function (keyName, keyNameSpace, storageResource) {
				_validateRequiredParam('keyName', keyName);
				_validateInputParamsTypes(_isNil(keyNameSpace) ? keyName : [keyName, keyNameSpace],
					'non-empty-string');
				if (!_isNil(storageResource)) {
					_validateInputParamsTypes(storageResource, 'non-empty-string');
				}
				var propKey = _buildPropKey(keyName, keyNameSpace);
				return _isStoredAndRespectsFormat(_getProperty(propKey, storageResource));
			},

			/**
			 * Returns a boolean indicating if the property expired or not, depending on the expiration configured when
			 * stored. If it is expired, property may be cleared based on `PropertyOptions.removeWhenInvalid` option.
			 *
			 * @public
			 * @param {String} keyName the key name for the property.
			 * @param {String} [keyNameSpace=None] Optional namespace to assign to the property.
			 * @param {String} [storageResource=InstanceOptions.storageResource] Optional Storage resource from which property to check expiration. If
			 * not sent, assumes the current instance options default storage resource.
			 * @returns {boolean} true if property expired, false if it did not or if it is not stored.
			 * @throws Error when input parameters are invalid.
			 * @throws TypeError when input parameters types are incorrect.
			 */
			isExpired: function (keyName, keyNameSpace, storageResource) {
				_validateRequiredParam('keyName', keyName);
				_validateInputParamsTypes(_isNil(keyNameSpace) ? keyName : [keyName, keyNameSpace],
					'non-empty-string');
				if (!_isNil(storageResource)) {
					_validateInputParamsTypes(storageResource, 'non-empty-string');
				}
				var propKey = _buildPropKey(keyName, keyNameSpace);
				return _isExpired(propKey, _getProperty(propKey, storageResource));
			},

			/**
			 * Checks that property {@link isStored}; if stored checks if the current value respects this handler's format
			 * (property value must be an Object, have storedTimestamp and payload object attributes); if it does
			 * respects the format checks if it {@link isExpired} based on `PropertyOptions.expStrategy`. Returns true if is stored, respects
			 * format and is not expired. If it is invalid, property may be cleared based on `PropertyOptions.removeWhenInvalid` option..
			 *
			 * @public
			 * @param {String} keyName the key name for the property.
			 * @param {String} [keyNameSpace=None] Optional namespace to assign to the property.
			 * @param {String} [storageResource=InstanceOptions.storageResource] Optional Storage resource from which property to check expiration. If
			 * not sent, assumes the current instance options default storage resource.
			 * @returns {boolean} true if valid, false if is not.
			 * @throws Error when input parameters are invalid.
			 * @throws TypeError when input parameters types are incorrect.
			 */
			isValid: function (keyName, keyNameSpace, storageResource) {
				_validateRequiredParam('keyName', keyName);
				_validateInputParamsTypes(_isNil(keyNameSpace) ? keyName : [keyName, keyNameSpace],
					'non-empty-string');
				if (!_isNil(storageResource)) {
					_validateInputParamsTypes(storageResource, 'non-empty-string');
				}
				var propKey = _buildPropKey(keyName, keyNameSpace);
				return _isValid(propKey, _getProperty(propKey, storageResource));
			},

			/**
			 * Returns a {@link Date} object with the date and time in which the property was stored.
			 *
			 * @public
			 * @param {String} keyName the key name for the property.
			 * @param {String} [keyNameSpace=No-Namespace] Optional namespace to assign to the property.
			 * @returns {Date} date the stored date.
			 * @throws Error when input parameters are invalid.
			 * @throws TypeError when input parameters types are incorrect.
			 */
			getStoredDate: function (keyName, keyNameSpace) {
				_validateRequiredParam('keyName', keyName);
				_validateInputParamsTypes(_isNil(keyNameSpace) ? keyName : [keyName, keyNameSpace],
					'non-empty-string');
				return _getStoredDate(_getProperty(_buildPropKey(keyName, keyNameSpace)));
			},

			/**
			 * Returns a {@link Date} object with the date in which the property expires.
			 *
			 * @public
			 * @param {String} keyName the key name for the property.
			 * @param {String} [keyNameSpace=No-Namespace] Optional namespace to assign to the property.
			 * @returns {Date} date the expiration date.
			 * @throws Error when input parameters are invalid.
			 * @throws TypeError when input parameters types are incorrect.
			 */
			getExpirationDate: function (keyName, keyNameSpace) {
				_validateRequiredParam('keyName', keyName);
				_validateInputParamsTypes(_isNil(keyNameSpace) ? keyName : [keyName, keyNameSpace],
					'non-empty-string');
				return _getExpirationDate(_getProperty(_buildPropKey(keyName, keyNameSpace)));
			},

			/**
			 * Returns a boolean indicating if the property expired or not, depending on the
			 * expiration configured when stored.
			 *
			 * @public
			 * @param {String} keyName the key name for the property.
			 * @param {String} [keyNameSpace=No-Namespace] Optional namespace to assign to the property.
			 * @returns {Number} Date in milliseconds.
			 * @throws Error when input parameters are invalid.
			 * @throws TypeError when input parameters types are incorrect.
			 */
			getUniqueKey: function (keyName, keyNameSpace) {
				_validateRequiredParam('keyName', keyName);
				_validateInputParamsTypes(_isNil(keyNameSpace) ? keyName : [keyName, keyNameSpace],
					'non-empty-string');
				return _buildPropKey(keyName, keyNameSpace);
			}
		};

		// Return object with static attributes
		return {

			/**
			 * Initializes the singleton instance of this library, it can only be called once within a browser session.
			 *
			 *
			 * @public
			 * @param {Object} instanceOptions
			 * @returns {eBizCommons.util.BrowserStorage}
			 * @throws TypeError when input parameters types are incorrect.
			 */
			initializeInstance: function (instanceOptions) {
				if (!_isNil(instanceOptions)) {
					_validateInputParamsTypes(instanceOptions, 'object');
				}
				if (!_instance) {
					_instance = _createInstance(instanceOptions);
				} else {
					_logAndThrowError('BrowserStorage instance is already created! Cannot override initialization.');
				}
				return this;
			},

			/**
			 * Get the current singleton instance, creates one with default configuration if it wasn't created.
			 *
			 * @returns {Object} The singleton instance
			 */
			getInstance: function () {
				if (!_instance) {
					LOGGER.warn('Creating BrowserStorage instance with native-library default configuration. ' +
						'***Some features may not work due to configuration requirements***');
					_instance = _createInstance();
				}
				return _instance;
			},

			STORAGE_RESOURCES: _STORAGE_RESOURCES,

			EXP_STRATEGIES: _EXP_STRATEGIES,

			/* test-code */
			/* This is only code for testing, to clear the closure variables between unit tests, should be removed
			 * when shipping this library.
			 * DO NOT USE ON PRODUCTION CODE.*/
			clearInstance: function () {
				_instance = null;
				_options = null;
			},

			getInstanceOptions: function () {
				return _options;
			}
			/* test-code */
		};
	})();
}());

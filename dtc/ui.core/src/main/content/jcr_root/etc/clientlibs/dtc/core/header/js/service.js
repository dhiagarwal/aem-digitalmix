
(function() {
"use strict";


headerModule.service('headerNavService', ['$http', '$q', 'userProfileManager','serviceURL',function($http, $q,userProfileManager,serviceURL) {


	var service = this;
	service.listNumber = [];
	service.getQuickSearchData = getQuickSearchData;
	service.init = init;

	function init() {
		service.numbers = [];
		return service.numbers;
	}
	
	this.getMbsStory = function() {
		var userId;
	    var mbsdeferred = $q.defer();
	    
		if(userProfileManager.isUserLoggedIn()){

			
			
	       var name=userProfileManager.getUserProfileDetailInfo().profileHeader.firstName;
	        userId = userProfileManager.getUserProfileMasterInfo().userId;
            var paramsArray = [];
			paramsArray[0] = mulesoftStorefrontEndpoint;
			paramsArray[1] = userId;
	        var getUrl = serviceURL.GetServiceURL('mbsGetStoryURL' , paramsArray);
			$http.get(getUrl).then(function (response) {
				mbsdeferred.resolve(response);
	        }, function(response) {
	        	mbsdeferred.resolve(response);
	        });
	    }
	
	    return mbsdeferred.promise;
	 }
	
	 this.getPageLocaleVal = function(currentPageVal) {
		   var deferred = $q.defer();
		   var paramsArray = [];
	    	paramsArray[0] = currentPageVal;
	    	
	    	var url = serviceURL.GetServiceURL('getPageLocaleValURL' , paramsArray);

	        $http({
	            method: 'GET',
	            url: url,
	        }).then(function successCallback(response) {
	            deferred.resolve(response.data);
	        });
	        return deferred.promise;
	    }

	function getQuickSearchData(searchText) {
		var paramsArray = [];
    	paramsArray[0] = searchText;
    	
    	var url = serviceURL.GetServiceURL('getQuickSearchDataURL' , paramsArray);
    	
		var deferred = $q.defer();
		$http({
			method : 'GET',
			url : url,
			params : {
				indent : 'on',
				q : '*.*',
				wt : 'json'
			}
		}).success(function(data) {
			deferred.resolve(data);
		}).error(function() {
			console.log('Search failed!');
		});
		return deferred.promise;
	}
	;
	
	var cartServiceBaseSite = "cosmetics";
    var user_id;
    var endPoint;
    if(userProfileManager.isUserLoggedIn()) {
        user_id = userProfileManager.getUserProfileMasterInfo().userId;
        endPoint = mulesoftHybrisEndpoint;
    } else {
        user_id = "anonymous";
        endPoint = mulesoftHybrisGuestEndpoint;
    }
    
    var addToCartResponse = $q.defer();
    var cartResponse = $q.defer();
    var addToCartUrl;
    var config;

    this.createCart = function() {
        cartResponse = $q.defer();
        var paramsArray = [];
    	paramsArray[0] = endPoint;
    	paramsArray[1] = cartServiceBaseSite;
    	paramsArray[2] = user_id;
    	
    	var url = serviceURL.GetServiceURL('createCartURL' , paramsArray);
        
        $http.post(url).then(function (response) {
			cartResponse.resolve(response);
        }, function(response) {
			cartResponse.resolve(response);
        });
        return cartResponse.promise;
    }

    this.addProductToCart = function(guid,data) {
        addToCartResponse = $q.defer();
        var paramsArray = [];
    	paramsArray[0] = endPoint;
    	paramsArray[1] = cartServiceBaseSite;
    	paramsArray[2] = user_id;
    	paramsArray[3] = guid;
    	var url = serviceURL.GetServiceURL('addProductToCartURL' , paramsArray);
    	
        if(userProfileManager.isUserLoggedIn()) {
            config = {
                    headers: {
                        'Content-Type': 'application/json;charset=utf-8;'
                    }
                }
        } else {
            config = {
                    headers: {
                        'Content-Type': 'application/json;charset=utf-8;'
                    }
                }
        }
        $http.post(url,data,config).then(function(response) {
            addToCartResponse.resolve(response);
        }, function(response) {
			addToCartResponse.resolve(response);
        });
        return addToCartResponse.promise;
    }  

	return service;
}]);
}());
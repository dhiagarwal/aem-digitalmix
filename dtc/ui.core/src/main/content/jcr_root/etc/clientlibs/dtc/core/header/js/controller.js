(function() {
"use strict";

headerModule
		.controller(
				'headerNavController',

				['$scope', '$timeout', '$window', '$rootScope', '$location', 'headerNavService', 'userProfileManager','$cookies',function($scope, $timeout, $window, $rootScope, $location, headerNavService,userProfileManager,$cookies){

					this.name = 'headerNav';
					this.isCategoryShown = true;
					this.activeSubCategory = '';
                    this.activeSubCategoryName = '';
					this.screenHeight = $window.innerHeight;
					this.loggedIn = false;
					this.accType = 'distributor';
                    this.showAccountInfo=false;
                    this.psvVal = '24,230';
                    this.gsvVal = '25,240';
					var self = this;
					$rootScope.accImage = '';
					this.accLevel = '/etc/clientlibs/mecommerce/core/main/css/src/assets/images/css/logo-brown.png';
					$rootScope.firstName = '';
					$rootScope.lastName = '';
					$rootScope.preferredName = '';
                    this.accType ='';
                    this.isCustomer=false;
                    $rootScope.searchCountResult = 0;

					this.isDevice = false;

                    if( /Android|iPhone|iPad|iPod/i.test(navigator.userAgent) ) {
                        this.isDevice = true;
                    }
					
					var paramValue = getParameterByName('param') ? getParameterByName('param') : '';
                    if(paramValue){
                        setCookie('dtcParam', paramValue, 5);
                    }
					
					self.items = $cookies.get("totalItems"); 
                    $rootScope.$on("updateBagItems", function(event,data) {
                        self.items = data;  
                    });

					self.searchMode = false;
					self.showSearch = false;

					this.openMenu = function() {
						this.searchMode = false;
						this.showSearch = true;
						self.clearValue();
					};
                    this.showSearchMode = function() {
                        this.searchMode=true;
                        angular.element(document.querySelector('#sideSearchInput')).attr('autofocus',true);
                    };

					this.showSubCategory = function(subCategory) {
						if(this.activeSubCategory !== subCategory){
							this.isCategoryShown = false;
	                        this.activeSubCategoryName = $("input[name="+subCategory+"value]").val();							
							this.activeSubCategory = subCategory;
						} else{
							this.isCategoryShown = true;
							this.activeSubCategory = '';
						}
					};
					this.hideSubCategory = function(event) {
						this.showAccountInfo=false;
				        if(event.target && (angular.element(event.target).hasClass("menu-dropdown") || angular.element(event.target).hasClass("overlay-dropdown-menu") || angular.element(event.target).hasClass("side-menu-back") ||  angular.element(event.target).hasClass("back-icon"))){
				        	this.isCategoryShown=true;
							this.activeSubCategory = '';
                            this.activeSubCategoryName = '';
                            this.openMenu();
						}
					};

                    this.getQueryStringValue = function (key) {  
  							return unescape(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + escape(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));  
					};  


                    this.loadMenuData = function(){

                        if(!userProfileManager.isUserLoggedIn()){
                             self.accTypeDesc = 'Sign In';
                             self.accImage ='';
                        }else if(userProfileManager.isUserLoggedIn()){
                            	var userDetails = userProfileManager.getUserProfileDetailInfo();
                        		var masterInfo = userProfileManager.getUserProfileMasterInfo();
                                if(masterInfo && (masterInfo.userType === USER_TYPE.RETAIL_CUSTOMER || masterInfo.userType === USER_TYPE.PREFERED_CUSTOMER || masterInfo.userType === USER_TYPE.FAMILY_MEMBER)){
                                    this.accTypeDesc = 'My Account';
                                    this.accType = 'customer';
                                    this.isCustomer=true;
                                }else if(masterInfo && (masterInfo.userType === USER_TYPE.PRIMARY_DISTRIBUTOR || masterInfo.userType === USER_TYPE.ASSOCIATE)){
                                    this.accTypeDesc = 'My Business';
                                    this.accType = 'distributor';
                                }
                                if(userDetails){
                                    $rootScope.accImage = userDetails['profileHeader']['profileThumbnailUrl']==undefined?'':userDetails['profileHeader']['profileThumbnailUrl'];;
                                    $rootScope.firstName = userDetails['profileHeader']['firstName'];
                                    $rootScope.lastName = userDetails['profileHeader']['lastName'];
                                    $rootScope.preferredName = userDetails['profileHeader']['preferredName'];
                                }
                        }
                        var currentPageVal=document.getElementById('resourcePath').value;
                        var promise = headerNavService.getPageLocaleVal(currentPageVal);

                        promise.then(function(data) {
                        	userProfileManager.setPageLocaleVal(data);                   	

                        });
                    };

                    this.showAccountModal = function(){
						if(!userProfileManager.isUserLoggedIn()){
                             var homePagePathURL = angular.element($('#nvgsigninpath')).val();
                             if(undefined != homePagePathURL && null != homePagePathURL && '' != homePagePathURL){
                                 window.location = window.location.protocol+'//'+window.location.host+ homePagePathURL + '.html';
                             }
                        }else if(userProfileManager.isUserLoggedIn()){
                        	 this.isCategoryShown=true;
                             this.activeSubCategory = '';
                             this.showAccountInfo=!this.showAccountInfo;
                        }
    				};

                    this.handleCustomAction = function(customActionPath){
                          userProfileManager.setInviteUrl($location.absUrl());
                          if(undefined != customActionPath && null != customActionPath && '' != customActionPath){
              					window.location = window.location.protocol+'//'+window.location.host+ customActionPath + '.html';
      					  }
    				};

                        this.handleMenuAction = function(actionDesc,index,isSignOut,isOpenInNewWindow){
                        	
                          var URL = angular.element($('#'+actionDesc+index)).val();	
                          if(undefined != isSignOut && isSignOut){
                              userProfileManager.deleteUserProfile();
                              $cookies.remove('cart_guid', { path: '/' });
                              $cookies.remove('totalItems', { path: '/' });
                              $cookies.remove('whyStoryId', { path: '/' });
                              $cookies.remove('quizTaken', { path: '/' });
                              $cookies.remove('leaveMECOption', { path: '/' });
                              gigya.accounts.logout({
                                forceProvidersLogout : true
                              });
                              /*
                              analyticsService.clearProductString();*/
							  //delete all cookies created for the site
								deleteAllCookies();
                          }


                          if(undefined != URL && null != URL && '' != URL){
                              if(undefined != isOpenInNewWindow && isOpenInNewWindow){
                                  window.open(URL);
                              }else{

                            	  if(actionDesc=='nvgmbsstorypath'){
                            		  var mbsPromise = headerNavService.getMbsStory();
                                      mbsPromise.then(function(data){
                                      if(data.status==200){
                                    	  actionDesc='nvgmbsstorypathtaken';
                                    	  URL = angular.element($('#'+actionDesc+index)).val();	
                                    	  window.location = window.location.protocol+'//'+window.location.host+ URL + '.html?outsideMBS=yes';
                                      	}else
                                      if(data.status==404){
                                    	  window.location = window.location.protocol+'//'+window.location.host+ URL + '.html';
                                         }
                                      })                            		  
                            	  }else{
                            		  window.location = window.location.protocol+'//'+window.location.host+ URL + '.html';
                            	  }
                              }
      					  }
    				};

    				function deleteAllCookies() {
    					var cookies = $cookies.getAll();
                        angular.forEach(cookies, function (v, k) {
                            if( k!='uname' &&  k!='searchCookie' &&  k!='qData' &&  k!='productSearchCookie'){
                                $cookies.remove(k, { path: '/' });
                            }

                            });
    				}

					// Just for testing purpose. Remove once all scenarios are
					// tested
					this.changeAcc = function() {
						if (this.accType == 'guest')
							this.accType = 'customer';
						else if (this.accType == 'customer')
							this.accType = 'distributor';
						else if (this.accType == 'distributor')
							this.accType = 'guest';
					};

					this.clearValue = function() {
						this.headerSearch = null;
					};

					this.submitSearch = function(navigationPath) {
						var search = '';
						var minPSV = '';
						var maxPSV = '';
						if (this.headerSearch != '' || this.minPSV
								|| this.maxPSV) {
							var searchInput = (this.headerSearch && this.headerSearch != '') ? (this.headerSearch.Key ? this.headerSearch.Key
									: this.headerSearch)
									: '';
							if (typeof searchInput === 'object') {
								searchInput = searchInput.num;
							}
							angular.element('#headerSearch').modal('hide');
							angular.element('#sideMenu').modal('hide');
							search = 'search=' + encodeURIComponent(searchInput);
							minPSV = this.minPSV ? (search != '' ? '&' : '')
									+ 'minPSV=' + this.minPSV : '';
							maxPSV = this.maxPSV ? ((search != '' || minPSV != '') ? '&'
									: '')
									+ 'maxPSV=' + this.maxPSV
									: '';
							$timeout(function() {
								window.location.href = navigationPath + '.html?search=' + encodeURIComponent(searchInput);
								searchHandler();
							}, 100);

						}
					}

					var searchHandler = $rootScope
							.$on(
									'searchHandler',
									function(ev, args) {
										angular.element('#' + args.type).modal();
										self.searchMode = true;
										self.showSearch = true;
										self.headerSearch = getParameterByName('search') ? getParameterByName('search')
												: '';
										self.minPSV = $location.search().minPSV ? $location
												.search().minPSV
												: '';
										self.maxPSV = $location.search().maxPSV ? $location
												.search().maxPSV
												: '';
									});
					function getParameterByName(name, url) {
						if (!url)
							url = window.location.href;
						name = name.replace(/[\[\]]/g, "\\$&");
						var regex = new RegExp("[?&]" + name
								+ "(=([^&#]*)|&|#|$)"), results = regex
								.exec(url);
						if (!results)
							return null;
						if (!results[2])
							return '';
						return decodeURIComponent(results[2]
								.replace(/\+/g, " "));
					}
					
					getProductSearchHistory();
					
					function getProductSearchHistory(){
						var searchedHistoryData = [];
                        var searchedHistoryCookie = '';
                        if($cookies.get("productSearchCookie")!=undefined && $cookies.get("productSearchCookie")){
                        searchedHistoryCookie = $cookies.get("productSearchCookie").split(',');
                        }
                        if(searchedHistoryCookie.length > 0){
                            for(var i=0; i<searchedHistoryCookie.length; i++){
                                var searchedTile = searchedHistoryCookie[i];
                                var searchedTileUrl = '.html?search=' + encodeURIComponent(searchedHistoryCookie[i]);
                                searchedHistoryData.push({
                                    tile : searchedTile,
                                    url : searchedTileUrl
                                });
                            }
                        }
                      self.searchedHistoryData = searchedHistoryData;
					}
					

					/* Quick Search */
					$scope.$watch(function() {
						return headerNavService.listNumber
					}, function() {
						self.numberList = headerNavService.listNumber
					});

					self.quickSearchData = headerNavService.init();

					self.headerSearch = null;
					self.updateNumberList = updateNumberList;
					self.numbers = new Bloodhound({
						datumTokenizer : function(d) {
							return Bloodhound.tokenizers.whitespace(d.num);
						},
						queryTokenizer : Bloodhound.tokenizers.whitespace,
						local : self.quickSearchData
					});

					self.searchOptions = {
						highlight : true
					};
					self.searchDataset = {
						displayKey : 'num',
						name : 'numbers',
						source : self.numbers.ttAdapter()
					};

					self.clearValue = function() {
						self.headerSearch = null;
					};

					function updateNumberList($event) {
						if (self.headerSearch.length == 1) {
							headerNavService
									.getQuickSearchData(self.headerSearch)
									.then(
											function(data) {
												if ((data.spellcheck.suggestions[1]) == null) {
													console
															.log("NO SEARCH FOUND.PLEASE ENTER CORRECT SEARCH TERMS.");
												} else if (data.spellcheck.suggestions[1].suggestion) {
													angular.forEach(
																	data.spellcheck.suggestions[1].suggestion,
																	function(
																			suggestion) {
																		var sug = {
																			num : suggestion
																		};
																		self.quickSearchData
																				.push(sug);
																	});
													;
													var bloodHoundSuggestion = new Bloodhound({
														datumTokenizer: function(d) {
															return Bloodhound.tokenizers.whitespace(d.num);
														},
														queryTokenizer: Bloodhound.tokenizers.whitespace,
														local: self.quickSearchData
													});
													self.searchDataset = {
														displayKey : 'num',
														name : 'numbers',
														source : bloodHoundSuggestion
													};
												}

												if (angular.element('#headerSearch #searchInput').is(":visible")) {
													setTimeout(function() { 
														angular.element('#headerSearch #searchInput').focus().setCursorPosition(self.headerSearch.length);
													}, 10);
												} else if (angular.element('.side-menu-search #sideSearchInput').is(":visible")) {
													setTimeout(function() { 
														angular.element('.side-menu-search .searchInput').focus().setCursorPosition(self.headerSearch.length);
													}, 20);
												}
											});
						}
						$event.stopImmediatePropagation();
					}

					angular.element('#headerSearch').on('shown.bs.modal',function(){
                        var $el = $(this).find('#searchInput')[0]; 
                            $el.focus();
                            $el.click();
                    });

					$.fn.setCursorPosition = function(pos) {
						this.each(function(index, elem) {
							if (elem.setSelectionRange) {
								elem.setSelectionRange(pos, pos);
							} else if (elem.createTextRange) {
								var range = elem.createTextRange();
								range.collapse(true);
								range.moveEnd('character', pos);
								range.moveStart('character', pos);
								range.select();
							}
						});
						return this;
					};
					
                    self.gotoCart = function(shoppingCartPage){
                    	window.location.href = shoppingCartPage+".html";
                    };
                    
                    self.gotoShopLandingPage = function(shopLandingPage){
                    	window.location.href = shopLandingPage + ".html";
                    }
                    
                    angular.element(window).on('resize', function() {
                        self.screenHeight = $window.innerHeight;
                        if(angular.element("#sideMenu").find('.side-menu').length !== 0){
                        	angular.element("#sideMenu").find('.side-menu')[0].style.minHeight = self.screenHeight+'px';
                        }
                        if(angular.element("#headerSearch").find('.header-search').length !== 0){
                        	angular.element("#headerSearch").find('.header-search')[0].style.minHeight = self.screenHeight+'px';
                        }
                    });

					 this.hideModal = function(event){
					  if(event.target && (event.target.id=='showAccountInfoId' || event.target.id=='showMyAccountId' || event.target.id=='showAccountInfo')) {
						 this.showAccountInfo=false;
					  } else{
						this.showAccountInfo=true;
					  }
					}

				}]);
}());
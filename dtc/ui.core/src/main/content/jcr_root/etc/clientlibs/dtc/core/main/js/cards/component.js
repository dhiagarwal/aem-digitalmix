'use strict';

cardsController.$inject = ['cardsService', '$scope', '$timeout', '$window'];
cardsModule.directive('cardsComponent',function(){

	return{
		restrict: 'E',
		scope: {
	      toDoCards : '<',
	      show : '=',
	      dots: '<',
        arrows: '<',
          useType: '=',
      	 cardsCount: '='
	    },

	    templateUrl: '/etc/clientlibs/dtc/core/main/js/cards/cards.html',
	    controller: cardsController,
	    controllerAs: 'cards',
	    bindToController: true
	}
    
})

function cardsController(cardsService, $scope, $timeout, $window){

    var self = this;
    this.timelabel = $('#timelabel').val();
    this.subheading = $('#subheading').val();
    this.isShown = false;
    $scope.currentIndex = 0;
    $scope.changedCount = 0;
    this.favourite = this.favourite || false;
    this.dots = this.dots || false;
    this.arrows = this.arrows || false;
    self.isCardsReady = true;
    self.slideDom = null;
    $scope.slickCardsConfig = {
      enabled: true,
      infinite: true,
      adaptiveHeight: false,
      mobileFirst: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: this.arrows,
      dots: this.dots,
      speed: 500,
      method: {
      },
      responsive: [
        {
          breakpoint: 0,
          settings: {
            arrows: this.arrows,
            draggable: true
          }
        }
      ],
      event: {
        afterChange: function (event, slick, currentSlide, nextSlide) {
          $scope.currentIndex = currentSlide; // save current index each time
          self.slideDom = slick.$slides.get(currentSlide);
        },
        init: function (event, slick) {
          slick.slickGoTo($scope.currentIndex); // slide to correct index when init
          $scope.changedCount = self.toDoCards.length;
          $scope.nextCount = self.toDoCards.length;
        }
      }
    };

    this.removeCard = function(slide){
        if(undefined != this.useType){
            return;
        }
    	console.log(slide.id);
    	var cardRemoved = cardsService.removeCard(slide.id);
        cardRemoved.then(function(data){
        	console.log(data);
      });

    };


    this.callToAction = function(url,id){
		if(undefined != this.useType){
            url = url + ".html";
            var cardRemoved = cardsService.removeCard(id,this.useType);
            cardRemoved.then(function(data){
                console.log(data);
            });
        }

    	$timeout(function(){
            $window.location.href = url ;    	

    	}, 1800);

    };
    
   $scope.removeSlide = function(event, targetSlide, eventInitiation) {
         self.isCardsReady = false;
        var slickHeight = angular.element('#cardsComponent').height();
        var bgCayManHeight = angular.element(event.currentTarget).closest('.bg-cayman').innerHeight();
        angular.element('#cardsComponent').css({'height': (slickHeight/16) + 'rem'});
        angular.element(event.currentTarget).closest('.bg-cayman').css({'height': (bgCayManHeight/16) + 'rem', 'max-height': (bgCayManHeight/16) + 'rem', 'overflow': 'hidden'});

        if(eventInitiation == 'icon'){
            if((self.toDoCards.length - 1) == this.currentIndex){
				      $scope.slickCardsConfig.method.slickNext();
            } else {
              angular.element("#cardsComponent").slick('slickAdd', self.slideDom);
              angular.element("#cardsComponent").slick('slickRemove', targetSlide);
            } 
        } else {
			     angular.element("#cardsComponent").slick('slickRemove', this.currentIndex);
        }
        angular.element('#cardsComponent .cards-section').css({'opacity': 0});
        $timeout(function() {
          if(self.toDoCards && self.toDoCards.length > 0) {
            angular.element('#cardsComponent .cards-section').animate({'opacity': 1});
          }
          self.isCardsReady = true;
          if(!self.toDoCards || (self.toDoCards && self.toDoCards.length == 0)) {
            self.show = true;
          }
      }, 0);
    };


     $scope.hideCard = function() {
		 if(self.toDoCards.length==1)    	 
    	 {

             $timeout(function(){
                 angular.element('.card-remove').removeClass('card-remove');
                 $scope.slickCardsConfig.method.slickGoTo('slickGoTo',parseInt(0))
             }, 250);

         }
         else
         {
              $scope.slickCardsConfig.method.slickNext();
              $scope.nextCount = $scope.nextCount - 1;
              if($scope.nextCount == 0){
                $scope.slickCardsConfig.method.slickGoTo(0);
                $scope.nextCount = self.toDoCards.length;
              }

         }




     };

    this.toggle = function () {
      this.isShown = !this.isShown
	  	}

};


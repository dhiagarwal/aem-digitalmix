;(function(){
signupComponent.service('signupService', ['$http', '$q','$window','userProfileManager', function ($http,$q,$window,userProfileManager){
    
    this.getMember = function(guid){
   	 var deferred = $q.defer();

		var getUrl = mulesoftStorefrontEndpoint+'account/'
				+ guid;
		$http.get(getUrl).then(function(response) {
			deferred.resolve(response.data);
		});

		return deferred.promise;
    }

	this.getPrivacySettings = function() {
		/*var deferred = $q.defer();
		var getUrl = $window.location.protocol+'//'+$window.location.host+'/dtc/signup/config';
		$http.get(getUrl).then(function(response) {
			var locales=userProfileManager.getPageLocaleVal();

			response.data.forEach(function(item){
				if(item.countryCode.toUpperCase() == locales.market.toUpperCase()){
					deferred.resolve(item);
				}
			});

		});
		return deferred.promise;*/
	}
	
	this.registerGigyaUser = function(data) {  
		var deferred = $q.defer();
        gigya.accounts.initRegistration({
    		callback : function(response){
                if(response.errorCode !== 0){
                    deferred.resolve(response);		
                }else{
					gigyaSignUp(response);
                }
    		}
    	});

        function gigyaSignUp(loginData){

            gigya.accounts.register({
                email : data.email,
                password :data.password,
                regToken: loginData.regToken,
                finalizeRegistration : true,
                callback : function(response){
                    deferred.resolve(response);
                }
    		});
    	
    	}

		return deferred.promise;
    }

}])
})();

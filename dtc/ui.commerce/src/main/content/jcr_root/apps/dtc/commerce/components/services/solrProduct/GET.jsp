<%@taglib prefix="cq" uri="http://www.day.com/taglibs/cq/1.0" %>
<cq:defineObjects/>
<!-- The line below allows us to do posts to the same url. By default this is disabled -->
<cq:includeClientLib categories="granite.csrf.standalone"/>
<html>
<head>
</head>
<body>
<h1>Solr Product Service</h1>
<form action="<%= currentNode.getPath() + "?update" %>" method="post">
    <input type="submit" value="Update Products"/>
</form>
<form action="<%= currentNode.getPath() + "?delete" %>" method="post">
    <input type="submit" value="Delete Products"/>
</form>
<%--<form action="<%= currentNode.getPath() + "?deleteOrphans" %>" method="post">--%>
    <%--<input type="submit" value="Delete Orphan Products"/>--%>
<%--</form>--%>
</body>
</html>

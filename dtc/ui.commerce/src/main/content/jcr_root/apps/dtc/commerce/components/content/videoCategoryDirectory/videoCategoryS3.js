"use strict";
use(["/libs/wcm/foundation/components/utils/AuthoringUtils.js",
    "/libs/wcm/foundation/components/utils/ResourceUtils.js",
    "/libs/sightly/js/3rd-party/q.js"
], function(AuthoringUtils, ResourceUtils, Q) {

    var CONST = {
    };

    var slyComponent = {};

	var resourceResolver = resource.getResourceResolver();
    var resProps = resource.adaptTo(Packages.org.apache.sling.api.resource.ValueMap);
    if(resProps){
        var path = resProps.get("videoPath", "");
		slyComponent.damPath = path;

    	if(slyComponent.damPath != ""){
        	var metadataRes = resourceResolver.getResource(slyComponent.damPath + "/jcr:content/metadata");
        	var metadataProps = metadataRes.adaptTo(Packages.org.apache.sling.api.resource.ValueMap);
            var myService = sling.getService(Packages.com.mec.core.services.EnvironmentConfig);
            var s3PathFromOSGI = myService.getS3BucketURL();
        	var s3Path = s3PathFromOSGI + metadataProps.get("mp4-hd", "");
			slyComponent.s3Path = s3Path;
   		}
    }


    return slyComponent;

});
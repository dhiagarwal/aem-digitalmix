<%--
  #%L
  ACS AEM Commons Package
  %%
  Copyright (C) 2013 Adobe
  %%
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
       http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  #L%
  --%>
<%@page contentType="application/json" pageEncoding="utf-8" import="java.util.Iterator,org.apache.sling.commons.json.*"%>
<%
%>
<%@include file="/libs/foundation/global.jsp"%>
<%
/* Create a json object of name/value parameters
   for use in select dropdowns
*/

    Resource listTestimonials = resource.getChild("testimonials");

    Iterator<Resource> listTestimonialsItr = listTestimonials.listChildren();

    JSONArray testimonialArr = new JSONArray();

    while (listTestimonialsItr.hasNext()) {

        JSONObject testimonial = new JSONObject();

        Resource testimonialItem = listTestimonialsItr.next();
        Node node = testimonialItem.adaptTo(Node.class);

        if(node.hasProperty("text")){

			testimonial.put("isVideo", false);
            testimonial.put("text", node.hasProperty("text") ? node.getProperty("text").getString() : "");
            testimonial.put("name", node.hasProperty("name") ? node.getProperty("name").getString() : "");            
            testimonial.put("age", node.hasProperty("age") ? node.getProperty("age").getString() : "");
        }

         if(node.hasProperty("video")){

            testimonial.put("isVideo", true);
            testimonial.put("video", node.hasProperty("s3Path") ? node.getProperty("s3Path").getString() : "");
            testimonial.put("name", node.hasProperty("name") ? node.getProperty("name").getString() : "");            
            testimonial.put("age", node.hasProperty("age") ? node.getProperty("age").getString() : "");

        }

        testimonialArr.put(testimonial);

    }


    out.println(testimonialArr);

%>
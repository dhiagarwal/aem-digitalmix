package apps.dtc.commerce.components.structure.products;

import com.adobe.cq.sightly.WCMUsePojo;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.WCMException;
import com.dtc.commerce.productdetail.service.ProductDetailService;
import com.dtc.commerce.productdetail.service.ProductsService;
import com.dtc.core.utils.LocaleUtil;
import java.util.ArrayList;
import com.dtc.commerce.utils.PMDUtil;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ResourceResolver;

import java.util.Locale;
import java.lang.Exception;
import javax.jcr.Node;
import javax.jcr.Property;
import java.util.Arrays;
import java.util.List;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.jcr.ValueFactory;

import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;

/**
 * Products use class.  It automatically generates the pmd structure if it doesn't exist.  The product template contains a productContainer component.
 * The productContainer is a wrapper that contains all components that are displayed on the product detail page.
 * The product template stores the productContainer in the PMD structure.  /content/pmd/01/00/36/01003610/productContainer
 */
public class Products extends WCMUsePojo {
	
    private final String PMD_PATH = "/content/pmd/";
    private final String PRODUCT_CONTAINER = "productContainer";
    private final String BASE_SKU = "baseSku";
    private final String MARKETS = "markets";
    private final String PMD_TEMPLATE = "dtc/commerce/template/productMasterData";
    private final String EMPTY_TEMPLATE = "/apps/dtc/core/templates/blank template";
    
    private final String PAGE_404 = "/404.html";
    
    private final String ERR_SKU_ABSENT = "The sku in the selector does not exist in the pim";
    private final String ERR_LOCALE_ABSENT = "Your path does not have a valid locale in it. Please create your page structure correctly.";

    private String errorMessage = null;
    private String pmdPath;
    private boolean showSkuPicker = true;
    private String allSkus;
    private String missingPmdSkus;
    private PageManager pm;
    private boolean errorPage = false;
    private String errorPagePath = null;

    /** The service. */
    ProductDetailService service;

    ProductsService productsService;

    /**
     * @param sku
     *
     * Creates necessary PMD page structure for a sku to exist in.  ex: 01/00/36/01003610
     */
    private void createPageStructure(String sku) {
        String part1 = sku.substring(0,2);
        String part2 = sku.substring(2,4);
        String part3 = sku.substring(4,6);
        try {
            //Make sure /content/pmd exists if it doesn't create the pmd page
            if (pm.getPage(PMD_PATH) == null) {
                pm.create("/content/", "pmd", PMD_TEMPLATE, "Product Master Data", true);
            }

            if (pm.getPage(PMD_PATH+part1) == null) {
                //create part1
                pm.create(PMD_PATH, part1, EMPTY_TEMPLATE, part1, true);
            }
            if (pm.getPage(PMD_PATH + part1 + "/" + part2) == null) {
                //create part2 page
                pm.create(PMD_PATH + part1, part2, EMPTY_TEMPLATE, part2, true);
            }
            if (pm.getPage(PMD_PATH + part1 + "/" + part2 + "/" + part3) == null) {
                //create part3 page
                pm.create(PMD_PATH + part1 + "/" + part2, part3, EMPTY_TEMPLATE, part3, true);
            }
            if (pm.getPage(PMD_PATH + part1 + "/" + part2 + "/" + part3 + "/" +sku) == null) {
                //create sku page
                pm.create(PMD_PATH + part1 + "/" + part2 + "/" + part3, sku, EMPTY_TEMPLATE, sku, true);
            }
        } catch(WCMException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param sku
     * @param variants
     * @param locale
     *
     * Creates all variant pages in the pmd and sets the base skus and market
     */
    private void createVariantPMDPages(String sku, List<String> variants, Locale locale) {
        String localeStr;
        //Loop through variants and make sure that they all get created if they don't exist
        for(int i=0; i<variants.size(); i++) {
            localeStr = getLocaleString(variants.get(i),locale);
            Page variantPage = pm.getPage(PMDUtil.getPmdPath(variants.get(i)) + "/" + localeStr);
            if (variantPage == null) {
                try {
                    createPageStructure(variants.get(i));
                    pm.create(PMDUtil.getPmdPath(variants.get(i)), localeStr, "dtc/commerce/template/productMasterDetail", localeStr, true);
                    // get the variant page again now that it is created
                    variantPage = pm.getPage(PMDUtil.getPmdPath(variants.get(i)) + "/" + localeStr);
                    variantPage.getContentResource().adaptTo(Node.class).addNode("productContainer", "nt:unstructured");
                    //Set the baseSku on the created page
                    Node variantNode = variantPage.getContentResource().getChild(PRODUCT_CONTAINER).adaptTo(Node.class);
                    variantNode.setProperty("sling:resourceType","dtc/commerce/components/content/productContainer");
                    variantNode.setProperty(BASE_SKU, sku);
                    variantNode.setProperty(MARKETS, new String[]{locale.getCountry().toString()});
                    variantNode.getSession().save();
                } catch (Exception e) {
                    System.out.println(e.getStackTrace());
                }
            }
        }
    }

    /**
     * @param baseSku
     * @param sku
     * @param locale
     *
     * Create the PMD page and add the base sku and market properties
     */
    private void createPMDPage(String baseSku, String sku, Locale locale, Boolean base) {
        String localeStr = getLocaleString(sku, locale);
        Page productPage = pm.getPage(PMDUtil.getPmdPath(sku) + "/" + localeStr);
        if (productPage == null) {
            //We need to create the page
            try {
                createPageStructure(sku);
                pm.create(PMDUtil.getPmdPath(sku), localeStr, "/apps/dtc/commerce/template/productMasterDetail", localeStr, true);
                //get product page again now that it is created
                productPage = pm.getPage(PMDUtil.getPmdPath(sku) + "/" + localeStr);
                productPage.getContentResource().adaptTo(Node.class).addNode("productContainer", "nt:unstructured");
                Node productNode = productPage.getContentResource().getChild(PRODUCT_CONTAINER).adaptTo(Node.class);
                productNode.setProperty("sling:resourceType","dtc/commerce/components/content/productContainer");
                productNode.setProperty(BASE_SKU, baseSku);
                productNode.setProperty(MARKETS, new String[]{locale.getCountry().toString()});
                if (base) {
                    //If we are creating a base node then set the hide flags for all components except for productdetail
                    productNode.setProperty("hidePdpUpSell", true);
                    productNode.setProperty("hidePdpUpSellKit", true);
                    productNode.setProperty("hideProductTestimonials", true);
                    productNode.setProperty("hidePdpProductSupplements", true);
                    productNode.setProperty("hidePdpProductUsage", true);
                    productNode.setProperty("hideProductResources", true);
                }
                productNode.getSession().save();
            } catch (Exception e) {
                System.out.println(e.getStackTrace());
            }
        } else {
            //If it exists check to see if we need to add the market to the market list
            updateMarkets(productPage, locale);
        }
    }

    /**
     * @param page
     * @param locale
     *
     * Update the pmd page with the new market
     */
    private void updateMarkets(Page pmdPage, Locale locale) {
        try {
            Node node = pmdPage.getContentResource().getChild(PRODUCT_CONTAINER).adaptTo(Node.class);
            Value[] markets = node.getProperty(MARKETS).getValues();
            boolean found = false;
            for(int i=0; i<markets.length; i++) {
                if (markets[i].getString().equals(locale.getCountry().toString())) {
                    found = true;
                }
            }
            if (!found) {
                //Add the new market to the Array
                ValueFactory vf = node.getSession().getValueFactory();
                Value[] result = Arrays.copyOf(markets, markets.length + 1);
                result[markets.length] = vf.createValue(locale.getCountry().toString());
                node.setProperty(MARKETS, result);
                node.getSession().save();
            }
        } catch(Exception e) {
            System.out.println(e.getStackTrace());
        }
    }

    private String getLocaleString(String sku, Locale locale) {
        String localeStr = locale.toString();
        if (pm.getPage(PMDUtil.getPmdPath(sku)+"/"+locale) == null) {
            localeStr = locale.getLanguage();
        }
        return localeStr;
    }

    @Override
    public void activate() throws Exception {
        ResourceResolverFactory resolverFactory = getSlingScriptHelper().getService(ResourceResolverFactory.class);
        ResourceResolver resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);
        service = getSlingScriptHelper().getService(ProductDetailService.class);
        productsService = getSlingScriptHelper().getService(ProductsService.class);
        String sku;
        String[] selectors = getRequest().getRequestPathInfo().getSelectors();
        pm = getPageManager();
        Locale locale = LocaleUtil.getLocaleFromPath(getCurrentPage().getPath());
        
        InheritanceValueMap iProperties = new HierarchyNodeInheritanceValueMap(getCurrentPage().getContentResource());
        errorPagePath = iProperties.getInherited("errorPages",String.class) + PAGE_404;

        if (locale == null) {
            errorMessage = ERR_LOCALE_ABSENT;
        }
        String country = locale.getCountry().toString();

        if (selectors.length > 0) {
            showSkuPicker = false;
        } else {
            allSkus = productsService.getAllPimSkus(locale.getCountry());
            missingPmdSkus = productsService.getMissingPMDSkus(locale.getCountry(), locale.getLanguage());
        }

        if (selectors.length == 1) {
            sku = selectors[0];
            //check if it is a base sku or product sku
            List<String> variants = service.getVariantList(sku, locale);
            if (variants != null) {
                //It is a base sku make sure the variant pages are created if we are in author
                if (getWcmMode().isEdit()) {
                    //create base page
                    createPMDPage("",sku, locale, true);
                    createVariantPMDPages(sku, variants, locale);
                }
            } else {
                //create pmd page with no baseSku since it is just a product if we are in author
                if (getWcmMode().isEdit()) {
                    //Todo check to see if it exists in the PIM before creating the page.  Else throw exception
                    //if (pm.getPage(PMDUtil.getPimPath(sku, country)) != null) {
                    if (resourceResolver.getResource(PMDUtil.getPimPath(sku, country)) != null) {
                        createPMDPage("", sku, locale, false);
                    } else {
                        errorMessage = ERR_SKU_ABSENT;
                    }
                }
            }
            //set the pmd path to load the components for the page
            pmdPath = PMDUtil.getPmdPath(sku) + "/" + getLocaleString(sku, locale) + "/jcr:content/productContainer";
            if(null == resourceResolver.getResource(pmdPath)){
                errorMessage = ERR_SKU_ABSENT;
            	errorPage = true;
            }
        } else if (selectors.length > 1) {
            //This url should only be hit if we are in author
            String baseSku = selectors[0];
            String variantSku = selectors[1];
            //If we are in author make sure the page is created in the pmd
            if (getWcmMode().isEdit()) {
                // Check to make sure it exists in the pim before creating in the pmd.  Throw exception if not in pim
            	if (resourceResolver.getResource(PMDUtil.getPimPath(baseSku, country)+ "/" + variantSku) != null) {
                    createPMDPage(baseSku, variantSku, locale, false);
                } else {
                    errorMessage = ERR_SKU_ABSENT;
                }
            }
            pmdPath = PMDUtil.getPmdPath(variantSku) + "/" + getLocaleString(variantSku, locale) + "/jcr:content/productContainer";
            if(null == resourceResolver.getResource(pmdPath)){
                errorMessage = ERR_SKU_ABSENT;
            	errorPage = true;
            }
        }
    }

    public String getErrorMessage() {
        return errorMessage;
    }
    public String getPmdPath() {
        return pmdPath;
    }
    public boolean getShowSkuPicker() {
        return showSkuPicker;
    }

    public boolean getErrorPage() {
        return errorPage;
    }

    public String getErrorPagePath() {
        return errorPagePath;
    }

    public String getAllSkus() {
        return allSkus;
    }

    public String getMissingPmdSkus() {
        return missingPmdSkus;
    }
}
/*******************************************************************************
* Author id : Deloitte Digital
* 
*******************************************************************************/

"use strict";

var global = this;

use(["/libs/wcm/foundation/components/utils/AuthoringUtils.js",
    "/libs/wcm/foundation/components/utils/ResourceUtils.js",
    "/libs/sightly/js/3rd-party/q.js"], function (AuthoringUtils, ResourceUtils, Q){

    var slyComponent = {};

    var question = {};

    // The actual text content
    slyComponent.heading = granite.resource.properties["congratulationsHeader"] || "";

	if (!slyComponent.heading) {
		slyComponent.topBannerImage={};
		slyComponent.topBannerImage.path="";
		slyComponent.bottomBannerImage={};
		slyComponent.bottomBannerImage.path="";
        slyComponent.avatarImage={};
		slyComponent.avatarImage.path="";


    } else {
		var _imagesResource1 = resource.getChild("topBannerImage");
        var resourceRefImageProperties1 = _imagesResource1.adaptTo(Packages.org.apache.sling.api.resource.ValueMap);
    	var damFileReferencePath1 = resourceRefImageProperties1.get('fileReference');
    	damFileReferencePath1 = damFileReferencePath1.replace(" ","%20");
    	slyComponent.topBannerImage={};
		slyComponent.topBannerImage.path=damFileReferencePath1;

		var _imagesResource2 = resource.getChild("bottomBannerImage");
        var resourceRefImageProperties2 = _imagesResource2.adaptTo(Packages.org.apache.sling.api.resource.ValueMap);
    	var damFileReferencePath2 = resourceRefImageProperties2.get('fileReference');
    	damFileReferencePath2 = damFileReferencePath2.replace(" ","%20");
    	slyComponent.bottomBannerImage={};
		slyComponent.bottomBannerImage.path=damFileReferencePath2;
		
		if(resource.getChild("avatarImage")!=undefined && resource.getChild("avatarImage")!= null){
        var _imagesResource3 = resource.getChild("avatarImage");
        var resourceRefImageProperties3 = _imagesResource3.adaptTo(Packages.org.apache.sling.api.resource.ValueMap);
    	var damFileReferencePath3 = resourceRefImageProperties3.get('fileReference');
    	damFileReferencePath3 = damFileReferencePath3.replace(" ","%20");
    	slyComponent.avatarImage={};
		slyComponent.avatarImage.path=damFileReferencePath3;
		}
    }

    return slyComponent;
});
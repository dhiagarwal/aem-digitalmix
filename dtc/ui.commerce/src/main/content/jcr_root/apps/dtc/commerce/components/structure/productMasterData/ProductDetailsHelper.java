package apps.dtc.commerce.components.structure.productMasterData;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.adobe.cq.sightly.WCMUse;
import java.util.ArrayList;
import java.util.Iterator;

public class ProductDetailsHelper extends WCMUse {
    private Page skuPage;
    private String sku;

    @Override
    public void activate() throws Exception {
        String[] selectors = getRequest().getRequestPathInfo().getSelectors();
        String part1, part2, part3;
        if (selectors.length > 0) {
            sku = selectors[0];
            part1 = sku.substring(0, 2);
            part2 = sku.substring(2, 4);
            part3 = sku.substring(4, 6);
            if (selectors.length > 1) {
                getResponse().sendRedirect("/content/pmd/" + part1 + "/" + part2 + "/" + part3 + "/" + sku + "/" + selectors[1] + ".html");
            } else if (selectors.length == 1) {
                sku = selectors[0];
                skuPage = getPageManager().getPage("/content/pmd/" + part1 + "/" + part2 + "/" + part3 + "/" + sku);
            }
        }
    }

    public String getSku() { return sku; }
    public Page getSkuPage() { return skuPage; }
}
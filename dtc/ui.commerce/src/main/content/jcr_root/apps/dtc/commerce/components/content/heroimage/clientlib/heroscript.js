(function ($, $document) {
    "use strict";
     $document.on("dialog-ready", function() {
		toggleHeroFields();
    });
     $(document).on("selected", ".herovariation", function(e) {
		toggleHeroFields();
    });
    function toggleHeroFields(){
          var selectObj = $(".herovariation").find("select");
		  var variation = $(selectObj).find(":selected").text();
         if(variation == "variation1" || variation =="Half Width Image"){
             $(".secondarylink").parent().hide();
             $(".secondarybuttonlabel").parent().hide();
             $(".displaysecondarylink").parent().hide();
             var categoryField =  $(".herocategorylabel");
			 $(categoryField).parent().show();
             if(!$(categoryField).val()){
				$(categoryField).addClass("is-invalid");
            	$(categoryField).attr('aria-invalid',true);
             }
             $(categoryField).attr('aria-required',true);
         }
         if(variation == "variation2" || variation =="Full Width Image"){
             $(".secondarylink").parent().show();
             $(".secondarybuttonlabel").parent().show();
             $(".displaysecondarylink").parent().show();
             var categoryField =  $(".herocategorylabel");
			 $(categoryField).parent().hide();
			 $(categoryField).removeClass("is-invalid");
             $(categoryField).attr('aria-invalid',false);
             $(categoryField).attr('aria-required',false);
         }
    }
})($, $(document));
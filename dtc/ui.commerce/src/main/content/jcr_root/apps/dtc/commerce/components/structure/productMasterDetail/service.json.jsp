<%@include file="/libs/foundation/global.jsp"%><%
%><%@ page language="java"
           contentType="application/json; charset=UTF-8"
           pageEncoding="UTF-8"
           import="com.mec.commerce.utils.PMDUtil,
                   org.apache.sling.commons.json.JSONObject"%><%@ page import="com.mec.commerce.productdetail.service.ProductDetailService"%><%@ page import="jdk.nashorn.internal.ir.annotations.Reference"%><%@ page import="com.day.cq.wcm.api.Page"%><%@ page import="java.util.Locale"%><%@ page import="com.mec.core.utils.LocaleUtil"%><%
    @Reference
    ProductDetailService productDetailService;

    try {
        String sku = properties.get("sku","");
        if (sku.isEmpty()) {
            //If sku is empty then we just use the name of the page
            sku = currentPage.getName();
        }
        Locale locale = LocaleUtil.getLocaleFromPath(currentPage.getPath());
        if (locale == null) {
            throw new Exception("Your path does not have a valid locale in it. Please create your page structure correctly.");
        }
        JSONObject o = productDetailService.getProductDetail(sku, locale);
        out.println(o.toString());
    } catch (Exception ex) {
        out.print("Exception : " + ex.toString());
    }
%>
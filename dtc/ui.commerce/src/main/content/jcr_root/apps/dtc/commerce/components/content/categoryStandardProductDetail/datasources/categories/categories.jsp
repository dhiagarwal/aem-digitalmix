<%@include file="/libs/granite/ui/global.jsp"%><%
%><%@page session="false"
          import="java.util.ArrayList,
                  java.util.Collections,
                  java.util.HashMap,
                  java.util.Iterator,
                  java.util.List,
                  org.apache.commons.collections.Transformer,
                  org.apache.commons.collections.iterators.TransformIterator,
                  org.apache.sling.api.resource.Resource,
                  org.apache.sling.api.resource.ResourceMetadata,
                  org.apache.sling.api.resource.ResourceResolver,
                  org.apache.sling.api.resource.ResourceUtil,
                  org.apache.sling.api.resource.ResourceWrapper,
                  org.apache.sling.api.resource.ValueMap,
                  org.apache.sling.api.wrappers.ValueMapDecorator,
                  com.adobe.granite.ui.components.Config,
                  com.adobe.granite.ui.components.ds.DataSource,
                  com.adobe.granite.ui.components.ds.EmptyDataSource,
                  com.adobe.granite.ui.components.ds.SimpleDataSource,
                  com.adobe.granite.ui.components.ds.ValueMapResource"%><%
    final ResourceResolver resolver = resourceResolver;

    Config dsCfg = new Config(resource.getChild("datasource"));
    Resource categoryRes = resolver.getResource("/etc/tags/mecommerce/categories");

    if (ResourceUtil.isNonExistingResource(categoryRes)) {
        request.setAttribute(DataSource.class.getName(), EmptyDataSource.instance());
        return;
    }

    List<KeyValue> categories = new ArrayList<KeyValue>();
    
    if (dsCfg.get("addNone", false)) {
        categories.add(new KeyValue("", ""));
    }

    for (Iterator<Resource> it = categoryRes.listChildren(); it.hasNext();) {


        Resource category = it.next();
        Resource subCategoryRes = resolver.getResource("/etc/tags/dtc/categories/"+category.getName());
		ValueMap mainCategoryVM = ResourceUtil.getValueMap(category);
        for(Iterator<Resource> subCategoryList = subCategoryRes.listChildren();subCategoryList.hasNext();){
		Resource subcategory = subCategoryList.next();
			 ValueMap vm = ResourceUtil.getValueMap(subcategory);
             String title = vm.get("jcr:title", subcategory.getName());
             String value = mainCategoryVM.get("jcr:title", category.getName())+" - "+vm.get("jcr:title", subcategory.getName());
        	 if (!title.equals("*")) {
            	value = value;
        	 }
            categories.add(new KeyValue("dtc:categories/"+category.getName()+"/"+subcategory.getName(), value));
        	}
    }

    @SuppressWarnings("unchecked")
    DataSource ds = new SimpleDataSource(new TransformIterator(categories.iterator(), new Transformer() {
        public Object transform(Object input) {
            try {
                KeyValue category = (KeyValue) input;

                ValueMap vm = new ValueMapDecorator(new HashMap<String, Object>());
                vm.put("value", category.key);
                vm.put("text", category.value);

                return new ValueMapResource(resolver, new ResourceMetadata(), "nt:unstructured", vm);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }));
    
    request.setAttribute(DataSource.class.getName(), ds);
%><%!
    private class KeyValue {
        String key;
        String value;
        
        public KeyValue(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }
%>
package apps.dtc.commerce.components.structure.basicProduct;

import com.adobe.cq.sightly.WCMUsePojo;
import com.day.cq.wcm.api.PageManager;
import com.mec.commerce.productdetail.service.ProductDetailService;
import com.mec.commerce.productdetail.service.ProductsService;
import com.mec.core.utils.LocaleUtil;
import com.mec.commerce.utils.PMDUtil;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ResourceResolver;

import java.util.Locale;
import java.lang.Exception;

import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;

/**
 * Products use class.  It automatically generates the pmd structure if it doesn't exist.  The product template contains a productContainer component.
 * The productContainer is a wrapper that contains all components that are displayed on the product detail page.
 * The product template stores the productContainer in the PMD structure.  /content/pmd/01/00/36/01003610/productContainer
 */
public class BasicProduct extends WCMUsePojo {
	
    private final String PMD_PATH = "/content/pmd/";
    private final String PRODUCT_CONTAINER = "productContainer";
    private final String BASE_SKU = "baseSku";
    private final String MARKETS = "markets";
    private final String PMD_TEMPLATE = "dtc/commerce/template/productMasterData";
    private final String EMPTY_TEMPLATE = "/apps/dtc/core/templates/blank template";
    
    private final String PAGE_404 = "/404.html";
    
    private final String ERR_SKU_ABSENT = "The sku in the selector does not exist in the pim";
    private final String ERR_LOCALE_ABSENT = "Your path does not have a valid locale in it. Please create your page structure correctly.";

    private String errorMessage = null;
    private String productDetailPath;
    private String supplementsPath;
    private String usagePath;
    private String resourcePath;
    private boolean showSkuPicker = true;
    private String allSkus;
    private String missingPmdSkus;
    private PageManager pm;
    private boolean errorPage = false;
    private String errorPagePath = null;

    /** The service. */
    ProductDetailService service;

    ProductsService productsService;

    private String getLocaleString(String sku, Locale locale) {
        String localeStr = locale.toString();
        if (pm.getPage(PMDUtil.getPmdPath(sku)+"/"+locale) == null) {
            localeStr = locale.getLanguage();
        }
        return localeStr;
    }

    private void setPaths(String pmdPath) {
        supplementsPath = pmdPath + "pdpProductSupplements";
        usagePath = pmdPath + "pdpProductUsage";
        resourcePath = pmdPath + "productResource";
        productDetailPath += "productDetail";
    }

    @Override
    public void activate() throws Exception {
        ResourceResolverFactory resolverFactory = getSlingScriptHelper().getService(ResourceResolverFactory.class);
        ResourceResolver resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);
        service = getSlingScriptHelper().getService(ProductDetailService.class);
        productsService = getSlingScriptHelper().getService(ProductsService.class);
        String sku, pmdPath;
        String[] selectors = getRequest().getRequestPathInfo().getSelectors();
        pm = getPageManager();
        Locale locale = LocaleUtil.getLocaleFromPath(getCurrentPage().getPath());
        
        InheritanceValueMap iProperties = new HierarchyNodeInheritanceValueMap(getCurrentPage().getContentResource());
        errorPagePath = iProperties.getInherited("errorPages",String.class) + PAGE_404;

        if (locale == null) {
            errorMessage = ERR_LOCALE_ABSENT;
        }
        String country = locale.getCountry().toString();

        if (selectors.length == 1) {
            sku = selectors[0];
            //set the pmd path to load the components for the page
            pmdPath = PMDUtil.getPmdPath(sku) + "/" + getLocaleString(sku, locale) + "/jcr:content/productContainer/";
            setPaths(pmdPath);
            if(null == resourceResolver.getResource(pmdPath)){
                errorMessage = ERR_SKU_ABSENT;
            	errorPage = true;
            }
        } else if (selectors.length > 1) {
            //This url should only be hit if we are in author
            String baseSku = selectors[0];
            String variantSku = selectors[1];
            pmdPath = PMDUtil.getPmdPath(variantSku) + "/" + getLocaleString(variantSku, locale) + "/jcr:content/productContainer/";
            setPaths(pmdPath);
            if(null == resourceResolver.getResource(pmdPath)){
                errorMessage = ERR_SKU_ABSENT;
            	errorPage = true;
            }
        }
    }

    public String getErrorMessage() {
        return errorMessage;
    }
    public String getProductDetailPath() {
        return productDetailPath;
    }
    public String getSupplementsPath() { return supplementsPath; }
    public String getUsagePath() { return usagePath; }
    public String getResourcePath() { return resourcePath; }
    public boolean getErrorPage() {
        return errorPage;
    }
    public String getErrorPagePath() {
        return errorPagePath;
    }
}
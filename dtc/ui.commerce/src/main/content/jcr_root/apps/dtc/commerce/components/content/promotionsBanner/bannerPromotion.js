/*******************************************************************************
* Author id : Deloitte Digital
* 
*******************************************************************************/

"use strict";

var global = this;

use(["/libs/wcm/foundation/components/utils/AuthoringUtils.js",
    "/libs/wcm/foundation/components/utils/ResourceUtils.js",
    "/libs/sightly/js/3rd-party/q.js"], function (AuthoringUtils, ResourceUtils, Q){

    var slyComponent = {};

    var question = {};

    // The actual text content
    slyComponent.heading = granite.resource.properties["overlayHeadLineOne"] || "";

	if (!slyComponent.heading) {
		slyComponent.firstImage={};
		slyComponent.firstImage.path="";


    } else {
		var _imagesResource1 = resource.getChild("firstimage");
        var resourceRefImageProperties1 = _imagesResource1.adaptTo(Packages.org.apache.sling.api.resource.ValueMap);
    	var damFileReferencePath1 = resourceRefImageProperties1.get('fileReference');
    	damFileReferencePath1 = damFileReferencePath1.replace(" ","%20");
    	slyComponent.firstImage={};
		slyComponent.firstImage.path=damFileReferencePath1;
    }

    return slyComponent;
});
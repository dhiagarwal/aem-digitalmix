<%@ taglib prefix="cq" uri="http://www.day.com/taglibs/cq/1.0" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.dtc.core.services.EnvironmentConfig" %>
<%@ page import="com.dtc.core.utils.SolrUtils" %>
<%@ page import="org.apache.sling.api.resource.ResourceResolver" %>
<%@ page import="javax.jcr.Session" %>
<cq:defineObjects/>
<%
	if (request.getParameter("update") != null) {
		ResourceResolver resolver = resource.getResourceResolver();
		Session theSession = resolver.adaptTo(Session.class);

		SolrUtils.updateAllProducts(theSession);
	}
	else if (request.getParameter("delete") != null) {
		EnvironmentConfig environmentConfig = sling.getService(com.dtc.core.services.EnvironmentConfig.class);
		SolrUtils.deleteAllProducts(environmentConfig);
	}
//	else if (request.getParameter("deleteOrphans") != null) {
//		EnvironmentConfig environmentConfig = sling.getService(com.dtc.core.services.EnvironmentConfig.class);
//		SolrUtils.deleteOrphanProducts(SolrUtils.SKU_WILDCARD, environmentConfig);
//		SolrUtils.deleteOrphanProducts(SolrUtils.BASE_SKU_WILDCARD, environmentConfig);
//	}

	response.sendRedirect(currentNode.getPath()+".html");
%>

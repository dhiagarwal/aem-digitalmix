/*******************************************************************************
* Author id : Deloitte Digital
* 
*******************************************************************************/

"use strict";

var global = this;

use(["/libs/wcm/foundation/components/utils/AuthoringUtils.js",
    "/libs/wcm/foundation/components/utils/ResourceUtils.js",
    "/libs/sightly/js/3rd-party/q.js"], function (AuthoringUtils, ResourceUtils, Q){

    var slyComponent = {};

    var question = {};

    // The actual text content
    slyComponent.heading = granite.resource.properties["WelcomeMessage"] || "";

		if(resource.getChild("avatarImage")!=undefined && resource.getChild("avatarImage")!= null){
        var _imagesResource3 = resource.getChild("avatarImage");
        var resourceRefImageProperties3 = _imagesResource3.adaptTo(Packages.org.apache.sling.api.resource.ValueMap);
    	var damFileReferencePath3 = resourceRefImageProperties3.get('fileReference');
    	damFileReferencePath3 = damFileReferencePath3.replace(" ","%20");
    	slyComponent.avatarImage={};
		slyComponent.avatarImage.path=damFileReferencePath3;
		}

    return slyComponent;
});
/*******************************************************************************
* Author id : Deloitte Digital
* 
*******************************************************************************/

"use strict";

var global = this;

use(["/libs/wcm/foundation/components/utils/AuthoringUtils.js",
    "/libs/wcm/foundation/components/utils/ResourceUtils.js",
    "/libs/sightly/js/3rd-party/q.js"], function (AuthoringUtils, ResourceUtils, Q){

    var slyComponent = {};

    var question = {};

    // The actual text content
    slyComponent.heading = granite.resource.properties["tileHeading"] || "";

	// Set up placeholder if empty
   if (!slyComponent.heading) {
		slyComponent.secondImage={};
		slyComponent.secondImage.path="";
    } 
 else {
    	var _imagesResource2 = resource.getChild("secondimage");
    	var resourceRefImageProperties2 = _imagesResource2.adaptTo(Packages.org.apache.sling.api.resource.ValueMap);
    	var damFileReferencePath2 = resourceRefImageProperties2.get('fileReference');
    	damFileReferencePath2 = damFileReferencePath2.replace(" ","%20");
		slyComponent.secondImage={};
		slyComponent.secondImage.path=damFileReferencePath2;
    }

    return slyComponent;
});
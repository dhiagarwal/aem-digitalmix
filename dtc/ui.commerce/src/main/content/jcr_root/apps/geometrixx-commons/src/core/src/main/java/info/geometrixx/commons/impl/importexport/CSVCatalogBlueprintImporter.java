/*************************************************************************
 *
 * ADOBE CONFIDENTIAL
 * __________________
 *
 *  Copyright 2012 Adobe Systems Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Adobe Systems Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Adobe Systems Incorporated and its
 * suppliers and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Adobe Systems Incorporated.
 **************************************************************************/
package info.geometrixx.commons.impl.importexport;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.jackrabbit.JcrConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.commerce.pim.api.CatalogBlueprintImporter;
import com.adobe.cq.commerce.pim.common.AbstractBlueprintImporter;
import com.adobe.cq.commerce.pim.common.Csv;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;

/**
 *
 */
@Component(metatype = true, label = "Geometrixx-Outdoors Catalog Blueprint Importer",
        description = "CSV-based catalog blueprint importer for Geometrixx-Outdoors")
@Service
@Properties(value = {
        @Property(name = "service.description", value = "CSV-based catalog blueprint importer for Geometrixx-Outdoors"),
        @Property(name = "commerceProvider", value = "geometrixx", propertyPrivate = true)
})
public class CSVCatalogBlueprintImporter extends AbstractBlueprintImporter implements CatalogBlueprintImporter {
    private static final Logger log = LoggerFactory.getLogger(CSVCatalogBlueprintImporter.class);

    // row structure:
    //   type, title, hierarchy, design, catalog-template, section-template, product-template, product-base-path, product-tags, product-search, additional-property=value, ...
    protected static final int TYPE_COL = 0;
    protected static final int TITLE_COL = 1;
    protected static final int HIERARCHY_COL = 2;
    protected static final int DESIGN_COL = 3;
    protected static final int CATALOG_TEMPLATE_COL = 4;
    protected static final int SECTION_TEMPLATE_COL = 5;
    protected static final int PRODUCT_TEMPLATE_COL = 6;
    protected static final int PRODUCT_BASE_PATH_COL = 7;
    protected static final int PRODUCT_TAGS_COL = 8;
    protected static final int PRODUCT_SEARCH_COL = 9;
    protected static final int FIRST_CUSTOM_PROP_COL = 10;

    protected Iterator<String[]> inputIterator;
    protected String currentCatalogPath;

    @Override
    protected boolean validateInput(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {
        ResourceResolver resourceResolver = request.getResourceResolver();

        String provider = request.getParameter("provider");
        if (provider == null || provider.length() == 0) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "No commerce provider specified.");
            return false;
        }

        String csvPath = request.getParameter("csvPath");
        InputStream is;
        try {
            Resource csvResource = resourceResolver.getResource(csvPath);
            Node source = csvResource.adaptTo(Node.class);
            is = source.getProperty(JcrConstants.JCR_CONTENT + "/" + JcrConstants.JCR_DATA).getBinary().getStream();
        } catch (Exception e) {
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Catalog Blueprint CSV [" + csvPath + "] not found.");
            return false;
        }

        String sep = request.getParameter("separator");
        String del = request.getParameter("delimiter");
        Csv csv = new Csv();
        if (sep != null) {
            csv.setFieldSeparatorRead(sep.charAt(0));
        }
        if (del != null) {
            csv.setFieldDelimiter(del.charAt(0));
        }
        inputIterator = csv.read(is, "utf-8");

        if (!inputIterator.hasNext()) {
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Catalog Blueprint CSV empty.");
            return false;
        }

        // first row is header
        inputIterator.next();

        if (!inputIterator.hasNext()) {
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Catalog Blueprint CSV contains header but no sections.");
            return false;
        }

        return true;
    }

    @Override
    protected void doImport(ResourceResolver resourceResolver, Node storeRoot, boolean incrementalImport)
            throws RepositoryException, IOException {

        Session session = resourceResolver.adaptTo(Session.class);

        String storePath = storeRoot.getPath();
        currentCatalogPath = null;

        while (inputIterator.hasNext()) {
            String[] cols = inputIterator.next();
            String type = get(cols, TYPE_COL);
            if (type.equals("catalog")) {
                processCatalogRow(resourceResolver, storePath, cols, session);
            } else if (type.equals("section")) {
                processSectionRow(resourceResolver, cols, session);
            } else {
                logMessage("ERROR unknown type: " + type, true);
                log.error("Row of unknown type: " + type);
            }
        }
    }

    protected void processCatalogRow(ResourceResolver resourceResolver, String storePath, String[] cols, Session session) {
        String catalogTitle = get(cols, TITLE_COL);
        try {
            PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
            Page catalog = createCatalog(pageManager, storePath, catalogTitle, session);
            currentCatalogPath = catalog.getPath();
            setProperties(resourceResolver, catalog, cols);
        } catch (Exception e) {
            logMessage("ERROR creating catalog " + catalogTitle, true);
            log.error("Failed to create catalog " + catalogTitle, e);
        }
    }

    protected void processSectionRow(ResourceResolver resourceResolver, String[] cols, Session session) {
        if (currentCatalogPath == null) {
            // something went wrong creating parent; bail out
            return;
        }

        String sectionTitle = get(cols, TITLE_COL);
        String hierarchyString = get(cols, HIERARCHY_COL);
        if (StringUtils.isEmpty(hierarchyString)) {
            logMessage("ERROR adding section (section hierarchy must contain at least its own node name)", true);
            log.error("Section hierarchy must contain at least its own node name");
            return;
        }
        String[] hierarchy = hierarchyString.split(",");
        try {
            PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
            Page section = createSection(pageManager, currentCatalogPath, hierarchy, sectionTitle, session);
            setProperties(resourceResolver, section, cols);
        } catch (Exception e) {
            logMessage("ERROR creating section " + hierarchyString, true);
            log.error("Failed to create section " + hierarchyString, e);
        }
    }

    protected void setProperties(ResourceResolver resourceResolver, Page page, String[] cols) throws RepositoryException {
        Node contentNode = page.getContentResource().adaptTo(Node.class);

        String design = get(cols, DESIGN_COL);
        if (StringUtils.isNotEmpty(design)) {
            setProperty(contentNode, "target/cq_designPath", design);
        }

        String template = get(cols, CATALOG_TEMPLATE_COL);
        if (StringUtils.isNotEmpty(template)) {
            setProperty(contentNode, "templates/catalog", template);
        }
        template = get(cols, SECTION_TEMPLATE_COL);
        if (StringUtils.isNotEmpty(template)) {
            setProperty(contentNode, "templates/section", template);
        }
        template = get(cols, PRODUCT_TEMPLATE_COL);
        if (StringUtils.isNotEmpty(template)) {
            setProperty(contentNode, "templates/product", template);
        }

        String basePath = get(cols, PRODUCT_BASE_PATH_COL);
        if (StringUtils.isNotEmpty(basePath)) {
            setProperty(contentNode, "filter/basePath", basePath);
        }
        String tagsString = get(cols, PRODUCT_TAGS_COL);
        if (StringUtils.isNotEmpty(tagsString)) {
            String[] matchTags = tagsString.split(",");
            createMissingTags(resourceResolver, matchTags);
            setProperty(contentNode, "filter/matchTags", "[" + tagsString + "]");
        }
        String search = get(cols, PRODUCT_SEARCH_COL);
        if (StringUtils.isNotEmpty(search)) {
            String searchType = search.substring(0, search.indexOf(":"));
            setProperty(contentNode, "filter/searchType", searchType);
            setProperty(contentNode, "filter/search", search);
        }

        for (int i = FIRST_CUSTOM_PROP_COL; i < cols.length; i++) {
            String customPropDefinition = cols[i];
            if (StringUtils.isNotEmpty(customPropDefinition)) {
                String parts[] = customPropDefinition.split("=", 2);
                String name = parts[0].trim();
                if (parts.length != 2) {
                    logMessage("ERROR adding custom property: syntax error in property definition", true);
                    log.error("Custom property syntax error");
                } else {
                    setProperty(contentNode, name, parts[1]);
                }
            }
        }
    }

    private String get(String[] cols, int i) {
        if (i < cols.length) {
            return cols[i];
        } else {
            return null;
        }
    }

}

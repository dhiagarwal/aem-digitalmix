/*************************************************************************
 *
 * ADOBE CONFIDENTIAL
 * __________________
 *
 *  Copyright 2012 Adobe Systems Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Adobe Systems Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Adobe Systems Incorporated and its
 * suppliers and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Adobe Systems Incorporated.
 **************************************************************************/
package info.geometrixx.commons.impl.productrelationships;

import com.adobe.cq.commerce.api.CommerceConstants;
import com.adobe.cq.commerce.api.CommerceException;
import com.adobe.cq.commerce.api.CommerceSession;
import com.adobe.cq.commerce.api.Product;
import com.adobe.cq.commerce.api.ProductRelationship;
import com.adobe.cq.commerce.api.ProductRelationshipsProvider;
import com.adobe.cq.commerce.api.promotion.Promotion;
import com.adobe.cq.commerce.api.promotion.PromotionHandler;
import com.adobe.cq.commerce.common.AbstractJcrCommerceSession;
import com.adobe.cq.commerce.common.CommerceHelper;
import com.adobe.cq.commerce.common.DefaultProductRelationship;
import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import info.geometrixx.commons.PerfectPartnerPromotionHandler;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Deactivate;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.wrappers.ValueMapDecorator;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <code>PerfectPartnerRelationshipProvider</code> supplies product relationships regarding
 * perfect-partner pairs.
 */
@Component(metatype = true,
        label = "Geometrixx-Outdoors PerfectPartner Recommendations Provider",
        description = "Example ProductRelationshipsProvider for perfect partner promotions")
@Service
@Properties(value = {
        @Property(name = "service.description", value = "Example ProductRelationshipsProvider for perfect partner promotions"),
        @Property(name = ProductRelationshipsProvider.RELATIONSHIP_TYPE_PN, value = PerfectPartnerRelationshipsProvider.RELATIONSHIP_TYPE, propertyPrivate = true)
})
public class PerfectPartnerRelationshipsProvider implements ProductRelationshipsProvider {

    public static final String RELATIONSHIP_TYPE = "info.geometrixx.perfect-partner";
    public static final String RELATIONSHIP_TITLE = "Perfect partners";

    private boolean enabled;

    @Property(boolValue = true, label = "Enable", description = "Provide recommendations")
    public final static String ENABLED = RELATIONSHIP_TYPE + ".enabled";

    @SuppressWarnings ("unused")
    @Activate
    private void activate(ComponentContext context) throws IOException {
        enabled = PropertiesUtil.toBoolean(context.getProperties().get(ENABLED), true);
    }

    @SuppressWarnings ("unused")
    @Deactivate
    private void deactivate() throws IOException {
    }

    @Override
    public Map<String, String> getRelationshipTypes() {
        Map<String, String> types = new HashMap<String, String>(0);
        types.put(RELATIONSHIP_TYPE, RELATIONSHIP_TITLE);
        return types;
    }

    /**
     * @return a list of products which complete perfect-partner-pairs with products already in the cart
     */
    @Override
    public List<ProductRelationship> getRelationships(SlingHttpServletRequest request, CommerceSession session, Page page, Product product)
            throws CommerceException {
        if (!enabled) {
            return null;
        }

        //
        // Don't provide relationships to non-Geometrixx pages:
        //
        if (page != null) {
            InheritanceValueMap properties = new HierarchyNodeInheritanceValueMap(page.getContentResource());
            String commerceProvider = properties.getInherited(CommerceConstants.PN_COMMERCE_PROVIDER, String.class);
            if (commerceProvider != null && !commerceProvider.equals("geometrixx")) {
                return null;
            }
        }

        PageManager pageManager = request.getResourceResolver().adaptTo(PageManager.class);
        if (session == null || pageManager == null) {
            return null;
        }

        Map<String, String> potentials = new HashMap<String, String>();
        if (session instanceof AbstractJcrCommerceSession) {
            List<Promotion> promos = ((AbstractJcrCommerceSession) session).getActivePromotions();
            for (Promotion p : promos) {
                PromotionHandler ph = p.adaptTo(PromotionHandler.class);
                if (ph instanceof PerfectPartnerPromotionHandler) {
                    ((PerfectPartnerPromotionHandler) ph).getPotentials(session, p, potentials);
                }
            }
        }

        List<ProductRelationship> relationships = new ArrayList<ProductRelationship>();

        for (String referenceProductPagePath : potentials.keySet()) {
            String crossSellProductPagePath = potentials.get(referenceProductPagePath);
            Page crossSellProductPage = pageManager.getPage(crossSellProductPagePath);
            Product crossSellProduct = crossSellProductPage != null ? CommerceHelper.findCurrentProduct(crossSellProductPage) : null;
            if (crossSellProduct != null) {
                ProductRelationship relationship = new DefaultProductRelationship(RELATIONSHIP_TYPE, RELATIONSHIP_TITLE, crossSellProduct);
                relationship.getMetadata().put("price", session.getProductPrice(crossSellProduct));
                relationship.getMetadata().put("partner", referenceProductPagePath);
                relationships.add(relationship);
            }
        }

        return relationships;
    }
}

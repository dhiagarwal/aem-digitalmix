/*************************************************************************
 *
 * ADOBE CONFIDENTIAL
 * __________________
 *
 *  Copyright 2012 Adobe Systems Incorporated
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Adobe Systems Incorporated and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Adobe Systems Incorporated and its
 * suppliers and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Adobe Systems Incorporated.
 **************************************************************************/
package info.geometrixx.commons.impl.productrelationships;

import com.adobe.cq.commerce.api.CommerceConstants;
import com.adobe.cq.commerce.api.CommerceException;
import com.adobe.cq.commerce.api.CommerceSession;
import com.adobe.cq.commerce.api.PlacedOrder;
import com.adobe.cq.commerce.api.PlacedOrderResult;
import com.adobe.cq.commerce.api.Product;
import com.adobe.cq.commerce.api.ProductRelationship;
import com.adobe.cq.commerce.api.ProductRelationshipsProvider;
import com.adobe.granite.security.user.UserProperties;
import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.personalization.UserPropertiesUtil;
import com.day.cq.wcm.api.Page;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Deactivate;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <code>PersonalizedRecommendationsProvider</code> provides a list of relationships to products
 * having at least one tag in common with a previously-purchased product.  The relationships are
 * sorted on the number of matched tags and the order history.
 *
 * NB: this is an example relationship provider which trades off performance for simplicity.  A
 * production system would require a much more performant implementation.
 */
@Component(metatype = true,
        label = "Geometrixx-Outdoors Personalized Recommendations Provider",
        description = "Example ProductRelationshipsProvider which recommends other products based on order history")
@Service
@Properties(value = {
        @Property(name = "service.description", value = "Example ProductRelationshipsProvider which recommends products based on order history"),
        @Property(name = ProductRelationshipsProvider.RELATIONSHIP_TYPE_PN, value = PersonalizedRecommendationsProvider.RELATIONSHIP_TYPE, propertyPrivate = true)
})
public class PersonalizedRecommendationsProvider implements ProductRelationshipsProvider {

    public static final String RELATIONSHIP_TYPE = "info.geometrixx.personalized-recommendations";
    public static final String RELATIONSHIP_TITLE = "For {givenName}";

    private boolean enabled;

    @Property(boolValue = true, label = "Enable", description = "Provide recommendations")
    public final static String ENABLED = RELATIONSHIP_TYPE + ".enabled";

    @SuppressWarnings ("unused")
    @Activate
    private void activate(ComponentContext context) throws IOException {
        enabled = PropertiesUtil.toBoolean(context.getProperties().get(ENABLED), true);
    }

    @SuppressWarnings ("unused")
    @Deactivate
    private void deactivate() throws IOException {
    }

    @Override
    public Map<String, String> getRelationshipTypes() {
        Map<String, String> types = new HashMap<String, String>(0);
        types.put(RELATIONSHIP_TYPE, RELATIONSHIP_TITLE);
        return types;
    }

    /**
     * @return a list of relationships to products having at least one tag in common with a product in
     * the current user's order history
     */
    @Override
    public List<ProductRelationship> getRelationships(SlingHttpServletRequest request, CommerceSession session, Page currentPage,
                                                      Product currentProduct) throws CommerceException {
        if (!enabled) {
            return null;
        }

        //
        // Don't provide relationships to non-Geometrixx pages:
        //
        if (currentPage != null) {
            InheritanceValueMap properties = new HierarchyNodeInheritanceValueMap(currentPage.getContentResource());
            String commerceProvider = properties.getInherited(CommerceConstants.PN_COMMERCE_PROVIDER, String.class);
            if (commerceProvider != null && !commerceProvider.equals("geometrixx")) {
                return null;
            }
        }

        if (session == null) {
            return null;
        }

        final UserProperties userProperties = request.adaptTo(UserProperties.class);
        String title = RELATIONSHIP_TITLE;
        if (UserPropertiesUtil.isAnonymous(userProperties)) {
            return null;
        } else {
            try {
                title = title.replace("{givenName}", userProperties.getProperty(UserProperties.GIVEN_NAME));
            } catch (Exception e) {
                // best efforts
                title = RELATIONSHIP_TITLE;
            }
        }

        //
        // Add products in order history to context:
        //
        List<Product> contextProducts = new ArrayList<Product>();
        PlacedOrderResult placedOrders = session.getPlacedOrders(null, 0, 5, null);
        for (PlacedOrder order : placedOrders.getOrders()) {
            List<CommerceSession.CartEntry> cartEntries = order.getCartEntries();
            for (CommerceSession.CartEntry entry : cartEntries) {
                if (entry.getProduct() != null) {
                  contextProducts.add(entry.getProduct());
                }
            }
        }

        //
        // Walk content-pages to find similar products:
        //
        ResourceResolver resolver = request.getResourceResolver();
        SimilarProductsCollector collector = new SimilarProductsCollector(resolver, session, RELATIONSHIP_TYPE, title, contextProducts);
        collector.walk(resolver.getResource("/content/geometrixx-outdoors/en"));
        return collector.getRelationships();
    }
}

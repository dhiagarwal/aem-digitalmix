<%@include file="/libs/foundation/global.jsp"%><%
%><%@ page language="java"
           pageEncoding="UTF-8"
           import="
           org.apache.commons.io.IOUtils,
           java.io.ByteArrayOutputStream"%>
<%@ page import="com.mec.commerce.api.XmlProductFeedImporter" %>
<%
    //load up all of the xml into a string
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    IOUtils.copy(request.getInputStream(), bytes);
    String productXml = new String(bytes.toByteArray());
    XmlProductFeedImporter importer = sling.getService(XmlProductFeedImporter.class);
    importer.importProducts(productXml);
%>
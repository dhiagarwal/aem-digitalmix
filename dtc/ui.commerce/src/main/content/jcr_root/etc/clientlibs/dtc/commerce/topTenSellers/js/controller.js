(function() {
"use strict";


topTenSellers.controller('topTenSellersController', ['$scope', 'topTenSellersService','userProfileManager','$q', function($scope,topTenSellersService,userProfileManager,$q) {

    
    var self = this; 
    self.show = true;
	self.hidePSV = true;
	self.productIds = [];
	self.parentProductIds = [];
	self.showPriceRange = true;
    self.max = self.max || 300;

    function getParameterByName(param) {
            var result =  window.location.search.match(
            new RegExp("(\\?|&)" + param + "(\\[\\])?=([^&]*)")
        );

        return result ? result[3] : false;
    }

    var userType;
    if(userProfileManager.isUserLoggedIn()) {
        var userTypes = userProfileManager.getUserProfileMasterInfo().userType.toLowerCase().split("_");
        if(userTypes.length > 1) {
            userType = userTypes[userTypes.length-1];
        } else {
            userType = userTypes[0];
        }
        
        if(userType == "distributor"){
            self.hidePSV = false;
        }
    }

    function init(){

        var param = getParameterByName("param");
		self.inParamVal=param;

		if(!param && getCookie('dtcParam')){
            self.inParamVal = getCookie('dtcParam');
        }

    	angular.element('[name="prodSKU"]').each(function(){
            self.productIds.push($(this).val());
        });
    	
    	angular.element('[name="parentProdSKU"]').each(function(){
            self.parentProductIds.push($(this).val());
            
        });
    	
    	self.getAvgRatings(self.parentProductIds);  	
    	
        var productSkus = self.productIds.join(",");
        var productDetailsPrice = topTenSellersService.getPriceData(productSkus);
        productDetailsPrice.then(function(data){
            var priceList = data.priceList;
			var stockStatus = "";
			if(priceList != undefined)			
			{
            for(var i=0; i < priceList.length ; i++){
				
				if(!priceList[i].hasOwnProperty('priceRange')){
					self.showPriceRange = true;
                    if (priceList[i].hasOwnProperty("stockData")) {
                        
                        if (priceList[i].stockData.hasOwnProperty("lineItemDetailsList") && (priceList[i].stockData.lineItemDetailsList[0].hasOwnProperty("safetyStockStatus"))) {
                            var stockStatusData = priceList[i].stockData.lineItemDetailsList[0];
                            
                            if (stockStatusData.safetyStockStatus == "LOW_STOCK") {
                                stockStatus = "LS";
                            } else if (stockStatusData.safetyStockStatus == "OUT_OF_STOCK") {
                                stockStatus = "OS";
                            } else if (stockStatusData.safetyStockStatus == "NORMAL") {
                                stockStatus = "";
                            } 
                            if((stockStatusData.safetyStockStatus == "OUT_OF_STOCK" || stockStatusData.safetyStockStatus == "LOW_STOCK") && stockStatusData.backOrdered){
                                stockStatus = "BO";
                            }
                            
                        }
                        
                    }  
                    
                    

                    if (priceList[i].hasOwnProperty("wholeSaleStatus")) {
                        if(priceList[i].wholeSaleStatus == "02"){
                            self.showPriceRange = false;
							stockStatus = "OS";
							angular.element('.psvSeperator-'+priceList[i].code).hide();
                        }
                        if(priceList[i].wholeSaleStatus == "05"){
                            self.showPriceRange = true;
							stockStatus = "OS";
                        }   
                    }
                    angular.element('.stock-'+priceList[i].code+'-'+stockStatus).removeClass('ng-hide');
                }
				
				if(self.showPriceRange){
                
                if(priceList[i].hasOwnProperty('priceRange')){
                   if(priceList[i].priceRange.hasOwnProperty('maxPrice') && priceList[i].priceRange.hasOwnProperty('minPrice') ){
						if(priceList[i].priceRange.maxPrice.value > priceList[i].priceRange.minPrice.value){
							angular.element('.price-'+priceList[i].code).html(priceList[i].priceRange.minPrice.formattedValue + " - "+ priceList[i].priceRange.maxPrice.formattedValue );
						}
						else{
							angular.element('.price-'+priceList[i].code).html(priceList[i].priceRange.minPrice.formattedValue);
						}
					}
					if(priceList[i].priceRange.hasOwnProperty('maxPv') && self.hidePSV == false && priceList[i].priceRange.hasOwnProperty('minPv')){
						var minPSV = (priceList[i].priceRange.minPv.value).toFixed(2);
		                var maxPSV = (priceList[i].priceRange.maxPv.value).toFixed(2);
						if(priceList[i].priceRange.maxPv.value > priceList[i].priceRange.minPv.value){
							angular.element('.psv-'+priceList[i].code).html(minPSV + " - " + maxPSV);
                        }
                        else{
                        	angular.element('.psv-'+priceList[i].code).html(minPSV);
                        }
					}

                }
                else{
					if(priceList[i].hasOwnProperty('price'))
					{
						angular.element('.price-'+priceList[i].code).html(priceList[i].price.formattedValue);
					}
					if(priceList[i].hasOwnProperty('pvValue') && self.hidePSV == false)
					{
						var pvValue = (priceList[i].pvValue.value).toFixed(2);
						angular.element('.psv-'+priceList[i].code).html(pvValue);
					}
                	
				}
			}
			}
        }

        });
    }
    
    self.getAvgRatings = function(skuIds) {
    	var avgRatingsPromiseArray=[];
    	for (var i=0; i<skuIds.length; i++ ) {
    		var promise = topTenSellersService.getRatings(skuIds[i]);
    		avgRatingsPromiseArray.push(promise);
    	}
    	
    	 $q.all(avgRatingsPromiseArray).then(function (data){
             for(var i=0;i<data.length;i++){
                 var ratingValue = Math.round(data[i].streamInfo.avgRatings._overall);
                 var rating = data[i].streamInfo.commentCount || 0;
                 var starRating = self.getStarRatingPosition(ratingValue, 'big');
                 var productSkuId = data[i].streamInfo.rssURL.split('/');
                 var path_skuId = productSkuId[productSkuId.length-1];
                 angular.element('.stars-'+path_skuId).attr("style","background-position: "+starRating);
             }
         });
    }
    
    self.getStarRatingPosition = function(ratingDecimalValue, starType) {
        var starTopPosition = 0;
        var starMargin = 0;
        var starWidth = 0;
        var halfStarTopMargin = 0,
            smallStarFirstPosition = 0;
        var halfStarPosition = -273;
        if (starType == "big") {
            starWidth = 18;
            halfStarPosition = -273;
            halfStarTopMargin = -26;
        } else {
            starWidth = 12;
            halfStarPosition = -242;
            halfStarTopMargin = -64;
            smallStarFirstPosition = -44;
        }
        var decimalValue = ratingDecimalValue.toString().replace(/^[^\.]+/, '0');
        var floorRatingValue = Math.floor(ratingDecimalValue);
        decimalValue = parseFloat(decimalValue);
        var mutipleValue = 0;
        if (0.26 <= decimalValue && decimalValue <= 0.74) {
            starMargin = halfStarTopMargin;
            floorRatingValue++;
        } else if (0.75 <= decimalValue && decimalValue <= 0.99) {
            starMargin = smallStarFirstPosition;
            floorRatingValue++;
        } else {
            starMargin = smallStarFirstPosition;
        }
        if (floorRatingValue >= this.max) {
            starMargin = smallStarFirstPosition;
            floorRatingValue = this.max;
        }
        floorRatingValue = floorRatingValue * starWidth;
        floorRatingValue = halfStarPosition + floorRatingValue
        return floorRatingValue + "px " + starMargin + "px";
    }
    init();
}]);
}());

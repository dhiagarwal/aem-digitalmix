(function() {
"use strict";

cartRecommendations.service('cartRecommendationsService', ['$http', '$q','userProfileManager','serviceURL', function($http, $q,userProfileManager,serviceURL) {

	var cartServiceBaseSite = "cosmetics";
    var user_id;
    var endPoint;
    var config;
    if(userProfileManager.isUserLoggedIn()) {
        user_id = userProfileManager.getUserProfileMasterInfo().userId;
        endPoint = mulesoftHybrisEndpoint;
    } else {
        user_id = "anonymous";
        endPoint = mulesoftHybrisGuestEndpoint;
    }

    var deferred = $q.defer();
	
	this.getCartData = function(cartID) {
        var deferred = $q.defer();
        
        var paramsArray = [];
        paramsArray[0] = endPoint;
        paramsArray[1] = cartServiceBaseSite;
        paramsArray[2] = user_id;
        paramsArray[3] = cartID;

        var url = serviceURL.GetServiceURL('cartURL' , paramsArray);
        
        
        
        $http.get(url).then(function(response) {
            deferred.resolve(response.data);
        },function(response) {
            console.log("Error in get Cart Service");
		});
        return deferred.promise;
    }

     this.getcartRecommendations = function(skuId,currentPath){
        var deferred = $q.defer();
        
        var paramsArray = [];
        paramsArray[0] = skuId;
        paramsArray[1] = currentPath;

        var url = serviceURL.GetServiceURL('cartRecommendationsURL' , paramsArray);
        
        $http({
            method: 'GET',
            url: url,
        }).then(function successCallback(response) {
            deferred.resolve(response.data);
        },function (response){ 
           console.log("Error in service")});
        return deferred.promise;
    }

    this.getPriceData = function(id) {
		var deferred = $q.defer();
		
		var paramsArray = [];
        paramsArray[0] = endPoint;
        paramsArray[1] = cartServiceBaseSite;
        paramsArray[2] = user_id;
        paramsArray[3] = id;

        var url = serviceURL.GetServiceURL('productsPriceURL' , paramsArray);
		
		$http.post(url).then(function(response) {
			deferred.resolve(response.data);
		},function (response){ console.log("Error in calling multiple prices: cartRecommendations : getPriceData")});
		return deferred.promise;
	}
	
	this.getRatings = function(sku) {
		var deferred = $q.defer();
		
		var paramsArray = [];
        paramsArray[0] = sku;

        var url = serviceURL.GetServiceURL('ratingsURL' , paramsArray);
		
		$http.jsonp(url).then(function(response) {
			deferred.resolve(response.data);
		});
		return deferred.promise;
	}
	
}]);
}());
(function() {
"use strict";

shoppingCart.controller('shoppingCartComponentController', ['$scope', 'shoppingCartComponentService', '$rootScope', 'userProfileManager','$cookies', function($scope, shoppingCartComponentService,$rootScope,userProfileManager,$cookies) {

	var cartID = $cookies.get('cart_guid');

    var self = this;
	self.hidePSV = true;
    this.bag = [];
    this.voucherApplied = [];
    var model = this.model = {};
	var prodPath = angular.element('input[name=productsPath]').val();
	var cartmodifiedMessage = angular.element('input[name=cartModified]').val();
    var outOfStockMessage = angular.element('input[name=outOfStockMessage]').val();
	var comingSoonMessage = angular.element('input[name=comingSoonMessage]').val();
	var itemsModifiedMessage = angular.element('input[name=itemsModified]').val();
    var shoppingCartRendition = angular.element('input[name=shoppingCartRendition]').val();
	var backOrderMessage = angular.element('input[name=backOrderMessage]').val();

    $scope.errPromoCode;
    var user_id;
	var userType;
	self.isUserLoggedIn = false;
    if(userProfileManager.isUserLoggedIn()) {
		self.isUserLoggedIn = true;
        user_id = userProfileManager.getUserProfileMasterInfo().userId;
        var userTypes = userProfileManager.getUserProfileMasterInfo().userType.toLowerCase().split("_");
        if(userTypes.length > 1) {
            userType = userTypes[userTypes.length-1];
        } else {
            userType = userTypes[0];
        }

        if(userType == "distributor"){
    		self.hidePSV = false;
        }
    } else {
		self.isUserLoggedIn = false;
        user_id = "anonymous";
    }
    var totalPriceObj = {
        "value": "0.00"
    }

    self.currencySymbol = "$";
	
	$rootScope.$on("updateCart", function(){
        self.initShoppingCart();
        $rootScope.$emit("updateCartRecommendations");
    });

	self.initShoppingCart = function(){
		if (cartID != undefined && cartID.length != 0) {
			var promise = shoppingCartComponentService.getCartData(cartID);
			promise.then(function(data) {
				self.updateBagEntries(data.totalUnitCount); 
				if (data.hasOwnProperty("appliedVouchers")) {
					for (var i = 0; i < data.appliedVouchers.length; i++) {
						self.voucherApplied.push(data.appliedVouchers[i].voucherCode);
					}
				}
				if (data.totalDiscounts.value != 0) {
					self.model.savings = true;
				}
				if (data.entries != undefined) {
					self.getImageProdUrl(data);
					self.bag = data;
					self.currencySymbol = self.bag.totalPrice.formattedValue.charAt(0);
					self.updateStockStatus(self.bag);

				} else {
					self.bag.totalUnitCount = 0;
					self.bag.totalPrice = totalPriceObj;
				}
			});
		} else {
			self.bag.totalUnitCount = 0;
			self.bag.totalPrice = totalPriceObj;
		}
	}

	self.stockStatus = function(value, item){
  		var i = item;
  		if(i == value){
    		return true;
  		}
  		else{
        	return false;
  		}
	};

    $scope.removeItem = function(code) {
    var promise = shoppingCartComponentService.updatedCartData(code, cartID);
    promise.then(function(data) {
		self.bag = data;
		self.getImageProdUrl(data);
		setTimeout(function() {
				$rootScope.$emit("updateCartRecommendations");
				}, 2000)
		if (self.bag.totalItems != 0) {
			self.isApply = false;
			} else {
				self.isApply = true;
				if (data.hasOwnProperty("appliedVouchers")) {
					for (var i = 0; i < data.appliedVouchers.length; i++) {
						var promoCode = data.appliedVouchers[i].voucherCode;
						self.removePromoCode(promoCode);
					}
				}
			}
		self.updateBagEntries(data.totalUnitCount);
		self.getCartOnDeleteItem();
		});
	};

    this.changeQuantity = function(value, index, skuId, stockLeft,
        operator) {
        if (operator == '-' && value > 1) {
            this.bag.entries[index].quantity--;
            self
                .getPriceValueOnModifyQuantity(
                    this.bag.entries[index].quantity,
                    this.bag.entries[index].product.baseOptions[0].selected.priceData.value,
                    index, skuId);
        }
        if (operator == '+' && value < stockLeft ) {
            this.bag.entries[index].quantity++;
            self
                .getPriceValueOnModifyQuantity(
                    this.bag.entries[index].quantity,
                    this.bag.entries[index].product.baseOptions[0].selected.priceData.value,
                    index, skuId);
        }

    };

    this.getPriceValueOnModifyQuantity = function(qty, cost, index, skuId) {
        var promise = shoppingCartComponentService
            .updateQuantity(user_id, cartID, qty, skuId);
        promise.then(function(data) {
			for(var i=0; i<data.entries.length;i++){
				var obj = self.bag.entries[i];
                obj["totalPrice"]["value"] = data.entries[i].totalPrice.value;
                obj["pvValue"] = data.entries[i].pvValue;
            }

            var priceValues = self.bag;
            priceValues["totalDiscounts"]["formattedValue"]=data.totalDiscounts.formattedValue;
            priceValues["subTotal"]["value"] = data.subTotal.value;
            priceValues["totalDiscounts"]["value"] = data.totalDiscounts.value;
			priceValues["totalUnitCount"] = data.totalUnitCount;
			self.updateBagEntries(data.totalUnitCount);
        });

    };

    this.redirectToPDPPage = function() {
    	window.location.href = angular.element($('#homePagePath')).val()+".html";
    };
    
    self.applyPromoCode = function(code,errorDefault) {
        if (!code) {
            self.model.err = true;
            $scope.errPromoCode = errorDefault;
            return;
        }
        if (cartID != undefined && cartID != null && cartID != '') {
            var applyVoucherResponse = shoppingCartComponentService
                .applyVoucher(code, cartID);
            applyVoucherResponse.then(function(data) {
                if (data.hasOwnProperty("errors") && data != '') {
                    var error = data.errors[0].message;
                    if (error.indexOf("ERR_VOUCHER_002") > -1) {
                        self.model.err = true;
                        $scope.errPromoCode = "Voucher " + code + " cannot be redeemed";
                        return;
                    } else if (error.indexOf("ERR_VOUCHER_001") > -1) {
                        self.model.err = true;
                        $scope.errPromoCode = "Invalid Promotional Code";
                        return;
                    } else if (error.indexOf("ERR_VOUCHER_003") > -1) {
                        self.model.err = true;
                        $scope.errPromoCode = "Error while applying voucher";
                        return;
                    }
                } else {
                    self.voucherApplied.push(code);
					if(self.voucherApplied.length > 1){
						self.voucherApplied.splice(0, 1);
                    }
                    var getCartResponse = shoppingCartComponentService.getCartData(cartID);
                    getCartResponse.then(function(data) {
                        self.model.savings = true;
                        self.bag.totalDiscounts.formattedValue = data.totalDiscounts.formattedValue;
                        self.bag.subTotal.value = data.subTotal.value;
                        self.bag.totalDiscounts.value = data.totalDiscounts.value;
						self.updateStockStatus(data);
                    });
                    self.promoCode = '';
                }
            });
        } else {
            self.model.promoCode = '';
        }
    }

    self.removePromoCode = function(code) {
        if (cartID != undefined && cartID != null && cartID != '') {
            var removeVoucherResponse = shoppingCartComponentService.removeVoucher(code,
                cartID);
            removeVoucherResponse.then(function(data) {
                if (self.model.err == true) {
                    self.toggleErr();
                }
                self.voucherApplied.pop(code);
                self.promoCode = '';
                var getCartResponse = shoppingCartComponentService.getCartData(cartID);
                getCartResponse.then(function(data) {
                    if (data.totalDiscounts.value != 0) {
                        self.model.savings = true;
                    } else {
                        self.model.savings = false;
                    }
					self.bag.subTotal.value = data.subTotal.value;
                    self.bag.totalDiscounts.value = data.totalDiscounts.value;
					self.updateStockStatus(data);
                });
            });
        }
    }

    self.toggleErr = function() {
        self.model.err = false;
    }

    self.getPdfPath = function(basePath) {
        var url = basePath + "/" +(window.location.pathname.split( '/' )[3]).split('_')[0]+".pdf";
        window.open(url, '_blank');
    }
	
	self.updateStockStatus = function(data){
        var entries = data.entries;
		var stock = "";
		var toastEnabled = false;
         if (data.entries != undefined) {
            for (var index = 0; index < entries.length; index++) {
				var obj = self.bag.entries[index];
        			if (entries[index].hasOwnProperty("stockData")) {
            			stock = entries[index].stockData.lineItemDetailsList;
            			for (var i = 0; i < stock.length; i++) {
               				if (stock[i].hasOwnProperty("safetyStockStatus")) {
                                if (stock[i].safetyStockStatus == "LOW_STOCK") {
                                    if (entries[index].quantityUpdated) {
                                        obj["stockStatus"] = "LS";
                                        obj["stockUpdate"] = '';
                                        obj["stockDescription"] = cartmodifiedMessage;
										obj["stockRemaining"] = stock[i].totalAvailableQuantity;
										toastEnabled = true;
                                        self.showToastMessage(itemsModifiedMessage);
                                    } else {
                                        obj["stockStatus"] = "LS";
                                        obj["stockUpdate"] = stock[i].totalAvailableQuantity;
                                        obj["stockDescription"] = '';
										obj["stockRemaining"] = stock[i].totalAvailableQuantity;
                                    }
            
                                } 
                                else if (stock[i].safetyStockStatus == "OUT_OF_STOCK") {
                                    if(stock[i].hasOwnProperty("backOrdered") && stock[i].backOrdered == false){
										obj["stockStatus"] = "OS";
										obj["stockDescription"] = outOfStockMessage;
										obj["stockRemaining"] = stock[i].totalAvailableQuantity;
										toastEnabled = true;
										self.showToastMessage(outOfStockMessage);
									}
                                } 
                                else if (stock[i].safetyStockStatus == "NORMAL") {
                                    obj["stockStatus"] = '';
                                    obj["stockUpdate"] = '';
                                    obj["stockDescription"] = '';
									obj["stockRemaining"] = stock[i].totalAvailableQuantity;
                       		 	}
                            if (stock[i].safetyStockStatus == "OUT_OF_STOCK"  && stock[i].backOrdered && stock[i].hasOwnProperty("backOrdered")) {
                                if(stock[i].hasOwnProperty("backOrderedDate")){
                                    obj["stockStatus"] = "BO";
                                    var dateString = stock[i].backOrderedDate;
                                    var backOrderDate = self.formatDate(dateString);
                                    obj["stockUpdate"] = backOrderDate;
                                    obj["stockDescription"] = '';
									obj["stockRemaining"] = 50000;
									toastEnabled = true;
									self.showToastMessage(backOrderMessage);
                                }                               
                        	}
                    	}
						if(entries[index].product.hasOwnProperty("wholeSaleStatus") && entries[index].product.wholeSaleStatus == "02")
                        {
							 obj["stockStatus"] = "OS";
                             obj["stockDescription"] = outOfStockMessage;
							 obj["stockRemaining"] = stock[i].totalAvailableQuantity;
							 toastEnabled = true;
							 self.showToastMessage(outOfStockMessage);
                        }
						if(entries[index].product.hasOwnProperty("wholeSaleStatus") && entries[index].product.wholeSaleStatus == "05")
                        {
							 obj["stockStatus"] = "OS";
                             obj["stockDescription"] = outOfStockMessage;
							 obj["stockRemaining"] = stock[i].totalAvailableQuantity;
							 toastEnabled = true;
							 self.showToastMessage(outOfStockMessage);
                        }
						if(entries[index].product.hasOwnProperty("wholeSaleStatus") && entries[index].product.wholeSaleStatus == "09")
                        {
                             obj["stockStatus"] = "CS";
							 obj["stockDescription"] = comingSoonMessage;
							 obj["stockRemaining"] = stock[i].totalAvailableQuantity;
							 toastEnabled = true;
							 self.showToastMessage(comingSoonMessage);     
                        }
               	 	}
					
            	}
            }
        }
         if(toastEnabled == false) {
			 self.showToastMessage("");       	 
         }
    }
	
	self.getCartOnDeleteItem = function(){
        var promise = shoppingCartComponentService.getCartData(cartID);
        promise.then(function(data) {
            self.getImageProdUrl(data);
            self.bag = data;
            self.updateStockStatus(self.bag);
        });
    }
	
	function init(){
		self.initShoppingCart();
    }

    init();
	
	self.gotoCheckout = function(checkoutUrl){
        userProfileManager.setPageReturnURL(window.location.href);
        if(undefined != checkoutUrl && null != checkoutUrl && '' != checkoutUrl){
            window.location.href = checkoutUrl + '.html';
        }
    }
	
	this.formatDate=function(backOrderdate) {
        var pattern = /(\d{4})\-(\d{2})\-(\d{2})/;
        if (window.location.href.indexOf("US") > -1){
            var formatUS = (""+backOrderdate).replace(pattern,'$2/$3/$1')
			return(formatUS);
        }
        else{
            var formatOther = (""+backOrderdate).replace(pattern,'$3/$2/$1')
			return(formatOther);
        }
    }
	
	self.getImageProdUrl = function(data){
        
        if (data.entries != undefined) {
            var entries = data.entries;
            var productURL = "";
            var baseOpt = "";
            var prod = "";
            var prodURL = "";
            
            var skuIDs = [];
            for(var s=0; s<data.entries.length; s++){
                skuIDs[s] = data.entries[s].product.code;                  
            }
            if(skuIDs.length > 0){
				var currentPagePath = window.location.pathname;
                var imageDataResponse = shoppingCartComponentService.getImageData(skuIDs,currentPagePath);
                imageDataResponse.then(function(imageData) {
                    if(imageData != null){
                        for(var j=0;j< imageData.length;j++){
                            var obj = data.entries[j].product;
                            if(imageData[j].sku === data.entries[j].product.code){
                                obj["imagePath"] = imageData[j].path + '/jcr:content/renditions/' + shoppingCartRendition;
								obj["productTitle"] = imageData[j].title;
                            }
                        }
                    }
                });
            }
            
            for (var index = 0; index < entries.length; index++) {
                prod = entries[index].product;
                baseOpt = prod.baseOptions;
                baseOpt.push(productURL);
                if (baseOpt[0].variantType == "MECGenericVariantProduct") {
                    prodURL = prodPath+"." + prod.code + ".html";
                } else {
                    prodURL = prodPath+"." + prod.baseProduct + "." + prod.code + ".html";
                }
                baseOpt[0].productURL = prodURL;
            }
            
        }
    }
	
	self.updateBagEntries = function(value){
	    if(value > 0){
	        $cookies.put('totalItems',value,{path:'/'});
	        $rootScope.$emit("updateBagItems",value);
	    }
	    else{
	        $rootScope.$emit("updateBagItems","");
	        $cookies.put('totalItems',"",{path:'/'});
	    }
	}
	self.showToastMessage = function(erorMessage){
		var toastData = {
                     "dangerType": false,
                     "msg": erorMessage
                 };
        $rootScope.$emit("showToastMsg", toastData);
	}
	
}]);
}());

var LocaleUtil = function(){
    //---------------------------------------------------------------------------
    //
    //                           Public Methods
    //
    //---------------------------------------------------------------------------
    var publicMethods = {
        getLocaleFromPath: function(path){
            var parts = path.split("/");

            var locale = null;
            for (var i=0; i < parts.length; i++) {
                if (parts[i].length == 2 && parts[i+1].length == 2) {
                    try {
                        locale = publicMethods.toLocale(parts[i+1]+"_"+parts[i]);
                        break;
                    } 
                    catch (e) {
                    }
                }
            }

            return locale;
        },
        toLocale: function(str){
            if (str == null) {
                return null;
            }
            
            var len = str.length;
            if (len != 2 && len != 5 && len < 7) {
                throw "Invalid locale format: " + str;
            }
            
            var ch0 = str.charAt(0);
            var ch1 = str.charAt(1);
            if (ch0 < 'a' || ch0 > 'z' || ch1 < 'a' || ch1 > 'z') {
                throw "Invalid locale format: " + str;
            }
            
            if (len == 2) {
                return new Locale(str, "");
            } else {
                if (str.charAt(2) != '_') {
                    throw "Invalid locale format: " + str;
                }
                var ch3 = str.charAt(3);
                if (ch3 == '_') {
                    return new Locale(str.substring(0, 2), "", str.substring(4));
                }
                var ch4 = str.charAt(4);
                if (ch3 < 'A' || ch3 > 'Z' || ch4 < 'A' || ch4 > 'Z') {
                    throw "Invalid locale format: " + str;
                }
                if (len == 5) {
                    return new Locale(str.substring(0, 2), str.substring(3, 5));
                } else {
                    if (str.charAt(5) != '_') {
                        throw "Invalid locale format: " + str;
                    }
                    return new Locale(str.substring(0, 2), str.substring(3, 5), str.substring(6));
                }
            }
        }
    };
    
    return publicMethods;
}();

(function() {
"use strict";

pdpCategoryIngredient.service('pdpCategoryIngredientService', ['$http', '$q','serviceURL', function($http, $q,serviceURL) {
	var cartServiceBaseSite = "cosmetics";

    this.getIngredients = function(currentPath){
        var deferred = $q.defer();
        var paramsArray = [];
		paramsArray[0] = currentPath;
		var url = serviceURL.GetServiceURL('getIngredientsURL' , paramsArray);
        $http({
            method: 'GET',
            url: url,
        }).then(function successCallback(response) {
            deferred.resolve(response.data);
        },function (response){ 
           console.log("")});
        return deferred.promise;
    }

}]);
}());
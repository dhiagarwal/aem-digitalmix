(function() {
"use strict";

var url = window.location.href;
var orderID = url.substring(url.lastIndexOf('?') + 9);

orderDetail.controller('orderDetailController', ['$scope', '$http', 'orderDetailService', 'userProfileManager', function($scope, $http,
		orderDetailService, userProfileManager) {

	var self = this;
	this.checkoutData = {};
	self.hidePSV = true;
	var orderReviewRendition = angular.element('input[name=orderReviewRendition]').val();
	var printPreviewCheckBox = angular.element('input[name=printPreviewCheckBox]').val();

	var userType;
	if (userProfileManager.isUserLoggedIn()) {
		var userTypes = userProfileManager.getUserProfileMasterInfo().userType
				.toLowerCase().split("_");
		if (userTypes.length > 1) {
			userType = userTypes[userTypes.length - 1];
		} else {
			userType = userTypes[0];
		}

		if (userType == "distributor") {
			self.hidePSV = false;
		}
	}

	var order = orderDetailService.getOrderData(orderID);
	order.then(function(data) {
		self.checkoutData = data;
		 if (data.entries != undefined) {
				var entries = data.entries;
				
				 var skuIDs = [];
              for(var s=0; s<data.entries.length; s++){
                  skuIDs[s] = data.entries[s].product.code;                  
              }
              if(skuIDs.length > 0){
				  var currentPagePath = window.location.pathname; 
                  var imageDataResponse = orderDetailService.getImageData(skuIDs,currentPagePath);
                  imageDataResponse.then(function(imageData) {
                       if(imageData != null){
                          for(var j=0;j< imageData.data.length;j++){
                              var obj = data.entries[j].product;
                              if(imageData.data[j].sku === data.entries[j].product.code){
                                  obj["imagePath"] = imageData.data[j].path + '/jcr:content/renditions/' + orderReviewRendition;
								  obj["productTitle"] = imageData.data[j].title;
                              }
                          }
                      }
                  });
              }
         
			}
		self.checkoutData.deliveryAddress.phone = self.formatPhoneNumber(self.checkoutData.deliveryAddress.phone);
		self.cardType = self.checkoutData.paymentInfo.cardType.code;
        self.deliveryCost = self.checkoutData.deliveryMode.deliveryCost.value;
        if(self.deliveryCost==0){
			self.deliveryCost = "FREE";
        }
        else{
			self.deliveryCost = self.checkoutData.deliveryMode.deliveryCost.formattedValue;
        }
        self.checkoutData.created = self.formatDate(self.checkoutData.created);
        if(printPreviewCheckBox){
        	if(window.innerWidth > 767){
        		setTimeout(function(){ 
        			window.print();
        		}, 400);
        	}
        }
	});

	this.formatPhoneNumber = function(phoneNumber) {
		var format = ("" + phoneNumber).replace(/\D/g, '');
		var formattedNumber = format.match(/^(\d{3})(\d{3})(\d{4})$/);
		return (!formattedNumber) ? null : "(" + formattedNumber[1] + ") "
				+ formattedNumber[2] + "-" + formattedNumber[3];
	}

    this.formatDate = function(orderDate) {
    	 var monthNames = ["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"];
     	var formatDate = orderDate.slice(0, 10).split('-');   
 		return(monthNames[formatDate[1]-1] +' '+ formatDate[2] +', '+ formatDate[0]);
    }

}]);
}());

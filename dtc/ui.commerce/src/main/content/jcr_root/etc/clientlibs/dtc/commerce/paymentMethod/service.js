(function() {
"use strict";

paymentMethod.service('paymentMethodService', ['$http', '$q','userProfileManager','serviceURL', function($http, $q,userProfileManager,serviceURL) {

	var cartServiceBaseSite = "cosmetics";
    var user_id;
    var endPoint;
    var config = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        };
    if(userProfileManager.isUserLoggedIn()) {
        user_id = userProfileManager.getUserProfileMasterInfo().userId;
        endPoint = mulesoftHybrisEndpoint;
    } else {
        user_id = "anonymous";
        endPoint = mulesoftHybrisGuestEndpoint;
    }

    var newCartResponse = $q.defer();

    this.setPaymentDetails = function(guid, accntHolderName, cardNumber, cardType, cardExpiryMonth, cardExpiryYear,
        firstName, lastName, addressLine1, addressLine2, zipCode, city, countryIsoCode, regionIsoCode) {
        var getPaymentResponse = $q.defer();
       
        
        var paramsArray = [];
		paramsArray[0] = endPoint;
		paramsArray[1] = cartServiceBaseSite;
		paramsArray[2] = user_id;
		paramsArray[3] = guid;
		
		/*Setting Query Parameters*/
		paramsArray[4] = escape(accntHolderName);
		paramsArray[5] = cardNumber;
		paramsArray[6] = cardType;
		paramsArray[7] = cardExpiryMonth;
		paramsArray[8] = cardExpiryYear;
		paramsArray[9] = escape(firstName);
		paramsArray[10] = escape(lastName);
		paramsArray[11] = escape(addressLine1);
		paramsArray[12] = escape(addressLine2);
		paramsArray[13] = zipCode;
		paramsArray[14] = escape(city);
		paramsArray[15] = countryIsoCode;
		paramsArray[16] = regionIsoCode;
		var url = serviceURL.GetServiceURL('setPaymentDetailsURL' , paramsArray);
        
      
        
        $http.post(url, '', config).then(function(response) {
            getPaymentResponse.resolve(response.data);
        }, function(response) {
        	getPaymentResponse.resolve(response.data);
        });
        return getPaymentResponse.promise;
    }

    this.setPaymentShipping = function(guid, accntHolderName, cardNumber, cardType, cardExpiryMonth, cardExpiryYear, checkBoxValue) {
        var getPaymentResponse = $q.defer();
        accntHolderName = escape(accntHolderName);
        var paramsArray = [];
		paramsArray[0] = endPoint;
		paramsArray[1] = cartServiceBaseSite;
		paramsArray[2] = user_id;
		paramsArray[3] = guid;
		// Params
		paramsArray[4] = accntHolderName;
		paramsArray[5] = cardNumber;
		paramsArray[6] = cardType;
		paramsArray[7] = cardExpiryMonth;
		paramsArray[8] = cardExpiryYear;
		paramsArray[9] = checkBoxValue;
		var url = serviceURL.GetServiceURL('setPaymentShippingURL' , paramsArray);
  
       
        $http.post(url, '', config).then(function(response) {
            getPaymentResponse.resolve(response);
        }, function(response) {
        	getPaymentResponse.resolve(response);
        });
        return getPaymentResponse.promise;
    }

    this.getStatesData = function() {
        var deferred_states = $q.defer();
        $http.get('/etc/clientlibs/dtc/commerce/paymentMethod/states.json').then(function(response) {
            deferred_states.resolve(response.data.states);
        });
        return deferred_states.promise;
    }

}]);
}());
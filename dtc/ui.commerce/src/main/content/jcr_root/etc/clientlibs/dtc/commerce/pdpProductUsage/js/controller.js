(function() {
"use strict";

productUsageComponent.controller('pdpProductGuideController', ['$scope', '$timeout', function($scope,$timeout) {
    var self = this;
    self.videoPlaying = false;
    var videoControl = angular.element.find('#video1');
    if(videoControl[0]!=undefined)
    videoControl[0].controls = false;
    self.togglePlay = function() {
        var video = angular.element.find('#video1');
        self.videoDuration = video.duration;
        if(video[0]!=undefined){
        if (video[0].paused) {
            video[0].play();
            video[0].controls = true;
            self.videoPlaying = true;
        } else {
            $timeout(function() { video[0].pause(); }, 10)
            video[0].controls = false;
            self.videoPlaying = false;
        }
        }
    };
    
    self.video = function() {
        var videoElements = angular.element.find('#video1');
        videoElements[0].currentTime = 0;
        videoElements[0].controls = false;
        self.videoPlaying = false;
    };

    self.video = function() {
        var videoElements = angular.element.find('#video1');
        videoElements[0].currentTime = 0;
        videoElements[0].controls = false;
        self.videoPlaying = false;
    };

}]);
}());

if(typeof DMCP === 'undefined' || DMCP === null) {
	DMCP = {};
}
DMCP.commerce = DMCP.commerce || {};

/**
 * @class DMCP.commerce.DtcHybrisImporter
 * @extends CQ.Ext.Viewport
 * The importer enables the user to import product catalogs from a remote Hybris system.
 * @constructor
 * Creates a new importer.
 * @param {Object} config The config object
 */
DMCP.commerce.DtcHybrisImporter = CQ.Ext.extend(CQ.Ext.Viewport, {
    constructor : function(config) {
        this.results = document.createElement("iframe");
        this.results.id = "results_cq-hybrisimporter";
        this.results.name = "results_cq-hybrisimporter";
        this.results.height = "100%";
        this.results.width = "100%";
        this.results.onload = this.onResultsLoad;
        this.results.onreadystatechange = this.onResultsLoad;

        var importer = this;
        DMCP.commerce.DtcHybrisImporter.superclass.constructor.call(this, {
            "id" :"cq-hybrisimporter",
            "layout":"border",
            "renderTo":CQ.Util.getRoot(),
            "items" : [
                {
                    "id":"cq-hybrisimporter-wrapper",
                    "xtype":"panel",
                    "region":"center",
                    "layout":"border",
                    "border":false,
                    "items": [
                        {
                            "id":"cq-header",
                            "xtype":"container",
                            "autoEl":"div",
                            "region":"north",
                            "items": [
                                {
                                    "xtype":"panel",
                                    "border":false,
                                    "layout":"column",
                                    "cls": "cq-header-toolbar",
                                    "items": [
                                        new CQ.UserInfo({}),
                                        new CQ.HomeLink({})
                                    ]
                                }
                            ]
                        },{
                            "layout": "vbox",
                            "region": "center",
                            "items": [
                                {
                                    "xtype" :"form",
                                    "id" :"cq-hybrisimporter-form",
                                    "title":CQ.I18n.getMessage("Dtc Hybris Catalog Importer"),
//                                    "region":"center",
                                    "standardSubmit" : true,
                                    "autoScroll": true,
                                    "border":false,
                                    "margins":"5 5 5 5",
                                    "autoHeight": true,
                                    "defaults" : {
                                        "anchor" : "-54"
                                    },
                                    "style" : "background-color:white",
                                    "bodyStyle" : "padding:10px",
                                    "items" : [
                                        {
                                            "xtype" : "textfield",
                                            "fieldLabel" : CQ.I18n.getMessage("Base Store"),
                                            "fieldDescription" : CQ.I18n.getMessage("Hybris base store name"),
                                            "name" : "store",
                                            "allowBlank" : false
                                        },{
                                            "xtype" : "textfield",
                                            "fieldLabel" : CQ.I18n.getMessage("Catalog"),
                                            "fieldDescription" : CQ.I18n.getMessage("Hybris catalog name"),
                                            "name" : "catalog",
                                            "allowBlank" : false
                                        },{
                                            "xtype" : "textfield",
                                            "fieldLabel" : CQ.I18n.getMessage("Country code"),
                                            "fieldDescription" : CQ.I18n.getMessage("ISO Country code (e.g. us)"),
                                            "name" : "country",
                                            "allowBlank" : false
                                        },{
                                            "fieldLabel": CQ.I18n.getMessage("Incremental Import"),
                                            "xtype":"selection",
                                            "type":"checkbox",
                                            "name":"incrementalImport",
                                            "id":"incrementalImport"
                                        },{
                                            "xtype":"hidden",
                                            "name":"_charset_",
                                            "value":"utf-8"
                                        },{
                                            "xtype":"hidden",
                                            "name":":operation",
                                            "value":"import"
                                        },{
                                            "xtype":"hidden",
                                            "name":"fallbackProvider",
                                            "value":"hybris"
                                        }
                                    ],
                                    "buttonAlign":"left",
                                    "buttons":[
                                        {
                                            "id":"cq-hybrisimporter-btn-import",
                                            "text":CQ.I18n.getMessage("Import Catalog"),
                                            "handler":function() {
                                                var form = CQ.Ext.getCmp("cq-hybrisimporter-form").getForm();
                                                if (form.isValid()) {
                                                    var btn = CQ.Ext.getCmp("cq-hybrisimporter-btn-import");
                                                    btn.setDisabled(true);

                                                    var log = CQ.Ext.getCmp("cq-hybrisimporter-log");
                                                    log.expand();

                                                    // submit form
                                                    form.getEl().dom.action = CQ.HTTP.externalize(config.url);
                                                    form.getEl().dom.method = "POST";
                                                    form.getEl().dom.target = "results_cq-hybrisimporter";
                                                    form.submit();
                                                }
                                            }
                                        },
                                        new CQ.Ext.ProgressBar({
                                            "id":"cq-hybrisimporter-progress",
                                            "width":400,
                                            "hidden":true
                                        })
                                    ]
                                }
                            ]
                        },{
                            "xtype":"panel",
                            "id" :"cq-hybrisimporter-log",
                            "region":"south",
                            "title":CQ.I18n.getMessage("Import Log"),
                            "margins":"-5 5 5 5",
                            "height": 300,
                            "split":true,
                            "collapsible": true,
                            "collapsed": false,
                            "items":[
                                new CQ.Ext.BoxComponent({
                                    "autoEl": {
                                        "tag": "div"
                                    },
                                    "style": {
                                        "width": "100%",
                                        "height": "100%",
                                        "margin": "-2px"
                                    },
                                    "listeners":{
                                        render:function(wrapper) {
                                            new CQ.Ext.Element(importer.results).appendTo(wrapper.getEl());
                                        }
                                    }
                                })
                            ],
                            "plugins":[
                                {
                                    init: function(p) {
                                        if (p.collapsible) {
                                            var r = p.region;
                                            if ((r == "north") || (r == "south")) {
                                                p.on("collapse", function() {
                                                    var ct = p.ownerCt;
                                                    if (ct.layout[r].collapsedEl && !p.collapsedTitleEl) {
                                                        p.collapsedTitleEl = ct.layout[r].collapsedEl.createChild ({
                                                            tag:"span",
                                                            cls:"x-panel-collapsed-text",
                                                            html:p.title
                                                        });
                                                    }
                                                }, false, {single:true});
                                            }
                                        }
                                    }
                                }
                            ]
                        }
                    ]
                }
            ]
        })
    },

    onResultsLoad: function() {
        var btnCatalog = CQ.Ext.getCmp("cq-hybrisimporter-btn-import");
        btnCatalog.setDisabled(false);
    }
});
CQ.Ext.reg("dtchybrisimporter", DMCP.commerce.DtcHybrisImporter);
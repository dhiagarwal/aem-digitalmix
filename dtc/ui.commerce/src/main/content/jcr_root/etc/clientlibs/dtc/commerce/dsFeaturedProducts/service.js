(function() {
"use strict";

dsFeaturedProducts.service('dsFeaturedProductsService', ['$http', '$q','userProfileManager','serviceURL', function($http, $q,userProfileManager,serviceURL) {

	var cartServiceBaseSite = "cosmetics";
    var user_id;
    var endPoint;
    if(userProfileManager.isUserLoggedIn()) {
        user_id = userProfileManager.getUserProfileMasterInfo().userId;
        endPoint = mulesoftHybrisEndpoint;
    } else {
        user_id = "anonymous";
        endPoint = mulesoftHybrisGuestEndpoint;
    }

    this.getSubDomain = function(userId){
        var deferred = $q.defer();
        var url = firebaseDatabaseURL + "/subdomains.json "
        $http.get(url).then(function (response) {
            var subDomains = response.data;
            
            Object.keys(subDomains).forEach(function(key){
                if(subDomains[key] == userId){
                    deferred.resolve(key);
                }
            });
            
        });
        return deferred.promise;
    }
  
    this.getFireBaseData = function(userId,subDomain,locale){
        var deferred = $q.defer();
        var url = firebaseDatabaseURL + "/users/"+userId+"/storefront/"+subDomain+".json "
        $http.get(url).then(function (response) {
            
            deferred.resolve(response.data[locale.market]);
        });
        return deferred.promise;
    }


    this.getDefaultFeaturedProducts = function(market,language) {
        var deferred = $q.defer();
        var paramsArray = [];
		paramsArray[0] = market;
		paramsArray[1] = language;

		var url = serviceURL.GetServiceURL('getFeaturedProductsURL',paramsArray);
        $http({
            method: 'GET',
            url: url,
        }).then(function successCallback(response) {
            deferred.resolve(response.data);
        }, function(response) {

        });
        return deferred.promise;
    }

	this.getImageData = function(skuIds){
        var deferred = $q.defer(); 
        var paramsArray = [];
		paramsArray[0] = skuIds;
		paramsArray[1]= window.location.pathname;
		var url = serviceURL.GetServiceURL('getImageDataPathParamURL' , paramsArray);    
        $http({
            method: 'GET',
            url: url,
            headers : { 'Content-Type' : 'application/json' }
        }).then(function successCallback(response) {
            deferred.resolve(response.data);
        },function (response){ 
            
        });
        return deferred.promise;
    };

	this.getPriceData = function(id) {
        var deferred = $q.defer();        
        var paramsArray = [];
		paramsArray[0] = endPoint;
		paramsArray[1] = cartServiceBaseSite;
		paramsArray[2] = user_id;
		paramsArray[3] = id;
		var url = serviceURL.GetServiceURL('productsPriceURL' , paramsArray);
		
		$http.post(url).then(function(response) {
			deferred.resolve(response.data);
		},function (response){ 
			
		});
		return deferred.promise;
	}


    this.getRatings = function(sku) {
        var deferred = $q.defer();
        var paramsArray = [];
        paramsArray[0] = sku;

        var url = serviceURL.GetServiceURL('ratingsURL' , paramsArray);
        
        $http.jsonp(url).then(function(response) {
			deferred.resolve(response.data);
		});
        return deferred.promise;
    }

}]);
}());
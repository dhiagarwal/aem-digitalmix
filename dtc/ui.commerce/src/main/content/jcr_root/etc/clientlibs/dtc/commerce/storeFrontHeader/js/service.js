(function() {
"use strict";

storefrontHeader.service('storefrontHeaderService', ['$http', '$q','userProfileManager','serviceURL', function($http, $q,userProfileManager,serviceURL) {
    this.getDistributorDetails = function(userId){
        var deferred = $q.defer(); 
        var url = firebaseDatabaseURL + "/users/"+userId + ".json";
		
		$http({
			method: 'GET',
			url: url,
		}).then(function successCallback(response) {
			deferred.resolve(response.data);
		},function (response){ 
			console.log("");
		});

        return deferred.promise;
    }
    
    var cartServiceBaseSite = "cosmetics";
    var user_id;
    var endPoint;
    if(userProfileManager.isUserLoggedIn()) {
        user_id = userProfileManager.getUserProfileMasterInfo().userId;
        endPoint = mulesoftHybrisEndpoint;
    } else {
        user_id = "anonymous";
        endPoint = mulesoftHybrisGuestEndpoint;
    }
    var cartResponse = $q.defer();
    var addToCartResponse = $q.defer();
    var config;
    
    this.createCart = function() {
        cartResponse = $q.defer();
        var paramsArray = [];
    	paramsArray[0] = endPoint;
    	paramsArray[1] = cartServiceBaseSite;
    	paramsArray[2] = user_id;
    	
    	var url = serviceURL.GetServiceURL('createCartURL' , paramsArray);
        
        $http.post(url).then(function (response) {
			cartResponse.resolve(response);
        }, function(response) {
			cartResponse.resolve(response);
        });
        return cartResponse.promise;
    }
    
    this.addProductToCart = function(guid,data) {
        addToCartResponse = $q.defer();
        var paramsArray = [];
    	paramsArray[0] = endPoint;
    	paramsArray[1] = cartServiceBaseSite;
    	paramsArray[2] = user_id;
    	paramsArray[3] = guid;
    	var url = serviceURL.GetServiceURL('addProductToCartURL' , paramsArray);
    	
        if(userProfileManager.isUserLoggedIn()) {
            config = {
                    headers: {
                        'Content-Type': 'application/json;charset=utf-8;'
                    }
                }
        } else {
            config = {
                    headers: {
                        'Content-Type': 'application/json;charset=utf-8;'
                    }
                }
        }
        $http.post(url,data,config).then(function(response) {
            addToCartResponse.resolve(response);
        }, function(response) {
			addToCartResponse.resolve(response);
        });
        return addToCartResponse.promise;
    } 

}]);
}());
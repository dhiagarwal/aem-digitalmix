(function() {
    "use strict";

    productSearchList.controller('productSearchListController', ['$timeout', '$location', '$rootScope', 'productSearchListService', '$scope', 'userProfileManager', '$cookies', function($timeout, $location, $rootScope, productSearchListService, $scope, userProfileManager, $cookies) {

        var self = this;
        self.noData = false;
        self.productData = {};
        self.showFilter = false;
        self.isMobileMode = false;
        self.isFavorited = false;
        self.searchResult = [];
        self.viewType = 0;
        self.starNumber = 5;
        self.rating = 3;
        self.pdpBasePath = angular.element('input[name=pdpBasePath]').val();
        $scope.seperator = " - ";
        var searchListRendition = angular.element('input[name=searchListRendition]').val();
        var searchGridRendition = angular.element('input[name=searchGridRendition]').val();

        var toptenparams = angular.element($('#toptenparams')).val();
    	var datas = eval("[" + toptenparams + "]");

		self.inLoadMore = false;
		self.start = 0;
		self.queryLength = 0;

        var userType;
        if (userProfileManager.isUserLoggedIn()) {
            var userTypes = userProfileManager.getUserProfileMasterInfo().userType.toLowerCase().split("_");
            if (userTypes.length > 1) {
                userType = userTypes[userTypes.length - 1];
            } else {
                userType = userTypes[0];
            }

        }
        self.starOff = "/etc/clientlibs/dtc/core/main/css/src/assets/images/content/pdp-productbasics/star-off.svg";
        self.starOn = "/etc/clientlibs/dtc/core/main/css/src/assets/images/content/pdp-productbasics/star-on.svg";

        self.getProductSearchResult = function(searchString,start) {
			self.inLoadMore = true;
			self.showLoader = true;
            if (searchString) {
                var productDataResponse = productSearchListService.getProductData(searchString,start);
                productDataResponse.then(function(solrProducts) {
                    self.productData = solrProducts;

                     var solarResultLength =0;
                    if(self.productData){
                       solarResultLength  = self.productData.length;
                    }

					self.queryLength = solarResultLength;

					setProductSearchHistory(searchString);

                    if (solarResultLength != 0) {
                        var multipleSKUArray = [];
                        for (var i = 0; i < solarResultLength; i++) {
                            multipleSKUArray[i] = self.productData[i].getSku().toUpperCase();
                        }
                        var multipleSKU = multipleSKUArray.join(",");

                        if (multipleSKU.length > 0) {
                            var imageDataResponse = productSearchListService.getImageData(multipleSKU);
                            imageDataResponse.then(function(imageData) {

                                if (imageData != null) {

                                    var priceDataResponse = productSearchListService.getPriceData(multipleSKU);
                                    priceDataResponse.then(function(data) {
                                        self.productPrices = data.priceList;

                                        var productSearchData = [];
                                        var productData = self.productData;
                                        var priceData = self.productPrices;

                                        var locale = LocaleUtil.getLocaleFromPath(window.location.href);

                                        for (var j = 0; j < productData.length; j++) {

                                            var hidePSV = true;
                                            if (userType == "distributor") {
                                                hidePSV = false;
                                            }

                                            var productLanguageDocument = productData[j].getLanguageChild(locale.getLanguage());
                                            var product_name = productLanguageDocument.getTitle();
                                            var product_desc = productLanguageDocument.getDescription();
                                            
                                            if (product_desc && product_desc.length > 152) {
                                                product_desc = product_desc.substring(0, 152) + "...";
                                            } 

                                            var product_price = '';
                                            var product_sku = productLanguageDocument.getSku();
                                            var product_psv = '';
                                            var isFavorite = false;
                                            var product_link = '';
                                            var product_maxpsv = '';
                                            var disableAddtoCart = false;
                                            var product_Stock_Status = '';
                                            var product_variantRatings = '';
                                            var show_price = true;
                                            var imagePathList = '';
                                            var imagePathGrid = '';
                                            var psvSeperator = false;

                                            // if (productData[j].hasOwnProperty('cq:commerceType')) {
                                                // var commerceType = productData[j]['cq:commerceType'][0];
                                                //
                                                // // Product - non variant
                                                // if (commerceType == "product") {
                                                //     product_link = self.pdpBasePath + "." + productData[j].identifier[0].toUpperCase() + ".html";
                                                // } 
                                                //    
                                                // // Product - variant - link to the base product.    
                                                // else {
                                                //     if (productData[j].hasOwnProperty('path_exact')) {
                                                //         var path_exact = productData[j].path_exact;
                                                //         var pathExactArray = path_exact.split("/");
                                                //         var baseProductId = pathExactArray[pathExactArray.length - 2];
                                                //     }
                                                //     product_link = self.pdpBasePath + "." + baseProductId.toUpperCase() + "." + productData[j].identifier[0].toUpperCase() + ".html";
                                                // }
                                            // }

                                            // I was told to not return any variant products from the solar search. All that is returned is regular products and base products.
                                            product_link = self.pdpBasePath + "." + product_sku + ".html";

                                            if (undefined != imageData && imageData.length > 0 && undefined != imageData[j] && null != imageData[j] && '' != imageData[j]) {
                                                if (imageData[j].hasOwnProperty('sku') && imageData[j].sku === product_sku.toUpperCase()) {
                                                    if (imageData[j].hasOwnProperty('path')) {
                                                        imagePathList = imageData[j].path + '/jcr:content/renditions/' + searchListRendition;
                                                        imagePathGrid = imageData[j].path + '/jcr:content/renditions/' + searchGridRendition;
                                                    }
                                                }

                                            }

                                            if (undefined != priceData && null != priceData && '' != priceData) {
                                                for (var k = 0; k < priceData.length; k++) {
                                                    if (priceData[k].hasOwnProperty('code') && product_sku.toUpperCase() === priceData[k].code) {

                                                        if (!priceData[k].hasOwnProperty("priceRange")) {
                                                            if (priceData[k].hasOwnProperty("stockData") && priceData[k].stockData.hasOwnProperty("lineItemDetailsList")) {
                                                                var stock = "";
                                                                stock = priceData[k].stockData.lineItemDetailsList[0];
                                                                if (stock.hasOwnProperty("safetyStockStatus")) {

                                                                    if (stock.safetyStockStatus == "LOW_STOCK") {
                                                                        product_Stock_Status = "LS";
                                                                    } else if (stock.safetyStockStatus == "OUT_OF_STOCK") {

                                                                        product_Stock_Status = "OS";
                                                                    }
                                                                    if (stock.safetyStockStatus == "OUT_OF_STOCK" && stock.backOrdered) {

                                                                        product_Stock_Status = "BO";

                                                                    }
                                                                }
                                                            }


                                                            if (priceData[k].hasOwnProperty('wholeSaleStatus')) {
                                                                if (priceData[k].wholeSaleStatus == "02") {
                                                                    product_Stock_Status = "OS";
                                                                    show_price = false;
                                                                    disableAddtoCart = true;
                                                                } else if (priceData[k].wholeSaleStatus == "05") {
                                                                    product_Stock_Status = "OS";
                                                                    disableAddtoCart = true;
                                                                } else if (priceData[k].wholeSaleStatus == "09") {
                                                                    product_Stock_Status = "CS";
                                                                    disableAddtoCart = true;
                                                                } else if (priceData[k].wholeSaleStatus == "10") {
                                                                    disableAddtoCart = false;
                                                                    if (undefined != stock && "" != stock && stock.hasOwnProperty("safetyStockStatus")) {
                                                                        if (stock.safetyStockStatus == "OUT_OF_STOCK" && stock.backOrdered == true) {
                                                                            product_Stock_Status = "BO";
                                                                            disableAddtoCart = false;
                                                                        }
                                                                        if (stock.safetyStockStatus == "OUT_OF_STOCK" && stock.backOrdered == false) {
                                                                            product_Stock_Status = "OS";
                                                                            disableAddtoCart = true;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }

                                                        if (show_price) {
                                                            if (priceData[k].hasOwnProperty('price') && priceData[k].price.hasOwnProperty('formattedValue')) {
                                                                product_price = priceData[k].price.formattedValue;
                                                            } else {
                                                                if (priceData[k].hasOwnProperty('priceRange') && priceData[k].priceRange.hasOwnProperty('maxPrice') && priceData[k].priceRange.hasOwnProperty('minPrice')) {
                                                                    if (priceData[k].priceRange.maxPrice.hasOwnProperty('value') && priceData[k].priceRange.minPrice.hasOwnProperty('value') && priceData[k].priceRange.maxPrice.value > priceData[k].priceRange.minPrice.value) {
                                                                        product_price = priceData[k].priceRange.minPrice.formattedValue + " - " + priceData[k].priceRange.maxPrice.formattedValue;
                                                                    } else {
                                                                        product_price = priceData[k].priceRange.minPrice.formattedValue;
                                                                    }
                                                                }
                                                            }
                                                            if (priceData[k].hasOwnProperty('pvValue')) {
                                                                product_psv = priceData[k].pvValue.value;
                                                                product_maxpsv = NaN;
                                                                psvSeperator = false;
                                                            } else {
                                                                if (priceData[k].hasOwnProperty('priceRange') && priceData[k].priceRange.hasOwnProperty('maxPv') &&
                                                                    priceData[k].priceRange.hasOwnProperty('minPv')) {
                                                                    if (priceData[k].priceRange.maxPv.hasOwnProperty('value') && priceData[k].priceRange.minPv.hasOwnProperty('value') && priceData[k].priceRange.maxPv.value > priceData[k].priceRange.minPv.value) {
                                                                        product_psv = priceData[k].priceRange.minPv.value;
                                                                        product_maxpsv = priceData[k].priceRange.maxPv.value;
                                                                        if (hidePSV) {
                                                                            psvSeperator = false;
                                                                        } else {
                                                                            psvSeperator = true;
                                                                        }
                                                                    } else {
                                                                        product_psv = priceData[k].priceRange.minPv.value;
                                                                        product_maxpsv = NaN;
                                                                        psvSeperator = false;
                                                                    }
                                                                } else {
                                                                    product_psv = NaN;
                                                                    product_maxpsv = NaN;
                                                                    hidePSV = true;
                                                                }
                                                            }
                                                        } else {
                                                            product_psv = NaN;
                                                            product_maxpsv = NaN;
                                                        }
                                                    }
                                                }
                                            }
                                            if (j == 0) {
                                                isFavorite = true;
                                            }

                                            // We shouldn't have any variants in the product result set.
                                            // if (self.productData.docs[j]['cq:commerceType'] == 'variant') {
                                            //     var path = self.productData.docs[0].path_exact.split('/');
                                            //     var path_arr = path[path.length - 2];
                                            //     product_variantRatings = path_arr.toUpperCase();
                                            //
                                            //
                                            // } else {
                                                product_variantRatings = product_sku.toUpperCase();
                                            // }
                                                
                                            productSearchData.push({
                                                product_name: product_name,
                                                product_desc: product_desc,
                                                product_sku: product_sku,
                                                product_price: product_price,
                                                product_psv: product_psv,
                                                isFavorite: isFavorite,
                                                product_link: product_link,
                                                product_maxpsv: product_maxpsv,
                                                product_Stock_Status: product_Stock_Status,
                                                product_addBagStatus: disableAddtoCart,
                                                product_variantRatings: product_variantRatings,
                                                show_price: show_price,
                                                imagePathList: imagePathList,
                                                imagePathGrid: imagePathGrid,
                                                hidePSV: hidePSV,
                                                psvSeperator: psvSeperator
                                            });

                                        }
										self.inLoadMore = false;
										self.showLoader = false;
										self.searchResult = self.searchResult.concat(productSearchData);

                                        var searchData = {
                                            "totalResult": self.queryLength
                                        };
                                        $rootScope.$emit("showSearchBanner", searchData);
										self.totalProducts = self.queryLength;
                                    });
                                }
                            });
                        }

                    } else if (self.queryLength == 0) {
                        var searchData = {
                            "totalResult": self.queryLength
                        };
                        $rootScope.$emit("showSearchBanner", searchData);
						self.totalProducts = self.queryLength;
                    }

                });
            }
        };

        var searchString = getParameterByName("search");
        var param = getParameterByName("param");
		if(!param && getCookie('dtcParam')){
            param = getCookie('dtcParam');
        }
        if(param){
            for(var i = 0; i < datas.length; i++) {
                var obj = datas[i];
                if(param == obj.paramval){
					searchString = obj.searchtxt;
                    break;
                }
            }
        }
        if (!searchString) {
            self.noData = true;
        } else {
            self.noData = false;
            self.getProductSearchResult(searchString,self.start);
        }

		self.loadMore = function(){
			if(!self.inLoadMore && self.queryLength > self.start){
				self.start = self.start+10;
					if(self.queryLength > self.start){
						self.inLoadMore = true;
						self.showLoader = true;
						self.getProductSearchResult(searchString,self.start);
					}
			}
		};

        function getParameterByName(name, url) {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }

        self.toggleFavorite = function() {
            self.isFavorited = !self.isFavorited;
            self.searchResult.isFavorite = !self.searchResult.isFavorite;
        };


        // self.limit = '';

        // self.minRangeSlider = {
        //     options: {
        //         floor: 0,
        //         ceil: 1000,
        //         hideLimitLabels: true,
        //         step: 1
        //     }
        // };

        //filter range json
        // {
        //  "filter_category" : "range",
        //  "filter_type": "PSV RANGE",
        //  "minValue": "0",
        //  "maxValue": "1000",
        //  "gap_range":"50",
        //  "filter_options":[]
        // }



        this.setLimit = function(filter) {
            filter.limit = null;
            if (filter.filter_options && filter.filter_options.length > 8) {
                filter.limit = 5;
            } else {
                filter.limit = filter.filter_options.length;
            }

        };




        var settings = {
            selectors: {
                tile: '.producttile',
                list: '#list-product',
                filter: '#filter-product'
            },
            classObj: {
                filterOpen: {
                    tile: 'col-sm-6 col-md-6 col-lg-4 col-xl-3',
                    list: 'col-sm-12 col-md-8 col-lg-8 col-xl-9'
                },
                filterClose: {
                    tile: 'col-sm-6 col-md-4 col-lg-3 col-xl-2',
                    list: 'col-sm-12 col-md-12 col-lg-12 col-xl-12'
                }
            }
        };

        //callFilter function will open the filter panel and adjust the bootstrap classes accordingly

        this.callFilter = function() {
            if (screen.width < 768) {

                self.isMobileMode = !self.isMobileMode;
            }


            var tileGroup = document.querySelectorAll(settings.selectors.tile);
            if (self.showFilter == false) {

                angular.element(settings.selectors.filter).addClass('is-open');

                for (var i = 0; i < tileGroup.length; i++) {
                    tileGroup[i].className = tileGroup[i].className.replace(settings.classObj.filterClose.tile, settings.classObj.filterOpen.tile);
                }
                document.querySelector(settings.selectors.list).className = document.querySelector(settings.selectors.list).className.replace(settings.classObj.filterClose.list, settings.classObj.filterOpen.list)

            } else {
                angular.element(settings.selectors.filter).removeClass('is-open');

                for (var i = 0; i < tileGroup.length; i++) {
                    tileGroup[i].className = tileGroup[i].className.replace(settings.classObj.filterOpen.tile, settings.classObj.filterClose.tile);
                }
                document.querySelector(settings.selectors.list).className = document.querySelector(settings.selectors.list).className.replace(settings.classObj.filterOpen.list, settings.classObj.filterClose.list)

            }

            self.showFilter = !self.showFilter;

        };

        //resetFilter function will reset the filter values
        this.resetFilter = function() {
            for (var i = 0; i < self.filterData.data.length; i++) {
                var currentFilter = self.filterData.data[i];
                if (currentFilter.filter_type == "SORT BY") {
                    currentFilter.selectedFilterType = 'Price';
                    currentFilter.isFilterSelected = false;
                    currentFilter.showFilterOptions = false;
                    currentFilter.isDropdownOpen = false;
                } else {
                    for (var j = 0; j < self.filterData.data[i].filter_options.length; j++) {
                        self.filterData.data[i].filter_options[j].currentSelected = false;
                    }

                    currentFilter.selectedFilterType = '';

                    if (currentFilter.showFilterOptions) {
                        currentFilter.showFilterOptions = !currentFilter.showFilterOptions;
                        currentFilter.isDropdownOpen = !currentFilter.isDropdownOpen;
                    }

                }
            }
            for (var i = 0; i < self.filterData.toggleData.length; i++) {
                var currentFilter = self.filterData.toggleData[i];
                currentFilter.isSelected = false;
            }
        };

        this.selectOption = function(currentOption, filterName) {
            for (var i = 0; i < self.filterData.data.length; i++) {

                var options = self.filterData.data[i].filter_options;

                for (var j = 0; j < options.length; j++) {

                    if (self.filterData.data[i].filter_options[j].currentSelected && (self.filterData.data[i].filter_type == filterName)) {

                        self.filterData.data[i].filter_options[j].currentSelected = false;

                    }

                    if (options[j].option_value === currentOption) {
                        self.filterData.data[i].isFilterSelected = true;
                        self.filterData.data[i].selectedFilterType = currentOption;
                        self.filterData.data[i].filter_options[j].currentSelected = true;
                    }
                }
            }

        };

        this.change = function(state, filter, $event) {
            // Prevent bubbling to showItem.
            // On recent browsers, only $event.stopPropagation() is needed
            if ($event.stopPropagation) $event.stopPropagation();
            if ($event.preventDefault) $event.preventDefault();
            $event.cancelBubble = true;
            $event.returnValue = false;
            if (state) {
                filter.showFilterOptions = !filter.showFilterOptions;
                filter.isDropdownOpen = !filter.isDropdownOpen;
            }

            // if(filter.filter_category == 'range'){
            //   filter.selectedFilterType = filter.minValue + " PSV to " + filter.maxValue + " PSV";
            //   self.refreshSlider();
            // }
        };
        
        function setProductSearchHistory(productSearchText) {
		if (productSearchText) {
			if ($cookies.get("productSearchCookie") != undefined && $cookies.get("productSearchCookie") != "") {
				var historyCookie = [];
				historyCookie = $cookies.get(
						"productSearchCookie").split(",");
                 if(self.queryLength > 0){
				historyCookie.unshift(productSearchText);
                 }
                var finalCookiesList = removeDuplicate(historyCookie);
				if (finalCookiesList.length > 5) {
					finalCookiesList.pop();
				}
				$cookies.put("productSearchCookie",
						finalCookiesList, {path:'/'});
			} else {
				if(self.queryLength > 0){
                 $cookies.put("productSearchCookie",
						productSearchText, {path:'/'});

                }
            }
		}

	}

	 function removeDuplicate(arr){
            var i,
            cookiesArr=[],
            obj={};
            
              for (i=0;i<arr.length;i++) {
                obj[arr[i]]=0;
              }
              for (i in obj) {
                cookiesArr.push(i);
              }
            return cookiesArr;

        }


        // self.refreshSlider = function () {
        //      $timeout(function () {
        //          $scope.$broadcast('rzSliderForceRender');
        //      });
        //  };

        //hideCurrentFilter function will open and close the option panel
        this.hideCurrentFilter = function(filterName) {
            for (var i = 0; i < self.filterData.data.length; i++) {

                var currentFilter = self.filterData.data[i].filter_type;

                if (currentFilter == filterName) {
                    self.filterData.data[i].isFilterSelected = !self.filterData.data[i].isFilterSelected;
                    self.filterData.data[i].isOptionSelected = true;
                }

            }

        };

        self.setViewType = function(_type) {
            if (_type == 1) {
                self.viewType = 1;
            } else {
                self.viewType = 0;
            }
        };

        self.getNumber = function() {
            var a = new Array();
            var i = 0;
            while (i < self.starNumber) {
                a[i] = i;
                i++;
            }
            return a;
        };

        self.starFunction = function(index) {
            if (index <= self.rating) {
                return self.starOn;
            } else {
                return self.starOff;
            }
        };

        self.addProductToCart = function(productID, $event, addBagStatus) {
            $rootScope.$emit("hideToastMsg");
            if (!addBagStatus) {
                var productQty = 1;
                var productData = {
                    "orderList": [{
                        "code": productID,
                        "qty": productQty
                    }]
                };
                if (productQty > 0) {
                    $rootScope.emitShowShoppingBag = true;
                    $rootScope.$emit("showShoppingBag", productData);
                }
            } else {
                $event.stopPropagation();
            }
        };

    }]);
}());
(function() {
"use strict";

paymentMethod.controller('paymentMethodController', ['$scope', 'checkoutContainerService', 'paymentMethodService', '$state', '$rootScope','$cookies', function($scope, checkoutContainerService, paymentMethodService, $state, $rootScope,$cookies) {

    var CART_GUID = $cookies.get("cart_guid");

    window.scrollTo( 0, 0 );
    
    var self = this;
    self.voucherApplied = [];

    self.model = {};
    self.model.paymentMethod = {};

	var shoppingBagPath = angular.element('input[name=shoppingBag]').val();

    var errorDefault = angular.element('input[name=promoErrorCode]').val();

    $scope.errPromoCode;

    self.promoSectionVisible = false;
    
	self.showAddressLine1Error = false;
    
    self.billingAddressData = {};
    self.checkoutPaymentForm = '';
    
    var dateObj = new Date();
    var month = dateObj.getUTCMonth() + 1; //months from 1-12
    if (month < 10) {
        month = "0" + month;
    }
    month = month.toString();
    var year = dateObj.getUTCFullYear().toString();
    year = year.substring(2, 4);

    this.paymentMethodData = {};

    self.model.paymentMethod.creditCardTypes = {
        "visa": "Visa",
        "mastercard": "Master",
        "discover": "Discover",
        "amex": "AMEX",
        "other": "Other"
    };
    self.model.paymentMethod.data = {
        "expiry": ""
    };
    self.model.paymentMethod.handleCreditCardNumber = new Array(1);
    self.model.paymentMethod.data.expiry = month + year;
    self.model.paymentMethod.data.sameAsShippingAddress = true;
	this.saveAndContinueState = angular.element('input[id=reviewState]').val();
    this.goBackState = angular.element('input[id=deliState]').val();
    self.defaultFormat="";
    self.cvvFormat="";
    self.retainDefaultValue="";
    self.retainCVVNumber="";
    self.invalidCardError = false;
    self.invalidExpiryError = false;
    self.invalidCVVError = false;
    self.isAmexCard = false;

	self.validLength=true;
    self.isMobileDevice = (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) || (navigator.userAgent.match('Android'));
	angular.element('#credit-card-number').on('keypress keyup', function(e){
    var firstNumber = this.value[0];
    var setNumber = this.value;
	if(firstNumber==3){
		angular.element("#cvv-number").attr('placeholder','XXXX');
		self.isAmexCard = true;
		if(this.value.length==4){
			angular.element("#credit-card-number").val(setNumber+"-");
			}
       		if(this.value.length==11){
       			angular.element("#credit-card-number").val(setNumber+"-");
            }
            if(e.keyCode==8){
            	angular.element("#credit-card-number").val(setNumber);
            }
            angular.element("#credit-card-number").attr('minlength','17');
            angular.element("#credit-card-number").attr('maxlength','17');
			self.defaultFormat="XXXX XXXXXX X"+this.value[(this.value.length)-4]+this.value[(this.value.length)-3]+this.value[(this.value.length)-2]+this.value[(this.value.length)-1];
     }else if(firstNumber==4 ||firstNumber==5 || firstNumber==6){
    	 angular.element("#cvv-number").attr('placeholder','XXX');
    	 self.isAmexCard = false;
            if(this.value.length==4){
            	angular.element("#credit-card-number").val(setNumber+"-");
            }
       		if(this.value.length==9){
       			angular.element("#credit-card-number").val(setNumber+"-");
            }
            if(this.value.length==14){
            	angular.element("#credit-card-number").val(setNumber+"-");
            }
            if(e.keyCode==8){
            	angular.element("#credit-card-number").val(setNumber);
			}
            angular.element("#credit-card-number").attr('minlength','19'); 
            angular.element("#credit-card-number").attr('maxlength','19');
			self.defaultFormat="XXXX XXXX XXXX "+this.value[(this.value.length)-4]+this.value[(this.value.length)-3]+this.value[(this.value.length)-2]+this.value[(this.value.length)-1];

     }
        else{
        	angular.element("#credit-card-number").attr('minlength','16'); 
        	angular.element("#credit-card-number").attr('maxlength','16');
        }
	});

	angular.element('#cvv-number').on('keyup', function(e){
        if(self.model.paymentMethod.handleCreditCardNumber[0]==3){
        	angular.element("#cvv-number").attr('minlength','4');
        	angular.element("#cvv-number").attr('maxlength','4');
			self.cvvFormat="XXXX";
        }
        if(self.model.paymentMethod.handleCreditCardNumber[0]==4 || self.model.paymentMethod.handleCreditCardNumber[0]==5 ||self.model.paymentMethod.handleCreditCardNumber[0]==6){
        	angular.element("#cvv-number").attr('minlength','3');
        	angular.element("#cvv-number").attr('maxlength','3');
            self.cvvFormat="XXX";
		}
        else{
        	angular.element("#cvv-number").attr('maxlength','4');
		}
    });

	angular.element('#credit-card-number').bind("cut copy paste",function(e) {
     e.preventDefault();
    });

	angular.element('#cvv-number').bind("cut copy paste",function(e) {
     e.preventDefault();
    });
     
     if($rootScope.securityCode){
    	 if($rootScope.securityCode.length==3){
 			self.retainCVVNumber = "XXX";
          }
     	if($rootScope.securityCode.length==4){
 			self.retainCVVNumber = "XXXX";
          }
          self.model.paymentMethod.data.cvv = $rootScope.securityCode;
     }

    if (CART_GUID != undefined && CART_GUID.length != 0) {

                var getCartResponse = checkoutContainerService.getCart(CART_GUID);
                getCartResponse.then(function(response) {
                    checkoutContainerService.getCartData = response.data;
                    self.cartData = response.data;
                    if(self.cartData.hasOwnProperty("paymentInfo")){
						self.billingAddressData.line1 = self.cartData.paymentInfo.billingAddress.line1;
						self.billingAddressData.town = self.cartData.paymentInfo.billingAddress.town;
						self.billingAddressData.state = self.cartData.paymentInfo.billingAddress.region.isocodeShort;
						self.billingAddressData.postalCode = self.cartData.paymentInfo.billingAddress.postalCode;
                    }
                $rootScope.$emit("refreshNavigation");

                    if (self.cartData.hasOwnProperty("appliedVouchers")) {
                    for (var i = 0; i < self.cartData.appliedVouchers.length; i++) {
                        self.voucherApplied.push(self.cartData.appliedVouchers[i].voucherCode);
                    }
                }

                if (self.cartData.hasOwnProperty("appliedVouchers") && self.cartData.totalDiscounts.value != 0) {
                    self.model.savings = true;
                }

                self.deliveryCost = "$0.00";
                if (self.cartData.hasOwnProperty("deliveryCost")) {
                    self.deliveryCost = self.cartData.deliveryCost.formattedValue;
                }

                self.taxesAvailable = false;
                if (self.cartData.hasOwnProperty("deliveryAddress") && self.cartData.deliveryAddress.hasOwnProperty("postalCode")) {
                    self.taxesAvailable = true;
                }

                    if(self.cartData.hasOwnProperty("paymentInfo")){

                self.model.paymentMethod.data.expiry = self.cartData.paymentInfo.expiryMonth + self.cartData.paymentInfo.expiryYear.substring(2, 4);
                
                var retainCardNumber = self.cartData.paymentInfo.cardNumber;
				if(retainCardNumber.substring(0,1)=="3"){
						retainCardNumber = retainCardNumber.replace(/\b(\d{4})(\d{6})(\d{5})\b/, '$1-$2-$3');
						self.retainDefaultValue = "XXXX XXXXXX X"+retainCardNumber[(retainCardNumber.length)-4]+retainCardNumber[(retainCardNumber.length)-3]+retainCardNumber[(retainCardNumber.length)-2]+retainCardNumber[(retainCardNumber.length)-1];
                    }
                  else {
                    	retainCardNumber = retainCardNumber.match(new RegExp('.{1,4}', 'g')).join("-");
                    	self.retainDefaultValue = "XXXX XXXX XXXX "+retainCardNumber[(retainCardNumber.length)-4]+retainCardNumber[(retainCardNumber.length)-3]+retainCardNumber[(retainCardNumber.length)-2]+retainCardNumber[(retainCardNumber.length)-1];
                   }
                   self.model.paymentMethod.handleCreditCardNumber = retainCardNumber;
                   angular.element('#credit-card-number').focus();
                   
                if (self.cartData.paymentInfo.billingAddress.shippingAddress) {
                    self.model.paymentMethod.data.sameAsShippingAddress = true;
                } else {
                    self.model.paymentMethod.data.sameAsShippingAddress = false;
                }
            }
                });

    }

    var states = {};
    var promise_states = paymentMethodService.getStatesData();
    promise_states.then(function(response_states) {
        self.states = response_states;

    });

    self.checkCreditCardType = function(event) {
        var cardNumber = self.model.paymentMethod.handleCreditCardNumber;
        var isGetCreditCardType = cardNumber && (cardNumber.length == 17 || cardNumber.length == 19) && self.cartData.paymentInfo.accountHolderName && self.checkCardExpiry() && self.model.paymentMethod.data.cvv;
        self.model.paymentMethod.cardNumber = self.model.paymentMethod.handleCreditCardNumber;
        if (isGetCreditCardType) {
            self.model.paymentMethod.cardNumber = cardNumber;
            self.model.test = self.GetCardType(self.model.paymentMethod.cardNumber);
            switch (self.GetCardType(self.model.paymentMethod.cardNumber)) {
                case self.model.paymentMethod.creditCardTypes.visa:
                    self.model.creditCardType = self.model.paymentMethod.creditCardTypes.visa;
                    break;

                case self.model.paymentMethod.creditCardTypes.mastercard:
                    self.model.creditCardType = self.model.paymentMethod.creditCardTypes.mastercard;
                    break;

                case self.model.paymentMethod.creditCardTypes.discover:
                    self.model.creditCardType = self.model.paymentMethod.creditCardTypes.discover;
                    break;

                case self.model.paymentMethod.creditCardTypes.amex:
                    self.model.creditCardType = self.model.paymentMethod.creditCardTypes.amex;
                    break;

                default:
                    self.model.creditCardType = self.model.paymentMethod.creditCardTypes.other;
                    break;
            }
            if (self.cardInfoIsValid && self.cardValid && self.validLength) {
                self.togglePaymentSection('.payment-section-validated', '.payment-section', event);
            }
        }
    };

    self.checkCardExpiry = function() {

        if (self.model.paymentMethod.data.expiry != undefined && self.model.paymentMethod.data.expiry != '') {

            var date = new Date();
            var defaultDate = date.getDate().toString();
            var expiryDate = new Date();
            var expiry = self.model.paymentMethod.data.expiry;
            var month = expiry.substring(0, 2);
            var year = expiry.substring(2, 4);
            var defaultMonth = date.getMonth() + 1;
            if (month > 12 && month > defaultMonth) {
            	self.invalidExpiryError = true;
                return false;
            }
            year = "20" + year;
            expiryDate.setFullYear(year, month, defaultDate);
            if (expiryDate <= date) {
            	self.invalidExpiryError = true;
                return false;
            } else {
            	self.invalidExpiryError = false;
                return true;
            }

        }
        else{
			self.invalidExpiryError = true;
            return false;
		}
    }

    self.togglePaymentSection = function(source, target,event) {
        var relatedTarget = event.relatedTarget || event.explicitOriginalTarget || document.activeElement;
        if(!(relatedTarget && 
           (relatedTarget.name=='cardName' || 
            relatedTarget.id=='credit-card-number' ||
            relatedTarget.name=='cardExpiry' || 
            relatedTarget.name=='cardCVV' ||
            relatedTarget.name=='cardZip'))) 
        {
            $(source).fadeIn(1000);
            $(target).fadeOut(0);
			if(self.model.paymentMethod.handleCreditCardNumber[0]==3){
        		angular.element("#cvv-number").attr('minlength','4');
        		angular.element("#cvv-number").attr('maxlength','4');
				self.cvvFormat="XXXX";
       	 	}
        	if(self.model.paymentMethod.handleCreditCardNumber[0]>3 && self.model.paymentMethod.handleCreditCardNumber[0]<7){
        		angular.element("#cvv-number").attr('minlength','3');
        		angular.element("#cvv-number").attr('maxlength','3');
            	self.cvvFormat="XXX";
			}
        }

        if(source=='.payment-section')
            formPayment.cardName.focus();
  	}; 

    self.GetCardType = function(number) {
        // visa
        var re = new RegExp("^4");
        if (number.match(re) != null)
            return self.model.paymentMethod.creditCardTypes.visa;

        // Mastercard
        re = new RegExp("^5[1-5]");
        if (number.match(re) != null)
            return self.model.paymentMethod.creditCardTypes.mastercard;

        // AMEX
        re = new RegExp("^3[47]");
        if (number.match(re) != null)
            return "AMEX";

        // Discover
        re = new RegExp("^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)");
        if (number.match(re) != null)
            return self.model.paymentMethod.creditCardTypes.discover;

        // Diners
        re = new RegExp("^36");
        if (number.match(re) != null)
            return "Diners";

        // Diners - Carte Blanche
        re = new RegExp("^30[0-5]");
        if (number.match(re) != null)
            return "Diners - Carte Blanche";

        // JCB
        re = new RegExp("^35(2[89]|[3-8][0-9])");
        if (number.match(re) != null)
            return "JCB";

        // Visa Electron
        re = new RegExp("^(4026|417500|4508|4844|491(3|7))");
        if (number.match(re) != null)
            return "Visa Electron";

        return null;
    }

    self.isCreditCardEmpty = function(index) {
        if (index != 0 && !self.model.paymentMethod.handleCreditCardNumber[0]) {
            document.getElementById('credit-card-number-0').focus();
        }
    };


    self.saveAndContinue = function(isValid, pageName, formPayment) {
    	self.fieldCheck(formPayment);
        if (isValid && self.checkCardExpiry() && self.cardInfoIsValid) {
            var checkBoxValue = self.model.paymentMethod.data.sameAsShippingAddress;
            var checkNum = ""+self.model.paymentMethod.handleCreditCardNumber;
		    var cardNumber=checkNum.replace(/-/g , "");
            var cardType = self.model.creditCardType.toLowerCase();
            var cardExpiry = self.model.paymentMethod.data.expiry;
            var cardExpiryMonth = cardExpiry.substring(0, 2);
            var cardExpiryYear = "20" + cardExpiry.substring(2, 4);
            var cardCVV = self.model.paymentMethod.data.cvv;
            $rootScope.securityCode = cardCVV;
            if (!checkBoxValue) {
                var addressLine2 = self.cartData.paymentInfo.billingAddress.line2;
                if(self.cartData.paymentInfo.billingAddress.line2 == undefined) {
                	addressLine2 = '';
                }
                var countryIsoCode = "US";
                var regionIsoCode = self.billingAddressData.state.substring(self.billingAddressData.state.length - 2);
                if (CART_GUID != undefined && CART_GUID != null && CART_GUID != '') {
                    var paymentSaveResponse = paymentMethodService.setPaymentDetails(CART_GUID, self.cartData.paymentInfo.accountHolderName, cardNumber, cardType, cardExpiryMonth, cardExpiryYear,
                        self.cartData.paymentInfo.billingAddress.firstName, self.cartData.paymentInfo.billingAddress.lastName, self.billingAddressData.line1, addressLine2, self.billingAddressData.postalCode, self.billingAddressData.town, countryIsoCode, regionIsoCode);
                    paymentSaveResponse.then(function(data) {
                    	if(data.hasOwnProperty("errors")){
                          var checkOutErrorToastData = {"dangerType": true,"msg": angular.element("#checkOutErrorToast").val()};
                          $rootScope.$emit("showCheckoutToastMsg", checkOutErrorToastData);}
                        else{
                            var disabletoastData = {"dangerType": false,"msg": angular.element("#checkOutErrorToast").val()};
                            $rootScope.$emit("showCheckoutToastMsg", disabletoastData);
                            self.goBack(pageName);}
                    });
                }
            } else {
                if (CART_GUID != undefined && CART_GUID != null && CART_GUID != '') {
                    var paymentSaveResponse = paymentMethodService.setPaymentShipping(CART_GUID, self.cartData.paymentInfo.accountHolderName, cardNumber, cardType, cardExpiryMonth, cardExpiryYear, checkBoxValue);
                    paymentSaveResponse.then(function(response) {
                    	if(response.status !=201 || response.data == undefined || response.data == null || response.data.hasOwnProperty("errors")){
                            var toastData = {"dangerType": true,"msg": angular.element("#checkOutErrorToast").val()};
                            $rootScope.$emit("showCheckoutToastMsg", toastData);}
                         else{
                            var disabletoastData = {"dangerType": false,"msg": angular.element("#checkOutErrorToast").val()};
                            $rootScope.$emit("showCheckoutToastMsg", disabletoastData);
                            self.goBack(pageName);}
                    });
                }
            }
        }
    };

    this.goBack = function(pageName) {
        $state.go(pageName);
    };
    
    self.cardValid = true;
    self.cardInfoIsValid = false;
    this.luhnCheck = function() {
        var isValid = false;
        var checkNum = ""+self.model.paymentMethod.handleCreditCardNumber;
		var ccNum=checkNum.replace(/-/g , "");
        var validCCArray = ['2', '3', '4', '5', '6'],
            ccArrayLength = validCCArray.length;
        if (ccNum.length >= 13) {
            while (ccArrayLength--) {
                if (ccNum[0].indexOf(validCCArray[ccArrayLength]) != -1) {

                    var
                        len = ccNum.length,
                        mul = 0,
                        prodArr = [
                            [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
                            [0, 2, 4, 6, 8, 1, 3, 5, 7, 9]
                        ],
                        sum = 0;

                    while (len--) {
                        sum += prodArr[mul][parseInt(ccNum.charAt(len), 10)];
                        mul ^= 1;
                    }
                    isValid = (sum % 10 === 0 && sum > 0);
                }
            }
        } else {
            isValid = true;
        }
        self.cardInfoIsValid = isValid;
        self.cardValid = isValid;
    };


    self.validLength=true;
    this.lengthCheck = function() {
        var isValid = false;
        var checkNum = self.model.paymentMethod.handleCreditCardNumber;
        var firstNumber = self.model.paymentMethod.handleCreditCardNumber[0];
        if(firstNumber==3){
            if(checkNum.length<17){
				self.validLength=false;
                self.invalidCardError = true;
            }
            else if(checkNum.length>=17){
				self.validLength=true;
            }
        }

        if(firstNumber==4 || firstNumber==5 || firstNumber==6){
            if(checkNum.length<19){
				self.validLength=false;
                self.invalidCardError = true;
            }
             else if(checkNum.length>=19){
				self.validLength=true;
            }
        }
        else{
			if(checkNum.length<16){
				self.validLength=false;
                self.invalidCardError = true;
            }
             else if(checkNum.length>=16){
				self.validLength=true;
            }
        }

    };

    this.validCheck = function(){
         self.invalidCardError = false;
		 self.validLength=true;
    };

  this.validCVVCheck = function(){
		 self.invalidCVVError = false;
  };
  
  this.validExpCheck = function(){
         self.invalidExpiryError = false;
  };

  this.cvvLength = function(){
      var firstNumber = self.model.paymentMethod.handleCreditCardNumber[0];
      var cvvNumber = angular.element("#cvv-number").val();
      if(firstNumber==3){
			if(cvvNumber.length==4)
          {
              self.invalidCVVError = false;
			  self.cardInfoIsValid=true;
          }
          else{
			  self.cardInfoIsValid=false;
              self.invalidCVVError = true;
          }
      }
      else if(firstNumber==4 || firstNumber==5 ||firstNumber==6 ){
			if(cvvNumber.length==3)
          {	
              self.invalidCVVError = false;
			  self.cardInfoIsValid=true;
          }
          else{
			  self.cardInfoIsValid=false;
              self.invalidCVVError = true;
          }
      }
  };


    this.getExpiryDate = function(value){
    	if(value != undefined && value != ''){
    		return value.substring(0,2) + '/' + value.substring(2,4);
    	}	

    };
    
    self.fieldCheck = function(form){
		self.touchAllFields(form);
		if (form.$valid) {
			return true;
		}
        else{
			return false;
        }
    };

    self.touchAllFields= function(form) {
		_.forEach(form.$error.required, function(field){
			field.$setTouched();
		});
	};
	
	/*-----------------START OF GOOGLE AUTOCOMPLETE-------------- */
	
    var autocomplete;
    var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        postal_code: 'short_name',
        postal_code_suffix : 'short_name'
      };

    function fillInAddress() {
        self.checkoutPaymentForm.zipcode.$setPristine();
        self.checkoutPaymentForm.zipcode.$setUntouched();
        var place = autocomplete.getPlace();
        var addressLine1 = "";
        var zipCode = "";
        var hasStreetNumber = false;
		self.showAddressLine1Error = false;

        self.billingAddressData.line1 = '';
        self.billingAddressData.postalCode = '';
        self.billingAddressData.town = '';
        self.billingAddressData.state = '';
		$scope.$apply();
		if(place.hasOwnProperty("address_components")){
        for (var index = 0; index < place.address_components.length; index++) {
            var addressType = place.address_components[index].types[0];
            if("" != addressType && undefined != addressType && null != addressType){
                var addressValue = place.address_components[index][componentForm[addressType]];              
                if (addressType == "street_number") {
                	addressLine1 = addressValue;
                	hasStreetNumber = true;
                } else if(addressType == "route") {
                	addressLine1 = addressLine1 + ' ' + addressValue;
                } else if(addressType == "locality") {
                    self.billingAddressData.town = addressValue;
                } else if(addressType == "administrative_area_level_1") {
                    self.billingAddressData.state = addressValue;
                } else if(addressType == "postal_code") {
                    zipCode = addressValue;
                } else if(addressType == "postal_code_suffix" && "" != zipCode && null != zipCode && undefined != zipCode){
                    zipCode = zipCode + '-' + addressValue;
                }
            }
        }
		self.billingAddressData.postalCode = zipCode;		
		if(hasStreetNumber){
            self.billingAddressData.line1 = addressLine1.trim();      
        }
        else{
            self.showAddressLine1Error = true;
            self.billingAddressData.line1 = place.formatted_address;   
            }
		}
		else{
            self.showAddressLine1Error = true;
            self.billingAddressData.line1 = place.name;
            }	
        $scope.$apply();
    }

	angular.element('#autocomplete').keydown(function (e) {
		if (e.which == 13 && $('.pac-container:visible').length) return false;
	});
	
    $scope.geolocate = function (form) {
        self.checkoutPaymentForm = form;
        if(typeof autocomplete !== "object"){
            autocomplete = new google.maps.places.Autocomplete((document.getElementById('autocomplete')), {types: ['geocode']});
            autocomplete.addListener('place_changed', fillInAddress);
        }
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var geolocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var circle = new google.maps.Circle({
                    center: geolocation,
                    radius: position.coords.accuracy
                });
                autocomplete.setBounds(circle.getBounds());
            });
        }
    }

    /*-----------------END OF GOOGLE AUTOCOMPLETE-------------- */
	
}]);
}());

(function() {
"use strict";

deliveryOptions.service('deliveryOptionsService', ['$http', '$q','userProfileManager','serviceURL', function($http, $q,userProfileManager,serviceURL) {

	var cartServiceBaseSite = "cosmetics";
    var user_id;
    var endPoint;
	var config={
		headers : {
			'Content-Type' : 'application/json'
		}
	};
    if(userProfileManager.isUserLoggedIn()) {
        user_id = userProfileManager.getUserProfileMasterInfo().userId;
        endPoint = mulesoftHybrisEndpoint;
    } else {
        user_id = "anonymous";
        endPoint = mulesoftHybrisGuestEndpoint;
    }

    this.getDeliveryOptions = function(cartID) {
        var deferred = $q.defer();
        var paramsArray = [];
    	paramsArray[0] = endPoint;
    	paramsArray[1] = cartServiceBaseSite;
    	paramsArray[2] = user_id;
    	paramsArray[3] = cartID;
    	
    	var url = serviceURL.GetServiceURL('getDeliveryOptionsURL' , paramsArray);
    	
        $http.get(url,config).then(function(response) {
            deferred.resolve(response.data);
        });
        return deferred.promise;
    }

       this.postDeliveryOptions = function(cartID, deliveryMode) {
        var deferred = $q.defer();
        var paramsArray = [];
    	paramsArray[0] = endPoint;
    	paramsArray[1] = cartServiceBaseSite;
    	paramsArray[2] = user_id;
    	paramsArray[3] = cartID;
    	paramsArray[4] = deliveryMode;
    	
    	var url = serviceURL.GetServiceURL('postDeliveryOptionsURL' , paramsArray);

        $http.put(url,'',config).then(function(response) {
            deferred.resolve(response);
        }, function(response) {
               deferred.resolve(response);
        });
        return deferred.promise;
    }

 }]);
}());

(function() {
"use strict";


authenticatedSuggestionsModule.service('authenticatedSuggestionsService', ['$http', '$q','userProfileManager','serviceURL', function($http, $q,userProfileManager,serviceURL) {

	this.$http = $http;
	var deferred = $q.defer();
	var staticComponents = $q.defer();
	var cardComponents = $q.defer();
	$http.get('/etc/clientlibs/dtc/signup/welcome-onboard/js/component.json').then(function(response) {
		staticComponents.resolve(response.data);
	});

	$http.get('/etc/clientlibs/dtc/signup/welcome-onboard/js/card.json').then(function(response) {
		cardComponents.resolve(response.data);
	});

	this.getCardComponent = function() {
		return cardComponents.promise;
	}

	this.fetchCards = function(slideId) {
		var deferred = $q.defer();
		var now = new Date();
		if(userProfileManager.isUserLoggedIn()){
			var userId = userProfileManager.getUserProfileMasterInfo().userId;
		}
		var paramsArray = [];
		paramsArray[0] = userId;
		paramsArray[1] = now.getTime();


		var url = serviceURL.GetServiceURL('fetchCardsURL' , paramsArray);

		var config = {
				headers : {
					'content-type': 'application/json'
				}
		}
		$http.get(url,config).success(function(response) {
			deferred.resolve(response);
		}).error(function(data){			
			deferred.reject(data);
		});
		return deferred.promise;
	}
	
	this.getMbsStory = function() {
		var userId;
	    var mbsdeferred = $q.defer();
	    
		if(userProfileManager.isUserLoggedIn()){

			
			
	       var name=userProfileManager.getUserProfileDetailInfo().profileHeader.firstName;
	        userId = userProfileManager.getUserProfileMasterInfo().userId;
            var paramsArray = [];
			paramsArray[0] = mulesoftStorefrontEndpoint;
			paramsArray[1] = userId;
	        var getUrl = serviceURL.GetServiceURL('mbsGetStoryURL' , paramsArray);
			$http.get(getUrl).then(function (response) {
				mbsdeferred.resolve(response);
	        }, function(response) {
	        	mbsdeferred.resolve(response);
	        });
	    } 
	
	    return mbsdeferred.promise;
	 }

}]);
}());

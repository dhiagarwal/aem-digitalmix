/**
 * This object represents a product document stored in Solr. It is designed to provide ease of access to all of the
 * document properties.
 *
 * @param solrProduct - This is the product document in its raw form as it is returned from an http call.
 *
 * @constructor
 */
var SolrProductAdaptor = function(solrProduct){
    var languageAndMarketDocs = solrProduct._childDocuments_;

    // Object to represent the language document for a Solr product document.
    var SolrProductLanguageAdaptor = function(productLanguage){
        this.getIndex = function() {
            return productLanguage.path_exact;
        };

        this.getSku = function() {
            return productLanguage.sku && productLanguage.sku.length > 0 ? productLanguage.sku[0] : undefined;
        };

        this.getMarkets = function() {
            return productLanguage.markets;
        };

        this.getTitle = function() {
            return productLanguage.title && productLanguage.title.length > 0 ? productLanguage.title[0] : undefined;
        };

        this.getDescription = function() {
            return productLanguage.description && productLanguage.description.length > 0 ? productLanguage.description[0] : undefined;
        };

        this.getIngredients = function() {
            return productLanguage.ingredients;
        };

        this.getCategories = function() {
            return productLanguage.categories;
        };

        this.getSearchTerms = function() {
            return productLanguage.searchTerms;
        };
    };

    // Object to represent the market document for a Solr product document.
    var SolrProductMarketAdaptor = function(productMarket){
        this.getIndex = function() {
            return productMarket.path_exact;
        };

        this.getSku = function() {
            return productMarket.sku && productMarket.sku.length > 0 ? productMarket.sku[0] : undefined;
        };

        this.getPurchasable = function() {
            return productMarket.purchasable && productMarket.purchasable.length > 0 ? productMarket.purchasable[0] : undefined;
        };

        this.getImageUrl = function() {
            return productMarket.url;
        };
    };


    this.getIndex = function() {
        return solrProduct.path_exact;
    };

    this.getSku = function() {
        return solrProduct.sku && solrProduct.sku.length > 0 ? solrProduct.sku[0] : undefined;
    };

    this.getMarkets = function() {
        return solrProduct.markets;
    };

    this.getLanguages = function() {
        return solrProduct.languages;
    };

    this.getRoot = function() {
        return solrProduct._root_ && solrProduct._root_.length > 0 ? solrProduct._root_[0] : undefined;
    };

    this.getVersion = function() {
        return solrProduct._version_;
    };

    /**
     *
     * @param lang - Example: "en"
     * @returns the language document in the product for the given language.
     */
    this.getLanguageChild = function(lang){
        var languageChild = null;

        if (languageAndMarketDocs) {
            for (var i = 0; i < languageAndMarketDocs.length; i++) {
                if (languageAndMarketDocs[i].path_exact === "product/" + this.getSku() + '/' + lang.toLowerCase()) {
                    languageChild = new SolrProductLanguageAdaptor(languageAndMarketDocs[i]);
                    break;
                }
            }
        }

        return languageChild;
    };

    /**
     *
     * @param market - Example: "US"
     * @returns the market document in the product for the given market.
     */
    this.getMarketChild = function(market){
        var marketChild = null;

        if (languageAndMarketDocs) {
            for (var i = 0; i < languageAndMarketDocs.length; i++) {
                if (languageAndMarketDocs[i].path_exact === "product/" + this.getSku() + '/' + market.toUpperCase()) {
                    marketChild = new SolrProductMarketAdaptor(languageAndMarketDocs[i]);
                    break;
                }
            }
        }

        return marketChild;
    };
};

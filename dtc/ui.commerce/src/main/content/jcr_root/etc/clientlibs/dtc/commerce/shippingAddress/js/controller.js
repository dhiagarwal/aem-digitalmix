(function() {
"use strict";

shippingAddress.controller('shippingAddressController', ['$scope', 'checkoutContainerService', 'shippingAddressService', '$state', '$rootScope','$cookies', function($scope, checkoutContainerService, shippingAddressService, $state, $rootScope,$cookies) {

	var cartID = $cookies.get("cart_guid");

	window.scrollTo( 0, 0 );
	
    var self = this;
	var shippingAddressModel = {};
	this.saveAndContinuePath = angular.element('input[id=deliState]').val();

    self.voucherApplied = [];

    self.model = {};

	var shoppingBagPath = angular.element('input[name=shoppingBag]').val();

    var errorDefault = angular.element('input[name=promoErrorCode]').val();

    $scope.errPromoCode;

    self.promoSectionVisible = true;
	
    self.showAddressLine1Error = false;   
    self.shippingAddressData = {};
    self.shippingAddressForm = '';
    
    if (cartID != undefined && cartID.length != 0) {

            var getCartResponse = checkoutContainerService.getCart(cartID);
            getCartResponse.then(function(response) {
                  checkoutContainerService.getCartData = response.data;

                self.cartData = response.data;
                $rootScope.$emit("refreshNavigation");

                if(self.cartData.hasOwnProperty("deliveryAddress")){
			self.shippingAddressModel = checkoutContainerService.getCartData.deliveryAddress;
			
			self.shippingAddressData.line1 = self.shippingAddressModel.line1;
			self.shippingAddressData.town = self.shippingAddressModel.town;
			self.shippingAddressData.state = self.shippingAddressModel.region.isocodeShort;
			self.shippingAddressData.postalCode = self.shippingAddressModel.postalCode;
        }

            if (self.cartData.hasOwnProperty("appliedVouchers")) {
                    for (var i = 0; i < self.cartData.appliedVouchers.length; i++) {
                        self.voucherApplied.push(self.cartData.appliedVouchers[i].voucherCode);
                    }
                }

                if (self.cartData.hasOwnProperty("appliedVouchers") && self.cartData.totalDiscounts.value != 0) {
                    self.model.savings = true;
                }

                self.deliveryCost = "$0.00";
                if (self.cartData.hasOwnProperty("deliveryCost")) {
                    self.deliveryCost = self.cartData.deliveryCost.formattedValue;
                }

                self.taxesAvailable = false;
                if (self.cartData.hasOwnProperty("deliveryAddress") && self.cartData.deliveryAddress.hasOwnProperty("postalCode")) {
                    self.taxesAvailable = true;
                }
            });

    } else {
		self.shippingAddressModel = 0;
    }

    var states = {};
    var promise_states = shippingAddressService.getStatesData();
	promise_states.then(function(response_states){
		self.states = response_states;
    });

	this.saveAndContinue = function(isValid, pageName) {
		if (isValid) {
            if (cartID != undefined && cartID.length != 0) {

                var addressLine2 = self.shippingAddressModel.line2;
                if(self.shippingAddressModel.line2 == undefined) {
					addressLine2 = '';
                }

				var promise_saveAddress = shippingAddressService
						.saveAddress(cartID,
                                     self.shippingAddressModel.firstName,
                                     self.shippingAddressModel.lastName, 
                                     "mr", 
                                     self.shippingAddressData.line1, 
                                     addressLine2,
                                     self.shippingAddressData.town, 
                                     self.shippingAddressData.postalCode, 
                                     self.shippingAddressModel.phone, 
                                     self.shippingAddressModel.email, 
                                     "US", 
                                     self.shippingAddressData.state);
				promise_saveAddress.then(function(response) {
				   
					if(response.status != 201 || response.data == undefined || response.data == null){
						var toastData = {"dangerType": true,"msg": angular.element("#checkOutErrorToast").val()};
                        $rootScope.$emit("showCheckoutToastMsg", toastData);
					}
					else{
					if(response.data.hasOwnProperty("errors")){
						if(response.data.errors[0].type == "MECZoneLookupError"){
							var toastData = {"dangerType": true,"msg": angular.element("#invalidStateZipErrorToast").val()};
	                        $rootScope.$emit("showCheckoutToastMsg", toastData);
						}
						else {
							var toastData = {"dangerType": true,"msg": angular.element("#checkOutErrorToast").val()};
	                        $rootScope.$emit("showCheckoutToastMsg", toastData);
						}
                    }
					else{
						var disabletoastData = {"dangerType": false,"msg": angular.element("#checkOutErrorToast").val()};
	                      $rootScope.$emit("showCheckoutToastMsg", disabletoastData);
			              self.goToPage(pageName);}
					}
					   
					
                }, function(reason) {
                  console.log(reason);
                }); 
            }
		}
	};

    this.goToPage = function(pageName) {
        $state.go(pageName);
	};
	
	/*-----------------START OF GOOGLE AUTOCOMPLETE-------------- */
	
    var autocomplete;
    var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        postal_code: 'short_name',
        postal_code_suffix : 'short_name'
      };

    function fillInAddress() {
        self.shippingAddressForm.zipcode.$setPristine();
        self.shippingAddressForm.zipcode.$setUntouched();
        var place = autocomplete.getPlace();
        var addressLine1 = "";
        var zipCode = "";
        var hasStreetNumber = false;
        self.showAddressLine1Error = false;
        
        self.shippingAddressData.line1 = '';
        self.shippingAddressData.town = '';
        self.shippingAddressData.state = '';
        self.shippingAddressData.postalCode = '';
		$scope.$apply();
		if(place.hasOwnProperty("address_components")){
        for (var index = 0; index < place.address_components.length; index++) {
            var addressType = place.address_components[index].types[0];
            if("" != addressType && undefined != addressType && null != addressType){
                var addressValue = place.address_components[index][componentForm[addressType]];              
                if (addressType == "street_number") {
                	addressLine1 = addressValue;
                	hasStreetNumber = true;
                } else if(addressType == "route") {
                	addressLine1 = addressLine1 + ' ' + addressValue;
                } else if(addressType == "locality") {
                    self.shippingAddressData.town = addressValue;
                } else if(addressType == "administrative_area_level_1") {
                    self.shippingAddressData.state = addressValue;
                } else if(addressType == "postal_code") {
                    zipCode = addressValue;
                } else if(addressType == "postal_code_suffix" && "" != zipCode && null != zipCode && undefined != zipCode){
                    zipCode = zipCode + '-' + addressValue;
                }
            }
        }
		self.shippingAddressData.postalCode = zipCode;
		if(hasStreetNumber){
            self.shippingAddressData.line1 = addressLine1.trim();
        }
        else{
            self.showAddressLine1Error = true;
            self.shippingAddressData.line1 = place.formatted_address;
            }
		}
		else{
                self.showAddressLine1Error = true;
                self.shippingAddressData.line1 = place.name;
            }
        $scope.$apply();
    }
	
	angular.element('#autocomplete').keydown(function (e) {
		if (e.which == 13 && $('.pac-container:visible').length) return false;
	});

    $scope.geolocate = function (form) {
        self.shippingAddressForm = form;
        if(typeof autocomplete !== "object"){
            autocomplete = new google.maps.places.Autocomplete((document.getElementById('autocomplete')), {types: ['geocode']});
            autocomplete.addListener('place_changed', fillInAddress);
        }
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var geolocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var circle = new google.maps.Circle({
                    center: geolocation,
                    radius: position.coords.accuracy
                });
                autocomplete.setBounds(circle.getBounds());
            });
        }
    }

    /*-----------------END OF GOOGLE AUTOCOMPLETE-------------- */
	
}]);
}());

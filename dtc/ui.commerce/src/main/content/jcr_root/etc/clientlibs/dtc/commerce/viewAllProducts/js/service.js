(function() {
"use strict";

viewAllProducts.service('viewAllProductsService', ['$http', '$q','userProfileManager','serviceURL', 'solrQueryService',function($http, $q,userProfileManager,serviceURL, solrQueryService) {

	 
    var deferred = $q.defer();

    this.SearchTextCntr = 0;
    this.setSearchTextCntr = function(count){
		this.SearchTextCntr = count;
    };
	this.getSearchTextCntr = function(){
		return this.SearchTextCntr;
    };

    this.getProductData = function(searchText) {
    	var MAX_ROW_TO_RETURN = 1000;
        var locale = LocaleUtil.getLocaleFromPath(window.location.href);
        return solrQueryService.getProductsAndBasesByCategory(searchText, locale.getLanguage(), locale.getCountry(), MAX_ROW_TO_RETURN);
    };

    this.getsubCategories = function(searchPath) {
    	var paramsArray = [];
    	paramsArray[0] = searchPath+".infinity.json"; 
    	var url = serviceURL.GetServiceURL('subCategoriesUrl' , paramsArray);         

        var deferred = $q.defer();
        $http({
            method: 'GET',
            url: url
        }).success(function(data) {
            deferred.resolve(data);

         }).error(function() {
            console.log('---');
        });
        return deferred.promise;
    }
    
}]);
}());
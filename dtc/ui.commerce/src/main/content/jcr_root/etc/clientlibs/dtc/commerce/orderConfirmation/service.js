
(function() {
"use strict";

orderConfirmation.service('orderConfirmationComponentService', ['$http', '$q','userProfileManager','serviceURL', function($http, $q,userProfileManager,serviceURL) {


	this.$http = $http;

	var cartServiceBaseSite = "cosmetics";
    var user_id;
    var endPoint;
    if(userProfileManager.isUserLoggedIn()) {
        user_id = userProfileManager.getUserProfileMasterInfo().userId;
        endPoint = mulesoftHybrisEndpoint;
    } else {
        user_id = "anonymous";
        endPoint = mulesoftHybrisGuestEndpoint;
    }
	
	var config={
			headers : {
                'Content-Type' : 'application/json'
            }
        };
    
  this.getFormdata = function(orderID) {
	  var deferred = $q.defer();
		var paramsArray = [];
		paramsArray[0] = endPoint;
		paramsArray[1] = cartServiceBaseSite;
		paramsArray[2] = user_id;
		paramsArray[3] = orderID;
		
		var url = serviceURL.GetServiceURL('getOrderDetailsURL' , paramsArray);
		$http.get(url,config).then(function(response) {
	    	deferred.resolve(response.data);
	  	});

    return deferred.promise;
  }
  
  this.getSubDomain = function(userId){
	  var deferred = $q.defer();
	  var url = firebaseDatabaseURL + "/subdomains.json ";
      $http.get(url, config).then(function (response) {
    	  var subDomains = response.data;
    	  
    	  Object.keys(subDomains).forEach(function(key){
    		  if(subDomains[key] == userId){
    			  deferred.resolve(key);
    		  }
    	  });
    	  
      });
	  return deferred.promise;
  }
  
  this.getDistInfo = function(userId,subDomain,locale){
	  var deferred = $q.defer();
	  var url = firebaseDatabaseURL + "/users/"+userId + ".json";
      $http.get(url, config).then(function (response) {
    	  var distInfo = {};
    	  
    	  distInfo.displayName = response.data.displayName;
    	  distInfo.photoURL = response.data.photoURL;
    	  distInfo.congratsMessage = response.data.storefront[subDomain][locale.market].lang[locale.language].congratsMessage;
    	  
    	  deferred.resolve(distInfo);
    	  
      });
	  return deferred.promise;
  }
  
}]);
}());
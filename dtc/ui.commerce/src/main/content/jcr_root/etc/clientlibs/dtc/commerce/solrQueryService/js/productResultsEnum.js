// Solr language node properties fields to search on;
var ProductResultsEnum = {
    ALL_PRODUCTS                    : 1,
    PRODUCTS_AND_BASE_PRODUCTS      : 2,
    PRODUCTS_AND_VARIANT_PRODUCTS   : 3
};

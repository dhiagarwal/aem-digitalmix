(function() {
"use strict";



orderConfirmation.controller('orderConfirmationController', ['$scope', '$http', 'orderConfirmationComponentService', 'userProfileManager','$window', '$cookies', 'serviceURL',  function($scope, $http, orderConfirmationComponentService, userProfileManager,$window,$cookies,serviceURL) {

    var self = this;
	self.guest=true;
	
	var orderID = getParameterByName('orderId');

    if(userProfileManager.isUserLoggedIn()) {
       self.guest=false;
    }
    this.confirmationData = [];
	if (orderID != undefined && orderID != null && orderID != '') {
		var promise = orderConfirmationComponentService.getFormdata(orderID);
        promise.then(function(response) {
            self.confirmationData = response;
		});
    }
	
	self.isStoreFront = $cookies.get("isStoreFront");
	if (self.isStoreFront != undefined && self.isStoreFront != "" && self.isStoreFront != "false") {
  		self.isStoreFront = true;
  	}else{
  		self.isStoreFront = false;
  	}
	self.locale = userProfileManager.getPageLocaleVal();
	
	self.userId = getParameterByName('storeId');
	if(self.userId){
		 var promise = orderConfirmationComponentService.getSubDomain(self.userId);
		 promise.then(function(subDomain) {
			 
			 var distPromise = orderConfirmationComponentService.getDistInfo(self.userId,subDomain,self.locale);
				distPromise.then(function(distInfo) {
					self.distProfileImage = distInfo.photoURL;
					self.distName = distInfo.displayName;
					self.congratsMessage = distInfo.congratsMessage;
				});
				
			});
	}
        function getParameterByName(name) {
            var url = window.location.href;
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }
	
	        
	self.viewOrPrint = function(pageName) {
		window.location.href = pageName+'?orderId='+orderID;
	};
	self.createAccount = function() {
	};
	
	$window.onunload = function () {
	};
}]);
}());

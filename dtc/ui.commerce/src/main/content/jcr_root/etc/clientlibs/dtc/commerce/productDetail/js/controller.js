(function() {
"use strict";

productDetail.controller('productDetailController', ['$location', '$rootScope', 'productDetailService', '$scope','$interval', 'userProfileManager','$cookies','$uibModal', function($location,$rootScope, productDetailService, $scope,$interval,userProfileManager,$cookies,$uibModal) {
	
    var self = this;    
    self.priceData ={};
    self.sku = angular.element('input[name=product-sku]').val();
    var baseSku = self.sku;
    self.baseV = self.sku;
	var variantProductSku = self.sku;
	
	self.backBtnClick = function() { 
        window.history.back();
 	 };
	 
    self.history = function() { 
        if(document.referrer){
            return true;
        }
        return false;
    };
  
    if(angular.element('input[name=variantSelectors]').val() != ''){
    	variantProductSku = angular.element('input[name=variantSelectors]').val();
    	self.sku = variantProductSku;
    	self.baseV = baseSku +"." + variantProductSku;
    }

    self.productName= angular.element('input[name=productName]').val();
    self.productSize = angular.element('input[name=productSize]').val();
	self.purchasable = angular.element('input[name=purchasable]').val();
	self.showShade = angular.element('input[name=showShade]').val();

    if(variantProductSku != undefined && variantProductSku != null && variantProductSku != ''){
		$rootScope.skuCrossSell= variantProductSku;
    	$rootScope.$emit("updateCrossSell",$rootScope.skuCrossSell);
    	$rootScope.$emit("updateUpSell",$rootScope.skuCrossSell);
    }
    else if(self.purchasable == 'true'){
		$rootScope.skuCrossSell= self.sku;
    	$rootScope.$emit("updateCrossSell",$rootScope.skuCrossSell);
    	$rootScope.$emit("updateUpSell",$rootScope.skuCrossSell);
    }

    self.queue = [];

    self.displaySize = false;
    self.showItemNo = false;
	self.hideVariant = false;
	self.hidePSV = true;
	self.hideRecurrOrder = true;
	self.hideStockStatus = true;
    self.disableAddtoCart = false;

    self.showError = false;
	self.selectedVariant = "";
	
    if(self.productSize!=''){
    	self.displaySize = true;
    }
	var userDetailType;
    if(userProfileManager.isUserLoggedIn()) {
        var userTypes = userProfileManager.getUserProfileMasterInfo().userType.toLowerCase().split("_");
        if(userTypes.length > 1) {
            userDetailType = userTypes[userTypes.length-1];
        } else {
            userDetailType = userTypes[0];
        }

        if(userDetailType == "distributor"){
    		self.hidePSV = false;
        }
    } 
    var isFavorite= false;
    self.psvValue = "";
    self.quantity = 1;
   	self.productQuantityMax =   self.stockRemaining;
    self.productQuantityMin=0;
    self.changeQuantity = function(temp) {
            self.productQuantityMax = self.stockRemaining+1;
        if (self.quantity >= self.productQuantityMin && self.quantity <= self.productQuantityMax) {
			if(self.stockRemaining <= 0){
				return;
			}
            if (temp === "+") {
                self.quantity++;
            }
            if (temp === "-") {
                self.quantity--;
            }
            if (self.quantity <= self.productQuantityMin) {
                self.quantity++;
            }
            if (self.quantity >= self.productQuantityMax) {
                self.quantity--;
            }
        }
    }
    self.checkFavorite = function() {
        if (isFavorite) {
            return "active";
        }
    };
    self.addFavorite = function(){
        if(isFavorite) { isFavorite = false;}
        else{isFavorite=true;}
    };
    self.starOff = "/etc/clientlibs/dtc/core/main/css/src/assets/images/content/pdp-productbasics/star-off.svg";
    self.starOn = "/etc/clientlibs/dtc/core/main/css/src/assets/images/content/pdp-productbasics/star-on.svg";
    self.starNumber = 5;
    self.rating = 0;
    self.ratingHolder = self.rating;
    self.ratingFlag = true;
    self.getNumber = function() {
        var a = new Array();
        var i = 0;
        while (i < self.starNumber) {
            a[i] = i;
            i++;
        }
        return a;
    }
    self.starFunction = function(index) {
        if (index <= self.rating) {
            return self.starOn;
        } else {
            return self.starOff;
        }
    };
    self.rateProduct = function(productRating) {
        self.ratingFlag = true;
        if (self.ratingFlag) {
            self.rating = productRating;
            self.ratingHolder = productRating;
            
        }
        
    };
    self.starMouseOver = function(index) {
        if (self.ratingFlag) {
            self.rating = index;
        }
    };
    self.starMouseLeave = function(index) {
        if (self.ratingFlag) {
            self.rating = self.ratingHolder;
        }
    };

    self.getPrice = function(sku){
        var promise = productDetailService.getPriceData(sku);
        promise.then(function(data) {
            self.priceData = data.priceList;
			self.price = '';
            self.minPvv = '';
            self.maxPvv = '';
            $scope.seperator = " - ";
			self.ShowPrice = true;
            var entries = data.priceList;
            if(self.purchasable == "true"){
			if(self.priceData[0].hasOwnProperty("stockData") && self.priceData[0].stockData.hasOwnProperty("lineItemDetailsList")){
                var stockDataStatus = self.priceData[0].stockData.lineItemDetailsList;
                self.stockRemaining = parseInt(stockDataStatus[0].totalAvailableQuantity);
                if(stockDataStatus[0].hasOwnProperty("safetyStockStatus")){

                    if(stockDataStatus[0].safetyStockStatus == "LOW_STOCK"){
                        self.stockStatus = "LS";
						self.totalStockAvailable = stockDataStatus[0].totalAvailableQuantity;
                    } else if(stockDataStatus[0].safetyStockStatus == "OUT_OF_STOCK"){
                        self.stockStatus = "OS";
                        self.disableAddtoCart = true;
                    } else if(stockDataStatus[0].safetyStockStatus == "NORMAL"){
                        self.stockStatus = "";
                    }

                    if(stockDataStatus[0].safetyStockStatus == "OUT_OF_STOCK" && stockDataStatus[0].hasOwnProperty("backOrdered")){
                        if(stockDataStatus[0].backOrdered){
                            self.stockStatus = "BO";
                            var dateString = stockDataStatus[0].backOrderedDate;
                            self.stockUpdate = self.formatDate(dateString);
							self.disableAddtoCart = false;
                            self.stockRemaining = 50000;
                        }
                    } else {
                        self.stockUpdate = "";
                    } 
                }
            }
			
            if(self.priceData[0].hasOwnProperty("wholeSaleStatus")){
                if(self.priceData[0].wholeSaleStatus == "02"){
                    self.stockStatus = "OS";
                    self.disableAddtoCart = true;
                    self.ShowPrice = false;
					self.stockRemaining = 0;

                }
                else if(self.priceData[0].wholeSaleStatus == "05"){
					self.stockStatus = "OS";
					self.disableAddtoCart = true;
                    self.ShowPrice = true;
					self.stockRemaining = 0;

                }
                else if(self.priceData[0].wholeSaleStatus == "09"){
                    self.stockStatus = "CS";
                    self.disableAddtoCart = true;
                    self.ShowPrice = true;
					self.stockRemaining = 0;

                }
                else if(self.priceData[0].wholeSaleStatus == "10"){
					if(self.priceData[0].wholeSaleStatus == "10" && stockDataStatus[0].safetyStockStatus == "OUT_OF_STOCK"){
						if(stockDataStatus[0].hasOwnProperty("backOrdered")){
							if(stockDataStatus[0].backOrdered){
								self.ShowPrice = true;
                                self.stockStatus = "BO";
								var dateString = stockDataStatus[0].backOrderedDate;
								self.stockUpdate = self.formatDate(dateString);
								self.disableAddtoCart = false;
								self.stockRemaining = 50000;
                            }
							else{
								self.ShowPrice = true;
								self.stockStatus = "OS";
							}
						}
						else{
							self.ShowPrice = true;
							self.stockStatus = "OS";
							}
                    }
                }
            }
			}
			
			if(self.priceData){
				if(self.ShowPrice){

				if(self.priceData[0].hasOwnProperty("price")){
					self.price = self.priceData[0].price.formattedValue;
                if(self.priceData[0].hasOwnProperty("pvValue")){
                	self.minPvv = self.priceData[0].pvValue.value;
            		self.maxPvv = NaN;
               		$scope.showSeperator = false;
                }
                else{
					self.minPvv = NaN;
            		self.maxPvv = NaN;
               		$scope.showSeperator = false;
                }
            }
			else{
				if(self.priceData[0].hasOwnProperty("priceRange")){
                 if((self.priceData[0].priceRange.maxPrice.value > self.priceData[0].priceRange.minPrice.value) &&
                   (self.priceData[0].priceRange.maxPv.value == self.priceData[0].priceRange.minPv.value) ){
                    self.price = self.priceData[0].priceRange.minPrice.formattedValue + " - " + self.priceData[0].priceRange.maxPrice.formattedValue;
                    self.minPvv = self.priceData[0].priceRange.minPv.value;
                    self.maxPvv = NaN;
                    if(self.hidePSV){
                    	$scope.showSeperator = false;
                    }
                    else{
						$scope.showSeperator = false;
                    }
                }
                 if((self.priceData[0].priceRange.maxPv.value > self.priceData[0].priceRange.minPv.value)
                  &&(self.priceData[0].priceRange.maxPrice.value == self.priceData[0].priceRange.minPrice.value)){
                     self.price = self.priceData[0].priceRange.minPrice.formattedValue ;
                     self.minPvv = self.priceData[0].priceRange.minPv.value;
                     self.maxPvv = self.priceData[0].priceRange.maxPvv.value;
                     if(self.hidePSV){
                    	$scope.showSeperator = false;
                     }
                     else{
						$scope.showSeperator = true;
                     }

                }
                if((self.priceData[0].priceRange.maxPv.value > self.priceData[0].priceRange.minPv.value)
                   &&(self.priceData[0].priceRange.maxPrice.value > self.priceData[0].priceRange.minPrice.value)){
                    self.price = self.priceData[0].priceRange.minPrice.formattedValue + " - " + self.priceData[0].priceRange.maxPrice.formattedValue;
                    self.minPvv = self.priceData[0].priceRange.minPv.value;
                    self.maxPvv = self.priceData[0].priceRange.maxPv.value;
                    if(self.hidePSV){
                    	$scope.showSeperator = false;
                    }
                    else{
						$scope.showSeperator = true;
                    }
                }
                if((self.priceData[0].priceRange.maxPv.value == self.priceData[0].priceRange.minPv.value)
                   &&(self.priceData[0].priceRange.maxPrice.value == self.priceData[0].priceRange.minPrice.value)){
                    self.price = self.priceData[0].priceRange.minPrice.formattedValue;
                    self.minPvv = self.priceData[0].priceRange.minPv.value;
                    self.maxPvv = NaN;
                    if(self.hidePSV){
                    	$scope.showSeperator = false;
                    }
                    else{
						$scope.showSeperator = false;
                    }
                }
			}
                else{
					self.price = '';
                    self.minPvv = NaN;
                    self.maxPvv = NaN;
                    self.hidePSV = true;
                    $scope.showSeperator = false;
                }

            }
		  }
				else{
					self.price = '';
                    self.minPvv = NaN;
                    self.maxPvv = NaN;
                    self.hidePSV = true;
                    $scope.showSeperator = false;
                }
			}
        });
    }
    self.getPrice(self.sku);

    self.slickPrimaryConfig = {
        enabled: true,
        infinite: true,
        adaptiveHeight: true,
        mobileFirst: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: true,
        speed: 500,
        responsive: [
            {
                breakpoint: 640,
                settings: {
                    arrows: true
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    arrows: true,
                    draggable: false
                }
            }
        ]
    };
    
    function init(){
    	if(typeof(angular.element('#productImages').val()) != undefined && angular.element('#productImages').val()!= null && angular.element('#productImages').val()!= ""){
	        var imagepath = JSON.parse(angular.element('#productImages').val()); 
	        if(imagepath.length > 0){
	            self.images = imagepath;            
	            self.imageName = imagepath[0].imagePath.split("/")[6];
	        }
    	}
    }
    init();
    
    self.addProductToCart = function(productID) {
        var getSelectedValue = "";
        var errorStatus = false;
        if(self.showShade == "true"){
        	if(!angular.element(".color-picker").hasClass("selected")){
                errorStatus = true;
              }        
        } else{
        	getSelectedValue = angular.element("#productDropDown :selected").val();
            if(getSelectedValue == "" && !self.hideVariant){
              errorStatus = true;
            }
        }
        if(errorStatus){
        	self.showError = true;
        } else{
        	var productQty = angular.element('input[name=product-qty]').val();
	        var productData = {
	            "orderList" : [
	                {"code":productID , "qty":productQty}
	            ]
	        };
	        if(productQty > 0){
	        	$rootScope.emitShowShoppingBag = true;
	            $rootScope.$emit("showShoppingBag", productData);
	        }
        }
    };

    self.loadProduct = function(variantSku,variantImagePath,variantName,jsonIng,size,variantType,event){
    	if ($cookies.get('cq-authoring-mode')) {
            var url= window.location.href;
    		var lastPartArr = url.substring(url.lastIndexOf('products')).split('.');
    	    lastPartArr[2] = variantSku;
    	    if(lastPartArr.length==3)
    	        lastPartArr.push("html");
    	    var urlLastPart = lastPartArr.join('.');
			window.location.href = url.substring(0, url.lastIndexOf('/')+1).concat(urlLastPart);
		}
		self.selectedVariant = variantType;
		self.hideStockStatus = false;
        self.disableAddtoCart = false;
    	$rootScope.varSelected = true;
		var colorPicker = angular.element('.color-picker');
        colorPicker.removeClass('selected');
        angular.element(event.target).addClass('selected');
        angular.element("#slickProduct").slick('unslick');
        var slideCount = angular.element("#slickProduct").slick("getSlick").slideCount;
        for (var i = slideCount; i > 0; i--) {
        	angular.element('#slickProduct').slick('slickRemove', i - 1);
        }

        angular.element('#slickProduct').slick('slickRemove', angular.element("#slickProduct").slick("getSlick").slideCount - 1);
		if (typeof(variantImagePath) != undefined && variantImagePath != null && variantImagePath != "") {
            self.images = JSON.parse(variantImagePath);
            self.imageName = self.images[0].imagePath.split("/")[6];
        }
        self.slickPrimaryConfig.enabled=false;
        $interval(function() {
            self.slickPrimaryConfig.adaptiveHeight = true;
            self.slickPrimaryConfig.enabled = true;
        }, 200, [2]);
		self.productSize = size;
		self.purchasable = "true";
        self.getPrice(variantSku);
        self.sku = variantSku;
        self.productName = variantName;
		self.showItemNo = true;
		self.baseV = baseSku +"." + variantSku;
        self.showError = false;
        $rootScope.$emit("hideToastMsg");
		self.addProductToQ( );
		$rootScope.skuCrossSell= self.sku;
        $rootScope.$emit("updateCrossSell",$rootScope.skuCrossSell);
		$rootScope.$emit("updateUpSell",$rootScope.skuCrossSell);
		$rootScope.$emit("updateUpSellKit",$rootScope.skuCrossSell);
		

        self.jsonV1 = JSON.parse(jsonIng);
        if($rootScope.loadSupplementTable!=undefined){
            $rootScope.loadSupplementTable(self.jsonV1);
        }
    };

    self.loadProductOnChange = function(){
        var variantSku = angular.element("#productDropDown").val();
        if(variantSku != ""){
			if ($cookies.get('cq-authoring-mode')) {
                var url= window.location.href;
        		var lastPartArr = url.substring(url.lastIndexOf('products')).split('.');
        	    lastPartArr[2] = variantSku;
        	    if(lastPartArr.length==3)
        	        lastPartArr.push("html");
        	    var urlLastPart = lastPartArr.join('.');
				window.location.href = url.substring(0, url.lastIndexOf('/')+1).concat(urlLastPart);
			}
            self.hideStockStatus = false;
            self.disableAddtoCart = false;
            $rootScope.varSelected = true;
			self.purchasable = "true";
            self.getPrice(variantSku);
            self.baseV = baseSku +"." + variantSku;
            self.sku = variantSku;
			self.showItemNo = true;
            self.productName = angular.element('#productDropDown option:selected').data('variantname');
			self.productSize  = angular.element('#productDropDown option:selected').data('size');
            var variantType = angular.element('#productDropDown option:selected').data('mecvarianttype');
            if(variantType == "Size"){
            	self.productSize = angular.element('#productDropDown option:selected').data('mecvariantvalue');
            }
            angular.element("#slickProduct").slick('unslick');
            var slideCount = angular.element("#slickProduct").slick("getSlick").slideCount;
            for (var i = slideCount; i > 0; i--) {
            	angular.element('#slickProduct').slick('slickRemove', i - 1);
            }
            angular.element('#slickProduct').slick('slickRemove', angular.element("#slickProduct").slick("getSlick").slideCount - 1);
            var variantImagePath = angular.element('#productDropDown option:selected').data('variantimagepath');
            if(typeof(variantImagePath) != undefined && variantImagePath != null  && variantImagePath != ""){
                self.images = variantImagePath;
                self.imageName = variantImagePath[0].imagePath.split("/")[6];
            }
            setTimeout(function() {
            	angular.element('#slickProduct').slick('reinit');
            	angular.element("#slickProduct").not('.slick-initialized').slick();
            }, 100);
            self.jsonV = angular.element('#productDropDown option:selected').data('variantjsoning');
            if($rootScope.loadSupplementTable!=undefined){
            	$rootScope.loadSupplementTable(self.jsonV);
                }
            self.showError = false;
            $rootScope.$emit("hideToastMsg");
        	self.addProductToQ( );
			$rootScope.skuCrossSell= self.sku;
			$rootScope.$emit("updateCrossSell",$rootScope.skuCrossSell);
			$rootScope.$emit("updateUpSell",$rootScope.skuCrossSell);
			$rootScope.$emit("updateUpSellKit",$rootScope.skuCrossSell);
        }
    };

    self.preSelectVariant = function(){
        var variantSelectors = angular.element('input[name=variantSelectors]').val();
        if(variantSelectors != ""){
        	angular.element('#productDropDown').val(variantSelectors);
            self.variantSkuFromUrl = variantSelectors;
            self.showItemNo = true;
            self.hideStockStatus = false;
            self.productSize  = angular.element('#productDropDown option:selected').data('size');
            var variantType = angular.element('#productDropDown option:selected').data('mecvarianttype');
            if(variantType == "Size"){
           	  self.productSize = angular.element('#productDropDown option:selected').data('mecvariantvalue');
            }
        } else {
            if(self.purchasable == "true"){
                self.hideVariant = true;
                self.showItemNo = true;
                self.hideStockStatus = false;
            }
        }
    }
    
    angular.element(document).ready(function () {
		self.preSelectVariant();
		self.addProductToQ( );
    });

    self.addProductToQ = function( ){
  	  self.duplicateProduct = false;
        var variantP = false;
		if( self.baseV.indexOf('.') > -1)
		{
			variantP = true;
		}
        var temp = {
          "skuID": self.baseV,
          "productTitle": self.productName,
          "varP":variantP,
          "varID":self.sku,
          "imageName" : self.imageName
        }


		var queue =[] ;
		if( $cookies.get('qData') !=undefined &&  $cookies.get('qData') !=""){
		queue = JSON.parse( $cookies.get('qData')).productData;
            if(queue.length !=undefined ){
                for(var i=0;i<queue.length;i++){
					if(queue[i].varID==temp.varID){
                        self.duplicateProduct = true;
                     	break;
                    }
                }
            }
        }

        if(!self.duplicateProduct)
        {
            queue.push(temp);
            if(queue.length > 5){
				var i = queue.shift();
              }
               $cookies.put('qData', JSON.stringify({productData:queue}) , {path:'/'});
        	}
      }

    self.checkStockStatus = function(value){
        var stockValue = self.stockStatus;
        if(stockValue == value){
            return true;
        }else{
            return false;
        }
    };

    $rootScope.$on("disableAddtoCart", function(event){
            self.disableAddtoCart = true;
    });

    this.formatDate=function(backOrderdate) {
        var pattern = /(\d{4})\-(\d{2})\-(\d{2})/;
        if (window.location.href.indexOf("US") > -1){
            var formatUS = (""+backOrderdate).replace(pattern,'$2/$3/$1')
			return(formatUS);
        }
        else{
            var formatOther = (""+backOrderdate).replace(pattern,'$3/$2/$1')
			return(formatOther);
        }
    }
	
	
	/**
        Star Rating variables
        */
        self.submitButtonDisable = true;
        self.$uibModal = $uibModal;
        self.isOpen = false;
        self.section = {
            sec1 : true,
            sec2 : true,
            sec3 : false,
            sec4 : false,
            sec5 : false,
            sec6 : false
        };
        
        var sku = angular.element('input[name=product-sku]').val();
        self.rated = 'false';
        
        self.ratingValue = 0; // total number of ratings
        self.ratingStars = 0;
        self.recommendedCount = 0; // number of recommendations
        self.ratingPercentage = "0%";
        self.ratingsDetails = {};
        self.rating = 0; // prefill stars with avg rating
        self.maxStar = 5;
        self.hasRating = false;
        self.max = this.max || 5;
        self.starType = "big";
        self.userReviewed = 'false';
        var url = window.location.href;
        var ratingModal = getParameterByName('ratingModal',url);
        
        /**
        Star Rating related functions
        */
        
        var uid = "";
        if(userProfileManager.isUserLoggedIn()) {
            if(userProfileManager.getUserProfileMasterInfo()!=null){
                uid = userProfileManager.getUserProfileMasterInfo().gigyaId;
            }
            hasRated();
        }
        var promise = productDetailService.getRatings(baseSku, new Date().getTime());
        promise.then(function(data) {
            if (data.hasOwnProperty('streamInfo') && data.streamInfo.commentCount > 0) {
                
                self.ratingValue = data.streamInfo.commentCount || 300;
                self.ratingStars = data.streamInfo.commentCount || 0;
                self.recommendedCount = data.comments[0].tags[0]
                .substring(11) || 0;
                self.ratingPercentage = Math
                .round((self.recommendedCount * 100)
                       / self.ratingValue)
                + "%";
                self.rating = Math
                .round(data.streamInfo.avgRatings._overall);// Math.round((self.recommendedCount*5)/self.ratingValue);
            }
            
            self.updateStars();
        });
        
        function hasRated() {
            var hasRatedPromise = productDetailService.hasRated(baseSku, uid);
            hasRatedPromise.then(function(data) {
                if (data != undefined && data.commentCount > 0) {
                    self.hasRating = true;
                    angular.element("#setPopOverRatingVal").val(data.comments[0].ratings._overall);
                    if(ratingModal == 'true'){
                        self.ratedPanel();
                    }else{
                        self.toggle('false');
                    }
                    
                    
                    self.userReviewed = 'true';
                }
            });
        }
        
        var anchorElement = angular.element(document.querySelector('.mouse-cursor'));
        anchorElement.on('click', function(evt) {
            if (self.hasRating) {
                self.ratedPanel();
            }
        });
        
        this.updateStars = function() {
            self.starClass = "star-" + self.starType;
            self.starRating = self.getStarRatingPosition(self.rating, self.starType);
        };
        
        this.getStarRatingPosition = function(ratingDecimalValue, starType) {
            
            var starTopPosition = 0;
            var starMargin = 0;
            var starWidth = 0;
            var halfStarTopMargin = 0,
                smallStarFirstPosition = 0;
            var halfStarPosition = -273;
            if (starType == "big") {
                starWidth = 18;
                halfStarPosition = -273;
                halfStarTopMargin = -26;
                
                
            } else {
                starWidth = 12;
                halfStarPosition = -242;
                halfStarTopMargin = -64;
                smallStarFirstPosition = -44;
                
            }
            var decimalValue = ratingDecimalValue.toString().replace(/^[^\.]+/, '0');
            var floorRatingValue = Math.floor(ratingDecimalValue);
            decimalValue = parseFloat(decimalValue);
            var mutipleValue = 0;
            if (0.26 <= decimalValue && decimalValue <= 0.74) {
                starMargin = halfStarTopMargin;
                floorRatingValue++;
            } else if (0.75 <= decimalValue && decimalValue <= 0.99) {
                starMargin = smallStarFirstPosition;
                floorRatingValue++;
            } else {
                starMargin = smallStarFirstPosition;
            }
            if (floorRatingValue >= this.max) {
                starMargin = smallStarFirstPosition;
                floorRatingValue = this.max;
            }
            floorRatingValue = floorRatingValue * starWidth;
            floorRatingValue = halfStarPosition + floorRatingValue
            return floorRatingValue + "px " + starMargin + "px";
        }
        
        
        
        this.recommendedAtLeastOnce = false;
        this.productRecommended = false;
        this.productReceived;
        var ele = angular.element(document
                                  .querySelector('.pdp-popover'));
        ele
        .on(
            'click',
            function(evt) {
                angular.element(".Yes-btn").click(function(e) {
                    self.productRecommended = true;
                });
                angular.element(".Sample-btn").click(function(e) {
                    self.productReceived = "Sample";
                });
                angular.element(".Purchase-btn").click(function(e) {
                    self.productReceived = "Purchase";
                });
                if (self.productRecommended) {
                    if(!self.recommendedAtLeastOnce){
                        self.recommendedCount = parseInt(self.recommendedCount) + 1;
                    }
                    self.recommendedAtLeastOnce = true;
                    self.productRecommended = false; // setting it to false again on the controller level.
                }
                evt.stopPropagation();
            });
        angular.element(".Yes-btn").click(function(e) {
            self.productRecommended = true;
            if (self.productRecommended) {
                if(!self.recommendedAtLeastOnce){
                    self.recommendedCount = parseInt(self.recommendedCount) + 1;
                }
                self.recommendedAtLeastOnce = true;
                self.productRecommended = false; // setting it to false again on the controller level.
            }
            e.stopPropagation();
        });
        
        angular.element(".Sample-btn").click(function(e) {
            self.productReceived = "Sample";
            e.stopPropagation();
        });
        
        angular.element(".Purchase-btn").click(function(e) {
            self.productReceived = "Purchase";
            e.stopPropagation();
        });

        this.close = function() {
        	var promise = productDetailService.setRatings(uid, baseSku, self.ratingPopover, self.productReceived, self.recommendedCount);
        	promise.then(function(data) {
        		if(data.status == 200 && data.data.statusReason == "OK") {
        			var getRatingPromise = productDetailService.getRatings(baseSku, new Date().getTime());
        			getRatingPromise.then(function(dataGet) {
        				if (dataGet.hasOwnProperty('streamInfo') && dataGet.streamInfo.commentCount > 0) {
                            self.ratingValue = dataGet.streamInfo.commentCount || 300;
                            self.recommendedCount = dataGet.comments[0].tags[0]
                            .substring(11) || 0;
                            self.ratingPercentage = Math
                            .round((self.recommendedCount * 100)
                                   / self.ratingValue)
                            + "%";
                            self.rating = Math
                            .round(dataGet.streamInfo.avgRatings._overall);
                            self.hasRating = true;
                        }
                        self.updateStars(self);
                        self.userReviewed = 'true';
        			});
        		}
        	});
        }
        
        this.closeModal = function() {
            this.isOpen = false;
            this.getOriginalState();
        }
        
        this.closeModal = function() {
            this.isOpen = false;
            this.getOriginalState();
        }
        
        this.starOffPopover = "/etc/clientlibs/dtc/core/main/css/src/assets/images/css/original-star.png";
        this.starOnPopover = "/etc/clientlibs/dtc/core/main/css/src/assets/images/css/hover-star.png";
        this.starNumberPopover = 5;
        this.ratingPopover = 0;
        this.ratingHolderPopover = this.ratingPopover;
        this.ratingFlagPopover = true;
        
        this.getNumber = function() {
            var a = new Array();
            var i = 0;
            while (i < this.starNumberPopover) {
                a[i] = i + 1;
                i++;
            }
            return a;
        }
        this.starFunction = function(index) {
            if (index <= this.ratingPopover) {
                return this.starOnPopover;
            } else {
                return this.starOffPopover;
            }
        };
        this.rateProduct = function(productRating) {
            var getRatingData = {};
            this.ratingFlagPopover = true;
            getRatingData = self.setRatingData;
            if (this.ratingFlagPopover) {
                var getRating = "rating-" + productRating;
                var getRatingVal = getRatingData[getRating];
                this.ratingPopover = productRating;
                angular.element("#setPopOverRatingVal").val(productRating);
                this.ratingHolderPopover = productRating;
                this.ratingVal = getRatingVal;
            }
            this.submitButtonDisable = false;
        };
        this.starMouseOver = function(index) {
            if (this.ratingFlagPopover) {
                this.ratingPopover = index;
            }
        };
        this.starMouseLeave = function(index) {
            if (this.ratingFlagPopover) {
                this.ratingPopover = this.ratingHolderPopover;
            }
        };
        
        var ratingData = '{"rating-1":"POOR","rating-2":"FAIR","rating-3":"AVERAGE","rating-4":"GOOD","rating-5":"EXCELLENT"}';
        self.setRatingData = JSON.parse(ratingData);
        
        this.toggle = function(open) {
            if (!open) {
                this.getOriginalState();
            };
        };
        
        this.getOriginalState = function() {
            angular.forEach(this.section, function(val, key) {
                if (key == 'sec1' || key == 'sec2') {
                    this.section[key] = true;
                } else {
                    this.section[key] = false;
                }
            }, this);
        }
        
        this.getModalState = function() {
            self.isOpen = true;
            self.toggle(self.isOpen);
            angular.forEach(this.section, function (val, key) {
                if(key == 'sec3'){this.section[key] = true;}
                else{this.section[key] = false;}
            }, this);
        };
        
        this.ratedPanel = function() {
            self.isOpen = true;
            self.rated = true;
            self.toggle(self.isOpen);
            angular.forEach(this.section, function (val, key) {
                if(key == 'sec6'){this.section[key] = true;
                                  var productRating = angular.element("#setPopOverRatingVal").val();
                                  this.ratingPopover = productRating ;}
                else{this.section[key] = false;}
            }, this);
        };
        
        
        this.openSignUpModal = function() {
            if(userProfileManager.isUserLoggedIn()) {
                self.getOriginalState();
            }
            else{
                var newPath = window.location.href;
                var redirectURL = addParameterToURL(newPath, "ratingModal", "true");
                var signInPath = angular.element("#nvgsigninpath").val();
                userProfileManager.setPageReturnURL(redirectURL);
                window.location = window.location.protocol+'//'+window.location.host+ signInPath + '.html';
            }
            
        };
        
        this.onRateProduct = function(current, next) {
            var self = this;
            if (uid.length == 0) {
                this.openSignUpModal().result.then(function(data) {
                    self.isOpen = true;
                    self.changeSection(current, next);
                });
            } else {
                self.changeSection(current, next);
            }
        };
        
        this.changeSection = function(current, next) {
            this.section[current] = false;
            this.section[next] = true;
        };
        
        
        $(document).ready(function(){
            if(ratingModal == 'true'){
                if(self.userReviewed == 'true'){
                    self.ratedPanel();
                }else{
                    self.getModalState();
                }
            }
            else{
                self.toggle('false');
            }
        });
        
        
        function addParameterToURL(url, param, val){
            var param_url = url;
            param_url += (param_url.split('?')[1] ? '&':'?') + param + "=" + val;
            return param_url;
        }

        function getParameterByName(name, url) {
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }

}]);
}());
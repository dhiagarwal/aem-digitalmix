(function() {
"use strict";

productTestimonial.service('productTestimonialService',['$http', '$q','serviceURL', function($http,$q,serviceURL) {

	this.getTestimonialIconData = function(sku, testimonialPath) {
    	var deferred = $q.defer();
    	var paramsArray = [];
    	paramsArray[0] = testimonialPath;
    	paramsArray[1] = sku;
    	var url = serviceURL.GetServiceURL('getTestimonialIconData' , paramsArray);   
  		$http.get(url).then(function(response) {
    		deferred.resolve(response.data);
  		});
    	return deferred.promise;
  	}
  	
}]);
}());
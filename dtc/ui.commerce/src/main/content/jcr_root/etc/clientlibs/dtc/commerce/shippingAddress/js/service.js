(function() {
"use strict";

shippingAddress.service('shippingAddressService', ['$http', '$q','userProfileManager','serviceURL', function($http, $q,userProfileManager,serviceURL) {

	var cartServiceBaseSite = "cosmetics";
    var user_id;
    var endPoint;
    var config = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        };
    if(userProfileManager.isUserLoggedIn()) {
        user_id = userProfileManager.getUserProfileMasterInfo().userId;
        endPoint = mulesoftHybrisEndpoint;
    } else {
        user_id = "anonymous";
        endPoint = mulesoftHybrisGuestEndpoint;
    }

     this.getStatesData = function() {
        var deferred_states = $q.defer();
        $http.get('/etc/clientlibs/dtc/commerce/shippingAddress/js/states.json').then(function(response) {
    		deferred_states.resolve(response.data.states);
  		});
        return deferred_states.promise;
    }

     this.saveAddress = function(cartID, firstName, lastName, titleCode
                                 	, addressLine1, addressLine2, town, postalCode
                                 	, phone, email, countryIsoCode, regionIsoCode) {
		var deferred_saveAddress = $q.defer();
	 
        var paramsArray = [];
		paramsArray[0] = endPoint;	
		paramsArray[1] = cartServiceBaseSite;
		paramsArray[2] = user_id;
		paramsArray[3] = cartID;
		// Query Params
		paramsArray[4] = escape(firstName);
		paramsArray[5] = escape(lastName);
		paramsArray[6] = titleCode;
		paramsArray[7] = escape(addressLine1);
		paramsArray[8] = escape(addressLine2);
		paramsArray[9] = escape(town);
		paramsArray[10] = postalCode;
		paramsArray[11] = phone;
		paramsArray[12] = escape(email);
		paramsArray[13] = countryIsoCode;
		paramsArray[14] = regionIsoCode;
		
		var url = serviceURL.GetServiceURL('saveAddress' , paramsArray);
		
        

         $http.post(url,'',config).then(function(response) {
             deferred_saveAddress.resolve(response);
         }, function(response) {
         	deferred_saveAddress.resolve(response);
         });
         return deferred_saveAddress.promise;
     }

 }]);
}());
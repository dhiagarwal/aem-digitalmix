(function() {
    "use strict";

    welcomeMessageDS.service('welcomeMessageService', ['$http', '$q', 'userProfileManager', 'serviceURL', function($http, $q, userProfileManager, serviceURL) {

        this.getSubDomain = function(userId) {
            var deferred = $q.defer();
            var url = firebaseDatabaseURL + "/subdomains.json "
            $http.get(url).then(function(response) {
                var subDomains = response.data;

                Object.keys(subDomains).forEach(function(key) {
                    if (subDomains[key] == userId) {
                        deferred.resolve(key);
                    }
                });

            });
            return deferred.promise;
        }
        
        this.getDistInfo = function(userId,subDomain,locale){
      	  var deferred = $q.defer();
      	  var url = firebaseDatabaseURL + "/users/"+userId + ".json";
            $http.get(url).then(function (response) {
          	  var distInfo = {};          	  
          	  distInfo.displayName = response.data.displayName;
          	  distInfo.photoURL = response.data.photoURL;
          	  distInfo.welcomeText = response.data.storefront[subDomain][locale.market].lang[locale.language].welcomeMessage;
          	  
          	  deferred.resolve(distInfo);
          	  
            });
      	  return deferred.promise;
        }
        
    }]);
}());
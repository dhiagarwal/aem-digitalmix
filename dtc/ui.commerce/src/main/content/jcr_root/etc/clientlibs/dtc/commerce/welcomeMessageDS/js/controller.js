(function() {
    "use strict";

    welcomeMessageDS.controller('welcomeMessageController', ['$rootScope', 'welcomeMessageService', '$scope', 'userProfileManager', function($rootScope, welcomeMessageService, $scope, userProfileManager) {

        var self = this;
        self.locale = userProfileManager.getPageLocaleVal();
        self.welcomeText;
        self.distributorName;
        self.distributorImage;
        self.userId = getParameterByName("storeId");
        var subDomain;
		if(self.userId){
			var promise = welcomeMessageService.getSubDomain(self.userId);
			promise.then(function(data) {
				subDomain = data;
				var distPromise = welcomeMessageService.getDistInfo(self.userId,subDomain,self.locale);
				distPromise.then(function(distInfo) {
					self.distributorImage = distInfo.photoURL;
					self.distributorName = distInfo.displayName;
					self.welcomeText = distInfo.welcomeText;
				});
			});
		}
        function getParameterByName(name, url) {
            if (!url) {
                url = window.location.href;
            }
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }

    }]);
}());
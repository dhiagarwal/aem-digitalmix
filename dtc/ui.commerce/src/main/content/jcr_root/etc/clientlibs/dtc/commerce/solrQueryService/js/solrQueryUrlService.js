var SolrQueryUrlService = function(){
    // Solr product keys - !!! Warning, don't change the string values. They map directly to the pmd productDetail node. !!!
    var PATH_EXACT = "path_exact";
    var SKU = "sku";
    var MARKETS = "markets";
    var LANGUAGES = "languages";
    var TITLE = "title";
    var DESCRIPTION = "description";
    var PRODUCTS = "products";
    var KEY_INGREDIENTS = "keyIng";
    var CATEGORIES = "categories";
    var SEARCH_TERMS = "searchTerms";
    var PURCHASABLE = "purchasable";
    var IMAGE_URL = "url";
    var PV = "pv";
    var RETAIL_PRICE = "rtl";
    var BASE_SKU = "baseSku";
    var ROOT = "_root_";
    var CHILD_DOCS = "_childDocuments_";
    var VERSION = "_version_";

    // Solr query parameters
    var PRODUCT_INDEX_PREFIX = "product/";
    var OR = "%20OR%20";
    var SKU_WILDCARD = "??????";
    var BASE_PRODUCT_SKU_WILDCARD = "????????????????";
    var PRODUCT_INDEX_QUERY_PARAM = PATH_EXACT + ":" + PRODUCT_INDEX_PREFIX;
    var CHILD_FILTER = "%20childFilter=";
    var PARENT_FILTER = "child%20parentFilter=";
    var DEFAULT_START_VALUE = 0;
    var DEFAULT_ROW_VALUE = 10;
    var MAX_ROW_VALUE = 100;
    var RETURN_JSON = "&wt=json";


    // Solr url path stuff
    var SOLR_SELECT_URI = "oak/select?fl="; //"/solr/oak/select?fl=";
    var JCR_CONTENT = "jcr:content";
    var PMD_PATH_PREFIX = "/content/pmd/";
    var PIM_PATH_PREFIX = "/etc/commerce/products/dtc/";
    var PMD_PRODUCT_CONTAINER_SUFFIX = "/jcr:content/productContainer";
    var PMD_PRODUCT_DETAIL_SUFFIX = "/jcr:content/productContainer/productDetail";
    var SOLR_UPDATE_SUFFIX_URI  = "/solr/oak/update/json?wt=json&commit=true";

    // Misc
    var PRODUCT_PREFIX_AND_SKU_LENGTH = PRODUCT_INDEX_PREFIX.length + SKU_WILDCARD.length;
    var BASE_PRODUCT_PREFIX_AND_SKU_LENGTH = PRODUCT_INDEX_PREFIX.length + BASE_PRODUCT_SKU_WILDCARD.length;

    //---------------------------------------------------------------------------
    //
    //                           Public Methods
    //
    //---------------------------------------------------------------------------
    var publicMethods = {
        getProductRootsForSearchTerms: function (productSearchResult, solrPrefixUriFromConfig, searchTerm, productSearchFields, language, market, rows, start){
            var resultFilterQueryStr = PRODUCT_INDEX_QUERY_PARAM + SKU_WILDCARD + "/" + language.toLowerCase();
            
            // See if we need to include base products
            if (productSearchResult === ProductResultsEnum.ALL_PRODUCTS || productSearchResult === ProductResultsEnum.PRODUCTS_AND_BASE_PRODUCTS){
                resultFilterQueryStr += OR + PRODUCT_INDEX_QUERY_PARAM + BASE_PRODUCT_SKU_WILDCARD + "/" + language.toLowerCase()
            }
            
            return solrPrefixUriFromConfig +
                SOLR_SELECT_URI + ROOT +
                "&fq=" + 
                MARKETS + ":" + market.toUpperCase() +
                getSearchTermQuery(searchTerm, productSearchFields) +
                getResultFilter(resultFilterQueryStr, rows, start);
        },
        getProductsByRoots: function (productSearchResult, solrPrefixUriFromConfig, matchingRootProducts, language, market, rows, start) {
            var solrUrl = "";
            
            if (matchingRootProducts.length > 0) {
                // Get the products for the matching root nodes
                var rootNodesQuery = "";
                var rootPath;
                for (var i = 0; i < matchingRootProducts.length; i++) {
                    rootPath = matchingRootProducts[i][ROOT];
                    
                    if (rootPath[0].length === PRODUCT_PREFIX_AND_SKU_LENGTH) {
                        if (rootNodesQuery.length > 0){
                            rootNodesQuery += OR;
                        }
                        rootNodesQuery += PATH_EXACT + ":" + rootPath;
                    }
                }

                var filterQueryStr = "&fq=";
                
                // See if we want to exclude variant products.
                if (productSearchResult === productSearchResult === ProductResultsEnum.PRODUCTS_AND_BASE_PRODUCTS){
                    filterQueryStr += "-baseSku:[*%20TO%20*]%20&&%20";
                }

                // Return the complete records for the given matches
                // Example solrUrl: http://dev-gateway-mulesoft-21:8080/mec/solr/oak/select?fl=*,[child%20parentFilter=path_exact:product/????????%20childFilter=path_exact:product/????????/en],[child%20parentFilter=path_exact:product/????????%20childFilter=path_exact:product/????????/US]&q=path_exact:product/01003611&rows=5&wt=json
                solrUrl =
                    solrPrefixUriFromConfig +
                    SOLR_SELECT_URI + "*," +
                    getChildrenFilter(SKU_WILDCARD, language.toLowerCase()) + "," +
                    getChildrenFilter(SKU_WILDCARD, market.toUpperCase()) +
                    filterQueryStr + 
                    MARKETS + ":" + market.toUpperCase() + "%20&&%20" +
                    LANGUAGES + ":" + language.toLowerCase() +
                    getResultFilter(rootNodesQuery, rows, start);

            }
            
            return solrUrl;
        },
        getBaseProductsByRoots: function (solrPrefixUriFromConfig, matchingRootProducts, language, market, rows, start) {
            var solrUrl = "";
            
            if (matchingRootProducts.length > 0) {
                // Get the products for the matching root nodes
                var rootNodesQuery = "";
                var rootPath;
                for (var i = 0; i < matchingRootProducts.length; i++) {
                    rootPath = matchingRootProducts[i][ROOT];
                    if (rootPath[0].length === BASE_PRODUCT_PREFIX_AND_SKU_LENGTH) {
                        if (rootNodesQuery.length > 0){
                            rootNodesQuery += OR;
                        }
                        rootNodesQuery += PATH_EXACT + ":" + rootPath;
                    }
                }

                // Return the complete records for the given matches
                // Example solrUrl: http://dev-gateway-mulesoft-21:8080/mec/solr/oak/select?fl=*,[child%20parentFilter=path_exact:product/????????%20childFilter=path_exact:product/????????/en],[child%20parentFilter=path_exact:product/????????%20childFilter=path_exact:product/????????/US]&q=path_exact:product/01003611&rows=5&wt=json
                solrUrl =
                    solrPrefixUriFromConfig +
                    SOLR_SELECT_URI + "*," +
                    getChildrenFilter(BASE_PRODUCT_SKU_WILDCARD, language.toLowerCase()) + "," +
                    getChildrenFilter(BASE_PRODUCT_SKU_WILDCARD, market.toUpperCase()) +
                    "&fq=" + 
                    MARKETS + ":" + market.toUpperCase() + "%20&&%20" +
                    LANGUAGES + ":" + language.toLowerCase() +
                    getResultFilter(rootNodesQuery, rows, start);

            }
            
            return solrUrl;
        }
    };

    //---------------------------------------------------------------------------
    //
    //                            Private Methods
    //
    //---------------------------------------------------------------------------
    function getSearchTermQuery(searchTerm, productSearchFields){
        var queryString = "";

        // We want to match any place the searchTerm is contained.
        var escapedSearchTerm = doctorSearchTerm(searchTerm);
        if (productSearchFields.length > 0) {
            queryString += "&fq=";

            if (productSearchFields.indexOf(ProductSearchFieldsEnum.SKU) > -1) {
                queryString += SKU + ":" + escapedSearchTerm + OR;
            }
            if (productSearchFields.indexOf(ProductSearchFieldsEnum.TITLE)> -1) {
                queryString += TITLE + ":" + escapedSearchTerm + OR;
            }
            if (productSearchFields.indexOf(ProductSearchFieldsEnum.DESCRIPTION)> -1) {
                queryString += DESCRIPTION + ":" + escapedSearchTerm + OR;
            }
            if (productSearchFields.indexOf(ProductSearchFieldsEnum.PRODUCTS)> -1) {
                queryString += PRODUCTS + ":" + escapedSearchTerm + OR;
            }
            if (productSearchFields.indexOf(ProductSearchFieldsEnum.KEY_INGREDIENTS)> -1) {
                queryString += KEY_INGREDIENTS + ":" + escapedSearchTerm + OR;
            }
            if (productSearchFields.indexOf(ProductSearchFieldsEnum.CATEGORIES)> -1) {
                queryString += CATEGORIES + ":" + escapedSearchTerm + OR;
            }
            if (productSearchFields.indexOf(ProductSearchFieldsEnum.SEARCH_TERMS)> -1) {
                queryString += SEARCH_TERMS + ":" + escapedSearchTerm + OR;
            }

            // Remove the last "OR".
            var lastIndexOfOr = queryString.lastIndexOf(OR);

            if (lastIndexOfOr != -1) {
                queryString = queryString.substring(0, lastIndexOfOr);
            }
        }

        return queryString;
    }

    function getChildrenFilter(sku, childIndexSuffix){
        return "[" + PARENT_FILTER + PRODUCT_INDEX_QUERY_PARAM + sku + CHILD_FILTER + PRODUCT_INDEX_QUERY_PARAM + sku + "/" + childIndexSuffix + "]";
    }

    function getResultFilter(queryParam, rows, start){
        if (!rows){
            rows = DEFAULT_ROW_VALUE;
        }
        else if (rows > MAX_ROW_VALUE){
            rows = MAX_ROW_VALUE;
        }
        if (!start){
            start = DEFAULT_START_VALUE;
        }

        return "&q=" + queryParam + "&rows=" + rows + "&start=" + start + RETURN_JSON;
    }

    function doctorSearchTerm(searchTerm){
        return '"*' + searchTerm + '*"';
    }
    
    return publicMethods;
}();

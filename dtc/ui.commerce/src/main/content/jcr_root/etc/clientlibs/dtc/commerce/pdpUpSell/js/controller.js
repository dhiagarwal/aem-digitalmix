(function() {
"use strict";

upsellComponent.controller('pdpUpsellComponentController', ['$scope', '$rootScope', 'pdpUpsellComponentService', function($scope,
                                                                                                                          $rootScope, pdpUpsellComponentService) {
    
    var self = this;
    this.priceData = {};
    this.stockData = {};
    var upSellSku = $rootScope.skuCrossSell;
    $scope.componentVisible = false;
    
    $rootScope.$on("updateUpSell", function() {
        self.initUpSell();

    });
    
    self.initUpSell = function() {
        var path = window.location.pathname;
        path = path.substring(0, path.lastIndexOf("."));
        var upSellSku = $rootScope.skuCrossSell;
        var promise = pdpUpsellComponentService.getUpSellProducts(upSellSku, path);
        promise.then(function(data) {
            if(!(angular.equals(data, []))){
                self.skuIdPrimary = data[0].skuid;
                self.imgPrimaryProd = data[0].imgProduct;
                self.productNamePrimary = data[0].productName;
                self.shortDescriptionPrimary = data[0].shortDescription;
                self.productType = data[0].type;
                self.productVariantCount = data[0].countryCode;
                self.upSellProdSkuid = data[1].skuid;
                self.upSellProdName = data[1].productName;
                self.upSellProdUrl = data[1].upSellUrl;
                self.upSellProdImage = data[1].imgProduct;
                self.upSellProdShortDescription = data[1].shortDescription;
                self.upSellProductType = data[1].type;
                self.upSellproductVariantCount = data[1].countryCode;
                if (data[0].purchasable && data[1].purchasable) {
                    self.getPrice(self.skuIdPrimary, self.upSellProdSkuid);
                    $scope.componentVisible = true;
                }
                else{
                    $scope.componentVisible = false;
                }
            }
            else{
                $scope.componentVisible = false;
            }
        });
    }
    
    self.getPrice = function(skuIdPrimary, upSellProdSkuid) {
        var quantity = 1;
        if (upSellProdSkuid != "") {
            var promise = pdpUpsellComponentService.getMultiplePriceData(
                skuIdPrimary, upSellProdSkuid);
            promise.then(function(data) {
                var upSellPrice = 0;
                self.priceData = data;
                var entries = data.priceList;
                for (var i = 0; i < entries.length; i++) {
                    
                    if(!entries[i].hasOwnProperty("priceRange")){
                        
                        if(entries[i].hasOwnProperty("stockData")){
                            var stockDataStatus = entries[i].stockData.lineItemDetailsList;
                            if(stockDataStatus[0].hasOwnProperty("safetyStockStatus")){
                                if(stockDataStatus[0].safetyStockStatus == "LOW_STOCK"){
									 $scope.componentVisible = true;
                                } else if(stockDataStatus[0].safetyStockStatus == "OUT_OF_STOCK" && stockDataStatus[0].backOrdered == false){
                                     $scope.componentVisible = false;
									 break;
                                } else if(stockDataStatus[0].safetyStockStatus == "NORMAL"){
                                    $scope.componentVisible = true;
                                }
                                
                            }
                            if(entries[i].hasOwnProperty("wholeSaleStatus")){
                                if(entries[i].wholeSaleStatus == "02"){
									 $scope.componentVisible = false;
									 break;
                                }
                                else if(entries[i].wholeSaleStatus == "05"){
									 $scope.componentVisible = false;
									 break;
                                }
                                else if(entries[i].wholeSaleStatus == "09"){
									 $scope.componentVisible = false;
									 break;
                                }
                                else if(entries[i].wholeSaleStatus == "10"){
                                    if(stockDataStatus[0].safetyStockStatus == "OUT_OF_STOCK" && stockDataStatus[0].backOrdered == false){
										$scope.componentVisible = false;
										break;
                                    }
                                    else{
										$scope.componentVisible = true;
                                    }
                                }

                            }       

                        }
                        if(entries[i].hasOwnProperty("price")) {
                        	var priceVal = entries[i].price.value;
                            var sum = parseFloat(priceVal);
                            upSellPrice = upSellPrice + sum;
                            var currencySymbol = entries[i].price.formattedValue;
                            currencySymbol = currencySymbol[0];
                            $scope.currency = currencySymbol;
                        }
                    }
                }
                if (isNaN(upSellPrice)) {
                    $scope.newPrice = "";
                } else {
                    $scope.newPrice = upSellPrice;
                }
                
            });
        }
    }
    
    $scope.addUpsellToCart = function() {
        
        var productUpsellQty = "1";
        var upsellProductA = self.skuIdPrimary;
        var upsellProductB = self.upSellProdSkuid;
        
        var productData = {
            "orderList" : [ {
                "code" : upsellProductA,
                "qty" : productUpsellQty
            }, {
                "code" : upsellProductB,
                "qty" : productUpsellQty
            } ]
        };
        if (upsellProductA != "" && upsellProductB != "") {
        	$rootScope.emitShowShoppingBag = true;
            $rootScope.$emit("showShoppingBag", productData);
        }
    }
    
    function init() {
        self.initUpSell();
    }
    
    init();
    
}]);
}());
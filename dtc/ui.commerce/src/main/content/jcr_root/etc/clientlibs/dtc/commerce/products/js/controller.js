(function() {
"use strict";

products.controller('productsController', [function() {

    var self = this;     
	self.totalSkus = 0;
    self.totalMissingSkus = 0;

    var allSkus = angular.element('input[name=allSkus]').val();
    var missingSkus = angular.element('input[name=missingSkus]').val();

	if (allSkus) {
    	self.allSkus = JSON.parse(allSkus);
        self.totalSkus = self.allSkus.length;
    }

    if (missingSkus) {
    	self.missingSkus = JSON.parse(missingSkus);
        self.totalMissingSkus = self.missingSkus.length;
    }

    self.redirectToPdp = function (url) {
        var currentPath = window.location.href;
        var pdpPath = currentPath.substring(0, currentPath.indexOf('.html')) + '.' + url.trim() + '.html';
        window.location.href = pdpPath;
    }

}]);
}());

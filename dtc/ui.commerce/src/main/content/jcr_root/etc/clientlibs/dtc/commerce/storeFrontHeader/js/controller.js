(function() {
"use strict";

storefrontHeader.controller('storefrontHeaderController', ['$scope', '$rootScope', 'storefrontHeaderService','$interval' , '$timeout','$q', 'userProfileManager','$cookies', function($scope, $rootScope,storefrontHeaderService,$interval, $timeout,$q,userProfileManager,$cookies) {


	var self=this;
	self.cartItem = $cookies.get('totalItems');
	self.disImage = false;
	$rootScope.$on("updateBagItems", function(event,data) {
	       self.cartItem = data;  
	});
	var storeId = getParamValue(location.href, "storeId");
	if(storeId){
	   var promise = storefrontHeaderService.getDistributorDetails(storeId);
		promise.then(function(data) {
				self.distributorName = data.displayName;
				if(data.hasOwnProperty("photoURL")){
                    self.imageUrl = data.photoURL;
                    self.disImage = true;
                    
                }
		});
	}
    function getParamValue(url, paramName){
        paramName = paramName.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + paramName + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if(results != null && results != undefined){
            if (!results) results = null;
            if (!results[2]) results = '';
            return decodeURIComponent(results[2].replace(/\+/g, " ")); 
        }
    }

   this.goToPage = function(pageName){
        window.location.href = pageName+".html";
	}
}]);
}());

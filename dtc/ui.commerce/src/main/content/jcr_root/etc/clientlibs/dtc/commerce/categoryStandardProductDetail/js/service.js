(function() {
"use strict";

categoryLandingPage.service('personalCareCategoryService', ['$http', '$q','userProfileManager','serviceURL', 'solrQueryService',function($http, $q,userProfileManager,serviceURL,solrQueryService) {


    this.$http = $http;
    var deferred = $q.defer();

    this.getRatings = function(sku) {
        var deferred = $q.defer();
        //Dynamic URL Generation
        var paramsArray = [];
        paramsArray[0] = sku;        
        
        var url = serviceURL.GetServiceURL('commentsURL' , paramsArray);
        
        $http
            .jsonp(url)
            .then(function(response) {
                deferred.resolve(response.data);
             });
        return deferred.promise;
    };


    this.hasRated = function(sku, uid) {
        var deferred = $q.defer();
        var paramsArray = [];
        paramsArray[0] = uid;  
        paramsArray[1] = sku;
        var url = serviceURL.GetServiceURL('hasRatedURL' , paramsArray);
        $http
            .jsonp(url).then(function(response) {
                deferred.resolve(response.data);
            });
        return deferred.promise;
    };

    
    this.getProductData = function(searchText) {
        var MAX_ROW_TO_RETURN = 1000;
        var locale = LocaleUtil.getLocaleFromPath(window.location.href);

        return solrQueryService.getProductsAndBasesByCategory(searchText, locale.getLanguage(), locale.getCountry(), MAX_ROW_TO_RETURN);
    };

    var cartServiceBaseSite = "cosmetics";
    var user_id;
    var endPoint;
    var config;
    if(userProfileManager.isUserLoggedIn()) {
        user_id = userProfileManager.getUserProfileMasterInfo().userId;
        endPoint = mulesoftHybrisEndpoint;
    } else {
        user_id = "anonymous";
        endPoint = mulesoftHybrisGuestEndpoint;
    }
    this.getPriceData = function(id) {
        var deferred = $q.defer();
        var paramsArray = [];
    	paramsArray[0] = endPoint; 
    	paramsArray[1] = cartServiceBaseSite; 
    	paramsArray[2] = user_id; 
    	paramsArray[3] = id; 
    	var url = serviceURL.GetServiceURL('productsPriceURL' , paramsArray);   
    	
        $http.post(url).then(function(response) {
            deferred.resolve(response.data);
        }, function(response) {
            console.log("Error in calling multiple prices: productSearchListService : getPriceData")
        });
        return deferred.promise;
    };
    this.SearchTextCntr = 0;
    this.setSearchTextCntr = function(count){
		this.SearchTextCntr = count;
    };
	this.getSearchTextCntr = function(){
		return this.SearchTextCntr;
    };
	
	this.getImageData = function(skuIds){
        var deferred = $q.defer(); 
        var paramsArray = [];
    	paramsArray[0] = skuIds;
    	paramsArray[1]= window.location.pathname;
    	var url = serviceURL.GetServiceURL('getImageDataPathParamURL' , paramsArray); 
        $http({
            method: 'GET',
            url: url,
        }).then(function successCallback(response) {
            deferred.resolve(response.data);
        },function (response){ 
            console.log("")
        });
        return deferred.promise;
    };

}]);
}());
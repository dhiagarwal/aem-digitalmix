(function() {
"use strict";

cartRecommendations.controller('cartRecommendationsController', ['$scope', '$rootScope', 'cartRecommendationsService','$interval' , '$timeout','$q', 'userProfileManager','$cookies', function($scope, $rootScope,cartRecommendationsService,$interval, $timeout,$q,userProfileManager,$cookies) {

    var cartID = $cookies.get('cart_guid');
    var self = this;
    $scope.seperator = " - ";
    $scope.slickType = "type2";
    self.priceData = {};
    var starState = {
        starOn: "/etc/clientlibs/dtc/core/main/css/src/assets/images/content/pdp-productbasics/star-on.svg",
        starOff: "/etc/clientlibs/dtc/core/main/css/src/assets/images/content/pdp-productbasics/star-off.svg"
    };
    self.maxStar = 5;
    self.ratingValue = self.ratingValue || 0;
    self.max = self.max || 300;
    self.hidePSV = true;
    self.ratingValue = 0;
    self.recommendationsReload = true;

    var userType;
    if (userProfileManager.isUserLoggedIn()) {
        var userTypes = userProfileManager.getUserProfileMasterInfo().userType.toLowerCase().split("_");
        if (userTypes.length > 1) {
            userType = userTypes[userTypes.length - 1];
        } else {
            userType = userTypes[0];
        }

        if (userType == "distributor") {
            self.hidePSV = false;
        }
    }

    $scope.slickSecondaryConfig = {
        enabled: true,
        infinite: true,
        adaptiveHeight: false,
        mobileFirst: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        initOnload: 'true',
        dots: false,
        speed: 500,
        arrows: true,
        responsive: [{
            breakpoint: 300,
            settings: {
                slidesToShow: 1,
                arrows: true
            }
        }, {
            breakpoint: 880,
            settings: {
                slidesToShow: 3
            }
        }, {
            breakpoint: 1130,
            settings: {
                slidesToShow: 4,
                draggable: false
            }
        }]
    };

    $rootScope.$on("updateCartRecommendations", function() {
        self.recommendationsReload = false;
        self.initCartRecommendations();
    });

    self.initCartRecommendations = function() {
        self.skuId = [];
        if (cartID != undefined && cartID.length != 0) {
            var promise = cartRecommendationsService.getCartData(cartID);
            promise.then(function(data) {
                if (data.entries != undefined) {
                    var entries = data.entries;
                    var prod = "";
                    for (var index = 0; index < entries.length; index++) {
                        prod = entries[index].product;
                        self.skuId.push(prod.code);
                    }
                    self.cartRecommendations(self.skuId);


                } else {
                    $scope.showCartRecommendations = false;
                }
            });
        }
    }

    self.cartRecommendations = function(skuId) {

        var recommendedSkuids = [];
        var starRatingsSkuids = [];
        var path = window.location.pathname;

        var promise = cartRecommendationsService.getcartRecommendations(skuId, path);
        promise.then(function(cartData) {

            if (cartData != undefined && cartData != null && cartData != '') {
                $scope.showCartRecommendations = true;
                $scope.recommendations = cartData;
                for (var i = 0; i < cartData.length; i++) {
                    recommendedSkuids.push(cartData[i].skuid);
                    starRatingsSkuids.push(cartData[i].baseid);
                }
                var promise1 = self.getAvgRatings(starRatingsSkuids);
                promise1.then(function(){
                    var promise2 = self.getRecommendationPrice(recommendedSkuids);
                    promise2.then(function(){
                        $interval(function() {
                            if($(".carousel.carousel-secondary").hasClass("slick-initialized")){
                                $scope.slickSecondaryConfig.enabled=false;
                                $(".carousel.carousel-secondary").slick("slickRemove").slick("slickAdd");
                                $scope.slickSecondaryConfig.adaptiveHeight = true;
                                $scope.slickSecondaryConfig.enabled = true;
                            }
                        }, 400, [2] );
                        self.recommendationsReload = true;
                    });
                });
            } else {
                $scope.showCartRecommendations = false;
            }
        });
    }

    self.getRecommendationPrice = function(skuIds) {
        var deferred = $q.defer();
        var recommendPrice = [];
        var promise = cartRecommendationsService.getPriceData(skuIds);
        promise.then(function(data) {
            recommendPrice = data.priceList;
            var stockStatus = "";
            for (var i = 0; i < $scope.recommendations.length; i++) {
                var obj = $scope.recommendations[i];
                self.showPriceRange = true;
                if (!recommendPrice[i].hasOwnProperty("priceRange")) {
                    if (recommendPrice[i].hasOwnProperty("stockData")) {
                        
                        if (recommendPrice[i].stockData.hasOwnProperty("lineItemDetailsList") && (recommendPrice[i].stockData.lineItemDetailsList[0].hasOwnProperty("safetyStockStatus"))) {
                            var stockStatusData = recommendPrice[i].stockData.lineItemDetailsList[0];
                            
                            if (stockStatusData.safetyStockStatus == "LOW_STOCK") {
                                stockStatus = "LS";
                            } else if (stockStatusData.safetyStockStatus == "OUT_OF_STOCK") {
                            	stockStatus = "OS";
                            } else if (stockStatusData.safetyStockStatus == "NORMAL") {
                                stockStatus = "";
                            } 
                            if((stockStatusData.safetyStockStatus == "OUT_OF_STOCK" || stockStatusData.safetyStockStatus == "LOW_STOCK") && stockStatusData.backOrdered){
                                stockStatus = "BO";
                            }
                        }
                    }

                    if (recommendPrice[i].hasOwnProperty("wholeSaleStatus")) {
                        if(recommendPrice[i].wholeSaleStatus == "02"){
                            self.showPriceRange = false;
                            stockStatus = "OS";
                            
                        }
                        if(recommendPrice[i].wholeSaleStatus == "05"){
                            self.showPriceRange = true;
                            stockStatus = "OS";
                        } 
                    }
                    angular.element('.stockRecently-'+recommendPrice[i].code+'-'+stockStatus).removeClass('ng-hide');
                }
                if(self.showPriceRange ) {
                if (recommendPrice[i].hasOwnProperty("priceRange")) {

                    if (recommendPrice[i].priceRange.hasOwnProperty("maxPrice") && recommendPrice[i].priceRange.hasOwnProperty("minPrice") && skuIds[i] == recommendPrice[i].code) {

                        if (recommendPrice[i].priceRange.maxPrice.value > recommendPrice[i].priceRange.minPrice.value) {
                            obj["price"] = recommendPrice[i].priceRange.minPrice.formattedValue + " - " + recommendPrice[i].priceRange.maxPrice.formattedValue;
                        } else {
                            obj["price"] = recommendPrice[i].priceRange.minPrice.formattedValue;
                        }
                    } else {
                        obj["price"] = "";
                    }


                    if (recommendPrice[i].priceRange.hasOwnProperty("minPv") && skuIds[i] == recommendPrice[i].code) {
                        obj["psv"] = recommendPrice[i].priceRange.minPv.value;
                    } else {
                        obj["psv"] = NaN;
                    }


                    if (recommendPrice[i].priceRange.hasOwnProperty("maxPv") && skuIds[i] == recommendPrice[i].code) {
                        if (recommendPrice[i].priceRange.maxPv.value > recommendPrice[i].priceRange.minPv.value) {
							obj["psvSeperator"] = true;
                            obj["maxPv"] = recommendPrice[i].priceRange.maxPv.value;
                        } else {
							obj["psvSeperator"] = false;
                            obj["maxPv"] = NaN;
                        }
                    } else {
						obj["psvSeperator"] = false;
                        obj["maxPv"] = NaN;
                    }

                } else {

                    if (recommendPrice[i].hasOwnProperty("price") && skuIds[i] == recommendPrice[i].code) {
                        obj["price"] = recommendPrice[i].price.formattedValue;
                    } else {
                        obj["price"] = "";
                    }


                    if (recommendPrice[i].hasOwnProperty("pvValue") && skuIds[i] == recommendPrice[i].code) {
                        obj["psv"] = recommendPrice[i].pvValue.value;
						obj["psvSeperator"] = false;
                        obj["maxPv"] = NaN;
                    } else {
                        obj["psv"] = NaN;
                    }
                }
              }
            }

        });
        deferred.resolve();
        return deferred.promise;
    }

    self.getAvgRatings = function(skuIds) {
        var deferred = $q.defer();
        var avgRatingsPromiseArray=[];
        for (var i = 0; i < skuIds.length; i++) {
            var promise = cartRecommendationsService.getRatings(skuIds[i]);
            avgRatingsPromiseArray.push(promise);
        }

        $q.all(avgRatingsPromiseArray).then(function (data){
            for(var i=0;i<data.length;i++){

                var ratingValue = Math.round(data[i].streamInfo.avgRatings._overall);
                var rating = data[i].streamInfo.commentCount || 0;
                var starRating = self.getStarRatingPosition(ratingValue, 'big');

                var obj = $scope.recommendations[i];
                obj["rating"] = rating;
                obj["stars"] = starRating;

            }
        });
        deferred.resolve();
        return deferred.promise;
    }

    self.getStarRatingPosition = function(ratingDecimalValue, starType) {
        var starTopPosition = 0;
        var starMargin = 0;
        var starWidth = 0;
        var halfStarTopMargin = 0,
            smallStarFirstPosition = 0;
        var halfStarPosition = -273;
        if (starType == "big") {
            starWidth = 18;
            halfStarPosition = -273;
            halfStarTopMargin = -26;
        } else {
            starWidth = 12;
            halfStarPosition = -242;
            halfStarTopMargin = -64;
            smallStarFirstPosition = -44;
        }
        var decimalValue = ratingDecimalValue.toString().replace(/^[^\.]+/, '0');
        var floorRatingValue = Math.floor(ratingDecimalValue);
        decimalValue = parseFloat(decimalValue);
        var mutipleValue = 0;
        if (0.26 <= decimalValue && decimalValue <= 0.74) {
            starMargin = halfStarTopMargin;
            floorRatingValue++;
        } else if (0.75 <= decimalValue && decimalValue <= 0.99) {
            starMargin = smallStarFirstPosition;
            floorRatingValue++;
        } else {
            starMargin = smallStarFirstPosition;
        }
        if (floorRatingValue >= this.max) {
            starMargin = smallStarFirstPosition;
            floorRatingValue = this.max;
        }
        floorRatingValue = floorRatingValue * starWidth;
        floorRatingValue = halfStarPosition + floorRatingValue
        return floorRatingValue + "px " + starMargin + "px";
    }

    this.addProductToCart = function(sku) {
        var productQty = "1";
        var productData = {
            "orderList": [{
                "code": sku,
                "qty": productQty
            }]
        };
        if (productQty > 0) {
        	$rootScope.emitShowShoppingBag = true;
            $rootScope.$emit("showShoppingBag", productData);
            setTimeout(function() {
                $rootScope.$emit("updateCart");
            }, 4000)
        }
    };

    function init() {
        self.initCartRecommendations();
    }
    init();

}]);
}());

(function() {
"use strict";


authenticatedSuggestionsModule.controller('authenticatedSuggestionsController', ['$scope','authenticatedSuggestionsService','$window','userProfileManager', function($scope,authenticatedSuggestionsService,$window,userProfileManager) {
	var self = this;
	self.cardDismissShow = false;
    self.distributor=false;


	 self.config = {
	    		        "columns": {
	    		            "lg": 3,
	    		            "xl": 6
	    		        }
	    		    }


	self.init = function(card){
        var cardtype=card;
		var i = 0;
		var allCards = JSON.parse(angular.element('input#basicsList').val());
		var filteredAllCards = [];
	    var today = new Date();
	    var someday = new Date();
	    for (i = 0; i < allCards.length; i++) { 
	        var item = allCards[i];
	        someday.setFullYear(item['expDate'].year,(item['expDate'].month)-1, item['expDate'].day);
	        if(someday >= today){
					filteredAllCards.push(item);
	        }
	    }



		var cardsFetched = authenticatedSuggestionsService.fetchCards();
		cardsFetched.then(function(data){
	             var removedIds= [];
	             var updatedCards = [];
	             for(var idObj in data ){
                    if(data[idObj].cardType==cardtype){
	                 removedIds.push(data[idObj].cardId);
                     }
	             }
	             for(var ind in filteredAllCards){
	                 for(var index in removedIds){
	                     if(removedIds[index]==filteredAllCards[ind].id){
	                         break;
	                     }else{
	                         if(index==removedIds.length-1){
                                 if(filteredAllCards[ind] && filteredAllCards[ind].title){
	                             updatedCards.push(filteredAllCards[ind]);
                                 }
	                         }
	                     }

	                 }
	             }
            if(removedIds.length==0){
				 self.toDoCards = filteredAllCards;	
            }
            else{
                 self.toDoCards=updatedCards;
            }
	             if(updatedCards.length==0 && removedIds.length!=0){
	                 self.cardDismissShow = true;
	             }	

		 },function(err){
			 self.toDoCards = filteredAllCards;	
		 });
        if(userProfileManager.isUserLoggedIn()){
            var master=userProfileManager.getUserProfileMasterInfo();
            if(master){
                if(master.userType==USER_TYPE.PRIMARY_DISTRIBUTOR){
					self.distributor=true;
                }

            }

        }

	}
	
	 self.breakpoint = function(){
	        if($window.innerWidth>=375 && $window.innerWidth<768)
	            return "sm";
	        else if($window.innerWidth>=768 && $window.innerWidth<1024)
	            return "md";
	        else if($window.innerWidth>=1024 && $window.innerWidth<1439)
	            return "lg";
	        else if($window.innerWidth>=1440)
	            return "xl";
	    }
	    
	    self.navigate = function(link){
	        console.log(link);
	    }

	    self.handleMenuAction = function(actionDesc, isSuggestions){
            var URL = isSuggestions ? angular.element('#'+actionDesc).data('path') : angular.element($('#'+actionDesc)).val();
                                     
            if(undefined != URL && null != URL && '' != URL){
            	if(actionDesc=='nvgmbsstorypath1'){
          		  var mbsPromise = authenticatedSuggestionsService.getMbsStory();
                    mbsPromise.then(function(data){
                    if(data.status==200){
                  	  actionDesc='nvgmbsstorypathtaken1';
                  	  URL = angular.element($('#'+actionDesc)).data('path');	
                  	  window.location = window.location.protocol+'//'+window.location.host+ URL + '.html?outsideMBS=yes';
                    	}else
                    if(data.status==404){
                  	  window.location = window.location.protocol+'//'+window.location.host+ URL + '.html';
                       }
                    })                            		  
          	  }else{
          		  window.location = window.location.protocol+'//'+window.location.host+ URL + '.html';
			  }
            }
	    }
		self.handleAccordionHeight = function() {
        angular.element('.dist-auth-cards .bg-cayman').css({'height': '', 'max-height': '', 'overflow': ''});
    	};
	
}]);
}());

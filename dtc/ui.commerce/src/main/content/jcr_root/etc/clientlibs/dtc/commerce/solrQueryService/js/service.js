(function() {
    "use strict";

    solrQuery.service('solrQueryService', ['$http', '$q', function ($http, $q) {
        //---------------------------------------------------------------------------
        //
        //                           Public Methods
        //
        //---------------------------------------------------------------------------

        /**
         * Find all products that have a field that matches the searchTerm. If the search term doesn't contain any wild
         * characters we will be search for an exact match for the value of searchTerm.
         *
         * @param searchTerm - Examples: Age, Age*, *Age*, etc.
         * @param language - Example: "en"
         * @param market   - Example: "US"
         *
         * @returns A promise to the result which may contain general products, base products, and variant products.
         */
        this.getAllProductsBySearchTerm = function(searchTerm, language, market, rows, start){
            return getProductsBySearchTerm(ProductResultsEnum.ALL_PRODUCTS, searchTerm, language, market, rows, start);
        };

        /**
         * Find all products that have a field that matches the searchTerm. If the search term doesn't contain any wild
         * characters we will be search for an exact match for the value of searchTerm.
         *
         * @param searchTerm - Examples: Age, Age*, *Age*, etc.
         * @param language - Example: "en"
         * @param market   - Example: "US"
         *
         * @returns A promise to the result which may contain general products and base products. Variant products are excluded.
         */
        this.getProductsAndBasesBySearchTerm = function(searchTerm, language, market, rows, start){
            return getProductsBySearchTerm(ProductResultsEnum.PRODUCTS_AND_BASE_PRODUCTS, searchTerm, language, market, rows, start);
        };

        /**
         * Find all products that have a field that matches the searchTerm. If the search term doesn't contain any wild
         * characters we will be search for an exact match for the value of searchTerm.
         *
         * @param searchTerm - Examples: Age, Age*, *Age*, etc.
         * @param language - Example: "en"
         * @param market   - Example: "US"
         *
         * @returns A promise to the result which may contain general products and variant products. Base products are excluded.
         */
        this.getProductsAndVariantsBySearchTerm = function(searchTerm, language, market, rows, start){
            return getProductsBySearchTerm(ProductResultsEnum.PRODUCTS_AND_VARIANT_PRODUCTS, searchTerm, language, market, rows, start);
        };

        /**
         * Find all products that have a category field that matches the searchTerm. If the search term doesn't contain any wild
         * characters we will be search for an exact match for the value of searchTerm.
         *
         * @param searchTerm - Examples: Age, Age*, *Age*, etc.
         * @param language - Example: "en"
         * @param market   - Example: "US"
         *
         * @returns A promise to the result which may contain general products, base products, and variant products.
         */
        this.getAllProductsByCategory = function(searchTerm, language, market, rows, start) {
            return getProductsByCategory(ProductResultsEnum.ALL_PRODUCTS, searchTerm, language, market, rows, start);
        };

        /**
         * Find all products that have a category field that matches the searchTerm. If the search term doesn't contain any wild
         * characters we will be search for an exact match for the value of searchTerm.
         *
         * @param searchTerm - Examples: Age, Age*, *Age*, etc.
         * @param language - Example: "en"
         * @param market   - Example: "US"
         *
         * @returns A promise to the result which may contain general products and base products. Variant products are excluded.
         */
        this.getProductsAndBasesByCategory = function(searchTerm, language, market, rows, start) {
            return getProductsByCategory(ProductResultsEnum.PRODUCTS_AND_BASE_PRODUCTS, searchTerm, language, market, rows, start);
        };

        /**
         * Find all products that have a category field that matches the searchTerm. If the search term doesn't contain any wild
         * characters we will be search for an exact match for the value of searchTerm.
         *
         * @param searchTerm - Examples: Age, Age*, *Age*, etc.
         * @param language - Example: "en"
         * @param market   - Example: "US"
         *
         * @returns A promise to the result which may contain general products and variant products. Base products are excluded.
         */
        this.getProductsAndVariantsByCategory = function(searchTerm, language, market, rows, start) {
            return getProductsByCategory(ProductResultsEnum.PRODUCTS_AND_VARIANT_PRODUCTS, searchTerm, language, market, rows, start);
        };
        
        //---------------------------------------------------------------------------
        //
        //                           Private Methods
        //
        //---------------------------------------------------------------------------

        function getProductsBySearchTerm(productSearchResult, searchTerm, language, market, rows, start){
            var productSearchFields = [];

            // Search all fields except "products". We exclude "prducts" because its values are made up of some of the
            // fields combined.
            productSearchFields.push(ProductSearchFieldsEnum.SKU);
            productSearchFields.push(ProductSearchFieldsEnum.TITLE);
            productSearchFields.push(ProductSearchFieldsEnum.DESCRIPTION);
            productSearchFields.push(ProductSearchFieldsEnum.KEY_INGREDIENTS);
            productSearchFields.push(ProductSearchFieldsEnum.CATEGORIES);
            productSearchFields.push(ProductSearchFieldsEnum.SEARCH_TERMS);

            return getProductsBySearchFields(productSearchResult, searchTerm, productSearchFields, language, market, rows, start);
        }

        function getProductsByCategory(productSearchResult, searchTerm, language, market, rows, start){
            var productSearchFields = [];
            productSearchFields.push(ProductSearchFieldsEnum.CATEGORIES);

            return getProductsBySearchFields(productSearchResult, searchTerm, productSearchFields, language, market, rows, start);
        }

        function getProductsBySearchFields(productSearchResult, searchTerm, productSearchFields, language, market, rows, start){
            var deferred = $q.defer();

            // Example solrUrl: http://dev-gateway-mulesoft-21:8080/mec/solr/oak/select?fl=_root_&fq=-baseSku:[* TO *] && markets:US&fq=categories:men&q=path_exact:product/????????/en&rows=5&wt=json
            var solrUrl = SolrQueryUrlService.getProductRootsForSearchTerms(productSearchResult, mulesoftSolrEndpoint, searchTerm, productSearchFields, language, market, rows, start);

            // Search for language nodes for the given market that match the search term.
            var queryResponsePromise = $q.defer();
            	$http({
                method : 'GET',
                url : solrUrl,
                headers : {
                    'Content-Type' : 'application/json'
                }
            }).success(function(data) {
            	queryResponsePromise.resolve(data);
            }).error(function(data) {
            	queryResponsePromise.reject(data);
                console.log('Search failed!');
            });

            queryResponsePromise.promise.then(function(responseData){
                var matchingRootProducts = responseData.response.docs;

                // Example solrUrl: http://dev-gateway-mulesoft-21:8080/mec/solr/oak/select?fl=*,[child%20parentFilter=path_exact:product/????????%20childFilter=path_exact:product/????????/en],[child%20parentFilter=path_exact:product/????????%20childFilter=path_exact:product/????????/US]&q=path_exact:product/01003611&rows=5&wt=json
                solrUrl = SolrQueryUrlService.getProductsByRoots(productSearchResult, mulesoftSolrEndpoint, matchingRootProducts, language, market, rows, start);

                if (solrUrl.length > 0) {
                    var productDocuments = [];

                    // Return the complete records for the given matches
                    queryResponsePromise = $q.defer();
                    $http({
                        method : 'GET',
                        url : solrUrl,
                        headers : {
                            'Content-Type' : 'application/json'
                        }
                    }).success(function(data) {
                        var docs = data.response.docs;

                        // Wrap the products in an adaptor for ease of use.
                        for (var i=0;i<docs.length;i++){
                            productDocuments.push(new SolrProductAdaptor(docs[i]));
                        }

                        // See if we want base products
                        if (productSearchResult === ProductResultsEnum.ALL_PRODUCTS || productSearchResult === ProductResultsEnum.PRODUCTS_AND_BASE_PRODUCTS){
                            queryResponsePromise.resolve(data);
                        }
                        else {
                            deferred.resolve(productDocuments);
                        }
                    }).error(function() {
                        console.log('Search failed!');
                    });

                    // See if we want base products
                    if (productSearchResult === ProductResultsEnum.ALL_PRODUCTS || productSearchResult === ProductResultsEnum.PRODUCTS_AND_BASE_PRODUCTS){
                        queryResponsePromise.promise.then(function (responseData) {
                            solrUrl = SolrQueryUrlService.getBaseProductsByRoots(mulesoftSolrEndpoint, matchingRootProducts, language, market, rows, start);

                            if (solrUrl.length > 0) {

                                // Return the complete records for the given matches
                                queryResponsePromise = $q.defer();
                                $http({
                                    method: 'GET',
                                    url: solrUrl,
                                    headers: {
                                        'Content-Type': 'application/json'
                                    }
                                }).success(function (data) {
                                    var docs = data.response.docs;

                                    // Wrap the products in an adaptor for ease of use.
                                    for (var i = 0; i < docs.length; i++) {
                                        productDocuments.push(new SolrProductAdaptor(docs[i]));
                                    }

                                    deferred.resolve(productDocuments);
                                }).error(function () {
                                    console.log('Search failed!');
                                });
                            }
                        });
                    }
                }
                else
                {
					deferred.resolve(undefined);
                }
            });

            return deferred.promise;
        }
    }])
}());
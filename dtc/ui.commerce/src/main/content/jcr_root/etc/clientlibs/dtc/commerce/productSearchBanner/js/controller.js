(function() {
"use strict";

productSearchBanner.controller('productSearchBannerController', ['$location', '$rootScope', 'productSearchBannerService', function($location,$rootScope, productSearchBannerService) {

	var self = this;
	self.zeroResult = false;
	self.closeMatchFound = false;
	self.resultNum = '';
	$rootScope.resultFound;

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }
	
	$rootScope.$on("showSearchBanner", function(event, data){
        self.resultNum = data.totalResult;
        self.searchBanner(data.totalResult);        
    });
	
    self.searchBanner = function(totalResult){

        var urlParam = getParameterByName("search");
        var urlminPSV = getParameterByName("minPSV");
        var urlmaxPSV = getParameterByName("maxPSV");
        if (urlParam || urlminPSV || urlmaxPSV) {
            
            self.minPSV = urlminPSV;
            self.maxPSV = urlmaxPSV;
            self.searchInput = urlParam;
            var searchResultTotal = totalResult;
            if(searchResultTotal == 0){
                self.resultNum = 0;
                $rootScope.resultFound=self.resultNum;
                self.closeMatchFound = true;
            } else {
                self.resultNum = searchResultTotal;
                $rootScope.resultFound=self.resultNum;
                self.zeroResult = false;
                self.closeMatchFound = false;
            }
            
        } else {
            self.minPSV = '';
            self.maxPSV = '';
            self.searchInput = '';
        }
    }
    
    self.searchClicked = function(){
    	if(screen.width < 1024){
    		$rootScope.$broadcast('searchHandler', {type:'sideMenu'});
    	}
        else{
        	$rootScope.$broadcast('searchHandler', {type:'headerSearch'});
        }
    }
    
}]);
}());

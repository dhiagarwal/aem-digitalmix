(function() {
"use strict";

productDetail
		.service(
				'productDetailService',
				['$http', '$q', 'userProfileManager','serviceURL',function($http, $q,userProfileManager,serviceURL) {

					var cartServiceBaseSite = "cosmetics";
                    var user_id;
                    var endPoint;
				    if(userProfileManager.isUserLoggedIn()) {
				        user_id = userProfileManager.getUserProfileMasterInfo().userId;
                        endPoint = mulesoftHybrisEndpoint;
				    } else {
				        user_id = "anonymous";
                        endPoint = mulesoftHybrisGuestEndpoint;
				    }


					this.getPriceData = function(id) {
						var deferred = $q.defer();
						var config={
							headers : {
								'Content-Type' : 'application/json'
							}
						};
						var paramsArray = [];
						paramsArray[0] = endPoint;
						paramsArray[1] = cartServiceBaseSite;
						paramsArray[2] = user_id;
						paramsArray[3] = id;
						var url = serviceURL.GetServiceURL('productsPriceURL' , paramsArray);
						
						$http.post(url,config).then(function(response) {
							deferred.resolve(response.data);
						},function (response){ 
							console.log("Error in calling multiple prices: productDetail : getPriceData");
						});
						return deferred.promise;
					
				        
					}
					
					
					this.setRatings = function(uid, baseSkuId, rating, commentText, tags) {
						var deferred = $q.defer();
						var paramsArray = [];
			     		paramsArray[0] = uid;
			     		paramsArray[1] = baseSkuId;
			     		paramsArray[2] = rating;
			     		paramsArray[3] = commentText;
			     		paramsArray[4] = tags;
			     		
			            var url = serviceURL.GetServiceURL('setProductRatingsURL' , paramsArray);
						$http.jsonp(url).then(function(data) {
							deferred.resolve(data);
						});
						return deferred.promise;
					}
					
					this.getRatings = function(sku, timeStamp) {
						var deferred = $q.defer();
						var paramsArray = [];
						paramsArray[0] = sku;
						paramsArray[1] = timeStamp;
						var url = serviceURL.GetServiceURL('ratingsDescOrderURL' , paramsArray);
						$http.jsonp(url)
								.then(function(response) {
									deferred.resolve(response.data);
								});
						return deferred.promise;
					}
					
					this.hasRated = function(sku, uid) {
						/* This Method originally does not pass Gigya Secret Key and Gigya user key. After Dynamic URL generation both these paramerters
						 * are included in the Query Params. Whether these two params are required or not is unclear. This needs to be tested*/
						var deferred = $q.defer();
						var paramsArray = [];
						paramsArray[0] = uid;
						paramsArray[1] = sku;
						paramsArray[2] = 'Products';
						var url = serviceURL.GetServiceURL('hasRatedURL' , paramsArray);
						$http.jsonp(url).then(function(response) {
							deferred.resolve(response.data);
						});
						/*$http.jsonp('https://comments.us1.gigya.com/comments.getUserComments?format=jsonp&callback=JSON_CALLBACK&senderUID='
										+ uid + '&apiKey=' + gigyaApiKey + '&categoryID=Products&streamID=' + sku).then(function(response) {
									deferred.resolve(response.data);
								});*/
						return deferred.promise;
					}
				}]);
}());
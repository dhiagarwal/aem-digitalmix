'use strict';

commerceModule.requires.push('checkoutContainerModule');

var checkoutContainer = angular.module('checkoutContainerModule', []);

checkoutContainer.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {


    var shipState = $('#shipState').val() || 'shipping';
    var deliState = $('#deliState').val() || 'delivery';
    var payState = $('#payState').val() || 'payment';
    var reviewState = $('#reviewState').val() || 'review';


    $urlRouterProvider.otherwise($('#shipRoute').val());
    
    $stateProvider

		.state('home', {
            url: '/'
        })

    	.state(shipState, {
            url: $('#shipRoute').val(),
            templateUrl: $('#ship').val()
        })

        .state(deliState, {
            url: $('#deliRoute').val(),
            templateUrl: $('#deli').val()       
        })

    	.state(payState, {
            url: $('#payRoute').val(),
            templateUrl: $('#pay').val()       
        })

    	.state(reviewState, {
            url: $('#reviewRoute').val(),
            templateUrl: $('#review').val()       
        });

}]);
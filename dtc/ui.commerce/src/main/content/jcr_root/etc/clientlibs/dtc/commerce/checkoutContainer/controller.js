(function() {
"use strict";

checkoutContainer.controller('checkoutContainerController', ['$scope', 'checkoutContainerService', '$state', '$location', '$rootScope', 'userProfileManager','$cookies',function($scope,
    checkoutContainerService,$state,$location,$rootScope,userProfileManager,$cookies) {

    var CART_GUID = $cookies.get("cart_guid");

    var self = this;
    
    self.cartData = [];
    
    self.voucherApplied = [];
    
    $scope.errPromoCode;
    
    self.model = {};
    
    self.orderSummary = {}
    
    self.initOrderSummaryStrings = function(){
    	
    	self.orderSummary.taxesLabel = angular.element('input[name=taxesLabel]').val();
    	self.orderSummary.shippingLabel = angular.element('input[name=shippingLabel]').val();
    	self.orderSummary.estimatedTotallLabel = angular.element('input[name=estimatedTotallLabel]').val();
    	self.orderSummary.promoCodeLabel = angular.element('input[name=promoCodeLabel]').val();
    	self.orderSummary.promoCodePlaceHolder = angular.element('input[name=promoCodePlaceHolder]').val();
    	self.orderSummary.promorewardsLabel = angular.element('input[name=promorewardsLabel]').val();
    	self.orderSummary.rewardsAppliedLabel = angular.element('input[name=rewardsAppliedLabel]').val();
    	self.orderSummary.buttonLabel = angular.element('input[name=buttonLabel]').val();
    	self.orderSummary.shippingDefaultLabel = angular.element('input[name=shippingDefaultLabel]').val();
    	self.orderSummary.errorPromoLabel = angular.element('input[name=errorPromoLabel]').val();
    	self.orderSummary.subtotalLabel = angular.element('input[name=subtotalLabel]').val();
    	self.orderSummary.savingsLabel = angular.element('input[name=savingsLabel]').val();
    	self.orderSummary.freeText = angular.element('input[name=freeText]').val();

    }
	
	function init(){
        if (CART_GUID != undefined && CART_GUID != null && CART_GUID != '') {

            var getCartResponse = checkoutContainerService.getCart(CART_GUID);
            getCartResponse.then(function(response){
                self.cartData = response.data;
                
                self.hasDeliveryAddress = self.cartData.hasOwnProperty("deliveryAddress");
                self.hasDeliveryMode = self.cartData.hasOwnProperty("deliveryMode");
                self.hasPaymentInfo = self.cartData.hasOwnProperty("paymentInfo");
                
                if (self.cartData.hasOwnProperty("appliedVouchers")) {
                    for (var i = 0; i < self.cartData.appliedVouchers.length; i++) {
                        self.voucherApplied.push(self.cartData.appliedVouchers[i].voucherCode);
                    }
                }
                
                /*if(self.hasDeliveryAddress && self.hasDeliveryMode && self.hasPaymentInfo && userProfileManager.isUserLoggedIn()){
                    $state.go($('input[id=reviewState]').val());
                }else{
                    $state.go($('input[id=shipState]').val());
                }*/
            });

        }
        self.initOrderSummaryStrings();
    }
    init();
   
	$rootScope.$on("refreshNavigation", function(){
		self.getCartDetail();
       	self.setNavTitle();
    });

    self.getCartDetail = function() {
        if (CART_GUID != undefined && CART_GUID != null && CART_GUID != '') {

            if (checkoutContainerService.getCartData != ''){

                    self.cartData = checkoutContainerService.getCartData;

                    self.hasDeliveryAddress = self.cartData.hasOwnProperty("deliveryAddress");
                    self.hasDeliveryMode = self.cartData.hasOwnProperty("deliveryMode");
                    self.hasPaymentInfo = self.cartData.hasOwnProperty("paymentInfo");
            }

        }
    }

    self.gotoPage = function(pageName) {
		$state.go(pageName);
		self.setNavTitle();
    }

    self.setNavTitle = function() {
       var url = $location.url();
        if(url == angular.element('input[id=shipRoute]').val()){
			self.navTitle= angular.element('input[name=shippingAddressTitle]').val();
            self.checkoutStep = "shippingAddress";
        }
        else if(url == angular.element('input[id=deliRoute]').val()){
			self.navTitle= angular.element('input[name=deliveryOptionsTitle]').val();
            self.checkoutStep = "deliveryOptions";
        }
        else if(url == angular.element('input[id=payRoute]').val()){
			self.navTitle= angular.element('input[name=paymentMethodTitle]').val();
            self.checkoutStep = "paymentMethod";
        }
        else if(url == angular.element('input[id=reviewRoute]').val()){
			self.navTitle= angular.element('input[name=orderReviewTitle]').val();
            self.checkoutStep = "orderReview";
        }
    };
    
    self.applyPromoCode = function(code,errorDefault) {
        if (!code) {
            self.model.err = true;
            $scope.errPromoCode = errorDefault;
            return;
        }
        if (CART_GUID != undefined && CART_GUID != null && CART_GUID != '') {
            var applyVoucherResponse = checkoutContainerService
                .applyVoucher(code, CART_GUID);
            applyVoucherResponse.then(function(data) {
                if (data.hasOwnProperty("errors") && data != '') {
                    var error = data.errors[0].message;
                    if (error.indexOf("ERR_VOUCHER_002") > -1) {
                        self.model.err = true;
                        $scope.errPromoCode = "Voucher " + code + " cannot be redeemed";
                        return;

                    } else if (error.indexOf("ERR_VOUCHER_001") > -1) {
                        self.model.err = true;
                        $scope.errPromoCode = "Invalid Promotional Code";
                        return;
                    } else if (error.indexOf("ERR_VOUCHER_003") > -1) {
                        self.model.err = true;
                        $scope.errPromoCode = "Error while applying voucher";
                        return;
                    }
                } else {
                    self.voucherApplied.push(code);
					if(self.voucherApplied.length > 1){
						self.voucherApplied.splice(0, 1);
                    }
                    var getCartResponse = checkoutContainerService.getCart(CART_GUID);
                    getCartResponse.then(function(response) {
                        self.model.savings = true;
                        self.cartData = response.data;
                    });
                    self.model.promoCode = '';
                }
            });
        } else {
            self.model.promoCode = '';
        }
    }

    self.togglePanel = function() {
		if (window.innerWidth < 1024) {
			switch(self.model.isPullLabelVisible) {
				case false:
				angular.element('body').css({'background': '', 'overflow': ''});
				angular.element('input,select').removeAttr('disabled');
				angular.element('.checkout-title-container').closest('.content').css('opacity', '');
				angular.element('.accordion-panel').css({'height': '', 'min-height': '', 'overflow': ''});
                angular.element('.overlay-checkout').remove();
				angular.element('.overlay-footer').remove();
				break;
				case true:
				angular.element('body').css({'background': 'black', 'overflow': 'hidden'});
				angular.element('input,select').attr('disabled', 'true');
                angular.element('.shopping-cart-top').find('input').attr('disabled', false);
				angular.element('.checkout-title-container').closest('.content').css('opacity', '0.4');
				angular.element('.accordion-panel').css({'max-height': '27.125rem', 'min-height': '23.4375rem', 'overflow-y': 'auto', 'overflow-x': 'hidden'});
                var docHeight = angular.element(document).height();
				angular.element('.checkout-title-container').append("<div class='overlay-checkout'></div>");
				angular.element(".overlay-checkout").height(docHeight);
				angular.element('.footer-details').append("<div class='overlay-footer'></div>");
				docHeight = angular.element(".footer-details").outerHeight();
				angular.element(".overlay-footer").height(docHeight);
				break;
			}
		}
	};

    self.removePromoCode = function(code) {
        if (cartID != undefined && CART_GUID != null && CART_GUID != '') {
            var removeVoucherResponse = checkoutContainerService.removeVoucher(code,
                CART_GUID);
            removeVoucherResponse.then(function(data) {
                if (self.model.err == true) {
                    self.toggleErr();
                }
                self.voucherApplied.pop(code);
                self.model.promoCode = '';
                var getCartResponse = checkoutContainerService.getCart(CART_GUID);
                getCartResponse.then(function(response) {
                    if (response.data.hasOwnProperty("appliedVouchers") && response.data.totalDiscounts.value != 0) {
                        self.model.savings = true;
                    } else {
                        self.model.savings = false;
                    }
                    self.cartData = response.data;
                });
            });
        }
    }
    
    self.toggleErr = function() {
        self.model.err = false;
    }

    self.reposition = function() {
        var modal = $(this),
            dialog = modal.find('.modal-dialog');
        modal.css('display', 'block');
        dialog.css("margin-top", Math.max(0, ($(window).height() - dialog.height()) / 2));
    };

    angular.element('.modal').on('show.bs.modal', self.reposition);
    $(window).on('resize', function() {
    	angular.element('.modal:visible').each(self.reposition);
    });

}]);
}());

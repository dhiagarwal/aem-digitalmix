(function() {
"use strict";

checkoutContainer.service('checkoutContainerService', ['$http', '$q','userProfileManager','serviceURL', function($http, $q,userProfileManager,serviceURL) {

	var cartServiceBaseSite = "cosmetics";
    var user_id;
    var endPoint;
    if(userProfileManager.isUserLoggedIn()) {
        user_id = userProfileManager.getUserProfileMasterInfo().userId;
        endPoint = mulesoftHybrisEndpoint;
    } else {
        user_id = "anonymous";
        endPoint = mulesoftHybrisGuestEndpoint;
    }
    
	var newCartResponse = $q.defer();

    this.getCartData = '';

	this.getCart = function(guid) {
        var getCartResponse = $q.defer();
        var paramsArray = [];
    	paramsArray[0] = endPoint;
    	paramsArray[1] = cartServiceBaseSite;
    	paramsArray[2] = user_id;
    	paramsArray[3] = guid;
    	var url = serviceURL.GetServiceURL('cartURL' , paramsArray); 
		$http.get(url).then(function(response) {
			getCartResponse.resolve(response);
		},function (response){
			getCartResponse.resolve(response);
        });
		return getCartResponse.promise;
	}

    this.applyVoucher = function(voucherCode,guid) {
        var applyVoucherResponse = $q.defer();
        var paramsArray = [];
    	paramsArray[0] = endPoint;
    	paramsArray[1] = cartServiceBaseSite;
    	paramsArray[2] = user_id;
    	paramsArray[3] = guid;
    	paramsArray[4] = voucherCode;
    	
    	var url = serviceURL.GetServiceURL('applyVoucherURL' , paramsArray);
    	
        $http.post(url).then(function successCallback(response) {
        		applyVoucherResponse.resolve(response.data);
        	  }, function errorCallback(response) {
        	    applyVoucherResponse.resolve(response.data);
        	  });
		 return applyVoucherResponse.promise;
    }

    this.removeVoucher = function(voucherCode,guid) {
       var removeVoucherResponse = $q.defer();
       var paramsArray = [];
       paramsArray[0] = endPoint;
   		paramsArray[1] = cartServiceBaseSite;
   		paramsArray[2] = user_id;
   		paramsArray[3] = guid;
   		paramsArray[4] = voucherCode;
   	
   		var url = serviceURL.GetServiceURL('removeVoucherURL' , paramsArray);
   		
       $http({method: 'DELETE', url: url}).then(function(response) {
        removeVoucherResponse.resolve(response.data);
       },function (response){
           console.log("Error in calling Remove Voucher :Checkout Container: removeVoucher")});
		 return removeVoucherResponse.promise;
    }

}]);
}());

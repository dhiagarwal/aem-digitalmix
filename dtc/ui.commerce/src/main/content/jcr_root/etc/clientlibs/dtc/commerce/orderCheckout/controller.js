(function() {
"use strict";


orderCheckout.controller('orderCheckoutController', ['$scope', '$http', 'checkoutContainerService', 'orderCheckoutService', '$state', '$location', '$rootScope', 'userProfileManager','$cookies', function($scope, $http, checkoutContainerService, orderCheckoutService, $state, $location, $rootScope, userProfileManager,$cookies) {

    var cartID = $cookies.get("cart_guid");

	window.scrollTo( 0, 0 );
	
    var self = this;
    this.bag = [];
    self.isChecked = false;
    this.voucherApplied = [];
    var model = this.model = {};
    var invalidCvv = angular.element('input[name=invalidCVV]').val();
    var insufficientBalance = angular.element('input[name=insufficientBalance]').val();
	var gatewayError = angular.element('input[name=gatewayError]').val();
	var currentUrl= $location.absUrl();
    this.editUrl = currentUrl.split("#")[0]+"#"; 
	this.shippingState = angular.element('input[id=shipState]').val();
    this.shippingRoute = angular.element('input[id=shipRoute]').val();
    this.deliveryState = angular.element('input[id=deliState]').val();
    this.deliveryRoute = angular.element('input[id=deliRoute]').val();
	this.paymentState = angular.element('input[id=payState]').val();
    this.paymentRoute = angular.element('input[id=payRoute]').val();
	var cartmodifiedMessage = angular.element('input[name=cartModified]').val();
    var outOfStockMessage = angular.element('input[name=outOfStockMessage]').val();
	var comingSoonMessage = angular.element('input[name=comingSoonMessage]').val();
    var orderReviewRendition = angular.element('input[name=orderReviewRendition]').val();
	var backOrderMessage = angular.element('input[name=backOrderMessage]').val();

	self.hidePSV = true;
	var termsConditionsErr = false;
	var showError = false;
	self.isDisabled = false;

    var userType;
    if(userProfileManager.isUserLoggedIn()) {
        var userTypes = userProfileManager.getUserProfileMasterInfo().userType.toLowerCase().split("_");
        if(userTypes.length > 1) {
            userType = userTypes[userTypes.length-1];
        } else {
            userType = userTypes[0];
        }
        
        if(userType == "distributor"){
            self.hidePSV = false;
        }
    }
	$rootScope.paymentError="";


    if (cartID != undefined && cartID != null && cartID != '') {

                var getCartResponse = checkoutContainerService.getCart(cartID);
                getCartResponse.then(function(response) {
                    checkoutContainerService.getCartData = response.data;
                    self.bag = checkoutContainerService.getCartData;
                    self.bag.deliveryAddress.phone=self.formatPhoneNumber(self.bag.deliveryAddress.phone);
                    self.cardType = self.bag.paymentInfo.cardType.code;
                    $rootScope.$emit("refreshNavigation");
					
				if (response.data.entries != undefined) {
    				var entries = response.data.entries;
    				
    				 var skuIDs = [];
                     for(var s=0; s<response.data.entries.length; s++){
                         skuIDs[s] = response.data.entries[s].product.code;                  
                     }
                     if(skuIDs.length > 0){
						 var currentPagePath = window.location.pathname;
                         var imageDataResponse = orderCheckoutService.getImageData(skuIDs,currentPagePath);
                         imageDataResponse.then(function(imageData) {
                             if(imageData != null){
                                 for(var j=0;j< imageData.length;j++){
                                     var obj = response.data.entries[j].product;
                                     if(imageData[j].sku === response.data.entries[j].product.code){
                                         obj["imagePath"] = imageData[j].path + '/jcr:content/renditions/' + orderReviewRendition;
										 obj["productTitle"] = imageData[j].title;
                                     }
                                 }
                             }
                         });
                     }
    				
    				var stock = '';
    				for (var index = 0; index < entries.length; index++) {
        				var obj = self.bag.entries[index];
        				if (entries[index].hasOwnProperty("stockData")) {
            				stock = entries[index].stockData.lineItemDetailsList;
            				for (var i = 0; i < stock.length; i++) {
                				if (stock[i].hasOwnProperty("safetyStockStatus")) {
                    				if (stock[i].safetyStockStatus == "LOW_STOCK") {
                        				if (entries[index].quantityUpdated) {
											obj["stockStatus"] = "LS";
                                            obj["stockUpdate"] = stock[i].totalAvailableQuantity;
                                            obj["stockDescription"] = cartmodifiedMessage;
											obj["quantity"] =  stock[i].availableQuantity;
                                            self.checkoutToastMessage(cartmodifiedMessage);
                        				} 
                                        else {
                                            obj["stockStatus"] = "LS";
                                            obj["stockUpdate"] = stock[i].totalAvailableQuantity;
                                            obj["stockDescription"] = '';
											obj["quantity"] =  stock[i].availableQuantity;
                        				}

                    				} 
                                    else if (stock[i].safetyStockStatus == "OUT_OF_STOCK") {
                                        if(stock[i].hasOwnProperty("backOrdered") && stock[i].backOrdered == false){
                                        obj["stockStatus"] = "OS";
                                        obj["stockDescription"] = outOfStockMessage;
                                        obj["quantity"] =  stock[i].availableQuantity;
                                        self.checkoutToastMessage(outOfStockMessage);
                                    }
										
										
                                    }
                                    else if (stock[i].safetyStockStatus == "NORMAL") {
                                        obj["stockStatus"] = '';
                                        obj["stockUpdate"] = '';
                                        obj["stockDescription"] = '';
										obj["quantity"] =  stock[i].availableQuantity;
                    				}
                    				if (stock[i].safetyStockStatus == "OUT_OF_STOCK" && stock[i].backOrdered && stock[i].hasOwnProperty("backOrdered")) {
                                        if(stock[i].hasOwnProperty("backOrderedDate")){
                                            obj["stockStatus"] = "BO";
                                            var dateString = stock[i].backOrderedDate;
                                            var backOrderDate = self.formatDate(dateString);
                                            obj["stockUpdate"] = backOrderDate;
                                            obj["stockDescription"] = '';
											obj["quantity"] = entries[index].quantity;
											self.checkoutToastMessage(backOrderMessage);
                                        }                                        
                                    }
                				}
								if(entries[index].product.hasOwnProperty("wholeSaleStatus") && entries[index].product.wholeSaleStatus == "02")
								{
									obj["stockStatus"] = "OS";
									obj["stockDescription"] = outOfStockMessage;
									obj["stockRemaining"] = stock[i].totalAvailableQuantity;
									self.checkoutToastMessage(outOfStockMessage);
								}
								if(entries[index].product.hasOwnProperty("wholeSaleStatus") && entries[index].product.wholeSaleStatus == "05")
								{
									obj["stockStatus"] = "OS";
									obj["stockDescription"] = outOfStockMessage;
									obj["stockRemaining"] = stock[i].totalAvailableQuantity;
									self.checkoutToastMessage(outOfStockMessage);
								}
								if(entries[index].product.hasOwnProperty("wholeSaleStatus") && entries[index].product.wholeSaleStatus == "09")
								{
									obj["stockStatus"] = "CS";
									obj["stockDescription"] = comingSoonMessage;
									obj["stockRemaining"] = stock[i].totalAvailableQuantity;
									self.checkoutToastMessage(comingSoonMessage);
								}	
            				}
							
        				}
    				}
				}

                    if (self.bag.hasOwnProperty("appliedVouchers")) {
						for (var i = 0; i < self.bag.appliedVouchers.length; i++) {
							self.voucherApplied.push(self.bag.appliedVouchers[i].voucherCode);
						}
					}
            if(self.bag.totalDiscounts.value!=0){
                self.model.savings = true;
                }
                });            

        }

    self.placeOrder = function(pageName) {
        if (cartID != undefined && cartID != null && cartID != '' && self.isChecked) {
        	self.isDisabled = true;
            var getCartResponse = checkoutContainerService.getCart(cartID);
            getCartResponse.then(function(response) {
            	if(response.status != 200 || response.data == undefined || response.data == null){
					var checkOutErrorToast = angular.element("#checkOutErrorToast").val();
					self.checkoutToastMessage(checkOutErrorToast);
                    self.isDisabled = false;
				}
				else{
                if (response.data.entries != undefined) {
                    var entries = response.data.entries;
                    var stockData = '';
                    var placeOrder = true;
                    
                    for (var index = 0; index < entries.length; index++) {
						var obj = self.bag.entries[index];
                        if (entries[index].hasOwnProperty("stockData")) {
                            stockData = entries[index].stockData.lineItemDetailsList;
                            for (var i = 0; i < stockData.length; i++) {
                                if (stockData[i].safetyStockStatus == "OUT_OF_STOCK" && stockData[i].hasOwnProperty("backOrdered")  && stockData[i].backOrdered == false) {
									obj["stockStatus"] = "OS";
                                    obj["stockDescription"] = outOfStockMessage;
									self.checkoutToastMessage(outOfStockMessage);
                                    placeOrder = false;
                                    break;
                                }
								if(entries[index].product.hasOwnProperty("wholeSaleStatus") && entries[index].product.wholeSaleStatus=="02"){
                                    
									obj["stockStatus"] = "OS";
                                    obj["stockDescription"] = outOfStockMessage;
									self.checkoutToastMessage(outOfStockMessage);
                                    placeOrder = false;
                                    break;
                            	}
                                if(entries[index].product.hasOwnProperty("wholeSaleStatus") && entries[index].product.wholeSaleStatus=="05"){
                                    
									obj["stockStatus"] = "OS";
                                    obj["stockDescription"] = outOfStockMessage;
									self.checkoutToastMessage(outOfStockMessage);
									placeOrder = false;
                                    break;
                            	}
								if(entries[index].product.hasOwnProperty("wholeSaleStatus") && entries[index].product.wholeSaleStatus=="09"){
									obj["stockStatus"] = "CS";
                                    obj["stockDescription"] = comingSoonMessage;
									self.checkoutToastMessage(comingSoonMessage);
									placeOrder = false;
                                    break;
                            	}
                            }
                        }
                        
                    }
                    if(placeOrder){
                        var placeOrder = orderCheckoutService.placeOrder(cartID,$rootScope.securityCode);
                        placeOrder.then(function(response) {
                        	if(response.status !=201 || response.data == undefined || response.data == null){
								var checkOutErrorToastMsg = angular.element("#checkOutErrorToast").val();
								self.checkoutToastMessage(checkOutErrorToastMsg);
                                self.isDisabled = false;
							}
                            if(response.data.hasOwnProperty("errors")){
                                var error = response.data.errors[0].message;
                                var reactCode;
                                if(error.indexOf(",")> -1){
                                	reactCode = (error.toString().split(",")[0]);
                                }
                                else{
                                	reactCode = error;
                                }
                                if (reactCode.indexOf("7") > -1 || reactCode.indexOf("5") > -1 || reactCode.indexOf("4") > -1 || reactCode.indexOf("3") > -1) {
                                        $rootScope.paymentError=invalidCvv;
                                        self.onEditIcon("payment");
                                    }
                                else if(error.indexOf("DA06") > -1){
                                    $rootScope.paymentError=insufficientBalance;
                                    self.onEditIcon("payment");
                                }
                                else if(reactCode.indexOf("6") > -1 ||reactCode.indexOf("2") > -1 ||reactCode.indexOf("1") > -1 ){
                                                $rootScope.paymentError=gatewayError;
                                                self.onEditIcon("payment");
                                            }}
                                else{
                                	$cookies.remove('cart_guid', {path:'/'});
                                	$cookies.remove('totalItems', {path:'/'});
                                    $rootScope.$emit("updateBagItems","");
                                	if(userProfileManager.isUserLoggedIn()) {
                                    window.location.href = pageName+'?orderId='+response.data.code;
                                	} else {
                                    window.location.href = pageName+'?orderId='+cartID;
                                }
                            }
                         
                        });
                    } else {
						self.checkoutToastMessage(outOfStockMessage);
                    }
                }}
            });
        } else {
            self.showError = true;
            self.termsConditionsErr = true;
        }
    }

    self.isAccepted = function(bool) {
    	self.isChecked=bool;
    }
	
	self.stockStatusCheck = function(value, item){
  		var i = item;
  		if(i == value){
    		return true;
 		 }
 		 else{
    		return false;
		}
	};

    self.getPdfPath = function(returnPolicyPdfPath) {
        window.open(returnPolicyPdfPath, '_blank');
    }

	this.onEditIcon = function(pageName) {
        $state.go(pageName,$rootScope.paymentError);
    };

    this.formatPhoneNumber=function(phoneNumber) {
        var format = (""+phoneNumber).replace(/\D/g, '');
        var formattedNumber = format.match(/^(\d{3})(\d{3})(\d{4})$/);
        return (!formattedNumber) ? null : "(" + formattedNumber[1] + ") " + formattedNumber[2] + "-" + formattedNumber[3];
    }   
    
    this.formatDate=function(backOrderdate) {
        var pattern = /(\d{4})\-(\d{2})\-(\d{2})/;
        if (window.location.href.indexOf("US") > -1){
            var formatUS = (""+backOrderdate).replace(pattern,'$2/$3/$1')
			return(formatUS);
        }
        else{
            var formatOther = (""+backOrderdate).replace(pattern,'$3/$2/$1')
			return(formatOther);
        }
    }
	
	self.checkoutToastMessage = function(errorMessage){
		var toastData = {
				"dangerType": true,
				"msg": errorMessage
			};
		$rootScope.$emit("showCheckoutToastMsg", toastData);
	}
	
}]);
}());

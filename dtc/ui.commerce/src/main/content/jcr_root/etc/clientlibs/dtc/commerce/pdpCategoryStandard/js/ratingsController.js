(function() {
"use strict";

pdpCategoryStandard.controller('RatingController', ['$uibModal', '$scope', '$http', 'pdpCategoryStandardService', function($uibModal, $scope, $http, pdpCategoryStandardService) {

    var self = this;
    var skuPath = angular.element('input[name=product-sku]').val();
    var sku = skuPath.split("/")[skuPath.split("/").length - 1];

    this.ratingValue = 0; // total number of ratings
    this.rating = 0; // prefill stars with avg rating
    this.max = this.max || 5;
    this.starType = this.starType || "big";
    self.ratings = "";

    var promise = pdpCategoryStandardService.getRatings(sku);
    promise
        .then(function(data) {
            self.ratings = data;
            if (data.streamInfo.commentCount > 0) {
                self.ratingValue = data.streamInfo.commentCount || 0;
            }
            self.updateStars(self);
        });



    this.updateStars = function(self) {
        self.starClass = "star-" + self.starType;
        self.starRating = self.getStarRatingPosition(self.ratingValue, self.starType);
    };

    this.getStarRatingPosition = function(ratingDecimalValue, starType) {
        var starTopPosition = 0;
        var starMargin = 0;
        var starWidth = 0;
        var halfStarTopMargin = 0,
            smallStarFirstPosition = 0;
        var halfStarPosition = -273;
        if (starType == "big") {
            starWidth = 18;
            halfStarPosition = -273;
            halfStarTopMargin = -26;

        } else {
            starWidth = 12;
            halfStarPosition = -242;
            halfStarTopMargin = -68;
            smallStarFirstPosition = -48;
        }
        var decimalValue = ratingDecimalValue.toString().replace(/^[^\.]+/, '0');
        var floorRatingValue = Math.floor(ratingDecimalValue);
        decimalValue = parseFloat(decimalValue);
        var mutipleValue = 0;
        if (0.26 <= decimalValue && decimalValue <= 0.74) {
            starMargin = halfStarTopMargin;
            floorRatingValue++;
        } else if (0.75 <= decimalValue && decimalValue <= 0.99) {
            starMargin = smallStarFirstPosition;
            floorRatingValue++;
        } else {
            starMargin = smallStarFirstPosition;
        }
        if (floorRatingValue >= this.max) {
            starMargin = smallStarFirstPosition;
            floorRatingValue = this.max;
        }
        floorRatingValue = floorRatingValue * starWidth;
        floorRatingValue = halfStarPosition + floorRatingValue
        return floorRatingValue + "px " + starMargin + "px";
    }

}]);
}());

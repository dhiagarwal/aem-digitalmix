(function() {
"use strict";


deliveryOptions.controller('deliveryOptionsController', ['$scope', 'checkoutContainerService', 'deliveryOptionsService', '$state', '$rootScope','$cookies', function($scope, checkoutContainerService, deliveryOptionsService, $state, $rootScope,$cookies) {

    var cartID = $cookies.get("cart_guid");

    window.scrollTo( 0, 0 );
    
    var self = this;
	this.saveAndContinueState = angular.element('input[id=payState]').val();
    this. goBackState = angular.element('input[id=shipState]').val();

    self.voucherApplied = [];

    self.model = {};

	var shoppingBagPath = angular.element('input[name=shoppingBag]').val();

    var errorDefault = angular.element('input[name=promoErrorCode]').val();

    $scope.errPromoCode;

    self.promoSectionVisible = false;
    

    if (cartID != undefined && cartID.length != 0) {
		self.disableDeliveryButton = true;
        var promise = deliveryOptionsService.getDeliveryOptions(cartID);

    	promise.then(function(response){
			self.disableDeliveryButton = false;
        	self.deliveryModes = response.deliveryModes;
			self.shippingType = response.deliveryModes[0].code;

                var getCartResponse = checkoutContainerService.getCart(cartID);
                getCartResponse.then(function(response) {
                      checkoutContainerService.getCartData = response.data;
                    $rootScope.$emit("refreshNavigation");

                    self.cartData = checkoutContainerService.getCartData;

            if(self.cartData.deliveryMode != undefined){
                self.shippingType = self.cartData.deliveryMode.code;
            }

            if (self.cartData.hasOwnProperty("appliedVouchers")) {
                    for (var i = 0; i < self.cartData.appliedVouchers.length; i++) {
                        self.voucherApplied.push(self.cartData.appliedVouchers[i].voucherCode);
                    }
                }

                if (self.cartData.hasOwnProperty("appliedVouchers") && self.cartData.totalDiscounts.value != 0) {
                    self.model.savings = true;
                }

                self.deliveryCost = "$0.00";
                if (self.cartData.hasOwnProperty("deliveryCost")) {
                    self.deliveryCost = self.cartData.deliveryCost.formattedValue;
                }

                self.taxesAvailable = false;
                if (self.cartData.hasOwnProperty("deliveryAddress") && self.cartData.deliveryAddress.hasOwnProperty("postalCode")) {
                    self.taxesAvailable = true;
                }
                });

		});

    }
	
    this.saveAndContinue = function(isValid, pageName) {
        if (isValid) {
            var promise_postDeliveryOptions = deliveryOptionsService
                .postDeliveryOptions(cartID, self.shippingType);
            promise_postDeliveryOptions.then(function(response) {
            	if(response.status != 200 || response.data == undefined || response.data == null || response.data.hasOwnProperty("errors")){
                    var toastData = {"dangerType": true,"msg": angular.element("#checkOutErrorToast").val()};
                    $rootScope.$emit("showCheckoutToastMsg", toastData);}
                else{
                    var disabletoastData = {"dangerType": false,"msg": angular.element("#checkOutErrorToast").val()};
                    $rootScope.$emit("showCheckoutToastMsg", disabletoastData);
                    self.goToPage(pageName);}    
            }, function(reason) {
                console.log(reason);
            });
        }
    };

    this.goToPage = function(pageName) {
        $state.go(pageName);
    };

}]);
}());

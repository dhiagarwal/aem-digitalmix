
(function() {
"use strict";

productSearchList.service('productSearchListService', ['$http', '$q','userProfileManager','serviceURL', 'solrQueryService', function($http, $q,userProfileManager,serviceURL, solrQueryService) {

    
	this.getProductData = function(searchText,start) {
        var MAX_ROW_TO_RETURN = 10;
        var locale = LocaleUtil.getLocaleFromPath(window.location.href);
        
        return solrQueryService.getProductsAndBasesBySearchTerm(searchText, locale.getLanguage(), locale.getCountry(), MAX_ROW_TO_RETURN, start);
    };

    var cartServiceBaseSite = "cosmetics";
    var user_id;
    var endPoint;
    if(userProfileManager.isUserLoggedIn()) {
        user_id = userProfileManager.getUserProfileMasterInfo().userId;
        endPoint = mulesoftHybrisEndpoint;
    } else {
        user_id = "anonymous";
        endPoint = mulesoftHybrisGuestEndpoint;
    }
    
    this.getPriceData = function(id) {
        var deferred = $q.defer();
        var paramsArray = [];
		paramsArray[0] = endPoint;	
		paramsArray[1] = cartServiceBaseSite;
		paramsArray[2] = user_id;
		paramsArray[3] = id;
		var url = serviceURL.GetServiceURL('productsPriceURL' , paramsArray);
		var priceRequest = {
				method: 'POST',
				url: url,
				headers : { 'Content-Type' : 'application/json' }
		};
        $http(priceRequest).then(function(response) {
            deferred.resolve(response.data);
        },function (response){ });
        return deferred.promise;        
    };

	
	 this.getRatings = function(sku) {
        var deferred = $q.defer();
        var paramsArray = [];
		paramsArray[0] = sku;			
		var url = serviceURL.GetServiceURL('ratingsURL' , paramsArray);        
        $http
        .jsonp(url)
        .then(function(response) {
            deferred.resolve(response.data);
        });
        return deferred.promise;
    };
    

    
    this.getImageData = function(skuIds){
        var deferred = $q.defer(); 
        var paramsArray = [];
		paramsArray[0] = skuIds;
		paramsArray[1]= window.location.pathname;
		var url = serviceURL.GetServiceURL('getImageDataPathParamURL' , paramsArray);    
        $http({
            method: 'GET',
            url: url,
            headers : { 'Content-Type' : 'application/json' }
        }).then(function successCallback(response) {
            deferred.resolve(response.data);
        },function (response){ 
            
        });
        return deferred.promise;
    };
    
}]);
}());
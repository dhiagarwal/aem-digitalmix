(function() {
    "use strict";

    pitchHeaderModule.controller('toastMsgController', ['$rootScope', function($rootScope) {

        var self = this;
        self.dangerType = false;
        self.errorMessage = "";
        self.successType = false;
        var scroll_top_duration = 700;

        //Check for browser's compatibility with session storage
        try {
            sessionStorage.testBrowser = "Safari";
            localStorage.testBrowser = "Safari";
        } catch (error) {
            var errorMessage = angular.element($('#browsererror')).val();
            self.dangerType = true;
            self.errorMessage = errorMessage;
        }

        $rootScope.$on("showToastMsg", function(event, data){
            self.dangerType = data.dangerType;
            self.errorMessage = data.msg;
            angular.element('html,body').animate({
                    scrollTop: 0
                }, scroll_top_duration
            );
        });

        $rootScope.$on("hideToastMsg", function(event){
            self.hideToastMessage();
        });

        self.hideToastMessage = function(){
            self.dangerType = false;
        };

    }]);
}());

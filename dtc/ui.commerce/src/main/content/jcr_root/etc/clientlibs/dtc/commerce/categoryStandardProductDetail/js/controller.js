(function() {
"use strict";

categoryLandingPage.controller('personalCareCategoryController', ['$rootScope', 'personalCareCategoryService', '$scope', 'userProfileManager', function($rootScope, personalCareCategoryService,$scope,userProfileManager) {
  	var self = this;
	var prodPath = angular.element('input[name=productsPath]').val();
    var categoryRendition = angular.element('input[name=categoryRendition]').val();
    self.productDataArr=[];

	self.hidePSV = true;
	$scope.psvEnabled = self.hidePSV;

    var userType;
    if(userProfileManager.isUserLoggedIn()) {
        var userTypes = userProfileManager.getUserProfileMasterInfo().userType.toLowerCase().split("_");
        if(userTypes.length > 1) {
            userType = userTypes[userTypes.length-1];
        } else {
            userType = userTypes[0];
        }
        
        if(userType == "distributor"){
            self.hidePSV = false;
			$scope.psvEnabled = self.hidePSV;
        }
    }

	var count = personalCareCategoryService.getSearchTextCntr();
	var searchText = angular.element(angular.element('input[name=searchText]')[count]).val();

    // The searchText is coming back like: "dtc:categories/bodycare". It should be just "bodycare" so we need to fix this:
    searchText = searchText.replace('dtc:categories/', '');
	
	var locale = LocaleUtil.getLocaleFromPath(window.location.href);
	
	personalCareCategoryService.setSearchTextCntr(count+1);
    var productData = personalCareCategoryService.getProductData(searchText);
    productData.then(function(data){

        self.productDataArr = data;
        
        var skuID = [];
        
        var newId = '';

        for(var i=0;i<self.productDataArr.length;i++){
			
			var productLanguageDocument = self.productDataArr[i].getLanguageChild(locale.getLanguage());
			self.productDataArr[i].product_name = productLanguageDocument.getTitle();
            self.productDataArr[i].product_sku = self.productDataArr[i].getSku().toUpperCase();
            
            self.productDataArr[i].prodUrl = prodPath+"."+ self.productDataArr[i].getSku()  + ".html";

            skuID[i] = self.productDataArr[i].getSku().toUpperCase();
        }
		
        newId = skuID.join(",");
        var imageDataResponse = personalCareCategoryService.getImageData(newId);
        imageDataResponse.then(function(data) {
            if(data != null){
                for(var j=0;j< data.length;j++){
                    var obj = self.productDataArr[j];
                    if(data[j].sku === self.productDataArr[j].getSku().toUpperCase()){
                        obj["imagePath"] = data[j].path + '/jcr:content/renditions/' + categoryRendition;
                    }
                }
            }
        });

        self.productDetails = personalCareCategoryService.getPriceData(newId);

        $rootScope.searchCountResult = $rootScope.searchCountResult+self.productDataArr.length;
	})
}]);
}());
var Locale = function(language, country, variant){
    var lang = language;
    var cntry = country;
    var varnt = variant;

    this.getLanguage = function(){
        return lang;
    };
    this.getCountry = function(){
        return cntry;
    };
    this.getVariant = function(){
        return varnt;
    };
};
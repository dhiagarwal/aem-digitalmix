(function() {
"use strict";

productSearchList
		.controller(
				'PopoverController',
				['$uibModal', '$scope', '$http', 'productSearchListService', function($uibModal, $scope, $http, productSearchListService) {
					var self = this;

					var sku = $scope.product.product_variantRatings;

					this.ratingValue = 0; // total number of ratings
					this.ratingStars = 0;
					this.recommendedCount = 0; // number of recommendations
					this.ratingPercentage = "0%";
					this.ratingsDetails = {};
					this.rating = 0; // prefill stars with avg rating
					this.maxStar = 5;
                    self.ratingDataArr = [];
                    this.max = this.max || 5;
            		this.starType = "small";

					var url = window.location.toString();
					var uid = url.substring(url.lastIndexOf("uid=") + 4,
							url.length);

					var promise = productSearchListService.getRatings(sku);
					promise
							.then(function(data) {
                                
                                self.ratingDataArr = data;
								if (data.hasOwnProperty('streamInfo') && data.streamInfo.hasOwnProperty('commentCount') && data.streamInfo.commentCount > 0) {
									self.rating = Math
											.round(data.streamInfo.avgRatings._overall);// Math.round((self.recommendedCount*5)/self.ratingValue);

								}
								self.updateStars(self);
							});


					this.updateStars = function(self) {
                        self.starClass = "star-" + self.starType;
                        self.starRating = self.getStarRatingPosition(self.rating, self.starType);
                     };

					 this.getStarRatingPosition = function(ratingDecimalValue, starType) {
                var starTopPosition = 0;
                var starMargin = 0;
                var starWidth = 0;
                var halfStarTopMargin = 0,
                    smallStarFirstPosition = 0;
                var halfStarPosition = -273;
                if (starType == "big") {
                    starWidth = 18;
                    halfStarPosition = -273;
                    halfStarTopMargin = -26;

                } else {
                    starWidth = 12;
                    halfStarPosition = -242;
                    halfStarTopMargin = -64;
                    smallStarFirstPosition = -44;
                }
                var decimalValue = ratingDecimalValue.toString().replace(/^[^\.]+/, '0');
                var floorRatingValue = Math.floor(ratingDecimalValue);
                decimalValue = parseFloat(decimalValue);
                var mutipleValue = 0;
                if (0.26 <= decimalValue && decimalValue <= 0.74) {
                    starMargin = halfStarTopMargin;
                    floorRatingValue++;
                } else if (0.75 <= decimalValue && decimalValue <= 0.99) {
                    starMargin = smallStarFirstPosition;
                    floorRatingValue++;
                } else {
                    starMargin = smallStarFirstPosition;
                }
                if (floorRatingValue >= this.max) {
                    starMargin = smallStarFirstPosition;
                    floorRatingValue = this.max;
                }
                floorRatingValue = floorRatingValue * starWidth;
                floorRatingValue = halfStarPosition + floorRatingValue
                return floorRatingValue + "px " + starMargin + "px";
            }


}]);
}());

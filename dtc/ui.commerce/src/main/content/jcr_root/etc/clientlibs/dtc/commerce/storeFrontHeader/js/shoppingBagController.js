(function() {
"use strict";

storefrontHeader.controller('shoppingBagController', ['storefrontHeaderService', '$timeout', '$rootScope', 'userProfileManager','$cookies','analyticsService', function(storefrontHeaderService, $timeout, $rootScope, userProfileManager,$cookies,analyticsService) {

    var self = this;
    var setDuration = 3000;
    var CART_GUID;
    var params;
    self.hidePSV = true;
    
    self.checkLoginStatus = function(){
        var userType;
        if(userProfileManager.isUserLoggedIn()) {
            var userTypes = userProfileManager.getUserProfileMasterInfo().userType.toLowerCase().split("_");
            if(userTypes.length > 1) {
                userType = userTypes[userTypes.length-1];
            } else {
                userType = userTypes[0];
            }
            
            if(userType == "distributor"){
                self.hidePSV = false;
            }
        }else {
            self.hidePSV = true;
        }
    }
    
    $rootScope.$on("showShoppingBag", function(event, data){
        if($rootScope.emitShowShoppingBag && data.orderList.length > 0){
            self.itemsAdded = data.orderList.length;
            params = data;
            self.getShoppingCartModalBox();    
            $rootScope.emitShowShoppingBag = false;
        }
    });
    
    self.getShoppingCartModalBox = function() { 
        
        CART_GUID = $cookies.get("cart_guid");
        
        if(CART_GUID == "" || CART_GUID == undefined){
            self.getcartGUID();
        }else{            
            self.addToCart();                    
        }
 
    }


    self.getcartGUID = function() {
        var cartResponse = storefrontHeaderService.createCart();
        var addToCartErrorMsg =  $('#addToCartErrorMessage').val();
        var toastData = {"dangerType" : true ,"msg" : addToCartErrorMsg};
		var expDate = new Date();
        expDate.setDate(expDate.getDate() + 1);
        cartResponse.then(function(response) {
            var user_id;
            var status = response.status;
			if(status == 400 || status == 401 || response.data == null || response.data ==""){
				$rootScope.$emit("showToastMsg", toastData);
				$rootScope.$emit("disableAddtoCart");	
			}
            else{
            if(userProfileManager.isUserLoggedIn()) {
               user_id = userProfileManager.getUserProfileMasterInfo().userId;
               $cookies.put('cart_guid',response.data.code,{path:'/'});
            } else {
                user_id = "anonymous";
                $cookies.put('cart_guid',response.data.guid,{expires:expDate,path:'/'});
            }
            
            var GUID = $cookies.get("cart_guid");
            if(GUID != "" && GUID != undefined){
                self.addToCart();  
            }
        }
        });
        
    }
    
    self.addToCart = function() {
        CART_GUID = $cookies.get("cart_guid");
        var addToCartErrorMsg =  $('#addToCartErrorMessage').val();
        var errorData = { "dangerType" : true , "msg" : addToCartErrorMsg };
		
		var outOfStockErrorMsg =  $('#toastOSMsg').val();
		var toastData = { "dangerType" : true ,	"msg" : outOfStockErrorMsg };
		
        var addToCartResponse = storefrontHeaderService.addProductToCart(CART_GUID,params);
        addToCartResponse.then(function(response) {
			if(response.status == 200 && response.data.hasOwnProperty("cartModifications") &&  response.data.cartModifications[0].statusCode == 'OUT_OF_STOCK'){
				
				$rootScope.$emit("showToastMsg", toastData);
				$rootScope.$emit("disableAddtoCart");
				
            } else if(response.status == 200 && response.data.hasOwnProperty("cartModifications") &&  response.data.cartModifications[0].statusCode == 'success'){
            
				var entries = response.data.cartModifications;
				var stockLevelStatus = "";
				var backOrdered = false;
				if(entries){
					for(var i=0; i < params.orderList.length; i++){
						for (var index = 0; index < entries.length; index++) {
							if(entries[index].hasOwnProperty("entry") && entries[index].entry.hasOwnProperty("stockData")){
								if(params.orderList[i].code == entries[index].entry.product.code){
									if(entries[index].entry.stockData.lineItemDetailsList[0].hasOwnProperty("safetyStockStatus")){
										stockLevelStatus = entries[index].entry.stockData.lineItemDetailsList[0].safetyStockStatus;
										backOrdered = entries[index].entry.stockData.lineItemDetailsList[0].backOrdered;
										break;
									}
								}
							}
						}
					}
					if(stockLevelStatus == "OUT_OF_STOCK" && backOrdered == false){
						$rootScope.$emit("showToastMsg", toastData);
						$rootScope.$emit("disableAddtoCart");
					} else {
						
						if(response.data.totalItems){
							self.totalPrice = response.data.totalPrice.formattedValue;
							self.totalItems = response.data.totalItems; 
							self.totalPsv = response.data.totalPvValue;
							self.checkLoginStatus();
							self.updateCartEntries(response.data.totalItems);
							$("#shoppingCartModal").modal();

						   $timeout(function() {
								$("#shoppingCartModal").modal("hide");
							}, setDuration);						
						}
					}
					// For Analytics
					analyticsService.updateProductStringOnAddToBag(response.data);
					
				}
			} else {
				$rootScope.$emit("showToastMsg", errorData);
                $rootScope.$emit("disableAddtoCart");
			}
        });
    }
	
	self.updateCartEntries = function(value){    
        if($cookies.get('totalItems') == null){
            $cookies.put('totalItems',value,{path:'/'});
        }
        else{
            $cookies.put('totalItems',value,{path:'/'});
        }
        self.totalBagItems =  $cookies.get("totalItems");
        $rootScope.$emit("updateBagItems",self.totalBagItems);     
    }

    
}]);
}());

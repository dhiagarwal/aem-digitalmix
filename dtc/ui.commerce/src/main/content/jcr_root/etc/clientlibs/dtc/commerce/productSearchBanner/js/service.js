(function() {
"use strict";

productSearchBanner.service('productSearchBannerService', ['$http', '$q','serviceURL', function($http, $q,serviceURL) {
    
	this.getProductData = function(searchText) {       
        var deferred = $q.defer();
        var paramsArray = [];
		paramsArray[0] = searchText;		
		var url = serviceURL.GetServiceURL('solrProductDataURL' , paramsArray);
        $http({
            method : 'GET',
            url : url,
            params : {
                defType : 'edismax',
                indent : 'on',
                qf : 'products',
                wt : 'json'
            }
        }).success(function(data) {
            deferred.resolve(data);
        }).error(function() {
            console.log('Search failed! in productSearchBanner service');
        });
        return deferred.promise;
    }


}]);
}());
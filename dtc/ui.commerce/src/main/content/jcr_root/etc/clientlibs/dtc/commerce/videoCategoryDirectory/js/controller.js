(function() {
"use strict";

videoCategoryDirectory.controller('videoCategoryDirectoryController', ['$scope', 'videoCategoryDirectoryService', '$timeout', '$interval', function($scope,videoCategoryDirectoryService,$timeout,$interval) {

  var self = this;
  self.videoPlaying = false;
  self.togglePlay = function() {
    self.videoPlaying = true;
      var video = angular.element.find('#video-category-directory');
      if (video[0].paused) {
          $timeout(function(){video[0].play();},10)
          video[0].controls=true;
          self.videoPlaying = true;
      } else {
          $timeout(function(){video[0].pause();},10)
          video[0].controls=false;
          self.videoPlaying = false;
      }
  };

}]);
}());

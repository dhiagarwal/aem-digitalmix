(function() {
"use strict";

dsFeaturedBundles.controller('dsFeaturedBundlesController', ['$scope', 'dsFeaturedBundlesService','$rootScope','$interval','userProfileManager','$q', function($scope,
   dsFeaturedBundlesService,$rootScope,$interval,userProfileManager,$q) {
    var self = this; 
	self.locale = userProfileManager.getPageLocaleVal(); 
    
    self.initFeaturedBundles = function(userId) {

        self.productSku = [];        
        var subdomainResponse  = dsFeaturedBundlesService.getSubDomain(userId);
        subdomainResponse.then(function(subdomain) {

            var firebaseData = dsFeaturedBundlesService.getFireBaseData(userId,subdomain,self.locale);
            firebaseData.then(function(response) {

            if(response.showPromotionsBundles) {
                if(response.showDefaultBundles) {                    
                    var defaultData	= dsFeaturedBundlesService.getDefaultFeaturedBundles(self.locale.market,self.locale.language);
                    defaultData.then(function(data) {
                   
                        if(data.hasOwnProperty('bundles')){
                            var customData = [];
                            var skus = [];                           
                            for (var i=0; i < data.bundles.length; i++) {                                
                                skus[i] = data.bundles[i].sku;
                                customData.push({ 
                                    img : data.bundles[i].path,
                                    name : data.bundles[i].title,
                                    skuid : data.bundles[i].sku
                                });
                            }
                            $scope.recos = customData;
                            self.productSku = skus;
                            self.getFeaturedBundlesPrice(self.productSku);
                            
                        }
                    });
                    
                } else {
                    
                    if(response.bundles.length > 0){
                    	self.bundleMessage = (response.customBundlesMessage && response.customBundlesMessage!= "") ? response.customBundlesMessage: '';
                    	
                        self.productSku = response.bundles;
                        var imageDataResponse	= dsFeaturedBundlesService.getImageData(self.productSku);
                        imageDataResponse.then(function(imageData) {
                        var customData = [];                           
                            
                            for (var i=0; i < self.productSku.length; i++) {
                                
                                var imagePath = '';
                                var title;
                                var url;
                                var baseId = '';
                                
                                if (imageData && imageData.length > 0 && imageData[i]) {
                                    if (imageData[i].hasOwnProperty('sku') && imageData[i].sku === self.productSku[i]) {
                                        if (imageData[i].hasOwnProperty('path')){
                                            imagePath = imageData[i].path + '/jcr:content/renditions/cq5dam.thumbnail.320.310.png';
                                        }
                                        if(imageData[i].hasOwnProperty('title')){
                                            title = imageData[i].title;
                                        }
                                    }                                 
                                  
                                }                                
                                customData.push({ 
                                    img : imagePath,
                                    name : title,
                                    skuid : self.productSku[i],

                                });
                            }
                            $scope.recos = customData;
                        });
                        
                        self.getFeaturedBundlesPrice(self.productSku);                                                 
                    }
                }
            }            
            });        
        });        
    };
    
    self.getFeaturedBundlesPrice = function(id) {
        var promise = dsFeaturedBundlesService.getPriceData(id);
        promise.then(function(data) {
            var priceData = data.priceList;
            for (var i = 0; i < $scope.recos.length; i++) {
                var obj = $scope.recos[i];             
                if(priceData[i].code == obj.skuid){	                                    
	               if (priceData[i].hasOwnProperty("stockData")) {	                        
	                   if (priceData[i].stockData.hasOwnProperty("lineItemDetailsList") && (priceData[i].stockData.lineItemDetailsList[0].hasOwnProperty("safetyStockStatus"))) {
	                        var stockStatusData = priceData[i].stockData.lineItemDetailsList[0];
	                            
	                        if (stockStatusData.safetyStockStatus == "LOW_STOCK") {
	                        	obj["status"] = "LS";
	                        	obj["message"] = "Only "+stockStatusData.totalAvailableQuantity+ " left";
	                        } else if (stockStatusData.safetyStockStatus == "OUT_OF_STOCK") {
	                        	obj["status"] = "OS";
	                        } else if((stockStatusData.safetyStockStatus == "OUT_OF_STOCK" || stockStatusData.safetyStockStatus == "LOW_STOCK") && stockStatusData.hasOwnProperty("backOrdered")){
                            	if(stockStatusData.backOrdered){
                               		obj["status"] = "BO";
                                    var dateString = stockStatusData.backOrderedDate;
                                    obj["message"] = "Avaliable on "+self.formatDate(dateString);                                    
                                }
                            }
	                    }
	               }
	                    
	               if (priceData[i].hasOwnProperty("wholeSaleStatus")) {
	                    if(priceData[i].wholeSaleStatus == "02"){
                            obj["status"] = "OS";
	                    }
	                    if(priceData[i].wholeSaleStatus == "05"){
                            obj["status"] = "OS";
	                    } 	                  
	                  
	                }
	                angular.element('.availability-indicator-'+priceData[i].code+'-'+obj["status"]).removeClass('ng-hide');     
                    if (priceData[i].hasOwnProperty("price") && $scope.recos[i].skuid == priceData[i].code) {
                        obj["price"] = priceData[i].price.value;
                    } else {
                        obj["price"] = "";
                    }
                }
            }
            self.getToatlPriceOfBundle($scope.recos);
			            
        });        
    };    

    self.getToatlPriceOfBundle = function(bundleData) {
      self.totalCost = 0;
      if(bundleData != null && bundleData.length > 0) {
        for(var i = 0; i < bundleData.length; i++) {
          self.totalCost = parseFloat(self.totalCost) + parseFloat(bundleData[i].price);
        }
      }
  		return self.totalCost;
    }; 

    self.formatDate = function (backOrderdate) {
        var pattern = /(\d{4})\-(\d{2})\-(\d{2})/;
        if (window.location.href.indexOf("US") > -1){
            var formatUS = (""+backOrderdate).replace(pattern,'$2/$3/$1')
			return(formatUS);
        }
        else{
            var formatOther = (""+backOrderdate).replace(pattern,'$3/$2/$1')
			return(formatOther);
        }
    }

    self.addProductToCart = function(bundleData) {
        var errorStatus = false;
        for(var i= 0; i<bundleData.length; i++) {
           if(bundleData[i].status == "OS") {
               errorStatus = true;
               break;
		    }
        }
        if(errorStatus) {
        	var errorMessage = angular.element('input[name=errorMessage]').val();
            self.showToastMessage(errorMessage);
        }else {
            var productData = {};
            productData.orderList = [];
            var productQty = 1;
			for(var i= 0; i<bundleData.length; i++) {
			   var orderList = {};
               orderList.code = bundleData[i].skuid;
               orderList.qty = productQty;
               productData.orderList.push(orderList);

            }
            if(productQty >0) {
               $rootScope.emitShowShoppingBag = true;
	           $rootScope.$emit("showShoppingBag", productData);
            }
        }
     };
     
     self.showToastMessage = function(erorMessage){
  		var toastData = {
                       "dangerType": true,
                       "msg": erorMessage
                   };
          $rootScope.$emit("showToastMsg", toastData);
  	};

    function getParameterByName(name) {
	    var url = window.location.href;
	    name = name.replace(/[\[\]]/g, "\\$&");
	    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
	        results = regex.exec(url);
	    if (!results) return null;
	    if (!results[2]) return '';
	    return decodeURIComponent(results[2].replace(/\+/g, " "));
	} 

    function init() {
    	var storeId = getParameterByName('storeId'); 
    	if(storeId){
    		self.initFeaturedBundles(storeId);
    	}
    }
    init();
}]);
}());

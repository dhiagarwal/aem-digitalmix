(function() {
    "use strict";
    
    viewAllProducts.controller('viewAllProductsController', ['$rootScope', 'viewAllProductsService', '$scope', 'userProfileManager', function($rootScope, viewAllProductsService,$scope,userProfileManager) {
        
        var self = this;
        var prodPath = angular.element('input[name=productsPath]').val();
        var count = viewAllProductsService.getSearchTextCntr();
        var searchText = angular.element('input[name=searchText]').val();
        viewAllProductsService.setSearchTextCntr(count);
        var searchPath = searchText;
        searchPath = searchPath.replace(":","/");
        var productJsonResponse = viewAllProductsService.getsubCategories(searchPath);
        productJsonResponse.then(function(data){
            self.subCategoriesList = data;
        });
        
        var locale = LocaleUtil.getLocaleFromPath(window.location.href);
		// The searchText is coming back like: "dtc:categories/Personalcare". It should be just "Personal Care" so we need to fix this:
        searchText = searchText.replace('dtc:categories/', '');
        var productData = viewAllProductsService.getProductData(searchText);
        productData.then(function(data){
            var productsList = data;
            var categoriesData = [];
            var categories = [];
            var data=[];
            $scope.products;
            for(var k =0 ; k < productsList.length; k++){
                var productLanguageDocument = productsList[k].getLanguageChild(locale.getLanguage());
                var productTitle = '';
                var prodSkuid = '';

                productTitle = productLanguageDocument.getTitle();
                prodSkuid =productLanguageDocument.getSku();
				if( productTitle && prodSkuid ){
					for( var index = 0; index < productLanguageDocument.getCategories().length; index++){
						var categoriesList = {};
						var productCategoryName = productLanguageDocument.getCategories(index);
						categoriesList.category = productCategoryName;
						categoriesList.productname = productTitle;
						categoriesList.skuid = prodSkuid;
						categoriesData.push(categoriesList);
					}
                }
            }
            
            if(categoriesData.length >0){
                for(var i=0 ; i<categoriesData.length;i++){
                    for(var h=0 ; h<categoriesData[i].category.length;h++){
                        var categoryName = categoriesData[i].category[h].substring(categoriesData[i].category[h].lastIndexOf('/')+1,categoriesData[i].category[h].length);

                        var categoryNameFormatted='';
                        if(self.subCategoriesList.hasOwnProperty(categoryName)){
                            categoryNameFormatted = self.subCategoriesList[categoryName]['jcr:title'];
                            
                            if(categories.indexOf(categoryNameFormatted)==-1){
                                
                                categories.push(categoryNameFormatted);
                                var categoryObj = {};
                                
                                categoryObj.category = categoryNameFormatted;
                                var productsObj = {};
                                productsObj.name  = categoriesData[i].productname;
                                productsObj.skuId = categoriesData[i].skuid;
                                productsObj.url = prodPath+"."+categoriesData[i].skuid+".html";
                                categoryObj.products = [];
                                categoryObj.products.push(productsObj);
                                categoryObj.count = 1;
                                data.push(categoryObj);
                            }
                            else{
                                for(var j=0; j<data.length;j++){
                                    if(data[j].category==categoryNameFormatted){
                                        var found = false;
                                        for(k in data[j].products){
                                            if(data[j].products[k].name == categoriesData[i].productname)
                                                found = true;
                                        }
                                        if(!found){
                                            var productsObj = {};
                                            productsObj.name  = categoriesData[i].productname;
                                            productsObj.skuId = categoriesData[i].skuid;
                                            productsObj.url = prodPath+"."+categoriesData[i].skuid+".html";
                                            data[j].products.push(productsObj);
                                            data[j].count = data[j].count+1;
                                        }                
                                    }
                                }
                            }
                        }
                    }
                    $scope.products = data;
                }  
            }
            
        })
    }]);
}());

(function() {
"use strict";

productSupplementComponent.controller('pdpProductSupplementController', ['$rootScope', '$scope', '$timeout', function($rootScope,$scope,$timeout) {
    var self = this;
    $rootScope.varSelected = false;
	var labelValue = angular.element('input[name=showKeyIngLabel]').val();
	if(labelValue != undefined || labelValue != null){
        if(labelValue.length > 0 || self.showLabel == true){
            self.showLabel = true;
        }else{
            self.showLabel = false;
        }
	}
	$rootScope.loadSupplementTable = function(variantJson){
	$rootScope.varSelected = true;
    self.variantJson = variantJson;

    };
}]);
}());

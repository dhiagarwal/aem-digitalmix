(function() {
"use strict";

categoryLandingPage.controller('priceDetailsController', ['$scope', 'personalCareCategoryService', '$rootScope', function($scope, personalCareCategoryService,$rootScope) {
  var self = this;


  	self.productDetails = $scope.$parent.$parent.perCareCtrl.productDetails;
  	self.productArray = $scope.$parent.$parent.perCareCtrl.productDataArr;
   
   
    self.productDetails.then(function(data){
    	for(var j=0; j< self.productArray.length; j++){
		self.price = '';
        self.minPvv = '';
        self.maxPvv = '';
        $scope.seperator = " - ";
		self.showPriceRange = true;
		self.stockStatus ="";
	    self.product_Arr =[];
	    self.showPsvLabel = false;
		
	    for(var z=0; z< data.priceList.length; z++){
            self.productValue = data.priceList[z];
            if(self.productArray[j].getSku().toUpperCase() == data.priceList[z].code ){
				if (!self.productValue.hasOwnProperty("priceRange")) {
		            if (self.productValue.hasOwnProperty("stockData")) {
		                var stockStatusData = self.productValue.stockData.lineItemDetailsList;
		                for (var i = 0; i < stockStatusData.length; i++) {
		                    if (stockStatusData[i].hasOwnProperty("safetyStockStatus")) {
		                        if (stockStatusData[i].safetyStockStatus == "LOW_STOCK") {
		                            self.stockStatus = "LS";
		                        } else if (stockStatusData[i].safetyStockStatus == "OUT_OF_STOCK") {
		                            self.stockStatus = "OS";
		                        } else if (stockStatusData[i].safetyStockStatus == "NORMAL") {
		                            self.stockStatus = "";
		                        } 
								if(stockStatusData[i].safetyStockStatus == "OUT_OF_STOCK" && stockStatusData[i].backOrdered){
		                            self.stockStatus = "BO";
		                        }
								
		                    }
		                }
		            }
					if(self.productValue.hasOwnProperty("wholeSaleStatus")){
						if(self.productValue.wholeSaleStatus == "02"){
		                 self.showPriceRange = false;
						 self.stockStatus = "OS";
						 self.showPsvLabel = true;
						}
						if(self.productValue.wholeSaleStatus == "05"){
						 self.stockStatus = "OS";
		                 self.showPriceRange = true;
						 }
						}
					}
		       if(self.showPriceRange){
				
				if(self.productValue.hasOwnProperty("priceRange")){
		            if((self.productValue.priceRange.maxPrice.value > self.productValue.priceRange.minPrice.value)
		                   &&(self.productValue.priceRange.maxPv.value == self.productValue.priceRange.minPv.value)) {
		
						self.price = self.productValue.priceRange.minPrice.formattedValue + " - " + self.productValue.priceRange.maxPrice.formattedValue;
		                self.minPvv = self.productValue.priceRange.minPv.value;
		                self.maxPvv = NaN;
		                if($scope.psvEnabled){
		                	self.showSeperator = false;
		                }
		                else{
		                	self.showSeperator = false;
		                }
		        	}
		
		            if((self.productValue.priceRange.maxPv.value > self.productValue.priceRange.minPv.value)
		                   &&(self.productValue.priceRange.maxPrice.value == self.productValue.priceRange.minPrice.value)) {
						self.minPvv = self.productValue.priceRange.minPv.value;
		                self.maxPvv = self.productValue.priceRange.maxPv.value;
		                self.price = self.productValue.priceRange.minPrice.formattedValue;
		                if($scope.psvEnabled){
		                	self.showSeperator = false;
		                }
		                else{
		                	self.showSeperator = true;
		                }
		
		        	}
		
		            if((self.productValue.priceRange.maxPv.value > self.productValue.priceRange.minPv.value)
		                   &&(self.productValue.priceRange.maxPrice.value > self.productValue.priceRange.minPrice.value)) {
		
		                self.price = self.productValue.priceRange.minPrice.formattedValue + " - " + self.productValue.priceRange.maxPrice.formattedValue;
		                self.minPvv = self.productValue.priceRange.minPv.value;
		                self.maxPvv = self.productValue.priceRange.maxPv.value;
		                if($scope.psvEnabled){
		                	self.showSeperator = false;
		                }
		                else{
		                	self.showSeperator = true;
		                }
		        	}
		
		            if((self.productValue.priceRange.maxPv.value == self.productValue.priceRange.minPv.value)
		                   &&(self.productValue.priceRange.maxPrice.value == self.productValue.priceRange.minPrice.value)) {
		            	self.price = self.productValue.priceRange.minPrice.formattedValue ;
		                self.minPvv = self.productValue.priceRange.minPv.value;
		                self.maxPvv = NaN;
		                if($scope.psvEnabled){
		                	self.showSeperator = false;
		                }
		                else{
		                	self.showSeperator = false;
		                }
		        	}
		      	}
		        else{
		            if(self.productValue.hasOwnProperty("price")){
		                self.price = self.productValue.price.formattedValue ;
		            }
		            else{
						self.price = '';
		            }
		            if(self.productValue.hasOwnProperty("pvValue")){
						self.minPvv = self.productValue.pvValue.value;
		                self.maxPvv = NaN;
		                self.showSeperator = false;
		            }
		            else{
						self.minPvv = NaN;
		                self.maxPvv = NaN;
		                self.showSeperator = false;
		            }
		
		
				}
			  }
			  else{
					self.minPvv = NaN;
		            self.maxPvv = NaN;
		            self.showSeperator = false;
				}
		       self.productArray[j].priceValue = self.price;
		       self.productArray[j].minPvvValue = self.minPvv;
		       self.productArray[j].maxPvvValue = self.maxPvv;
		       self.productArray[j].stock_Status = self.stockStatus;
		       self.productArray[j].seperator = self.showSeperator;
		       self.productArray[j].psvLabel = self.showPsvLabel;
		    }
			    }
		    	}
		    });
	    self.stockCheck = function(value, stockStatus){
	
	        if(stockStatus == value){
	            return true;
	        }
	        else{
	        return false;
	        }
	    };
	}]);
}());
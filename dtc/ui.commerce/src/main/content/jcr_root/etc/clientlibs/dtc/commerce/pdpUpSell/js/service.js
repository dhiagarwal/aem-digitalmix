
(function() {
"use strict";

upsellComponent.service('pdpUpsellComponentService', ['$http', '$q','userProfileManager','serviceURL', function($http, $q,userProfileManager,serviceURL) {


	var cartServiceBaseSite = "cosmetics";
    var user_id;
    var endPoint;
    if(userProfileManager.isUserLoggedIn()) {
        user_id = userProfileManager.getUserProfileMasterInfo().userId;
        endPoint = mulesoftHybrisEndpoint;
    } else {
        user_id = "anonymous";
        endPoint = mulesoftHybrisGuestEndpoint;
    }
    
	this.getMultiplePriceData = function(productCurrent, productComplementary) {
		var deferred = $q.defer();
		var paramsArray = [];
		paramsArray[0] = endPoint;
		paramsArray[1] = cartServiceBaseSite;
		paramsArray[2] = user_id;
		paramsArray[3] = productCurrent + ',' + productComplementary + '';
		var url = serviceURL.GetServiceURL('productsPriceURL' , paramsArray);

		$http.post(url)
				.then(function(response) {
					deferred.resolve(response.data);
				});
		return deferred.promise;
	}

    this.getUpSellProducts = function(sku, currentPath) {
        var deferred = $q.defer();
        var paramsArray = [];
		paramsArray[0] = sku;
		paramsArray[1] = currentPath;
		var url = serviceURL.GetServiceURL('getUpSellProductsURL' , paramsArray);
        $http({
            method: 'GET',
            url: url,
        }).then(function successCallback(response) {
            deferred.resolve(response.data);
        }, function(response) {
            console.log("")
        });
        return deferred.promise;
    }
}]);
}());
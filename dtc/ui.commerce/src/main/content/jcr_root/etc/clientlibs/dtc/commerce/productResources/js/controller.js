(function() {
"use strict";

productResources.controller('productResourcesController', ['$rootScope', 'userProfileManager', function($rootScope,userProfileManager) {

    var self = this;    

    self.marketingMaterials = angular.element('input[name=marketingMaterials]').val();
    self.productInformation = angular.element('input[name=productInformation]').val();
    self.otherSource = angular.element('input[name=otherSource]').val();

    self.getFile = function(basePath) {
        window.open(basePath, '_blank');
    };

	self.marketingMaterials = getJSON(self.marketingMaterials);
    self.productInformation = getJSON(self.productInformation);
    self.otherSource = getJSON(self.otherSource);

	self.isEmpty = self.marketingMaterials.length > 0 || self.productInformation.length > 0 || self.otherSource.length > 0;

     function getJSON(value) {

         var jsonValue = JSON.parse('[' + value + ']');
         var finalValue = [];

		 var isUserLoggedIn = userProfileManager.isUserLoggedIn();
         var userType; 

         if(isUserLoggedIn) {
			var userTypeArray = userProfileManager.getUserProfileMasterInfo().userType.toLowerCase().split("_");
             if(userTypeArray.length > 1) {
                     userType = userTypeArray[userTypeArray.length-1];
                 } else {
                     userType = userTypeArray[0];
                 }
         }
         for(var i=0; i< jsonValue.length; i++) {
			if(isUserLoggedIn && userType == "distributor") {
				finalValue.push(jsonValue[i]);
            } else {
                if(jsonValue[i].userType != "distributor") {
                	finalValue.push(jsonValue[i]);
                }
            }
         }

         for(var i=0; i< finalValue.length; i++) {   
             if(finalValue[i].file.split('.').pop() == 'docx') {
                 if(finalValue[i].imageSrc.length > 0) {
                	 finalValue[i].thumbnail_url = finalValue[i].imageSrc;
                 } else {
                	 finalValue[i].thumbnail_url = "/etc/clientlibs/dtc/core/main/css/src/assets/images/css/ic-document@3x.png";
                 }
                 finalValue[i].product_logo = "/etc/clientlibs/dtc/core/main/css/src/assets/images/css/ic-document-asset-copy-18@3x.png";
             } else if(finalValue[i].file.split('.').pop() == 'pptx') {
                 if(finalValue[i].imageSrc.length > 0) {
                	 finalValue[i].thumbnail_url = finalValue[i].imageSrc;
                 } else {
                	 finalValue[i].thumbnail_url = "/etc/clientlibs/dtc/core/main/css/src/assets/images/css/ic-presentation@3x.png";
                 }
                 finalValue[i].product_logo = "/etc/clientlibs/dtc/core/main/css/src/assets/images/css/ic-presentation-asset-copy-18@3x.png";
             } else if(finalValue[i].file.split('.').pop() == 'pdf') {
                 if(finalValue[i].imageSrc.length > 0) {
                	 finalValue[i].thumbnail_url = finalValue[i].imageSrc;
                 } else {
                	 finalValue[i].thumbnail_url = "/etc/clientlibs/dtc/core/main/css/src/assets/images/css/ic-pdf-copy-6@3x.png";
                 }
                 finalValue[i].product_logo = "/etc/clientlibs/dtc/core/main/css/src/assets/images/css/ic-pdf-asset-copy-19@3x.png";
             }

         }

         return finalValue;
     }

 	self.isDeleted = false;
	self.isFavorited = false;
	self.ifFavUndoVisible = false,
    self.ifDeleteVisible = "=",
    self.ifShareVisible = "=",
    self.ifFavoriteVisible = "="

    this.toggleFavorite = function () {
        self.isFavorited = !self.isFavorited;
        self.isFavorite = !self.isFavorite;
	};

	this.toggleDel = function(){
		self.isDeleted = !self.isDeleted;
	};

}]);
}());

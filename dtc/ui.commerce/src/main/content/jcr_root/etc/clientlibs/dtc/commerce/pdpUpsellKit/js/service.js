
(function() {
"use strict";

pdpUpsellKitComponent.service('pdpUpsellKitComponentService', ['$http', '$q','userProfileManager','serviceURL', function($http,
		$q,userProfileManager,serviceURL) {


	var cartServiceBaseSite = "cosmetics";
    var user_id;
    var endPoint;
    if(userProfileManager.isUserLoggedIn()) {
        user_id = userProfileManager.getUserProfileMasterInfo().userId;
        endPoint = mulesoftHybrisEndpoint;
    } else {
        user_id = "anonymous";
        endPoint = mulesoftHybrisGuestEndpoint;
    }

    this.getUpSellKitProduct = function(sku, currentPath) {
        var deferred = $q.defer();
        var paramsArray = [];
		paramsArray[0] = sku;
		paramsArray[1] = currentPath;
		var url = serviceURL.GetServiceURL('getUpSellKitProductURL' , paramsArray);
        $http({
            method: 'GET',
            url: url,
        }).then(function successCallback(response) {
            deferred.resolve(response.data);
        }, function(response) {
            console.log("")
        });
        return deferred.promise;
    }

	this.getKitPriceData = function(kitProduct) {
		var deferred = $q.defer();
		var paramsArray = [];
		paramsArray[0] = endPoint;
		paramsArray[1] = cartServiceBaseSite;
		paramsArray[2] = user_id;
		paramsArray[3] = kitProduct;
		var url = serviceURL.GetServiceURL('productsPriceURL' , paramsArray);
		$http.post(url)
				.then(function(response) {
					deferred.resolve(response.data);
				});
		return deferred.promise;
	}


	this.getKitStock = function(kitProduct) {
		var deferred = $q.defer();
		var Country = "US";
		var localeID = "us";
		var quantity = "1";
		var salesOfficeCode = "1015";
		var reqBody = {
			"countryCode" : Country,
			"salesOffice" : salesOfficeCode,
			"locale" : localeID,
			"items" : [ {
				"requestedQuantity" : quantity,
				"sku" : kitProduct
			} ]
		}

		var data = JSON.stringify(reqBody);
		var paramsArray = [];
		paramsArray[0] = endPoint;
		paramsArray[1] = cartServiceBaseSite;
		paramsArray[2] = user_id;
		var url = serviceURL.GetServiceURL('stockLevelURL' , paramsArray);
		
		$http.post(url, data).then(function(response) {
			deferred.resolve(response.data);

		});
		return deferred.promise;
	}

}]);
}());
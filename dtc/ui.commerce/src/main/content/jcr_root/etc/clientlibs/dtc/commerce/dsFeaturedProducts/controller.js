(function() {
"use strict";

dsFeaturedProducts.controller('dsFeaturedProductsController', ['$scope', 'dsFeaturedProductsService','$rootScope','$interval','userProfileManager','$q', function($scope,
   dsFeaturedProductsService,$rootScope,$interval,userProfileManager,$q) {
    var self = this;
    $scope.seperator= " - ";
    $scope.slickType = "type1";
    this.priceData = {};
    $scope.hidePSV = false;

	self.locale = userProfileManager.getPageLocaleVal();

  
    $scope.slickSecondaryConfig = {
        enabled : true,
        infinite : true,
        adaptiveHeight : false,
        mobileFirst : true,
        slidesToShow : 1,
        slidesToScroll : 1,
        dots : false,
        focusonselect : true,
        speed : 500,
        responsive : [ {
            breakpoint : 640,
            settings : {
                slidesToShow : 3,
            }
        }, {
            breakpoint : 1024,
            settings : {
                slidesToShow : 4,
                draggable : false,
            }
        } ]
    };
    

    self.initFeaturedProducts = function(userId) {

        self.productSku = [];
        var starRatingsSkuids = [];
        var subdomainResponse  = dsFeaturedProductsService.getSubDomain(userId);
        subdomainResponse.then(function(subdomain) {

            var firebaseData = dsFeaturedProductsService.getFireBaseData(userId,subdomain,self.locale);
            firebaseData.then(function(response) {

                if(response.showDefaultProducts){
                    
                    var defaultData	= dsFeaturedProductsService.getDefaultFeaturedProducts(self.locale.market,self.locale.language);
                    defaultData.then(function(data) {
                        if(data.hasOwnProperty('products')){
                            var customData = [];
                            var skus = [];
                            var starRatingsSkuids = [];
                            for (var i=0; i < data.products.length; i++) {
                                
                                skus[i] = data.products[i].sku;
                                if(data.products[i].baseSku){
                                    starRatingsSkuids[i] = data.products[i].baseSku;
                                } else {
                                    starRatingsSkuids[i] = data.products[i].sku;
                                }
                                
                                customData.push({ 
                                    img : data.products[i].path,
                                    name : data.products[i].title,
                                    skuid : data.products[i].sku,
                                    baseid : data.products[i].baseSku,
                                    url : data.products[i].productPath
                                });
                            }
                            $scope.recos = customData;
                            self.productSku = skus;

                            self.getCrossSellPrice(self.productSku);
                            $scope.slickSecondaryConfig.enabled=false; 
                            self.getAvgRatings(starRatingsSkuids);
                        }
                    });
                    
                } else {
                    
                    if(response.products.length > 0){
                        self.productSku = response.products;
                        var imageDataResponse	= dsFeaturedProductsService.getImageData(self.productSku);
                        imageDataResponse.then(function(imageData) {
                            var customData = [];
                            var starRatingsSkuids = [];
                            
                            for (var i=0; i < self.productSku.length; i++) {
                                
                                var imagePath = '';
                                var title;
                                var url;
                                var baseId = '';
                                
                                if (imageData && imageData.length > 0 && imageData[i]) {
                                    if (imageData[i].hasOwnProperty('sku') && imageData[i].sku === self.productSku[i]) {
                                        if (imageData[i].hasOwnProperty('path')){
                                            imagePath = imageData[i].path + '/jcr:content/renditions/cq5dam.thumbnail.320.310.png';
                                        }
                                        if(imageData[i].hasOwnProperty('title')){
                                            title = imageData[i].title;
                                        }
                                    }
                                    
                                    if(imageData[i].baseSku){
                                        starRatingsSkuids[i] = imageData[i].baseSku;
                                        baseId =  imageData[i].baseSku;
                                        url = '.'+ imageData[i].baseSku + '.' + self.productSku[i] +'.html'; 
                                    } else {
                                        url = '.' + self.productSku[i] +'.html'; 
                                    }
                                }
                                
                                customData.push({ 
                                    img : imagePath,
                                    name : title,
                                    skuid : self.productSku[i],
                                    baseid : baseId,
                                    url : url
                                });
                            }
                            $scope.recos = customData;

                        });
                        
                        self.getCrossSellPrice(self.productSku);
                        $scope.slickSecondaryConfig.enabled=false; 
                        self.getAvgRatings(starRatingsSkuids);
                    }
                }
            });
        });
        
    }
    
    self.getCrossSellPrice = function(id) {
        var promise = dsFeaturedProductsService.getPriceData(id);
        promise.then(function(data) {
            var recommendPrice = data.priceList;
            for (var i = 0; i < $scope.recos.length; i++) {
                var obj = $scope.recos[i];
                var stockStatus = "";
                self.showPriceRange = true;
                if(recommendPrice[i].code == obj.skuid){
                if (!recommendPrice[i].hasOwnProperty("priceRange")) {                      
                    if (recommendPrice[i].hasOwnProperty("stockData")) {
                        
                        if (recommendPrice[i].stockData.hasOwnProperty("lineItemDetailsList") && (recommendPrice[i].stockData.lineItemDetailsList[0].hasOwnProperty("safetyStockStatus"))) {
                            var stockStatusData = recommendPrice[i].stockData.lineItemDetailsList[0];
                            
                            if (stockStatusData.safetyStockStatus == "LOW_STOCK") {
                            	stockStatus = "LS";
                            } else if (stockStatusData.safetyStockStatus == "OUT_OF_STOCK") {
                            	stockStatus = "OS";
                            }
                            if((stockStatusData.safetyStockStatus == "OUT_OF_STOCK" || stockStatusData.safetyStockStatus == "LOW_STOCK") && stockStatusData.backOrdered){
                            	stockStatus = "BO";
                            }
                        }
                    } 
                    if (recommendPrice[i].hasOwnProperty("wholeSaleStatus")) {
                        if(recommendPrice[i].wholeSaleStatus == "02"){
                            self.showPriceRange = false;
                            stockStatus = "OS";
                        }
                        if(recommendPrice[i].wholeSaleStatus == "05"){
                            self.showPriceRange = true;
                            stockStatus = "OS";
                        } 
                    }
                    angular.element('.stockRecently-'+recommendPrice[i].code+'-'+stockStatus).removeClass('ng-hide');
                }
                if(self.showPriceRange ) {
                if (recommendPrice[i].hasOwnProperty("priceRange")) {
                    
                    if (recommendPrice[i].priceRange.hasOwnProperty("maxPrice") && recommendPrice[i].priceRange.hasOwnProperty("minPrice") && $scope.recos[i].skuid == recommendPrice[i].code) {
                        if(recommendPrice[i].priceRange.maxPrice.value > recommendPrice[i].priceRange.minPrice.value){
                            obj["price"] = recommendPrice[i].priceRange.minPrice.formattedValue + " - " + recommendPrice[i].priceRange.maxPrice.formattedValue;
                        }
                        else{
                            obj["price"] = recommendPrice[i].priceRange.minPrice.formattedValue ;
                        }
                    } else {
                        obj["price"] = "";
                    }
                    
                    
                    if (recommendPrice[i].priceRange.hasOwnProperty("minPv") && $scope.recos[i].skuid == recommendPrice[i].code) {
                        obj["psvValue"] = recommendPrice[i].priceRange.minPv.value;
                    } else {
                        obj["psvValue"] = NaN;
                    }
                    
                    if (recommendPrice[i].priceRange.hasOwnProperty("maxPv") && $scope.recos[i].skuid == recommendPrice[i].code) {
                        if(recommendPrice[i].priceRange.maxPv.value > recommendPrice[i].priceRange.minPv.value){
                            obj["maxPv"] = recommendPrice[i].priceRange.maxPv.value;
                            obj["showPsvSeperator"] = true;
                        }
                        else{
                            obj["maxPv"] = NaN;
                            obj["showPsvSeperator"] = false;
                        }
                    } else {
                    	obj["showPsvSeperator"] = false;
                        obj["maxPv"] = NaN;
                    }
                    
                } 
                else {
                    if (recommendPrice[i].hasOwnProperty("price") && $scope.recos[i].skuid == recommendPrice[i].code) {
                        obj["price"] = recommendPrice[i].price.formattedValue;
                    } else {
                        obj["price"] = "";
                    }
                    
                    if (recommendPrice[i].hasOwnProperty("pvValue") && $scope.recos[i].skuid == recommendPrice[i].code) {
                        obj["psvValue"] = recommendPrice[i].pvValue.value;
                        obj["maxPv"] = NaN;
                        obj["showPsvSeperator"] = false;
                    } else {
                        obj["psvValue"] = NaN;
                        obj["maxPv"] = NaN;			
                    }
                }
              }

            }
            }
			$interval(function() {
            		$scope.slickSecondaryConfig.adaptiveHeight = true;
            		$scope.slickSecondaryConfig.enabled = true;
        		}, 500);
            
        });
        
    }
    
    self.getAvgRatings = function(skuIds) {

        var avgRatingsPromiseArray=[];
        for (var i = 0; i < skuIds.length; i++) {
            var promise = dsFeaturedProductsService.getRatings(skuIds[i]);
            avgRatingsPromiseArray.push(promise);
        }

        $q.all(avgRatingsPromiseArray).then(function (data){
            for(var i=0;i<data.length;i++){

                var ratingValue = Math.round(data[i].streamInfo.avgRatings._overall);
                var rating = data[i].streamInfo.commentCount || 0;
                var starRating = self.getStarRatingPosition(ratingValue, 'big');

                var obj = $scope.recos[i];
                obj["rating"] = rating;
                obj["stars"] = starRating;

            }

            $interval(function() {
                if($(".carousel.carousel-secondary").hasClass("slick-initialized")){
                    $scope.slickSecondaryConfig.enabled=false;
                    $(".carousel.carousel-secondary").slick("slickRemove").slick("slickAdd");
                    $scope.slickSecondaryConfig.adaptiveHeight = true;
                    $scope.slickSecondaryConfig.enabled = true;
                }
              }, 400, [2] );
        });
    }

    self.getStarRatingPosition = function(ratingDecimalValue, starType) {
        var starTopPosition = 0;
        var starMargin = 0;
        var starWidth = 0;
        var halfStarTopMargin = 0,
            smallStarFirstPosition = 0;
        var halfStarPosition = -273;
        if (starType == "big") {
            starWidth = 18;
            halfStarPosition = -273;
            halfStarTopMargin = -26;
        } else {
            starWidth = 12;
            halfStarPosition = -242;
            halfStarTopMargin = -64;
            smallStarFirstPosition = -44;
        }
        var decimalValue = ratingDecimalValue.toString().replace(/^[^\.]+/, '0');
        var floorRatingValue = Math.floor(ratingDecimalValue);
        decimalValue = parseFloat(decimalValue);
        var mutipleValue = 0;
        if (0.26 <= decimalValue && decimalValue <= 0.74) {
            starMargin = halfStarTopMargin;
            floorRatingValue++;
        } else if (0.75 <= decimalValue && decimalValue <= 0.99) {
            starMargin = smallStarFirstPosition;
            floorRatingValue++;
        } else {
            starMargin = smallStarFirstPosition;
        }
        if (floorRatingValue >= this.max) {
            starMargin = smallStarFirstPosition;
            floorRatingValue = this.max;
        }
        floorRatingValue = floorRatingValue * starWidth;
        floorRatingValue = halfStarPosition + floorRatingValue
        return floorRatingValue + "px " + starMargin + "px";
    }

    function getParameterByName(name) {
	    var url = window.location.href;
	    name = name.replace(/[\[\]]/g, "\\$&");
	    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
	        results = regex.exec(url);
	    if (!results) return null;
	    if (!results[2]) return '';
	    return decodeURIComponent(results[2].replace(/\+/g, " "));
	} 
	  
    function init() {
    	var storeId = getParameterByName('storeId'); 
    	if(storeId){
    		self.initFeaturedProducts(storeId);
    	}
    }
    init();
}]);
}());

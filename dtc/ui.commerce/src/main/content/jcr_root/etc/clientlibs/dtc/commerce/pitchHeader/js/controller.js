(function() {
    "use strict";

    pitchHeaderModule.controller('pitchHeaderController', ['$scope', '$rootScope', '$http', 'serviceURL', function($scope, $rootScope, $http, serviceURL) {

        var self = this;
        var config = {
            headers: {
                'Content-Type': 'application/json',
                'showgloballoader': true
            }
        };
        var userId = getParameterByName('userId');
        var paramsArray = [userId];
        var storefrontOwner = JSON.parse(localStorage.getItem('storefrontOwner'));
        //If we have the storefrontOwner in local storage and the userId hasn't changed
        if (storefrontOwner != null && (userId == null || storefrontOwner.sapId == userId)) {
            self.sponsorName = storefrontOwner.displayName;
            self.avatarSrc = storefrontOwner.photoURL;
        } else {
            var url = firebaseDatabaseURL + serviceURL.GetServiceURL('storefrontOwnerURL', paramsArray);
            $http.get(url, config).then(function (response) {
                //Pitches will continue to grow.  no need to put all the pitches in localstorage
                delete response.data.pitches;
                localStorage.setItem('storefrontOwner', JSON.stringify(response.data));
                self.sponsorName = response.data.displayName;
                self.avatarSrc = response.data.photoURL;
            });
        }

        function getParameterByName(name) {
            var url = window.location.href;
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }

        $scope.backBtnClick = function() {
            window.history.back();
        };
    }]);
}());

(function() {
"use strict";

checkoutContainer.controller('toastMsgControllerCheckout', ['$rootScope', 'checkoutContainerService', function($rootScope, checkoutContainerService) {
    
    var self = this; 
    self.dangerType = false;
    self.errorMessage = "";
    self.successType = false;
    var scroll_top_duration = 700;
    
    $rootScope.$on("showCheckoutToastMsg", function(event, data){
            self.dangerType = data.dangerType;
            self.errorMessage = data.msg;
            angular.element('html,body').animate({
               	scrollTop: 0
            	}, scroll_top_duration
        	);
    });

    $rootScope.$on("hideToastMsg", function(event){
           self.hideToastMessage();
    });
    
    self.hideToastMessage = function(){
        self.dangerType = false;
    };

}]);
}());

(function() {
    "use strict";

    var config = {
        apiKey: firebaseApiKey,
        authDomain: firebaseAuthDomain,
        databaseURL: firebaseDatabaseURL,
        storageBucket: firebaseStorageBucket,
    };
    firebase.initializeApp(config);

}());
(function() {
"use strict";

pdpCategoryIngredient.controller('pdpCategoryIngredientController', ['$scope', 'pdpCategoryIngredientService', function($scope,pdpCategoryIngredientService) {

    var path = window.location.pathname ;
    var promise = pdpCategoryIngredientService.getIngredients(path);
	this.isFirstOpen = false;
		
    promise.then(function(response) {
       	$scope.ingredients = response;
		if($scope.ingredients!= undefined){
            for(var i=0 ; i< $scope.ingredients.length; i++){
                var items = $scope.ingredients[i].items;
                var count = 0;
                for(var j=0; j < items.length; j++){
                    var obj = $scope.ingredients[i].items;
                    count = count+ parseInt(items[j].count);
                    obj["count"] = count;
                }
            }
        }
    });
	
	if(window.innerWidth < 768){
    	this.isFirstOpen = true;
  	}

    this.getProducts = function(searchResultPath,ingredient){
        window.location.href = searchResultPath+'.html?search='+ingredient;
    }

}]);
}());

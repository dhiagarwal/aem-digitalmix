(function() {
"use strict";

featuredVideo.controller('featuredVideoController', ['$scope', '$timeout', function($scope, $timeout) {

    var self = this;

    self.featuredVideo = angular.element('input[name=videoURL]').val();
    self.backgroundImageUrl = angular.element('input[name=imageURL]').val();
    self.videoPlaying = false;

    self.togglePlay = function() {
        this.videoPlaying = true;
        var video = document.querySelector('video');
        if (video.paused) {
            $timeout(function(){video.play();},10)
            video.controls=true;
            this.videoPlaying = true;
            angular.element(".watch-video-panel").css("background-image","");
        } else {
            $timeout(function(){video.pause();},10)
            video.controls=false;
            this.videoPlaying = false;
            angular.element(".watch-video-panel").css("background-image","url("+this.backgroundImageUrl+")");
        }
    };  
    
    this.onVideoEnd = function() {
    	var video = document.querySelector('video');

    	if (video.ended) {
    		video.controls=false;
            this.videoPlaying = false;
            angular.element(".watch-video-panel").css("background-image","url("+this.backgroundImageUrl+")");
    	}
    };
}]);
}());

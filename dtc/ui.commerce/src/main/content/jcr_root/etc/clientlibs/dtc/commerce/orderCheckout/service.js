
(function() {
"use strict";

orderCheckout.service('orderCheckoutService', ['$http', '$q','userProfileManager','serviceURL' ,function($http, $q,userProfileManager,serviceURL) {


	var cartServiceBaseSite = "cosmetics";
    var user_id;
    var endPoint;
    var config = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
				//'showgloballoader': true
            }
        };
    if(userProfileManager.isUserLoggedIn()) {
        user_id = userProfileManager.getUserProfileMasterInfo().userId;
        endPoint = mulesoftHybrisEndpoint;
    } else {
        user_id = "anonymous";
        endPoint = mulesoftHybrisGuestEndpoint;
    }
    
    this.placeOrder = function(cartID,securityCode){
		var placeOrderResponse = $q.defer();
		var paramsArray = [];
		paramsArray[0] = endPoint;
		paramsArray[1] = cartServiceBaseSite;
		paramsArray[2] = user_id;
		paramsArray[3] = cartID;
		paramsArray[4] = securityCode;
		var url = serviceURL.GetServiceURL('placeOrderURL' , paramsArray);
        $http.post(url, '', config).then(function successCallback(response) {
			placeOrderResponse.resolve(response);
            }, function errorCallback(response) {
                placeOrderResponse.resolve(response);
        });
        return placeOrderResponse.promise;

		};
		
    this.getImageData = function(skuIds,path){
        var deferred = $q.defer(); 
        var paramsArray = [];
		paramsArray[0] = skuIds;
		paramsArray[1] = path;
	
		var url = serviceURL.GetServiceURL('getImageDataPathParamURL' , paramsArray);
        $http({
            method: 'GET',
            url: url,

        }).then(function successCallback(response) {
            deferred.resolve(response.data);
        },function (response){ 
            console.log("")
        });
        return deferred.promise;
    }	
		
}]);
}());
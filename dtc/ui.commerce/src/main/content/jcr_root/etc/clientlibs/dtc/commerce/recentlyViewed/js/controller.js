(function() {
"use strict";

recentlyViewed.controller('recentlyViewedController' ,['$location', '$scope', 'recentlyViewedService', 'userProfileManager','$cookies','$q','$interval', function($location, $scope, recentlyViewedService, userProfileManager,$cookies, $q, $interval ) {
    
    var self = this;
    $scope.slickType = "type1";
    $scope.seperator = " - ";
    $scope.hidePSV = true;
    self.maxStar = 5;
    self.ratingValue = self.ratingValue || 0;
    self.max = self.max || 300;
    self.ratingValue = 0;
    var userType;
	self.showPriceRange = true;
    if(userProfileManager.isUserLoggedIn()) {
        var userTypes = userProfileManager.getUserProfileMasterInfo().userType.toLowerCase().split("_");
        if(userTypes.length > 1) {
            userType = userTypes[userTypes.length-1];
        } else {
            userType = userTypes[0];
        }
        
        if(userType == "distributor"){
        	$scope.hidePSV = false;
        }
    }
    
    $scope.slickSecondaryConfig = {
    	    enabled: true,
    	    infinite: true,
    	    adaptiveHeight: false,
    	    mobileFirst: true,
    	    slidesToShow: 1,
    	    slidesToScroll: 1,
    	    dots: false,
    	    focusonselect: true,
    	    speed: 500,
    	    responsive: [
    	        {
    	            breakpoint: 640,
    	            settings: {
    	                slidesToShow: 3,
    	            }
    	        },
    	        {
    	            breakpoint: 1024,
    	            settings: {
    	                slidesToShow: 4,
    	                draggable: false,
    	            }
    	        }
    	    ]
    	};

    
    self.getUpdatedData = function(categoryDataTemp){
        
        self.productSku = [];
        var baseProductSku = [];
        var starRatingsSkuids = [];
        var productDataPromiseArray = [];
        $scope.recos = categoryDataTemp.productData;
        var recentlyViewedRendition = $('input[name=recentltlyViewedRendition]').val();
        if(categoryDataTemp && categoryDataTemp.productData){
            for (var i = 0; i < categoryDataTemp.productData.length; i++){
                self.productSku[i] = categoryDataTemp.productData[i].varID;
                if((categoryDataTemp.productData[i].skuID).includes(".")) {
                	baseProductSku[i] = (categoryDataTemp.productData[i].skuID).substr(0,(categoryDataTemp.productData[i].skuID).indexOf("."));
                }
                else {
                	baseProductSku[i] = categoryDataTemp.productData[i].skuID;
                }
                categoryDataTemp.productData[i].img = "/content/dam/dtc/products/"+categoryDataTemp.productData[i].varID + "/" + categoryDataTemp.productData[i].imageName + "/jcr:content/renditions/"+recentlyViewedRendition;
                categoryDataTemp.productData[i].url = "." + categoryDataTemp.productData[i].skuID.toLowerCase() +".html";
                categoryDataTemp.productData[i].skuid = categoryDataTemp.productData[i].varID;
                categoryDataTemp.productData[i].name = categoryDataTemp.productData[i].productTitle;
                starRatingsSkuids.push(baseProductSku[i]);
            }
            $q.all(productDataPromiseArray).then(function (data){
                self.getAvgRatings(starRatingsSkuids);
            });
        }
        return categoryDataTemp;
    }
   if($cookies.get("qData")!=undefined && $cookies.get("qData")!=""){
    	self.categoryData = self.getUpdatedData(JSON.parse($cookies.get("qData")));
    }  
   
   self.getAvgRatings = function(skuIds) {

       var avgRatingsPromiseArray=[];
       for (var i = 0; i < skuIds.length; i++) {
           var promise = recentlyViewedService.getRatings(skuIds[i]);
           avgRatingsPromiseArray.push(promise);
       }

       $q.all(avgRatingsPromiseArray).then(function (data){
           for(var i=0;i<data.length;i++){
               var ratingValue = Math.round(data[i].streamInfo.avgRatings._overall);
               var rating = data[i].streamInfo.commentCount || 0;
               var starRating = self.getStarRatingPosition(ratingValue, 'big'); 
               var obj = $scope.recos[i];
               obj["rating"] = rating;
               obj["stars"] = starRating;
           }

           $interval(function() {
               if($(".carousel.carousel-secondary").hasClass("slick-initialized")){
                   $scope.slickSecondaryConfig.enabled=false;
                   $(".carousel.carousel-secondary").slick("slickRemove").slick("slickAdd");
                   $scope.slickSecondaryConfig.adaptiveHeight = true;
                   $scope.slickSecondaryConfig.enabled = true;
               }
             }, 400, [2] );
       });
   }

   self.getStarRatingPosition = function(ratingDecimalValue, starType) {
       var starTopPosition = 0;
       var starMargin = 0;
       var starWidth = 0;
       var halfStarTopMargin = 0,
           smallStarFirstPosition = 0;
       var halfStarPosition = -273;
       if (starType == "big") {
           starWidth = 18;
           halfStarPosition = -273;
           halfStarTopMargin = -26;
       } else {
           starWidth = 12;
           halfStarPosition = -242;
           halfStarTopMargin = -64;
           smallStarFirstPosition = -44;
       }
       var decimalValue = ratingDecimalValue.toString().replace(/^[^\.]+/, '0');
       var floorRatingValue = Math.floor(ratingDecimalValue);
       decimalValue = parseFloat(decimalValue);
       var mutipleValue = 0;
       if (0.26 <= decimalValue && decimalValue <= 0.74) {
           starMargin = halfStarTopMargin;
           floorRatingValue++;
       } else if (0.75 <= decimalValue && decimalValue <= 0.99) {
           starMargin = smallStarFirstPosition;
           floorRatingValue++;
       } else {
           starMargin = smallStarFirstPosition;
       }
       if (floorRatingValue >= this.max) {
           starMargin = smallStarFirstPosition;
           floorRatingValue = this.max;
       }
       floorRatingValue = floorRatingValue * starWidth;
       floorRatingValue = halfStarPosition + floorRatingValue
       return floorRatingValue + "px " + starMargin + "px";
   }
   
    function init(){
        
    	var productSkus;
        if(self.productSku){
        	productSkus = self.productSku.join(",");
        }
        var productDetailsPrice = recentlyViewedService.getPriceData(productSkus);
        productDetailsPrice.then(function(data){
            var priceList = data.priceList;
			var stockStatus = "";
			if(priceList != undefined)
			{
            for(var i=0; i < $scope.recos.length ; i++){
            	var obj = $scope.recos[i];
            	var stockStatus = "";
            	self.showPriceRange = true;
				if(!priceList[i].hasOwnProperty('priceRange')){
                     if (priceList[i].hasOwnProperty("stockData")) {
                        
                        if (priceList[i].stockData.hasOwnProperty("lineItemDetailsList") && (priceList[i].stockData.lineItemDetailsList[0].hasOwnProperty("safetyStockStatus"))) {
                            var stockStatusData = priceList[i].stockData.lineItemDetailsList[0];
                            if (stockStatusData.safetyStockStatus == "LOW_STOCK") {
                                stockStatus = "LS";
                            } else if (stockStatusData.safetyStockStatus == "OUT_OF_STOCK") {
                                stockStatus = "OS";
                            } else if (stockStatusData.safetyStockStatus == "NORMAL") {
                                stockStatus = "";
                            } 
                            if((stockStatusData.safetyStockStatus == "OUT_OF_STOCK" || stockStatusData.safetyStockStatus == "LOW_STOCK") && stockStatusData.backOrdered){
                                stockStatus = "BO";
                            }
                        }
                    }  
                    if (priceList[i].hasOwnProperty("wholeSaleStatus")) {
                        if(priceList[i].wholeSaleStatus == "02"){
                            self.showPriceRange = false;
							stockStatus = "OS";
                        }
                        if(priceList[i].wholeSaleStatus == "05"){
                            self.showPriceRange = true;
							stockStatus = "OS";
                        } 
                    }
					angular.element('.stockRecently-'+priceList[i].code+'-'+stockStatus).removeClass('ng-hide');
					}
                if(self.showPriceRange ){
                if(priceList[i].hasOwnProperty('priceRange')){
                    if(priceList[i].priceRange.hasOwnProperty('maxPrice') && priceList[i].priceRange.hasOwnProperty('minPrice') ){
                        if(priceList[i].priceRange.maxPrice.value > priceList[i].priceRange.minPrice.value){
                        	obj["price"] = priceList[i].priceRange.minPrice.formattedValue + " - "+ priceList[i].priceRange.maxPrice.formattedValue;
                        }
                        else{
                        	obj["price"] = priceList[i].priceRange.minPrice.formattedValue;
                        }
                    }
                    else {
                        obj["price"] = "";
                    }
                    if(priceList[i].priceRange.hasOwnProperty('minPv')){
                		obj["psvValue"] = priceList[i].priceRange.minPv.value;
                    }
	                else {
	                        obj["psvValue"] = NaN;
	                }
                    if (priceList[i].priceRange.hasOwnProperty("maxPv")) {
	            		if(priceList[i].priceRange.maxPv.value > priceList[i].priceRange.minPv.value){
	            			 obj["maxPv"] = priceList[i].priceRange.maxPv.value;
	            			 obj["showPsvSeperator"] = true;
	                    }
	                    else {
	                    	obj["maxPv"] = NaN;
	                    	obj["showPsvSeperator"] = false;
	                    }
                    }
                    else {
                    	obj["showPsvSeperator"] = false;
                        obj["maxPv"] = NaN;
                    }
                    
                } else {
                    if(priceList[i].hasOwnProperty('price'))
                    {
                    	obj["price"] = priceList[i].price.formattedValue;
                    }
                    else {
                        obj["price"] = "";
                    }
                    if(priceList[i].hasOwnProperty('pvValue'))
                    {
                    	obj["psvValue"] = priceList[i].pvValue.value;
                        obj["maxPv"] = NaN;
                        obj["showPsvSeperator"] = false;
                    } 
                    else {
                        obj["psvValue"] = NaN;
                        obj["maxPv"] = NaN;			
                    }               
                }
            }
			}
        }

        });
    }
    init();

    self.redirect = function(url) {
		window.location.href = url;
    }
	
}]);
}());

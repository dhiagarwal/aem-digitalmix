
(function() {
"use strict";

shoppingCart.service('shoppingCartComponentService', ['$http', '$q', 'userProfileManager', 'serviceURL',function($http, $q,userProfileManager,serviceURL) {

var cartServiceBaseSite = "cosmetics";
    
    var user_id;
    var endPoint;
    var config;
    if(userProfileManager.isUserLoggedIn()) {
        user_id = userProfileManager.getUserProfileMasterInfo().userId;
        endPoint = mulesoftHybrisEndpoint;
    } else {
        user_id = "anonymous";
        endPoint = mulesoftHybrisGuestEndpoint;
    }

    this.getCartData = function(cartID) {
        var deferred = $q.defer();
		var config={
                headers : {
                    'Content-Type' : 'application/json'
                }
            };
		var paramsArray = [];
		paramsArray[0] = endPoint;	
		paramsArray[1] = cartServiceBaseSite;
		paramsArray[2] = user_id;
		paramsArray[3] = cartID;
		var url = serviceURL.GetServiceURL('cartURL' , paramsArray);

        $http.get(url,config).then(function(response) {
            deferred.resolve(response.data);
        },function(response) {
            console.log("Error in get Cart Service");
		});
        return deferred.promise;
    }

    this.updatedCartData = function(prodID, CartID) {
        var deferred_delete = $q.defer();
        var paramsArray = [];
		paramsArray[0] = endPoint;	
		paramsArray[1] = cartServiceBaseSite;
		paramsArray[2] = user_id;
		paramsArray[3] = CartID;
		paramsArray[4] = prodID;
		var url = serviceURL.GetServiceURL('removeProductURL' , paramsArray);
		
        $http({method: 'DELETE', url: url}).then(function(response) {
            deferred_delete.resolve(response.data);
        });
        return deferred_delete.promise;
    }

    this.updateQuantity = function(userId, cartId, quantity, skuId) {
        var deferred_updateQuantity = $q.defer();
        var values = {
            'qty': quantity
        };
        if(userProfileManager.isUserLoggedIn()) {
        	config = {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                }
        } else {
        	config = {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                };
        }
        
        var paramsArray = [];
		paramsArray[0] = endPoint;	
		paramsArray[1] = cartServiceBaseSite;
		paramsArray[2] = userId;
		paramsArray[3] = cartId;
		paramsArray[4] = skuId;
		var url = serviceURL.GetServiceURL('updateQuantityURL' , paramsArray);
        
        var promise = $http.put(url, $.param(values), config).success(
            deferred_updateQuantity.resolve);
        return deferred_updateQuantity.promise;
    }
	
	this.getGuestCheckoutEnabled = function() {
	    var deferred_config=$q.defer();
    	var getUrl = '/content/dtc/configurations/americas/_jcr_content.list.json';
		$http.get(getUrl).then(function(response) {
            deferred_config.resolve(response.data);
    	});
        return deferred_config.promise;
    }
	
	this.applyVoucher = function(voucherCode,guid) {
        var applyVoucherResponse = $q.defer();
        var paramsArray = [];
		paramsArray[0] = endPoint;	
		paramsArray[1] = cartServiceBaseSite;
		paramsArray[2] = user_id;
		paramsArray[3] = guid;
		paramsArray[4] = voucherCode;
		var url = serviceURL.GetServiceURL('applyVoucherURL' , paramsArray);
        $http.post(url).then(function successCallback(response) {
        		applyVoucherResponse.resolve(response.data);
        	  }, function errorCallback(response) {
        	    applyVoucherResponse.resolve(response.data);
        	  });
		return applyVoucherResponse.promise;
    }

	this.removeVoucher = function(voucherCode,guid) {
		var removeVoucherResponse = $q.defer();
		var paramsArray = [];
		paramsArray[0] = endPoint;	
		paramsArray[1] = cartServiceBaseSite;
		paramsArray[2] = user_id;
		paramsArray[3] = guid;
		paramsArray[4] = voucherCode;
		var url = serviceURL.GetServiceURL('removeVoucherURL' , paramsArray);	  
		$http({method: 'DELETE', url: url}).then(function(response) {
			removeVoucherResponse.resolve(response.data);
		},function (response){ 
			console.log("Error in calling Remove Voucher :Checkout Container: removeVoucher");
		});
		return removeVoucherResponse.promise;
	}
    
	this.getImageData = function(skuIds,path){
        var deferred = $q.defer(); 

        var paramsArray = [];
		paramsArray[0] = skuIds;
		paramsArray[1] = path;
		var url = serviceURL.GetServiceURL('getImageDataPathParamURL' , paramsArray);	
		
		$http({
			method: 'GET',
			url: url,
		}).then(function successCallback(response) {
			deferred.resolve(response.data);
		},function (response){ 
			console.log("");
		});

        return deferred.promise;
    }
}]);
}());
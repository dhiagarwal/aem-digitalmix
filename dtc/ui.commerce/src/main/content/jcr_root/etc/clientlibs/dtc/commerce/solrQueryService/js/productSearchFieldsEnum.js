// Solr language node properties fields to search on;
var ProductSearchFieldsEnum = {
    SKU             : 1,
    TITLE           : 2,
    DESCRIPTION     : 3,
    PRODUCTS        : 4,
    KEY_INGREDIENTS     : 5,
    CATEGORIES      : 6,
    SEARCH_TERMS    : 7
};

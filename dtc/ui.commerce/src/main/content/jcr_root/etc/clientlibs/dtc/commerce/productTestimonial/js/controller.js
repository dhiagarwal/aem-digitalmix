(function() {
"use strict";

productTestimonial.controller('productTestimonialController', ['$timeout', 'productTestimonialService', '$sce', function($timeout, productTestimonialService, $sce) {
    var self = this;
    this.testimonials = {};
    this.videoPlaying = false;
    var testimonialPath = angular.element('input[name=testimonialBasePath]').val();
    
    function init(){
		var url =  window.location.toString();
        var subUrl =url.substr(url.lastIndexOf('/') + 1);
        var skuArray = subUrl.split('.');
        if(skuArray.length > 2){
			var promise = productTestimonialService.getTestimonialIconData(skuArray[1], testimonialPath);
			promise.then(function(data) {
            	self.testimonials = data;
        	});
        }
    }

    init();

    this.slickVideoConfig = {
        enabled: true,
        infinite: true,
        adaptiveHeight: false,
        mobileFirst: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        draggable: false,
        arrows: false,
        dots: true,
        speed:500,
        event: {
            beforeChange: function (event, slick, currentSlide, nextSlide) {
            },
            afterChange: function (event, slick, currentSlide, nextSlide) {
                var video = angular.element.find('video');
                angular.forEach(video, function(value, key){
                    if(video[key]) {
                        video[key].pause();
                        video[key].controls=false;
                        self.videoPlaying = false;
                    }
                });
            }
        }
    };

    self.togglePlay = function(index) {
        var video = angular.element.find('#video-'+index);

        if (video[0].paused) {
            video[0].play();
            video[0].controls=true;
            this.videoPlaying = true;
        } else {
            $timeout(function(){video[0].pause();},10) 
            video[0].controls=false;
            this.videoPlaying = false;
        }
    };
    
    self.video = function(index) {
        var videoElements = angular.element.find('#video-'+index);
        videoElements[0].currentTime = 0;
        videoElements[0].controls = false;
        self.videoPlaying = false;
    };
    
    self.trustUrl = function(url) {
        return $sce.trustAsResourceUrl(url);
   }

}]);
}());
'use strict';

commerceModule.requires.push('productSearchListModule');

var productSearchList = angular.module("productSearchListModule", [])
.directive('scrolly', function (){
  return {
      restrict: 'A',
      link: function (scope, element, attrs, $document) {
          var raw = element[0];
          var $window = angular.element(window);
          $window.bind('scroll', function () {
              var scrollPos = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0 + document.documentElement.clientHeight;
              var elemBottom = element[0].offsetTop + element.height();
              if (scrollPos >= elemBottom) {
                scope.$apply(attrs.scrolly);
              }
          });
      }
  }
 });
(function() {
"use strict";

orderDetail.service('orderDetailService', ['$http', '$q','userProfileManager','serviceURL', function($http, $q,userProfileManager,serviceURL) {
	this.$http = $http;
	this.$q = $q;

	var cartServiceBaseSite = "cosmetics";
    var user_id;
    var endPoint;
    if(userProfileManager.isUserLoggedIn()) {
        user_id = userProfileManager.getUserProfileMasterInfo().userId;
        endPoint = mulesoftHybrisEndpoint;
    } else {
        user_id = "anonymous";
        endPoint = mulesoftHybrisGuestEndpoint;
    }

	this.getOrderData = function(orderID) {
		var deferred = this.$q.defer();
		
		var paramsArray = [];
		paramsArray[0] = endPoint;
		paramsArray[1] = cartServiceBaseSite;
		paramsArray[2] = user_id;
		paramsArray[3] = orderID;
		
		var url = serviceURL.GetServiceURL('getOrderDetailsURL' , paramsArray);
		$http.get(url).success(deferred.resolve);
		return deferred.promise;
	};
	
	
    this.getImageData = function(skuIds,path){
        var deferred = $q.defer(); 
        var paramsArray = [];
		paramsArray[0] = skuIds;
		paramsArray[1] = path;
		
		var url = serviceURL.GetServiceURL('getImageDataPathParamURL' , paramsArray);
        $http({
            method: 'GET',
            url: url,
        }).then(function successCallback(response) {
            deferred.resolve(response);            
        },function (response){ 
            console.log("")
        });
        return deferred.promise;
    }

}]);
}());

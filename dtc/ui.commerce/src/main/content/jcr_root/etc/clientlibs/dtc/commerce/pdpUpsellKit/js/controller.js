
(function() {
"use strict";

pdpUpsellKitComponent.controller('pdpUpsellKitComponentController', ['$scope', '$rootScope', 'pdpUpsellKitComponentService', 'userProfileManager', function(
		$scope, $rootScope, pdpUpsellKitComponentService, userProfileManager) {

	var self = this;
	this.kitData = {};
	$scope.hidePSV = true;
    var upSellKitSku=$rootScope.skuCrossSell;
    $scope.componentVisible = false;

    $rootScope.$on("updateUpSellKit", function() {
        initUpSellKit();
    });

    var userType;
    if(userProfileManager.isUserLoggedIn()) {
        var userTypes = userProfileManager.getUserProfileMasterInfo().userType.toLowerCase().split("_");
        if(userTypes.length > 1) {
            userType = userTypes[userTypes.length-1];
        } else {
            userType = userTypes[0];
        }
        
        if(userType == "distributor"){
            $scope.hidePSV = false;
        }
    }
	
	function initUpSellKit() {

        var path = window.location.pathname;
        path = path.substring(0, path.lastIndexOf("."));
        var promise = pdpUpsellKitComponentService.getUpSellKitProduct(upSellKitSku, path);
        promise.then(function(data) {
            if(!(angular.equals(data, {}))){
                $scope.skuId = data.skuid;
                $scope.kitImage = data.imgProduct;
                $scope.productName = data.productName;
                $scope.shortDescription = data.shortDescription;
                $scope.productType = data.type;
                $scope.upSellKitProdUrl = data.upSellUrl;
                if (data.purchasable) {
                    $scope.componentVisible = true;
                    var pricePromise = pdpUpsellKitComponentService.getKitPriceData(data.skuid);
                    pricePromise.then(function(data) {
                        self.kitData = data;
                        var entries = data.priceList;
                        if(entries[0].hasOwnProperty("price")){
                            $scope.upsellKitPrice = entries[0].price.formattedValue;
                        }
                        if(entries[0].hasOwnProperty('pvValue')){
                            $scope.psvValue = entries[0].pvValue.value;
                        }
                        if(!entries[0].hasOwnProperty("priceRange")){
                                
                            if(entries[0].hasOwnProperty("stockData")){
                                var stockDataStatus = entries[0].stockData.lineItemDetailsList;
                                if(stockDataStatus[0].hasOwnProperty("safetyStockStatus")){
                                    if(stockDataStatus[0].safetyStockStatus == "LOW_STOCK"){
                                         $scope.componentVisible = true;
                                    } else if(stockDataStatus[0].safetyStockStatus == "OUT_OF_STOCK" && stockDataStatus[0].backOrdered == false){
                                         $scope.componentVisible = false;
                                    } else if(stockDataStatus[0].safetyStockStatus == "NORMAL"){
                                        $scope.componentVisible = true;
                                    }
                                    
                                }
                                if(entries[0].hasOwnProperty("wholeSaleStatus")){
                                    if(entries[0].wholeSaleStatus == "02"){
                                         $scope.componentVisible = false;
                                    }
                                    else if(entries[0].wholeSaleStatus == "05"){
                                         $scope.componentVisible = false;
                                    }
                                    else if(entries[0].wholeSaleStatus == "09"){
                                         $scope.componentVisible = false;
                                    }
                                    else if(entries[0].wholeSaleStatus == "10"){
                                        if(stockDataStatus[0].safetyStockStatus == "OUT_OF_STOCK" && stockDataStatus[0].backOrdered == false){
                                            $scope.componentVisible = false;
                                        }
                                        else{
                                            $scope.componentVisible = true;
                                        }
                                    }
                                }       
                            }
                        }
                    });
                }
                else{
                    $scope.componentVisible = false;
                }
            }
            else{
                $scope.componentVisible = false;
            }
        });
	}

	$scope.addUpsellKitToCart = function() {

		var productKitUpsellQty = "1";
		var productData = {
			"orderList" : [ {
				"code" : $scope.skuId,
				"qty" : productKitUpsellQty
			} ]
		};
		if ($scope.skuId != "") {
			$rootScope.emitShowShoppingBag = true;
			$rootScope.$emit("showShoppingBag", productData);
		}
	}
	
	function init() {
        initUpSellKit();
    }

    init();

}]);
}());
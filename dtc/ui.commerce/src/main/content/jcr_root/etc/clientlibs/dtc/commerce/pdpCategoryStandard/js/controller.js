(function() {
"use strict";

pdpCategoryStandard.controller('pdpCategoryStandardController', ['$rootScope', 'pdpCategoryStandardService', '$scope', 'userProfileManager', function($rootScope, pdpCategoryStandardService, $scope, userProfileManager) {

    var self = this;
    self.skuPath = angular.element('input[name=product-sku]').val();
    self.sku = self.skuPath.split("/")[self.skuPath.split("/").length - 1];
	self.sku = self.sku.toUpperCase();
    self.psvValue = "";
	self.hidePSV = true;

    var userType;
    if(userProfileManager.isUserLoggedIn()) {
        var userTypes = userProfileManager.getUserProfileMasterInfo().userType.toLowerCase().split("_");
        if(userTypes.length > 1) {
            userType = userTypes[userTypes.length-1];
        } else {
            userType = userTypes[0];
        }
        
        if(userType == "distributor"){
            self.hidePSV = false;
        }
    }
	
	
    self.getPrice = function(sku) {
        var promise = pdpCategoryStandardService.getPriceData(sku);
        promise.then(function(data) {
            self.priceData = data.priceList;
			self.price = '';
            self.minPvv = '';
            self.maxPvv = '';
            $scope.seperator = " - ";

            if(self.priceData[0].hasOwnProperty("priceRange")){
				if((self.priceData[0].priceRange.maxPrice.value > self.priceData[0].priceRange.minPrice.value)
                   &&(self.priceData[0].priceRange.maxPv.value == self.priceData[0].priceRange.minPv.value)){
                    self.price = self.priceData[0].priceRange.minPrice.formattedValue + " - " + self.priceData[0].priceRange.maxPrice.formattedValue;
                    self.minPvv = self.priceData[0].priceRange.minPv.value;
                    self.maxPvv = NaN;
                    if(self.hidePSV){
						$scope.showSeperator = false;
                    }
                    else{
                   		 $scope.showSeperator = false;
                    }

                }
                if((self.priceData[0].priceRange.maxPv.value > self.priceData[0].priceRange.minPv.value)
                  &&(self.priceData[0].priceRange.maxPrice.value == self.priceData[0].priceRange.minPrice.value)){
                     self.price = self.priceData[0].priceRange.minPrice.formattedValue ;
                     self.minPvv = self.priceData[0].priceRange.minPv.value;
                     self.maxPvv = self.priceData[0].priceRange.maxPvv.value;
                     if(self.hidePSV){
						$scope.showSeperator = false;
					 }
                    else{
                   		 $scope.showSeperator = true;
                    }

                }
                if((self.priceData[0].priceRange.maxPv.value > self.priceData[0].priceRange.minPv.value)
                   &&(self.priceData[0].priceRange.maxPrice.value > self.priceData[0].priceRange.minPrice.value)){
                    self.price = self.priceData[0].priceRange.minPrice.formattedValue + " - " + self.priceData[0].priceRange.maxPrice.formattedValue;
                    self.minPvv = self.priceData[0].priceRange.minPv.value;
                    self.maxPvv = self.priceData[0].priceRange.maxPv.value;
                    if(self.hidePSV){
						$scope.showSeperator = false;
                    }
                    else{
                   		 $scope.showSeperator = true;
                    }
                }
                if((self.priceData[0].priceRange.maxPv.value == self.priceData[0].priceRange.minPv.value)
                   &&(self.priceData[0].priceRange.maxPrice.value == self.priceData[0].priceRange.minPrice.value)){
                    self.price = self.priceData[0].priceRange.minPrice.formattedValue;
                    self.minPvv = self.priceData[0].priceRange.minPv.value;
                    self.maxPvv = NaN;
                    if(self.hidePSV){
						$scope.showSeperator = false;
                    }
                    else{
                   		 $scope.showSeperator = false;
                    }
                }

            }
            else{

                if(self.priceData[0].hasOwnProperty("price")){
                	self.price = self.priceData[0].price.formattedValue ;
            	}
           		else{
					self.price = '';
            	}
            	if(self.priceData[0].hasOwnProperty("pvValue")){
					self.minPvv = self.priceData[0].pvValue.value;
                	self.maxPvv = NaN;
                    $scope.showSeperator = false;
            	}
           		else{
					self.minPvv = '';
                	self.maxPvv = NaN;
                    $scope.showSeperator = false;
          		}

            }
        });
    }
    self.getPrice(self.sku);
	
	

    self.addProductToCart = function(productID) {
        var productData = {
            "orderList": [{
                "code": productID,
                "qty": 1
            }]
        };
        $rootScope.emitShowShoppingBag = true;
        $rootScope.$emit("showShoppingBag", productData);
    };

}]);
}());

(function() {
"use strict";

pdpCrosssell.controller('pdpCrosssellComponentController', ['$scope', 'pdpCrosssellComponentService','$rootScope','$interval','userProfileManager','$q', function($scope,
   pdpCrosssellComponentService,$rootScope,$interval,userProfileManager,$q) {
    var self = this;
    $scope.seperator= " - ";
    $scope.slickType = "type1";
    this.priceData = {};
    $scope.hidePSV = true;
    var userType;
    if(userProfileManager.isUserLoggedIn()) {
        var userTypes = userProfileManager.getUserProfileMasterInfo().userType.toLowerCase().split("_");
        if(userTypes.length > 1) {
            userType = userTypes[userTypes.length-1];
        } else {
            userType = userTypes[0];
        }
        
        if(userType == "distributor"){
            $scope.hidePSV = false;
        }
    }
    
    $scope.slickSecondaryConfig = {
        enabled : true,
        infinite : true,
        adaptiveHeight : false,
        mobileFirst : true,
        slidesToShow : 1,
        slidesToScroll : 1,
        dots : false,
        focusonselect : true,
        speed : 500,
        responsive : [ {
            breakpoint : 640,
            settings : {
                slidesToShow : 3,
            }
        }, {
            breakpoint : 1024,
            settings : {
                slidesToShow : 4,
                draggable : false,
            }
        } ]
    };
    
    $rootScope.$on("updateCrossSell", function() {
        self.initCrossSellProducts();
    });
    
    self.initCrossSellProducts = function() {
        var path = window.location.pathname;
        //path = path.substring(0, path.lastIndexOf("."));
        var crossSellSku = $rootScope.skuCrossSell;
        //var crossSellSku = "us10150010000003";
        var starRatingsSkuids = [];
        var id = "";
        var promise = pdpCrosssellComponentService.getCrossSellProducts(crossSellSku, path);
        promise.then(function(data) {
            $scope.recos = data;
            if (data.length == 0) {
                $scope.visible = false;
            } else {
                $scope.visible = true;
            }
            if (data != undefined && data != '') {
                for (var i = 0; i < data.length; i++) {
                    id = id.concat(data[i].skuid + ",");
                    starRatingsSkuids.push(data[i].baseid);
                }
                id = id.substring(0, id.lastIndexOf(","));
                self.getCrossSellPrice(id);
				$scope.slickSecondaryConfig.enabled=false; 
				self.getAvgRatings(starRatingsSkuids);
            }
        });
    }
    
    self.getCrossSellPrice = function(id) {
        var promise = pdpCrosssellComponentService.getPriceData(id);
        promise.then(function(data) {
            var recommendPrice = data.priceList;
            for (var i = 0; i < $scope.recos.length; i++) {
                var obj = $scope.recos[i];
                var stockStatus = "";
                self.showPriceRange = true;
                if (!recommendPrice[i].hasOwnProperty("priceRange")) {                      
                    if (recommendPrice[i].hasOwnProperty("stockData")) {
                        
                        if (recommendPrice[i].stockData.hasOwnProperty("lineItemDetailsList") && (recommendPrice[i].stockData.lineItemDetailsList[0].hasOwnProperty("safetyStockStatus"))) {
                            var stockStatusData = recommendPrice[i].stockData.lineItemDetailsList[0];
                            
                            if (stockStatusData.safetyStockStatus == "LOW_STOCK") {
                            	stockStatus = "LS";
                            } else if (stockStatusData.safetyStockStatus == "OUT_OF_STOCK") {
                            	stockStatus = "OS";
                            } else if (stockStatusData.safetyStockStatus == "NORMAL") {
                                stockStatus = "";
                            } 
                            if((stockStatusData.safetyStockStatus == "OUT_OF_STOCK" || stockStatusData.safetyStockStatus == "LOW_STOCK") && stockStatusData.backOrdered){
                            	stockStatus = "BO";
                            }
                        }
                    } 
                    if (recommendPrice[i].hasOwnProperty("wholeSaleStatus")) {
                        if(recommendPrice[i].wholeSaleStatus == "02"){
                            self.showPriceRange = false;
                            stockStatus = "OS";
                        }
                        if(recommendPrice[i].wholeSaleStatus == "05"){
                            self.showPriceRange = true;
                            stockStatus = "OS";
                        } 
                    }
                    angular.element('.stockRecently-'+recommendPrice[i].code+'-'+stockStatus).removeClass('ng-hide');
                }
                if(self.showPriceRange ) {
                if (recommendPrice[i].hasOwnProperty("priceRange")) {
                    
                    if (recommendPrice[i].priceRange.hasOwnProperty("maxPrice") && recommendPrice[i].priceRange.hasOwnProperty("minPrice") && $scope.recos[i].skuid == recommendPrice[i].code) {
                        if(recommendPrice[i].priceRange.maxPrice.value > recommendPrice[i].priceRange.minPrice.value){
                            obj["price"] = recommendPrice[i].priceRange.minPrice.formattedValue + " - " + recommendPrice[i].priceRange.maxPrice.formattedValue;
                        }
                        else{
                            obj["price"] = recommendPrice[i].priceRange.minPrice.formattedValue ;
                        }
                    } else {
                        obj["price"] = "";
                    }
                    
                    
                    if (recommendPrice[i].priceRange.hasOwnProperty("minPv") && $scope.recos[i].skuid == recommendPrice[i].code) {
                        obj["psvValue"] = recommendPrice[i].priceRange.minPv.value;
                    } else {
                        obj["psvValue"] = NaN;
                    }
                    
                    if (recommendPrice[i].priceRange.hasOwnProperty("maxPv") && $scope.recos[i].skuid == recommendPrice[i].code) {
                        if(recommendPrice[i].priceRange.maxPv.value > recommendPrice[i].priceRange.minPv.value){
                            obj["maxPv"] = recommendPrice[i].priceRange.maxPv.value;
                            obj["showPsvSeperator"] = true;
                        }
                        else{
                            obj["maxPv"] = NaN;
                            obj["showPsvSeperator"] = false;
                        }
                    } else {
                    	obj["showPsvSeperator"] = false;
                        obj["maxPv"] = NaN;
                    }
                    
                } 
                else {
                    if (recommendPrice[i].hasOwnProperty("price") && $scope.recos[i].skuid == recommendPrice[i].code) {
                        obj["price"] = recommendPrice[i].price.formattedValue;
                    } else {
                        obj["price"] = "";
                    }
                    
                    if (recommendPrice[i].hasOwnProperty("pvValue") && $scope.recos[i].skuid == recommendPrice[i].code) {
                        obj["psvValue"] = recommendPrice[i].pvValue.value;
                        obj["maxPv"] = NaN;
                        obj["showPsvSeperator"] = false;
                    } else {
                        obj["psvValue"] = NaN;
                        obj["maxPv"] = NaN;			
                    }
                }
              }
                
            }
			$interval(function() {
            		$scope.slickSecondaryConfig.adaptiveHeight = true;
            		$scope.slickSecondaryConfig.enabled = true;
        		}, 500);
            
        });
        
    }
    
    self.getAvgRatings = function(skuIds) {

        var avgRatingsPromiseArray=[];
        for (var i = 0; i < skuIds.length; i++) {
            var promise = pdpCrosssellComponentService.getRatings(skuIds[i]);
            avgRatingsPromiseArray.push(promise);
        }

        $q.all(avgRatingsPromiseArray).then(function (data){
            for(var i=0;i<data.length;i++){

                var ratingValue = Math.round(data[i].streamInfo.avgRatings._overall);
                var rating = data[i].streamInfo.commentCount || 0;
                var starRating = self.getStarRatingPosition(ratingValue, 'big');

                var obj = $scope.recos[i];
                obj["rating"] = rating;
                obj["stars"] = starRating;

            }

            $interval(function() {
                if($(".carousel.carousel-secondary").hasClass("slick-initialized")){
                    $scope.slickSecondaryConfig.enabled=false;
                    $(".carousel.carousel-secondary").slick("slickRemove").slick("slickAdd");
                    $scope.slickSecondaryConfig.adaptiveHeight = true;
                    $scope.slickSecondaryConfig.enabled = true;
                }
              }, 400, [2] );
        });
    }

    self.getStarRatingPosition = function(ratingDecimalValue, starType) {
        var starTopPosition = 0;
        var starMargin = 0;
        var starWidth = 0;
        var halfStarTopMargin = 0,
            smallStarFirstPosition = 0;
        var halfStarPosition = -273;
        if (starType == "big") {
            starWidth = 18;
            halfStarPosition = -273;
            halfStarTopMargin = -26;
        } else {
            starWidth = 12;
            halfStarPosition = -242;
            halfStarTopMargin = -64;
            smallStarFirstPosition = -44;
        }
        var decimalValue = ratingDecimalValue.toString().replace(/^[^\.]+/, '0');
        var floorRatingValue = Math.floor(ratingDecimalValue);
        decimalValue = parseFloat(decimalValue);
        var mutipleValue = 0;
        if (0.26 <= decimalValue && decimalValue <= 0.74) {
            starMargin = halfStarTopMargin;
            floorRatingValue++;
        } else if (0.75 <= decimalValue && decimalValue <= 0.99) {
            starMargin = smallStarFirstPosition;
            floorRatingValue++;
        } else {
            starMargin = smallStarFirstPosition;
        }
        if (floorRatingValue >= this.max) {
            starMargin = smallStarFirstPosition;
            floorRatingValue = this.max;
        }
        floorRatingValue = floorRatingValue * starWidth;
        floorRatingValue = halfStarPosition + floorRatingValue
        return floorRatingValue + "px " + starMargin + "px";
    }
    
    function init() {
        self.initCrossSellProducts();
    }
    init();
}]);
}());
